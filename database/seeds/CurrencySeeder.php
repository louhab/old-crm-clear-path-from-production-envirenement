<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            'iso' => "MAD",
            'name' => "Moroccan Dirham",
        ]);

        DB::table('currencies')->insert([
            'iso' => "EUR",
            'name' => "Euro",
        ]);

        DB::table('currencies')->insert([
            'iso' => "USD",
            'name' => "US Dollar",
        ]);

    }
}
