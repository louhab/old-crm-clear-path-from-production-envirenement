<?php

use Illuminate\Database\Seeder;

use App\Models\MediaSheet;

class MediaSheetSeeder extends Seeder
{
    private $sheets = [
        [
           'name_fr' => 'Clear1 - Documents de l\'étudiant',
           'labelEng' => 'Clear1 - Student documents',
           'steps' => [
                [
                    11 => ['program_id' => 4],
                ],
            ]
        ],
        [
            'name_fr' => 'Clear2 - Documents des garants',
            'labelEng' => 'Clear2 - Documents of guarantors',
            'steps' => [
                [
                    12 => ['program_id' => 4],
                ],
            ]
        ],
        [
            'name_fr' => 'Clear3 - Fiche de renseignements à remplir',
            'labelEng' => 'Clear3 - Information sheet',
            'steps' => [
            ]
        ],
        [
        'name_fr' => 'Clear4 - étape de visa et permis d\'études _ les documents à fournir pour la demande de CAQ',
        'labelEng' => 'Clear4 - étape de visa et permis d\'études _ les documents à fournir pour la demande de CAQ',
        'steps' => [
                [
                    21 => ['program_id' => 4],
                ],
            ]
        ],
        [
            'name_fr' => 'Clear5 - étape de visa et permis d\'études _ les documents à fournir par l\'étudiant',
            'labelEng' => 'Clear5 - étape de visa et permis d\'études _ les documents à fournir par l\'étudiant',
            'steps' => [
                [
                    22 => ['program_id' => 4],
                ],
            ]
        ],
        [
            'name_fr' => 'Clear6 - étape de visa et permis d\'études _ les documents à fournir par les garants',
            'labelEng' => 'Clear6 - étape de visa et permis d\'études _ les documents à fournir par les garants',
            'steps' => [
                [
                    23 => ['program_id' => 4],
                ],
            ]
        ],
        [// 7
            'name_fr' => 'Clear11 - Procédure pour demander l\'équivalence des diplômes',
            'labelEng' => 'Clear11 - Diploma equivalence application procedure',
            'steps' => [
                /*[
                    11 => ['program_id' => 1],
                ],
                [
                    11 => ['program_id' => 2],
                ],
                [
                    11 => ['program_id' => 5],
                ],*/
            ]
        ],
        [// 8
            'name_fr' => 'Clear12 - Documents à fournir pour la demande de résidence permanente - Travailleur Qualifié Québec',
            'labelEng' => 'Clear12 - Documents to provide to apply for the permanent residence - Qualified Worker Quebec',
            'steps' => [
                [
                    12 => ['program_id' => 3],
                ],
            ]
        ],
        [// 9
            'name_fr' => 'Clear13 - Documents à fournir pour la demande de résidence permanente Entrée Express',
            'labelEng' => 'Clear13 - Documents to provide to apply for permanent residence - Express Entry',
            'steps' => [
                [
                    18 => ['program_id' => 1],
                ],
                /*[
                    13 => ['program_id' => 2],
                ],
                [
                   13 => ['program_id' => 5],
                ],*/
            ]
        ],
        [//10
            'name_fr' => 'Documents client: Equivalence et test de langue',
            'labelEng' => 'Client documents: Equivalence and language test',
            'steps' => [
                [
                    10 => ['program_id' => 1],
                ],
            ]
        ],
    ];

     /**
      * Run the database seeds.
      *
      * @return void
      */
     public function run()
     {
         /*foreach ($this->sheets as $phase) {
             \App\Models\MediaSheet::where("name_fr", "LIKE", "%" .$phase["name_fr"]."%")
                 ->update(['labelEng' => $phase["labelEng"]]);
         }*/
         foreach ($this->sheets as $sheet) {

             $steps = $sheet['steps'];

             unset($sheet['steps']);

             $mediaSheet = \App\Models\MediaSheet::where('name_fr', $sheet['name_fr'])->first();
             if (!$mediaSheet) {
                 $mediaSheet = \App\Models\MediaSheet::create(
                     $sheet
                 );

                //foreach ($steps as $step) $informationSheet->steps()->attach($step);
                foreach ($steps as $step) $mediaSheet->substeps()->attach($step);
             }
         }
     }
 }
