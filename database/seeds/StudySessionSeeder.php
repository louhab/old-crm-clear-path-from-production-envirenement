<?php

use Illuminate\Database\Seeder;
use App\Models\StudySession;

class StudySessionSeeder extends Seeder
{
    private $study_sessions = [
        ['label_fr'=> "Session d'automne", 'label_en'=> "Fall session"],
        ['label_fr'=> "Session d'Hiver", 'label_en'=> "Winter session"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->study_sessions as $session) {
            StudySession::create(
                [
                    'label_fr' => $session['label_fr'],
                    'label_en' => $session['label_en'],
                ]
            );
        }
    }
}
