<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CustomersImmigrantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Customer::flushEventListeners();

        $faker = Faker\Factory::create();
        \App\Models\Customer::create([
            'id' => 100,
            'program_id' => 2,
            'ranking_id' => random_int(1, 3),
            'country_id' => random_int(1, 241),
            'professional_domain_id' => random_int(1, 3),
            'english_level_id' => random_int(1, 5),
            'professional_experience' => random_int(1, 20),
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'email' => "immigrant@clearpath.ca",
            'customer_email' => $faker->email,
            'password' => Hash::make('123456'),
            'birthday' => $faker->date(),
            'adresse_line_1' => $faker->address,
            'postal_code' => $faker->postcode,
            'budget' => random_int(7000, 11000)
        ]);
        for ($i=2;$i < 100; $i++) {
            \App\Models\Customer::create([
                'id' => $i + 99,
                'program_id' => 2,
                'ranking_id' => random_int(1, 3),
                'country_id' => random_int(1, 241),
                'professional_domain_id' => random_int(1, 3),
                'english_level_id' => random_int(1, 5),
                'professional_experience' => random_int(1, 20),
                'firstname' => $faker->firstName,
                'lastname' => $faker->lastName,
                'customer_email' => $faker->email,
                'birthday' => $faker->date(),
                'adresse_line_1' => $faker->address,
                'postal_code' => $faker->postcode,
                'budget' => random_int(7000, 11000)
            ]);
        }
    }
}
