<?php

use Illuminate\Database\Seeder;

class HistoCallsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i=1;$i < 100; $i++) {
            \App\Models\HistoCall::create([
                'id' => $i,
                'user_id' => random_int(1, 3),
                'customer_id' => random_int(1, 198),
                'call_dt' => $faker->dateTime,
                'duration' => random_int(1, 30),
                'call_descr' => $faker->text
            ]);
        }
    }
}
