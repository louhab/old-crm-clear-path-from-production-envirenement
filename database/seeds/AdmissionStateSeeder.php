<?php

use Illuminate\Database\Seeder;

use App\Models\AdmissionState;

class AdmissionStateSeeder extends Seeder
{
    private $states = [
        // Admission
        [
            'admission_state_group_id' => 1,
            'label' => 'Désistement Admission',
            'labelEng' => 'Disclaimer Admission',
            'program_id' => 4,
            'visible' => 1,
            'order' => 1
        ],
        [
            'admission_state_group_id' => 1,
            'label' => 'Désistement Suivi Admission',
            'labelEng' => 'Disclaimer Admission monitoring',
            'program_id' => 4,
            'visible' => 1,
            'order' => 3
        ],
        [
            'admission_state_group_id' => 1,
            'label' => 'Désistement Admis',
            'labelEng' => 'Disclaimer Admitted',
            'program_id' => 4,
            'visible' => 1,
            'order' => 4
        ],
        [
            'admission_state_group_id' => 1,
            'label' => 'Désistement Refus',
            'labelEng' => 'Disclaimer Refus',
            'program_id' => 4,
            'visible' => 1,
            'order' => 5
        ],
        [
            'admission_state_group_id' => 1,
            'label' => 'Désistement Admission Ouverte',
            'labelEng' => 'Disclaimer Admission Opened',
            'program_id' => 4,
            'visible' => 1,
            'order' => 2
        ],
        // CAQ
        [
            'admission_state_group_id' => 2,
            'label' => 'Désistement CAQ',
            'labelEng' => 'Disclaimer CAQ',
            'program_id' => 4,
            'visible' => 1,
            'order' => 1
        ],
        [
            'admission_state_group_id' => 2,
            'label' => 'Désistement CAQ Ouvert',
            'labelEng' => 'Disclaimer CAQ Opened',
            'program_id' => 4,
            'visible' => 1,
            'order' => 2
        ],
        [
            'admission_state_group_id' => 2,
            'label' => 'Désistement CAQ Suivi',
            'labelEng' => 'Disclaimer CAQ monitoring',
            'program_id' => 4,
            'visible' => 1,
            'order' => 3
        ],
        [
            'admission_state_group_id' => 2,
            'label' => 'Désistement CAQ accepté',
            'labelEng' => 'Disclaimer CAQ accepted',
            'program_id' => 4,
            'visible' => 1,
            'order' => 4
        ],
        [
            'admission_state_group_id' => 2,
            'label' => 'Désistement CAQ refusé',
            'labelEng' => 'Disclaimer CAQ refusé',
            'program_id' => 4,
            'visible' => 1,
            'order' => 5
        ],
        // Visa&PE
        [
            'admission_state_group_id' => 3,
            'label' => 'Désistement Visa&PE',
            'labelEng' => 'Disclaimer Visa&PE',
            'program_id' => 4,
            'visible' => 1,
            'order' => 1
        ],
        [
            'admission_state_group_id' => 3,
            'label' => 'Désistement Visa&PE Ouvert',
            'labelEng' => 'Disclaimer Visa&PE Opened',
            'program_id' => 4,
            'visible' => 1,
            'order' => 2
        ],
        [
            'admission_state_group_id' => 3,
            'label' => 'Désistement Visa&PE Suivi',
            'labelEng' => 'Disclaimer Visa&PE monitoring',
            'program_id' => 4,
            'visible' => 1,
            'order' => 3
        ],
        [
            'admission_state_group_id' => 3,
            'label' => 'Désistement Visa&PE accepté',
            'labelEng' => 'Disclaimer Visa&PE accepted',
            'program_id' => 4,
            'visible' => 1,
            'order' => 4
        ],
        [
            'admission_state_group_id' => 3,
            'label' => 'Désistement Visa&PE refusé',
            'labelEng' => 'Disclaimer Visa&PE refusé',
            'program_id' => 4,
            'visible' => 1,
            'order' => 5
        ],
        // Entree Express
        // WES
        [
            'admission_state_group_id' => 4,
            'label' => 'Désistement WES',
            'labelEng' => 'Disclaimer WES',
            'program_id' => 1,
            'visible' => 1,
            'order' => 1
        ],
        [
            'admission_state_group_id' => 4,
            'label' => 'Désistement WES Ouvert',
            'labelEng' => 'Disclaimer WES Opened',
            'program_id' => 1,
            'visible' => 1,
            'order' => 2
        ],
        [
            'admission_state_group_id' => 4,
            'label' => 'Désistement WES Suivi',
            'labelEng' => 'Disclaimer WES monitoring',
            'program_id' => 1,
            'visible' => 1,
            'order' => 3
        ],
        [
            'admission_state_group_id' => 4,
            'label' => 'Désistement WES accepté',
            'labelEng' => 'Disclaimer WES accepted',
            'program_id' => 1,
            'visible' => 1,
            'order' => 4
        ],
        [
            'admission_state_group_id' => 4,
            'label' => 'Désistement WES refusé',
            'labelEng' => 'Disclaimer WES refusé',
            'program_id' => 1,
            'visible' => 1,
            'order' => 5
        ],
        // Profil
        [
            'admission_state_group_id' => 5,
            'label' => 'Désistement Profil',
            'labelEng' => 'Disclaimer Profil',
            'program_id' => 1,
            'visible' => 1,
            'order' => 1
        ],
        [
            'admission_state_group_id' => 5,
            'label' => 'Désistement Profil Ouvert',
            'labelEng' => 'Disclaimer Profil Opened',
            'program_id' => 1,
            'visible' => 1,
            'order' => 2
        ],
        [
            'admission_state_group_id' => 5,
            'label' => 'Désistement Profil Suivi',
            'labelEng' => 'Disclaimer Profil monitoring',
            'program_id' => 1,
            'visible' => 1,
            'order' => 3
        ],
        [
            'admission_state_group_id' => 5,
            'label' => 'Désistement Profil accepté',
            'labelEng' => 'Disclaimer Profil accepted',
            'program_id' => 1,
            'visible' => 1,
            'order' => 4
        ],
        [
            'admission_state_group_id' => 5,
            'label' => 'Désistement Profil refusé',
            'labelEng' => 'Disclaimer Profil refusé',
            'program_id' => 1,
            'visible' => 1,
            'order' => 5
        ],
        // VISA
        [
            'admission_state_group_id' => 6,
            'label' => 'Désistement VISA',
            'labelEng' => 'Disclaimer VISA',
            'program_id' => 1,
            'visible' => 1,
            'order' => 1
        ],
        [
            'admission_state_group_id' => 6,
            'label' => 'Désistement VISA Ouvert',
            'labelEng' => 'Disclaimer VISA Opened',
            'program_id' => 1,
            'visible' => 1,
            'order' => 2
        ],
        [
            'admission_state_group_id' => 6,
            'label' => 'Désistement VISA Suivi',
            'labelEng' => 'Disclaimer VISA monitoring',
            'program_id' => 1,
            'visible' => 1,
            'order' => 3
        ],
        [
            'admission_state_group_id' => 6,
            'label' => 'Désistement VISA accepté',
            'labelEng' => 'Disclaimer VISA accepted',
            'program_id' => 1,
            'visible' => 1,
            'order' => 4
        ],
        [
            'admission_state_group_id' => 6,
            'label' => 'Désistement VISA refusé',
            'labelEng' => 'Disclaimer VISA refusé',
            'program_id' => 1,
            'visible' => 1,
            'order' => 5
        ],
        [
            'admission_state_group_id' => 1,
            'label' => 'Repport Admission',
            'labelEng' => 'Repport Admission',
            'program_id' => 4,
            'visible' => 1,
            'order' => 1
        ],
        [
            'admission_state_group_id' => 1,
            'label' => 'Repport Suivi Admission',
            'labelEng' => 'Repport Admission monitoring',
            'program_id' => 4,
            'visible' => 1,
            'order' => 3
        ],
        [
            'admission_state_group_id' => 1,
            'label' => 'Repport Admis',
            'labelEng' => 'Repport Admitted',
            'program_id' => 4,
            'visible' => 1,
            'order' => 4
        ],
        [
            'admission_state_group_id' => 1,
            'label' => 'Repport Refus',
            'labelEng' => 'Repport Refus',
            'program_id' => 4,
            'visible' => 1,
            'order' => 5
        ],
        [
            'admission_state_group_id' => 1,
            'label' => 'Repport Admission Ouverte',
            'labelEng' => 'Repport Admission Opened',
            'program_id' => 4,
            'visible' => 1,
            'order' => 2
        ],
        // CAQ
        [
            'admission_state_group_id' => 2,
            'label' => 'Repport CAQ',
            'labelEng' => 'Repport CAQ',
            'program_id' => 4,
            'visible' => 1,
            'order' => 1
        ],
        [
            'admission_state_group_id' => 2,
            'label' => 'Repport CAQ Ouvert',
            'labelEng' => 'Repport CAQ Opened',
            'program_id' => 4,
            'visible' => 1,
            'order' => 2
        ],
        [
            'admission_state_group_id' => 2,
            'label' => 'Repport CAQ Suivi',
            'labelEng' => 'Repport CAQ monitoring',
            'program_id' => 4,
            'visible' => 1,
            'order' => 3
        ],
        [
            'admission_state_group_id' => 2,
            'label' => 'Repport CAQ accepté',
            'labelEng' => 'Repport CAQ accepted',
            'program_id' => 4,
            'visible' => 1,
            'order' => 4
        ],
        [
            'admission_state_group_id' => 2,
            'label' => 'Repport CAQ refusé',
            'labelEng' => 'Repport CAQ refusé',
            'program_id' => 4,
            'visible' => 1,
            'order' => 5
        ],
        // Visa&PE
        [
            'admission_state_group_id' => 3,
            'label' => 'Repport Visa&PE',
            'labelEng' => 'Repport Visa&PE',
            'program_id' => 4,
            'visible' => 1,
            'order' => 1
        ],
        [
            'admission_state_group_id' => 3,
            'label' => 'Repport Visa&PE Ouvert',
            'labelEng' => 'Repport Visa&PE Opened',
            'program_id' => 4,
            'visible' => 1,
            'order' => 2
        ],
        [
            'admission_state_group_id' => 3,
            'label' => 'Repport Visa&PE Suivi',
            'labelEng' => 'Repport Visa&PE monitoring',
            'program_id' => 4,
            'visible' => 1,
            'order' => 3
        ],
        [
            'admission_state_group_id' => 3,
            'label' => 'Repport Visa&PE accepté',
            'labelEng' => 'Repport Visa&PE accepted',
            'program_id' => 4,
            'visible' => 1,
            'order' => 4
        ],
        [
            'admission_state_group_id' => 3,
            'label' => 'Repport Visa&PE refusé',
            'labelEng' => 'Repport Visa&PE refusé',
            'program_id' => 4,
            'visible' => 1,
            'order' => 5
        ],
        // Entree Express
        // WES
        [
            'admission_state_group_id' => 4,
            'label' => 'Repport WES',
            'labelEng' => 'Repport WES',
            'program_id' => 1,
            'visible' => 1,
            'order' => 1
        ],
        [
            'admission_state_group_id' => 4,
            'label' => 'Repport WES Ouvert',
            'labelEng' => 'Repport WES Opened',
            'program_id' => 1,
            'visible' => 1,
            'order' => 2
        ],
        [
            'admission_state_group_id' => 4,
            'label' => 'Repport WES Suivi',
            'labelEng' => 'Repport WES monitoring',
            'program_id' => 1,
            'visible' => 1,
            'order' => 3
        ],
        [
            'admission_state_group_id' => 4,
            'label' => 'Repport WES accepté',
            'labelEng' => 'Repport WES accepted',
            'program_id' => 1,
            'visible' => 1,
            'order' => 4
        ],
        [
            'admission_state_group_id' => 4,
            'label' => 'Repport WES refusé',
            'labelEng' => 'Repport WES refusé',
            'program_id' => 1,
            'visible' => 1,
            'order' => 5
        ],
        // Profil
        [
            'admission_state_group_id' => 5,
            'label' => 'Repport Profil',
            'labelEng' => 'Repport Profil',
            'program_id' => 1,
            'visible' => 1,
            'order' => 1
        ],
        [
            'admission_state_group_id' => 5,
            'label' => 'Repport Profil Ouvert',
            'labelEng' => 'Repport Profil Opened',
            'program_id' => 1,
            'visible' => 1,
            'order' => 2
        ],
        [
            'admission_state_group_id' => 5,
            'label' => 'Repport Profil Suivi',
            'labelEng' => 'Repport Profil monitoring',
            'program_id' => 1,
            'visible' => 1,
            'order' => 3
        ],
        [
            'admission_state_group_id' => 5,
            'label' => 'Repport Profil accepté',
            'labelEng' => 'Repport Profil accepted',
            'program_id' => 1,
            'visible' => 1,
            'order' => 4
        ],
        [
            'admission_state_group_id' => 5,
            'label' => 'Repport Profil refusé',
            'labelEng' => 'Repport Profil refusé',
            'program_id' => 1,
            'visible' => 1,
            'order' => 5
        ],
        // VISA
        [
            'admission_state_group_id' => 6,
            'label' => 'Repport VISA',
            'labelEng' => 'Repport VISA',
            'program_id' => 1,
            'visible' => 1,
            'order' => 1
        ],
        [
            'admission_state_group_id' => 6,
            'label' => 'Repport VISA Ouvert',
            'labelEng' => 'Repport VISA Opened',
            'program_id' => 1,
            'visible' => 1,
            'order' => 2
        ],
        [
            'admission_state_group_id' => 6,
            'label' => 'Repport VISA Suivi',
            'labelEng' => 'Repport VISA monitoring',
            'program_id' => 1,
            'visible' => 1,
            'order' => 3
        ],
        [
            'admission_state_group_id' => 6,
            'label' => 'Repport VISA accepté',
            'labelEng' => 'Repport VISA accepted',
            'program_id' => 1,
            'visible' => 1,
            'order' => 4
        ],
        [
            'admission_state_group_id' => 6,
            'label' => 'Repport VISA refusé',
            'labelEng' => 'Repport VISA refusé',
            'program_id' => 1,
            'visible' => 1,
            'order' => 5
        ],
        [
            'admission_state_group_id' => 6,
            'label' => 'Canada',
            'labelEng' => 'Canada',
            'program_id' => 1,
            'visible' => 1,
            'order' => 5
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->states as $phase) {
            AdmissionState::where("label", "LIKE", $phase[0])
                ->update(['labelEng' => $phase[1]]);
        }*/
        foreach ($this->states as $state) {
            $status = AdmissionState::where("label", $state["label"]);
            if ($status->first()) {
                $status->update($state);
            } else {
                AdmissionState::create(
                    $state
                );
            }
        }
    }
}
