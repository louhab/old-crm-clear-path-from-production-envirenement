<?php

use Illuminate\Database\Seeder;
use App\Models\AmountAvailable;

class AddAmountsSeeder extends Seeder
{
    private $amounts = [
        1 => [
            ">= 100000 EURO",
            ">= 100000 USD"
        ],
        2 => [
            "125000 EURO",
            "125000 USD",
        ],
        3 => [
            "150000 EURO",
            "150000 USD",
        ],
        4 => [
            "175000 EURO",
            "175000 USD",
        ],
        5 => [
            ">= 200000 EURO",
            ">= 200000 USD",
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->amounts as $id => $amount) {
            $amounts = AmountAvailable::find($id);
            $amounts->amount_eur = $amount[0];
            $amounts->amount_usd = $amount[1];
            $amounts->save();
        }
    }
}
