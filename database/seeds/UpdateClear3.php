<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UpdateClear3 extends Seeder
{
    private $customerFields = [
        [
            'customer_field_group_id' => 1,
            'field_name' => 'eye_colour',
            'template' => 'text',
        ],
        [
            'customer_field_group_id' => 1,
            'field_name' => 'size',
            'template' => 'text',
        ],
        [
            'customer_field_group_id' => 1,
            'field_name' => 'previous_spouse_firstname',
            'template' => 'text',
        ],
        [
            'customer_field_group_id' => 1,
            'field_name' => 'previous_spouse_lastname',
            'template' => 'text',
        ],
        [
            'customer_field_group_id' => 1,
            'field_name' => 'passport_number',
            'template' => 'text',
        ],
        [
            'customer_field_group_id' => 1,
            'field_name' => 'passport_expiration_date',
            'template' => 'date',
        ],


        [
            'customer_field_group_id' => 2,
            'field_name' => 'work_business_status',
            'template' => 'text',
        ],

        // ✅
        [
            'customer_field_group_id' => 5,
            'customer_field_sub_group_id' => 8,
            'field_name' => 'various_travel_city',
            'template' => 'text',
        ],

        [
            'customer_field_group_id' => 5,
            'customer_field_sub_group_id' => 7,
            'field_name' => 'past_residence_zip',
            'template' => 'text',
        ],

        // en cours (done) to be checked later
        [
            'customer_field_group_id' => 5,
            'field_name' => 'desired_province_for_living',
            'template' => 'text',
        ],
        [
            'customer_field_group_id' => 5,
            'field_name' => 'desired_city_for_living',
            'template' => 'text',
        ],
        [
            'customer_field_group_id' => 5,
            'field_name' => 'has_cert_for_living',
            'template' => 'text',
        ],

        // extra (section 6) (like section 1)
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_received_conviction',
            'template' => 'select',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_committed_criminal_act',
            'template' => 'select',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_submitted_request',
            'template' => 'select',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_denied_refugee_status',
            'template' => 'select',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_been_refused_visa',
            'template' => 'select',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_took_part_in_act_genocide',
            'template' => 'select',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_advocated_use_armed_struggle',
            'template' => 'select',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_had_ties_to_group_that_used_armed_struggle',
            'template' => 'select',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_been_member_of_organization',
            'template' => 'select',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_is_subject_detention',
            'template' => 'select',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_suffered_from_serious_illness',
            'template' => 'select',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
        ],

    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->customerFields as $field) {
            $customer_field_group_id = $field["customer_field_group_id"];
            unset($field["customer_field_group_id"]);

            $lastId = DB::table('customer_fields')->insertGetId($field);

            DB::table('customer_field_information_sheet')->insert([
                "customer_field_id" => $lastId,
                "information_sheet_id" => 3,
                "customer_field_group_id" => $customer_field_group_id,
                "program_id" => 1,
                "required" => 1,
                "order" => 10
            ]);
        }
    }
}
