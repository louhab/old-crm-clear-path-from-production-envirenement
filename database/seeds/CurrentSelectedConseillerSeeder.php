<?php

use Illuminate\Database\Seeder;

use App\Models\CurrentSelectedConseiller;

class CurrentSelectedConseillerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CurrentSelectedConseiller::create(
            [
                'conseiller_id' => 0
            ]
        );
    }
}
