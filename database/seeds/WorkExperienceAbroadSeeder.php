<?php

use Illuminate\Database\Seeder;

use App\Models\WorkExperienceAbroad;

class WorkExperienceAbroadSeeder extends Seeder
{
    private $scores = [
        ['code'=> 'A', 'label'=> 'None or less than a year'     ,"labelEnd"=>""],
        ['code'=> 'B', 'label'=> '1 year'                         ,"labelEnd"=>""],
        ['code'=> 'C', 'label'=> '2 years'                        ,"labelEnd"=>""],
        ['code'=> 'D', 'label'=> '3 years or more'                ,"labelEnd"=>""],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->scores as $score) {
            WorkExperienceAbroad::create(
                [
                    'label' => $score['label'],
                    'code' => $score['code'],
                ]
            );
        }
    }
}
