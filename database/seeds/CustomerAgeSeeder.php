<?php

use Illuminate\Database\Seeder;

use App\Models\CustomerAge;

class CustomerAgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $letters = range('A', 'Z');
        $code_ = '';
        $ind   = 0;
        foreach(range(17,45) as $nbr) {

            $code = $code_ . $letters[$ind];
            $age = $nbr . ' ans';
            $age_eng= $nbr . " years old";

            if($nbr == 17){
                $age = $nbr . ' ans ou moins';
                $age_eng = $nbr . ' years old or less';
            }

            if($nbr == 45){
                $age = $nbr . ' ans ou plus';
                $age_eng = $nbr . ' years old or more';
            }

            CustomerAge::create(
                [
                    'label' => $age,
                    'code'  => $code,
                    'labelEng'=>$age_eng
                ]
            );
            /*CustomerAge::where("label", "LIKE", $age)
                ->update(['labelEng' => $age_eng]);*/
            if ($letters[$ind] == 'Z') {
                $code_ = 'A';
                $ind = -1;
            }
            $ind += 1;
        }
    }
}
    /*
    <select id="q3" name="q3" class="form-control mrgn-bttm-md">
      <option value="badvalue" label="Selectionner...">Selectionner...</option>
      <option value="A" "="">17 ans ou moins</option>
      <option value="B" "="">18 ans</option>
      <option value="C" "="">19 ans</option>
      <option value="D" "="">20 ans</option>
      <option value="E" "="">21 ans</option>
      <option value="F" "="">22 ans</option>
      <option value="G" "="">23 ans</option>
      <option value="H" "="">24 ans</option>
      <option value="I" "="">25 ans</option>
      <option value="J" "="">26 ans</option>
      <option value="K" "="">27 ans</option>
      <option value="L" "="">28 ans</option>
      <option value="M" "="">29 ans</option>
      <option value="N" "="">30 ans</option>
      <option value="O" "="">31 ans</option>
      <option value="P" "="">32 ans</option>
      <option value="Q" "="">33 ans</option>
      <option value="R" "="">34 ans</option>
      <option value="S" "="">35 ans</option>
      <option value="T" "="">36 ans</option>
      <option value="U" "="">37 ans</option>
      <option value="V" "="">38 ans</option>
      <option value="W" "="">39 ans</option>
      <option value="X" "="">40 ans</option>
      <option value="Y" "="">41 ans</option>
      <option value="Z" "="">42 ans</option>
      <option value="AA" "="">43 ans</option>
      <option value="AB" "="">44 ans</option>
      <option value="AC" "="">45 ans ou plus</option>
    </select>
    */
