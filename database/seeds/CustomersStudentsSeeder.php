<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CustomersStudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Customer::flushEventListeners();

        $faker = Faker\Factory::create();
        \App\Models\Customer::create([
            'id' => 1,
            'program_id' => 1,
            'ranking_id' => random_int(1, 3),
            'country_id' => random_int(1, 241),
            'school_level_id' => random_int(1, 3),
            'english_level_id' => random_int(1, 5),
            'projected_diploma_type_id' => random_int(1, 3),
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'email' => "student@clearpath.ca",
            'customer_email' => $faker->email,
            'password' => Hash::make('123456'),
            'birthday' => $faker->date(),
            'adresse_line_1' => $faker->address,
            'postal_code' => $faker->postcode,
            'budget' => random_int(7000, 11000)
        ]);
        for ($i=2;$i < 100; $i++) {
            \App\Models\Customer::create([
                'id' => $i,
                'program_id' => 1,
                'ranking_id' => random_int(1, 3),
                'country_id' => random_int(1, 241),
                'school_level_id' => random_int(1, 3),
                'english_level_id' => random_int(1, 5),
                'projected_diploma_type_id' => random_int(1, 3),
                'firstname' => $faker->firstName,
                'lastname' => $faker->lastName,
                'customer_email' => $faker->email,
                'birthday' => $faker->date(),
                'adresse_line_1' => $faker->address,
                'postal_code' => $faker->postcode,
                'budget' => random_int(7000, 11000)
            ]);
        }
    }
}
