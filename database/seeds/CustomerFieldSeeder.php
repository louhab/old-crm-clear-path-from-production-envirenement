<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CustomerFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // for UK
        // Seeders to be add is
        // 🔥 1- program_steps (customer_field_program_step)

        $program_steps = file_get_contents(__DIR__ . '/customer_field_program_step.json');
        $program_steps_out = json_decode($program_steps, true);

        foreach ($program_steps_out as $key => $value) {
            DB::table('customer_field_program_step')->insert(
                [
                    "customer_field_id" => $value["customer_field_id"],
                    "program_step_id" => $value["program_step_id"],
                    "program_id" => 31,
                    "order" => $value["order"],
                    "required" => $value["required"],
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                ]
            );
        }

        // 🔥 2- program_sub_steps (customer_field_program_sub_step)
        $program_sub_steps = file_get_contents(__DIR__ . '/customer_field_program_sub_step.json');
        $program_sub_steps_out = json_decode($program_sub_steps, true);

        foreach ($program_sub_steps_out as $key => $value) {
            DB::table('customer_field_program_sub_step')->insert(
                [
                    "customer_field_id" => $value["customer_field_id"],
                    "program_sub_step_id" => $value["program_sub_step_id"],
                    "program_id" => 31,
                    "order" => $value["order"],
                    "required" => $value["required"],
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                ]
            );
        }

        // 🔥 3- InformationSheet (customer_field_information_sheet)

        $information_sheet = file_get_contents(__DIR__ . '/customer_field_information_sheet.json');
        $information_sheet_out = json_decode($information_sheet, true);

        foreach ($information_sheet_out as $key => $value) {
            DB::table('customer_field_information_sheet')->insert(
                [
                    "customer_field_id" => $value["customer_field_id"],
                    "information_sheet_id" => $value["information_sheet_id"],
                    "program_id" => 31,
                    "customer_field_group_id" => $value["customer_field_group_id"],
                    "order" => $value["order"],
                    "required" => $value["required"],
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now()
                ]
            );
        }


        // for ES
        // Seeders to be add is
        // 🔥 1- program_steps (customer_field_program_step)

        $program_steps = file_get_contents(__DIR__ . '/customer_field_program_step.json');
        $program_steps_out = json_decode($program_steps, true);

        foreach ($program_steps_out as $key => $value) {
            DB::table('customer_field_program_step')->insert(
                [
                    "customer_field_id" => $value["customer_field_id"],
                    "program_step_id" => $value["program_step_id"],
                    "program_id" => 32,
                    "order" => $value["order"],
                    "required" => $value["required"],
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                ]
            );
        }

        // 🔥 2- program_sub_steps (customer_field_program_sub_step)
        $program_sub_steps = file_get_contents(__DIR__ . '/customer_field_program_sub_step.json');
        $program_sub_steps_out = json_decode($program_sub_steps, true);

        foreach ($program_sub_steps_out as $key => $value) {
            DB::table('customer_field_program_sub_step')->insert(
                [
                    "customer_field_id" => $value["customer_field_id"],
                    "program_sub_step_id" => $value["program_sub_step_id"],
                    "program_id" => 32,
                    "order" => $value["order"],
                    "required" => $value["required"],
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                ]
            );
        }

        // 🔥 3- InformationSheet (customer_field_information_sheet)

        $information_sheet = file_get_contents(__DIR__ . '/customer_field_information_sheet.json');
        $information_sheet_out = json_decode($information_sheet, true);

        foreach ($information_sheet_out as $key => $value) {
            DB::table('customer_field_information_sheet')->insert(
                [
                    "customer_field_id" => $value["customer_field_id"],
                    "information_sheet_id" => $value["information_sheet_id"],
                    "program_id" => 32,
                    "customer_field_group_id" => $value["customer_field_group_id"],
                    "order" => $value["order"],
                    "required" => $value["required"],
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now()
                ]
            );
        }
    }
}
