<?php

use App\Models\TuitionFee;
use Illuminate\Database\Seeder;

class TuitionFeesSeeder extends Seeder
{
    private $elements = [
        ['label_fr'=> "Non, je ne veux pas payer les frais de scolarité en avance", 'label_en'=> "Non, je ne veux pas payer les frais de scolarité en avance"],
        ['label_fr'=> "Je paierai les frais de scolarité de la première session avant la soumission de mon dossier Visa et permis d'études", 'label_en'=> "Je paierai les frais de scolarité de la première session avant la soumission de mon dossier Visa et permis d'études"],
        ['label_fr'=> "Je paierai les frais de scolarité de la première année avant la soumission de mon dossier Visa et permis d'études", 'label_en'=> "Je paierai les frais de scolarité de la première année avant la soumission de mon dossier Visa et permis d'études"],
        ['label_fr'=> "Je paierai la totalité des frais scolaires avant la soumission de mon dossier Visa et permis d'études", 'label_en'=> "Je paierai la totalité des frais scolaires avant la soumission de mon dossier Visa et permis d'études"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->elements as $element) {
            TuitionFee::create(
                [
                    'label_fr' => $element['label_fr'],
                    'label_en' => $element['label_en'],
                ]
            );
        }
    }
}
