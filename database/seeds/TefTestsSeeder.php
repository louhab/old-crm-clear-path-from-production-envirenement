<?php

use App\Models\TefTest;
use Illuminate\Database\Seeder;

class TefTestsSeeder extends Seeder
{
    private $elements = [
        ['label_fr'=> "Non, je ne peux pas passer le test TEF", 'label_en'=> "Non, je ne peux pas passer le test TEF"],
        ['label_fr'=> "Je passerai le test TEF", 'label_en'=> "Je passerai le test TEF"],
        ['label_fr'=> "J'ai déjà fait le test TEF", 'label_en'=> "J'ai déjà fait le test TEF"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->elements as $element) {
            TefTest::create(
                [
                    'label_fr' => $element['label_fr'],
                    'label_en' => $element['label_en'],
                ]
            );
        }
    }
}
