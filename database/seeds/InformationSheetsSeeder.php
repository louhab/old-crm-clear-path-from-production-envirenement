<?php

use Illuminate\Database\Seeder;

class InformationSheetsSeeder extends Seeder
{
    private $sheets = [
        [
            'name_fr' => 'Clear1000 - Questionnaire',
            'name_en' => 'Clear1000 - Questionnaire',
            'steps' => [
                [
                    5 => ['program_id' => 4],
                ],
            ]
        ],
        [
            'name_fr' => 'Clear1001 - Conseil',
            'name_en' => 'Clear1001 - Advice',
            'steps' => [
                [
                    6 => ['program_id' => 1],
                ],
                [
                    6 => ['program_id' => 2],
                ],
                [
                    6 => ['program_id' => 3],
                ],
                [
                    6 => ['program_id' => 4],
                ],
                [
                    6 => ['program_id' => 5],
                ],
                [
                    6 => ['program_id' => 6],
                ],
                [
                    6 => ['program_id' => 7],
                ],
                [
                    6 => ['program_id' => 8],
                ],
                [
                    6 => ['program_id' => 9],
                ],
                [
                    6 => ['program_id' => 10],
                ],
                [
                    6 => ['program_id' => 11],
                ],
                [
                    6 => ['program_id' => 12],
                ],
                [
                    6 => ['program_id' => 13],
                ],
                [
                    6 => ['program_id' => 14],
                ],
                [
                    6 => ['program_id' => 15],
                ],
                [
                    6 => ['program_id' => 16],
                ],
                [
                    6 => ['program_id' => 17],
                ],
                [
                    6 => ['program_id' => 18],
                ],
                [
                    6 => ['program_id' => 19],
                ],
                [
                    6 => ['program_id' => 20],
                ],
                [
                    6 => ['program_id' => 21],
                ],
                [
                    6 => ['program_id' => 22],
                ],
                [
                    6 => ['program_id' => 23],
                ],
            ]
          ],
        [
            'name_fr' => 'Fiche de renseignement',
            'name_en' => 'Information sheet',
            'steps' => [
                [
                    11 => ['program_id' => 1],
                ],
                [
                    11 => ['program_id' => 2],
                ],
                [
                    13 => ['program_id' => 3],
                ],
                [
                    13 => ['program_id' => 4],
                ],
                [
                    13 => ['program_id' => 13],
                ],
            ]
          ],
          [
            'name_fr' => 'Clear1002 - Orientation',
            'name_en' => 'Clear1002 - Orientation',
            'steps' => [
                [
                    14 => ['program_id' => 4],
                ],
            ]
          ],
          [
            'name_fr' => "Clear7 - Questionnaire pour préparation à l'entrevue",
            'name_en' => "Clear7 - Questionnaire for interview preparation",
            'steps' => [
                [
                    30 => ['program_id' => 1],
                ],
                [
                    30 => ['program_id' => 2],
                ],
                [
                    30 => ['program_id' => 3],
                ],
                [
                    30 => ['program_id' => 4],
                ],
                [
                    30 => ['program_id' => 5],
                ],
                [
                    30 => ['program_id' => 6],
                ],
                [
                    30 => ['program_id' => 7],
                ],
                [
                    30 => ['program_id' => 8],
                ],
                [
                    30 => ['program_id' => 9],
                ],
                [
                    30 => ['program_id' => 10],
                ],
                [
                    30 => ['program_id' => 11],
                ],
                [
                    30 => ['program_id' => 12],
                ],
                [
                    30 => ['program_id' => 13],
                ],
                [
                    30 => ['program_id' => 14],
                ],
                [
                    30 => ['program_id' => 15],
                ],
                [
                    30 => ['program_id' => 16],
                ],
                [
                    30 => ['program_id' => 17],
                ],
                [
                    30 => ['program_id' => 18],
                ],
                [
                    30 => ['program_id' => 19],
                ],
                [
                    30 => ['program_id' => 20],
                ],
                [
                    30 => ['program_id' => 21],
                ],
                [
                    30 => ['program_id' => 22],
                ],
                [
                    30 => ['program_id' => 23],
                ],
            ]
          ],
          [
            'name_fr' => "Clear8 - Questionnaire corrigé pour préparer l'entrevue",
            'name_en' => "Clear8 - Corrected questionnaire to prepare for the interview",
            'steps' => [
                [
                    31 => ['program_id' => 1],
                ],
                [
                    31 => ['program_id' => 2],
                ],
                [
                    31 => ['program_id' => 3],
                ],
                [
                    31 => ['program_id' => 4],
                ],
                [
                   31 => ['program_id' => 5],
                ],
                [
                   31 => ['program_id' => 6],
                ],
                [
                   31 => ['program_id' => 7],
                ],
                [
                   31 => ['program_id' => 8],
                ],
                [
                   31 => ['program_id' => 9],
                ],
                [
                   31 => ['program_id' => 10],
                ],
                [
                   31 => ['program_id' => 11],
                ],
                [
                   31 => ['program_id' => 12],
                ],
                [
                   31 => ['program_id' => 13],
                ],
                [
                   31 => ['program_id' => 14],
                ],
                [
                   31 => ['program_id' => 15],
                ],
                [
                   31 => ['program_id' => 16],
                ],
                [
                   31 => ['program_id' => 17],
                ],
                [
                   31 => ['program_id' => 18],
                ],
                [
                   31 => ['program_id' => 19],
                ],
                [
                   31 => ['program_id' => 20],
                ],
                [
                   31 => ['program_id' => 21],
                ],
                [
                   31 => ['program_id' => 22],
                ],
                [
                   31 => ['program_id' => 23],
                ],
            ]
          ],
        [
            'name_fr' => "Clear1004 - Scoring",
            'name_en' => "Clear1004 - Scoring",
            'steps' => [
                [
                    5 => ['program_id' => 1],
                ],
                [
                    5 => ['program_id' => 2],
                ],
                [
                    5 => ['program_id' => 3],
                ],
                [
                   5 => ['program_id' => 5],
                ],
                [
                   5 => ['program_id' => 6],
                ],
                [
                   5 => ['program_id' => 7],
                ],
                [
                   5 => ['program_id' => 8],
                ],
                [
                   5 => ['program_id' => 9],
                ],
                [
                   5 => ['program_id' => 10],
                ],
                [
                   5 => ['program_id' => 11],
                ],
                [
                   5 => ['program_id' => 12],
                ],
                [
                   5 => ['program_id' => 13],
                ],
                [
                   5 => ['program_id' => 14],
                ],
                [
                   5 => ['program_id' => 15],
                ],
                [
                   5 => ['program_id' => 16],
                ],
                [
                   5 => ['program_id' => 17],
                ],
                [
                   5 => ['program_id' => 18],
                ],
                [
                   5 => ['program_id' => 19],
                ],
                [
                   5 => ['program_id' => 20],
                ],
                [
                   5 => ['program_id' => 21],
                ],
                [
                   5 => ['program_id' => 22],
                ],
                [
                   5 => ['program_id' => 23],
                ],
            ]
        ],
        [
            'name_fr' => 'Fiche de renseignement:',
            'name_en' => 'Fiche de renseignement:',
            'steps' => [

            ]
        ],
        [
            'name_fr' => 'Clear10 - Fiche de renseignement',
            'name_en' => 'Clear10 - Fiche de renseignement',
            'steps' => [
                [
                    5 => ['program_id' => 1],
                ],
            ]
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->sheets as $phase) {
            \App\Models\InformationSheet::where("name_fr", "LIKE", $phase["name_fr"])
                ->update(['name_en' => $phase["name_en"]]);
        }*/
        foreach ($this->sheets as $sheet) {

            $steps = $sheet['steps'];

            unset($sheet['steps']);

            $informationSheet = \App\Models\InformationSheet::where('name_fr', $sheet['name_fr'])->first();
            if (!$informationSheet) {
                $informationSheet = \App\Models\InformationSheet::create(
                    $sheet
                );
            }

            //foreach ($steps as $step) $informationSheet->steps()->attach($step);
            foreach ($steps as $step) $informationSheet->substeps()->attach($step);
        }
    }
}
