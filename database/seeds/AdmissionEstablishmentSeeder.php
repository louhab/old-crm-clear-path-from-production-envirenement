<?php

use Illuminate\Database\Seeder;

use App\Models\AdmissionEstablishment;

class AdmissionEstablishmentSeeder extends Seeder
{
    private $etbs = [
        ["label" => "CEGEP","lableEng"=>"CEGEP"],//1
        ["label" => "Collège","lableEng"=>"College"],//2
        ["label" => "Université","lableEng"=>"University"],//3
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->etbs as $etb) {
            AdmissionEstablishment::create($etb);
        }
    }
}
