<?php

use App\Models\GuarantorBusinessIncome;
use Illuminate\Database\Seeder;

class GuarantorBusinessIncomesSeeder extends Seeder
{
    private $incomes = [
        ['label_fr'=> "Un revenu d'entreprise", 'label_en'=> "Un revenu d'entreprise"],
        ['label_fr'=> "Deux revenus d'entreprise", 'label_en'=> "Deux revenus d'entreprise"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->incomes as $income) {
            GuarantorBusinessIncome::create(
                [
                    'label_fr' => $income['label_fr'],
                    'label_en' => $income['label_en'],
                ]
            );
        }
    }
}
