<?php

use Illuminate\Database\Seeder;

class LeadsStudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i=1;$i < 100; $i++) {
            \App\Models\Lead::create([
                'id' => $i,
                'program_id' => 1,
                'ranking_id' => random_int(1, 3),
                'country_id' => random_int(1, 241),
                'school_level_id' => random_int(1, 3),
                'english_level_id' => random_int(1, 5),
                'projected_diploma_type_id' => random_int(1, 3),
                'firstname' => $faker->firstName,
                'lastname' => $faker->lastName,
                'email' => $faker->email,
                'birthday' => $faker->date(),
                'adresse_line_1' => $faker->address,
                'postal_code' => $faker->postcode,
                'budget' => random_int(7000, 11000)
            ]);
        }
    }
}
