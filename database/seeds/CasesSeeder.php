<?php

use Illuminate\Database\Seeder;

class CasesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1;$i<100;$i++) {
            \App\Models\CustomerCase::create([
                'id' => $i,
                'user_id' => random_int(1, 3),
                'customer_id' => random_int(1, 99),
                'program_id' => 1,
                'program_step_id' => random_int(1, 12)
            ]);
        }

        /*for ($i=100;$i<199;$i++) {
            \App\Models\CustomerCase::create([
                'id' => $i,
                'user_id' => random_int(1, 3),
                'customer_id' => random_int(100, 198),
                'program_id' => 2,
                'program_step_id' => random_int(12, 24)
            ]);
        }*/
    }
}
