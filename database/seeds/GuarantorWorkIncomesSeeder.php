<?php

use App\Models\GuarantorWorkIncome;
use Illuminate\Database\Seeder;

class GuarantorWorkIncomesSeeder extends Seeder
{
    private $incomes = [
        ['label_fr'=> "Un revenu de travail", 'label_en'=> "Un revenu de travail"],
        ['label_fr'=> "Deux revenus de travail", 'label_en'=> "Deux revenus de travail"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->incomes as $income) {
            GuarantorWorkIncome::create(
                [
                    'label_fr' => $income['label_fr'],
                    'label_en' => $income['label_en'],
                ]
            );
        }
    }
}
