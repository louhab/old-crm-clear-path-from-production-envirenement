<?php

use Illuminate\Database\Seeder;

use App\Models\MaritalStatus;

class MaritalStatusSeeder extends Seeder
{
    private $statuses = [
        ['code' => 'A', 'label_fr' => 'Mariage annulé', 'label_en' => 'Canceled wedding'],
        ['code' => 'B', 'label_fr' => 'Conjoint de fait', 'label_en' => 'Common-law partner'],
        ['code' => 'C', 'label_fr' => 'Divorcé/séparé', 'label_en' => 'Divorced / separated'],
        ['code' => 'D', 'label_fr' => 'Séparé légalement', 'label_en' => 'Legally separated'],
        ['code' => 'E', 'label_fr' => 'Marié', 'label_en' => 'Married'],
        ['code' => 'F', 'label_fr' => 'Jamais marié/célibataire', 'label_en' => 'Never married / single'],
        ['code' => 'G', 'label_fr' => 'Veuf', 'label_en' => 'Widower'],
    ];

    /**
     * <select id="q1" name="q1" class="form-control mrgn-bttm-md">
     *   <option value="A">Mariage annulé</option>
     *   <option value="B">Conjoint de fait</option>
     *   <option value="C">Divorcé/séparé</option>
     *   <option value="D">Séparé légalement</option>
     *   <option value="E">Marié</option>
     *   <option value="F">Jamais marié/célibataire</option>
     *   <option value="G">Veuf</option>
     * </select>
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->statuses as $status) {
            MaritalStatus::create(
                [
                    'status_label_fr' => $status['label_fr'],
                    'status_label_en' => $status['label_en'],
                    'code' => $status['code'],
                ]
            );
        }
    }
}
