<?php

use Illuminate\Database\Seeder;

class ProgramSubStepsSeeder extends Seeder
{
    private $pgrSteps = [
        ['step_id' => 1, 'step_label' => "Informations Personnelles", "visible_for_conseiller" => 1, "visible_for_agent" => 1, "visible_for_backoffice" => 0                                    ,"labelEng"=>"Personal Information"],//1
        ['step_id' => 1, 'step_label' => "Tasks", "visible_for_conseiller" => 1, "visible_for_agent" => 1, "visible_for_backoffice" => 0                                                        ,"labelEng"=>"Tasks"],//2
        ['step_id' => 2, 'step_label' => "Paiement 1:Disabled", "visible_for_conseiller" => 1, "visible_for_agent" => 1, "visible_for_backoffice" => 0                                          ,"labelEng"=>"Payment 1: Disabled"],//3
        ['step_id' => 2, 'step_label' => "CV", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                           ,"labelEng"=>"CV"],//4
        ['step_id' => 2, 'step_label' => "Clear 1000", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                   ,"labelEng"=>"Clear 1000"],//5
        ['step_id' => 2, 'step_label' => "Clear 1001", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                   ,"labelEng"=>"Clear 1001"],//6
        ['step_id' => 2, 'step_label' => "Paiement 1", "visible_for_conseiller" => 1, "visible_for_agent" => 1, "visible_for_backoffice" => 0                                                   ,"labelEng"=>"Payment 1"],//7
        ['step_id' => 3, 'step_label' => "Paiement 2", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                   ,"labelEng"=>"Payment 2"],//8
        ['step_id' => 3, 'step_label' => "Orientation", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                  ,"labelEng"=>"Orientation"],//9
        ['step_id' => 4, 'step_label' => "Paiement 3", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                   ,"labelEng"=>"Payment 3"],//10
        ['step_id' => 4, 'step_label' => "Clear1 - Documents de l'étudiant", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                             ,"labelEng"=>"Clear1 - Student documents"],//11
        ['step_id' => 4, 'step_label' => "Clear2 - Documents des garants", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                               ,"labelEng"=>"Clear2 - Guarantor documents"],//12
        ['step_id' => 4, 'step_label' => "Orientation - Documents", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                      ,"labelEng"=>"Orientation - Documents"],//13
        ['step_id' => 4, 'step_label' => "Clear1002 - Orientation", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                      ,"labelEng"=>"Clear1002 - Orientation"],//14
        ['step_id' => 5, 'step_label' => "Tasks", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 1                                                        ,"labelEng"=>"Tasks"],//15
        ['step_id' => 6, 'step_label' => "Admission", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                    ,"labelEng"=>"Admission"],//16
        ['step_id' => 6, 'step_label' => "Lettres d'acceptation", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                        ,"labelEng"=>"Letters of acceptance"],//17
        ['step_id' => 6, 'step_label' => "Inscription et choix de cours", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                ,"labelEng"=>"Registration and course selection"],//18
        ['step_id' => 7, 'step_label' => "Paiement 4", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                   ,"labelEng"=>"Payment 4"],//19
        ['step_id' => 7, 'step_label' => "Tasks", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                        ,"labelEng"=>"Tasks"],//20
        ['step_id' => 8, 'step_label' => "Clear4 - Documents à fournir pour la demande de CAQ", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0          ,"labelEng"=>"Clear4 - Documents to be provided for the CAQ"],//21
        ['step_id' => 8, 'step_label' => "Clear5 - Compléments des documents de l'étudiant", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0             ,"labelEng"=>"Clear5 - Completion of the student documents"],//22
        ['step_id' => 8, 'step_label' => "Clear6 - Compléments des documents des garants", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0               ,"labelEng"=>"Clear6 - Completion of the documents of the guarantees"],//23
        ['step_id' => 8, 'step_label' => "CAQ Notifications", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 1                                            ,"labelEng"=>"CAQ Notifications"],//24
        ['step_id' => 8, 'step_label' => "Visa et PE Notifications", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 1                                     ,"labelEng"=>"Visa and Student Permit Notifications"],//25
        ['step_id' => 9, 'step_label' => "Paiement 5", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                   ,"labelEng"=>"Paiement 5"],//26
        ['step_id' => 9, 'step_label' => "Tasks", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                        ,"labelEng"=>"Tasks"],//27
        ['step_id' => 10, 'step_label' => "Notifications", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 1                                               ,"labelEng"=>"Notifications"],//28
        ['step_id' => 10, 'step_label' => "Imm5739", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                     ,"labelEng"=>"Imm5739"],//29
        ['step_id' => 11, 'step_label' => "Clear7 - Questionnaire pour préparation à l'entrevue", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0        ,"labelEng"=>"Clear7 - Interview preparation questionnaire"],//30
        ['step_id' => 11, 'step_label' => "Clear8 - Questionnaire corrigé pour préparer l'entrevue", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0     ,"labelEng"=>"Clear8 - Corrected questionnaire to get prepared for the interview"],//31
        ['step_id' => 11, 'step_label' => "Immigration", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                 ,"labelEng"=>"Immigration"],//32
        ['step_id' => 11, 'step_label' => "Lettre de demande des documents additionnels", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                ,"labelEng"=>"Letter to request additional documents"],//33
        ['step_id' => 12, 'step_label' => "Séjour", "visible_for_conseiller" => 1, "visible_for_agent" => 0, "visible_for_backoffice" => 0                                                      ,"labelEng"=>"Stay"],//34
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->pgrSteps as $phase) {
            \App\Models\ProgramSubStep::where("step_label", "LIKE", "%" .$phase["step_label"]."%")
                ->update(['labelEng' => $phase["labelEng"]]);
        }*/

        foreach ($this->pgrSteps as $pgrStep) {
            \App\Models\ProgramSubStep::create(
                $pgrStep
            );
        }
    }
}
