<?php

use Illuminate\Database\Seeder;

class SubStepStateSeeder extends Seeder
{
    private $states = [
        // [
        //     "name_fr" => "E-mail 1 : candidature",
        //     "name_en" => "E-mail 1 : candidature",
		// 	"visible_for_conseiller" => 1,
		// 	"visible_for_agent" => 1,
        //     // 'style_class' => 'success',
        //     "steps" => [
        //         [
        //             2 => ['program_id' => 1, "order" => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 2, "order" => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 3, "order" => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 4, "order" => 1],
        //         ],
        //     ]
        // ],
        [
            "name_fr" => "Appel : relance modèle 1",
            "name_en" => "Call: relaunch model 1",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 1,
            "visible_for_backoffice" => 0,
            // 'style_class' => 'warning',
            "steps" => [
                [
                    2 => ['program_id' => 1, "order" => 2],
                ],
                [
                    2 => ['program_id' => 2, "order" => 2],
                ],
                [
                    2 => ['program_id' => 3, "order" => 2],
                ],
                [
                    2 => ['program_id' => 4, "order" => 2],
                ],
                [
                    2 => ['program_id' => 5, "order" => 2],
                ],
                [
                    2 => ['program_id' => 6, "order" => 2],
                ],
                [
                    2 => ['program_id' => 7, "order" => 2],
                ],
                [
                    2 => ['program_id' => 8, "order" => 2],
                ],
                [
                    2 => ['program_id' => 9, "order" => 2],
                ],
                [
                    2 => ['program_id' => 10, "order" => 2],
                ],
                [
                    2 => ['program_id' => 11, "order" => 2],
                ],
                [
                    2 => ['program_id' => 12, "order" => 2],
                ],
                [
                    2 => ['program_id' => 13, "order" => 2],
                ],
                [
                    2 => ['program_id' => 14, "order" => 2],
                ],
                [
                    2 => ['program_id' => 15, "order" => 2],
                ],
                [
                    2 => ['program_id' => 16, "order" => 2],
                ],
                [
                    2 => ['program_id' => 17, "order" => 2],
                ],
                [
                    2 => ['program_id' => 18, "order" => 2],
                ],
                [
                    2 => ['program_id' => 19, "order" => 2],
                ],
                [
                    2 => ['program_id' => 20, "order" => 2],
                ],
                [
                    2 => ['program_id' => 21, "order" => 2],
                ],
                [
                    2 => ['program_id' => 22, "order" => 2],
                ],
                [
                    2 => ['program_id' => 23, "order" => 2],
                ],

            ]
        ],
        [
            "name_fr" => "Message whatsapp",
            "name_en" => "Whatsapp message",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 1,
            "visible_for_backoffice" => 0,
            // 'style_class' => 'warning',
            "steps" => [
                [
                    2 => ['program_id' => 1, "order" => 3],
                ],
                [
                    2 => ['program_id' => 2, "order" => 3],
                ],
                [
                    2 => ['program_id' => 3, "order" => 3],
                ],
                [
                    2 => ['program_id' => 4, "order" => 3],
                ],
                [
                    2 => ['program_id' => 5, "order" => 3],
                ],
                [
                    2 => ['program_id' => 6, "order" => 3],
                ],
                [
                    2 => ['program_id' => 7, "order" => 3],
                ],
                [
                    2 => ['program_id' => 8, "order" => 3],
                ],
                [
                    2 => ['program_id' => 9, "order" => 3],
                ],
                [
                    2 => ['program_id' => 10, "order" => 3],
                ],
                [
                    2 => ['program_id' => 11, "order" => 3],
                ],
                [
                    2 => ['program_id' => 12, "order" => 3],
                ],
                [
                    2 => ['program_id' => 13, "order" => 3],
                ],
                [
                    2 => ['program_id' => 14, "order" => 3],
                ],
                [
                    2 => ['program_id' => 15, "order" => 3],
                ],
                [
                    2 => ['program_id' => 16, "order" => 3],
                ],
                [
                    2 => ['program_id' => 17, "order" => 3],
                ],
                [
                    2 => ['program_id' => 18, "order" => 3],
                ],
                [
                    2 => ['program_id' => 19, "order" => 3],
                ],
                [
                    2 => ['program_id' => 20, "order" => 3],
                ],
                [
                    2 => ['program_id' => 21, "order" => 3],
                ],
                [
                    2 => ['program_id' => 22, "order" => 3],
                ],
                [
                    2 => ['program_id' => 23, "order" => 3],
                ],

            ]
        ],

        [
            "name_fr" => "Paiement 1: 200DH",
            "name_en" => "Payment 1: 200DH",
			"visible_for_conseiller" => 0,
			"visible_for_agent" => 1,
            "visible_for_backoffice" => 0,
            "steps" => [
                [
                    7 => ['program_id' => 1, "order" => 1],
                ],
                [
                    7 => ['program_id' => 2, "order" => 1],
                ],
                [
                    7 => ['program_id' => 3, "order" => 1],
                ],
                [
                    7 => ['program_id' => 4, "order" => 1],
                ],
                [
                    7 => ['program_id' => 5, "order" => 1],
                ],
                [
                    7 => ['program_id' => 6, "order" => 1],
                ],
                [
                    7 => ['program_id' => 7, "order" => 1],
                ],
                [
                    7 => ['program_id' => 8, "order" => 1],
                ],
                [
                    7 => ['program_id' => 9, "order" => 1],
                ],
                [
                    7 => ['program_id' => 10, "order" => 1],
                ],
                [
                    7 => ['program_id' => 11, "order" => 1],
                ],
                [
                    7 => ['program_id' => 12, "order" => 1],
                ],
                [
                    7 => ['program_id' => 13, "order" => 1],
                ],
                [
                    7 => ['program_id' => 14, "order" => 1],
                ],
                [
                    7 => ['program_id' => 15, "order" => 1],
                ],
                [
                    7 => ['program_id' => 16, "order" => 1],
                ],
                [
                    7 => ['program_id' => 17, "order" => 1],
                ],
                [
                    7 => ['program_id' => 18, "order" => 1],
                ],
                [
                    7 => ['program_id' => 19, "order" => 1],
                ],
                [
                    7 => ['program_id' => 20, "order" => 1],
                ],
                [
                    7 => ['program_id' => 21, "order" => 1],
                ],
                [
                    7 => ['program_id' => 22, "order" => 1],
                ],
                [
                    7 => ['program_id' => 23, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Reçu",
            "name_en" => "Invoice",
			"visible_for_conseiller" => 0,
			"visible_for_agent" => 1,
            "visible_for_backoffice" => 0,
            "steps" => [
                // [
                //     7 => ['program_id' => 1, "order" => 2],
                // ],
                // [
                //     7 => ['program_id' => 2, "order" => 2],
                // ],
                // [
                //     7 => ['program_id' => 3, "order" => 2],
                // ],
                // [
                //     7 => ['program_id' => 4, "order" => 2],
                // ],
                [
                    8 => ['program_id' => 1, "order" => 2],
                ],
                [
                    8 => ['program_id' => 2, "order" => 2],
                ],
                [
                    8 => ['program_id' => 3, "order" => 2],
                ],
                [
                    8 => ['program_id' => 4, "order" => 2],
                ],
                [
                     8=> ['program_id' => 5, "order" => 2],
                ],
                [
                     8=> ['program_id' => 6, "order" => 2],
                ],
                [
                    8 => ['program_id' => 7, "order" => 2],
                ],
                [
                    8 => ['program_id' => 8, "order" => 2],
                ],
                [
                    8 => ['program_id' => 9, "order" => 2],
                ],
                [
                    8 => ['program_id' => 10, "order" => 2],
                ],
                [
                    8 => ['program_id' => 11, "order" => 2],
                ],
                [
                    8 => ['program_id' => 12, "order" => 2],
                ],
                [
                    8 => ['program_id' => 13, "order" => 2],
                ],
                [
                    8 => ['program_id' => 14, "order" => 2],
                ],
                [
                    8 => ['program_id' => 15, "order" => 2],
                ],
                [
                    8 => ['program_id' => 16, "order" => 2],
                ],
                [
                    8 => ['program_id' => 17, "order" => 2],
                ],
                [
                    8 => ['program_id' => 18, "order" => 2],
                ],
                [
                    8 => ['program_id' => 19, "order" => 2],
                ],
                [
                    8 => ['program_id' => 20, "order" => 2],
                ],
                [
                    8 => ['program_id' => 21, "order" => 2],
                ],
                [
                    8 => ['program_id' => 22, "order" => 2],
                ],
                [
                    8 => ['program_id' => 23, "order" => 2],
                ],
                [
                    10 => ['program_id' => 1, "order" => 2],
                ],
                [
                    10 => ['program_id' => 2, "order" => 2],
                ],
                [
                    10 => ['program_id' => 3, "order" => 2],
                ],
                [
                    10 => ['program_id' => 4, "order" => 2],
                ],
                [
                    10=> ['program_id' => 5, "order" => 2],
               ],
               [
                    10=> ['program_id' => 6, "order" => 2],
               ],
               [
                    10 => ['program_id' => 7, "order" => 2],
               ],
               [
                     10 => ['program_id' => 8, "order" => 2],
               ],
               [
                    10 => ['program_id' => 9, "order" => 2],
               ],
               [
                    10 => ['program_id' => 10, "order" => 2],
               ],
               [
                    10 => ['program_id' => 11, "order" => 2],
               ],
               [
                    10 => ['program_id' => 12, "order" => 2],
               ],
               [
                    10 => ['program_id' => 13, "order" => 2],
               ],
               [
                    10 => ['program_id' => 14, "order" => 2],
               ],
               [
                   10 => ['program_id' => 15, "order" => 2],
               ],
               [
                   10 => ['program_id' => 16, "order" => 2],
               ],
               [
                   10 => ['program_id' => 17, "order" => 2],
               ],
               [
                   10 => ['program_id' => 18, "order" => 2],
               ],
               [
                   10 => ['program_id' => 19, "order" => 2],
               ],
               [
                   10 => ['program_id' => 20, "order" => 2],
               ],
               [
                   10 => ['program_id' => 21, "order" => 2],
               ],
               [
                   10 => ['program_id' => 22, "order" => 2],
               ],
               [
                   10 => ['program_id' => 23, "order" => 2],
               ],
                [
                    19 => ['program_id' => 1, "order" => 2],
                ],
                [
                    19 => ['program_id' => 2, "order" => 2],
                ],
                [
                    19 => ['program_id' => 3, "order" => 2],
                ],
                [
                    19 => ['program_id' => 4, "order" => 2],
                ],
                [
                    19=> ['program_id' => 5, "order" => 2],
               ],
               [
                    19=> ['program_id' => 6, "order" => 2],
               ],
               [
                    19 => ['program_id' => 7, "order" => 2],
               ],
               [
                     19 => ['program_id' => 8, "order" => 2],
               ],
               [
                    19 => ['program_id' => 9, "order" => 2],
               ],
               [
                    19 => ['program_id' => 10, "order" => 2],
               ],
               [
                    19 => ['program_id' => 11, "order" => 2],
               ],
               [
                    19 => ['program_id' => 12, "order" => 2],
               ],
               [
                    19 => ['program_id' => 13, "order" => 2],
               ],
               [
                    19 => ['program_id' => 14, "order" => 2],
               ],
               [
                   19 => ['program_id' => 15, "order" => 2],
               ],
               [
                   19 => ['program_id' => 16, "order" => 2],
               ],
               [
                   19 => ['program_id' => 17, "order" => 2],
               ],
               [
                   19 => ['program_id' => 18, "order" => 2],
               ],
               [
                   19 => ['program_id' => 19, "order" => 2],
               ],
               [
                   19 => ['program_id' => 20, "order" => 2],
               ],
               [
                   19 => ['program_id' => 21, "order" => 2],
               ],
               [
                   19 => ['program_id' => 22, "order" => 2],
               ],
               [
                   19 => ['program_id' => 23, "order" => 2],
               ],
                [
                    26 => ['program_id' => 1, "order" => 2],
                ],
                [
                    26 => ['program_id' => 2, "order" => 2],
                ],
                [
                    26 => ['program_id' => 3, "order" => 2],
                ],
                [
                    26 => ['program_id' => 4, "order" => 2],
                ],
                [
                    26=> ['program_id' => 5, "order" => 2],
               ],
               [
                    26=> ['program_id' => 6, "order" => 2],
               ],
               [
                    26 => ['program_id' => 7, "order" => 2],
               ],
               [
                    26 => ['program_id' => 8, "order" => 2],
               ],
               [
                    26 => ['program_id' => 9, "order" => 2],
               ],
               [
                    26 => ['program_id' => 10, "order" => 2],
               ],
               [
                    26 => ['program_id' => 11, "order" => 2],
               ],
               [
                    26 => ['program_id' => 12, "order" => 2],
               ],
               [
                    26 => ['program_id' => 13, "order" => 2],
               ],
               [
                    26 => ['program_id' => 14, "order" => 2],
               ],
               [
                   26 => ['program_id' => 15, "order" => 2],
               ],
               [
                   26 => ['program_id' => 16, "order" => 2],
               ],
               [
                   26 => ['program_id' => 17, "order" => 2],
               ],
               [
                   26 => ['program_id' => 18, "order" => 2],
               ],
               [
                   26 => ['program_id' => 19, "order" => 2],
               ],
               [
                   26 => ['program_id' => 20, "order" => 2],
               ],
               [
                   26 => ['program_id' => 21, "order" => 2],
               ],
               [
                   26 => ['program_id' => 22, "order" => 2],
               ],
               [
                   26 => ['program_id' => 23, "order" => 2],
               ],
            ]
        ],
        [
            "name_fr" => "Paiement 2: 2300DH",
            "name_en" => "Payment 2: 2300DH",
			"visible_for_conseiller" => 0,
			"visible_for_agent" => 1,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    8 => ['program_id' => 1, "order" => 1],
                ],
                [
                    8 => ['program_id' => 2, "order" => 1],
                ],
                [
                    8 => ['program_id' => 3, "order" => 1],
                ],
                [
                    8 => ['program_id' => 4, "order" => 1],
                ],
                [
                    8 => ['program_id' => 5, "order" => 1],
                ],
                [
                    8 => ['program_id' => 6, "order" => 1],
                ],
                [
                    8 => ['program_id' => 7, "order" => 1],
                ],
                [
                    8 => ['program_id' => 8, "order" => 1],
                ],
                [
                    8 => ['program_id' => 9, "order" => 1],
                ],
                [
                    8 => ['program_id' => 10, "order" => 1],
                ],
                [
                    8 => ['program_id' => 11, "order" => 1],
                ],
                [
                    8 => ['program_id' => 12, "order" => 1],
                ],
                [
                    8 => ['program_id' => 13, "order" => 1],
                ],
                [
                    8 => ['program_id' => 14, "order" => 1],
                ],
                [
                    8 => ['program_id' => 15, "order" => 1],
                ],
                [
                    8 => ['program_id' => 16, "order" => 1],
                ],
                [
                    8 => ['program_id' => 17, "order" => 1],
                ],
                [
                    8 => ['program_id' => 18, "order" => 1],
                ],
                [
                    8 => ['program_id' => 19, "order" => 1],
                ],
                [
                    8 => ['program_id' => 20, "order" => 1],
                ],
                [
                    8 => ['program_id' => 21, "order" => 1],
                ],
                [
                    8 => ['program_id' => 22, "order" => 1],
                ],
                [
                    8 => ['program_id' => 23, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Clear1002 – Orientation",
            "name_en" => "Clear1002 - Orientation",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 4, "order" => 4],
                ],
            ]
        ],
        [
            "name_fr" => "Clear1 - Documents de l'étudiant",
            "name_en" => "Clear1 - Student documents",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 4, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Clear2 - Documents des garants",
            "name_en" => "Clear2 - Documents of guarantors",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 4, "order" => 2],
                ],
            ]
        ],
        [
            "name_fr" => "Clear3 - Fiche de renseignements à remplir",
            "name_en" => "Clear3 - Information sheet",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 3, "order" => 3],
                ],
                [
                    9 => ['program_id' => 4, "order" => 3],
                ],
            ]
        ],
        [
            "name_fr" => "Paiement 3: 5800DH",
            "name_en" => "Payment 3: 5800DH",
			"visible_for_conseiller" => 0,
			"visible_for_agent" => 1,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    10 => ['program_id' => 1, "order" => 1],
                ],
                [
                    10 => ['program_id' => 2, "order" => 1],
                ],
                [
                    10 => ['program_id' => 3, "order" => 1],
                ],
                [
                    10 => ['program_id' => 4, "order" => 1],
                ],
                [
                    10=> ['program_id' => 5, "order" => 1],
               ],
               [
                    10=> ['program_id' => 6, "order" => 1],
               ],
               [
                    10 => ['program_id' => 7, "order" => 1],
               ],
               [
                     10 => ['program_id' => 8, "order" => 1],
               ],
               [
                    10 => ['program_id' => 9, "order" => 1],
               ],
               [
                    10 => ['program_id' => 10, "order" => 1],
               ],
               [
                    10 => ['program_id' => 11, "order" => 1],
               ],
               [
                    10 => ['program_id' => 12, "order" => 1],
               ],
               [
                    10 => ['program_id' => 13, "order" => 1],
               ],
               [
                    10 => ['program_id' => 14, "order" => 1],
               ],
               [
                   10 => ['program_id' => 15, "order" => 1],
               ],
               [
                   10 => ['program_id' => 16, "order" => 1],
               ],
               [
                   10 => ['program_id' => 17, "order" => 1],
               ],
               [
                   10 => ['program_id' => 18, "order" => 1],
               ],
               [
                   10 => ['program_id' => 19, "order" => 1],
               ],
               [
                   10 => ['program_id' => 20, "order" => 1],
               ],
               [
                   10 => ['program_id' => 21, "order" => 1],
               ],
               [
                   10 => ['program_id' => 22, "order" => 1],
               ],
               [
                   10 => ['program_id' => 23, "order" => 1],
               ],
            ]
        ],
        [
            "name_fr" => "Notification du service admin",
            "name_en" => "Admin service notification",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    15 => ['program_id' => 1, "order" => 1],
                ],
                [
                    15 => ['program_id' => 2, "order" => 1],
                ],
                [
                    15 => ['program_id' => 3, "order" => 1],
                ],
                [
                    15 => ['program_id' => 4, "order" => 1],
                ],
                [
                    15 => ['program_id' => 5, "order" => 1],
                ],
                [
                    15 => ['program_id' => 6, "order" => 1],
                ],
                [
                    15 => ['program_id' => 7, "order" => 1],
                ],
                [
                    15 => ['program_id' => 8, "order" => 1],
                ],
                [
                    15 => ['program_id' => 9, "order" => 1],
                ],
                [
                    15 => ['program_id' => 10, "order" => 1],
                ],
                [
                    15 => ['program_id' => 11, "order" => 1],
                ],
                [
                    15 => ['program_id' => 12, "order" => 1],
                ],
                [
                    15 => ['program_id' => 13, "order" => 1],
                ],
                [
                    15 => ['program_id' => 14, "order" => 1],
                ],
                [
                    15 => ['program_id' => 15, "order" => 1],
                ],
                [
                    15 => ['program_id' => 16, "order" => 1],
                ],
                [
                    15 => ['program_id' => 17, "order" => 1],
                ],
                [
                    15 => ['program_id' => 18, "order" => 1],
                ],
                [
                    15 => ['program_id' => 19, "order" => 1],
                ],
                [
                    15 => ['program_id' => 20, "order" => 1],
                ],
                [
                    15 => ['program_id' => 21, "order" => 1],
                ],
                [
                    15 => ['program_id' => 22, "order" => 1],
                ],
                [
                    15 => ['program_id' => 23, "order" => 1],
                ],
                [
                    28 => ['program_id' => 1, "order" => 1],
                ],
                [
                    28 => ['program_id' => 2, "order" => 1],
                ],
                [
                    28 => ['program_id' => 3, "order" => 1],
                ],
                [
                    28 => ['program_id' => 4, "order" => 1],
                ],
                [
                    28 => ['program_id' => 5, "order" => 1],
                ],
                [
                    28 => ['program_id' => 6, "order" => 1],
                ],
                [
                    28 => ['program_id' => 7, "order" => 1],
                ],
                [
                    28 => ['program_id' => 8, "order" => 1],
                ],
                [
                    28 => ['program_id' => 9, "order" => 1],
                ],
                [
                    28 => ['program_id' => 10, "order" => 1],
                ],
                [
                    28 => ['program_id' => 11, "order" => 1],
                ],
                [
                    28 => ['program_id' => 12, "order" => 1],
                ],
                [
                    28 => ['program_id' => 13, "order" => 1],
                ],
                [
                    28 => ['program_id' => 14, "order" => 1],
                ],
                [
                    28 => ['program_id' => 15, "order" => 1],
                ],
                [
                    28 => ['program_id' => 16, "order" => 1],
                ],
                [
                    28 => ['program_id' => 17, "order" => 1],
                ],
                [
                    28 => ['program_id' => 18, "order" => 1],
                ],
                [
                    28 => ['program_id' => 19, "order" => 1],
                ],
                [
                    28 => ['program_id' => 20, "order" => 1],
                ],
                [
                    28 => ['program_id' => 21, "order" => 1],
                ],
                [
                    28 => ['program_id' => 22, "order" => 1],
                ],
                [
                    28 => ['program_id' => 23, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Créer un e-mail du client sur webmail",
            "name_en" => "Créer un e-mail du client sur webmail",
			"visible_for_conseiller" => 0,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 1,
            "steps" => [
                [
                    15 => ['program_id' => 1, "order" => 2],
                ],
                [
                    15 => ['program_id' => 2, "order" => 2],
                ],
                [
                    15 => ['program_id' => 3, "order" => 2],
                ],
                [
                    15 => ['program_id' => 4, "order" => 2],
                ],
                [
                    15 => ['program_id' => 5, "order" => 2],
                ],
                [
                    15 => ['program_id' => 6, "order" => 2],
                ],
                [
                    15 => ['program_id' => 7, "order" => 2],
                ],
                [
                    15 => ['program_id' => 8, "order" => 2],
                ],
                [
                    15 => ['program_id' => 9, "order" => 2],
                ],
                [
                    15 => ['program_id' => 10, "order" => 2],
                ],
                [
                    15 => ['program_id' => 11, "order" => 2],
                ],
                [
                    15 => ['program_id' => 12, "order" => 2],
                ],
                [
                    15 => ['program_id' => 13, "order" => 2],
                ],
                [
                    15 => ['program_id' => 14, "order" => 2],
                ],
                [
                    15 => ['program_id' => 15, "order" => 2],
                ],
                [
                    15 => ['program_id' => 16, "order" => 2],
                ],
                [
                    15 => ['program_id' => 17, "order" => 2],
                ],
                [
                    15 => ['program_id' => 18, "order" => 2],
                ],
                [
                    15 => ['program_id' => 19, "order" => 2],
                ],
                [
                    15 => ['program_id' => 20, "order" => 2],
                ],
                [
                    15 => ['program_id' => 21, "order" => 2],
                ],
                [
                    15 => ['program_id' => 22, "order" => 2],
                ],
                [
                    15 => ['program_id' => 23, "order" => 2],
                ],
            ]
        ],
        [
            "name_fr" => "Notification du service conseiller ",
            "name_en" => "Notification du service conseiller ",
			"visible_for_conseiller" => 0,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 1,
            "steps" => [
                [
                    15 => ['program_id' => 1, "order" => 3],
                ],
                [
                    15 => ['program_id' => 2, "order" => 3],
                ],
                [
                    15 => ['program_id' => 3, "order" => 3],
                ],
                [
                    15 => ['program_id' => 4, "order" => 3],
                ],
                [
                    15 => ['program_id' => 5, "order" => 3],
                ],
                [
                    15 => ['program_id' => 6, "order" =>3],
                ],
                [
                    15 => ['program_id' => 7, "order" => 3],
                ],
                [
                    15 => ['program_id' => 8, "order" => 3],
                ],
                [
                    15 => ['program_id' => 9, "order" => 3],
                ],
                [
                    15 => ['program_id' => 10, "order" => 3],
                ],
                [
                    15 => ['program_id' => 11, "order" => 3],
                ],
                [
                    15 => ['program_id' => 12, "order" => 3],
                ],
                [
                    15 => ['program_id' => 13, "order" => 3],
                ],
                [
                    15 => ['program_id' => 14, "order" => 3],
                ],
                [
                    15 => ['program_id' => 15, "order" => 3],
                ],
                [
                    15 => ['program_id' => 16, "order" => 3],
                ],
                [
                    15 => ['program_id' => 17, "order" => 3],
                ],
                [
                    15 => ['program_id' => 18, "order" => 3],
                ],
                [
                    15 => ['program_id' => 19, "order" => 3],
                ],
                [
                    15 => ['program_id' => 20, "order" => 3],
                ],
                [
                    15 => ['program_id' => 21, "order" => 3],
                ],
                [
                    15 => ['program_id' => 22, "order" => 3],
                ],
                [
                    15 => ['program_id' => 23, "order" => 3],
                ],
                [
                    28 => ['program_id' => 1, "order" => 1],
                ],
                [
                    28 => ['program_id' => 2, "order" => 1],
                ],
                [
                    28 => ['program_id' => 3, "order" => 1],
                ],
                [
                    28 => ['program_id' => 4, "order" => 1],
                ],
                [
                    28 => ['program_id' => 5, "order" => 1],
                ],
                [
                    28 => ['program_id' => 6, "order" => 1],
                ],
                [
                    28 => ['program_id' => 7, "order" => 1],
                ],
                [
                    28 => ['program_id' => 8, "order" => 1],
                ],
                [
                    28 => ['program_id' => 9, "order" => 1],
                ],
                [
                    28 => ['program_id' => 10, "order" => 1],
                ],
                [
                    28 => ['program_id' => 11, "order" => 1],
                ],
                [
                    28 => ['program_id' => 12, "order" => 1],
                ],
                [
                    28 => ['program_id' => 13, "order" => 1],
                ],
                [
                    28 => ['program_id' => 14, "order" => 1],
                ],
                [
                    28 => ['program_id' => 15, "order" => 1],
                ],
                [
                    28 => ['program_id' => 16, "order" => 1],
                ],
                [
                    28 => ['program_id' => 17, "order" => 1],
                ],
                [
                    28 => ['program_id' => 18, "order" => 1],
                ],
                [
                    28 => ['program_id' => 19, "order" => 1],
                ],
                [
                    28 => ['program_id' => 20, "order" => 1],
                ],
                [
                    28 => ['program_id' => 21, "order" => 1],
                ],
                [
                    28 => ['program_id' => 22, "order" => 1],
                ],
                [
                    28 => ['program_id' => 23, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Soumettre les demandes d'admission",
            "name_en" => "Soumettre les demandes d'admission",
			"visible_for_conseiller" => 0,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 1,
            "steps" => [
                [
                    15 => ['program_id' => 1, "order" => 4],
                ],
                [
                    15 => ['program_id' => 2, "order" => 4],
                ],
                [
                    15 => ['program_id' => 3, "order" => 4],
                ],
                [
                    15 => ['program_id' => 4, "order" => 4],
                ],
                [
                    15 => ['program_id' => 5, "order" => 4],
                ],
                [
                    15 => ['program_id' => 6, "order" => 4],
                ],
                [
                    15 => ['program_id' => 7, "order" => 4],
                ],
                [
                    15 => ['program_id' => 8, "order" => 4],
                ],
                [
                    15 => ['program_id' => 9, "order" => 4],
                ],
                [
                    15 => ['program_id' => 10, "order" => 4],
                ],
                [
                    15 => ['program_id' => 11, "order" => 4],
                ],
                [
                    15 => ['program_id' => 12, "order" => 4],
                ],
                [
                    15 => ['program_id' => 13, "order" => 4],
                ],
                [
                    15 => ['program_id' => 14, "order" => 4],
                ],
                [
                    15 => ['program_id' => 15, "order" => 4],
                ],
                [
                    15 => ['program_id' => 16, "order" => 4],
                ],
                [
                    15 => ['program_id' => 17, "order" => 4],
                ],
                [
                    15 => ['program_id' => 18, "order" => 4],
                ],
                [
                    15 => ['program_id' => 19, "order" => 4],
                ],
                [
                    15 => ['program_id' => 20, "order" => 4],
                ],
                [
                    15 => ['program_id' => 21, "order" => 4],
                ],
                [
                    15 => ['program_id' => 22, "order" => 4],
                ],
                [
                    15 => ['program_id' => 23, "order" => 4],
                ],
            ]
        ],
        [
            "name_fr" => "Transmettre les logins des sessions d'admission au conseiller ",
            "name_en" => "Send admission sessions logins to the advisor",
			"visible_for_conseiller" => 0,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 1,
            "steps" => [
                [
                    15 => ['program_id' => 1, "order" => 5],
                ],
                [
                    15 => ['program_id' => 2, "order" => 5],
                ],
                [
                    15 => ['program_id' => 3, "order" => 5],
                ],
                [
                    15 => ['program_id' => 4, "order" => 5],
                ],
                [
                    15 => ['program_id' => 5, "order" =>5],
                ],
                [
                    15 => ['program_id' => 6, "order" =>5],
                ],
                [
                    15 => ['program_id' => 7, "order" =>5],
                ],
                [
                    15 => ['program_id' => 8, "order" =>5],
                ],
                [
                    15 => ['program_id' => 9, "order" =>5],
                ],
                [
                    15 => ['program_id' => 10, "order" =>5],
                ],
                [
                    15 => ['program_id' => 11, "order" =>5],
                ],
                [
                    15 => ['program_id' => 12, "order" =>5],
                ],
                [
                    15 => ['program_id' => 13, "order" =>5],
                ],
                [
                    15 => ['program_id' => 14, "order" =>5],
                ],
                [
                    15 => ['program_id' => 15, "order" =>5],
                ],
                [
                    15 => ['program_id' => 16, "order" =>5],
                ],
                [
                    15 => ['program_id' => 17, "order" =>5],
                ],
                [
                    15 => ['program_id' => 18, "order" =>5],
                ],
                [
                    15 => ['program_id' => 19, "order" =>5],
                ],
                [
                    15 => ['program_id' => 20, "order" =>5],
                ],
                [
                    15 => ['program_id' => 21, "order" =>5],
                ],
                [
                    15 => ['program_id' => 22, "order" =>5],
                ],
                [
                    15 => ['program_id' => 23, "order" =>5],
                ],
            ]
        ],
        [
            "name_fr" => "Confirmation de la soumission des admissions auprès du client",
            "name_en" => "Confirmation of admission submissions to the customer",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    15 => ['program_id' => 1, "order" => 6],
                ],
                [
                    15 => ['program_id' => 2, "order" => 6],
                ],
                [
                    15 => ['program_id' => 3, "order" => 6],
                ],
                [
                    15 => ['program_id' => 4, "order" => 6],
                ],
                [
                    15 => ['program_id' => 5, "order" => 6],
                ],
                [
                    15 => ['program_id' => 6, "order" => 6],
                ],
                [
                    15 => ['program_id' => 7, "order" => 6],
                ],
                [
                    15 => ['program_id' => 8, "order" => 6],
                ],
                [
                    15 => ['program_id' => 9, "order" => 6],
                ],
                [
                    15 => ['program_id' => 10, "order" => 6],
                ],
                [
                    15 => ['program_id' => 11, "order" => 6],
                ],
                [
                    15 => ['program_id' => 12, "order" => 6],
                ],
                [
                    15 => ['program_id' => 13, "order" => 6],
                ],
                [
                    15 => ['program_id' => 14, "order" => 6],
                ],
                [
                    15 => ['program_id' => 15, "order" => 6],
                ],
                [
                    15 => ['program_id' => 16, "order" => 6],
                ],
                [
                    15 => ['program_id' => 17, "order" => 6],
                ],
                [
                    15 => ['program_id' => 18, "order" => 6],
                ],
                [
                    15 => ['program_id' => 19, "order" => 6],
                ],
                [
                    15 => ['program_id' => 20, "order" => 6],
                ],
                [
                    15 => ['program_id' => 21, "order" => 6],
                ],
                [
                    15 => ['program_id' => 22, "order" => 6],
                ],
                [
                    15 => ['program_id' => 23, "order" => 6],
                ],
            ]
        ],
        [
            "name_fr" => "Compléments des demandes d'admission",
            "name_en" => " Admission request complements",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    16 => ['program_id' => 1, "order" => 2],
                ],
                [
                    16 => ['program_id' => 2, "order" => 2],
                ],
                [
                    16 => ['program_id' => 3, "order" => 2],
                ],
                [
                    16 => ['program_id' => 4, "order" => 2],
                ],
                [
                    16 => ['program_id' => 5, "order" => 2],
                ],
                [
                    16 => ['program_id' => 6, "order" => 2],
                ],
                [
                    16 => ['program_id' => 7, "order" => 2],
                ],
                [
                    16 => ['program_id' => 8, "order" => 2],
                ],
                [
                    16 => ['program_id' => 9, "order" => 2],
                ],
                [
                    16 => ['program_id' => 10, "order" => 2],
                ],
                [
                    16 => ['program_id' => 11, "order" => 2],
                ],
                [
                    16 => ['program_id' => 12, "order" => 2],
                ],
                [
                    16 => ['program_id' => 13, "order" => 2],
                ],
                [
                    16 => ['program_id' => 14, "order" => 2],
                ],
                [
                    16 => ['program_id' => 15, "order" => 2],
                ],
                [
                    16 => ['program_id' => 16, "order" => 2],
                ],
                [
                    16 => ['program_id' => 17, "order" => 2],
                ],
                [
                    16 => ['program_id' => 18, "order" => 2],
                ],
                [
                    16 => ['program_id' => 19, "order" => 2],
                ],
                [
                    16 => ['program_id' => 20, "order" => 2],
                ],
                [
                    16 => ['program_id' => 21, "order" => 2],
                ],
                [
                    16 => ['program_id' => 22, "order" => 2],
                ],
                [
                    16 => ['program_id' => 23, "order" => 2],
                ],
            ]
        ],
        [
            "name_fr" => "Suivi des admission",
            "name_en" => "Admission monitoring",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    16 => ['program_id' => 1, "order" => 1],
                ],
                [
                    16 => ['program_id' => 2, "order" => 1],
                ],
                [
                    16 => ['program_id' => 3, "order" => 1],
                ],
                [
                    16 => ['program_id' => 4, "order" => 1],
                ],
                [
                    16 => ['program_id' => 5, "order" => 1],
                ],
                [
                    16 => ['program_id' => 6, "order" => 1],
                ],
                [
                    16 => ['program_id' => 7, "order" => 1],
                ],
                [
                    16 => ['program_id' => 8, "order" => 1],
                ],
                [
                    16 => ['program_id' => 9, "order" => 1],
                ],
                [
                    16 => ['program_id' => 10, "order" =>1],
                ],
                [
                    16 => ['program_id' => 11, "order" => 1],
                ],
                [
                    16 => ['program_id' => 12, "order" => 1],
                ],
                [
                    16 => ['program_id' => 13, "order" => 1],
                ],
                [
                    16 => ['program_id' => 14, "order" => 1],
                ],
                [
                    16 => ['program_id' => 15, "order" => 1],
                ],
                [
                    16 => ['program_id' => 16, "order" => 1],
                ],
                [
                    16 => ['program_id' => 17, "order" => 1],
                ],
                [
                    16 => ['program_id' => 18, "order" => 1],
                ],
                [
                    16 => ['program_id' => 19, "order" => 1],
                ],
                [
                    16 => ['program_id' => 20, "order" => 1],
                ],
                [
                    16 => ['program_id' => 21, "order" => 1],
                ],
                [
                    16 => ['program_id' => 22, "order" => 1],
                ],
                [
                    16 => ['program_id' => 23, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Inscription et choix de cours",
            "name_en" => "Registration and course selection",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    18 => ['program_id' => 1, "order" => 1],
                ],
                [
                    18 => ['program_id' => 2, "order" => 1],
                ],
                [
                    18 => ['program_id' => 3, "order" => 1],
                ],
                [
                    18 => ['program_id' => 4, "order" => 1],
                ],
                [
                    18 => ['program_id' => 5, "order" => 1],
                ],
                [
                    18 => ['program_id' => 6, "order" => 1],
                ],
                [
                    18 => ['program_id' => 7, "order" => 1],
                ],
                [
                    18 => ['program_id' => 8, "order" => 1],
                ],
                [
                    18 => ['program_id' => 9, "order" => 1],
                ],
                [
                    18 => ['program_id' => 10, "order" =>1],
                ],
                [
                    18 => ['program_id' => 11, "order" => 1],
                ],
                [
                    18 => ['program_id' => 12, "order" => 1],
                ],
                [
                    18 => ['program_id' => 13, "order" => 1],
                ],
                [
                    18 => ['program_id' => 14, "order" => 1],
                ],
                [
                    18 => ['program_id' => 15, "order" => 1],
                ],
                [
                    18 => ['program_id' => 16, "order" => 1],
                ],
                [
                    18 => ['program_id' => 17, "order" => 1],
                ],
                [
                    18 => ['program_id' => 18, "order" => 1],
                ],
                [
                    18 => ['program_id' => 19, "order" => 1],
                ],
                [
                    18 => ['program_id' => 20, "order" => 1],
                ],
                [
                    18 => ['program_id' => 21, "order" => 1],
                ],
                [
                    18 => ['program_id' => 22, "order" => 1],
                ],
                [
                    18 => ['program_id' => 23, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Paiement 4: 5000DH",
            "name_en" => "Paiement 4: 5000DH",
			"visible_for_conseiller" => 0,
			"visible_for_agent" => 1,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    19 => ['program_id' => 1, "order" => 1],
                ],
                [
                    19 => ['program_id' => 2, "order" => 1],
                ],
                [
                    19 => ['program_id' => 3, "order" => 1],
                ],
                [
                    19 => ['program_id' => 4, "order" => 1],
                ],
                [
                   19 => ['program_id' => 5, "order" => 1],
                ],
                [
                   19 => ['program_id' => 6, "order" => 1],
                ],
                [
                   19 => ['program_id' => 7, "order" => 1],
                ],
                [
                   19 => ['program_id' => 8, "order" => 1],
                ],
                [
                   19 => ['program_id' => 9, "order" => 1],
                ],
                [
                   19 => ['program_id' => 10, "order" =>1],
                ],
                [
                   19 => ['program_id' => 11, "order" => 1],
                ],
                [
                   19 => ['program_id' => 12, "order" => 1],
                ],
                [
                   19 => ['program_id' => 13, "order" => 1],
                ],
                [
                   19 => ['program_id' => 14, "order" => 1],
                ],
                [
                   19 => ['program_id' => 15, "order" => 1],
                ],
                [
                   19 => ['program_id' =>16, "order" => 1],
                ],
                [
                   19 => ['program_id' => 17, "order" => 1],
                ],
                [
                   19 => ['program_id' => 18, "order" => 1],
                ],
                [
                   19 => ['program_id' => 19, "order" => 1],
                ],
                [
                   19 => ['program_id' => 20, "order" => 1],
                ],
                [
                   19 => ['program_id' => 21, "order" => 1],
                ],
                [
                   19 => ['program_id' => 22, "order" => 1],
                ],
                [
                   19 => ['program_id' => 23, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Clear4 - Documents à fournir pour la demande de CAQ",
            "name_en" => "Clear4 - Documents to provide for the CAQ request",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    20 => ['program_id' => 1, "order" => 1],
                ],
                [
                    20 => ['program_id' => 2, "order" => 1],
                ],
                [
                    20 => ['program_id' => 3, "order" => 1],
                ],
                [
                    20 => ['program_id' => 4, "order" => 1],
                ],
                [
                    20 => ['program_id' => 5, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 6, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 7, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 8, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 9, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 10, "order" =>1],
                 ],
                 [
                    20 => ['program_id' => 11, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 12, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 13, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 14, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 15, "order" => 1],
                 ],
                 [
                    20 => ['program_id' =>16, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 17, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 18, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 19, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 20, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 21, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 22, "order" => 1],
                 ],
                 [
                    20 => ['program_id' => 23, "order" => 1],
                 ],

            ]
        ],
        [
            "name_fr" => "Clear5 - Compléments des documents de l'étudiant",
            "name_en" => "Clear5 - Student documents completion",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    20 => ['program_id' => 1, "order" => 2],
                ],
                [
                    20 => ['program_id' => 2, "order" => 2],
                ],
                [
                    20 => ['program_id' => 3, "order" => 2],
                ],
                [
                    20 => ['program_id' => 4, "order" => 2],
                ],
                [
                    20 => ['program_id' => 5, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 6, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 7, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 8, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 9, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 10, "order" =>2],
                 ],
                 [
                    20 => ['program_id' => 11, "order" =>2],
                 ],
                 [
                    20 => ['program_id' => 12, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 13, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 14, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 15, "order" => 2],
                 ],
                 [
                    20 => ['program_id' =>16, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 17, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 18, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 19, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 20, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 21, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 22, "order" => 2],
                 ],
                 [
                    20 => ['program_id' => 23, "order" => 2],
                 ],

            ]
        ],
        [
            "name_fr" => "Clear6 - Compléments des documents des garants",
            "name_en" => "Clear6 - Guarantors documents completion",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    20 => ['program_id' => 1, "order" => 3],
                ],
                [
                    20 => ['program_id' => 2, "order" => 3],
                ],
                [
                    20 => ['program_id' => 3, "order" => 3],
                ],
                [
                    20 => ['program_id' => 4, "order" => 3],
                ],
                [
                    20 => ['program_id' => 5, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 6, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 7, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 8, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 9, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 10, "order" =>3],
                 ],
                 [
                    20 => ['program_id' => 11, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 12, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 13, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 14, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 15, "order" => 3],
                 ],
                 [
                    20 => ['program_id' =>16, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 17, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 18, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 19, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 20, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 21, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 22, "order" => 3],
                 ],
                 [
                    20 => ['program_id' => 23, "order" => 3],
                 ],
            ]
        ],
        [
            "name_fr" => "Documents Clear4, Clear5 et Clear6",
            "name_en" => "Clear4, Clear5 and Clear6 documents",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                // [
                //     8 => ['program_id' => 1],
                // ],
                // [
                //     8 => ['program_id' => 2],
                // ],
                // [
                //     8 => ['program_id' => 3],
                // ],
                // [
                //     8 => ['program_id' => 4],
                // ],
            ]
        ],

        [
            "name_fr" => "CAQ : Notification du service admin",
            "name_en" => "CAQ: Admin service notification",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    24 => ['program_id' => 1, "order" => 1],
                ],
                [
                    24 => ['program_id' => 2, "order" => 1],
                ],
                [
                    24 => ['program_id' => 3, "order" => 1],
                ],
                [
                    24 => ['program_id' => 4, "order" => 1],
                ],
                [
                    24 => ['program_id' => 5, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 6, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 7, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 8, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 9, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 10, "order" =>1],
                 ],
                 [
                    24 => ['program_id' => 11, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 12, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 13, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 14, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 15, "order" => 1],
                 ],
                 [
                    24 => ['program_id' =>16, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 17, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 18, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 19, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 20, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 21, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 22, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 23, "order" => 1],
                 ],
            ]
        ],
        [
            "name_fr" => "CAQ : Notification du service conseiller",
            "name_en" => "CAQ: Advisor service notification ",
			"visible_for_conseiller" => 0,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 1,
            "steps" => [
                [
                    24 => ['program_id' => 1, "order" => 2],
                ],
                [
                    24 => ['program_id' => 2, "order" => 2],
                ],
                [
                    24 => ['program_id' => 3, "order" => 2],
                ],
                [
                    24 => ['program_id' => 4, "order" => 2],
                ],
                [
                    24 => ['program_id' => 5, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 6, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 7, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 8, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 9, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 10, "order" =>2],
                 ],
                 [
                    24 => ['program_id' => 11, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 12, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 13, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 14, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 15, "order" => 2],
                 ],
                 [
                    24 => ['program_id' =>16, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 17, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 18, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 19, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 20, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 21, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 22, "order" => 2],
                 ],
                 [
                    24 => ['program_id' => 23, "order" => 2],
                 ],
            ]
        ],
        [
            "name_fr" => "Visa et PE : Notification du service admin",
            "name_en" => "Visa and PE: Admin service notification",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    25 => ['program_id' => 1, "order" => 1],
                ],
                [
                    25 => ['program_id' => 2, "order" => 1],
                ],
                [
                    25 => ['program_id' => 3, "order" => 1],
                ],
                [
                    25 => ['program_id' => 4, "order" => 1],
                ],
                [
                    24 => ['program_id' => 5, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 6, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 7, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 8, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 9, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 10, "order" =>1],
                 ],
                 [
                    24 => ['program_id' => 11, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 12, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 13, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 14, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 15, "order" => 1],
                 ],
                 [
                    24 => ['program_id' =>16, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 17, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 18, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 19, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 20, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 21, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 22, "order" => 1],
                 ],
                 [
                    24 => ['program_id' => 23, "order" => 1],
                 ],
            ]
        ],
        [
            "name_fr" => "Visa et PE : Notification du service conseiller",
            "name_en" => "Visa and PE: Advisor service notification",
			"visible_for_conseiller" => 0,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 1,
            "steps" => [
                [
                    25 => ['program_id' => 1, "order" => 2],
                ],
                [
                    25 => ['program_id' => 2, "order" => 2],
                ],
                [
                    25 => ['program_id' => 3, "order" => 2],
                ],
                [
                    25 => ['program_id' => 4, "order" => 2],
                ],
                [
                    25 => ['program_id' => 5, "order" => 2],
                ],
                [
                    25 => ['program_id' => 6, "order" => 2],
                ],
                [
                    25 => ['program_id' => 7, "order" => 2],
                ],
                [
                    25 => ['program_id' => 8, "order" => 2],
                ],
                [
                    25 => ['program_id' => 9, "order" => 2],
                ],
                [
                    25 => ['program_id' => 10, "order" => 2],
                ],
                [
                    25 => ['program_id' => 11, "order" => 2],
                ],
                [
                    25 => ['program_id' => 12, "order" => 2],
                ],
                [
                    25 => ['program_id' => 13, "order" => 2],
                ],
                [
                    25 => ['program_id' => 14, "order" => 2],
                ],
                [
                    25 => ['program_id' => 15, "order" => 2],
                ],
                [
                    25 => ['program_id' => 16, "order" => 2],
                ],
                [
                    25 => ['program_id' => 17, "order" => 2],
                ],
                [
                    25 => ['program_id' => 18, "order" => 2],
                ],
                [
                    25 => ['program_id' => 19, "order" => 2],
                ],
                [
                    25 => ['program_id' => 20, "order" => 2],
                ],
                [
                    25 => ['program_id' => 21, "order" => 2],
                ],
                [
                    25 => ['program_id' => 22, "order" => 2],
                ],
                [
                    25 => ['program_id' => 23, "order" => 2],
                ],

            ]
        ],

        [
            "name_fr" => "Paiement 5: 6700DH",
            "name_en" => "Payment 5: 6700DH",
			"visible_for_conseiller" => 0,
			"visible_for_agent" => 1,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    26 => ['program_id' => 1, "order" => 1],
                ],
                [
                    26 => ['program_id' => 2, "order" => 1],
                ],
                [
                    26 => ['program_id' => 3, "order" => 1],
                ],
                [
                    26 => ['program_id' => 4, "order" => 1],
                ],
                [
                    26 => ['program_id' => 5, "order" => 1],
                ],
                [
                    26 => ['program_id' => 6, "order" => 1],
                ],
                [
                    26 => ['program_id' => 7, "order" => 1],
                ],
                [
                    26 => ['program_id' => 8, "order" => 1],
                ],
                [
                    26 => ['program_id' => 9, "order" => 1],
                ],
                [
                    26 => ['program_id' => 10, "order" => 1],
                ],
                [
                    26 => ['program_id' => 11, "order" => 1],
                ],
                [
                    26 => ['program_id' => 12, "order" => 1],
                ],
                [
                    26 => ['program_id' => 13, "order" => 1],
                ],
                [
                    26 => ['program_id' => 14, "order" => 1],
                ],
                [
                    26 => ['program_id' => 15, "order" => 1],
                ],
                [
                    26 => ['program_id' => 16, "order" => 1],
                ],
                [
                    26 => ['program_id' => 17, "order" => 1],
                ],
                [
                    26 => ['program_id' => 18, "order" => 1],
                ],
                [
                    26 => ['program_id' => 19, "order" => 1],
                ],
                [
                    26 => ['program_id' => 20, "order" => 1],
                ],
                [
                    26 => ['program_id' => 21, "order" => 1],
                ],
                [
                    26 => ['program_id' => 22, "order" => 1],
                ],
                [
                    26 => ['program_id' => 23, "order" => 1],
                ],

            ]
        ],
        [
            "name_fr" => "Signature de la déclaration de consentement de la demande de CAQ",
            "name_en" => "Declaration of consent signature for the CAQ request",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    27 => ['program_id' => 1, "order" => 1],
                ],
                [
                    27 => ['program_id' => 2, "order" => 1],
                ],
                [
                    27 => ['program_id' => 3, "order" => 1],
                ],
                [
                    27 => ['program_id' => 4, "order" => 1],
                ],
                [
                    27 => ['program_id' => 5, "order" => 1],
                ],
                [
                    27 => ['program_id' => 6, "order" => 1],
                ],
                [
                    27 => ['program_id' => 7, "order" => 1],
                ],
                [
                    27 => ['program_id' => 8, "order" => 1],
                ],
                [
                    27 => ['program_id' => 9, "order" => 1],
                ],
                [
                    27 => ['program_id' => 10, "order" => 1],
                ],
                [
                    27 => ['program_id' => 11, "order" => 1],
                ],
                [
                    27 => ['program_id' => 12, "order" => 1],
                ],
                [
                    27 => ['program_id' => 13, "order" => 1],
                ],
                [
                    27 => ['program_id' => 14, "order" => 1],
                ],
                [
                    27 => ['program_id' => 15, "order" => 1],
                ],
                [
                    27 => ['program_id' => 16, "order" => 1],
                ],
                [
                    27 => ['program_id' => 17, "order" => 1],
                ],
                [
                    27 => ['program_id' => 18, "order" => 1],
                ],
                [
                    27 => ['program_id' => 19, "order" => 1],
                ],
                [
                    27 => ['program_id' => 20, "order" => 1],
                ],
                [
                    27 => ['program_id' => 21, "order" => 1],
                ],
                [
                    27 => ['program_id' => 22, "order" => 1],
                ],
                [
                    27 => ['program_id' => 23, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Signature des formulaires CIC par l'etudiant",
            "name_en" => "CIC forms signed - Student
            ",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            "steps" => [
                [
                    27 => ['program_id' => 1, "order" => 2],
                ],
                [
                    27 => ['program_id' => 2, "order" => 2],
                ],
                [
                    27 => ['program_id' => 3, "order" => 2],
                ],
                [
                    27 => ['program_id' => 4, "order" => 2],
                ],
                [
                    27 => ['program_id' => 5, "order" => 2],
                ],
                [
                    27 => ['program_id' => 6, "order" => 2],
                ],
                [
                    27 => ['program_id' => 7, "order" => 2],
                ],
                [
                    27 => ['program_id' => 8, "order" => 2],
                ],
                [
                    27 => ['program_id' => 9, "order" => 2],
                ],
                [
                    27 => ['program_id' => 10, "order" =>2],
                ],
                [
                    27 => ['program_id' => 11, "order" => 2],
                ],
                [
                    27 => ['program_id' => 12, "order" => 2],
                ],
                [
                    27 => ['program_id' => 13, "order" => 2],
                ],
                [
                    27 => ['program_id' => 14, "order" => 2],
                ],
                [
                    27 => ['program_id' => 15, "order" => 2],
                ],
                [
                    27 => ['program_id' => 16, "order" => 2],
                ],
                [
                    27 => ['program_id' => 17, "order" => 2],
                ],
                [
                    27 => ['program_id' => 18, "order" => 2],
                ],
                [
                    27 => ['program_id' => 19, "order" => 2],
                ],
                [
                    27 => ['program_id' => 20, "order" => 2],
                ],
                [
                    27 => ['program_id' => 21, "order" => 2],
                ],
                [
                    27 => ['program_id' => 22, "order" => 2],
                ],
                [
                    27 => ['program_id' => 23, "order" => 2],
                ],
            ]
        ],
        [
            "name_fr" => "Préparation du séjour",
            "name_en" => "Stay preparation",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 0,
			"visible_for_backoffice" => 0,
            // 'style_class' => 'success',
            "steps" => [
                [
                    34 => ['program_id' => 1, "order" => 1],
                ],
                [
                    34 => ['program_id' => 2, "order" => 1],
                ],
                [
                    34 => ['program_id' => 3, "order" => 1],
                ],
                [
                    34 => ['program_id' => 4, "order" => 1],
                ],
                [
                    34 => ['program_id' => 5, "order" => 1],
                ],
                [
                    34 => ['program_id' => 6, "order" => 1],
                ],
                [
                    34 => ['program_id' => 7, "order" => 1],
                ],
                [
                    34 => ['program_id' => 8, "order" => 1],
                ],
                [
                    34 => ['program_id' => 9, "order" => 1],
                ],
                [
                    34 => ['program_id' => 10, "order" => 1],
                ],
                [
                    34 => ['program_id' => 11, "order" => 1],
                ],
                [
                    34 => ['program_id' => 12, "order" => 1],
                ],
                [
                    34 => ['program_id' => 13, "order" => 1],
                ],
                [
                    34 => ['program_id' => 14, "order" => 1],
                ],
                [
                    34 => ['program_id' => 15, "order" => 1],
                ],
                [
                    34 => ['program_id' => 16, "order" => 1],
                ],
                [
                    34 => ['program_id' => 17, "order" => 1],
                ],
                [
                    34 => ['program_id' => 18, "order" => 1],
                ],
                [
                    34 => ['program_id' => 19, "order" => 1],
                ],
                [
                    34 => ['program_id' => 20, "order" => 1],
                ],
                [
                    34 => ['program_id' => 21, "order" => 1],
                ],
                [
                    34 => ['program_id' => 22, "order" => 1],
                ],
                [
                    34 => ['program_id' => 23, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Devis",
            "name_en" => "Budget",
			"visible_for_conseiller" => 1,
			"visible_for_agent" => 1,
			"visible_for_backoffice" => 1,
            "steps" => [
            ]
        ],
        [
            "name_fr" => "Clear11 - Procédure pour demander l\'équivalence des diplômes",
            "name_en" => "Clear11 - Procedure to request diploma equivalences",
            "visible_for_conseiller" => 1,
            "visible_for_agent" => 0,
            "visible_for_backoffice" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 1, "order" => 1],
                ],
                [
                    9 => ['program_id' => 2, "order" => 1],
                ],
                [
                   9 => ['program_id' => 5, "order" => 1],
                ],
                [
                   9 => ['program_id' => 6, "order" => 1],
                ],
                [
                   9 => ['program_id' => 7, "order" => 1],
                ],
                [
                   9 => ['program_id' => 8, "order" => 1],
                ],
                [
                   9 => ['program_id' => 9, "order" => 1],
                ],
                [
                   9 => ['program_id' => 10, "order" => 1],
                ],
                [
                   9 => ['program_id' => 11, "order" => 1],
                ],
                [
                   9 => ['program_id' => 12, "order" => 1],
                ],
                [
                   9 => ['program_id' => 13, "order" => 1],
                ],
                [
                   9 => ['program_id' => 14, "order" => 1],
                ],
                [
                   9 => ['program_id' => 15, "order" => 1],
                ],
                [
                   9 => ['program_id' => 16, "order" => 1],
                ],
                [
                   9 => ['program_id' => 17, "order" => 1],
                ],
                [
                   9 => ['program_id' => 18, "order" => 1],
                ],
                [
                   9 => ['program_id' => 19, "order" => 1],
                ],
                [
                   9 => ['program_id' => 20, "order" => 1],
                ],
                [
                   9 => ['program_id' => 21, "order" => 1],
                ],
                [
                   9 => ['program_id' => 22, "order" => 1],
                ],
                [
                   9 => ['program_id' => 23, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Clear12 - Documents à fournir pour la demande de résidence permanente - Travailleur Qualifié Québec",
            "name_en" => "Clear12 - Documents to provide to apply for the permanent residence - Qualified Worker Quebec",
            "visible_for_conseiller" => 1,
            "visible_for_agent" => 0,
            "visible_for_backoffice" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 3, "order" => 2],
                ],
            ]
        ],
        [
            "name_fr" => "Clear13 - Documents à fournir pour la demande de résidence permanente Entrée Express",
            "name_en" => "Clear13 - Documents to provide to apply for permanent residence Express Entry",
            "visible_for_conseiller" => 1,
            "visible_for_agent" => 0,
            "visible_for_backoffice" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 1, "order" => 3],
                ],
                [
                    9 => ['program_id' => 2, "order" => 3],
                ],
                [
                    9 => ['program_id' => 5, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 6, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 7, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 8, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 9, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 10, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 11, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 12, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 13, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 14, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 15, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 16, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 17, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 18, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 19, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 20, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 21, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 22, "order" => 3],
                 ],
                 [
                    9 => ['program_id' => 23, "order" => 3],
                 ],
            ]
        ],
        [
            "name_fr" => "Guide de préparation de test de langue",
            "name_en" => "Language Test Preparation Guide",
            "visible_for_conseiller" => 1,
            "visible_for_agent" => 0,
            "visible_for_backoffice" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 3, "order" => 3],
                ],
            ]
        ],
        [
            "name_fr" => "Rencontre 2: Terminer Appel",
            "name_en" => "Meeting 2: End Call",
            "visible_for_conseiller" => 1,
            "visible_for_agent" => 0,
            "visible_for_backoffice" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 1, "order" => 1],
                ],
                [
                    9 => ['program_id' => 2, "order" => 1],
                ],
                [
                    9 => ['program_id' => 3, "order" => 1],
                ],
                [
                    9 => ['program_id' => 4, "order" => 1],
                ],
                [
                    9 => ['program_id' => 5, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 6, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 7, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 8, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 9, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 10, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 11, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 12, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 13, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 14, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 15, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 16, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 17, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 18, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 19, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 20, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 21, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 22, "order" => 1],
                 ],
                 [
                    9 => ['program_id' => 23, "order" => 1],
                 ],
            ]
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->states as $phase) {
            \App\Models\SubStepState::where("name_fr", "LIKE", $phase["name_fr"])
                ->update(['name_en' => $phase["name_en"]]);
        }*/

        foreach ($this->states as $state) {

            $steps = $state['steps'];

            unset($state['steps']);

            $stat = \App\Models\SubStepState::where('name_fr', $state['name_fr'])->first();
            if (!$stat) {
                $stat = \App\Models\SubStepState::create(
                    $state
                );
            }

            foreach ($steps as $step) $stat->substeps()->attach($step);
        }
    }
}
