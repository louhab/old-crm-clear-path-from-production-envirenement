<?php

use Illuminate\Database\Seeder;

use App\Models\FrenchDiploma;

class FrenchDiplomaSeeder extends Seeder
{
    // No, Dalf, Delf 
    private $diplomas = [
        ['label'=> 'No'],
        ['label'=> 'Dalf'],
        ['label'=> 'Delf'],
        ['label'=> 'TCF'],
        ['label'=> 'TEF'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->diplomas as $diploma) {
            FrenchDiploma::create(
                [
                    'diploma_label' => $diploma['label'],
                ]
            );
        }
    }
}
