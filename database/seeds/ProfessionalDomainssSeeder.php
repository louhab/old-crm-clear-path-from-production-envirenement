<?php

use App\Models\ProfessionalDomain;
use Illuminate\Database\Seeder;

class ProfessionalDomainssSeeder extends Seeder
{
    private $domains = [
        ['domain'=> 'Santé'         ,'domain_an'=>"Health"],
        ['domain'=> 'Informatique'  ,'domain_an'=>"Computer Science"],
        ['domain'=> 'Agriculture'   ,'domain_an'=>"Agriculture"]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->domains as $phase) {
            ProfessionalDomain::where("professional_domain_name", "LIKE", $phase["domain"])
                ->update(['labelEng' => $phase["domain_an"]]);
        }*/

        foreach ($this->domains as $domain) {
            ProfessionalDomain::create(
                [
                    'professional_domain_name' => $domain['domain'],
                    'labelEng'=>$domain['domain_an']

                    ]
            );
        }
    }
}
