<?php

use Illuminate\Database\Seeder;

class StepPhasesSeeder extends Seeder
{
    private $phases = [
        ['phase_label_fr' => 'PROSPECTION', 'phase_label_en' => 'PROSPECTION'],
        ['phase_label_fr' => 'DÉVELOPPEMENT PROJET D’ÉTUDE', 'phase_label_en' => 'DEVELOPMENT OF THE STUDY PROJECT'],
        ['phase_label_fr' => 'DEMANDE VISA ET PERMIS D’ÉTUDES', 'phase_label_en' => 'VISA APPLICATION AND STUDY PERMIT'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->phases as $phase) {
            \App\Models\StepPhase::where("phase_label_fr", "LIKE", "%" .$phase["phase_label_fr"]."%")
                ->update(['phase_label_en' => $phase["phase_label_en"]]);
        }*/

        foreach ($this->phases as $phase) {
            \App\Models\StepPhase::create(
                $phase
            );
        }
    }
}
