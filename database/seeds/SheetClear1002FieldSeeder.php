<?php

use Illuminate\Database\Seeder;

use App\Models\SheetClear1002Field;

class SheetClear1002FieldSeeder extends Seeder
{
    private $combinations = [
        //lang : english
        [
            "language_id" => 1,
            "city_id" => 2,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 6,
            "diplomas_id" => 4,
            "program_id" => 7
        ],
        [
            "language_id" => 1,
            "city_id" => 2,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 7,
            "diplomas_id" => 6,
            "program_id" => 16
        ],
        //lang : french
        [
            "language_id" => 2,
            "city_id" => 1,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 9,
            "diplomas_id" => 1,
            "program_id" => 16
        ],
        // city 2
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 1,
            "etab_id" => 1,
            "etab_name_id" => 25,
            "diplomas_id" => 2,
            "program_id" => 7
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 1,
            "etab_id" => 2,
            "etab_name_id" => 17,
            "diplomas_id" => 2,
            "program_id" => 12
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 1,
            "etab_id" => 2,
            "etab_name_id" => 16,
            "diplomas_id" => 2,
            "program_id" => 10
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 1,
            "etab_id" => 2,
            "etab_name_id" => 19,
            "diplomas_id" => 2,
            "program_id" => 4
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 1,
            "etab_id" => 2,
            "etab_name_id" => 14,
            "diplomas_id" => 2,
            "program_id" => 15
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 2,
            "etab_id" => 1,
            "etab_name_id" => 24,
            "diplomas_id" => 3,
            "program_id" => 7
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 2,
            "etab_id" => 2,
            "etab_name_id" => 15,
            "diplomas_id" => 3,
            "program_id" => 3
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 2,
            "etab_id" => 2,
            "etab_name_id" => 21,
            "diplomas_id" => 3,
            "program_id" => 16
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 2,
            "etab_id" => 2,
            "etab_name_id" => 18,
            "diplomas_id" => 3,
            "program_id" => 11
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 2,
            "etab_id" => 2,
            "etab_name_id" => 20,
            "diplomas_id" => 3,
            "program_id" => 7
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 2,
            "etab_id" => 2,
            "etab_name_id" => 14,
            "diplomas_id" => 3,
            "program_id" => 13
        ],
        //
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 5,
            "diplomas_id" => 1,
            "program_id" => 7
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 3,
            "diplomas_id" => 6,
            "program_id" => 1
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 4,
            "diplomas_id" => 5,
            "program_id" => 7
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 2,
            "diplomas_id" => 4,
            "program_id" => 1
        ],
        [
            "language_id" => 2,
            "city_id" => 2,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 1,
            "diplomas_id" => 1,
            "program_id" => 1
        ],
        // city 3
        [
            "language_id" => 2,
            "city_id" => 3,
            "level_id" => 1,
            "etab_id" => 2,
            "etab_name_id" => 22,
            "diplomas_id" => 2,
            "program_id" => 14
        ],
        // city 4
        [
            "language_id" => 2,
            "city_id" => 4,
            "level_id" => 2,
            "etab_id" => 2,
            "etab_name_id" => 23,
            "diplomas_id" => 3,
            "program_id" => 6
        ],
        [
            "language_id" => 2,
            "city_id" => 4,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 8,
            "diplomas_id" => 5,
            "program_id" => 16
        ],
        //city 5
        [
            "language_id" => 2,
            "city_id" => 5,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 13,
            "diplomas_id" => 1,
            "program_id" => 2
        ],
        //city 6
        [
            "language_id" => 2,
            "city_id" => 6,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 11,
            "diplomas_id" => 6,
            "program_id" => 5
        ],
        //city 7
        [
            "language_id" => 2,
            "city_id" => 7,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 10,
            "diplomas_id" => 4,
            "program_id" => 5
        ],
        //city 8
        [
            "language_id" => 2,
            "city_id" => 8,
            "level_id" => 3,
            "etab_id" => 3,
            "etab_name_id" => 12,
            "diplomas_id" => 5,
            "program_id" => 5
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach ($this->combinations as $combination) {
            SheetClear1002Field::create([
                "admission_lang_study" => $combination["language_id"],
                "admission_city" => $combination["city_id"],
                "admission_school_level" => $combination["level_id"],
                "admission_est" => $combination["etab_id"],
                "admission_est_name" => $combination["etab_name_id"],
                "admission_diplomas" => $combination["diplomas_id"],
                "admission_program" => $combination["program_id"]
            ]);
        }
    }
}
