<?php

use App\Models\GuarantorInvestmentIncome;
use Illuminate\Database\Seeder;

class GuarantorInvestmentIncomesSeeder extends Seeder
{
    private $incomes = [
        ['label_fr'=> "Un revenu  d'investissement et placement", 'label_en'=> "Un revenu  d'investissement et placement"],
        ['label_fr'=> "Deux revenus  d'investissement et placement", 'label_en'=> "Deux revenus  d'investissement et placement"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->incomes as $income) {
            GuarantorInvestmentIncome::create(
                [
                    'label_fr' => $income['label_fr'],
                    'label_en' => $income['label_en'],
                ]
            );
        }
    }
}
