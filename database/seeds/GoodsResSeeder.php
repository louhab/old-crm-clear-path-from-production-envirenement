<?php

use App\Models\GoodsRes;
use Illuminate\Database\Seeder;

class GoodsResSeeder extends Seeder
{
    private $incomes = [
        ['label_fr'=> "Un bien immobilier", 'label_en'=> "Un bien immobilier"],
        ['label_fr'=> "Deux biens immobiliers", 'label_en'=> "Deux biens immobiliers"],
        ['label_fr'=> "Trois biens immobiliers", 'label_en'=> "Trois biens immobiliers"],

    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->incomes as $income) {
            GoodsRes::create(
                [
                    'label_fr' => $income['label_fr'],
                    'label_en' => $income['label_en'],
                ]
            );
        }
    }
}
