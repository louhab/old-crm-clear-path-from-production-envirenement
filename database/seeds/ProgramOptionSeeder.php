<?php

use Illuminate\Database\Seeder;
use App\Models\ProgramOption;

class ProgramOptionSeeder extends Seeder
{
    private $options = [
        //['Paiement 0', ["1" => [200, 4], "1" => [200, 1]]],
        // paiement 2
        ['name_fr' => 'Coût d\'évaluation des diplômes', 'program_payment_id' => 2, 'program_id' => 1,'amount' => 2432, 'has_couple' => false           ,"labelEng"=>"Diploma assessment cost"],
        ['name_fr' => 'Coût d\'évaluation des diplômes Conjoint', 'program_payment_id' => 2, 'program_id' => 1,'amount' => 2432, 'has_couple' => true   ,"labelEng"=>"Diploma assessment cost - Partner"],
        ['name_fr' => 'Test de francais', 'program_payment_id' => 2, 'program_id' => 1,'amount' => 2700, 'has_couple' => false                          ,"labelEng"=>"French test"],
        ['name_fr' => 'Test de francais Conjoint', 'program_payment_id' => 2, 'program_id' => 1,'amount' => 2700, 'has_couple' => true                  ,"labelEng"=>"French test - Partner"],
        ['name_fr' => 'Test d\'anglais', 'program_payment_id' => 2, 'program_id' => 1,'amount' => 2700, 'has_couple' => false                           ,"labelEng"=>"English test"],
        ['name_fr' => 'Test d\'anglais Conjoint', 'program_payment_id' => 2, 'program_id' => 1,'amount' => 2700, 'has_couple' => true                   ,"labelEng"=>"English test - Partner"],
        // paiement 4,"labelEng"=>""
        ['name_fr' => 'Coût de la demande d\'immigration Adulte', 'program_payment_id' => 4, 'program_id' => 1,'amount' => 3800, 'has_couple' => false  ,"labelEng"=>"Immigration application fee - Adult"],
        ['name_fr' => 'Coût de la demande d\'immigration conjoint', 'program_payment_id' => 4, 'program_id' => 1,'amount' => 3800, 'has_couple' => true ,"labelEng"=>"Immigration application fee - Partnet"],
        // paiment 5,"labelEng"=>""
        ['name_fr' => 'Droit de résidence permanente Adulte', 'program_payment_id' => 5, 'program_id' => 1,'amount' => 6270, 'has_couple' => false      ,"labelEng"=>"Permanent Residence - Adult"],
        ['name_fr' => 'Droit de résidence permanente conjoint', 'program_payment_id' => 5, 'program_id' => 1,'amount' => 6270, 'has_couple' => true     ,"labelEng"=>"Permanent Residence - Partner"],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->options as $phase) {
            ProgramOption::where("name_fr", "LIKE", $phase["name_fr"])
                ->update(['labelEng' => $phase["labelEng"]]);
        }*/

        foreach ($this->options as $option) {
            ProgramOption::create($option);
        }
    }
}
