<?php

use Illuminate\Database\Seeder;

use App\Models\CustomerRadioField;

class CustomerRadioFieldsSeeder extends Seeder
{
    private $fields = [
        ['name'=> 'medical_visit', 'code' => 'AA', 'value_fr'=> 'Avant la demande', 'value_en'=> 'Before the request'],
        ['name'=> 'medical_visit', 'code' => 'AB', 'value_fr'=> 'Apres la demande', 'value_en'=> 'After the request'],

        ['name'=> 'health', 'code' => 'AC', 'value_fr'=> 'Aucune maladie', 'value_en'=> 'No disease'],
        ['name'=> 'health', 'code' => 'AD', 'value_fr'=> 'Maladie', 'value_en'=> 'Suffering from some disease'],

        ['name'=> 'criminal_record', 'code' => 'AE', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        ['name'=> 'criminal_record', 'code' => 'AF', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],

        ['name'=> 'anthroprometric', 'code' => 'AG', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        ['name'=> 'anthroprometric', 'code' => 'AH', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],

        //Goods
        ['name'=> 'goods', 'code' => 'AI', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'goods', 'code' => 'AJ', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        //
        ['name'=> 'rental_income', 'code' => 'AK', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'rental_income', 'code' => 'AL', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        //
        ['name'=> 'business_income', 'code' => 'AM', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'business_income', 'code' => 'AN', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        //tuition_fees
        ['name'=> 'tuition_fees', 'code' => 'AO', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'tuition_fees', 'code' => 'AP', 'value_fr'=> 'Néant', 'value_en'=> 'No'],
        // cpg_account
        ['name'=> 'cpg_account', 'code' => 'AQ', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'cpg_account', 'code' => 'AR', 'value_fr'=> 'Néant', 'value_en'=> 'No'],
        //
        ['name'=> 'tef_test', 'code' => 'AS', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'tef_test', 'code' => 'AT', 'value_fr'=> 'Néant', 'value_en'=> 'No'],
        // language
        ['name'=> 'language', 'code' => 'AU', 'value_fr'=> 'Francais', 'value_en'=> 'French'],
        ['name'=> 'language', 'code' => 'AV', 'value_fr'=> 'Anglais', 'value_en'=> 'English'],
        // ['name'=> 'language', 'code' => 'AUV', 'value_fr'=> 'Francais - Anglais', 'value_en'=> 'French - English'],
        // training_type
        // ['name'=> 'training_type', 'code' => 'AAB', 'value_fr'=> 'Néant', 'value_en'=> 'No'],
        ['name'=> 'training_type', 'code' => 'AAC', 'value_fr'=> 'Formation professionnelle', 'value_en'=> 'Professional training'],
        ['name'=> 'training_type', 'code' => 'AAD', 'value_fr'=> 'Collège', 'value_en'=> 'College'],
        ['name'=> 'training_type', 'code' => 'AAE', 'value_fr'=> 'Université', 'value_en'=> 'University'],
        // ['name'=> 'training_type', 'code' => 'A_E', 'value_fr'=> 'Autre', 'value_en'=> 'Other'],
        // diplomas
        // ['name'=> 'diplomas', 'code' => 'AAF', 'value_fr'=> 'Néant', 'value_en'=> 'No'],
        ['name'=> 'diplomas', 'code' => 'AAG', 'value_fr'=> 'DEP', 'value_en'=> 'DEP'],
        ['name'=> 'diplomas', 'code' => 'AAH', 'value_fr'=> 'DEC', 'value_en'=> 'DEC'],
        ['name'=> 'diplomas', 'code' => 'AAI', 'value_fr'=> 'Baccalauréat', 'value_en'=> 'Baccalaureate'],
        ['name'=> 'diplomas', 'code' => 'AAJ', 'value_fr'=> 'Licence', 'value_en'=> 'Licence'],
        ['name'=> 'diplomas', 'code' => 'AAK', 'value_fr'=> 'DESS', 'value_en'=> 'DESS'],
        ['name'=> 'diplomas', 'code' => 'AAL', 'value_fr'=> 'Maîtrise', 'value_en'=> 'Master\'s degree'],
        ['name'=> 'diplomas', 'code' => 'AAM', 'value_fr'=> 'Doctorat', 'value_en'=> 'Doctorat'],
        // annual_budget
        // ['name'=> 'annual_budget', 'code' => 'AAN', 'value_fr'=> 'Néant', 'value_en'=> 'No'],
        ['name'=> 'annual_budget', 'code' => 'AAO', 'value_fr'=> '10000', 'value_en'=> '10000'],
        ['name'=> 'annual_budget', 'code' => 'AAP', 'value_fr'=> '15000', 'value_en'=> '15000'],
        ['name'=> 'annual_budget', 'code' => 'AAQ', 'value_fr'=> '20000', 'value_en'=> '20000'],
        ['name'=> 'annual_budget', 'code' => 'AAR', 'value_fr'=> '25000', 'value_en'=> '25000'],
        ['name'=> 'annual_budget', 'code' => 'AARRAA', 'value_fr'=> '>= 25000', 'value_en'=> '>= 25000'],
        // admission_fees
        ['name'=> 'admission_fees', 'code' => 'AAS', 'value_fr'=> 'Néant', 'value_en'=> 'No'],
        ['name'=> 'admission_fees', 'code' => 'AAT', 'value_fr'=> '10000', 'value_en'=> '10000'],
        ['name'=> 'admission_fees', 'code' => 'AAU', 'value_fr'=> '15000', 'value_en'=> '15000'],
        ['name'=> 'admission_fees', 'code' => 'AAV', 'value_fr'=> '20000', 'value_en'=> '20000'],
        ['name'=> 'admission_fees', 'code' => 'AAW', 'value_fr'=> '25000', 'value_en'=> '25000'],


        //Is Spouse Canadien
        ['name'=> 'spouse_cit', 'code' => 'spouse_cit-B', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'spouse_cit','code' => 'spouse_cit-A', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        // spouse_joining
        ['name'=> 'spouse_joining', 'code'=> 'spouse_joining-B', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'spouse_joining', 'code'=> 'spouse_joining-A', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        // diplomas_canadien
        ['name'=> 'diplomas_canadien', 'code'=> 'diplomas_canadien-B', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'diplomas_canadien', 'code'=> 'diplomas_canadien-A', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        // education_type
        ['name'=> 'education_type', 'code'=> 'education_type-A', 'value_fr'=> 'École secondaire ou moins', 'value_en'=> 'High school or less'],
        ['name'=> 'education_type', 'code'=> 'education_type-B', 'value_fr'=> 'Diplôme ou certificat d’un ou de deux ans', 'value_en'=> 'One or two year degree or certificate'],
        ['name'=> 'education_type', 'code'=> 'education_type-C', 'value_fr'=> 'Degré, diplôme ou certificat de trois ans ou plus OU une maîtrise, un doctorat ou un diplôme professionnel avec au moins une année de scolarité', 'value_en'=> 'Three or more years\' degree, diploma or certificate OR a master\'s, doctoral or professional degree with at least one year of schooling'],
        // lang_test_expire
        ['name'=> 'lang_test_expire', 'code'=> 'lang_test_expire-B', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        ['name'=> 'lang_test_expire', 'code'=> 'lang_test_expire-A', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        // lang_test
        ['name'=> 'lang_test', 'code'=> 'lang_test-A', 'value_fr'=> 'CELPIP-G', 'value_en'=> 'CELPIP-G'],
        ['name'=> 'lang_test', 'code'=> 'lang_test-B', 'value_fr'=> 'IELTS', 'value_en'=> 'IELTS'],
        ['name'=> 'lang_test', 'code'=> 'lang_test-C', 'value_fr'=> 'TEF Canada', 'value_en'=> 'TEF Canada'],
        ['name'=> 'lang_test', 'code'=> 'lang_test-D', 'value_fr'=> 'TCF Canada', 'value_en'=> 'TCF Canada'],
        // other_lang_test
        ['name'=> 'other_lang_test', 'code'=> 'other_lang_test-A', 'value_fr'=> 'CELPIP-G', 'value_en'=> 'CELPIP-G'],
        ['name'=> 'other_lang_test', 'code'=> 'other_lang_test-B', 'value_fr'=> 'IELTS', 'value_en'=> 'IELTS'],
        ['name'=> 'other_lang_test', 'code'=> 'other_lang_test-C', 'value_fr'=> 'sans objet', 'value_en'=> 'Not applicable'],
        // certificate_of_competence
        ['name'=> 'certificate_of_competence', 'code'=> 'certificate_of_competence-B', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'certificate_of_competence', 'code'=> 'certificate_of_competence-A', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        // valid_job
        ['name'=> 'valid_job', 'code'=> 'valid_job-B', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'valid_job', 'code'=> 'valid_job-A', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        // job_kind
        ['name'=> 'job_kind', 'code'=> 'job_kind-A', 'value_fr'=> 'Genre de compétence 00 de la CNP', 'value_en'=> 'Type of competence  00 of the CNP'],
        ['name'=> 'job_kind', 'code'=> 'job_kind-B', 'value_fr'=> 'Niveau de compétence A ou B ou tout genre autre que 00 de la CNP', 'value_en'=> 'Skill level A or B or any type other than CNP 00'],
        ['name'=> 'job_kind', 'code'=> 'job_kind-C', 'value_fr'=> 'Niveau de compétence C ou D de la CNP ', 'value_en'=> 'CNP Skill Level C or D'],
        // certificate_of_designation
        ['name'=> 'certificate_of_designation', 'code'=> 'certificate_of_designation-B', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'certificate_of_designation', 'code'=> 'certificate_of_designation-A', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        // sibling_canada
        ['name'=> 'sibling_canada', 'code'=> 'sibling_canada-B', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'sibling_canada', 'code'=> 'sibling_canada-A', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        // spouse_lang_test
        ['name'=> 'spouse_lang_test', 'code'=> 'spouse_lang_test-A', 'value_fr'=> 'CELPIP-G', 'value_en'=> 'CELPIP-G'],
        ['name'=> 'spouse_lang_test', 'code'=> 'spouse_lang_test-B', 'value_fr'=> 'IELTS', 'value_en'=> 'IELTS'],
        ['name'=> 'spouse_lang_test', 'code'=> 'spouse_lang_test-C', 'value_fr'=> 'TEF Canada', 'value_en'=> 'TEF Canada'],
        ['name'=> 'spouse_lang_test', 'code'=> 'spouse_lang_test-D', 'value_fr'=> 'TCF Canada', 'value_en'=> 'TCF Canada'],
        ['name'=> 'spouse_lang_test', 'code'=> 'spouse_lang_test-E', 'value_fr'=> 'sans objet', 'value_en'=> 'Not applicable'],

        // clear1001 visa
        // visa_request
        ['name'=> 'visa_request', 'code'=> 'BAA', 'value_fr'=> 'Oui', 'value_en'=> 'Yes'],
        ['name'=> 'visa_request', 'code'=> 'BAB', 'value_fr'=> 'Non', 'value_en'=> 'No'],
        // visa_request_result
        ['name'=> 'visa_request_result', 'code'=> 'BAC', 'value_fr'=> 'Admis', 'value_en'=> 'Admitted'],
        ['name'=> 'visa_request_result', 'code'=> 'BAD', 'value_fr'=> 'Refus', 'value_en'=> 'Refusal'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->fields as $phase) {
            CustomerRadioField::where("value_fr", "LIKE", $phase["value_fr"])
                ->update(['value_en' => $phase["value_en"]]);
        }*/

        foreach ($this->fields as $field) {
            $code = null;
            if (array_key_exists('code', $field)) {
                $code = $field['code'];
            }
            CustomerRadioField::create(
                [
                    'name' => $field['name'],
                    'value_fr' => $field['value_fr'],
                    'value_en' => $field['value_en'],
                    'code' => $code,
                ]
            );
        }
    }
}
