<?php

use Illuminate\Database\Seeder;

use App\Models\MeetingShow;

class MeetingShowSeeder extends Seeder
{
    private $slides = [
        [
            "label" => "silde 1",
            "content" => "Accueil, déterminer le profil duclient potentiel et comprendre son besoin",
            "hasButton" => 0,
            "duration" => 180,
            "order" => 1
        ],
        [
            "label" => "silde 2",
            "content" => "Vendre le Canada",
            "hasButton" => 0,
            "duration" => 120,
            "order" => 2
        ],
        [
            "label" => "silde 3",
            "content" => "Vendre les services de Clear Path Canada et expliquer notre processus de 6 rencontres",
            "hasButton" => 0,
            "duration" => 480,
            "order" => 3
        ],
        [
            "label" => "silde 4",
            "content" => "Simulation: Remplir le Clear 1001",
            "hasButton" => 1,
            "duration" => 1020,
            "order" => 4
        ],
        [
            "label" => "silde 5",
            "content" => "Simulation: Expliquer le devis",
            "hasButton" => 0,
            "duration" => 300,
            "order" => 5
        ],
        [
            "label" => "silde 6",
            "content" => "Simulation: Expliquer le contrat",
            "hasButton" => 0,
            "duration" => 300,
            "order" => 6
        ],
        [
            "label" => "silde 7",
            "content" => "Simulation: Expliquer le portail client potentiel",
            "hasButton" => 0,
            "duration" => 180,
            "order" => 7
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->slides as $slide) {
            MeetingShow::create(
                [
                    "label" => $slide["label"],
                    "content" => $slide["content"],
                    "hasButton" => $slide["hasButton"],
                    "duration" => $slide["duration"],
                    "order" => $slide["order"]
                ]
            );
        }
        //
    }
}
