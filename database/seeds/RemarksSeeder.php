<?php

use Illuminate\Database\Seeder;

use App\Models\Remarks;

class RemarksSeeder extends Seeder
{
    private $remarks = [
        ['field_name' => 'level_study', 'conseill' => 'Non admisisble'                                                                                                                                                                                      ,"labelEng"=>"Not admissible"],
        ['field_name' => 'level_study', 'conseill' => 'Admissible'                                                                                                                                                                                          ,"labelEng"=>"Admissible"],
        ['field_name' => 'branch', 'conseill' => 'Non admisisble'                                                                                                                                                                                           ,"labelEng"=>"Not admissible"],
        ['field_name' => 'branch', 'conseill' => 'Admissible'                                                                                                                                                                                               ,"labelEng"=>"Admissible"],
        ['field_name' => 'grade', 'conseill' => 'Non admisisble'                                                                                                                                                                                            ,"labelEng"=>"Not admissible"],
        ['field_name' => 'grade', 'conseill' => 'Admissible'                                                                                                                                                                                                ,"labelEng"=>"Admissible"],
        ['field_name' => 'lang_level', 'conseill' => 'Non admisisble'                                                                                                                                                                                       ,"labelEng"=>"Not admissible"],
        ['field_name' => 'lang_level', 'conseill' => 'Admissible'                                                                                                                                                                                           ,"labelEng"=>"Admissible"],
        ['field_name' => 'diploma_fr', 'conseill' => 'Pas besoin'                                                                                                                                                                                           ,"labelEng"=>"No need"],
        ['field_name' => 'diploma_fr', 'conseill' => 'Conforme'                                                                                                                                                                                             ,"labelEng"=>"Compliant"],
        ['field_name' => 'test_fr', 'conseill' => 'Pas besoin'                                                                                                                                                                                              ,"labelEng"=>"No need"],
        ['field_name' => 'test_fr', 'conseill' => 'Conforme'                                                                                                                                                                                                ,"labelEng"=>"Compliant"],
        ['field_name' => 'test_fr', 'conseill' => 'TEF A faire'                                                                                                                                                                                             ,"labelEng"=>"TEF To do"],
        ['field_name' => 'diploma_en', 'conseill' => 'Pas besoin'                                                                                                                                                                                           ,"labelEng"=>"No need"],
        ['field_name' => 'diploma_en', 'conseill' => 'Conforme'                                                                                                                                                                                             ,"labelEng"=>"Compliant"],
        ['field_name' => 'diploma_en', 'conseill' => 'A faire'                                                                                                                                                                                              ,"labelEng"=>"To do"],
        ['field_name' => 'test_en', 'conseill' => 'Pas besoin'                                                                                                                                                                                              ,"labelEng"=>"No need"],
        ['field_name' => 'test_en', 'conseill' => 'Conforme'                                                                                                                                                                                                ,"labelEng"=>"Compliant"],
        ['field_name' => 'test_en', 'conseill' => 'A faire'                                                                                                                                                                                                 ,"labelEng"=>"To do"],
        ['field_name' => 'medical_visit', 'conseill' => 'Refus de visa : pour certaines maladies chroniques coûteuses', 'group' => '7'                                                                                                                      ,"labelEng"=>"Visa denial: for certain costly chronic diseases"],
        ['field_name' => 'health', 'conseill' => 'tous les examens, analyses ou traitements particuliers requis', 'group' => '7'                                                                                                                            ,"labelEng"=>"Any special examinations, tests or treatments required"],
        ['field_name' => 'health', 'conseill' => 'Frais : 1700 DH', 'group' => '7'                                                                                                                                                                          ,"labelEng"=>"Fees: 1700 DH"],
        ['field_name' => 'health', 'conseill' => 'Résultat  de la visite médicale est validé pour 12 mois', 'group' => '7'                                                                                                                                  ,"labelEng"=>"The result of the medical examination is validated for 12 months"],
        ['field_name' => 'criminal_record', 'conseill' => 'Casier judiciaire (Tribunal)', 'group' => '9'                                                                                                                                                    ,"labelEng"=>"Casier judiciaire (Ministry of Justice)"],
        ['field_name' => 'anthroprometric', 'conseill' => 'Fiche anthropométrique (Police)', 'group' => '9'                                                                                                                                                 ,"labelEng"=>"Anthropometric profile (police)"],
        ['field_name' => 'anthroprometric', 'conseill' => 'Motif pénal : non admissible', 'group' => '9'                                                                                                                                                    ,"labelEng"=>"Penal reason: not admissible"],
        ['field_name' => 'anthroprometric', 'conseill' => 'Valable dans tous les pays de plus de 6 mois de résidence', 'group' => '9'                                                                                                                       ,"labelEng"=>"Valid in all countries with more than 6 months of residence"],
        ['field_name' => 'garant_type', 'conseill' => 'Compte bancaire personnel 6 mois', 'group' => '8'                                                                                                                                                    ,"labelEng"=>"Personal bank account 6 months"],
        ['field_name' => 'income', 'conseill' => 'Non conforme', 'group' => '8'                                                                                                                                                                             ,"labelEng"=>"Not Compliant"],
        ['field_name' => 'income', 'conseill' => 'Conforme', 'group' => '8'                                                                                                                                                                                 ,"labelEng"=>"Compliant"],
        ['field_name' => 'amount', 'conseill' => 'Il faut démontrer une capacité financiére de 300000 MAD et plus.   Il faut présenter les six derniers relevés bancaire avec un solde mensuel de 300000 MAD et plus.', 'group' => '8'                      ,"labelEng"=>"You must demonstrate a financial capacity of 300000 MAD and more. You must present the last six bank statements with a monthly balance of 300000 MAD or more."],
        ['field_name' => 'goods', 'conseill' => 'Certificat de propriété de tous les biens et une évaluation financiére faite auprés d\'un notaire, évaluateur ou expert comptable', 'group' => '8'                                                         ,"labelEng"=>"Certificate of ownership of all assets and a financial evaluation from a notary, appraiser or accountant"],
        ['field_name' => 'rental_income', 'conseill' => 'Besoin des contrats locatifs', 'group' => '8'                                                                                                                                                      ,"labelEng"=>"Need rental contracts"],
        ['field_name' => 'business_income', 'conseill' => 'Besoin des états financiers de l\'entreprise et si vous êtes travailleur autonome il faut présenter la carte du travailleur autonome et une attesttion de revenu légalisée', 'group' => '8'      ,"labelEng"=>"Need the financial statements of the business and if you are self-employed you must present the self-employed worker card and a legalized income certificate"],
        ['field_name' => 'tuition_fees', 'conseill' => 'Paiement des frais de scolarité de la première année ( il faut prévoir un minimum de 82000 MAD)', 'group' => '10'                                                                                   ,"labelEng"=>"Payment of tuition fees for the first year (a minimum of 8.700 MAD must be provided)"],
        ['field_name' => 'cpg_account', 'conseill' => 'Ouvrir un compte CPG au Canada et bloquer un montant de 10200$(76500 MAD)', 'group' => '10'                                                                                                          ,"labelEng"=>"Open a CPG account in Canada and block an amount of $10,200 (76500 MAD)"],
        ['field_name' => 'tef_test', 'conseill' => 'Passer le TEF et avoir une note de 6', 'group' => '10'                                                                                                                                                  ,"labelEng"=>"Pass the TEF and have a grade of 7 (minimum)"],
        ['field_name' => 'admission_fees', 'code' => 'AAC', 'conseill' => 'DEP 1800 heures', 'group' => '11'                                                                                                                                                ,"labelEng"=>"DEP 1.800 hours"],
        ['field_name' => 'admission_fees', 'code' => 'AAC', 'conseill' => 'Admissibilité PEQ (immigration au Québec)', 'group' => '11'                                                                                                                      ,"labelEng"=>"PEQ Eligibility (immigration to Quebec)"],
        ['field_name' => 'admission_fees', 'code' => 'AAC', 'conseill' => 'Travailler 18 mois après la fin des études', 'group' => '11'                                                                                                                     ,"labelEng"=>"Work 18 months after graduation"],
        ['field_name' => 'admission_fees', 'code' => 'AAC', 'conseill' => 'Travailler à temps plein', 'group' => '11'                                                                                                                                       ,"labelEng"=>"Working full time"],
        ['field_name' => 'admission_fees', 'code' => 'AAC', 'conseill' => 'Emploi CNP : Niveau 0, A, B et C', 'group' => '11'                                                                                                                               ,"labelEng"=>"CNP job: Level 0, A, B and C"],
        ['field_name' => 'admission_fees', 'code' => 'AAC', 'conseill' => 'Occuper un emploi à temps plein au moment de la demande', 'group' => '11'                                                                                                        ,"labelEng"=>"Have a full-time job at the time of the request"],
        ['field_name' => 'admission_fees', 'code' => 'AAC', 'conseill' => '12000$ par année et plus', 'group' => '11'                                                                                                                                       ,"labelEng"=>"12000$ per year and more"],
        ['field_name' => 'admission_fees', 'code' => 'AAD', 'conseill' => 'DEC', 'group' => '11'                                                                                                                                                            ,"labelEng"=>"DEC"],
        ['field_name' => 'admission_fees', 'code' => 'AAD', 'conseill' => 'Admissibilité PEQ (immigration au Québec)', 'group' => '11'                                                                                                                      ,"labelEng"=>"PEQ eligibility (immigration to Quebec)"],
        ['field_name' => 'admission_fees', 'code' => 'AAD', 'conseill' => 'Travailler 12 mois après la fin des études', 'group' => '11'                                                                                                                     ,"labelEng"=>"Work 12 months after graduation"],
        ['field_name' => 'admission_fees', 'code' => 'AAD', 'conseill' => 'Travailler à temps plein', 'group' => '11'                                                                                                                                       ,"labelEng"=>"Full time job"],
        ['field_name' => 'admission_fees', 'code' => 'AAD', 'conseill' => 'Emploi CNP : Niveau 0, A, B et C', 'group' => '11'                                                                                                                               ,"labelEng"=>"CNP job: Level 0, A, B and C"],
        ['field_name' => 'admission_fees', 'code' => 'AAD', 'conseill' => 'Occuper un emploi à temps plein au moment de la demande', 'group' => '11'                                                                                                        ,"labelEng"=>"Hold a full-time job at the time of application"],
        ['field_name' => 'admission_fees', 'code' => 'AAD', 'conseill' => '17000$ par année et plus', 'group' => '11'                                                                                                                                       ,"labelEng"=>"17000$ per year and more"],
        ['field_name' => 'admission_fees', 'code' => 'AAE', 'conseill' => 'Bachelor (licence)', 'group' => '11'                                                                                                                                             ,"labelEng"=>"Bachelor (licence)"],
        ['field_name' => 'admission_fees', 'code' => 'AAE', 'conseill' => 'DESS', 'group' => '11'                                                                                                                                                           ,"labelEng"=>"DESS"],
        ['field_name' => 'admission_fees', 'code' => 'AAE', 'conseill' => 'Maîtrise ', 'group' => '11'                                                                                                                                                      ,"labelEng"=>"Mastery"],
        ['field_name' => 'admission_fees', 'code' => 'AAE', 'conseill' => 'Doctorat', 'group' => '11'                                                                                                                                                       ,"labelEng"=>"Doctorate"],
        ['field_name' => 'admission_fees', 'code' => 'AAE', 'conseill' => 'Admissibilité PEQ (immigration au Québec)', 'group' => '11'                                                                                                                      ,"labelEng"=>"PEQ eligibility (immigration to Quebec)"],
        ['field_name' => 'admission_fees', 'code' => 'AAE', 'conseill' => 'Admissibilité PEQ (même chose que le DEC)', 'group' => '11'                                                                                                                      ,"labelEng"=>"Admissibilité PEQ (même chose que le DEC)"],
        ['field_name' => 'admission_fees', 'code' => 'AAE', 'conseill' => '10000$ par année et plus', 'group' => '11'                                                                                                                                       ,"labelEng"=>"10000$ per year and more"],



        // ['field_name' => '', 'conseill' => '', 'group' => '11'],
        // ['field_name' => '', 'conseill' => ''],
        // ['field_name' => '', 'conseill' => ''],
        // ['field_name' => '', 'conseill' => ''],
        // ['field_name' => '', 'conseill' => ''],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->remarks as $phase) {
            Remarks::where("conseill", "LIKE", $phase["conseill"])
                ->update(['labelEng' => $phase["labelEng"]]);
        }*/
        foreach ($this->remarks as $remark) {
            $data = [
                'field_name' => $remark['field_name'],
                'conseill' => $remark['conseill'],
                'labelEng' => $remark['labelEng']
            ];
            if(array_key_exists('group', $remark)){
                $data['remark_group_id'] = $remark['group'];
            }
            if(array_key_exists('code', $remark)){
                $data['code'] = $remark['code'];
            }
            Remarks::create(
                $data
            );
        }
    }
}
