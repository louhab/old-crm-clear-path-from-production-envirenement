<?php

use Illuminate\Database\Seeder;

class FieldSheetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sheet = \App\Models\InformationSheet::find(3);
        foreach ($sheet->fields()->wherePivot('program_id', '=', 4)->withPivot(['id', 'customer_field_group_id', 'required', 'order'])->orderBy('customer_fields.id')->get() as $field) {
            // dd($field->toArray());
            $field_sheet = \App\Models\FieldSheet::find($field->pivot->id);
            $field_sheet->customer_field_group_id = $field->customer_field_group_id;
            $field_sheet->save();
        }

    }
}
