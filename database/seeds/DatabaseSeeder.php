<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CurrentSelectedConseillerSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(RankingsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(StepPhasesSeeder::class);
        $this->call(ProgramStepsSeeder::class);
        $this->call(ProgramSubStepsSeeder::class);
        $this->call(ProgramsSeeder::class); // <---
        // ProgramStepStateSeeder <---
        $this->call(DocumentTypeGroupSeeder::class); // <---
        $this->call(MediaSheetSeeder::class); // <---
        $this->call(DocumentTypesSeeder::class); // <---
        $this->call(LeadFormMarketingCampaignsSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(EnglishLevelsSeeder::class);
        $this->call(ProfessionalDomainssSeeder::class);
        $this->call(ProjectedDiplomaTypesSeeder::class);
        $this->call(SchoolLevelsSeeder::class);
        $this->call(LeadStatusSeeder::class);
        $this->call(SelectedLeadSupport::class);
        // $this->call(LeadsSeeder::class);
        // $this->call(CustomersStudentsSeeder::class);
        // $this->call(CustomersImmigrantsSeeder::class);
        // $this->call(HistoCallsSeeder::class);
        // $this->call(CustomersMessagesSeeder::class);

        // ProgramPaymentSeeder
        $this->call(CurrencySeeder::class);
        $this->call(ProgramPaymentSeeder::class);
        $this->call(PhasesSeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(ServiceDetailsSeeder::class);
        $this->call(CustomerFieldGroupsSeeder::class);
        $this->call(CustomerFieldSubGroupSeeder::class);
        $this->call(InformationSheetsSeeder::class); // <---
        $this->call(CustomerFieldsSeeder::class); // <---
        $this->call(StepStatesSeeder::class);
        $this->call(SubStepStateSeeder::class); // <---
        //new fields
        $this->call(BranchSeeder::class);
        $this->call(LevelOfStudySeeder::class);
        $this->call(LanguageLevelSeeder::class);
        $this->call(EnglishDiplomaSeeder::class);
        $this->call(EnglishTestSeeder::class);
        $this->call(FrenchDiplomaSeeder::class);
        $this->call(FrenchTestSeeder::class);
        $this->call(CustomerRadioFieldsSeeder::class);
        $this->call(GuarantorTypeSeeder::class);
        $this->call(AmountAvailableSeeder::class);
        $this->call(MonthlyIncomeSeeder::class);

        $this->call(PaymentMethodSeeder::class);
        $this->call(MaritalStatusSeeder::class);
        $this->call(EducationLevelSeeder::class);
        $this->call(CustomerAgeSeeder::class);
        $this->call(WorkExperienceSeeder::class);
        $this->call(WorkExperienceAbroadSeeder::class);
        $this->call(LanguageTestScoreSeeder::class);
        $this->call(ProgramChoiceSeeder::class);
        $this->call(RemarksSeeder::class);
        $this->call(ProvinceSeeder::class);
        $this->call(CanadaCitySeeder::class);

        $this->call(AdmissionProgramSeeder::class);
        $this->call(AdmissionDiplomasSeeder::class);
        $this->call(AdmissionEtabNameSeeder::class);
        $this->call(AdmissionEstablishmentSeeder::class);
        $this->call(AdmissionSchoolLevelSeeder::class);
        $this->call(AdmissionCitySeeder::class);
        $this->call(LanguageOfStudySeeder::class);
        // $this->call(SheetClear1002FieldSeeder::class);
        //
        $this->call(AdmissionStateGroupSeeder::class);
        $this->call(AdmissionStateSeeder::class);
        $this->call(ProgramFieldSeeder::class);
        //
        $this->call(ImmigrantActivitySeeder::class);
        $this->call(ImmigrantStatusSeeder::class);
        //
        $this->call(ProgramOptionSeeder::class);
        //
        $this->call(OrientationSchoolSeeder::class);

        // Years of graduation
        $this->call(GraduationYearSeeder::class);

        // Amoubts and incomes in usd&euro
        $this->call(AddAmountsSeeder::class);
        $this->call(AddIncomesSeeder::class);
        // is test taken
        $this->call(IsLanguageTestTakenSeeder::class);
    }
}
