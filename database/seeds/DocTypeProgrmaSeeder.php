<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DocTypeProgrmaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $program_id_uk = 31;

        $json_uk = file_get_contents(__DIR__ . '/document_type_program_sub_step.json');

        $json_data_uk = json_decode($json_uk, true);

        foreach ($json_data_uk as $key => $value) {
            DB::table('document_type_program_sub_step')->insert([
                "program_sub_step_id" => $value["program_sub_step_id"],
                "document_type_id" => $value["document_type_id"],
                "program_id" => $program_id_uk,
                "order" => $value["order"],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }


        $program_id_es = 32;
        $json_es = file_get_contents(__DIR__ . '/document_type_program_sub_step.json');

        $json_data_es = json_decode($json_es, true);
        foreach ($json_data_es as $key => $value) {
            DB::table('document_type_program_sub_step')->insert([
                "program_sub_step_id" => $value["program_sub_step_id"],
                "document_type_id" => $value["document_type_id"],
                "program_id" => $program_id_es,
                "order" => $value["order"],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
