<?php

use App\Models\Phase;
use Illuminate\Database\Seeder;

class PhasesSeeder extends Seeder
{
    private $phases = [

        //IMMIGRANT
        ['name'=> 'Etape 1 Analyse des données et admissibilité', 'name_en'=> 'PHASE 1 DATA ANALYSIS AND ELIGIBILITY', 'order' => 1, 'program_id' => 2],
        ['name'=> 'Etape 2 Développement du projet d\'immigration', 'name_en' => 'PHASE 2 DEVELOPMENT OF THE IMMIGRATION PROJECT', 'order' => 2, 'program_id' => 2],
        ['name'=> 'Etape 3 Demande du visa et résidence permanente', 'name_en' => 'PHASE 3 VISA APPLICATION AND PERMANENT RESIDENCE', 'order' => 3, 'program_id' => 2],
        ['name'=> 'Options', 'name_en'=> 'Options', 'order' => 4, 'program_id' => 2],

        //ETUDIANT
        ['name'=> 'Etape 1 Analyse des données et admissibilité', 'name_en'=> 'PHASE 1 DATA ANALYSIS AND ELIGIBILITY', 'order' => 1, 'program_id' => 1],
        ['name'=> 'Etape 2 Développement du projet d\'études', 'name_en'=> 'PHASE 2 DEVELOPMENT OF THE STUDY PROJECT', 'order' => 2, 'program_id' => 1],
        ['name'=> 'Etape 3 Demande du visa et permis d\'études', 'name_en'=> 'PHASE 3 APPLICATION FOR VISA AND STUDY PERMIT', 'order' => 3, 'program_id' => 1],
        ['name'=> 'Options', 'name_en'=> 'Options', 'order' => 4, 'program_id' => 1],

    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->phases as $phase) {
            Phase::create(
                [
                    'name' => $phase['name'],
                    'name_en' => $phase['name_en'],
                    'order' => $phase['order'],
                    'program_id' => $phase['program_id']
                ]
            );
        }
    }
}
