<?php

use Illuminate\Database\Seeder;

class DocumentTypeGroupSeeder extends Seeder
{
    private $documentTypeGroups = [
        /*['name_fr' => 'Récurant principal', 'name_en' => 'Récurant principal', 'order' => '1'],//1
        ['name_fr' => 'Conjoint', 'name_en' => 'Conjoint', 'order' => '2'],//2*/
        // new seed
        // clear 1 groups
        ['name_fr' => "Documents d'identité", 'name_en' => "Documents d'identité", 'order' => '10'],//3
        ['name_fr' => "Dossier scolaire", 'name_en' => "Dossier scolaire", 'order' => '20'],//4
        ['name_fr' => "Documents du Niveau Baccalauréat", 'name_en' => "Documents du Niveau Baccalauréat", 'order' => '30'],//5
        ['name_fr' => "Documents du Baccalauréat", 'name_en' => "Documents du Baccalauréat", 'order' => '40'],//6
        ['name_fr' => "Documents du diplôme niveau Bac + 2", 'name_en' => "Documents du diplôme niveau Bac + 2", 'order' => '50'],//7
        ['name_fr' => "Documents du licence", 'name_en' => "Documents du licence", 'order' => '60'],//8
        ['name_fr' => "Documents de la Licence professionnelle", 'name_en' => "Documents de la Licence professionnelle", 'order' => '70'],//9
        ['name_fr' => "Documents de la Maîtrise", 'name_en' => "Documents de la Maîtrise", 'order' => '80'],//10
        ['name_fr' => "Documents du Doctorat", 'name_en' => "Documents du Doctorat", 'order' => '90'],//11
        // clear 2 groups
        ['name_fr' => "Garant Nom Prenom", 'name_en' => "Garant Nom Prenom", 'order' => '10'],//12
        ['name_fr' => "Documents financiers des revenus de travail  N° 1", 'name_en' => "Documents financiers des revenus de travail  N° 1", 'order' => '20'],//13
        ['name_fr' => "Documents financiers des revenus de travail  N° 2", 'name_en' => "Documents financiers des revenus de travail  N° 2", 'order' => '30'],//14
        ['name_fr' => "Documents financiers des revenus de retraite  N° 1", 'name_en' => "Documents financiers des revenus de retraite  N° 1", 'order' => '40'],//15
        ['name_fr' => "Documents financiers des revenus de retraite  N° 2", 'name_en' => "Documents financiers des revenus de retraite  N° 2", 'order' => '50'],//16
        ['name_fr' => "Documents financiers des revenus d'entreprise  N° 1", 'name_en' => "Documents financiers des revenus d'entreprise  N° 1", 'order' => '60'],//17
        ['name_fr' => "Documents financiers des revenus d'entreprise  N° 2", 'name_en' => "Documents financiers des revenus d'entreprise  N° 2", 'order' => '70'],//18
        ['name_fr' => "Documents financiers des revenus de profession libérale  N° 1", 'name_en' => "Documents financiers des revenus de profession libérale  N° 1", 'order' => '80'],//19
        ['name_fr' => "Documents financiers des revenus de profession libérale  N° 2", 'name_en' => "Documents financiers des revenus de profession libérale  N° 2", 'order' => '90'],//20
        ['name_fr' => "Documents financiers des revenus de travailleur autonome  N° 1", 'name_en' => "Documents financiers des revenus de travailleur autonome  N° 1", 'order' => '100'],//21
        ['name_fr' => "Documents financiers des revenus de travailleur autonome  N° 2", 'name_en' => "Documents financiers des revenus de travailleur autonome  N° 2", 'order' => '110'],//22
        ['name_fr' => "Documents financiers des revenus locatifs  N° 1", 'name_en' => "Documents financiers des revenus locatifs  N° 1", 'order' => '120'],//23
        ['name_fr' => "Documents financiers des revenus locatifs  N° 2", 'name_en' => "Documents financiers des revenus locatifs  N° 2", 'order' => '130'],//24

        ['name_fr' => "Documents financiers des revenus d’investissement et placement  N° 1", 'name_en' => "Documents financiers des revenus d’investissement et placement  N° 1", 'order' => '140'],//25
        ['name_fr' => "Documents financiers des revenus d’investissement et placement  N° 2", 'name_en' => "Documents financiers des revenus d’investissement et placement  N° 2", 'order' => '150'],//26
        ['name_fr' => "Documents financiers des autres sources de revenus N° 1", 'name_en' => "Documents financiers des autres sources de revenus N° 1", 'order' => '160'],//27
        ['name_fr' => "Documents financiers des autres sources de revenus N° 2", 'name_en' => "Documents financiers des autres sources de revenus N° 2", 'order' => '170'],//28
        ['name_fr' => "6 derniers relevés bancaires personnels", 'name_en' => "6 derniers relevés bancaires personnels", 'order' => '180'],//29
        /*['name_fr' => "", 'name_en' => "", 'order' => '190'],//30
        ['name_fr' => "", 'name_en' => "", 'order' => '200'],//31*/
        // end seed
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->documentTypeGroups as $documentTypeGroup) {
            \App\Models\DocumentTypeGroup::create(
                $documentTypeGroup
            );
        }
    }
}
