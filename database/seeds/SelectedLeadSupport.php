<?php

use App\Models\SelectedLeadsSupport;
use Illuminate\Database\Seeder;

class SelectedLeadSupport extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SelectedLeadsSupport::create(
            [
                'number' => 1
            ]
        );
    }
}
