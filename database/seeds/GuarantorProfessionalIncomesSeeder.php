<?php

use App\Models\GuarantorProfessionalIncome;
use Illuminate\Database\Seeder;

class GuarantorProfessionalIncomesSeeder extends Seeder
{
    private $incomes = [
        ['label_fr'=> "Un revenu de profession libérale", 'label_en'=> "Un revenu de profession libérale"],
        ['label_fr'=> "Deux revenus de profession libérale", 'label_en'=> "Deux revenus de profession libérale"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->incomes as $income) {
            GuarantorProfessionalIncome::create(
                [
                    'label_fr' => $income['label_fr'],
                    'label_en' => $income['label_en'],
                ]
            );
        }
    }
}
