<?php

use Illuminate\Database\Seeder;

use App\Models\AdmissionProgram;

class AdmissionProgramSeeder extends Seeder
{
    private $prgms = [
        ["label" => "Administration "                                                                                         ,"labelEng"=>"Administration"],//1
        ["label" => "Comptabilité"                                                                                            ,"labelEng"=>"Comptability"],//2
        ["label" => "Dessin industriel, Mécanique automobile, Électricité, soudage-montage"                                   ,"labelEng"=>"Industrial design, automobile mechanics, electricity, welding-assembly"],//3
        ["label" => "Electronique industrielle"                                                                               ,"labelEng"=>"Industrial Electronics"],//4
        ["label" => "Finance"                                                                                                 ,"labelEng"=>"Finance"],//5
        ["label" => "Gestion de réseaux"                                                                                      ,"labelEng"=>"Network management"],//6
        ["label" => "Informatique"                                                                                            ,"labelEng"=>"Computer Science"],//7
        ["label" => "Informatique de gestion"                                                                                 ,"labelEng"=>"Management IT"],//8
        ["label" => "Ingénierie"                                                                                              ,"labelEng"=>"Engineering"],//9
        ["label" => "Mécanique"                                                                                               ,"labelEng"=>"Mechanical"],//10
        ["label" => "Santé"                                                                                                   ,"labelEng"=>"Health"],//11
        ["label" => "Soudage"                                                                                                 ,"labelEng"=>"Welding"],//12
        ["label" => "Soutien informatique"                                                                                    ,"labelEng"=>"Computer support"],//13
        ["label" => "Télécommunication"                                                                                       ,"labelEng"=>"Telecommunication"],//14
        ["label" => "Télécommunications, Gestion de réseaux informatiques, Informatique de gestion, Electronique industrielle","labelEng"=>"Telecommunications, Computer network management, Management IT, Industrial electronics"],//15
        ["label" => "TI"                                                                                                      ,"labelEng"=>"Internet Technology (IT)"],//16
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->prgms as $prgm) {
            AdmissionProgram::create($prgm);
        }
    }
}
