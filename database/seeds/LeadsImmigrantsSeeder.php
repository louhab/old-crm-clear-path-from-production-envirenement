<?php

use Illuminate\Database\Seeder;

class LeadsImmigrantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i=1;$i < 100; $i++) {
            \App\Models\Lead::create([
                'id' => $i + 99,
                'program_id' => 2,
                'ranking_id' => random_int(1, 3),
                'country_id' => random_int(1, 241),
                'professional_domain_id' => random_int(1, 3),
                'english_level_id' => random_int(1, 5),
                'professional_experience' => random_int(1, 20),
                'firstname' => $faker->firstName,
                'lastname' => $faker->lastName,
                'email' => $faker->email,
                'birthday' => $faker->date(),
                'adresse_line_1' => $faker->address,
                'postal_code' => $faker->postcode,
                'budget' => random_int(7000, 11000)
            ]);
        }
    }
}
