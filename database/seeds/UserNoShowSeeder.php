<?php

use App\Models\Lead;
use App\Models\LeadStateLog;
use App\Models\Customer;
use Illuminate\Database\Seeder;

class UserNoShowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $advs = [26, 49, 57, 70, 69, 78, 91, 92, 3, 105, 104, 113, 115, 117, 135, 140, 141, 142, 143, 157];
        // $advs = [26, 49, 57, 70];
        $json = file_get_contents(__DIR__ . '/leads.json');

        $json_data = json_decode($json, true);

        $i = 0;
        foreach ($json_data as $key => $value) {

            if ($i >= count($advs)) $i = 0;

            // echo $value['id'] . "\n";
            // echo $advs[$i] . "\n";

            $lead = Lead::find($value['id']);
            $lead->lead_status_id = 9;
            $lead->conseiller_id = $advs[$i];
            $lead->save();

            $i++;
        }
    }
}
