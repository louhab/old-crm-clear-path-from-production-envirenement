<?php

use Conner\Tagging\TaggingUtility;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use \App\Models\DocumentType;

class DocumentTypesSeeder extends Seeder
{
    private $documents = [
        [
            "name_fr" => "CV",
            "name_en" => "CV",
            "signable" => 0,
            "customer_owner" => 1,
            "visible_for_customer" => 1,
            "steps" => [
                [
                    4 => ['program_id' => 1, 'order' => 1],
                ],
                [
                    4 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    4 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    4 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 1, 'order' => 5],
                ],
                [
                    6 => ['program_id' => 4, 'order' => 5],
                ],
            ],
            "sheets" => [
                [
                    // 1 => ['program_id' => 4, 'order' => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Cin recto",
            "name_en" => "ID - Front",
            "signable" => 0,
            "customer_owner" => 1,
            "visible_for_customer" => 1,
            "steps" => [
                [
                    6 => ['program_id' => 1, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 4, 'order' => 1],
                ],
            ],
        ],
        [
            "name_fr" => "Cin verso",
            "name_en" => "ID - Back",
            "signable" => 0,
            "customer_owner" => 1,
            "visible_for_customer" => 1,
            "steps" => [
                [
                    6 => ['program_id' => 1, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 4, 'order' => 3],
                ],
            ],
        ],
        [
            "name_fr" => "Clear1001 – Conseil",
            "name_en" => "Clear1001 – Advice",
            "signable" => 0,
            "customer_owner" => 0,
            "visible_for_customer" => 1,
            "steps" => [
                [
                    6 => ['program_id' => 4, "order" => 1],
                ],
            ],
            "sheets" => [
                [
                    //1 => ['program_id' => 4, 'order' => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Clear1004 - Rapport Scoring",
            "name_en" => "Clear1004 - Scoring report",
            "signable" => 0,
            "customer_owner" => 0,
            "visible_for_customer" => 1,
            "steps" => [
                [
                    6 => ['program_id' => 1, "order" => 1],
                ],
                [
                    6 => ['program_id' => 2, "order" => 1],
                ],
                [
                    6 => ['program_id' => 3, "order" => 1],
                ],
                [
                    6 => ['program_id' => 5, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 6, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 7, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 8, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 9, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 10, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 11, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 12, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 13, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 14, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 15, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 16, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 17, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 18, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 19, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 20, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 21, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 22, 'order' => 1],
                ],
                [
                    6 => ['program_id' => 23, 'order' => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Devis",
            "name_en" => "Devis",
            "signable" => 0,
            "customer_owner" => 0,
            "visible_for_customer" => 1,
            "steps" => [
                [
                    6 => ['program_id' => 1, "order" => 2],
                ],
                [
                    6 => ['program_id' => 2, "order" => 2],
                ],
                [
                    6 => ['program_id' => 3, "order" => 2],
                ],
                [
                    6 => ['program_id' => 4, "order" => 2],
                ],
                [
                    6 => ['program_id' => 5, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 6, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 7, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 8, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 9, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 10, 'order' =>2],
                ],
                [
                    6 => ['program_id' => 11, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 12, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 13, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 14, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 15, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 16, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 17, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 18, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 19, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 20, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 21, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 22, 'order' => 2],
                ],
                [
                    6 => ['program_id' => 23, 'order' => 2],
                ],
            ]
        ],
        [
            "name_fr" => "Contrat client",
            "name_en" => "Customer contract",
            "signable" => 1,
            "customer_owner" => 0,
            "steps" => [
                [
                    6 => ['program_id' => 1, "order" => 3],
                ],
                [
                    6 => ['program_id' => 2, "order" => 3],
                ],
                [
                    6 => ['program_id' => 3, "order" => 3],
                ],
                [
                    6 => ['program_id' => 4, "order" => 3],
                ],
                [
                    6 => ['program_id' => 5, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 6, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 7, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 8, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 9, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 10, 'order' =>3],
                ],
                [
                    6 => ['program_id' => 11, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 12, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 13, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 14, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 15, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 16, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 17, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 18, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 19, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 20, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 21, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 22, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 23, 'order' => 3],
                ],
            ]
        ],
        [
            "name_fr" => "Reçu",
            "name_en" => "Reçu",
            "signable" => 0,
            "customer_owner" => 0,
            "visible_for_customer" => 1,
            "steps" => [
                [
                    7 => ['program_id' => 1, "order" => 2],
                ],
                [
                    7 => ['program_id' => 2, "order" => 2],
                ],
                [
                    7 => ['program_id' => 3, "order" => 2],
                ],
                [
                    7 => ['program_id' => 4, "order" => 2],
                ],
                [
                    7 => ['program_id' => 5, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 6, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 7, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 8, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 9, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 10, 'order' =>2],
                ],
                [
                    7 => ['program_id' => 11, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 12, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 13, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 14, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 15, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 16, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 17, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 18, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 19, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 20, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 21, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 22, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 23, 'order' => 2],
                ],
            ]
        ],
        [
            "name_fr" => "Facture",
            "name_en" => "Facture",
            "signable" => 0,
            "customer_owner" => 0,
            "visible_for_customer" => 1,
            "steps" => [
                [
                    7 => ['program_id' => 1, "order" => 3],
                ],
                [
                    7 => ['program_id' => 2, "order" => 3],
                ],
                [
                    7 => ['program_id' => 3, "order" => 3],
                ],
                [
                    7 => ['program_id' => 4, "order" => 3],
                ],
                [
                    7 => ['program_id' => 5, "order" => 3],
                ],
                [
                    7 => ['program_id' => 6, "order" => 3],
                ],
                [
                    7 => ['program_id' => 7, "order" => 3],
                ],
                [
                    7 => ['program_id' => 8, "order" => 3],
                ],
                [
                    7 => ['program_id' => 9, "order" => 3],
                ],
                [
                    7 => ['program_id' => 10, "order" => 3],
                ],
                [
                    7 => ['program_id' => 11, "order" => 3],
                ],
                [
                    7 => ['program_id' => 12, "order" => 3],
                ],
                [
                    7 => ['program_id' => 13, "order" => 3],
                ],
                [
                    7 => ['program_id' => 14, "order" => 3],
                ],
                [
                    7 => ['program_id' => 15, "order" => 3],
                ],
                [
                    7 => ['program_id' => 16, "order" => 3],
                ],
                [
                    7 => ['program_id' => 17, "order" => 3],
                ],
                [
                    7 => ['program_id' => 18, "order" => 3],
                ],
                [
                    7 => ['program_id' => 19, "order" => 3],
                ],
                [
                    7 => ['program_id' => 20, "order" => 3],
                ],
                [
                    7 => ['program_id' => 21, "order" => 3],
                ],
                [
                    7 => ['program_id' => 22, "order" => 3],
                ],
                [
                    7 => ['program_id' => 23, "order" => 3],
                ],
            ]
        ],
        [
            "name_fr" => "Contrats pour les revenus locatifs du garant 1 (maisons, terrains, magasins)",
            "name_en" => "Contrats pour les revenus locatifs du garant 1 (maisons, terrains, magasins)",
            "signable" => 1,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 10],
                ],
                [
                    12 => ['program_id' => 2, "order" => 10],
                ],
                [
                    12 => ['program_id' => 3, "order" => 10],
                ],
                [
                    12 => ['program_id' => 4, "order" => 10],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 17],
                ],
            ]
        ],
        [
            "name_fr" => "Diplôme n1 et reléve de note",
            "name_en" => "Diplôme n1 et reléve de note",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    11 => ['program_id' => 1, "order" => 4],
                ],
                [
                    11 => ['program_id' => 2, "order" => 4],
                ],
                [
                    11 => ['program_id' => 3, "order" => 4],
                ],
                [
                    11 => ['program_id' => 4, "order" => 4],
                ],
                [
                    11 => ['program_id' => 5, "order" => 4],
                ],
                [
                    11 => ['program_id' => 6, "order" => 4],
                ],
                [
                    11 => ['program_id' => 7, "order" => 4],
                ],
                [
                    11 => ['program_id' => 8, "order" => 4],
                ],
                [
                    11 => ['program_id' => 9, "order" => 4],
                ],
                [
                    11 => ['program_id' => 10, "order" => 4],
                ],
                [
                    11 => ['program_id' => 11, "order" => 4],
                ],
                [
                    11 => ['program_id' => 12, "order" => 4],
                ],
                [
                    11 => ['program_id' => 13, "order" => 4],
                ],
                [
                    11 => ['program_id' => 14, "order" => 4],
                ],
                [
                    11 => ['program_id' => 15, "order" => 4],
                ],
                [
                    11 => ['program_id' => 16, "order" => 4],
                ],
                [
                    11 => ['program_id' => 17, "order" => 4],
                ],
                [
                    11 => ['program_id' => 18, "order" => 4],
                ],
                [
                    11 => ['program_id' => 19, "order" => 4],
                ],
                [
                    11 => ['program_id' => 20, "order" => 4],
                ],
                [
                    11 => ['program_id' => 21, "order" => 4],
                ],
                [
                    11 => ['program_id' => 22, "order" => 4],
                ],
                [
                    11 => ['program_id' => 23, "order" => 4],
                ],
            ],
            "sheets" => [
                [
                    // 1 => ['program_id' => 4, 'order' => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Diplôme n2 et reléve de note",
            "name_en" => "Diplôme n2 et reléve de note",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    11 => ['program_id' => 1, "order" => 5],
                ],
                [
                    11 => ['program_id' => 2, "order" => 5],
                ],
                [
                    11 => ['program_id' => 3, "order" => 5],
                ],
                [
                    11 => ['program_id' => 4, "order" => 5],
                ],
                [
                    11 => ['program_id' => 5, "order" =>5],
                ],
                [
                    11 => ['program_id' => 6, "order" =>5],
                ],
                [
                    11 => ['program_id' => 7, "order" =>5],
                ],
                [
                    11 => ['program_id' => 8, "order" =>5],
                ],
                [
                    11 => ['program_id' => 9, "order" =>5],
                ],
                [
                    11 => ['program_id' => 10, "order" =>5],
                ],
                [
                    11 => ['program_id' => 11, "order" =>5],
                ],
                [
                    11 => ['program_id' => 12, "order" =>5],
                ],
                [
                    11 => ['program_id' => 13, "order" =>5],
                ],
                [
                    11 => ['program_id' => 14, "order" =>5],
                ],
                [
                    11 => ['program_id' => 15, "order" =>5],
                ],
                [
                    11 => ['program_id' => 16, "order" =>5],
                ],
                [
                    11 => ['program_id' => 17, "order" =>5],
                ],
                [
                    11 => ['program_id' => 18, "order" =>5],
                ],
                [
                    11 => ['program_id' => 19, "order" =>5],
                ],
                [
                    11 => ['program_id' => 20, "order" =>5],
                ],
                [
                    11 => ['program_id' => 21, "order" =>5],
                ],
                [
                    11 => ['program_id' => 22, "order" =>5],
                ],
                [
                    11 => ['program_id' => 23, "order" =>5],
                ],
            ],
            "sheets" => [
                [
                    // 1 => ['program_id' => 4, 'order' => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Imm5476 formulaire à signer",
            "name_en" => "Imm5476 formulaire à signer",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    22 => ['program_id' => 1, "order" => 3],
                ],
                [
                    22 => ['program_id' => 2, "order" => 3],
                ],
                [
                    22 => ['program_id' => 3, "order" => 3],
                ],
                [
                    22 => ['program_id' => 4, "order" => 3],
                ],
                [
                    22 => ['program_id' => 5, "order" => 3],
                ],
            ],
            "sheets" => [
                /*[
                    5 => ['program_id' => 4, 'order' => 9],
                ],*/
            ]
        ],
        [
            "name_fr" => "Imm5476 formulaire signée",
            "name_en" => "Imm5476 formulaire signée",
            "signable" => 0,
            "customer_owner" => 1,
            "steps" => [
                [
                    22 => ['program_id' => 4, "order" => 3],
                ],
            ],
        ],
        [
            "name_fr" => "Attestation de travail du garant 1",
            "name_en" => "Attestation de travail du garant 1",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 5],
                ],
                [
                    12 => ['program_id' => 2, "order" => 5],
                ],
                [
                    12 => ['program_id' => 3, "order" => 5],
                ],
                [
                    12 => ['program_id' => 4, "order" => 5],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 5],
                ],
            ]
        ],
        [
            "name_fr" => "Attestation de scolarité",
            "name_en" => "Attestation de scolarité",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    11 => ['program_id' => 1, "order" => 2],
                ],
                [
                    11 => ['program_id' => 2, "order" => 2],
                ],
                [
                    11 => ['program_id' => 3, "order" => 2],
                ],
                [
                    11 => ['program_id' => 4, "order" => 2],
                ],
                [
                    11 => ['program_id' => 5, "order" =>2],
                ],
            ],
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 3],
                ],
            ]
        ],
        [
            "name_fr" => "Photographie",
            "name_en" => "Photographie",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    11 => ['program_id' => 1, "order" => 3],
                ],
                [
                    11 => ['program_id' => 2, "order" => 3],
                ],
                [
                    11 => ['program_id' => 3, "order" => 3],
                ],
                [
                    11 => ['program_id' => 4, "order" => 3],
                ],
            ],
            "sheets" => [
                [
                    5 => ['program_id' => 4, 'order' => 5],
                ],
            ]
        ],
        [
            "name_fr" => "Certificat de lien de parenté",
            "name_en" => "Certificat de lien de parenté",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 1],
                ],
                [
                    12 => ['program_id' => 2, "order" => 1],
                ],
                [
                    12 => ['program_id' => 3, "order" => 1],
                ],
                [
                    12 => ['program_id' => 4, "order" => 1],
                ],
                [
                    12 => ['program_id' => 5, "order" => 1],
                ],
                [
                    12 => ['program_id' => 6, "order" => 1],
                ],
                [
                    12 => ['program_id' => 7, "order" => 1],
                ],
                [
                    12 => ['program_id' => 8, "order" => 1],
                ],
                [
                    12 => ['program_id' => 9, "order" => 1],
                ],
                [
                    12 => ['program_id' => 10, "order" => 1],
                ],
                [
                    12 => ['program_id' => 11, "order" => 1],
                ],
                [
                    12 => ['program_id' => 12, "order" => 1],
                ],
                [
                    12 => ['program_id' => 13, "order" => 1],
                ],
                [
                    12 => ['program_id' => 14, "order" => 1],
                ],
                [
                    12 => ['program_id' => 15, "order" => 1],
                ],
                [
                    12 => ['program_id' => 16, "order" => 1],
                ],
                [
                    12 => ['program_id' => 17, "order" => 1],
                ],
                [
                    12 => ['program_id' => 18, "order" => 1],
                ],
                [
                    12 => ['program_id' => 19, "order" => 1],
                ],
                [
                    12 => ['program_id' => 20, "order" => 1],
                ],
                [
                    12 => ['program_id' => 21, "order" => 1],
                ],
                [
                    12 => ['program_id' => 22, "order" => 1],
                ],
                [
                    12 => ['program_id' => 23, "order" => 1],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Certificat de mariage",
            "name_en" => "Certificat de mariage",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 2],
                ],
                [
                    12 => ['program_id' => 2, "order" => 2],
                ],
                [
                    12 => ['program_id' => 3, "order" => 6],
                ],
                [
                    12 => ['program_id' => 4, "order" => 2],
                ],
                [
                    12 => ['program_id' => 5, "order" => 2],
                ],
                [
                    12 => ['program_id' => 6, "order" => 2],
                ],
                [
                    12 => ['program_id' => 7, "order" => 2],
                ],
                [
                    12 => ['program_id' => 8, "order" => 2],
                ],
                [
                    12 => ['program_id' => 9, "order" => 2],
                ],
                [
                    12 => ['program_id' => 10, "order" => 2],
                ],
                [
                    12 => ['program_id' => 11, "order" => 2],
                ],
                [
                    12 => ['program_id' => 12, "order" => 2],
                ],
                [
                    12 => ['program_id' => 13, "order" => 2],
                ],
                [
                    12 => ['program_id' => 14, "order" => 2],
                ],
                [
                    12 => ['program_id' => 15, "order" => 2],
                ],
                [
                    12 => ['program_id' => 16, "order" => 2],
                ],
                [
                    12 => ['program_id' => 17, "order" => 2],
                ],
                [
                    12 => ['program_id' => 18, "order" => 2],
                ],
                [
                    12 => ['program_id' => 19, "order" => 2],
                ],
                [
                    12 => ['program_id' => 20, "order" => 2],
                ],
                [
                    12 => ['program_id' => 21, "order" => 2],
                ],
                [
                    12 => ['program_id' => 22, "order" => 2],
                ],
                [
                    12 => ['program_id' => 23, "order" => 2],
                ],
                [
                    13 => ['program_id' => 1, "order" => 5],
                ],
                [
                    13 => ['program_id' => 2, "order" => 5],
                ],
                [
                    13 => ['program_id' => 5, "order" => 5],
                ],
                [
                    13 => ['program_id' => 6, "order" => 5],
                ],
                [
                    13 => ['program_id' => 7, "order" => 5],
                ],
                [
                    13 => ['program_id' => 8, "order" => 5],
                ],
                [
                    13 => ['program_id' => 9, "order" => 5],
                ],
                [
                    13 => ['program_id' => 10, "order" => 5],
                ],
                [
                    13 => ['program_id' => 11, "order" => 5],
                ],
                [
                    13 => ['program_id' => 12, "order" => 5],
                ],
                [
                    13 => ['program_id' => 13, "order" => 5],
                ],
                [
                    13 => ['program_id' => 14, "order" => 5],
                ],
                [
                    13 => ['program_id' => 15, "order" => 5],
                ],
                [
                    13 => ['program_id' => 16, "order" => 5],
                ],
                [
                    13 => ['program_id' => 17, "order" => 5],
                ],
                [
                    13 => ['program_id' => 18, "order" => 5],
                ],
                [
                    13 => ['program_id' => 19, "order" => 5],
                ],
                [
                    13 => ['program_id' => 20, "order" => 5],
                ],
                [
                    13 => ['program_id' => 21, "order" => 5],
                ],
                [
                    13 => ['program_id' => 22, "order" => 5],
                ],
                [
                    13 => ['program_id' => 23, "order" => 5],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 5],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 5],
                ],
                [
                    8 => ['program_id' => 3, 'order' => 6],
                ],
                [
                    2 => ['program_id' => 4, 'order' => 3],
                ],
            ]
        ],
        [
            "name_fr" => "Copie de la première page de passeport",
            "name_en" => "Copie de la première page de passeport",
            "signable" => 0,
            "customer_owner" => 1,
            "visible_for_customer" => 1,
            "steps" => [
                [
                    11 => ['program_id' => 1, "order" => 6],
                ],
                [
                    11 => ['program_id' => 2, "order" => 6],
                ],
                [
                    11 => ['program_id' => 3, "order" => 6],
                ],
                [
                    11 => ['program_id' => 4, "order" => 6],
                ],
                [
                    11 => ['program_id' => 5, "order" => 6],
                ],
                [
                    12 => ['program_id' => 3, "order" => 2],
                ],
                [
                    13 => ['program_id' => 1, "order" => 2],
                ],
                [
                    13 => ['program_id' => 2, "order" => 2],
                ],
                [
                    13 => ['program_id' => 5, "order" => 2],
                ],
                /*[
                    17 => ['program_id' => 1, "order" => 2],
                ],*/
                [
                    21 => ['program_id' => 1, "order" => 1],
                ],
                [
                    21 => ['program_id' => 2, "order" => 1],
                ],
                [
                    21 => ['program_id' => 3, "order" => 1],
                ],
                [
                    21 => ['program_id' => 4, "order" => 1],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 2],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 2],
                ],
                [
                    8 => ['program_id' => 3, 'order' => 2],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 32],
                ],
                [
                    4 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    10 => ['program_id' => 1, 'order' => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Extrait de naissance",
            "name_en" => "Extrait de naissance",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    11 => ['program_id' => 1, "order" => 1],
                ],
                [
                    11 => ['program_id' => 2, "order" => 1],
                ],
                [
                    11 => ['program_id' => 3, "order" => 1],
                ],
                [
                    11 => ['program_id' => 4, "order" => 1],
                ],
                [
                    11 => ['program_id' => 5, "order" => 1],
                ],
            ],
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Lettre de recommandation",
            "name_en" => "Lettre de recommandation",
            "signable" => 0,
            "is_multiple" => 1,
            "customer_owner" => 0,
            "steps" => [
                [
                    11 => ['program_id' => 1, "order" => 8],
                ],
                [
                    11 => ['program_id' => 2, "order" => 8],
                ],
                [
                    11 => ['program_id' => 3, "order" => 8],
                ],
                [
                    11 => ['program_id' => 4, "order" => 8],
                ],
                [
                    11 => ['program_id' => 5, "order" => 8],
                ],
                [
                    11 => ['program_id' => 6, "order" => 8],
                ],
                [
                    11 => ['program_id' => 7, "order" => 8],
                ],
                [
                    11 => ['program_id' => 8, "order" => 8],
                ],
                [
                    11 => ['program_id' => 9, "order" => 8],
                ],
                [
                    11 => ['program_id' => 10, "order" => 8],
                ],
                [
                    11 => ['program_id' => 11, "order" => 8],
                ],
                [
                    11 => ['program_id' => 12, "order" => 8],
                ],
                [
                    11 => ['program_id' => 13, "order" => 8],
                ],
                [
                    11 => ['program_id' => 14, "order" => 8],
                ],
                [
                    11 => ['program_id' => 15, "order" => 8],
                ],
                [
                    11 => ['program_id' => 16, "order" => 8],
                ],
                [
                    11 => ['program_id' => 17, "order" => 8],
                ],
                [
                    11 => ['program_id' => 18, "order" => 8],
                ],
                [
                    11 => ['program_id' => 19, "order" => 8],
                ],
                [
                    11 => ['program_id' => 20, "order" => 8],
                ],
                [
                    11 => ['program_id' => 21, "order" => 8],
                ],
                [
                    11 => ['program_id' => 22, "order" => 8],
                ],
                [
                    11 => ['program_id' => 23, "order" => 8],
                ],
            ],
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 81],
                ],
            ]
        ],
        [
            "name_fr" => "Lettre d'acceptation",
            "name_en" => "Lettre d'acceptation",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    17 => ['program_id' => 4, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Lettre d’acceptation de l'université",
            "name_en" => "Lettre d’acceptation de l'université",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    21 => ['program_id' => 1, "order" => 2],
                ],
                [
                    21 => ['program_id' => 2, "order" => 2],
                ],
                [
                    21 => ['program_id' => 3, "order" => 2],
                ],
                [
                    21 => ['program_id' => 4, "order" => 2],
                ],
                [
                    21 => ['program_id' => 5, "order" => 2],
                ],
                [
                    22 => ['program_id' => 1, "order" => 4],
                ],
                [
                    22 => ['program_id' => 2, "order" => 4],
                ],
                [
                    22 => ['program_id' => 3, "order" => 4],
                ],
                /*[
                    22 => ['program_id' => 4, "order" => 4],
                ],*/
                [
                    22 => ['program_id' => 5, "order" => 4],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 2],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 2],
                ],
                [
                    8 => ['program_id' => 3, 'order' => 2],
                ],
                [
                    // 4 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    5 => ['program_id' => 4, 'order' => 11],
                ],
            ]
        ],
        [
            "name_fr" => "Attestation de fréquentation scolaire",
            "name_en" => "Attestation de fréquentation scolaire",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    21 => ['program_id' => 1, "order" => 5],
                ],
                [
                    21 => ['program_id' => 2, "order" => 5],
                ],
                [
                    21 => ['program_id' => 3, "order" => 5],
                ],
                [
                    21 => ['program_id' => 4, "order" => 5],
                ],
                [
                    21 => ['program_id' => 5, "order" => 5],
                ],
            ],
            "sheets" => [
                [
                    4 => ['program_id' => 4, 'order' => 7],
                ],
            ]
        ],
        [
            "name_fr" => "Attestation d'assurance médicale valide",
            "name_en" => "Attestation d'assurance médicale valide",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    21 => ['program_id' => 1, "order" => 6],
                ],
                [
                    21 => ['program_id' => 2, "order" => 6],
                ],
                [
                    21 => ['program_id' => 3, "order" => 6],
                ],
                [
                    21 => ['program_id' => 4, "order" => 6],
                ],
                [
                    21 => ['program_id' => 5, "order" => 6],
                ],
            ],
            "sheets" => [
                [
                    4 => ['program_id' => 4, 'order' => 9],
                ],
            ]
        ],
        [
            "name_fr" => "Confirmation de la lettre d'acceptation",
            "name_en" => "Confirmation de la lettre d'acceptation",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    17 => ['program_id' => 4, "order" => 2],
                ],
            ]
        ],

        // [
        //     "name_fr" => "Réléve de note complemntaires",
        //     "name_en" => "Réléve de note complemntaires",
        //     "signable" => 0,
        //     "customer_owner" => 0,
        //     "steps" => [
        //         // [
        //         //     11 => ['program_id' => 4],
        //         // ],
        //     ]
        // ],

        [
            "name_fr" => "Déclaration Engagement et autorisation",
            "name_en" => "Déclaration Engagement et autorisation",
            "signable" => 1,
            "customer_owner" => 0,
            "steps" => [
                [
                    21 => ['program_id' => 1, "order" => 3],
                ],
                [
                    21 => ['program_id' => 2, "order" => 3],
                ],
                [
                    21 => ['program_id' => 3, "order" => 3],
                ],
                [
                    21 => ['program_id' => 4, "order" => 3],
                ],
                [
                    21 => ['program_id' => 5, "order" => 3],
                ],
            ],
            "sheets" => [
                [
                    // 4 => ['program_id' => 4, 'order' => 3],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé de notes",
            "name_en" => "Relevé de notes",
            "signable" => 1,
            "customer_owner" => 0,
            "steps" => [
                [
                    21 => ['program_id' => 1, "order" => 3],
                ],
                [
                    21 => ['program_id' => 2, "order" => 3],
                ],
                [
                    21 => ['program_id' => 3, "order" => 3],
                ],
                [
                    21 => ['program_id' => 4, "order" => 3],
                ],
                [
                    21 => ['program_id' => 5, "order" => 3],
                ],
            ],
            "sheets" => [
                [
                    4 => ['program_id' => 4, 'order' => 5],
                ],
            ]
        ],
        [
            "name_fr" => "Lettre de CAQ pour les études au Québec",
            "name_en" => "Lettre de CAQ pour les études au Québec",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    22 => ['program_id' => 1, "order" => 5],
                ],
                [
                    22 => ['program_id' => 2, "order" => 5],
                ],
                [
                    22 => ['program_id' => 3, "order" => 5],
                ],
                /*[
                    22 => ['program_id' => 4, "order" => 5],
                ],*/
            ],
            "sheets" => [
                [
                    5 => ['program_id' => 4, 'order' => 13],
                ],
            ]
        ],
        [
            "name_fr" => "Imm5645 formulaire à signer",
            "name_en" => "Imm5645 formulaire à signer",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    22 => ['program_id' => 1, "order" => 2],
                ],
                [
                    22 => ['program_id' => 2, "order" => 2],
                ],
                [
                    22 => ['program_id' => 3, "order" => 2],
                ],
                [
                    22 => ['program_id' => 4, "order" => 2],
                ],
                [
                    22 => ['program_id' => 5, "order" => 2],
                ],
            ],
            "sheets" => [
                /*[
                    5 => ['program_id' => 4, 'order' => 7],
                ],*/
            ]
        ],
        [
            "name_fr" => "Imm5645 formulaire signée",
            "name_en" => "Imm5645 formulaire signée",
            "signable" => 0,
            "customer_owner" => 1,
            "steps" => [
                [
                    22 => ['program_id' => 4, "order" => 2],
                ],
            ],
        ],
        [
            "name_fr" => "Imm5739 : Confirmation d'envoi de la demande de Visa et Permis d'études",
            "name_en" => "Imm5739 : Confirmation d'envoi de la demande de Visa et Permis d'études",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    29 => ['program_id' => 1, "order" => 1],
                ],
                [
                    29 => ['program_id' => 2, "order" => 1],
                ],
                [
                    29 => ['program_id' => 3, "order" => 1],
                ],
                [
                    29 => ['program_id' => 4, "order" => 1],
                ],
                [
                    29 => ['program_id' => 5, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Lettre de demande des documents additionnels",
            "name_en" => "Lettre de demande des documents additionnels",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    33 => ['program_id' => 1, "order" => 1],
                ],
                [
                    33 => ['program_id' => 2, "order" => 1],
                ],
                [
                    33 => ['program_id' => 3, "order" => 1],
                ],
                [
                    33 => ['program_id' => 4, "order" => 1],
                ],
                [
                    33 => ['program_id' => 5, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Imm5756 : Lettre d'instructions pour la collecte des données biométriques",
            "name_en" => "Imm5756 : Lettre d'instructions pour la collecte des données biométriques",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    32 => ['program_id' => 1, "order" => 1],
                ],
                [
                    32 => ['program_id' => 2, "order" => 1],
                ],
                [
                    32 => ['program_id' => 3, "order" => 1],
                ],
                [
                    32 => ['program_id' => 4, "order" => 1],
                ],
                [
                    32 => ['program_id' => 5, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Imm5825 : validité de vos renseignements biométriques",
            "name_en" => "Imm5825 : validité de vos renseignements biométriques",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    32 => ['program_id' => 1, "order" => 2],
                ],
                [
                    32 => ['program_id' => 2, "order" => 2],
                ],
                [
                    32 => ['program_id' => 3, "order" => 2],
                ],
                [
                    32 => ['program_id' => 4, "order" => 2],
                ],
                [
                    32 => ['program_id' => 5, "order" => 2],
                ],
            ]
        ],
        [
            "name_fr" => "Imm1017 : Directives relatives à l'examen médical",
            "name_en" => "Imm1017 : Directives relatives à l'examen médical",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    32 => ['program_id' => 1, "order" => 3],
                ],
                [
                    32 => ['program_id' => 2, "order" => 3],
                ],
                [
                    32 => ['program_id' => 3, "order" => 3],
                ],
                [
                    32 => ['program_id' => 4, "order" => 3],
                ],
                [
                    32 => ['program_id' => 5, "order" => 3],
                ],
            ]
        ],
        [
            "name_fr" => "Imm5740 : Demande de passeport original",
            "name_en" => "Imm5740 : Demande de passeport original",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    32 => ['program_id' => 1, "order" => 4],
                ],
                [
                    32 => ['program_id' => 2, "order" => 4],
                ],
                [
                    32 => ['program_id' => 3, "order" => 4],
                ],
                [
                    32 => ['program_id' => 4, "order" => 4],
                ],
                [
                    32 => ['program_id' => 5, "order" => 4],
                ],
            ]
        ],
        [
            "name_fr" => "Imm5813 : Résultats de la demande",
            "name_en" => "Imm5813 : Résultats de la demande",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    32 => ['program_id' => 1, "order" => 5],
                ],
                [
                    32 => ['program_id' => 2, "order" => 5],
                ],
                [
                    32 => ['program_id' => 3, "order" => 5],
                ],
                [
                    32 => ['program_id' => 4, "order" => 5],
                ],
                [
                    32 => ['program_id' => 5, "order" => 5],
                ],
            ]
        ],


        [
            "name_fr" => "Prise en charge à signer",
            "name_en" => "Prise en charge à signer",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    23 => ['program_id' => 1, "order" => 1],
                ],
                [
                    23 => ['program_id' => 2, "order" => 1],
                ],
                [
                    23 => ['program_id' => 3, "order" => 1],
                ],
                [
                    23 => ['program_id' => 4, "order" => 1],
                ],
                [
                    22 => ['program_id' => 4, "order" => 1],
                ],
            ],
            "sheets" => [
                /*[
                    2 => ['program_id' => 4, 'order' => 3],
                ],
                [
                    6 => ['program_id' => 4, 'order' => 1],
                ],*/
            ]
        ],
        [
            "name_fr" => "Prise en charge signée",
            "name_en" => "Prise en charge signée",
            "signable" => 0,
            "customer_owner" => 1,
            "steps" => [
                [
                    22 => ['program_id' => 4, "order" => 1],
                ],
            ],
        ],
        [
            "name_fr" => "Attestation de pension du garant 1",
            "name_en" => "Attestation de pension du garant 1",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 8],
                ],
                [
                    12 => ['program_id' => 2, "order" => 8],
                ],
                [
                    12 => ['program_id' => 3, "order" => 8],
                ],
                [
                    12 => ['program_id' => 4, "order" => 8],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 15],
                ],
            ]
        ],
        [
            "name_fr" => "Attestation de salaire du garant 1",
            "name_en" => "Attestation de salaire du garant 1",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 6],
                ],
                [
                    12 => ['program_id' => 2, "order" => 6],
                ],
                [
                    12 => ['program_id' => 3, "order" => 6],
                ],
                [
                    12 => ['program_id' => 4, "order" => 6],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 7],
                ],
            ]
        ],
        // blt_de_paie @ahaloua
        [
            "name_fr" => "Les quatre derniers bulletins de paies du garant - 1",
            "name_en" => "Les quatre derniers bulletins de paies du garant - 1",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 7],
                ],
                [
                    12 => ['program_id' => 2, "order" => 7],
                ],
                [
                    12 => ['program_id' => 3, "order" => 7],
                ],
                [
                    12 => ['program_id' => 4, "order" => 7],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 9],
                ],
            ]
        ],
        // to be changed (order from 23 to 9 just to keep files below each other)
        [
            "name_fr" => "Les quatre derniers bulletins de paies du garant - 2",
            "name_en" => "Les quatre derniers bulletins de paies du garant - 2",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 7],
                ],
                [
                    12 => ['program_id' => 2, "order" => 7],
                ],
                [
                    12 => ['program_id' => 3, "order" => 7],
                ],
                [
                    12 => ['program_id' => 4, "order" => 7],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 9],
                ],
            ]
        ],
        [
            "name_fr" => "Les quatre derniers bulletins de paies du garant - 3",
            "name_en" => "Les quatre derniers bulletins de paies du garant - 3",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 7],
                ],
                [
                    12 => ['program_id' => 2, "order" => 7],
                ],
                [
                    12 => ['program_id' => 3, "order" => 7],
                ],
                [
                    12 => ['program_id' => 4, "order" => 7],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 9],
                ],
            ]
        ],
        [
            "name_fr" => "Les quatre derniers bulletins de paies du garant - 4",
            "name_en" => "Les quatre derniers bulletins de paies du garant - 4",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 7],
                ],
                [
                    12 => ['program_id' => 2, "order" => 7],
                ],
                [
                    12 => ['program_id' => 3, "order" => 7],
                ],
                [
                    12 => ['program_id' => 4, "order" => 7],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 9],
                ],
            ]
        ],


        [//_________________
            "name_fr" => "3 derniers bulletins de paies du garant",
            "name_en" => "3 derniers bulletins de paies du garant",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    23 => ['program_id' => 1, "order" => 3],
                ],
                [
                    23 => ['program_id' => 2, "order" => 3],
                ],
                [
                    23 => ['program_id' => 3, "order" => 3],
                ],
                [
                    23 => ['program_id' => 4, "order" => 3],
                ],
                [
                    23 => ['program_id' => 5, "order" => 3],
                ],
            ],
            "sheets" => [
                [
                    // 6 => ['program_id' => 4, 'order' => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Rélévé bancaire du garant (3 derniers mois)",
            "name_en" => "Rélévé bancaire du garant (3 derniers mois)",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    23 => ['program_id' => 1, "order" => 2],
                ],
                [
                    23 => ['program_id' => 2, "order" => 2],
                ],
                [
                    23 => ['program_id' => 3, "order" => 2],
                ],
                [
                    23 => ['program_id' => 4, "order" => 2],
                ],
                [
                    23 => ['program_id' => 5, "order" => 2],
                ],
                [
                    23 => ['program_id' => 6, "order" => 2],
                ],
                [
                    23 => ['program_id' => 7, "order" => 2],
                ],
                [
                    23 => ['program_id' => 8, "order" => 2],
                ],
                [
                    23 => ['program_id' => 9, "order" => 2],
                ],
                [
                    23 => ['program_id' => 10, "order" => 2],
                ],
                [
                    23 => ['program_id' => 11, "order" => 2],
                ],
                [
                    23 => ['program_id' => 12, "order" => 2],
                ],
                [
                    23 => ['program_id' => 13, "order" => 2],
                ],
                [
                    23 => ['program_id' => 14, "order" => 2],
                ],
                [
                    23 => ['program_id' => 15, "order" => 2],
                ],
                [
                    23 => ['program_id' => 16, "order" => 2],
                ],
                [
                    23 => ['program_id' => 17, "order" => 2],
                ],
                [
                    23 => ['program_id' => 18, "order" => 2],
                ],
                [
                    23 => ['program_id' => 19, "order" => 2],
                ],
                [
                    23 => ['program_id' => 20, "order" => 2],
                ],
                [
                    23 => ['program_id' => 21, "order" => 2],
                ],
                [
                    23 => ['program_id' => 22, "order" => 2],
                ],
                [
                    23 => ['program_id' => 23, "order" => 2],
                ],
            ],
            "sheets" => [
                [
                    // 6 => ['program_id' => 4, 'order' => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Certificats de propriété du garant 1 (maisons, terrains, magasins)",
            "name_en" => "Certificats de propriété du garant 1 (maisons, terrains, magasins)",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 9],
                ],
                [
                    12 => ['program_id' => 2, "order" => 9],
                ],
                [
                    12 => ['program_id' => 3, "order" => 9],
                ],
                [
                    12 => ['program_id' => 4, "order" => 9],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 13],
                ],
            ]
        ],
        [
            "name_fr" => "Fiche anthropométrique",
            "name_en" => "Fiche anthropométrique",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    22 => ['program_id' => 1, "order" => 1],
                ],
                [
                    22 => ['program_id' => 2, "order" => 1],
                ],
                [
                    22 => ['program_id' => 3, "order" => 1],
                ],
                /*[
                    22 => ['program_id' => 4, "order" => 1],
                ],*/
                [
                    22 => ['program_id' => 5, "order" => 1],
                ],
                [
                    12 => ['program_id' => 3, "order" => 10],
                ],
                [
                    13 => ['program_id' => 1, "order" => 10],
                ],
                [
                    13 => ['program_id' => 2, "order" => 10],
                ],
                [
                    13 => ['program_id' => 5, "order" => 10],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 10],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 10],
                ],
                [
                    8 => ['program_id' => 3, 'order' => 10],
                ],
                [
                    9 => ['program_id' => 5, "order" => 10],
                ],
                [
                    5 => ['program_id' => 4, 'order' => 3],
                ],
            ]
        ],
        [
            "name_fr" => "Casier judiciaire",
            "name_en" => "Casier judiciaire",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    5 => ['program_id' => 4, 'order' => 5],
                ],
            ]
        ],
        // @ahaloua rb1_start
        [
            "name_fr" => "Relevé bancaire du garant 1 (12 mois) - 1",
            "name_en" => "Relevé bancaire du garant 1 (12 mois) - 1",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 4],
                ],
                [
                    12 => ['program_id' => 2, "order" => 4],
                ],
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
                [
                    12 => ['program_id' => 4, "order" => 4],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 11],
                ],
            ]
        ],
        [
            "name_fr" => "Clear1002 - Orientation",
            "name_en" => "Clear1002 - Orientation",
            "signable" => 0,
            "customer_owner" => 0,
            "visible_for_customer" => 1,
            "steps" => [
                [
                    14 => ['program_id' => 4, "order" => 1],
                ],
            ]
        ],
        //
        [
            "name_fr" => "Deux photos (format passeport)",
            "name_en" => "Deux photos (format passeport)",
            "signable" => 0,
            "customer_owner" => 0,
            "visible_for_customer" => 1,
            "steps" => [
                [
                    12 => ['program_id' => 3, "order" => 1],
                ],
                [
                    13 => ['program_id' => 1, "order" => 1],
                ],
                [
                    13 => ['program_id' => 2, "order" => 1],
                ],
                [
                    13 => ['program_id' => 5, "order" => 1],
                ],
                [
                    13 => ['program_id' => 6, "order" => 1],
                ],
                [
                    13 => ['program_id' => 7, "order" => 1],
                ],
                [
                    13 => ['program_id' => 8, "order" => 1],
                ],
                [
                    13 => ['program_id' => 9, "order" => 1],
                ],
                [
                    13 => ['program_id' => 10, "order" => 1],
                ],
                [
                    13 => ['program_id' => 11, "order" => 1],
                ],
                [
                    13 => ['program_id' => 12, "order" => 1],
                ],
                [
                    13 => ['program_id' => 13, "order" => 1],
                ],
                [
                    13 => ['program_id' => 14, "order" => 1],
                ],
                [
                    13 => ['program_id' => 15, "order" => 1],
                ],
                [
                    13 => ['program_id' => 16, "order" => 1],
                ],
                [
                    13 => ['program_id' => 17, "order" => 1],
                ],
                [
                    13 => ['program_id' => 18, "order" => 1],
                ],
                [
                    13 => ['program_id' => 19, "order" => 1],
                ],
                [
                    13 => ['program_id' => 20, "order" => 1],
                ],
                [
                    13 => ['program_id' => 21, "order" => 1],
                ],
                [
                    13 => ['program_id' => 22, "order" => 1],
                ],
                [
                    13 => ['program_id' => 23, "order" => 1],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 1],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    8 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    9 => ['program_id' => 5, "order" => 1],
                ],
                [
                    9 => ['program_id' => 6, "order" => 1],
                ],
                [
                    9 => ['program_id' => 7, "order" => 1],
                ],
                [
                    9 => ['program_id' => 8, "order" => 1],
                ],
                [
                    9 => ['program_id' => 9, "order" => 1],
                ],
                [
                    9 => ['program_id' => 10, "order" => 1],
                ],
                [
                    9 => ['program_id' => 11, "order" => 1],
                ],
                [
                    9 => ['program_id' => 12, "order" => 1],
                ],
                [
                    9 => ['program_id' => 13, "order" => 1],
                ],
                [
                    9 => ['program_id' => 14, "order" => 1],
                ],
                [
                    9 => ['program_id' => 15, "order" => 1],
                ],
                [
                    9 => ['program_id' => 16, "order" => 1],
                ],
                [
                    9 => ['program_id' => 17, "order" => 1],
                ],
                [
                    9 => ['program_id' => 18, "order" => 1],
                ],
                [
                    9 => ['program_id' => 19, "order" => 1],
                ],
                [
                    9 => ['program_id' => 20, "order" => 1],
                ],
                [
                    9 => ['program_id' => 21, "order" => 1],
                ],
                [
                    9 => ['program_id' => 22, "order" => 1],
                ],
                [
                    9 => ['program_id' => 23, "order" => 1],
                ],

            ]
        ],
        [
            "name_fr" => "Copie des visas et tous les timbres d'entrées",
            "name_en" => "Copie des visas et tous les timbres d'entrées",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    13 => ['program_id' => 1, "order" => 3],
                ],
                [
                    13 => ['program_id' => 2, "order" => 3],
                ],
                [
                    12 => ['program_id' => 3, "order" => 3],
                ],
                [
                    13 => ['program_id' => 5, "order" => 3],
                ],
                [
                    13 => ['program_id' => 6, "order" => 3],
                ],
                [
                    13 => ['program_id' => 7, "order" => 3],
                ],
                [
                    13 => ['program_id' => 8, "order" => 3],
                ],
                [
                    13 => ['program_id' => 9, "order" => 3],
                ],
                [
                    13 => ['program_id' => 10, "order" => 3],
                ],
                [
                    13 => ['program_id' => 11, "order" => 3],
                ],
                [
                    13 => ['program_id' => 12, "order" => 3],
                ],
                [
                    13 => ['program_id' => 13, "order" => 3],
                ],
                [
                    13 => ['program_id' => 14, "order" => 3],
                ],
                [
                    13 => ['program_id' => 15, "order" => 3],
                ],
                [
                    13 => ['program_id' => 16, "order" => 3],
                ],
                [
                    13 => ['program_id' => 17, "order" => 3],
                ],
                [
                    13 => ['program_id' => 18, "order" => 3],
                ],
                [
                    13 => ['program_id' => 19, "order" => 3],
                ],
                [
                    13 => ['program_id' => 20, "order" => 3],
                ],
                [
                    13 => ['program_id' => 21, "order" => 3],
                ],
                [
                    13 => ['program_id' => 22, "order" => 3],
                ],
                [
                    13 => ['program_id' => 23, "order" => 3],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 3],
                ],
                [
                    8 => ['program_id' => 3, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 5, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 6, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 7, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 8, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 9, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 10, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 11, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 12, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 13, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 14, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 15, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 16, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 17, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 18, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 19, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 20, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 21, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 22, 'order' => 3],
                ],
                [
                    9 => ['program_id' => 23, 'order' => 3],
                ],
            ]
        ],
        [
            "name_fr" => "Certificat de sélection du Québec (CSQ)",
            "name_en" => "Certificat de sélection du Québec (CSQ)",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
            ],
            "sheets" => [
                [
                    8 => ['program_id' => 3, 'order' => 4],
                ],
            ]
        ],
        [
            "name_fr" => "Certificat de naissance",
            "name_en" => "Certificat de naissance",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 3, "order" => 5],
                ],
                [
                    13 => ['program_id' => 1, "order" => 4],
                ],
                [
                    13 => ['program_id' => 2, "order" => 4],
                ],
                [
                    13 => ['program_id' => 5, "order" => 4],
                ],
                [
                    13 => ['program_id' => 6, "order" => 4],
                ],
                [
                    13 => ['program_id' => 7, "order" => 4],
                ],
                [
                    13 => ['program_id' => 8, "order" => 4],
                ],
                [
                    13 => ['program_id' => 9, "order" => 4],
                ],
                [
                    13 => ['program_id' => 10, "order" => 4],
                ],
                [
                    13 => ['program_id' => 11, "order" => 4],
                ],
                [
                    13 => ['program_id' => 12, "order" => 4],
                ],
                [
                    13 => ['program_id' => 13, "order" => 4],
                ],
                [
                    13 => ['program_id' => 14, "order" => 4],
                ],
                [
                    13 => ['program_id' => 15, "order" => 4],
                ],
                [
                    13 => ['program_id' => 16, "order" => 4],
                ],
                [
                    13 => ['program_id' => 17, "order" => 4],
                ],
                [
                    13 => ['program_id' => 18, "order" => 4],
                ],
                [
                    13 => ['program_id' => 19, "order" => 4],
                ],
                [
                    13 => ['program_id' => 20, "order" => 4],
                ],
                [
                    13 => ['program_id' => 21, "order" => 4],
                ],
                [
                    13 => ['program_id' => 22, "order" => 4],
                ],
                [
                    13 => ['program_id' => 23, "order" => 4],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 4],
                ],
                [
                    8 => ['program_id' => 3, 'order' => 5],
                ],
                [
                    9 => ['program_id' => 5, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 6, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 7, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 8, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 9, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 10, 'order' =>4],
                ],
                [
                    9 => ['program_id' => 11, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 12, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 13, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 14, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 15, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 16, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 17, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 18, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 19, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 20, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 21, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 22, 'order' => 4],
                ],
                [
                    9 => ['program_id' => 23, 'order' => 4],
                ],
            ]
        ],
        [
            "name_fr" => "Certificat de divorce",
            "name_en" => "Certificat de divorce",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 3, "order" => 7],
                ],
                [
                    13 => ['program_id' => 1, "order" => 6],
                ],
                [
                    13 => ['program_id' => 2, "order" => 6],
                ],
                [
                    13 => ['program_id' => 5, "order" => 6],
                ],
                [
                    13 => ['program_id' => 6, "order" => 6],
                ],
                [
                    13 => ['program_id' => 7, "order" => 6],
                ],
                [
                    13 => ['program_id' => 8, "order" => 6],
                ],
                [
                    13 => ['program_id' => 9, "order" => 6],
                ],
                [
                    13 => ['program_id' => 10, "order" =>6],
                ],
                [
                    13 => ['program_id' => 11, "order" => 6],
                ],
                [
                    13 => ['program_id' => 12, "order" => 6],
                ],
                [
                    13 => ['program_id' => 13, "order" => 6],
                ],
                [
                    13 => ['program_id' => 14, "order" => 6],
                ],
                [
                    13 => ['program_id' => 15, "order" => 6],
                ],
                [
                    13 => ['program_id' => 16, "order" => 6],
                ],
                [
                    13 => ['program_id' => 17, "order" => 6],
                ],
                [
                    13 => ['program_id' => 18, "order" => 6],
                ],
                [
                    13 => ['program_id' => 19, "order" => 6],
                ],
                [
                    13 => ['program_id' => 20, "order" => 6],
                ],
                [
                    13 => ['program_id' => 21, "order" => 6],
                ],
                [
                    13 => ['program_id' => 22, "order" => 6],
                ],
                [
                    13 => ['program_id' => 23, "order" => 6],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 6],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 6],
                ],
                [
                    8 => ['program_id' => 3, 'order' => 7],
                ],
                [
                    9 => ['program_id' => 5, "order" => 6],
                ],
                [
                    9 => ['program_id' => 6, "order" => 6],
                ],
                [
                    9 => ['program_id' => 7, "order" => 6],
                ],
                [
                    9 => ['program_id' => 8, "order" => 6],
                ],
                [
                    9 => ['program_id' => 9, "order" => 6],
                ],
                [
                    9 => ['program_id' => 10, "order" =>6],
                ],
                [
                    9 => ['program_id' => 11, "order" => 6],
                ],
                [
                    9 => ['program_id' => 12, "order" => 6],
                ],
                [
                    9 => ['program_id' => 13, "order" => 6],
                ],
                [
                    9 => ['program_id' => 14, "order" => 6],
                ],
                [
                    9 => ['program_id' => 15, "order" => 6],
                ],
                [
                    9 => ['program_id' => 16, "order" => 6],
                ],
                [
                    9 => ['program_id' => 17, "order" => 6],
                ],
                [
                    9 => ['program_id' => 18, "order" => 6],
                ],
                [
                    9 => ['program_id' => 19, "order" => 6],
                ],
                [
                    9 => ['program_id' => 20, "order" => 6],
                ],
                [
                    9 => ['program_id' => 21, "order" => 6],
                ],
                [
                    9 => ['program_id' => 22, "order" => 6],
                ],
                [
                    9 => ['program_id' => 23, "order" => 6],
                ],
            ]
        ],
        [
            "name_fr" => "Certificat de décès de conjoint",
            "name_en" => "Certificat de décès de conjoint",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 3, "order" => 8],
                ],
                [
                    13 => ['program_id' => 1, "order" => 7],
                ],
                [
                    13 => ['program_id' => 2, "order" => 7],
                ],
                [
                    13 => ['program_id' => 5, "order" => 7],
                ],
                [
                    13 => ['program_id' => 6, "order" => 7],
                ],
                [
                    13 => ['program_id' => 7, "order" => 7],
                ],
                [
                    13 => ['program_id' => 8, "order" => 7],
                ],
                [
                    13 => ['program_id' => 9, "order" => 7],
                ],
                [
                    13 => ['program_id' => 10, "order" =>7],
                ],
                [
                    13 => ['program_id' => 11, "order" => 7],
                ],
                [
                    13 => ['program_id' => 12, "order" => 7],
                ],
                [
                    13 => ['program_id' => 13, "order" => 7],
                ],
                [
                    13 => ['program_id' => 14, "order" => 7],
                ],
                [
                    13 => ['program_id' => 15, "order" => 7],
                ],
                [
                    13 => ['program_id' => 16, "order" => 7],
                ],
                [
                    13 => ['program_id' => 17, "order" => 7],
                ],
                [
                    13 => ['program_id' => 18, "order" => 7],
                ],
                [
                    13 => ['program_id' => 19, "order" => 7],
                ],
                [
                    13 => ['program_id' => 20, "order" => 7],
                ],
                [
                    13 => ['program_id' => 21, "order" => 7],
                ],
                [
                    13 => ['program_id' => 22, "order" => 7],
                ],
                [
                    13 => ['program_id' => 23, "order" => 7],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 7],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 7],
                ],
                [
                    8 => ['program_id' => 3, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 5, "order" => 7],
                ],
                [
                    9 => ['program_id' => 6, "order" => 7],
                ],
                [
                    9 => ['program_id' => 7, "order" => 7],
                ],
                [
                    9 => ['program_id' => 8, "order" => 7],
                ],
                [
                    9 => ['program_id' => 9, "order" => 7],
                ],
                [
                    9 => ['program_id' => 10, "order" => 7],
                ],
                [
                    9 => ['program_id' => 11, "order" => 7],
                ],
                [
                    9 => ['program_id' => 12, "order" => 7],
                ],
                [
                    9 => ['program_id' => 13, "order" => 7],
                ],
                [
                    9 => ['program_id' => 14, "order" => 7],
                ],
                [
                    9 => ['program_id' => 15, "order" => 7],
                ],
                [
                    9 => ['program_id' => 16, "order" => 7],
                ],
                [
                    9 => ['program_id' => 17, "order" => 7],
                ],
                [
                    9 => ['program_id' => 18, "order" => 7],
                ],
                [
                    9 => ['program_id' => 19, "order" => 7],
                ],
                [
                    9 => ['program_id' => 20, "order" => 7],
                ],
                [
                    9 => ['program_id' => 21, "order" => 7],
                ],
                [
                    9 => ['program_id' => 22, "order" => 7],
                ],
                [
                    9 => ['program_id' => 23, "order" => 7],
                ],
            ]
        ],
        [
            "name_fr" => "Copie de CIN recto et verso",
            "name_en" => "Copie de CIN recto et verso",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    13 => ['program_id' => 1, "order" => 9],
                ],
                [
                    13 => ['program_id' => 2, "order" => 9],
                ],
                [
                    12 => ['program_id' => 3, "order" => 9],
                ],
                [
                    13 => ['program_id' => 5, "order" => 9],
                ],
                [
                    13 => ['program_id' => 6, "order" => 9],
                ],
                [
                    13 => ['program_id' => 7, "order" => 9],
                ],
                [
                    13 => ['program_id' => 8, "order" => 9],
                ],
                [
                    13 => ['program_id' => 9, "order" => 9],
                ],
                [
                    13 => ['program_id' => 10, "order" =>9],
                ],
                [
                    13 => ['program_id' => 11, "order" => 9],
                ],
                [
                    13 => ['program_id' => 12, "order" => 9],
                ],
                [
                    13 => ['program_id' => 13, "order" => 9],
                ],
                [
                    13 => ['program_id' => 14, "order" => 9],
                ],
                [
                    13 => ['program_id' => 15, "order" => 9],
                ],
                [
                    13 => ['program_id' => 16, "order" => 9],
                ],
                [
                    13 => ['program_id' => 17, "order" => 9],
                ],
                [
                    13 => ['program_id' => 18, "order" => 9],
                ],
                [
                    13 => ['program_id' => 19, "order" => 9],
                ],
                [
                    13 => ['program_id' => 20, "order" => 9],
                ],
                [
                    13 => ['program_id' => 21, "order" => 9],
                ],
                [
                    13 => ['program_id' => 22, "order" => 9],
                ],
                [
                    13 => ['program_id' => 23, "order" => 9],
                ],
            ],
            "sheets" => [
                [
                    8 => ['program_id' => 3, 'order' => 9],
                ],
                [
                    9 => ['program_id' => 1, 'order' => 9],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 9],
                ],
                [
                    9 => ['program_id' => 5, "order" => 9],
                ],
                [
                    9 => ['program_id' => 6, "order" => 9],
                ],
                [
                    9 => ['program_id' => 7, "order" => 9],
                ],
                [
                    9 => ['program_id' => 8, "order" => 9],
                ],
                [
                    9 => ['program_id' => 9, "order" => 9],
                ],
                [
                    9 => ['program_id' => 10, "order" =>9],
                ],
                [
                    9 => ['program_id' => 11, "order" => 9],
                ],
                [
                    9 => ['program_id' => 12, "order" => 9],
                ],
                [
                    9 => ['program_id' => 13, "order" => 9],
                ],
                [
                    9 => ['program_id' => 14, "order" => 9],
                ],
                [
                    9 => ['program_id' => 15, "order" => 9],
                ],
                [
                    9 => ['program_id' => 16, "order" => 9],
                ],
                [
                    9 => ['program_id' => 17, "order" => 9],
                ],
                [
                    9 => ['program_id' => 18, "order" => 9],
                ],
                [
                    9 => ['program_id' => 19, "order" => 9],
                ],
                [
                    9 => ['program_id' => 20, "order" => 9],
                ],
                [
                    9 => ['program_id' => 21, "order" => 9],
                ],
                [
                    9 => ['program_id' => 22, "order" => 9],
                ],
                [
                    9 => ['program_id' => 23, "order" => 9],
                ],
            ]
        ],
        [
            "name_fr" => "Document de statut au Canada (Permis de travail ou Permis d'étude)",
            "name_en" => "Document de statut au Canada (Permis de travail ou Permis d'étude)",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 3, "order" => 11],
                ],
            ],
            "sheets" => [
                [
                    8 => ['program_id' => 3, 'order' => 11],
                ],
            ]
        ],
        //
        [
            "name_fr" => "Une copie certifiée de tous les diplômes par l'université ou l'école",
            "name_en" => "Une copie certifiée de tous les diplômes par l'université ou l'école",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    11 => ['program_id' => 1, "order" => 1],
                ],
                [
                    11 => ['program_id' => 2, "order" => 1],
                ],
                [
                    11 => ['program_id' => 5, "order" => 1],
                ],
                [
                    11 => ['program_id' => 6, "order" => 1],
                ],
                [
                    11 => ['program_id' => 7, "order" => 1],
                ],
                [
                    11 => ['program_id' => 8, "order" => 1],
                ],
                [
                    11 => ['program_id' => 9, "order" => 1],
                ],
                [
                    11 => ['program_id' => 10, "order" =>1],
                ],
                [
                    11 => ['program_id' => 11, "order" => 1],
                ],
                [
                    11 => ['program_id' => 12, "order" => 1],
                ],
                [
                    11 => ['program_id' =>13, "order" => 1],
                ],
                [
                    11 => ['program_id' => 14, "order" => 1],
                ],
                [
                    11 => ['program_id' => 15, "order" => 1],
                ],
                [
                    11 => ['program_id' => 16, "order" => 1],
                ],
                [
                    11 => ['program_id' => 17, "order" => 1],
                ],
                [
                    11 => ['program_id' => 18, "order" => 1],
                ],
                [
                    11 => ['program_id' => 19, "order" => 1],
                ],
                [
                    11 => ['program_id' => 20, "order" => 1],
                ],
                [
                    11 => ['program_id' => 21, "order" => 1],
                ],
                [
                    11 => ['program_id' => 22, "order" => 1],
                ],
                [
                    11 => ['program_id' => 23, "order" => 1],
                ],
            ],
            "sheets" => [
                [
                    7 => ['program_id' => 1, 'order' => 1],
                ],
                [
                    7 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    7 => ['program_id' => 5, "order" => 1],
                ],
                [
                    7 => ['program_id' => 6, "order" => 1],
                ],
                [
                    7 => ['program_id' => 7, "order" => 1],
                ],
                [
                    7 => ['program_id' => 8, "order" => 1],
                ],
                [
                    7 => ['program_id' => 9, "order" => 1],
                ],
                [
                    7 => ['program_id' => 10, "order" =>1],
                ],
                [
                    7 => ['program_id' => 11, "order" => 1],
                ],
                [
                    7 => ['program_id' => 12, "order" => 1],
                ],
                [
                    7 => ['program_id' =>13, "order" => 1],
                ],
                [
                    7 => ['program_id' => 14, "order" => 1],
                ],
                [
                    7 => ['program_id' => 15, "order" => 1],
                ],
                [
                    7 => ['program_id' => 16, "order" => 1],
                ],
                [
                    7 => ['program_id' => 17, "order" => 1],
                ],
                [
                    7 => ['program_id' => 18, "order" => 1],
                ],
                [
                    7 => ['program_id' => 19, "order" => 1],
                ],
                [
                    7 => ['program_id' => 20, "order" => 1],
                ],
                [
                    7 => ['program_id' => 21, "order" => 1],
                ],
                [
                    7 => ['program_id' => 22, "order" => 1],
                ],
                [
                    7 => ['program_id' => 23, "order" => 1],
                ],
            ]
        ],
        //
        [
            "name_fr" => "Des copies certifiées des relevés de notes de toutes les années d'études",
            "name_en" => "Des copies certifiées des relevés de notes de toutes les années d'études",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    11 => ['program_id' => 1, "order" => 2],
                ],
                [
                    11 => ['program_id' => 2, "order" => 2],
                ],
                [
                    11 => ['program_id' => 5, "order" => 2],
                ],
                [
                    11 => ['program_id' => 6, "order" => 2],
                ],
                [
                    11 => ['program_id' => 7, "order" => 2],
                ],
                [
                    11 => ['program_id' => 8, "order" => 2],
                ],
                [
                    11 => ['program_id' => 9, "order" => 2],
                ],
                [
                    11 => ['program_id' => 10, "order" =>2],
                ],
                [
                    11 => ['program_id' => 11, "order" => 2],
                ],
                [
                    11 => ['program_id' => 12, "order" => 2],
                ],
                [
                    11 => ['program_id' =>13, "order" => 2],
                ],
                [
                    11 => ['program_id' => 14, "order" => 2],
                ],
                [
                    11 => ['program_id' => 15, "order" => 2],
                ],
                [
                    11 => ['program_id' => 16, "order" => 2],
                ],
                [
                    11 => ['program_id' => 17, "order" => 2],
                ],
                [
                    11 => ['program_id' => 18, "order" => 2],
                ],
                [
                    11 => ['program_id' => 19, "order" => 2],
                ],
                [
                    11 => ['program_id' => 20, "order" => 2],
                ],
                [
                    11 => ['program_id' => 21, "order" => 2],
                ],
                [
                    11 => ['program_id' => 22, "order" => 2],
                ],
                [
                    11 => ['program_id' => 23, "order" => 2],
                ],
            ],
            "sheets" => [
                [
                    7 => ['program_id' => 1, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 2, 'order' => 2],
                ],
                [
                    7 => ['program_id' => 5, "order" => 2],
                ],
                [
                    7 => ['program_id' => 6, "order" => 2],
                ],
                [
                    7 => ['program_id' => 7, "order" => 2],
                ],
                [
                    7 => ['program_id' => 8, "order" => 2],
                ],
                [
                    7 => ['program_id' => 9, "order" => 2],
                ],
                [
                    7 => ['program_id' => 10, "order" =>2],
                ],
                [
                    7 => ['program_id' => 11, "order" => 2],
                ],
                [
                    7 => ['program_id' => 12, "order" => 2],
                ],
                [
                    7 => ['program_id' =>13, "order" => 2],
                ],
                [
                    7 => ['program_id' => 14, "order" => 2],
                ],
                [
                    7 => ['program_id' => 15, "order" => 2],
                ],
                [
                    7 => ['program_id' => 16, "order" => 2],
                ],
                [
                    7 => ['program_id' => 17, "order" => 2],
                ],
                [
                    7 => ['program_id' => 18, "order" => 2],
                ],
                [
                    7 => ['program_id' => 19, "order" => 2],
                ],
                [
                    7 => ['program_id' => 20, "order" => 2],
                ],
                [
                    7 => ['program_id' => 21, "order" => 2],
                ],
                [
                    7 => ['program_id' => 22, "order" => 2],
                ],
                [
                    7 => ['program_id' => 23, "order" => 2],
                ],
            ]
        ],
        //
        [
            "name_fr" => "une copie cachetée de votre dernier diplôme",
            "name_en" => "une copie cachetée de votre dernier diplôme",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    11 => ['program_id' => 1, "order" => 3],
                ],
                [
                    11 => ['program_id' => 2, "order" => 3],
                ],
                [
                    11 => ['program_id' => 5, "order" => 3],
                ],
                [
                    11 => ['program_id' => 6, "order" => 3],
                ],
                [
                    11 => ['program_id' => 7, "order" => 3],
                ],
                [
                    11 => ['program_id' => 8, "order" => 3],
                ],
                [
                    11 => ['program_id' => 9, "order" => 3],
                ],
                [
                    11 => ['program_id' => 10, "order" =>3],
                ],
                [
                    11 => ['program_id' => 11, "order" => 3],
                ],
                [
                    11 => ['program_id' => 12, "order" => 3],
                ],
                [
                    11 => ['program_id' =>13, "order" => 3],
                ],
                [
                    11 => ['program_id' => 14, "order" => 3],
                ],
                [
                    11 => ['program_id' => 15, "order" => 3],
                ],
                [
                    11 => ['program_id' => 16, "order" => 3],
                ],
                [
                    11 => ['program_id' => 17, "order" => 3],
                ],
                [
                    11 => ['program_id' => 18, "order" => 3],
                ],
                [
                    11 => ['program_id' => 19, "order" => 3],
                ],
                [
                    11 => ['program_id' => 20, "order" => 3],
                ],
                [
                    11 => ['program_id' => 21, "order" => 3],
                ],
                [
                    11 => ['program_id' => 22, "order" => 3],
                ],
                [
                    11 => ['program_id' => 23, "order" => 3],
                ],
            ],
            "sheets" => [
                [
                    7 => ['program_id' => 1, 'order' => 3],
                ],
                [
                    7 => ['program_id' => 2, 'order' => 3],
                ],
                [
                    7 => ['program_id' => 5, "order" => 3],
                ],
                [
                    7 => ['program_id' => 6, "order" => 3],
                ],
                [
                    7 => ['program_id' => 7, "order" => 3],
                ],
                [
                    7 => ['program_id' => 8, "order" => 3],
                ],
                [
                    7 => ['program_id' => 9, "order" => 3],
                ],
                [
                    7 => ['program_id' => 10, "order" =>3],
                ],
                [
                    7 => ['program_id' => 11, "order" => 3],
                ],
                [
                    7 => ['program_id' => 12, "order" => 3],
                ],
                [
                    7 => ['program_id' =>13, "order" =>3 ],
                ],
                [
                    7 => ['program_id' => 14, "order" => 3],
                ],
                [
                    7 => ['program_id' => 15, "order" => 3],
                ],
                [
                    7 => ['program_id' => 16, "order" => 3],
                ],
                [
                    7 => ['program_id' => 17, "order" => 3],
                ],
                [
                    7 => ['program_id' => 18, "order" => 3],
                ],
                [
                    7 => ['program_id' => 19, "order" => 3],
                ],
                [
                    7 => ['program_id' => 20, "order" => 3],
                ],
                [
                    7 => ['program_id' => 21, "order" => 3],
                ],
                [
                    7 => ['program_id' => 22, "order" => 3],
                ],
                [
                    7 => ['program_id' => 23, "order" => 3],
                ],
            ]
        ],
        //
        [
            "name_fr" => "vos relevés de notes certifiés de toutes les années étudiées pour obtenir votre dernier diplôme",
            "name_en" => "vos relevés de notes certifiés de toutes les années étudiées pour obtenir votre dernier diplôme",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    11 => ['program_id' => 1, "order" => 4],
                ],
                [
                    11 => ['program_id' => 2, "order" => 4],
                ],
                [
                    11 => ['program_id' => 5, "order" => 4],
                ],
                [
                    11 => ['program_id' => 6, "order" => 4],
                ],
                [
                    11 => ['program_id' => 7, "order" => 4],
                ],
                [
                    11 => ['program_id' => 8, "order" => 4],
                ],
                [
                    11 => ['program_id' => 9, "order" => 4],
                ],
                [
                    11 => ['program_id' => 10, "order" =>4],
                ],
                [
                    11 => ['program_id' => 11, "order" => 4],
                ],
                [
                    11 => ['program_id' => 12, "order" => 4],
                ],
                [
                    11 => ['program_id' =>13, "order" => 4],
                ],
                [
                    11 => ['program_id' => 14, "order" => 4],
                ],
                [
                    11 => ['program_id' => 15, "order" => 4],
                ],
                [
                    11 => ['program_id' => 16, "order" => 4],
                ],
                [
                    11 => ['program_id' => 17, "order" => 4],
                ],
                [
                    11 => ['program_id' => 18, "order" => 4],
                ],
                [
                    11 => ['program_id' => 19, "order" => 4],
                ],
                [
                    11 => ['program_id' => 20, "order" =>4],
                ],
                [
                    11 => ['program_id' => 21, "order" =>4],
                ],
                [
                    11 => ['program_id' => 22, "order" => 4],
                ],
                [
                    11 => ['program_id' => 23, "order" => 4],
                ],
            ],
            "sheets" => [
                [
                    7 => ['program_id' => 1, 'order' => 4],
                ],
                [
                    7 => ['program_id' => 2, 'order' => 4],
                ],
                [
                    7 => ['program_id' => 5, "order" => 4],
                ],
                [
                    7 => ['program_id' => 6, "order" => 4],
                ],
                [
                    7 => ['program_id' => 7, "order" => 4],
                ],
                [
                    7 => ['program_id' => 8, "order" => 4],
                ],
                [
                    7 => ['program_id' => 9, "order" => 4],
                ],
                [
                    7 => ['program_id' => 10, "order" =>4],
                ],
                [
                    7 => ['program_id' => 11, "order" => 4],
                ],
                [
                    7 => ['program_id' => 12, "order" => 4],
                ],
                [
                    7 => ['program_id' =>13, "order" =>4 ],
                ],
                [
                    7 => ['program_id' => 14, "order" => 4],
                ],
                [
                    7 => ['program_id' => 15, "order" => 4],
                ],
                [
                    7 => ['program_id' => 16, "order" =>4],
                ],
                [
                    7 => ['program_id' => 17, "order" => 4],
                ],
                [
                    7 => ['program_id' => 18, "order" => 4],
                ],
                [
                    7 => ['program_id' => 19, "order" => 4],
                ],
                [
                    7 => ['program_id' => 20, "order" => 4],
                ],
                [
                    7 => ['program_id' => 21, "order" => 4],
                ],
                [
                    7 => ['program_id' => 22, "order" => 4],
                ],
                [
                    7 => ['program_id' => 23, "order" => 4],
                ],
            ]
        ],
        //
        [
            "name_fr" => "Paiement des frais de l'équivalence",
            "name_en" => "Paiement des frais de l'équivalence",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    11 => ['program_id' => 1, "order" => 4],
                ],
                [
                    11 => ['program_id' => 2, "order" => 4],
                ],
                [
                    11 => ['program_id' => 5, "order" => 4],
                ],
                [
                    11 => ['program_id' => 6, "order" => 4],
                ],
                [
                    11 => ['program_id' => 7, "order" => 4],
                ],
                [
                    11 => ['program_id' => 8, "order" => 4],
                ],
                [
                    11 => ['program_id' => 9, "order" => 4],
                ],
                [
                    11 => ['program_id' => 10, "order" =>4],
                ],
                [
                    11 => ['program_id' => 11, "order" => 4],
                ],
                [
                    11 => ['program_id' => 12, "order" => 4],
                ],
                [
                    11 => ['program_id' =>13, "order" => 4],
                ],
                [
                    11 => ['program_id' => 14, "order" => 4],
                ],
                [
                    11 => ['program_id' => 15, "order" => 4],
                ],
                [
                    11 => ['program_id' => 16, "order" => 4],
                ],
                [
                    11 => ['program_id' => 17, "order" => 4],
                ],
                [
                    11 => ['program_id' => 18, "order" => 4],
                ],
                [
                    11 => ['program_id' => 19, "order" => 4],
                ],
                [
                    11 => ['program_id' => 20, "order" =>4],
                ],
                [
                    11 => ['program_id' => 21, "order" =>4],
                ],
                [
                    11 => ['program_id' => 22, "order" => 4],
                ],
                [
                    11 => ['program_id' => 23, "order" => 4],
                ],
            ],
            "sheets" => [
                [
                    7 => ['program_id' => 1, 'order' => 4],
                ],
                [
                    7 => ['program_id' => 2, 'order' => 4],
                ],
                [
                    7 => ['program_id' => 5, "order" => 4],
                ],
                [
                    7 => ['program_id' => 6, "order" => 4],
                ],
                [
                    7 => ['program_id' => 7, "order" => 4],
                ],
                [
                    7 => ['program_id' => 8, "order" => 4],
                ],
                [
                    7 => ['program_id' => 9, "order" => 4],
                ],
                [
                    7 => ['program_id' => 10, "order" =>4],
                ],
                [
                    7 => ['program_id' => 11, "order" => 4],
                ],
                [
                    7 => ['program_id' => 12, "order" => 4],
                ],
                [
                    7 => ['program_id' =>13, "order" =>4 ],
                ],
                [
                    7 => ['program_id' => 14, "order" => 4],
                ],
                [
                    7 => ['program_id' => 15, "order" => 4],
                ],
                [
                    7 => ['program_id' => 16, "order" =>4],
                ],
                [
                    7 => ['program_id' => 17, "order" => 4],
                ],
                [
                    7 => ['program_id' => 18, "order" => 4],
                ],
                [
                    7 => ['program_id' => 19, "order" => 4],
                ],
                [
                    7 => ['program_id' => 20, "order" => 4],
                ],
                [
                    7 => ['program_id' => 21, "order" => 4],
                ],
                [
                    7 => ['program_id' => 22, "order" => 4],
                ],
                [
                    7 => ['program_id' => 23, "order" => 4],
                ],
            ]
        ],
        // step 13 media 9
        [
            "name_fr" => "Certificat d’adoption",
            "name_en" => "Certificat d’adoption",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    13 => ['program_id' => 1, "order" => 8],
                ],
                [
                    13 => ['program_id' => 2, "order" => 8],
                ],
                [
                    13 => ['program_id' => 5, "order" => 8],
                ],
                [
                    13 => ['program_id' => 6, "order" => 8],
                ],
                [
                    13 => ['program_id' => 7, "order" => 8],
                ],
                [
                    13 => ['program_id' => 8, "order" => 8],
                ],
                [
                    13 => ['program_id' => 9, "order" => 8],
                ],
                [
                    13 => ['program_id' => 10, "order" => 8],
                ],
                [
                    13 => ['program_id' => 11, "order" => 8],
                ],
                [
                    13 => ['program_id' => 12, "order" => 8],
                ],
                [
                    13 => ['program_id' => 13, "order" => 8],
                ],
                [
                    13 => ['program_id' => 14, "order" => 8],
                ],
                [
                    13 => ['program_id' => 15, "order" => 8],
                ],
                [
                    13 => ['program_id' => 16, "order" => 8],
                ],
                [
                    13 => ['program_id' => 17, "order" => 8],
                ],
                [
                    13 => ['program_id' => 18, "order" => 8],
                ],
                [
                    13 => ['program_id' => 19, "order" => 8],
                ],
                [
                    13 => ['program_id' => 20, "order" => 8],
                ],
                [
                    13 => ['program_id' => 21, "order" => 8],
                ],
                [
                    13 => ['program_id' => 22, "order" => 8],
                ],
                [
                    13 => ['program_id' => 23, "order" => 8],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 5, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 6, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 7, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 8, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 9, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 10, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 11, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 12, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 13, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 14, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 15, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 16, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 17, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 18, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 19, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 20, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 21, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 22, 'order' => 8],
                ],
                [
                    9 => ['program_id' => 23, 'order' => 8],
                ],

            ]
        ],
        //
        [
            "name_fr" => "Resultat d'examen linguistique : TEF Canada, TCF Canada, IELTS",
            "name_en" => "Resultat d'examen linguistique : TEF Canada, TCF Canada, IELTS",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    13 => ['program_id' => 1, "order" => 11],
                ],
                [
                    13 => ['program_id' => 2, "order" => 11],
                ],
                [
                    13 => ['program_id' => 5, "order" => 11],
                ],
                [
                    13 => ['program_id' => 6, "order" => 11],
                ],
                [
                    13 => ['program_id' => 7, "order" => 11],
                ],
                [
                    13 => ['program_id' => 8, "order" => 11],
                ],
                [
                    13 => ['program_id' => 9, "order" => 11],
                ],
                [
                    13 => ['program_id' => 10, "order" => 11],
                ],
                [
                    13 => ['program_id' => 11, "order" => 11],
                ],
                [
                    13 => ['program_id' => 12, "order" => 11],
                ],
                [
                    13 => ['program_id' => 13, "order" => 11],
                ],
                [
                    13 => ['program_id' => 14, "order" => 11],
                ],
                [
                    13 => ['program_id' => 15, "order" => 11],
                ],
                [
                    13 => ['program_id' => 16, "order" => 11],
                ],
                [
                    13 => ['program_id' => 17, "order" => 11],
                ],
                [
                    13 => ['program_id' => 18, "order" => 11],
                ],
                [
                    13 => ['program_id' => 19, "order" => 11],
                ],
                [
                    13 => ['program_id' => 20, "order" => 11],
                ],
                [
                    13 => ['program_id' => 21, "order" => 11],
                ],
                [
                    13 => ['program_id' => 22, "order" => 11],
                ],
                [
                    13 => ['program_id' => 23, "order" => 11],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 5, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 6, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 7, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 8, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 9, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 10, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 11, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 12, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 13, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 14, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 15, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 16, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 17, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 18, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 19, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 20, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 21, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 22, 'order' => 11],
                ],
                [
                    9 => ['program_id' => 23, 'order' => 11],
                ],
            ]
        ],
        //
        [
            "name_fr" => "Preuves d’études au Canada ou une évaluation des diplômes d’études aux fins d’immigration",
            "name_en" => "Preuves d’études au Canada ou une évaluation des diplômes d’études aux fins d’immigration",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    13 => ['program_id' => 1, "order" => 12],
                ],
                [
                    13 => ['program_id' => 2, "order" => 12],
                ],
                [
                    13 => ['program_id' => 5, "order" => 12],
                ],
                [
                    13 => ['program_id' => 6, "order" => 12],
                ],
                [
                    13 => ['program_id' => 7, "order" => 12],
                ],
                [
                    13 => ['program_id' => 8, "order" => 12],
                ],
                [
                    13 => ['program_id' => 9, "order" => 12],
                ],
                [
                    13 => ['program_id' => 10, "order" => 12],
                ],
                [
                    13 => ['program_id' => 11, "order" => 12],
                ],
                [
                    13 => ['program_id' => 12, "order" => 12],
                ],
                [
                    13 => ['program_id' => 13, "order" => 12],
                ],
                [
                    13 => ['program_id' => 14, "order" => 12],
                ],
                [
                    13 => ['program_id' => 15, "order" => 12],
                ],
                [
                    13 => ['program_id' => 16, "order" => 12],
                ],
                [
                    13 => ['program_id' => 17, "order" => 12],
                ],
                [
                    13 => ['program_id' => 18, "order" => 12],
                ],
                [
                    13 => ['program_id' => 19, "order" => 12],
                ],
                [
                    13 => ['program_id' => 20, "order" => 12],
                ],
                [
                    13 => ['program_id' => 21, "order" => 12],
                ],
                [
                    13 => ['program_id' => 22, "order" => 12],
                ],
                [
                    13 => ['program_id' => 23, "order" => 12],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 5, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 6, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 7, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 8, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 9, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 10, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 11, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 12, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 13, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 14, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 15, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 16, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 17, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 18, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 19, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 20, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 21, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 22, 'order' => 12],
                ],
                [
                    9 => ['program_id' => 23, 'order' => 12],
                ],
            ]
        ],
        //
        [
            "name_fr" => "Désignation d’une province",
            "name_en" => "Désignation d’une province",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    13 => ['program_id' => 1, "order" => 13],
                ],
                [
                    13 => ['program_id' => 2, "order" => 13],
                ],
                [
                    13 => ['program_id' => 5, "order" => 13],
                ],
                [
                    13 => ['program_id' => 6, "order" => 13],
                ],
                [
                    13 => ['program_id' => 7, "order" => 13],
                ],
                [
                    13 => ['program_id' => 8, "order" => 13],
                ],
                [
                    13 => ['program_id' => 9, "order" => 13],
                ],
                [
                    13 => ['program_id' => 10, "order" => 13],
                ],
                [
                    13 => ['program_id' => 11, "order" => 13],
                ],
                [
                    13 => ['program_id' => 12, "order" => 13],
                ],
                [
                    13 => ['program_id' => 13, "order" => 13],
                ],
                [
                    13 => ['program_id' => 14, "order" => 13],
                ],
                [
                    13 => ['program_id' => 15, "order" => 13],
                ],
                [
                    13 => ['program_id' => 16, "order" => 13],
                ],
                [
                    13 => ['program_id' => 17, "order" => 13],
                ],
                [
                    13 => ['program_id' => 18, "order" => 13],
                ],
                [
                    13 => ['program_id' => 19, "order" => 13],
                ],
                [
                    13 => ['program_id' => 20, "order" => 13],
                ],
                [
                    13 => ['program_id' => 21, "order" => 13],
                ],
                [
                    13 => ['program_id' => 22, "order" => 13],
                ],
                [
                    13 => ['program_id' => 23, "order" => 13],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 5, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 6, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 7, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 8, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 9, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 10, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 11, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 12, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 13, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 14, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 15, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 16, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 17, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 18, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 19, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 20, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 21, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 22, 'order' => 13],
                ],
                [
                    9 => ['program_id' => 23, 'order' => 13],
                ],
            ]
        ],
        //
        [
            "name_fr" => "Offre d’emploi écrite d’un employeur au Canada",
            "name_en" => "Offre d’emploi écrite d’un employeur au Canada",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    13 => ['program_id' => 1, "order" => 14],
                ],
                [
                    13 => ['program_id' => 2, "order" => 14],
                ],
                [
                    13 => ['program_id' => 5, "order" => 14],
                ],
                [
                    13 => ['program_id' => 6, "order" => 14],
                ],
                [
                    13 => ['program_id' => 7, "order" => 14],
                ],
                [
                    13 => ['program_id' => 8, "order" => 14],
                ],
                [
                    13 => ['program_id' => 9, "order" => 14],
                ],
                [
                    13 => ['program_id' => 10, "order" => 14],
                ],
                [
                    13 => ['program_id' => 11, "order" => 14],
                ],
                [
                    13 => ['program_id' => 12, "order" => 14],
                ],
                [
                    13 => ['program_id' => 13, "order" => 14],
                ],
                [
                    13 => ['program_id' => 14, "order" => 14],
                ],
                [
                    13 => ['program_id' => 15, "order" => 14],
                ],
                [
                    13 => ['program_id' => 16, "order" => 14],
                ],
                [
                    13 => ['program_id' => 17, "order" => 14],
                ],
                [
                    13 => ['program_id' => 18, "order" => 14],
                ],
                [
                    13 => ['program_id' => 19, "order" => 14],
                ],
                [
                    13 => ['program_id' => 20, "order" => 14],
                ],
                [
                    13 => ['program_id' => 21, "order" => 14],
                ],
                [
                    13 => ['program_id' => 22, "order" => 14],
                ],
                [
                    13 => ['program_id' => 23, "order" => 14],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 5, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 6, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 7, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 8, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 9, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 10, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 11, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 12, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 13, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 14, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 15, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 16, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 17, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 18, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 19, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 20, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 21, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 22, 'order' => 14],
                ],
                [
                    9 => ['program_id' => 23, 'order' => 14],
                ],
            ]
        ],
        //
        [
            "name_fr" => "Preuve d’expérience de travail",
            "name_en" => "Preuve d’expérience de travail",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    13 => ['program_id' => 1, "order" => 15],
                ],
                [
                    13 => ['program_id' => 2, "order" => 15],
                ],
                [
                    13 => ['program_id' => 5, "order" => 15],
                ],
                [
                    13 => ['program_id' => 6, "order" => 15],
                ],
                [
                    13 => ['program_id' => 7, "order" => 15],
                ],
                [
                    13 => ['program_id' => 8, "order" => 15],
                ],
                [
                    13 => ['program_id' => 9, "order" => 15],
                ],
                [
                    13 => ['program_id' => 10, "order" => 15],
                ],
                [
                    13 => ['program_id' => 11, "order" => 15],
                ],
                [
                    13 => ['program_id' => 12, "order" => 15],
                ],
                [
                    13 => ['program_id' => 13, "order" => 15],
                ],
                [
                    13 => ['program_id' => 14, "order" => 15],
                ],
                [
                    13 => ['program_id' => 15, "order" => 15],
                ],
                [
                    13 => ['program_id' => 16, "order" => 15],
                ],
                [
                    13 => ['program_id' => 17, "order" => 15],
                ],
                [
                    13 => ['program_id' => 18, "order" => 15],
                ],
                [
                    13 => ['program_id' => 19, "order" => 15],
                ],
                [
                    13 => ['program_id' => 20, "order" => 15],
                ],
                [
                    13 => ['program_id' => 21, "order" => 15],
                ],
                [
                    13 => ['program_id' => 22, "order" => 15],
                ],
                [
                    13 => ['program_id' => 23, "order" => 15],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 5, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 6, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 7, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 8, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 9, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 10, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 11, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 12, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 13, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 14, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 15, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 16, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 17, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 18, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 19, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 20, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 21, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 22, 'order' => 15],
                ],
                [
                    9 => ['program_id' => 23, 'order' => 15],
                ],
            ]
        ],
        //
        [
            "name_fr" => "Certificat de qualification dans un métier spécialisé décerné au Canada par une province ou un territoire",
            "name_en" => "Certificat de qualification dans un métier spécialisé décerné au Canada par une province ou un territoire",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    13 => ['program_id' => 1, "order" => 16],
                ],
                [
                    13 => ['program_id' => 2, "order" => 16],
                ],
                [
                    13 => ['program_id' => 5, "order" => 16],
                ],
                [
                    13 => ['program_id' => 6, "order" => 16],
                ],
                [
                    13 => ['program_id' => 7, "order" => 16],
                ],
                [
                    13 => ['program_id' => 8, "order" => 16],
                ],
                [
                    13 => ['program_id' => 9, "order" => 16],
                ],
                [
                    13 => ['program_id' => 10, "order" => 16],
                ],
                [
                    13 => ['program_id' => 11, "order" => 16],
                ],
                [
                    13 => ['program_id' => 12, "order" => 16],
                ],
                [
                    13 => ['program_id' => 13, "order" => 16],
                ],
                [
                    13 => ['program_id' => 14, "order" => 16],
                ],
                [
                    13 => ['program_id' => 15, "order" => 16],
                ],
                [
                    13 => ['program_id' => 16, "order" => 16],
                ],
                [
                    13 => ['program_id' => 17, "order" => 16],
                ],
                [
                    13 => ['program_id' => 18, "order" => 16],
                ],
                [
                    13 => ['program_id' => 19, "order" => 16],
                ],
                [
                    13 => ['program_id' => 20, "order" => 16],
                ],
                [
                    13 => ['program_id' => 21, "order" => 16],
                ],
                [
                    13 => ['program_id' => 22, "order" => 16],
                ],
                [
                    13 => ['program_id' => 23, "order" => 16],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 5, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 6, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 7, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 8, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 9, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 10, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 11, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 12, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 13, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 14, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 15, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 16, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 17, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 18, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 19, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 20, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 21, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 22, 'order' => 16],
                ],
                [
                    9 => ['program_id' => 23, 'order' => 16],
                ],
            ]
        ],
        //
        [
            "name_fr" => "Examens médicaux",
            "name_en" => "Examens médicaux",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    13 => ['program_id' => 1, "order" => 17],
                ],
                [
                    13 => ['program_id' => 2, "order" => 17],
                ],
                [
                    13 => ['program_id' => 5, "order" => 17],
                ],
                [
                    13 => ['program_id' => 6, "order" => 17],
                ],
                [
                    13 => ['program_id' => 7, "order" => 17],
                ],
                [
                    13 => ['program_id' => 8, "order" => 17],
                ],
                [
                    13 => ['program_id' => 9, "order" => 17],
                ],
                [
                    13 => ['program_id' => 10, "order" => 17],
                ],
                [
                    13 => ['program_id' => 11, "order" => 17],
                ],
                [
                    13 => ['program_id' => 12, "order" => 17],
                ],
                [
                    13 => ['program_id' => 13, "order" => 17],
                ],
                [
                    13 => ['program_id' => 14, "order" => 17],
                ],
                [
                    13 => ['program_id' => 15, "order" => 17],
                ],
                [
                    13 => ['program_id' => 16, "order" => 17],
                ],
                [
                    13 => ['program_id' => 17, "order" => 17],
                ],
                [
                    13 => ['program_id' => 18, "order" => 17],
                ],
                [
                    13 => ['program_id' => 19, "order" => 17],
                ],
                [
                    13 => ['program_id' => 20, "order" => 17],
                ],
                [
                    13 => ['program_id' => 21, "order" => 17],
                ],
                [
                    13 => ['program_id' => 22, "order" => 17],
                ],
                [
                    13 => ['program_id' => 23, "order" => 17],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 17],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 17],
                ],
                [
                    9 => ['program_id' => 5, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 6, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 7, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 8, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 9, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 10, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 11, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 12, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 13, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 14, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 15, 'order' =>17],
                ],
                [
                    9 => ['program_id' =>16, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 17, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 18, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 19, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 20, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 21, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 22, 'order' =>17],
                ],
                [
                    9 => ['program_id' => 23, 'order' =>17],
                ],
            ]
        ],
        //
        [
            "name_fr" => "Preuve de fonds suffisants",
            "name_en" => "Preuve de fonds suffisants",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    13 => ['program_id' => 1, "order" => 18],
                ],
                [
                    13 => ['program_id' => 2, "order" => 18],
                ],
                [
                    13 => ['program_id' => 5, "order" => 18],
                ],
                [
                    13 => ['program_id' => 6, "order" => 18],
                ],
                [
                    13 => ['program_id' => 7, "order" => 18],
                ],
                [
                    13 => ['program_id' => 8, "order" => 18],
                ],
                [
                    13 => ['program_id' => 9, "order" => 18],
                ],
                [
                    13 => ['program_id' => 10, "order" => 18],
                ],
                [
                    13 => ['program_id' => 11, "order" => 18],
                ],
                [
                    13 => ['program_id' => 12, "order" => 18],
                ],
                [
                    13 => ['program_id' => 13, "order" => 18],
                ],
                [
                    13 => ['program_id' => 14, "order" => 18],
                ],
                [
                    13 => ['program_id' => 15, "order" => 18],
                ],
                [
                    13 => ['program_id' => 16, "order" => 18],
                ],
                [
                    13 => ['program_id' => 17, "order" => 18],
                ],
                [
                    13 => ['program_id' => 18, "order" => 18],
                ],
                [
                    13 => ['program_id' => 19, "order" => 18],
                ],
                [
                    13 => ['program_id' => 20, "order" => 18],
                ],
                [
                    13 => ['program_id' => 21, "order" => 18],
                ],
                [
                    13 => ['program_id' => 22, "order" => 18],
                ],
                [
                    13 => ['program_id' => 23, "order" => 18],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 18],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 18],
                ],
                [
                    9 => ['program_id' => 5, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 6, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 7, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 8, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 9, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 10, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 11, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 12, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 13, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 14, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 15, 'order' =>18],
                ],
                [
                    9 => ['program_id' =>16, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 17, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 18, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 19, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 20, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 21, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 22, 'order' =>18],
                ],
                [
                    9 => ['program_id' => 23, 'order' =>18],
                ],

            ]
        ],
        //
        [
            "name_fr" => "Preuve d’un lien avec un parent au Canada",
            "name_en" => "Preuve d’un lien avec un parent au Canada",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    13 => ['program_id' => 1, "order" => 19],
                ],
                [
                    13 => ['program_id' => 2, "order" => 19],
                ],
                [
                    13 => ['program_id' => 5, "order" => 19],
                ],
                [
                    13 => ['program_id' => 6, "order" => 19],
                ],
                [
                    13 => ['program_id' => 7, "order" => 19],
                ],
                [
                    13 => ['program_id' => 8, "order" => 19],
                ],
                [
                    13 => ['program_id' => 9, "order" => 19],
                ],
                [
                    13 => ['program_id' => 10, "order" => 19],
                ],
                [
                    13 => ['program_id' => 11, "order" => 19],
                ],
                [
                    13 => ['program_id' => 12, "order" => 19],
                ],
                [
                    13 => ['program_id' => 13, "order" => 19],
                ],
                [
                    13 => ['program_id' => 14, "order" => 19],
                ],
                [
                    13 => ['program_id' => 15, "order" => 19],
                ],
                [
                    13 => ['program_id' => 16, "order" => 19],
                ],
                [
                    13 => ['program_id' => 17, "order" => 19],
                ],
                [
                    13 => ['program_id' => 18, "order" => 19],
                ],
                [
                    13 => ['program_id' => 19, "order" => 19],
                ],
                [
                    13 => ['program_id' => 20, "order" => 19],
                ],
                [
                    13 => ['program_id' => 21, "order" => 19],
                ],
                [
                    13 => ['program_id' => 22, "order" => 19],
                ],
                [
                    13 => ['program_id' => 23, "order" => 19],
                ],
            ],
            "sheets" => [
                [
                    9 => ['program_id' => 1, 'order' => 19],
                ],
                [
                    9 => ['program_id' => 2, 'order' => 19],
                ],
                [
                    9 => ['program_id' => 5, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 6, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 7, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 8, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 9, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 10, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 11, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 12, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 13, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 14, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 15, 'order' =>19],
                ],
                [
                    9 => ['program_id' =>16, 'order' => 19],
                ],
                [
                    9 => ['program_id' => 17, 'order' => 19],
                ],
                [
                    9 => ['program_id' => 18, 'order' => 19],
                ],
                [
                    9 => ['program_id' => 19, 'order' => 19],
                ],
                [
                    9 => ['program_id' => 20, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 21, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 22, 'order' =>19],
                ],
                [
                    9 => ['program_id' => 23, 'order' =>19],
                ],
            ]
        ],
        // orientation
        // Relève note premiere année bac S1
        [
            "name_fr" => "Relevé de notes première année bac S1",
            "name_en" => "Relevé de notes première année bac S1",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 17],
                ],
            ]
        ],
        // Relève note premiere année bac S2
        [
            "name_fr" => "Relevé de notes première année bac S2",
            "name_en" => "Relevé de notes première année bac S2",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 19],
                ],
            ]
        ],
        // Relève note régional
        [
            "name_fr" => "Relevé de notes régional",
            "name_en" => "Relevé de notes régional",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 21],
                ],
            ]
        ],
        // Relève note deuxième année bac S1
        [
            "name_fr" => "Relevé de notes deuxième année bac S1",
            "name_en" => "Relevé de notes deuxième année bac S1",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 23],
                ],
            ]
        ],
        // Relève note deuxième année bac S2
        [
            "name_fr" => "Relevé de notes deuxième année bac S2",
            "name_en" => "Relevé de notes deuxième année bac S2",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 25],
                ],
            ]
        ],
        // Relève note premiere année S1
        [
            "name_fr" => "Relevé de notes première année licence S1",
            "name_en" => "Relevé de notes première année licence S1",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 41],
                ],
            ]
        ],
        // Relève note premiere année S2
        [
            "name_fr" => "Relevé de notes première année licence S2",
            "name_en" => "Relevé de notes première année licence S2",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 43],
                ],
            ]
        ],
        // Relève note deuxième année S1
        [
            "name_fr" => "Relevé de notes deuxième année S1",
            "name_en" => "Relevé de notes deuxième année S1",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 45],
                ],
            ]
        ],
        // Relève note deuxième année S2
        [
            "name_fr" => "Relevé de notes deuxième année S2",
            "name_en" => "Relevé de notes deuxième année S2",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 47],
                ],
            ]
        ],
        // Relève note troisième année S1
        [
            "name_fr" => "Relevé de notes troisième année S1",
            "name_en" => "Relevé de notes troisième année S1",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 49],
                ],
            ]
        ],
        // Relève note troisième année S2
        [
            "name_fr" => "Relevé de notes troisième année S2",
            "name_en" => "Relevé de notes troisième année S2",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 51],
                ],
            ]
        ],
        // Lettre de motivation
        [
            "name_fr" => "Lettre de motivation",
            "name_en" => "Lettre de motivation",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    // 1 => ['program_id' => 4, 'order' => 65],
                ],
            ]
        ],
        // Attestation de réussite
        [
            "name_fr" => "Attestation de réussite",
            "name_en" => "Attestation de réussite",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 26],
                ],
            ]
        ],
        // TCF Canada
        [
            "document_type_group_id" => 1,
            "name_fr" => "TCF Canada",
            "name_en" => "TCF Canada",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    10 => ['program_id' => 1, 'order' => 3],
                ],
            ]
        ],
        // TEF Canada
        [
            "document_type_group_id" => 1,
            "name_fr" => "TEF Canada",
            "name_en" => "TEF Canada",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    10 => ['program_id' => 1, 'order' => 5],
                ],
            ]
        ],
        // IELTS
        [
            "document_type_group_id" => 1,
            "name_fr" => "IELTS",
            "name_en" => "IELTS",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    10 => ['program_id' => 1, 'order' => 7],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 73],
                ],
            ]
        ],
        // Equivalence des diplômes (WES)
        [
            "document_type_group_id" => 1,
            "name_fr" => "Equivalence des diplômes (WES)",
            "name_en" => "Equivalence des diplômes (WES)",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    10 => ['program_id' => 1, 'order' => 9],
                ],
            ]
        ],
        // TCF Canada conjoint
        [
            "document_type_group_id" => 2,
            "name_fr" => "TCF Canada Conjoint",
            "name_en" => "TCF Canada Conjoint",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    10 => ['program_id' => 1, 'order' => 11],
                ],
            ]
        ],
        // TEF Canada
        [
            "document_type_group_id" => 2,
            "name_fr" => "TEF Canada Conjoint",
            "name_en" => "TEF Canada Conjoint",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    10 => ['program_id' => 1, 'order' => 11],
                ],
            ]
        ],
        // IELTS
        [
            "document_type_group_id" => 2,
            "name_fr" => "IELTS Conjoint",
            "name_en" => "IELTS Conjoint",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    10 => ['program_id' => 1, 'order' => 15],
                ],
            ]
        ],
        // Equivalence des diplômes (WES)
        [
            "document_type_group_id" => 2,
            "name_fr" => "Equivalence des diplômes (WES) Conjoint",
            "name_en" => "Equivalence des diplômes (WES) Conjoint",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    10 => ['program_id' => 1, 'order' => 17],
                ],
            ]
        ],
        // soumission docs entree express
        [
            "name_fr" => "IMM5786",
            "name_en" => "IMM5786",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    17 => ['program_id' => 1, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "IMM5787",
            "name_en" => "IMM5787",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    17 => ['program_id' => 1, "order" => 2],
                ],
            ]
        ],
        [
            "name_fr" => "IMM10002",
            "name_en" => "IMM10002",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    17 => ['program_id' => 1, "order" => 3],
                ],
            ]
        ],
        [
            "name_fr" => "IMM10003",
            "name_en" => "IMM10003",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    17 => ['program_id' => 1, "order" => 4],
                ],
            ]
        ],
        // wes
        [
            "name_fr" => "Accusé de réception WES",
            "name_en" => "Accusé de réception WES",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 1, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Bon d’envoi WES",
            "name_en" => "Bon d’envoi WES",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 1, "order" => 2],
                ],
            ]
        ],
        [
            "name_fr" => "Reçu WES",
            "name_en" => "Reçu WES",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                /*[
                    9 => ['program_id' => 1, "order" => 3],
                ],*/
            ]
        ],
        [
            "name_fr" => "Lettre WES",
            "name_en" => "Lettre WES",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 1, "order" => 4],
                ],
            ]
        ],
        // caq
        [
            "name_fr" => "Déclaration de CAQ à signer",
            "name_en" => "Déclaration de CAQ à signer",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 4, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Déclaration de CAQ signée",
            "name_en" => "Déclaration de CAQ signée",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 4, "order" => 2],
                ],
            ]
        ],
        [
            "name_fr" => "Reçu de CAQ",
            "name_en" => "Reçu de CAQ",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 4, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Lettre de CAQ",
            "name_en" => "Lettre de CAQ",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 4, "order" => 1],
                ],
            ]
        ],
        //
        [
            "name_fr" => "Niveau Bac",
            "name_en" => "Niveau Bac",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    1 => ['program_id' => 4, "order" => 5],
                ],
            ]
        ],
        [
            "name_fr" => "Diplôme du baccalauréat",
            "name_en" => "Diplôme du baccalauréat",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    1 => ['program_id' => 4, "order" => 15],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé de notes national",
            "name_en" => "Relevé de notes national",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 27],
                ],
            ]
        ],
        [
            "name_fr" => "Diplôme Bac+2 ou Attestation de réussite",
            "name_en" => "Diplôme Bac+2 ou Attestation de réussite",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    1 => ['program_id' => 4, "order" => 29],
                ],
            ]
        ],
        [
            "name_fr" => "Diplôme de licence ou Attestation de réussite",
            "name_en" => "Diplôme de licence ou Attestation de réussite",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    1 => ['program_id' => 4, "order" => 39],
                ],
            ]
        ],
        [
            "name_fr" => "Diplôme de Maîtrise ou Attestation de réussite",
            "name_en" => "Diplôme de Maîtrise ou Attestation de réussite",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    1 => ['program_id' => 4, "order" => 53],
                ],
            ]
        ],
        // niveau bac+2
        [
            "name_fr" => "Relevé de notes première année niveau Bac S1",
            "name_en" => "Relevé de notes première année niveau Bac S1",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 7],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé de notes première année niveau Bac S2",
            "name_en" => "Relevé de notes première année niveau Bac S2",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 9],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé de notes deuxième année niveau Bac S1",
            "name_en" => "Relevé de notes deuxième année niveau Bac S1",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 11],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé de notes deuxième année niveau Bac S2",
            "name_en" => "Relevé de notes deuxième année niveau Bac S2",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 13],
                ],
            ]
        ],
        //bac+2
        [
            "name_fr" => "Relevé de notes première année Bac+2 S1",
            "name_en" => "Relevé de notes première année Bac+2 S1",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 31],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé de notes première année Bac+2 S2",
            "name_en" => "Relevé de notes première année Bac+2 S2",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 33],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé de notes deuxième année Bac+2 S1",
            "name_en" => "Relevé de notes deuxième année Bac+2 S1",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 35],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé de notes deuxième année Bac+2 S2",
            "name_en" => "Relevé de notes deuxième année Bac+2 S2",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 37],
                ],
            ]
        ],
        // maîtrise
        [
            "name_fr" => "Relevé de notes première année maîtrise S1",
            "name_en" => "Relevé de notes première année maîtrise S1",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 55],
                ],
            ]
        ],
        // Relève note premiere année S2
        [
            "name_fr" => "Relevé de notes première année maîtrise S2",
            "name_en" => "Relevé de notes première année maîtrise S2",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 57],
                ],
            ]
        ],
        // Relève note deuxième année S1
        [
            "name_fr" => "Relevé de notes deuxième année maîtrise S1",
            "name_en" => "Relevé de notes deuxième année maîtrise S1",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 59],
                ],
            ]
        ],
        // Relève note deuxième année S2
        [
            "name_fr" => "Relevé de notes deuxième année maîtrise S2",
            "name_en" => "Relevé de notes deuxième année maîtrise S2",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 61],
                ],
            ]
        ],
        // clear2
        [
            "name_fr" => "Attestation de travail du garant 2",
            "name_en" => "Attestation de travail du garant 2",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 5],
                ],
                [
                    12 => ['program_id' => 2, "order" => 5],
                ],
                [
                    12 => ['program_id' => 3, "order" => 5],
                ],
                [
                    12 => ['program_id' => 4, "order" => 5],
                ],
            ],
            "sheets" => [
                /*[
                    2 => ['program_id' => 4, 'order' => 19],
                ],*/
            ]
        ],
        [
            "name_fr" => "Attestation de salaire du garant 2",
            "name_en" => "Attestation de salaire du garant 2",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 6],
                ],
                [
                    12 => ['program_id' => 2, "order" => 6],
                ],
                [
                    12 => ['program_id' => 3, "order" => 6],
                ],
                [
                    12 => ['program_id' => 4, "order" => 6],
                ],
            ],
            "sheets" => [
                /*[
                    2 => ['program_id' => 4, 'order' => 21],
                ],*/
            ]
        ],
        [
            "name_fr" => "Relevé bancaire du garant 2 (12 mois) - 1",
            "name_en" => "Relevé bancaire du garant 2 (12 mois) - 1",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 4],
                ],
                [
                    12 => ['program_id' => 2, "order" => 4],
                ],
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
                [
                    12 => ['program_id' => 4, "order" => 4],
                ],
            ],
            "sheets" => [
                /*[
                    2 => ['program_id' => 4, 'order' => 25],
                ],*/
            ]
        ],
        [
            "name_fr" => "Relevé bancaire du garant 2 (12 mois) - 2",
            "name_en" => "Relevé bancaire du garant 2 (12 mois) - 2",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 4],
                ],
                [
                    12 => ['program_id' => 2, "order" => 4],
                ],
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
                [
                    12 => ['program_id' => 4, "order" => 4],
                ],
            ],
            "sheets" => [
                /*[
                    2 => ['program_id' => 4, 'order' => 25],
                ],*/
            ]
        ],
        [
            "name_fr" => "Relevé bancaire du garant 2 (12 mois) - 3",
            "name_en" => "Relevé bancaire du garant 2 (12 mois) - 3",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 4],
                ],
                [
                    12 => ['program_id' => 2, "order" => 4],
                ],
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
                [
                    12 => ['program_id' => 4, "order" => 4],
                ],
            ],
            "sheets" => [
                /*[
                    2 => ['program_id' => 4, 'order' => 25],
                ],*/
            ]
        ],
        [
            "name_fr" => "Relevé bancaire du garant 2 (12 mois) - 4",
            "name_en" => "Relevé bancaire du garant 2 (12 mois) - 4",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 4],
                ],
                [
                    12 => ['program_id' => 2, "order" => 4],
                ],
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
                [
                    12 => ['program_id' => 4, "order" => 4],
                ],
            ],
            "sheets" => [
                /*[
                    2 => ['program_id' => 4, 'order' => 25],
                ],*/
            ]
        ],
        [
            "name_fr" => "Relevé bancaire du garant 2 (12 mois) - 5",
            "name_en" => "Relevé bancaire du garant 2 (12 mois) - 5",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 4],
                ],
                [
                    12 => ['program_id' => 2, "order" => 4],
                ],
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
                [
                    12 => ['program_id' => 4, "order" => 4],
                ],
            ],
            "sheets" => [
                /*[
                    2 => ['program_id' => 4, 'order' => 25],
                ],*/
            ]
        ],
        [
            "name_fr" => "Relevé bancaire du garant 2 (12 mois) - 6",
            "name_en" => "Relevé bancaire du garant 2 (12 mois) - 6",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 4],
                ],
                [
                    12 => ['program_id' => 2, "order" => 4],
                ],
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
                [
                    12 => ['program_id' => 4, "order" => 4],
                ],
            ],
            "sheets" => [
                /*[
                    2 => ['program_id' => 4, 'order' => 25],
                ],*/
            ]
        ],
        // @ahaloua rb2_end
        [
            "name_fr" => "Certificats de propriété du garant 2 (maisons, terrains, magasins)",
            "name_en" => "Certificats de propriété du garant 2 (maisons, terrains, magasins)",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 9],
                ],
                [
                    12 => ['program_id' => 2, "order" => 9],
                ],
                [
                    12 => ['program_id' => 3, "order" => 9],
                ],
                [
                    12 => ['program_id' => 4, "order" => 9],
                ],
            ],
            "sheets" => [
                /*[
                    2 => ['program_id' => 4, 'order' => 27],
                ],*/
            ]
        ],
        [
            "name_fr" => "Attestation de pension du garant 2",
            "name_en" => "Attestation de pension du garant 2",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 8],
                ],
                [
                    12 => ['program_id' => 2, "order" => 8],
                ],
                [
                    12 => ['program_id' => 3, "order" => 8],
                ],
                [
                    12 => ['program_id' => 4, "order" => 8],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 29],
                ],
            ]
        ],
        [
            "name_fr" => "Contrats pour les revenus locatifs du garant 2 (maisons, terrains, magasins)",
            "name_en" => "Contrats pour les revenus locatifs du garant 2 (maisons, terrains, magasins)",
            "signable" => 1,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 10],
                ],
                [
                    12 => ['program_id' => 2, "order" => 10],
                ],
                [
                    12 => ['program_id' => 3, "order" => 10],
                ],
                [
                    12 => ['program_id' => 4, "order" => 10],
                ],
            ],
            "sheets" => [
                /*[
                    2 => ['program_id' => 4, 'order' => 31],
                ],*/
            ]
        ],
        // Relevés bancaires des six derniers mois (entreprise)
        [
            "name_fr" => "Relevés bancaires des six derniers mois (entreprise)",
            "name_en" => "Relevés bancaires des six derniers mois (entreprise)",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 4, "order" => 8],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 33],
                ],
            ]
        ],
        // Statut de l’entreprise (entreprise)
        [
            "name_fr" => "Statut de l’entreprise (entreprise)",
            "name_en" => "Statut de l’entreprise (entreprise)",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 4, "order" => 8],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 35],
                ],
            ]
        ],
        // Déclaration d'impôt (entreprise)
        [
            "name_fr" => "Déclaration d'impôt (entreprise)",
            "name_en" => "Déclaration d'impôt (entreprise)",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 4, "order" => 8],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 37],
                ],
            ]
        ],
        // Registre de commerce (entreprise)
        [
            "name_fr" => "Registre de commerce (entreprise)",
            "name_en" => "Registre de commerce (entreprise)",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 4, "order" => 8],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 39],
                ],
            ]
        ],
        // clear1 test langue
        //TCF
        [
            "name_fr" => "TCF",
            "name_en" => "TCF",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 63],
                ],
            ]
        ],
        //TEF
        [
            "name_fr" => "TEF",
            "name_en" => "TEF",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 65],
                ],
            ]
        ],
        //TFI
        [
            "name_fr" => "TFI",
            "name_en" => "TFI",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 67],
                ],
            ]
        ],
        //DALF
        [
            "name_fr" => "DALF",
            "name_en" => "DALF",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 69],
                ],
            ]
        ],
        //DELF
        [
            "name_fr" => "DELF",
            "name_en" => "DELF",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 71],
                ],
            ]
        ],
        //IELTS
        //GMAT
        [
            "name_fr" => "GMAT",
            "name_en" => "GMAT",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 75],
                ],
            ]
        ],
        //TOFEL
        [
            "name_fr" => "TOFEL",
            "name_en" => "TOFEL",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 77],
                ],
            ]
        ],
        //TOEIC
        [
            "name_fr" => "TOEIC",
            "name_en" => "TOEIC",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 79],
                ],
            ]
        ],
        // Etud - R4 - CPG
        [
            "name_fr" => "CPG",
            "name_en" => "CPG",
            "signable" => 0,
            "customer_owner" => 0,
            /*"steps" => [
                [
                    9 => ['program_id' => 4, "order" => 20],
                ],
            ]*/
        ],
        [
            "name_fr" => "Facture scolaire",
            "name_en" => "Facture scolaire",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 4, "order" => 23],
                ],
            ]
        ],
        // add r1 task: Demande de note
        [
            "name_fr" => "Demande de note: Lettre de Refus",
            "name_en" => "Demande de note: Lettre de Refus",
            "signable" => 0,
            "customer_owner" => 1,
            "steps" => [
                [
                    8 => ['program_id' => 4, "order" => 1],
                ],
            ]
        ],
        [
            "name_fr" => "Demande de note à signer",
            "name_en" => "Demande de note à signer",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    8 => ['program_id' => 4, "order" => 3],
                ],
            ]
        ],
        [
            "name_fr" => "Demande de note signée",
            "name_en" => "Demande de note signée",
            "signable" => 0,
            "customer_owner" => 1,
            "steps" => [
                [
                    8 => ['program_id' => 4, "order" => 5],
                ],
            ]
        ],
        [
            "name_fr" => "Demande de note: Reçu",
            "name_en" => "Demande de note: Reçu",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    8 => ['program_id' => 4, "order" => 7],
                ],
            ]
        ],
        [
            "name_fr" => "Anciens visa canadien",
            "name_en" => "Anciens visa canadien",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 83],
                ],
            ]
        ],
        [
            "name_fr" => "Autre Visa",
            "name_en" => "Autre Visa",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 85],
                ],
            ]
        ],
        [
            "name_fr" => "Lettre de refus",
            "name_en" => "Lettre de refus",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 87],
                ],
            ]
        ],
        [
            "name_fr" => "Lettre Empreinte biométrique",
            "name_en" => "Lettre Empreinte biométrique",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    1 => ['program_id' => 4, 'order' => 89],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé bancaire du garant 1 (12 mois) - 2",
            "name_en" => "Relevé bancaire du garant 1 (12 mois) - 2",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 4],
                ],
                [
                    12 => ['program_id' => 2, "order" => 4],
                ],
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
                [
                    12 => ['program_id' => 4, "order" => 4],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 11],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé bancaire du garant 1 (12 mois) - 3",
            "name_en" => "Relevé bancaire du garant 1 (12 mois) - 3",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 4],
                ],
                [
                    12 => ['program_id' => 2, "order" => 4],
                ],
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
                [
                    12 => ['program_id' => 4, "order" => 4],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 11],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé bancaire du garant 1 (12 mois) - 4",
            "name_en" => "Relevé bancaire du garant 1 (12 mois) - 4",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 4],
                ],
                [
                    12 => ['program_id' => 2, "order" => 4],
                ],
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
                [
                    12 => ['program_id' => 4, "order" => 4],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 11],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé bancaire du garant 1 (12 mois) - 5",
            "name_en" => "Relevé bancaire du garant 1 (12 mois) - 5",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 4],
                ],
                [
                    12 => ['program_id' => 2, "order" => 4],
                ],
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
                [
                    12 => ['program_id' => 4, "order" => 4],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 11],
                ],
            ]
        ],
        [
            "name_fr" => "Relevé bancaire du garant 1 (12 mois) - 6",
            "name_en" => "Relevé bancaire du garant 1 (12 mois) - 6",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    12 => ['program_id' => 1, "order" => 4],
                ],
                [
                    12 => ['program_id' => 2, "order" => 4],
                ],
                [
                    12 => ['program_id' => 3, "order" => 4],
                ],
                [
                    12 => ['program_id' => 4, "order" => 4],
                ],
            ],
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 11],
                ],
            ]
        ],
        [
            "name_fr" => "Compte CPG",
            "name_en" => "Compte CPG",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 4, "order" => 20],
                ],
            ]
        ],
        [
            "name_fr" => "Recu Banque",
            "name_en" => "Recu Banque",
            "signable" => 0,
            "customer_owner" => 1,
            "steps" => [
                [
                    9 => ['program_id' => 4, "order" => 21],
                ],
            ]
        ],
        [
            "name_fr" => "Attestation de solde CPG",
            "name_en" => "Attestation de solde CPG",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    9 => ['program_id' => 4, "order" => 22],
                ],
            ]
        ],
        [
            "name_fr" => "Recu CIC",
            "name_en" => "Recu CIC",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    29 => ['program_id' => 4, "order" => 3],
                ],
            ]
        ],
        [
            "name_fr" => "Rendez-vous empreintes digitales",
            "name_en" => "Rendez-vous empreintes digitales",
            "signable" => 0,
            "customer_owner" => 0,
            "steps" => [
                [
                    32 => ['program_id' => 4, "order" => 0],
                ],
            ]
        ],
        // new seed
        [
            "name_fr" => "Pièce d'identité",
            "name_en" => "Pièce d'identité",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 100],
                ],
            ]
        ],
        // new seed
        // bultin de pension
        [
            "name_fr" => "Les quatre derniers bulletins de pension du garant - 1",
            "name_en" => "Les quatre derniers bulletins de pension du garant - 1",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 20],
                ],
            ]
        ],
        // to be changed (order from 23 to 9 just to keep files below each other)
        [
            "name_fr" => "Les quatre derniers bulletins de pension du garant - 2",
            "name_en" => "Les quatre derniers bulletins de pension du garant - 2",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 30],
                ],
            ]
        ],
        [
            "name_fr" => "Les quatre derniers bulletins de pension du garant - 3",
            "name_en" => "Les quatre derniers bulletins de pension du garant - 3",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 40],
                ],
            ]
        ],
        [
            "name_fr" => "Les quatre derniers bulletins de pension du garant - 4",
            "name_en" => "Les quatre derniers bulletins de pension du garant - 4",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 50],
                ],
            ]
        ],
        // new seed
        [
            "name_fr" => "Attestation ou carte de profession libérale ou carte de travailleur autonome",
            "name_en" => "Attestation ou carte de profession libérale ou carte de travailleur autonome",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 20],
                ],
            ]
        ],
        [
            "name_fr" => "Déclaration d'impôt de l'année précédente",
            "name_en" => "Déclaration d'impôt de l'année précédente",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 30],
                ],
            ]
        ],
        // new seed
        [
            "name_fr" => "Preuves de revenus",
            "name_en" => "Preuves de revenus",
            "signable" => 0,
            "customer_owner" => 0,
            "sheets" => [
                [
                    2 => ['program_id' => 4, 'order' => 30],
                ],
            ]
        ],
        // end new seed
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->documents as $phase) {
            \App\Models\DocumentType::where("name_fr", "LIKE", $phase["name_fr"])
                ->update(['name_fr' => $phase["name_fr"]]);
        }*/
        foreach ($this->documents as $document) {
            $document['collection_name'] = Str::random(30);
            if(!isset($document['steps'])){
                $document['steps'] = [];
            }
            $steps = $document['steps'];
            if(!isset($document['sheets'])){
                $document['sheets'] = [];
            }
            $sheets = $document['sheets'];
            unset($document['sheets']);

            unset($document['steps']);

            /*$doc = \App\Models\DocumentType::where('name_fr', $document['name_fr'])->first();
            if ($doc) {
                $doc->substeps()->sync([]);
            } else {
            }*/
                $doc = \App\Models\DocumentType::create(
                    $document
                );
            /*if (str_ends_with($document['name_fr'], 'signée')) {
                $doc = \App\Models\DocumentType::create(
                    $document
                );
            } else {
                continue;
            }*/

            foreach ($sheets as $sheet) $doc->sheets()->attach($sheet);
            foreach ($steps as $step) $doc->substeps()->attach($step);
        }
    }
    public function run1(){
        $groups = [
            'Niveau Bac',
            'Niveau Bac +2',
            'Baccalaureat',
            'Bac +1',
            'Bac +2',
            'Licence',
            'Licence Professionnelle',
            'Maitrise 1',
            'Maitrise 2',
            'Doctorat',
            'Niveau Bac en cours',
            'Niveau Bac +2 en cours',
            'Baccalaureat en cours',
            'Bac +1 en cours',
            'Bac +2 en cours',
            'Licence en cours',
            'Licence Professionnelle en cours',
            'Maitrise 1 en cours',
            'Maitrise 2 en cours',
            'Doctorat en cours',
            // types
            'Père',
            'Mère',
            'Grand Père',
            'Frère',
            'Soeur',
            'Oncle',
            'Tente',
            'Époux',
            'Épouse',
            // incomes
            "Salaire",
            "Pension",
            "Entreprise",
            "Profession libéral ou Travailleur autonome",
            "Revenu locatif",
            "Autre",
        ];
        foreach ($groups as $groupName) {
            $tagGroupModel = TaggingUtility::tagGroupModelString();

            $tagGroup = new $tagGroupModel();
            $tagGroup->name = $groupName;
            $tagGroup->slug = TaggingUtility::normalize($groupName);

            $tagGroup->save();
        }
    }
}

/**
 *  doc types to be translated from database (document_types table)
 *
 * Accusé de réception WES => WES receipt acknowledgment
 * Bon d’envoi WES => WES shipping slip
 * Lettre WES => WES letter
 *
 * Copie de la première page de passeport => Copy of the first page of the passport
 * Equivalence des diplômes (WES) => Diploma equivalences (WES)
 * TCF Canada Conjoint => TCF Canada - Spouse
 * TEF Canada Conjoint => TEF Canada - Spouse
 * IELTS Conjoint => IELTS - Spouse
 */

