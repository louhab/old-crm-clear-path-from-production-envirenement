<?php

use Illuminate\Database\Seeder;

class CustomerFieldsSeeder extends Seeder
{
    private $customerFields = [
        // 🌱 clear

        // Done
        // [
        //     'customer_field_group_id' => 1,
        //     'field_name' => 'eye_colour',
        //     'template' => 'text',
        //     "steps" => [
        //         [
        //             1 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 5, 'order' => 1],
        //         ],
        //     ],
        //     "sheets" => [
        //         [
        //             2 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 4, 'order' => 1],
        //         ],
        //     ]
        // ],
        // [
        //     'customer_field_group_id' => 1,
        //     'field_name' => 'size',
        //     'template' => 'text',
        //     "steps" => [
        //         [
        //             1 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 5, 'order' => 1],
        //         ],
        //     ],
        //     "sheets" => [
        //         [
        //             2 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 4, 'order' => 1],
        //         ],
        //     ]
        // ],
        // [
        //     'customer_field_group_id' => 1,
        //     'field_name' => 'previous_spouse_firstname',
        //     'template' => 'text',
        //     "steps" => [
        //         [
        //             1 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 5, 'order' => 1],
        //         ],
        //     ],
        //     "sheets" => [
        //         [
        //             2 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 4, 'order' => 1],
        //         ],
        //     ]
        // ],
        // [
        //     'customer_field_group_id' => 1,
        //     'field_name' => 'previous_spouse_lastname',
        //     'template' => 'text',
        //     "steps" => [
        //         [
        //             1 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 5, 'order' => 1],
        //         ],
        //     ],
        //     "sheets" => [
        //         [
        //             2 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 4, 'order' => 1],
        //         ],
        //     ]
        // ],
        // [
        //     'customer_field_group_id' => 1,
        //     'field_name' => 'passport_number',
        //     'template' => 'text',
        //     "steps" => [
        //         [
        //             1 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 5, 'order' => 1],
        //         ],
        //     ],
        //     "sheets" => [
        //         [
        //             2 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 4, 'order' => 1],
        //         ],
        //     ]
        // ],
        // [
        //     'customer_field_group_id' => 1,
        //     'field_name' => 'passport_expiration_date',
        //     'template' => 'date',
        //     "steps" => [
        //         [
        //             1 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 5, 'order' => 1],
        //         ],
        //     ],
        //     "sheets" => [
        //         [
        //             2 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             2 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             3 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             5 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 1, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             6 => ['program_id' => 4, 'order' => 1],
        //         ],
        //     ]
        // ],

        // // status
        // [
        //     'customer_field_group_id' => 2,
        //     'field_name' => 'work_business_status',
        //     'template' => 'text',
        //     "steps" => [],
        //     "sheets" => [
        //         [
        //             3 => ['program_id' => 1, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 2, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 3, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 4, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 5, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 6, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 7, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 8, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 9, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 10, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 11, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 12, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 13, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 14, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 15, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 16, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 17, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 18, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 19, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 20, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 21, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 22, "required" => 0, 'order' => 4],
        //         ],
        //         [
        //             3 => ['program_id' => 23, "required" => 0, 'order' => 4],
        //         ],
        //     ]
        // ],

        // // various_travel_city
        // [
        //     'customer_field_group_id' => 5,
        //     'customer_field_sub_group_id' => 8,
        //     'field_name' => 'various_travel_city',
        //     'template' => 'text',
        //     "steps" => [
        //         [
        //             1 => ['program_id' => 1, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 5, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 6, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 7, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 8, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 9, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 10, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 11, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 12, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 13, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 14, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 15, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 16, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 17, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 18, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 19, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 20, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 21, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 22, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 23, "required" => 0, 'order' => 3],
        //         ],
        //     ],
        //     "sheets" => [
        //         [
        //             3 => ['program_id' => 1, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 2, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 3, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 4, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 5, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 6, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 7, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 8, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 9, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 10, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 11, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 12, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 13, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 14, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 15, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 16, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 17, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 18, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 19, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 20, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 21, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 22, "required" => 0, 'order' => 14],
        //         ],
        //         [
        //             3 => ['program_id' => 23, "required" => 0, 'order' => 14],
        //         ],
        //     ]
        // ],

        // // past_residence_zip
        // [
        //     'customer_field_group_id' => 5,
        //     'customer_field_sub_group_id' => 7,
        //     'field_name' => 'past_residence_zip',
        //     'template' => 'text',
        //     "steps" => [
        //         [
        //             1 => ['program_id' => 1, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 5, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 6, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 7, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 8, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 9, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 10, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 11, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 12, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 13, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 14, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 15, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 16, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 17, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 18, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 19, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 20, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 21, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 22, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 23, "required" => 0, 'order' => 3],
        //         ],
        //     ],
        //     "sheets" => [
        //         [
        //             3 => ['program_id' => 1, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 2, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 3, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 4, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 5, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 6, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 7, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 8, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 9, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 10, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 11, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 12, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 13, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 14, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 15, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 16, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 17, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 18, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 19, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 20, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 21, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 22, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 23, "required" => 0, 'order' => 9],
        //         ],
        //     ]
        // ],

        // // desired_province_for_living
        // [
        //     'customer_field_group_id' => 5,
        //     'customer_field_sub_group_id' => 9,
        //     'field_name' => 'desired_province_for_living',
        //     'template' => 'text',
        //     "steps" => [
        //         [
        //             1 => ['program_id' => 1, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 5, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 6, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 7, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 8, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 9, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 10, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 11, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 12, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 13, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 14, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 15, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 16, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 17, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 18, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 19, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 20, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 21, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 22, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 23, "required" => 0, 'order' => 3],
        //         ],
        //     ],
        //     "sheets" => [
        //         [
        //             3 => ['program_id' => 1, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 2, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 3, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 4, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 5, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 6, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 7, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 8, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 9, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 10, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 11, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 12, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 13, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 14, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 15, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 16, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 17, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 18, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 19, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 20, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 21, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 22, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 23, "required" => 0, 'order' => 9],
        //         ],
        //     ]
        // ],
        // // desired_city_for_living
        // [
        //     'customer_field_group_id' => 5,
        //     'customer_field_sub_group_id' => 9,
        //     'field_name' => 'desired_city_for_living',
        //     'template' => 'text',
        //     "steps" => [
        //         [
        //             1 => ['program_id' => 1, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 5, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 6, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 7, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 8, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 9, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 10, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 11, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 12, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 13, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 14, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 15, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 16, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 17, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 18, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 19, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 20, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 21, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 22, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 23, "required" => 0, 'order' => 3],
        //         ],
        //     ],
        //     "sheets" => [
        //         [
        //             3 => ['program_id' => 1, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 2, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 3, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 4, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 5, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 6, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 7, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 8, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 9, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 10, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 11, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 12, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 13, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 14, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 15, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 16, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 17, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 18, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 19, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 20, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 21, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 22, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 23, "required" => 0, 'order' => 9],
        //         ],
        //     ]
        // ],
        // // has_cert_for_living
        // [
        //     'customer_field_group_id' => 5,
        //     'customer_field_sub_group_id' => 9,
        //     'field_name' => 'has_cert_for_living',
        //     'template' => 'text',
        //     "steps" => [
        //         [
        //             1 => ['program_id' => 1, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 2, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 3, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 4, 'order' => 1],
        //         ],
        //         [
        //             1 => ['program_id' => 5, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 6, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 7, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 8, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 9, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 10, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 11, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 12, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 13, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 14, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 15, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 16, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 17, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 18, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 19, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 20, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 21, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 22, "required" => 0, 'order' => 3],
        //         ],
        //         [
        //             1 => ['program_id' => 23, "required" => 0, 'order' => 3],
        //         ],
        //     ],
        //     "sheets" => [
        //         [
        //             3 => ['program_id' => 1, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 2, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 3, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 4, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 5, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 6, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 7, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 8, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 9, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 10, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 11, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 12, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 13, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 14, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 15, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 16, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 17, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 18, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 19, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 20, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 21, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 22, "required" => 0, 'order' => 9],
        //         ],
        //         [
        //             3 => ['program_id' => 23, "required" => 0, 'order' => 9],
        //         ],
        //     ]
        // ],


        // section 6
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_received_conviction',
            'template' => 'text',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
            "steps" => [
                [
                    1 => ['program_id' => 1, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 5, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 6, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 7, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 8, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 9, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 10, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 11, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 12, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 13, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 14, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 15, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 16, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 17, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 18, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 19, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 20, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 21, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 22, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 23, "required" => 0, 'order' => 3],
                ],
            ],
            "sheets" => [
                [
                    3 => ['program_id' => 1, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 2, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 3, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 4, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 5, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 6, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 7, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 8, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 9, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 10, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 11, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 12, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 13, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 14, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 15, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 16, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 17, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 18, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 19, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 20, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 21, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 22, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 23, "required" => 0, 'order' => 9],
                ],
            ]
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_committed_criminal_act',
            'template' => 'text',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
            "steps" => [
                [
                    1 => ['program_id' => 1, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 5, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 6, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 7, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 8, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 9, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 10, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 11, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 12, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 13, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 14, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 15, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 16, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 17, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 18, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 19, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 20, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 21, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 22, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 23, "required" => 0, 'order' => 3],
                ],
            ],
            "sheets" => [
                [
                    3 => ['program_id' => 1, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 2, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 3, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 4, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 5, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 6, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 7, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 8, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 9, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 10, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 11, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 12, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 13, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 14, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 15, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 16, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 17, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 18, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 19, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 20, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 21, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 22, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 23, "required" => 0, 'order' => 9],
                ],
            ]
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_submitted_request',
            'template' => 'text',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
            "steps" => [
                [
                    1 => ['program_id' => 1, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 5, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 6, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 7, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 8, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 9, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 10, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 11, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 12, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 13, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 14, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 15, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 16, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 17, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 18, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 19, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 20, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 21, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 22, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 23, "required" => 0, 'order' => 3],
                ],
            ],
            "sheets" => [
                [
                    3 => ['program_id' => 1, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 2, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 3, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 4, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 5, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 6, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 7, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 8, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 9, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 10, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 11, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 12, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 13, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 14, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 15, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 16, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 17, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 18, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 19, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 20, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 21, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 22, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 23, "required" => 0, 'order' => 9],
                ],
            ]
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_denied_refugee_status',
            'template' => 'text',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
            "steps" => [
                [
                    1 => ['program_id' => 1, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 5, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 6, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 7, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 8, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 9, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 10, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 11, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 12, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 13, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 14, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 15, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 16, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 17, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 18, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 19, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 20, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 21, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 22, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 23, "required" => 0, 'order' => 3],
                ],
            ],
            "sheets" => [
                [
                    3 => ['program_id' => 1, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 2, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 3, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 4, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 5, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 6, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 7, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 8, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 9, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 10, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 11, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 12, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 13, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 14, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 15, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 16, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 17, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 18, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 19, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 20, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 21, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 22, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 23, "required" => 0, 'order' => 9],
                ],
            ]
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_been_refused_visa',
            'template' => 'text',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
            "steps" => [
                [
                    1 => ['program_id' => 1, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 5, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 6, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 7, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 8, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 9, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 10, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 11, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 12, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 13, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 14, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 15, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 16, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 17, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 18, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 19, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 20, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 21, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 22, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 23, "required" => 0, 'order' => 3],
                ],
            ],
            "sheets" => [
                [
                    3 => ['program_id' => 1, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 2, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 3, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 4, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 5, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 6, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 7, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 8, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 9, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 10, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 11, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 12, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 13, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 14, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 15, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 16, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 17, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 18, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 19, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 20, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 21, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 22, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 23, "required" => 0, 'order' => 9],
                ],
            ]
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_took_part_in_act_genocide',
            'template' => 'text',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
            "steps" => [
                [
                    1 => ['program_id' => 1, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 5, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 6, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 7, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 8, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 9, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 10, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 11, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 12, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 13, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 14, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 15, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 16, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 17, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 18, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 19, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 20, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 21, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 22, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 23, "required" => 0, 'order' => 3],
                ],
            ],
            "sheets" => [
                [
                    3 => ['program_id' => 1, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 2, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 3, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 4, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 5, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 6, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 7, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 8, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 9, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 10, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 11, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 12, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 13, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 14, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 15, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 16, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 17, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 18, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 19, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 20, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 21, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 22, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 23, "required" => 0, 'order' => 9],
                ],
            ]
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_advocated_use_armed_struggle',
            'template' => 'text',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
            "steps" => [
                [
                    1 => ['program_id' => 1, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 5, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 6, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 7, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 8, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 9, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 10, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 11, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 12, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 13, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 14, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 15, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 16, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 17, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 18, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 19, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 20, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 21, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 22, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 23, "required" => 0, 'order' => 3],
                ],
            ],
            "sheets" => [
                [
                    3 => ['program_id' => 1, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 2, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 3, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 4, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 5, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 6, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 7, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 8, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 9, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 10, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 11, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 12, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 13, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 14, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 15, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 16, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 17, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 18, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 19, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 20, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 21, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 22, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 23, "required" => 0, 'order' => 9],
                ],
            ]
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_had_ties_to_group_that_used_armed_struggle',
            'template' => 'text',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
            "steps" => [
                [
                    1 => ['program_id' => 1, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 5, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 6, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 7, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 8, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 9, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 10, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 11, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 12, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 13, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 14, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 15, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 16, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 17, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 18, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 19, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 20, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 21, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 22, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 23, "required" => 0, 'order' => 3],
                ],
            ],
            "sheets" => [
                [
                    3 => ['program_id' => 1, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 2, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 3, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 4, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 5, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 6, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 7, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 8, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 9, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 10, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 11, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 12, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 13, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 14, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 15, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 16, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 17, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 18, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 19, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 20, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 21, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 22, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 23, "required" => 0, 'order' => 9],
                ],
            ]
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_been_member_of_organization',
            'template' => 'text',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
            "steps" => [
                [
                    1 => ['program_id' => 1, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 5, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 6, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 7, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 8, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 9, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 10, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 11, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 12, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 13, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 14, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 15, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 16, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 17, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 18, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 19, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 20, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 21, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 22, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 23, "required" => 0, 'order' => 3],
                ],
            ],
            "sheets" => [
                [
                    3 => ['program_id' => 1, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 2, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 3, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 4, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 5, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 6, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 7, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 8, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 9, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 10, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 11, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 12, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 13, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 14, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 15, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 16, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 17, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 18, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 19, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 20, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 21, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 22, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 23, "required" => 0, 'order' => 9],
                ],
            ]
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_is_subject_detention',
            'template' => 'text',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
            "steps" => [
                [
                    1 => ['program_id' => 1, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 5, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 6, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 7, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 8, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 9, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 10, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 11, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 12, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 13, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 14, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 15, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 16, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 17, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 18, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 19, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 20, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 21, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 22, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 23, "required" => 0, 'order' => 3],
                ],
            ],
            "sheets" => [
                [
                    3 => ['program_id' => 1, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 2, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 3, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 4, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 5, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 6, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 7, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 8, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 9, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 10, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 11, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 12, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 13, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 14, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 15, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 16, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 17, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 18, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 19, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 20, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 21, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 22, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 23, "required" => 0, 'order' => 9],
                ],
            ]
        ],
        [
            'customer_field_group_id' => 14,
            'field_name' => 'extra_suffered_from_serious_illness',
            'template' => 'text',
            'select_model' => '\App\Models\Clear3Questioning',
            'select_model_label' => 'label_fr',
            'select_model_id' => 'id',
            "steps" => [
                [
                    1 => ['program_id' => 1, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 2, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 3, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    1 => ['program_id' => 5, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 6, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 7, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 8, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 9, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 10, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 11, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 12, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 13, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 14, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 15, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 16, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 17, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 18, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 19, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 20, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 21, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 22, "required" => 0, 'order' => 3],
                ],
                [
                    1 => ['program_id' => 23, "required" => 0, 'order' => 3],
                ],
            ],
            "sheets" => [
                [
                    3 => ['program_id' => 1, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 2, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 3, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 4, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 5, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 6, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 7, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 8, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 9, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 10, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 11, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 12, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 13, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 14, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 15, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 16, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 17, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 18, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 19, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 20, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 21, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 22, "required" => 0, 'order' => 9],
                ],
                [
                    3 => ['program_id' => 23, "required" => 0, 'order' => 9],
                ],
            ]
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->customerFields as $customerField) {

            $steps = $customerField['steps'] ?? [];
            $sheets = $customerField['sheets'] ?? [];

            unset($customerField['steps']);
            unset($customerField['sheets']);

            /*$field = \App\Models\CustomerField::where('field_name', $customerField['field_name'])->first();
            if ($field && array_key_exists('customer_field_sub_group_id', $customerField)){
                $field->update(['customer_field_sub_group_id' => $customerField['customer_field_sub_group_id']]);
                // $field->save();
            }*/

            $field = \App\Models\CustomerField::create(
                $customerField
            );
            foreach ($steps as $step) $field->steps()->attach($step);
            foreach ($steps as $step) $field->substeps()->attach($step);
            foreach ($sheets as $sheet) $field->sheets()->attach($sheet);
        }
    }
}
