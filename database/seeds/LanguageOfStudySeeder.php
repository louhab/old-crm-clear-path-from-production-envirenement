<?php

use Illuminate\Database\Seeder;

use App\Models\LanguageOfStudy;

class LanguageOfStudySeeder extends Seeder
{
    private $languages = [
        ["Anglais","English"],
        ["Français","French"],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->languages as $phase) {
            LanguageOfStudy::where("label", "LIKE", $phase[0])
                ->update(['labelEng' => $phase[1]]);
        }*/

        foreach ($this->languages as $language) {
            LanguageOfStudy::create(
                [
                    'label' => $language[0],
                    'labelEng'=>$language[1]
                ]
            );

            // $citiesToAttach = [];

            // foreach ($language[1] as $city) {
            //     $citiesToAttach[$city] = [
            //         'order' => $city
            //     ];
            // }

            // $lang->cities()->sync($citiesToAttach);
        }
    }
}
