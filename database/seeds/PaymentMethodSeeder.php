<?php

use Illuminate\Database\Seeder;

use App\Models\PaymentMethod;

class PaymentMethodSeeder extends Seeder
{
    private $methods = [
        ['label_fr'=> 'Espèce', 'label_en'=> 'Cash'],
        ['label_fr'=> 'Virement', 'label_en'=> 'Transfer'],
        ['label_fr'=> 'Chèque', 'label_en'=> 'Bank Check'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->methods as $method) {
            PaymentMethod::create(
                [
                    'method_label_fr' => $method['label_fr'],
                    'method_label_en' => $method['label_en'],
                ]
            );
        }
    }
}
