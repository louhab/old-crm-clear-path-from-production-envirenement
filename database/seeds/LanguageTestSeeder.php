<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class LanguageTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $raw_lang_tests = file_get_contents(__DIR__ . '/language_tests.json');

        $lang_tests = json_decode($raw_lang_tests, true);

        foreach ($lang_tests as $key => $value) {
            \App\Models\LanguageTest::create([
                "id" => $value["id"],
                "label" => $value["label"],
                "program_ids" => $value["program_ids"],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
