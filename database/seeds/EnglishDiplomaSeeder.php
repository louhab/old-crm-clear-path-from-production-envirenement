<?php

use Illuminate\Database\Seeder;

use App\Models\EnglishDiploma;

class EnglishDiplomaSeeder extends Seeder
{
    // No, Ielts
    private $diplomas = [
        ['label'=> 'No'],
        ['label'=> 'Ielts'],
        ['label'=> 'Toefl'],
        ['label'=> 'Toeic'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->diplomas as $diploma) {
            EnglishDiploma::create(
                [
                    'diploma_label' => $diploma['label'],
                ]
            );
        }
    }
}
