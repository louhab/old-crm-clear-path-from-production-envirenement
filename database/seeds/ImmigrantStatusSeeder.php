<?php

use Illuminate\Database\Seeder;

use App\Models\ImmigrantStatus;

class ImmigrantStatusSeeder extends Seeder
{
    private $states = [
        ['label_fr'=> 'Touriste'                ,'labelEng'=>"Tourist"],
        ['label_fr'=> 'Etudiant'                ,'labelEng'=>"Student"],
        ['label_fr'=> 'Resident Permanent'      ,'labelEng'=>"Permanent Resident"],
        ['label_fr'=> 'Travailleur Temporaire'  ,'labelEng'=>"Temporary worker"],
        ['label_fr'=> 'Citoyen'                 ,'labelEng'=>"Citizen"],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->states as $phase) {
            ImmigrantStatus::where("label_fr", "LIKE", $phase["label_fr"])
                ->update(['labelEng' => $phase["labelEng"]]);
        }*/

        foreach ($this->states as $state) {
            ImmigrantStatus::create(
                $state
            );
        }
    }
}
