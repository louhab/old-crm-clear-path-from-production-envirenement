<?php

use Illuminate\Database\Seeder;
use App\Models\MonthlyIncome;

class AddIncomesSeeder extends Seeder
{
    private $incomes = [
        1 => [
            "<= 5000 EURO",
            "<= 5000 USD"
        ],
        2 => [
            "> 5000 DH, <= 7500 EURO",
            "> 5000 DH, <= 7500 USD",
        ],
        3 => [
            "> 7500 EURO, <= 10000 EURO",
            "> 7500 USD, <= 10000 USD",
        ],
        4 => [
            "> 10000 EURO",
            "> 10000 USD",
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->incomes as $id => $income) {
            $monthly_income = MonthlyIncome::find($id);
            $monthly_income->income_eur = $income[0];
            $monthly_income->income_usd = $income[1];
            $monthly_income->save();
        }
    }
}
