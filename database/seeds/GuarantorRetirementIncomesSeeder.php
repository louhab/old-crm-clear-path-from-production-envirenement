<?php

use App\Models\GuarantorRetirementIncome;
use Illuminate\Database\Seeder;

class GuarantorRetirementIncomesSeeder extends Seeder
{
    private $incomes = [
        ['label_fr'=> "Un revenu de retraite", 'label_en'=> "Un revenu de retraite"],
        ['label_fr'=> "Deux revenus de retraite", 'label_en'=> "Deux revenus de retraite"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->incomes as $income) {
            GuarantorRetirementIncome::create(
                [
                    'label_fr' => $income['label_fr'],
                    'label_en' => $income['label_en'],
                ]
            );
        }
    }
}
