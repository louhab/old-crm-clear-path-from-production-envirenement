<?php

use App\Models\CpgAccount;
use Illuminate\Database\Seeder;

class CpgAccountsSeeder extends Seeder
{
    private $elements = [
        ['label_fr'=> "Non, je ne veux pas ouvrir un compte bancaire au Canada", 'label_en'=> "Non, je ne veux pas ouvrir un compte bancaire au Canada"],
        ['label_fr'=> "Je veux ouvrir un compte  bancaire au Canada et verser un montant de 10200$ avant la soumission de mon dossier Visa et permis d'études", 'label_en'=> "Je veux ouvrir un compte  bancaire au Canada et verser un montant de 10200$ avant la soumission de mon dossier Visa et permis d'études"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->elements as $element) {
            CpgAccount::create(
                [
                    'label_fr' => $element['label_fr'],
                    'label_en' => $element['label_en'],
                ]
            );
        }
    }
}
