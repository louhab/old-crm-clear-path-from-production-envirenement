<?php

use Illuminate\Database\Seeder;

use App\Models\GuarantorType;

class GuarantorTypeSeeder extends Seeder
{
    private $garants = [
        ['label_fr'=> 'Père', 'label_en'=> 'Father'],
        ['label_fr'=> 'Mère', 'label_en'=> 'Mother'],
        ['label_fr'=> 'Grand Père', 'label_en'=> 'Grandfather'],
        ['label_fr'=> 'Frère', 'label_en'=> 'Brother'],
        ['label_fr'=> 'Soeur', 'label_en'=> 'Sister'],
        ['label_fr'=> 'Oncle', 'label_en'=> 'Uncle'],
        ['label_fr'=> 'Tente', 'label_en'=> 'Tent'],

        ['label_fr'=> 'Époux', 'label_en'=> 'Husband'],
        ['label_fr'=> 'Épouse', 'label_en'=> 'Spouse'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->garants as $garant) {
            GuarantorType::create(
                [
                    'garant_label_fr' => $garant['label_fr'],
                    'garant_label_en' => $garant['label_en'],
                ]
            );
        }
    }
}
