<?php

use App\Models\ProgramStep;
use Illuminate\Database\Seeder;

class ProgramStepsSeeder extends Seeder
{
    private $pgrSteps = [
        ['step_phase_id' => 1, 'step_label' => 'Analyse du dossier', 'visible_for_conseiller' => 1, 'visible_for_agent' => 1, 'visible_for_backoffice' => 0                                                                ,"labelEng"=>"Dossier analysis"     ],//1
        ['step_phase_id' => 1, 'step_label' => 'Conseil (Rencontre 1)', 'visible_for_conseiller' => 1, 'visible_for_agent' => 1, 'visible_for_backoffice' => 0                                                             ,"labelEng"=>"Consulting (Meeting 1)"     ],//2

        ['step_phase_id' => 2, 'step_label' => 'Orientation pour la demande d’admission (Rencontre 2)', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 0                             ,"labelEng"=>"Orientation for the admission application (Meeting 2)"     ],//3
        ['step_phase_id' => 2, 'step_label' => 'Préparation de la demande d’admission (Rencontre 3)', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 0                               ,"labelEng"=>"Preparation of the admission application (Meeting 3)"     ],//4
        ['step_phase_id' => 2, 'step_label' => 'Soumission de la demande d’admission', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 1                                              ,"labelEng"=>"Submission of the Admission Request"     ],//5
        ['step_phase_id' => 2, 'step_label' => 'Suivi de la demande d’admission', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 0                                                   ,"labelEng"=>"Follow-up of the admission request"     ],//6

        ['step_phase_id' => 3, 'step_label' => 'Préparation de la demande de visa et PE (Rencontre 4)', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 0                             ,"labelEng"=>"Preparation of the visa application and Student Permit (Meeting 4)"     ],//7
        ['step_phase_id' => 3, 'step_label' => 'Préparation de la demande de visa et PE', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 1                                           ,"labelEng"=>"Preparation of visa application and Student Permit"     ],//8
        ['step_phase_id' => 3, 'step_label' => 'Préparation de la demande de visa et PE (Rencontre 5)', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 0                             ,"labelEng"=>"Preparation of the visa application and Student Program (Meeting 5)"     ],//9
        ['step_phase_id' => 3, 'step_label' => 'Soumission de la demande de visa et PE', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 1                                            ,"labelEng"=>"Submission of visa application and EP"     ],//10
        ['step_phase_id' => 3, 'step_label' => 'Suivi de la demande de visa et PE', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 0                                                 ,"labelEng"=>"Follow-up of visa and EP application"     ],//11
        ['step_phase_id' => 3, 'step_label' => 'Préparation du séjour (Rencontre 6)', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 0                                               ,"labelEng"=>"Preparation of the stay (Meeting 6)"     ],//12

        ['step_phase_id' => 2, 'step_label' => 'Préparation de la demande d\'équivalence et les tests de langues (Rencontre 2)', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 0    ,"labelEng"=>"Preparation of the equivalence application and language tests (Meeting 2)"     ],//13
        ['step_phase_id' => 2, 'step_label' => 'Préparation du profil :program: (Rencontre 3)', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 0                                     ,"labelEng"=>"Preparation of the profile :program: (Meeting 3)"     ],//14
        ['step_phase_id' => 2, 'step_label' => 'Soumission du profil :program:', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 1                                                    ,"labelEng"=>"Submission of profile :program:"     ],//15
        ['step_phase_id' => 2, 'step_label' => 'Suivi du profil :program:', 'visible_for_conseiller' => 1, 'visible_for_agent' => 0, 'visible_for_backoffice' => 0                                                         ,"labelEng"=>"Follow-up of profile :program:"     ],//16

    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->pgrSteps as $phase) {
            ProgramStep::where("step_label", "LIKE", "%" .$phase["step_label"]."%")
                ->update(['labelEng' => $phase["labelEng"]]);
        }*/

        foreach ($this->pgrSteps as $pgrStep) {
            ProgramStep::create(
                $pgrStep
            );
        }
    }
}
