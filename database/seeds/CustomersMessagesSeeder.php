<?php

use Illuminate\Database\Seeder;

class CustomersMessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i=1;$i < 30; $i++) {
            \App\Models\CustomerMessage::create([
                'id' => $i,
                'user_id' => 1,
                'customer_id' => 2,
                'object' => $faker->text($maxNbChars = 50),
                'message' => $faker->text($maxNbChars = 200)
            ]);
        }
    }
}
