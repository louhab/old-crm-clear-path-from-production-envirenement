<?php

use Illuminate\Database\Seeder;

use App\Models\AdmissionSchoolLevel;

class AdmissionSchoolLevelSeeder extends Seeder
{
    private $levels = [
        ["Diplôme d'étude collégial", "Diploma"],
        ["Diplôme professionnel"    , "Professional diploma"],
        ["Diplôme universitaire"    , "University degree"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->levels as $level) {
            AdmissionSchoolLevel::create([
                'label' => $level[0],
                'labelEng' => $level[1]
            ]);
            //$level_ = AdmissionSchoolLevel::where('label', $level[0])->first();

            // $etabsToAttach = [];

            // foreach ($level[1] as $level) {
            //     $etabsToAttach[$level] = [
            //         'order' => $level
            //     ];
            // }
            // $level_->etabs()->sync($etabsToAttach);
        }
    }
}
