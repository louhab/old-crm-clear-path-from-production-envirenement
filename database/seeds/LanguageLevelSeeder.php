<?php

use Illuminate\Database\Seeder;

use App\Models\LanguageLevel;

class LanguageLevelSeeder extends Seeder
{
    // 5, 6, 7, 8 , 9, 10
    private $levels = [
        ['label'=> '5'],
        ['label'=> '6'],
        ['label'=> '7'],
        ['label'=> '8'],
        ['label'=> '9'],
        ['label'=> '10'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->levels as $level) {
            LanguageLevel::create(
                [
                    'level_label' => $level['label'],
                ]
            );
        }
    }
}
