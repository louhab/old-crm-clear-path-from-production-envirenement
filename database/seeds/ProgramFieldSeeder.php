<?php

use Illuminate\Database\Seeder;

use App\Models\ProgramField;

class ProgramFieldSeeder extends Seeder
{
    private $fields = [
        /**
         * => Administration des Affaires
         * Gestion de l’administration des Affaires
         * Ressources Humaines
         * Service à la clientèle
         */

        [
            "label_fr" => "Gestion de l'administration des Affaires",
            "labelEng" => "Gestion de l'administration des Affaires",
            "category" => "Administration des Affaires",
            'visible' => true
        ],
        [
            "label_fr" => "Ressources Humaines",
            "labelEng" => "Ressources Humaines",
            "category" => "Administration des Affaires",
            'visible' => true
        ],
        [
            "label_fr" => "Service à la clientèle",
            "labelEng" => "Service à la clientèle",
            "category" => "Administration des Affaires",
            'visible' => true
        ],


        /**
         * => Agriculture
         * Agriculteur
         * Conseiller agricole
         * Ingénieur agronome
         */

        [
            "label_fr" => "Agriculteur",
            "labelEng" => "Agriculteur",
            "category" => "Agriculture",
            'visible' => true
        ],
        [
            "label_fr" => "Conseiller agricole",
            "labelEng" => "Conseiller agricole",
            "category" => "Agriculture",
            'visible' => true
        ],
        [
            "label_fr" => "Ingénieur agronome",
            "labelEng" => "Ingénieur agronome",
            "category" => "Agriculture",
            'visible' => true
        ],


        /**
         * => Architecture et Génie civil
         * Architecte
         * Architecte d'intérieur.
         * Agent de développement local
         * Génie civil
         * Dessinateur- projeteur
         * Géomètre
         * Topographe
         */

        [
            "label_fr" => "Architecte",
            "labelEng" => "Architecte",
            "category" => "Architecture et Génie civil",
            'visible' => true
        ],
        [
            "label_fr" => "Architecte d'intérieur",
            "labelEng" => "Architecte d'intérieur",
            "category" => "Architecture et Génie civil",
            'visible' => true
        ],
        [
            "label_fr" => "Agent de développement local",
            "labelEng" => "Agent de développement local",
            "category" => "Architecture et Génie civil",
            'visible' => true
        ],
        [
            "label_fr" => "Génie civil",
            "labelEng" => "Génie civil",
            "category" => "Architecture et Génie civil",
            'visible' => true
        ],
        [
            "label_fr" => "Dessinateur- projeteur",
            "labelEng" => "Dessinateur- projeteur",
            "category" => "Architecture et Génie civil",
            'visible' => true
        ],
        [
            "label_fr" => "Géomètre",
            "labelEng" => "Géomètre",
            "category" => "Architecture et Génie civil",
            'visible' => true
        ],
        [
            "label_fr" => "Topographe",
            "labelEng" => "Topographe",
            "category" => "Architecture et Génie civil",
            'visible' => true
        ],


        /**
         * => Construction et BTP
         * Ingénierie en construction
         * Briquetage
         * Maçonnerie
         * Electricité
         * Plomberie
         * Peinture en BTP
         */

        [
            "label_fr" => "Ingénierie en construction",
            "labelEng" => "Ingénierie en construction",
            "category" => "Construction et BTP",
            'visible' => true
        ],
        [
            "label_fr" => "Briquetage",
            "labelEng" => "Briquetage",
            "category" => "Construction et BTP",
            'visible' => true
        ],
        [
            "label_fr" => "Maçonnerie",
            "labelEng" => "Maçonnerie",
            "category" => "Construction et BTP",
            'visible' => true
        ],
        [
            "label_fr" => "Electricité",
            "labelEng" => "Electricité",
            "category" => "Construction et BTP",
            'visible' => true
        ],
        [
            "label_fr" => "Plomberie",
            "labelEng" => "Plomberie",
            "category" => "Construction et BTP",
            'visible' => true
        ],
        [
            "label_fr" => "Peinture en BTP",
            "labelEng" => "Peinture en BTP",
            "category" => "Construction et BTP",
            'visible' => true
        ],

        /**
         * => Design - Art
         * Art appliqué
         * Audio-visuel
         * Infographie
         * Design
         * Design industriel
         */

        [
            "label_fr" => "Art appliqué",
            "labelEng" => "Art appliqué",
            "category" => "Design - Art",
            'visible' => true
        ],
        [
            "label_fr" => "Audio-visuel",
            "labelEng" => "Audio-visuel",
            "category" => "Design - Art",
            'visible' => true
        ],
        [
            "label_fr" => "Infographie",
            "labelEng" => "Infographie",
            "category" => "Design - Art",
            'visible' => true
        ],
        [
            "label_fr" => "Design",
            "labelEng" => "Design",
            "category" => "Design - Art",
            'visible' => true
        ],
        [
            "label_fr" => "Design industriel",
            "labelEng" => "Design industriel",
            "category" => "Design - Art",
            'visible' => true
        ],

        /**
         * => Domaine de santé
         * Ingénierie et Génie biomédical
         * Pharmacie
         * Dentaire
         * Infirmerie
         * Kinésithérapie
         * Métiers de santé
         */

        [
            "label_fr" => "Ingénierie et Génie biomédical",
            "labelEng" => "Ingénierie et Génie biomédical",
            "category" => "Domaine de santé",
            'visible' => true
        ],
        [
            "label_fr" => "Pharmacie",
            "labelEng" => "Pharmacie",
            "category" => "Domaine de santé",
            'visible' => true
        ],
        [
            "label_fr" => "Dentaire",
            "labelEng" => "Dentaire",
            "category" => "Domaine de santé",
            'visible' => true
        ],
        [
            "label_fr" => "Infirmerie",
            "labelEng" => "Infirmerie",
            "category" => "Domaine de santé",
            'visible' => true
        ],
        [
            "label_fr" => "Kinésithérapie",
            "labelEng" => "Kinésithérapie",
            "category" => "Domaine de santé",
            'visible' => true
        ],
        [
            "label_fr" => "Métiers de santé",
            "labelEng" => "Métiers de santé",
            "category" => "Domaine de santé",
            'visible' => true
        ],

        /**
         * => Droit et Justice de protection du public
         * Droit
         * Protection du public
         * Conseiller /ère en sécurité, enquêtes et services judiciaires.
         * Attaché Judiciaire
         * Cadre juridique
         * Police
         */

        [
            "label_fr" => "Droit",
            "labelEng" => "Droit",
            "category" => "Droit et Justice de protection du public",
            'visible' => true
        ],
        [
            "label_fr" => "Protection du public",
            "labelEng" => "Protection du public",
            "category" => "Droit et Justice de protection du public",
            'visible' => true
        ],
        [
            "label_fr" => "Conseiller /ère en sécurité, enquêtes et services judiciaires",
            "labelEng" => "Conseiller /ère en sécurité, enquêtes et services judiciaires",
            "category" => "Droit et Justice de protection du public",
            'visible' => true
        ],
        [
            "label_fr" => "Attaché Judiciaire",
            "labelEng" => "Attaché Judiciaire",
            "category" => "Droit et Justice de protection du public",
            'visible' => true
        ],
        [
            "label_fr" => "Cadre juridique",
            "labelEng" => "Cadre juridique",
            "category" => "Droit et Justice de protection du public",
            'visible' => true
        ],
        [
            "label_fr" => "Police",
            "labelEng" => "Police",
            "category" => "Droit et Justice de protection du public",
            'visible' => true
        ],

        /**
         * => Enseignement et Education
         * Enseignant
         * Coach
         * Educateur/ éducatrice
         * Aide enseignant
         */

        [
            "label_fr" => "Enseignant",
            "labelEng" => "Enseignant",
            "category" => "Enseignement et Education",
            'visible' => true
        ],
        [
            "label_fr" => "Coach",
            "labelEng" => "Coach",
            "category" => "Enseignement et Education",
            'visible' => true
        ],
        [
            "label_fr" => "Educateur/ éducatrice",
            "labelEng" => "Educateur/ éducatrice",
            "category" => "Enseignement et Education",
            'visible' => true
        ],
        [
            "label_fr" => "Topographe",
            "labelEng" => "Topographe",
            "category" => "Enseignement et Education",
            'visible' => true
        ],

        /**
         * => Electricité
         * Technicien en électricité
         * Installation électrique
         */

        [
            "label_fr" => "Technicien en électricité",
            "labelEng" => "Technicien en électricité",
            "category" => "Electricité",
            'visible' => true
        ],
        [
            "label_fr" => "Installation électrique",
            "labelEng" => "Installation électrique",
            "category" => "Electricité",
            'visible' => true
        ],

        /**
         * => Electronique
         * Ingénieur en  électronique
         * Technicien en électronique
         */

        [
            "label_fr" => "Ingénieur en  électronique",
            "labelEng" => "Ingénieur en  électronique",
            "category" => "Electronique",
            'visible' => true
        ],
        [
            "label_fr" => "Technicien en électronique",
            "labelEng" => "Technicien en électronique",
            "category" => "Electronique",
            'visible' => true
        ],

        /**
         * => Environnement
         * Energie renouvelable
         * Biologiste en environnement
         * Animateur nature
         * Conseiller en environnement
         * Chargé d’hygiène et de sécurité
         */

        [
            "label_fr" => "Energie renouvelable",
            "labelEng" => "Energie renouvelable",
            "category" => "Environnement",
            'visible' => true
        ],
        [
            "label_fr" => "Biologiste en environnement",
            "labelEng" => "Biologiste en environnement",
            "category" => "Environnement",
            'visible' => true
        ],
        [
            "label_fr" => "Animateur nature",
            "labelEng" => "Animateur nature",
            "category" => "Environnement",
            'visible' => true
        ],
        [
            "label_fr" => "Conseiller en environnement",
            "labelEng" => "Conseiller en environnement",
            "category" => "Environnement",
            'visible' => true
        ],
        [
            "label_fr" => "Chargé d'hygiène et de sécurité",
            "labelEng" => "Chargé d'hygiène et de sécurité",
            "category" => "Environnement",
            'visible' => true
        ],

        /**
         * => Finance Banque et Assurance
         * Comptabilité
         * Trésorerie
         * Fiscalité
         * Analyse financière
         */

        [
            "label_fr" => "Comptabilité",
            "labelEng" => "Comptabilité",
            "category" => "Finance Banque et Assurance",
            'visible' => true
        ],
        [
            "label_fr" => "Trésorerie",
            "labelEng" => "Trésorerie",
            "category" => "Finance Banque et Assurance",
            'visible' => true
        ],
        [
            "label_fr" => "Fiscalité",
            "labelEng" => "Fiscalité",
            "category" => "Finance Banque et Assurance",
            'visible' => true
        ],
        [
            "label_fr" => "Analyse financière",
            "labelEng" => "Analyse financière",
            "category" => "Finance Banque et Assurance",
            'visible' => true
        ],

        /**
         * => Hôtellerie
         * Directeur d’hôtel
         * Agent d’Accueil
         * Voiturier
         * Responsable de salle
         * Femme de chambre
         */

        [
            "label_fr" => "Directeur d’hôtel",
            "labelEng" => "Directeur d’hôtel",
            "category" => "Hôtellerie",
            'visible' => true
        ],
        [
            "label_fr" => "Agent d’Accueil",
            "labelEng" => "Agent d’Accueil",
            "category" => "Hôtellerie",
            'visible' => true
        ],
        [
            "label_fr" => "Voiturier",
            "labelEng" => "Voiturier",
            "category" => "Hôtellerie",
            'visible' => true
        ],
        [
            "label_fr" => "Responsable de salle",
            "labelEng" => "Responsable de salle",
            "category" => "Hôtellerie",
            'visible' => true
        ],
        [
            "label_fr" => "Femme de chambre",
            "labelEng" => "Femme de chambre",
            "category" => "Hôtellerie",
            'visible' => true
        ],

        /**
         * IT Système d’information - Informatique
         * Informatique
         * Système d’information
         * Développement web
         * Développement mobile
         * Intelligence artificielle
         * Ingénierie en cyber sécurité
         * Webmaster
         */

        [
            "label_fr" => "Informatique",
            "labelEng" => "Informatique",
            "category" => "IT Système d'information - Informatique",
            'visible' => true
        ],
        [
            "label_fr" => "Système d'information",
            "labelEng" => "Système d'information",
            "category" => "IT Système d'information - Informatique",
            'visible' => true
        ],
        [
            "label_fr" => "Développement web",
            "labelEng" => "Développement web",
            "category" => "IT Système d'information - Informatique",
            'visible' => true
        ],
        [
            "label_fr" => "Développement mobile",
            "labelEng" => "Développement mobile",
            "category" => "IT Système d'information - Informatique",
            'visible' => true
        ],
        [
            "label_fr" => "Intelligence artificielle",
            "labelEng" => "Intelligence artificielle",
            "category" => "IT Système d'information - Informatique",
            'visible' => true
        ],
        [
            "label_fr" => "Ingénierie en cyber sécurité",
            "labelEng" => "Ingénierie en cyber sécurité",
            "category" => "IT Système d'information - Informatique",
            'visible' => true
        ],
        [
            "label_fr" => "Webmaster",
            "labelEng" => "Webmaster",
            "category" => "IT Système d'information - Informatique",
            'visible' => true
        ],

        /**
         * => Logistique  et approvisionnement
         * Acheteur
         * Technicien logistique
         * Approvisionneur
         * Cariste
         * Magasinier
         * Manutentionnaire
         * Logisticien
         */


        [
            "label_fr" => "Acheteur",
            "labelEng" => "Acheteur",
            "category" => "Logistique  et approvisionnement",
            'visible' => true
        ],
        [
            "label_fr" => "Technicien logistique",
            "labelEng" => "Technicien logistique",
            "category" => "Logistique  et approvisionnement",
            'visible' => true
        ],
        [
            "label_fr" => "Approvisionneur",
            "labelEng" => "Approvisionneur",
            "category" => "Logistique  et approvisionnement",
            'visible' => true
        ],
        [
            "label_fr" => "Cariste",
            "labelEng" => "Cariste",
            "category" => "Logistique  et approvisionnement",
            'visible' => true
        ],
        [
            "label_fr" => "Magasinier",
            "labelEng" => "Magasinier",
            "category" => "Logistique  et approvisionnement",
            'visible' => true
        ],
        [
            "label_fr" => "Manutentionnaire",
            "labelEng" => "Manutentionnaire",
            "category" => "Logistique  et approvisionnement",
            'visible' => true
        ],
        [
            "label_fr" => "Logisticien",
            "labelEng" => "Logisticien",
            "category" => "Logistique  et approvisionnement",
            'visible' => true
        ],

        /**
         * Management – International Business
         * Commerce
         * Commerce international
         * Communication
         * Gestion
         * Marketing
         * Management de projet
         */

        [
            "label_fr" => "Commerce",
            "labelEng" => "Commerce",
            "category" => "Management - International Business",
            'visible' => true
        ],
        [
            "label_fr" => "Commerce international",
            "labelEng" => "Commerce international",
            "category" => "Management - International Business",
            'visible' => true
        ],
        [
            "label_fr" => "Communication",
            "labelEng" => "Communication",
            "category" => "Management - International Business",
            'visible' => true
        ],
        [
            "label_fr" => "Gestion",
            "labelEng" => "Gestion",
            "category" => "Management - International Business",
            'visible' => true
        ],
        [
            "label_fr" => "Marketing",
            "labelEng" => "Marketing",
            "category" => "Management - International Business",
            'visible' => true
        ],
        [
            "label_fr" => "Management de projet",
            "labelEng" => "Management de projet",
            "category" => "Management - International Business",
            'visible' => true
        ],


        /**
         * => Mécanique
         * Mécanique d’industrie
         * Mécanique de voiture
         * Mécanicien Véhicules de transport
         * Débosseleur / Carrossier
         * Soudeur
         */

        [
            "label_fr" => "Mécanique d’industrie",
            "labelEng" => "Mécanique d’industrie",
            "category" => "Mécanique",
            'visible' => true
        ],
        [
            "label_fr" => "Mécanique de voiture",
            "labelEng" => "Mécanique de voiture",
            "category" => "Mécanique",
            'visible' => true
        ],
        [
            "label_fr" => "Mécanicien Véhicules de transport",
            "labelEng" => "Mécanicien Véhicules de transport",
            "category" => "Mécanique",
            'visible' => true
        ],
        [
            "label_fr" => "Débosseleur / Carrossier",
            "labelEng" => "Débosseleur / Carrossier",
            "category" => "Mécanique",
            'visible' => true
        ],
        [
            "label_fr" => "Soudeur",
            "labelEng" => "Soudeur",
            "category" => "Mécanique",
            'visible' => true
        ],

        /**
         * Mine
         *  Ingénieur minier
         * Inspecteur des mines
         * Foreur
         * Mineur
         */

        [
            "label_fr" => "Ingénieur minier",
            "labelEng" => "Ingénieur minier",
            "category" => "Mine",
            'visible' => true
        ],
        [
            "label_fr" => "Inspecteur des mines",
            "labelEng" => "Inspecteur des mines",
            "category" => "Mine",
            'visible' => true
        ],
        [
            "label_fr" => "Foreur",
            "labelEng" => "Foreur",
            "category" => "Mine",
            'visible' => true
        ],
        [
            "label_fr" => "Mineur",
            "labelEng" => "Mineur",
            "category" => "Mine",
            'visible' => true
        ],

        /**
         * => Restauration
         * Chef de cuisine
         * Cuisinier (ère)
         * Commis
         * Serveur
         */

        [
            "label_fr" => "Chef de cuisine",
            "labelEng" => "Chef de cuisine",
            "category" => "Restauration",
            'visible' => true
        ],
        [
            "label_fr" => "Cuisinier (ère)",
            "labelEng" => "Cuisinier (ère)",
            "category" => "Restauration",
            'visible' => true
        ],
        [
            "label_fr" => "Commis",
            "labelEng" => "Commis",
            "category" => "Restauration",
            'visible' => true
        ],
        [
            "label_fr" => "Serveur",
            "labelEng" => "Serveur",
            "category" => "Restauration",
            'visible' => true
        ],


        /**
         * => Sciences
         * Sciences
         * Sciences humaines
         * Sciences politiques
         * Sciences vétérinaires
         * Sciences dentaires
         * Sciences biomédicales et pharmaceutiques
         * Sciences psychologiques
         */

        [
            "label_fr" => "Sciences",
            "labelEng" => "Sciences",
            "category" => "Sciences",
            'visible' => true
        ],
        [
            "label_fr" => "Sciences humaines",
            "labelEng" => "Sciences humaines",
            "category" => "Sciences",
            'visible' => true
        ],
        [
            "label_fr" => "Sciences politiques",
            "labelEng" => "Sciences politiques",
            "category" => "Sciences",
            'visible' => true
        ],
        [
            "label_fr" => "Sciences vétérinaires",
            "labelEng" => "Sciences vétérinaires",
            "category" => "Sciences",
            'visible' => true
        ],
        [
            "label_fr" => "Sciences dentaires",
            "labelEng" => "Sciences dentaires",
            "category" => "Sciences",
            'visible' => true
        ],
        [
            "label_fr" => "Sciences biomédicales et pharmaceutiques",
            "labelEng" => "Sciences biomédicales et pharmaceutiques",
            "category" => "Sciences",
            'visible' => true
        ],
        [
            "label_fr" => "Sciences psychologiques",
            "labelEng" => "Sciences psychologiques",
            "category" => "Sciences",
            'visible' => true
        ],

        /**
         * => Sociologie
         * Sociologie
         * Géopolitique
         */

        [
            "label_fr" => "Sociologie",
            "labelEng" => "Sociologie",
            "category" => "Sociologie",
            'visible' => true
        ],
        [
            "label_fr" => "Géopolitique",
            "labelEng" => "Géopolitique",
            "category" => "Sociologie",
            'visible' => true
        ],

        /**
         * => Stylisme et Mode
         * Conseiller en mode
         * Designer en textile
         * Coordinateur de mode
         * Styliste
         */

        [
            "label_fr" => "Conseiller en mode",
            "labelEng" => "Conseiller en mode",
            "category" => "Stylisme et Mode",
            'visible' => true
        ],
        [
            "label_fr" => "Designer en textile",
            "labelEng" => "Designer en textile",
            "category" => "Stylisme et Mode",
            'visible' => true
        ],
        [
            "label_fr" => "Coordinateur de mode",
            "labelEng" => "Coordinateur de mode",
            "category" => "Stylisme et Mode",
            'visible' => true
        ],
        [
            "label_fr" => "Styliste",
            "labelEng" => "Styliste",
            "category" => "Stylisme et Mode",
            'visible' => true
        ],


    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->fields as $field) {
            ProgramField::create(
                $field
            );
        }
    }
}
