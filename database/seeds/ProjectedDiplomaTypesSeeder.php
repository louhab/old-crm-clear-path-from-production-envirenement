<?php

use App\Models\ProjectedDiplomaType;
use Illuminate\Database\Seeder;

class ProjectedDiplomaTypesSeeder extends Seeder
{
    private $diplomaTypes = [
        ['diploma'=> 'MIAGE'],
        ['diploma'=> 'DOCTORAT'],
        ['diploma'=> 'MBS']
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->diplomaTypes as $diplomaType) {
            ProjectedDiplomaType::create(
                [
                    'projected_diploma_type_name' => $diplomaType['diploma']
                ]
            );
        }
    }
}
