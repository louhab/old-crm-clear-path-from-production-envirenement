<?php

use Illuminate\Database\Seeder;

use App\Models\EnglishTest;

class EnglishTestSeeder extends Seeder
{
    // Toffl, Toiec
    private $tests = [
        ['en' => 'TOFFL', 'fr' => 'TOFFL'],
        ['en' => 'TOEIC', 'fr' => 'TOEIC'],
        ['en' => 'No', 'fr' => 'Non'],
        ['en' => 'To do', 'fr' => 'à faire'],
        ['en' => 'IELTS', 'fr' => 'IELTS'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*foreach ($this->tests as $test) {
            // $translations = ['en' => 'No', 'fr' => 'Non'];
            EnglishTest::create(
                [
                    'test_label' => $test['label'],
                ]
            );
        }*/
        $i = 0;
        foreach (EnglishTest::orderBy("id")->get() as $test) {
            $test->test_label = $this->tests[$i];
            $test->save();
            $i++;
        }
    }
}
