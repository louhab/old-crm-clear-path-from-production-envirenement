<?php

use Illuminate\Database\Seeder;

use App\Models\AdmissionCity;

class AdmissionCitySeeder extends Seeder
{
    private $cities = [
        ["Moncton","Moncton"],
        ["Montréal","Montreal"],
        ["Ontario","Ontario"],
        ["Ottawa","Ottawa"],
        ["Québec","Quebec"],
        ["Rimouski","Rimouski"],
        ["Sainte Anne","Sainte Anne"],
        ["Trois Rivières","Trois Rivières"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->cities as $city) {
            AdmissionCity::create(
                [
                    'label' => $city[0],
                    'labelEng'=>$city[1]
                ]
            );

            // $levelsToAttach = [];

            // foreach ($city[1] as $city) {
            //     $levelsToAttach[$city] = [
            //         //'lang_city_id' =>
            //         'order' => $city
            //     ];
            // }

            // $city_->levels()->sync($levelsToAttach);
        }
    }
}
