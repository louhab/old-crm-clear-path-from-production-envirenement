<?php

use Illuminate\Database\Seeder;

class Clear3QuestioningSeeder extends Seeder
{
    private $labels = [
        ["label_fr" => "Oui", "label_en" => "Yes"],
        ["label_fr" => "Non", "label_en" => "No"],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->labels as $label) {
            \App\Models\Clear3Questioning::create($label);
        }
    }
}
