<?php

use Illuminate\Database\Seeder;

class StepStatesSeeder extends Seeder
{
    private $states = [

        //Analyse du Dossier
        //['program_step_id'=> 1, 'select_box_label_fr' => 'Acceptée', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Oui', 'customer_message_en' => 'Yes', 'customer_message_style_class' => 'success'],

        //['program_step_id'=> 1, 'select_box_label_fr' => 'Refusée', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Non', 'customer_message_en' => 'No', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande d'admission
        // ['program_step_id'=> 2, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

        // ['program_step_id'=> 2, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

        // ['program_step_id'=> 2, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande de visa
        ['program_step_id'=> 10, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 10, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 10, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],

    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->states as $state) {
            \App\Models\StepState::create(
                $state
            );
        }
    }
}
