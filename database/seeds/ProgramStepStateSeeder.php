<?php

use Illuminate\Database\Seeder;

class ProgramStepStateSeeder extends Seeder
{
    private $states = [

        //Soumission de la demande d'admission
        ['program_step_id'=> 1, 'program_id'=> 1, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 1, 'program_id'=> 1, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 1, 'program_id'=> 1, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande de visa
        ['program_step_id'=> 10, 'program_id'=> 1, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 10, 'program_id'=> 1, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 10, 'program_id'=> 1, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande d'admission
        ['program_step_id'=> 1, 'program_id'=> 5, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 1, 'program_id'=> 5, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 1, 'program_id'=> 5, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande de visa
        ['program_step_id'=> 10, 'program_id'=> 5, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 10, 'program_id'=> 5, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 10, 'program_id'=> 5, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande d'admission
        ['program_step_id'=> 1, 'program_id'=> 6, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 1, 'program_id'=> 6, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 1, 'program_id'=> 6, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande de visa
        ['program_step_id'=> 10, 'program_id'=> 6, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 10, 'program_id'=> 6, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 10, 'program_id'=> 6, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande d'admission
        ['program_step_id'=> 1, 'program_id'=> 7, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 1, 'program_id'=> 7, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 1, 'program_id'=> 7, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande de visa
        ['program_step_id'=> 10, 'program_id'=> 7, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 10, 'program_id'=> 7, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 10, 'program_id'=> 7, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande d'admission
        ['program_step_id'=> 1, 'program_id'=> 8, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 1, 'program_id'=> 8, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 1, 'program_id'=> 8, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande de visa
        ['program_step_id'=> 10, 'program_id'=> 8, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 10, 'program_id'=> 8, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 10, 'program_id'=> 8, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande d'admission
        ['program_step_id'=> 1, 'program_id'=> 9, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 1, 'program_id'=> 9, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 1, 'program_id'=> 9, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

            //Soumission de la demande de visa
            ['program_step_id'=> 10, 'program_id'=> 9, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

            ['program_step_id'=> 10, 'program_id'=> 9, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

            ['program_step_id'=> 10, 'program_id'=> 9, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],

            //Soumission de la demande d'admission
            ['program_step_id'=> 1, 'program_id'=> 10 , 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

            ['program_step_id'=> 1, 'program_id'=> 10 , 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

            ['program_step_id'=> 1, 'program_id'=> 10 , 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

            //Soumission de la demande de visa
            ['program_step_id'=> 10, 'program_id'=> 10 , 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

            ['program_step_id'=> 10, 'program_id'=> 10 , 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

            ['program_step_id'=> 10, 'program_id'=> 10 , 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],

            //Soumission de la demande d'admission
            ['program_step_id'=> 1, 'program_id'=> 11, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

            ['program_step_id'=> 1, 'program_id'=> 11, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

            ['program_step_id'=> 1, 'program_id'=> 11, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

            //Soumission de la demande de visa
            ['program_step_id'=> 10, 'program_id'=> 11, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

            ['program_step_id'=> 10, 'program_id'=> 11, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

            ['program_step_id'=> 10, 'program_id'=> 11, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],

            //Soumission de la demande d'admission
            ['program_step_id'=> 1, 'program_id'=> 12, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

            ['program_step_id'=> 1, 'program_id'=> 12, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

            ['program_step_id'=> 1, 'program_id'=> 12, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

            //Soumission de la demande de visa
            ['program_step_id'=> 10, 'program_id'=> 12, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

            ['program_step_id'=> 10, 'program_id'=> 12, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

            ['program_step_id'=> 10, 'program_id'=> 12, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],

            //Soumission de la demande d'admission
            ['program_step_id'=> 1, 'program_id'=> 13, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

            ['program_step_id'=> 1, 'program_id'=> 13, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

            ['program_step_id'=> 1, 'program_id'=> 13, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

            //Soumission de la demande de visa
            ['program_step_id'=> 10, 'program_id'=> 13 , 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

            ['program_step_id'=> 10, 'program_id'=> 13 , 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

            ['program_step_id'=> 10, 'program_id'=> 13 , 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],

            //Soumission de la demande d'admission
         ['program_step_id'=> 1, 'program_id'=> 14, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

         ['program_step_id'=> 1, 'program_id'=> 14, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

         ['program_step_id'=> 1, 'program_id'=> 14, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

         //Soumission de la demande de visa
         ['program_step_id'=> 10, 'program_id'=> 14 , 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

         ['program_step_id'=> 10, 'program_id'=> 14 , 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

         ['program_step_id'=> 10, 'program_id'=> 14 , 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],
          //Soumission de la demande d'admission
        ['program_step_id'=> 1, 'program_id'=> 15, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 1, 'program_id'=> 15, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 1, 'program_id'=> 15, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande de visa
        ['program_step_id'=> 10, 'program_id'=> 15, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 10, 'program_id'=> 15, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 10, 'program_id'=> 15, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],
         //Soumission de la demande d'admission
         ['program_step_id'=> 1, 'program_id'=> 16 , 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

         ['program_step_id'=> 1, 'program_id'=> 16 , 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

         ['program_step_id'=> 1, 'program_id'=> 16 , 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

         //Soumission de la demande de visa
         ['program_step_id'=> 10, 'program_id'=> 16 , 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

         ['program_step_id'=> 10, 'program_id'=> 16 , 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

         ['program_step_id'=> 10, 'program_id'=> 16 , 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],
          //Soumission de la demande d'admission
        ['program_step_id'=> 1, 'program_id'=> 17, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 1, 'program_id'=> 17, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 1, 'program_id'=> 17, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande de visa
        ['program_step_id'=> 10, 'program_id'=> 17 , 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 10, 'program_id'=> 17 , 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 10, 'program_id'=> 17 , 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],
         //Soumission de la demande d'admission
         ['program_step_id'=> 1, 'program_id'=> 18, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

         ['program_step_id'=> 1, 'program_id'=> 18, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

         ['program_step_id'=> 1, 'program_id'=> 18, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

         //Soumission de la demande de visa
         ['program_step_id'=> 10, 'program_id'=> 18, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

         ['program_step_id'=> 10, 'program_id'=> 18, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

         ['program_step_id'=> 10, 'program_id'=> 18, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],
          //Soumission de la demande d'admission
        ['program_step_id'=> 1, 'program_id'=> 19, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 1, 'program_id'=> 19, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 1, 'program_id'=> 19, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande de visa
        ['program_step_id'=> 10, 'program_id'=> 19 , 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 10, 'program_id'=> 19 , 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 10, 'program_id'=> 19 , 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],
         //Soumission de la demande d'admission
         ['program_step_id'=> 1, 'program_id'=> 20 , 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

         ['program_step_id'=> 1, 'program_id'=> 20 , 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

         ['program_step_id'=> 1, 'program_id'=> 20 , 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

         //Soumission de la demande de visa
         ['program_step_id'=> 10, 'program_id'=> 20, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

         ['program_step_id'=> 10, 'program_id'=> 20, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

         ['program_step_id'=> 10, 'program_id'=> 20, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],
          //Soumission de la demande d'admission
        ['program_step_id'=> 1, 'program_id'=> 21 , 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 1, 'program_id'=> 21 , 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 1, 'program_id'=> 21 , 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande de visa
        ['program_step_id'=> 10, 'program_id'=> 21 , 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 10, 'program_id'=> 21 , 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 10, 'program_id'=> 21 , 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],
         //Soumission de la demande d'admission
         ['program_step_id'=> 1, 'program_id'=> 22 , 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

         ['program_step_id'=> 1, 'program_id'=> 22 , 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

         ['program_step_id'=> 1, 'program_id'=> 22 , 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

         //Soumission de la demande de visa
         ['program_step_id'=> 10, 'program_id'=> 22 , 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

         ['program_step_id'=> 10, 'program_id'=> 22 , 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

         ['program_step_id'=> 10, 'program_id'=> 22 , 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],
          //Soumission de la demande d'admission
        ['program_step_id'=> 1, 'program_id'=> 23, 'select_box_label_fr' => 'Demande d\'admission en cours', 'select_box_label_en' => 'Admission request in progress', 'customer_message_fr' => 'Votre demande d\'admission est en cours', 'customer_message_en' => 'Your admission request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 1, 'program_id'=> 23, 'select_box_label_fr' => 'Demande d\'admission acceptée', 'select_box_label_en' => 'Admission request accepted', 'customer_message_fr' => 'Votre demande d\'admission est acceptée', 'customer_message_en' => 'Your admission request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 1, 'program_id'=> 23, 'select_box_label_fr' => 'Demande d\'admission refusée', 'select_box_label_en' => 'Admission request refused', 'customer_message_fr' => 'Votre demande d\'admission est refusée', 'customer_message_en' => 'Your admission request is refused', 'customer_message_style_class' => 'danger'],

        //Soumission de la demande de visa
        ['program_step_id'=> 10, 'program_id'=> 23, 'select_box_label_fr' => 'Demande de visa en cours', 'select_box_label_en' => 'Visa request in progress', 'customer_message_fr' => 'Votre demande de visa est en cours', 'customer_message_en' => 'Your visa request is in progress', 'customer_message_style_class' => 'info'],

        ['program_step_id'=> 10, 'program_id'=> 23, 'select_box_label_fr' => 'Demande de visa acceptée', 'select_box_label_en' => 'Visa request accepted', 'customer_message_fr' => 'Votre demande de visa est acceptée', 'customer_message_en' => 'Your visa request is accepted', 'customer_message_style_class' => 'success'],

        ['program_step_id'=> 10, 'program_id'=> 23, 'select_box_label_fr' => 'Demande de visa refusée', 'select_box_label_en' => 'Visa request refused', 'customer_message_fr' => 'Votre demande de visa est refusée', 'customer_message_en' => 'Your visa request is refused', 'customer_message_style_class' => 'danger'],



    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->states as $state) {
            $pgm = \App\Models\ProgramStepState::where('program_step_id', $state['program_step_id'])
                ->where('program_id', $state['program_id'])
                ->where('select_box_label_fr', $state['select_box_label_fr'])
                ->first();
            if (!$pgm) {
                \App\Models\ProgramStepState::create(
                    $state
                );
            }
        }
    }
}
