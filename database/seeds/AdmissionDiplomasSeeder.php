<?php

use Illuminate\Database\Seeder;

use App\Models\AdmissionDiplomas;

class AdmissionDiplomasSeeder extends Seeder
{
    private $dps = [
        ["label" => "Baccalauréat","labelEng"=> "Baccalaureate"],//1
        ["label" => "DEC (Diplôme d'étude collégial)","labelEng"=> "DEC/DCS (Diplomas)"],//2
        ["label" => "DEP (Diplôme d'étude professionnel)","labelEng"=> "DEP (Professional Studies Diploma)"],//3
        ["label" => "DESS","labelEng"=> "DESS"],//4
        ["label" => "Doctorat","labelEng"=> "Doctorate"],//5
        ["label" => "Maîtrise","labelEng"=> "Master's Degree"],//6
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->dps as $dp) {
            AdmissionDiplomas::create($dp);
        }
    }
}
