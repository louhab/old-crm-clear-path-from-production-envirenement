<?php

use App\Models\MonthlyIncomeType;
use Illuminate\Database\Seeder;

class MonthlyIncomeTypeSeeder extends Seeder
{
    private $montly_income_types = [
        ['label_fr'=> "revenu de travail", 'label_en'=> "work income"],
        ['label_fr'=> "revenu de retraite", 'label_en'=> "retirement income"],
        ['label_fr'=> "revenu d'entreprise", 'label_en'=> "business income"],
        ['label_fr'=> "revenu de profession libérale", 'label_en'=> "professional income"],
        ['label_fr'=> "revenu de travailleur autonome", 'label_en'=> "self-employed income"],
        ['label_fr'=> "revenu locatif", 'label_en'=> "rental income"],
        ['label_fr'=> "revenu d'investissement et placement", 'label_en'=> "investment income and placement"],
        ['label_fr'=> "autres revenus", 'label_en'=> "autres revenus"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->montly_income_types as $type) {
            MonthlyIncomeType::create(
                [
                    'label_fr' => $type['label_fr'],
                    'label_en' => $type['label_en'],
                ]
            );
        }
    }
}
