<?php

use Illuminate\Database\Seeder;

use App\Models\WorkExperience;

class WorkExperienceSeeder extends Seeder
{
    private $scores = [
        ['code'=> 'A', 'label'=> 'Aucune ou moins d\'un an',"labelEng"=>"None or less than a year"],
        ['code'=> 'B', 'label'=> '1 an'                    ,"labelEng"=>"1 year"],
        ['code'=> 'C', 'label'=> '2 ans'                   ,"labelEng"=>"2 years"],
        ['code'=> 'D', 'label'=> '3 ans'                   ,"labelEng"=>"3 years"],
        ['code'=> 'E', 'label'=> '4 ans'                   ,"labelEng"=>"4 years"],
        ['code'=> 'F', 'label'=> '5 ans ou plus'           ,"labelEng"=>"5 years or more"],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->scores as $phase) {
            WorkExperience::where("label", "LIKE", $phase["label"])
                ->update(['labelEng' => $phase["labelEng"]]);
        }*/

        foreach ($this->scores as $score) {
            WorkExperience::create(
                [
                    'label' => $score['label'],
                    'code' => $score['code'],
                    'labelEng' => $score['labelEng']
                ]
            );
        }
    }
}
/*
<select id="q6i" name="q6i" class="form-control mrgn-bttm-md">
    <option value="badvalue" label="Selectionner...">Selectionner...</option>
    <option value="A">Aucune ou moins d'un an</option>
    <option value="B">1 an</option>
    <option value="C">2 ans</option>
    <option value="D">3 ans</option>
    <option value="E">4 ans</option>
    <option value="F">5 ans ou plus</option>
</select>
*/
