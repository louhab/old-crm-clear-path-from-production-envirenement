<?php
use App\Models\Service;
use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{

    private $billableArray = [
        1, 4, 6, 9, 14, 15, //IMMIGRANT
        16, 17, 19, 20, 23, 25, 26 //STUDENT
    ];

    private $editableQteArray = [13, 14, 26];

    private $services = [

        //IMMIGRANT

        //Phase 1
        //Phase 1
        [
            'id' => 1,
            'phase_id'=> 1,
            'program_payment_id' => 1,
            'name' => 'Rencontre 1 : Analyse des données et admissibilité',
            'name_en' => 'Meeting 1: Data Analysis and eligibility',
            'single_price' => 200,
            'couple_price' => 200,
            'order' => 2,
            'translations' => [
                'single_price' => [
                    'EUR' => 50,
                    'USD' => 50,
                ],
                'couple_price' => [
                    'EUR' => 50,
                    'USD' => 50,
                ],
            ]
        ],
        //Phase 2
        [
            'id' => 2,
            'phase_id'=> 2,
            'program_payment_id' => 2,
            'name' => 'Rencontre 2 : Préparation de la demande d\'équivalence et les tests de langues',
            'name_en' => 'Meeting 2: Preparation of the equivalence request and language tests',
            'single_price' => 700,
            'couple_price' => 900,
            'order' => 4,
            'translations' => [
                'single_price' => [
                    'EUR' => 130,
                    'USD' => 100,
                ],
                'couple_price' => [
                    'EUR' => 130,
                    'USD' => 100,
                ],
            ]
        ],
        [
            'id' => 3,
            'phase_id'=> 2,
            'program_payment_id' => 2,
            'name' => 'Frais WES et frais d\'envoi avec DHL',
            'name_en' => 'WES fees and shipping costs (with DHL)',
            'single_price' => 2600,
            'couple_price' => 2600,
            'optional' => 1,
            'order' => 6,
            'translations' => [
                'single_price' => [
                    'EUR' => 270,
                    'USD' => 270,
                ],
                'couple_price' => [
                    'EUR' => 270,
                    'USD' => 270,
                ],
            ]
        ],
        [
            'id' => 4,
            'phase_id'=> 2,
            'program_payment_id' => 2,
            'name' => 'TCF Canada',
            'name_en' => 'TCF Canada',
            'single_price' => 2750,
            'couple_price' => 2750,
            'optional' => 1,
            'order' => 8,
            'translations' => [
                'single_price' => [
                    'EUR' => 270,
                    'USD' => 280,
                ],
                'couple_price' => [
                    'EUR' => 270,
                    'USD' => 280,
                ],
            ]
        ],
        [
            'id' => 5,
            'phase_id'=> 2,
            'program_payment_id' => 2,
            'name' => 'TEF Canada',
            'name_en' => 'TEF Canada',
            'single_price' => 2750,
            'couple_price' => 2750,
            'optional' => 1,
            'order' => 10,
            'translations' => [
                'single_price' => [
                    'EUR' => 270,
                    'USD' => 280,
                ],
                'couple_price' => [
                    'EUR' => 270,
                    'USD' => 280,
                ],
            ]
        ],
        [
            'id' => 6,
            'phase_id'=> 2,
            'program_payment_id' => 2,
            'name' => 'IELTS',
            'name_en' => 'IELTS',
            'single_price' => 2750,
            'couple_price' => 2750,
            'optional' => 1,
            'order' => 12,
            'translations' => [
                'single_price' => [
                    'EUR' => 270,
                    'USD' => 280,
                ],
                'couple_price' => [
                    'EUR' => 270,
                    'USD' => 280,
                ],
            ]
        ],
        [
            'id' => 7,
            'phase_id'=> 2,
            'program_payment_id' => 3,
            'name' => 'Rencontre 3 : Préparation de la demande du profil Entrée Express',
            'name_en' => 'Meeting 3: Preparation of the Express Entry profile application',
            'single_price' => 5000,
            'couple_price' => 7500,
            'order' => 14,
            'translations' => [
                'single_price' => [
                    'EUR' => 850,
                    'USD' => 600,
                ],
                'couple_price' => [
                    'EUR' => 850,
                    'USD' => 850,
                ],
            ]
        ],
        //Phase 3
        [
            'id' => 8,
            'phase_id'=> 3,
            'program_payment_id' => 4,
            'name' => 'Rencontre 4 : Préparation de la demande du Visa & RP',
            'name_en' => 'Meeting 4: Preparation of the Visa & PR application',
            'single_price' => 5000,
            'couple_price' => 6000,
            'order' => 16,
            'translations' => [
                'single_price' => [
                    'EUR' => 700,
                    'USD' => 600,
                ],
                'couple_price' => [
                    'EUR' => 700,
                    'USD' => 700,
                ],
            ]
        ],
        [
            'id' => 9,
            'phase_id'=> 3,
            'program_payment_id' => 5,
            'name' => 'Rencontre 5 : Soumission de la demande du Visa & RP',
            'name_en' => 'Meeting 5: Visa & PR request presentation',
            'single_price' => 5000,
            'couple_price' => 6000,
            'order' => 18,
            'translations' => [
                'single_price' => [
                    'EUR' => 700,
                    'USD' => 600,
                ],
                'couple_price' => [
                    'EUR' => 700,
                    'USD' => 700,
                ],
            ]
        ],
        [
            'id' => 10,
            'phase_id'=> 3,
            'program_payment_id' => 5,
            'name' => 'Frais de la demande d\'immigration',
            'name_en' => 'Immigration application fee',
            'single_price' => 3800,
            'couple_price' => 3800,
            'optional' => 1,
            'order' => 20,
            'translations' => [
                'single_price' => [
                    'EUR' => 400,
                    'USD' => 400,
                ],
                'couple_price' => [
                    'EUR' => 400,
                    'USD' => 400,
                ],
            ]
        ],
        [
            'id' => 11,
            'phase_id'=> 3,
            'program_payment_id' => 5,
            'name' => 'Frais de la visite médicale',
            'name_en' => 'Medical visit fee',
            'single_price' => 1550,
            'couple_price' => 1550,
            'optional' => 1,
            'order' => 22,
            'translations' => [
                'single_price' => [
                    'EUR' => 200,
                    'USD' => 160,
                ],
                'couple_price' => [
                    'EUR' => 200,
                    'USD' => 160,
                ],
            ]
        ],
        [
            'id' => 12,
            'phase_id'=> 3,
            'program_payment_id' => 5,
            'name' => 'Biométrie, Frais de visa et résidence permanente',
            'name_en' => 'Biometrics, visa fees and permanent residency',
            'single_price' => 6300,
            'couple_price' => 6300,
            'optional' => 1,
            'order' => 24,
            'translations' => [
                'single_price' => [
                    'EUR' => 750,
                    'USD' => 640,
                ],
                'couple_price' => [
                    'EUR' => 750,
                    'USD' => 640,
                ],
            ]
        ],
        //Options
        [
            'id' => 13,
            'phase_id'=> 4,
            'name' => 'Package : Accueil, intégration, conseils et encadrement au Canada',
            'name_en' => 'Welcome package: Welcome, integration, advice and support in Canada',
            'single_price' => 1450,
            'couple_price' => 1450,
            'order' => 26,
            'translations' => [
                'single_price' => [
                    'EUR' => 160,
                    'USD' => 145,
                ],
                'couple_price' => [
                    'EUR' => 160,
                    'USD' => 145,
                ],
            ]
        ],
        [
            'id' => 14,
            'phase_id'=> 4,
            'name' => 'Séance de coaching par heure au Canada',
            'name_en' => 'Coaching session in Canada (per hour)',
            'single_price' => 700,
            'couple_price' => 700,
            'order' => 28,
            'translations' => [
                'single_price' => [
                    'EUR' => 70,
                    'USD' => 70,
                ],
                'couple_price' => [
                    'EUR' => 70,
                    'USD' => 70,
                ],
            ]
        ],


        //ETUDIANT

        //Phase 1
        [
            'id' => 16,
            'phase_id'=> 5,
            'program_payment_id' => 1,
            'name' => 'Rencontre 1 : Analyse des données et admissibilité',
            'name_en' => "Meeting 1: Data Analysis and Eligibility",
            'single_price' => 200 ,
            'couple_price' => null,
            'order' => 2,
            'translations' => [
                'single_price' => [
                    'EUR' => 50,
                    'USD' => 50,
                ],
            ]
        ],
        //Phase 2
        [
            'id' => 17,
            'phase_id'=> 6,
            'program_payment_id' => 2,
            'name' => 'Rencontre 2 : Préparation de la demande d\'admission',
            'name_en' => 'Meeting 2: Preparation of the admission request',
            'single_price' => 2800,
            'couple_price' => null,
            'order' => 4,
            'translations' => [
                'single_price' => [
                    'EUR' => 280,
                    'USD' => 280,
                ],
            ]
        ],
        [
            'id' => 18,
            'phase_id'=> 6,
            'program_payment_id' => 3,
            'name' => 'Rencontre 3 : Soumission de la demande d\'admission',
            'name_en' => 'Meeting 3: Admission application presentation',
            'single_price' => 5000,
            'couple_price' => null,
            'order' => 6,
            'translations' => [
                'single_price' => [
                    'EUR' => 500,
                    'USD' => 500,
                ],
            ]
        ],
        [
            'id' => 19,
            'phase_id'=> 6,
            'program_payment_id' => 3,
            'name' => 'Frais des admissions collégiales et universitaires',
            'name_en' => 'College and university admissions fees',
            'single_price' => 2500,
            'couple_price' => null,
            'optional' => 1,
            'order' => 8,
            'translations' => [
                'single_price' => [
                    'EUR' => 250,
                    'USD' => 250,
                ],
            ]
        ],
        //Phase 3
        [
            'id' => 20,
            'phase_id'=> 7,
            'program_payment_id' => 4,
            'name' => 'Rencontre 4 : Préparation de la demande du Visa & PE',
            'name_en' => 'Meeting 4: Preparation of the Visa & PE application',
            'single_price' => 6000 ,
            'couple_price' => null,
            'order' => 10,
            'translations' => [
                'single_price' => [
                    'EUR' => 600,
                    'USD' => 600,
                ],
            ]
        ],
        [
            'id' => 21,
            'phase_id'=> 7,
            'program_payment_id' => 5,
            'name' => 'Rencontre 5 : Soumission de la demande du Visa & PE',
            'name_en' => 'Meeting 5: Visa & PE application presentation',
            'single_price' => 6000 ,
            'couple_price' => null,
            'order' => 12,
            'translations' => [
                'single_price' => [
                    'EUR' => 600,
                    'USD' => 600,
                ],
            ]
        ],
        [
            'id' => 22,
            'phase_id'=> 7,
            'program_payment_id' => 5,
            'name' => 'Biométrie, Frais de visa et de permis d\'études',
            'name_en' => 'Biometrics, Visa and study permit fees',
            'single_price' => 1900 ,
            'couple_price' => null,
            'optional' => 1,
            'order' => 14,
            'translations' => [
                'single_price' => [
                    'EUR' => 200,
                    'USD' => 200,
                ],
            ]
        ],
        //Options
        [
            'id' => 23,
            'phase_id'=> 8,
            'name' => 'Frais de la demande de CAQ',
            'name_en' => 'CAQ application fee',
            'single_price' => 1000,
            'couple_price' => null,
            'order' => 16,
            'translations' => [
                'single_price' => [
                    'EUR' => 110,
                    'USD' => 110,
                ],
            ]
        ],
        [
            'id' => 24,
            'phase_id'=> 8,
            'name' => 'Resoumission de la demande de visa et permis d\'études',
            'name_en' => 'Visa application and study permit presentation (again)',
            'single_price' => 3000,
            'couple_price' => null,
            'order' => 18,
            'translations' => [
                'single_price' => [
                    'EUR' => 300,
                    'USD' => 300,
                ],
            ]
        ],
        [
            'id' => 25,
            'phase_id'=> 8,
            'name' => 'Test de langue',
            'name_en' => 'Language test',
            'single_price' => 2800,
            'couple_price' => null,
            'order' => 20,
            'translations' => [
                'single_price' => [
                    'EUR' => 280,
                    'USD' => 280,
                ],
            ]
        ],
        [
            'id' => 26,
            'phase_id'=> 8,
            'name' => 'Package : Accueil, intégration, conseils et encadrement au Canada',
            'name_en' => 'Package: Welcome, integration, advice and support in Canada',
            'single_price' => 1450,
            'couple_price' => null,
            'order' => 22,
            'translations' => [
                'single_price' => [
                    'EUR' => 160,
                    'USD' => 160,
                ],
            ]
        ],
        [
            'id' => 27,
            'phase_id'=> 8,
            'name' => 'Séance de coaching par heure au Canada',
            'name_en' => 'Coaching session per hour in Canada',
            'single_price' => 700,
            'couple_price' => null,
            'order' => 24,
            'translations' => [
                'single_price' => [
                    'EUR' => 70,
                    'USD' => 70,
                ],
            ]
        ],

    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        /*foreach ($this->services as $phase) {
            Service::where("name", "LIKE", $phase["name"])
                ->update(['labelEng' => $phase["labelEng"]]);
        }*/

        foreach ($this->services as $service) {
            $translations = $service["translations"];
            $translations["single_price"]["MAD"] = $service["single_price"];
            $translations["couple_price"]["MAD"] = $service["couple_price"];
            unset($service["single_price"]);
            unset($service["couple_price"]);
            unset($service["translations"]);
            $service['program_id'] = $this->getProgramId($service['id']);
            $service['billable'] = 0;
            $service['can_edit_qte'] = 0;
            // $service['single_price'] = strval($service['single_price']);
            // $service['couple_price'] = strval($service['couple_price']);

            $s = Service::create(
                $service
            );
            // $s->setTranslation('name', 'en', 'Updated name in English');

            /*$translations = [
                'EUR' => ,
                'USD' => ,
            ];*/
            foreach ($translations as $key => $translation) {
                $s->setTranslations($key, $translation);
            }
            $s->save();
        }

    }

    /**[
    'id' => $service['id'],
    'phase_id' => $service['phase_id'],
    'program_id' => $this->getProgramId($service['id']),
    'program_payment_id' => $service['program_payment_id'],
    'name' => $service['name'],
    'name_en'=>$service['name_en'],
    'single_price' => $service['single_price'],
    'couple_price' => $service['couple_price'],
    'billable' => 0,
    'can_edit_qte' => 0,
    'order' => $service['order']
    ]
     * Return program Id.
     *
     * @return unsigned integer
     */

    private function getProgramId($c) {
        return $c <= 15 ? 2 : 1;
    }

    /**
     * Return billable state.
     *
     * @return boolean
     */

    private function isBillable($c) {
        return in_array($c, $this->billableArray);
    }

    /**
     * Return editable Qte state.
     *
     * @return boolean
     */

    private function isEditableQTE($c) {
        return in_array($c, $this->editableQteArray);
    }
}
