<?php

use Illuminate\Database\Seeder;

use App\Models\AdmissionEtabName;

class AdmissionEtabNameSeeder extends Seeder
{
    private $names = [
        ["label" => "UQAM",          "labelEng"=>"UQAM" ,"url" => "https://uqam.ca"],//1
        ["label" => "UDEM",          "labelEng"=>"UDEM" ,"url" => "https://www.umontreal.ca"],//2
        ["label" => "ETS",           "labelEng"=>"ETS" ,"url" => "https://www.etsmtl.ca"],//3
        ["label" => "HEC",           "labelEng"=>"HEC" ,"url" => "https://www.hec.ca"],//4
        ["label" => "ENAP",          "labelEng"=>"ENAP" ,"url" => "https://www.enap.ca/enap/fr/accueil.aspx"],//5
        ["label" => "CONCORDIA",     "labelEng"=>"CONCORDIA" ,"url" => "https://www.concordia.ca"],//6
        ["label" => "MCGILL",        "labelEng"=>"MCGILL" ,"url" => "https://www.mcgill.ca"],//7
        ["label" => "UOTTAWA",       "labelEng"=>"UOTTAWA" ,"url" => "https://www.uottawa.ca/en"],//8
        ["label" => "UMONCTON",      "labelEng"=>"UMONCTON" ,"url" => "https://www.umoncton.ca"],//9
        ["label" => "SAINTE ANNE",   "labelEng"=>"SAINTE ANNE", "url" => "https://www.usainteanne.ca"],//10
        ["label" => "UQAR",          "labelEng"=>"UQAR", "url" => "https://www.uqar.ca"],//11
        ["label" => "UQTR",          "labelEng"=>"UQTR", "url" => "https://www.uqtr.ca"],//12
        ["label" => "ULAVAL",        "labelEng"=>"ULAVAL", "url" => "https://www.ulaval.ca"],//13
        ["label" => "TECCART",       "labelEng"=>"TECCART", "url" => "https://www.teccart.qc.ca"],//14
        ["label" => "AVIRON",        "labelEng"=>"AVIRON", "url" => "https://www.avironquebec.com"],//15
        ["label" => "HERZING",       "labelEng"=>"HERZING", "url" => "https://www.herzing.ca/fr/accueil"],//16
        ["label" => "CDI",           "labelEng"=>"CDI", "url" => "https://www.collegecdi.ca"],//17
        ["label" => "LA SALLE",      "labelEng"=>"LA SALLE", "url" => "https://www.collegelasalle.com"],//18
        ["label" => "ROSEMENT",      "labelEng"=>"ROSEMENT", "url" => "https://www.crosemont.qc.ca"],//19
        ["label" => "MAISONNEUVE",   "labelEng"=>"MAISONNEUVE", "url" => "https://www.cmaisonneuve.qc.ca"],//20
        ["label" => "DAWSON",        "labelEng"=>"DAWSON", "url" => "https://www.dawsoncollege.qc.ca"],//21
        ["label" => "BOREAL",        "labelEng"=>"BOREAL", "url" => "https://www.collegeboreal.ca"],//22
        ["label" => "LA CITE",       "labelEng"=>"LA CITE", "url" => "https://www.collegelacite.ca"],//23
        ["label" => "VIEUX MONTREAL","labelEng"=>"VIEUX MONTREAL", "url" => "https://www.cvm.qc.ca"],//24
        ["label" => "Marie Victorin","labelEng"=>"Marie Victorin", "url" => "https://www.collegemv.qc.ca"],//25
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->names as $name) {
            AdmissionEtabName::create($name);
        }
    }
}
