<?php

use Illuminate\Database\Seeder;

use App\Models\LanguageTestScore;

class LanguageTestScoreSeeder extends Seeder
{
    private $scores = [
        // ['code'=> 'A', 'label'=> 'M, 0 - 3'],
        // ['code'=> 'B', 'label'=> '4'],
        // ['code'=> 'C', 'label'=> '5'],
        // ['code'=> 'D', 'label'=> '6'],
        // ['code'=> 'E', 'label'=> '7'],
        // ['code'=> 'F', 'label'=> '8'],
        // ['code'=> 'G', 'label'=> '9'],
        // ['code'=> 'H', 'label'=> '10 - 12'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->scores as $score) {
            LanguageTestScore::create(
                [
                    'score' => $score['label'],
                    'code' => $score['code'],
                ]
            );
        }
    }
}
