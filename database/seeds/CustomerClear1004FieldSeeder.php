<?php

use Illuminate\Database\Seeder;

use App\Models\CustomerClear1004Fields;

class CustomerClear1004FieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skip = array('id', 'customer_id');
        $fields = array_diff(Illuminate\Support\Facades\Schema::getColumnListing('customer_clear1004_fields'), $skip);
        //dd($fields);
        foreach(\App\Models\Customer::all() as $customer) {
            $customer_clear = \App\Models\CustomerClear1004Fields::where('customer_id', $customer->id)->first();
            if(!$customer_clear){
                $customer_clear = \App\Models\CustomerClear1004Fields::create([
                    "customer_id" => $customer->id
                ]);
            }
            foreach($fields as $key => $value){
                $customer_clear->$value = $customer->$value;
            }
            $customer_clear->save();
        }
    }
}
