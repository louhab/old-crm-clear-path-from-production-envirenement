<?php

use Illuminate\Database\Seeder;
use App\Models\OrientationSchool;
class OrientationSchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // OrientationSchool::truncate();

        $csvFile = fopen(base_path("database/seeds/csv/new_BD_Update.csv"), "r");

        $firstline = true;
        $columns = [
            'budget',
            'langue',
            'ville',
            'session',
            'date_limit_admission',
            'niveau_scolaire',
            'note',
            'test_langue',
            'score',
            'domaine_etude',
            'programme',
            'diplome',
            'type_etablissement',
            'nom_etablissement',
            'duree',
            'frais_admission',
            'prix_annuel',
            'prix_total',
            'lien',
            'code_programme',
            'exigence',
            'Analyse',
            'classement_choix',
        ];
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                $row = array();
                foreach($columns as $key => $column) {
                    $row[$column] = $data[$key];
                }
                OrientationSchool::create($row);
            }
            $firstline = false;
        }

        fclose($csvFile);
    }
}
