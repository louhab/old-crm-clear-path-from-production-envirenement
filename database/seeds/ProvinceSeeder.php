<?php

use Illuminate\Database\Seeder;

use App\Models\Province;

class ProvinceSeeder extends Seeder
{
    private $labels = [
        // city
        ['value_fr'=> 'Quebec'      ,"labelEng"=>"Quebec"],
        ['value_fr'=> 'Ontario'     ,"labelEng"=>"Ontario"],
        ['value_fr'=> 'British'     ,"labelEng"=>"British"],
        ['value_fr'=> 'Columbia'    ,"labelEng"=>"Columbia"],
        ['value_fr'=> 'Alberta'     ,"labelEng"=>"Alberta"],
        ['value_fr'=> 'Autre'       ,"labelEng"=>"Other"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->labels as $phase) {
            Province::where("label", "LIKE", $phase["value_fr"])
                ->update(['labelEng' => $phase["labelEng"]]);
        }*/
        foreach ($this->labels as $label) {
            Province::create(
                [
                    'label' => $label['value_fr'],
                    'labelEng'=>$label['labelEng']
                ]
            );
        }
    }
}
