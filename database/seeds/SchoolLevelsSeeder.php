<?php

use App\Models\SchoolLevel;
use Illuminate\Database\Seeder;

class SchoolLevelsSeeder extends Seeder
{
    private $schoolLevels = [
        ['level'=> 'Bacalauréat',"labelEng"=>"Baccalaureate"],
        ['level'=> 'Bac+2'      ,"labelEng"=>"Bac+2"],
        ['level'=> 'Bac+4'      ,"labelEng"=>"Bac+4"]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->schoolLevels as $phase) {
            SchoolLevel::where("school_level_name", "LIKE", $phase["level"])
                ->update(['labelEng' => $phase["labelEng"]]);
        }*/

        foreach ($this->schoolLevels as $schoolLevel) {
            SchoolLevel::create(
                [
                    'school_level_name' => $schoolLevel['level'],
                    'labelEng' => $schoolLevel['labelEng']
                ]
            );
        }
    }
}
