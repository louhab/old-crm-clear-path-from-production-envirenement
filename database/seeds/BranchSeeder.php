<?php

use Illuminate\Database\Seeder;

use App\Models\Branch;

class BranchSeeder extends Seeder
{
    private $branchs = [
        ['label_fr'=> 'Sciences', 'label_en'=> 'Science'],
        ['label_fr'=> 'Economie', 'label_en'=> 'Economy'],
        ['label_fr'=> 'Lettre', 'label_en'=> 'Letter'],
        ['label_fr'=> 'Gestion', 'label_en'=> 'Management'],
        ['label_fr'=> 'Finance', 'label_en'=> 'Finance'],
        ['label_fr'=> 'Marketing', 'label_en'=> 'Marketing'],
        ['label_fr'=> 'Comptabilité', 'label_en'=> 'Accounting'],
        ['label_fr'=> 'Mécanique', 'label_en'=> 'Mechanical'],
        ['label_fr'=> 'Electricité', 'label_en'=> 'Electricity'],
        ['label_fr'=> 'Génie', 'label_en'=> 'Genie'],
        ['label_fr'=> 'Autre', 'label_en'=> 'Other'],
        // Colonne J Excel
        ['label_fr' => "Informatique", 'label_en' => "Computer science"],
        ['label_fr' => "Santé", 'label_en' => "Health"],
        ['label_fr' => "Education", 'label_en' => "Education"],
        ['label_fr' => "Fiscalité", 'label_en' => "Taxation"],
        ['label_fr' => "Mode", 'label_en' => "Mode"],
        ['label_fr' => "Commerce", 'label_en' => "Trade"],
        ['label_fr' => "Droit", 'label_en' => "Law"],
        ['label_fr' => "Architecture", 'label_en' => "Architecture"],
        ['label_fr' => "Sociologie", 'label_en' => "Sociology"],
        ['label_fr' => "Mine", 'label_en' => "Mine"],
        ['label_fr' => "Environnement", 'label_en' => "Environnement"],
        ['label_fr' => "Police", 'label_en' => "Police"],
        ['label_fr' => "Agriculture", 'label_en' => "Agriculture"],
        ['label_fr' => "Construction", 'label_en' => "Construction"],
        ['label_fr' => "Electronique", 'label_en' => "Electronic"],
        ['label_fr' => "Graduate Certificate", 'label_en' => "Graduate Certificate"],
        ['label_fr' => "Technologies de l'information", 'label_en' => "Information technology"],
        ['label_fr' => "Commerce international", 'label_en' => "International Business"],
        ['label_fr' => "Design", 'label_en' => "Design"],
        ['label_fr' => "Logistique et chaîne d'approvisionnement", 'label_en' => "Logistics and Supply Chain"],
        ['label_fr' => "Administration des affaires - Gestion", 'label_en' => "Business Administration - Management"],
        ['label_fr' => "Communication", 'label_en' => "Communication"],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->branchs as $branch) {
            Branch::create(
                [
                    'branch_label_fr' => $branch['label_fr'],
                    'branch_label_en' => $branch['label_en'],
                ]
            );
        }
    }
}
