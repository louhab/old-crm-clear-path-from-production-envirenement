<?php

use Illuminate\Database\Seeder;

use App\Models\LevelOfStudy;

class LevelOfStudySeeder extends Seeder
{
    private $levels = [
        [
            'label_fr'=> 'Niveau Bac',
            'label_en'=> 'Bac Level',
            'docs' => [
                [
                    69 => ['program_id' => 4, 'order' => 2],
                ],
                [
                    70 => ['program_id' => 4, 'order' => 3],
                ],
                [
                    81 => ['program_id' => 4, 'order' => 1],
                ],
            ]
        ],
        [
            'label_fr'=> 'Niveau Bac +2',
            'label_en'=> 'Level Bac+2',
            'docs' => []
        ],
        /*[
            'label_fr'=> 'Baccalaureat en cours',
            'label_en'=> 'Baccalaureate in progress',
            'docs' => [
                [
                    69 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    70 => ['program_id' => 4, 'order' => 2],
                ],
                [
                    71 => ['program_id' => 4, 'order' => 3],
                ],
                [
                    72 => ['program_id' => 4, 'order' => 4],
                ],
                [
                    73 => ['program_id' => 4, 'order' => 5],
                ],
                [
                    81 => ['program_id' => 4, 'order' => 6],
                ],
            ]
        ],*/
        [
            'label_fr'=> 'Baccalaureat',
            'label_en'=> 'Baccalaureate',
            'docs' => [
                [
                    69 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    70 => ['program_id' => 4, 'order' => 2],
                ],
                [
                    71 => ['program_id' => 4, 'order' => 3],
                ],
                [
                    72 => ['program_id' => 4, 'order' => 4],
                ],
                [
                    73 => ['program_id' => 4, 'order' => 5],
                ],
            ]
        ],
        [
            'label_fr'=> 'Bac +1',
            'label_en'=> 'Bac +1',
            'docs' => []
        ],
        [
            'label_fr'=> 'Bac +2',
            'label_en'=> 'Bac +2',
            'docs' => [
                [
                    74 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    75 => ['program_id' => 4, 'order' => 2],
                ],
                [
                    76 => ['program_id' => 4, 'order' => 3],
                ],
                [
                    77 => ['program_id' => 4, 'order' => 4],
                ],
            ]
        ],
        [
            'label_fr'=> 'Licence',
            'label_en'=> 'Licence',
            'docs' => [
                [
                    74 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    75 => ['program_id' => 4, 'order' => 2],
                ],
                [
                    76 => ['program_id' => 4, 'order' => 3],
                ],
                [
                    77 => ['program_id' => 4, 'order' => 4],
                ],
                [
                    78 => ['program_id' => 4, 'order' => 5],
                ],
                [
                    79 => ['program_id' => 4, 'order' => 6],
                ],
            ]
        ],
        ['label_fr'=> 'Licence Professionnelle', 'label_en'=> 'Professional License'],
        ['label_fr'=> 'Maitrise 1', 'label_en'=> 'Mastery 1'],
        [
            'label_fr'=> 'Maitrise 2',
            'label_en'=> 'Mastery 2',
            'docs' => [
                [
                    74 => ['program_id' => 4, 'order' => 1],
                ],
                [
                    75 => ['program_id' => 4, 'order' => 2],
                ],
                [
                    76 => ['program_id' => 4, 'order' => 3],
                ],
                [
                    77 => ['program_id' => 4, 'order' => 4],
                ],
                [
                    80 => ['program_id' => 4, 'order' => 5],
                ],
                [
                    52 => ['program_id' => 4, 'order' => 6],
                ],
            ]
        ],
        [
            'label_fr'=> 'Doctorat',
            'label_en'=> 'Doctorate',
            'docs' => []
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->levels as $phase) {
            LevelOfStudy::where("level_label_fr", "LIKE", $phase["label_fr"])
                ->update(['level_label_en' => $phase["label_en"]]);
        }
        Baccalauréat français
        Baccalauréat français en cours
        Licence professionnelle
        Licence professionnelle en cours
        Niveau Bac en cours
        Baccalauréat en cours
        Licence en cours
        Maitrise en cours
        */

        foreach ($this->levels as $level) {
            if(!isset($level['docs'])){
                $level['docs'] = [];
            }
            $docs = $level['docs'];
            unset($level['docs']);

            /*$lev = LevelOfStudy::where('level_label_fr', $level['label_fr'])->first();
            if (!$lev) {
                $lev = LevelOfStudy::create(
                    [
                        'level_label_fr' => $level['label_fr'],
                        'level_label_en' => $level['label_en'],
                    ]
                );
            } else {
                continue;
            }*/
            LevelOfStudy::create(
                [
                    'level_label_fr' => $level['label_fr'],
                    'level_label_en' => $level['label_en'],
                ]
            );
            LevelOfStudy::create(
                [
                    'level_label_fr' => $level['label_fr'] . " en cours",
                    'level_label_en' => $level['label_en'] . " in progress",
                ]
            );

            // foreach ($docs as $doc) $lev->docs()->attach($doc);
        }
    }
}
