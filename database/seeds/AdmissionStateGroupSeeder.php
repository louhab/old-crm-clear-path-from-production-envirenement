<?php

use Illuminate\Database\Seeder;
use App\Models\AdmissionStateGroup;

class AdmissionStateGroupSeeder extends Seeder
{
    private $groups = [
        [
            'label_fr' => 'Admission',
            'label_en' => 'Admission',
            'order' => 1
        ],
        [
            'label_fr' => 'CAQ',
            'label_en' => 'CAQ',
            'order' => 2
        ],
        [
            'label_fr' => 'Visa&PE',
            'label_en' => 'Visa&PE',
            'order' => 3
        ],
        [
            'label_fr' => 'WES',
            'label_en' => 'WES',
            'order' => 4
        ],
        [
            'label_fr' => 'Profil',
            'label_en' => 'Profil',
            'order' => 5
        ],
        [
            'label_fr' => 'VISA',
            'label_en' => 'VISA',
            'order' => 6
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->groups as $group) {
            AdmissionStateGroup::create(
                $group
            );
        }
    }
}
