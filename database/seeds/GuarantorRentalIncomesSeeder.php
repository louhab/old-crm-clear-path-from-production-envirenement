<?php

use App\Models\GuarantorRentalIncome;
use Illuminate\Database\Seeder;

class GuarantorRentalIncomesSeeder extends Seeder
{
    private $incomes = [
        ['label_fr'=> "Un revenu locatif", 'label_en'=> "Un revenu locatif"],
        ['label_fr'=> "Deux revenus locatif", 'label_en'=> "Deux revenus locatif"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->incomes as $income) {
            GuarantorRentalIncome::create(
                [
                    'label_fr' => $income['label_fr'],
                    'label_en' => $income['label_en'],
                ]
            );
        }
    }
}
