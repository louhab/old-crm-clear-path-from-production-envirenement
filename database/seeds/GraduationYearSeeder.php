<?php

use Illuminate\Database\Seeder;

class GraduationYearSeeder extends Seeder
{

    private $labels = [
        ["label" => "2021/2022"],
        ["label" => "2020/2021"],
        ["label" => "2019/2020"],
        ["label" => "2018/2019"],
        ["label" => "2017/2018"],
        ["label" => "2016/2017"],
        ["label" => "2015/2016"],
        ["label" => "2014/2015"],
        ["label" => "2013/2014"],
        ["label" => "2012/2013"],
        ["label" => "2011/2012"],
        ["label" => "2010/2011"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->labels as $label) {
            \App\Models\GraduationYear::create($label);
        }
    }
}
