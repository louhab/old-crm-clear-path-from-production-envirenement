<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => 1,//admin
            'name' => 'admin',
            'email' => 'admin@immg.ca',
            'phone' => '0600000000',
            'password' => Hash::make('123456'),
            'created_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
            'role_id' => 2,//manager
            'name' => 'manager',
            'email' => 'manager@immg.ca',
            'phone' => '0600000001',
            'password' => Hash::make('123456'),
            'created_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
            'role_id' => 3,//conseiller
            'name' => 'conseiller',
            'email' => 'conseiller@immg.ca',
            'phone' => '0600000002',
            'password' => Hash::make('123456'),
            'created_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
            'role_id' => 4,//agent
            'name' => 'agent',
            'email' => 'agent@immg.ca',
            'phone' => '0600000003',
            'password' => Hash::make('123456'),
            'created_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
            'role_id' => 6,//agent
            'name' => 'backoffice',
            'email' => 'backoffice@immg.ca',
            'phone' => '0600000004',
            'password' => Hash::make('123456'),
            'created_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
            'role_id' => 7,//support
            'name' => 'support',
            'email' => 'support@immg.ca',
            'phone' => '0600000007',
            'password' => Hash::make('123456'),
            'created_at' => Carbon::now()
        ]);
    }
}
