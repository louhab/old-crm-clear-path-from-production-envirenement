<?php

use Illuminate\Database\Seeder;

use App\Models\ImmigrantActivity;

class ImmigrantActivitySeeder extends Seeder
{
    private $activities = [
        ['label_fr'=> 'Emploi'     ,'labelEng'=>'Employee' ],
        ['label_fr'=> 'Retraité'   ,'labelEng'=>'Retirement' ],
        ['label_fr'=> 'Touriste'   ,'labelEng'=>'Tourist' ],
        ['label_fr'=> 'Chomage'    ,'labelEng'=>'Unemployed' ],
        ['label_fr'=> 'Étude'      ,'labelEng'=>'Study' ],
        ['label_fr'=> 'Stage'      ,'labelEng'=>'Stage' ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->activities as $phase) {
            ImmigrantActivity::where("label_fr", "LIKE", $phase["label_fr"])
                ->update(['labelEng' => $phase["labelEng"]]);
        }*/

        foreach ($this->activities as $activity) {
            ImmigrantActivity::create(
                $activity
            );
        }
    }
}
