<?php

use Illuminate\Database\Seeder;

class CustomerFieldGroupsSeeder extends Seeder
{
    private $customerFieldGroups = [
        ['name_fr' => 'Informations Personnelles', 'name_en' => 'Personal Informations', 'order' => '1'], //1
        ['name_fr' => 'Emploi', 'name_en' => 'Current job', 'order' => '3'], //2
        ['name_fr' => 'Scolarité', 'name_en' => 'School', 'order' => '2'], //3
        ['name_fr' => 'Informations des membre de la famille', 'name_en' => 'Family member information', 'order' => '4'], //4
        ['name_fr' => 'Pays de résidence et voyages', 'name_en' => 'Country of residence and travel', 'order' => '5'], //5

        ['name_fr' => 'Dossier Scolaire', 'name_en' => 'Academic Record', 'order' => '6'], //6
        ['name_fr' => 'Evaluation du dossier médical de l\'étudiant ', 'name_en' => 'Assessment of the student\'s medical file', 'order' => '11'], //7
        ['name_fr' => 'Evaluation du dossier financier du Garant', 'name_en' => 'Assessment of the Guarantor\'s financial file', 'order' => '9'], //8
        ['name_fr' => 'Evaluation du dossier criminel de l\'étudiant', 'name_en' => 'Assessment of the student\'s criminal record', 'order' => '11'], //9
        ['name_fr' => 'Solutions financières et programme Volet direct', 'name_en' => 'Financial solutions and direct component program', 'order' => '10'], //10
        ['name_fr' => 'Etude Envisagée au Canada', 'name_en' => 'Study Considered in Canada', 'order' => '6'], //11
        ['name_fr' => 'Documents de l\'étudiant', 'name_en' => 'Student documents', 'order' => '1'], //12
        ['name_fr' => 'Informations sur les anciennes demandes des visas & permis d´études', 'name_en' => 'Informations sur les anciennes demandes des visas & permis d’études', 'order' => '13'], //13

        ['name_fr' => 'Questions légales', 'name_en' => 'Questions légales', 'order' => '13'], //13
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->customerFieldGroups as $phase) {
            \App\Models\CustomerFieldGroup::where("name_fr", "LIKE", $phase["name_fr"])
                ->update(['name_en' => $phase["name_en"]]);
        }*/

        foreach ($this->customerFieldGroups as $customerFieldGroup) {
            \App\Models\CustomerFieldGroup::create(
                $customerFieldGroup
            );
        }
    }
}
