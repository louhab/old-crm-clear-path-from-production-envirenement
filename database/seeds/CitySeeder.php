<?php

use App\Models\Cities;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents(__DIR__ . '/muni.json');
        $json_data = json_decode($json, true);

        foreach ($json_data as $key => $value) {
            Cities::create([
                "city" => $value["Muni"],
            ]);
        }
    }
}
