<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\CanadaCity;

class CanadaCitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = file_get_contents(__DIR__ . '/canada_cities.json');

        $cities_out = json_decode($cities, true);

        // dd($cities_out);

        foreach ($cities_out as $key => $value) {
            CanadaCity::create([
                "id" => $value["id"] ?? null,
                "label" => $value["label"],
                "labelEng" => $value["labelEng"],
                "program_ids" => $value["program_ids"],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
