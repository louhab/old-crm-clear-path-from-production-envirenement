<?php

use Illuminate\Database\Seeder;

class ServiceDetailsSeeder extends Seeder
{
    private $serviceDetails = [
        //Service Details
        // Etud - Etape1
        [
            'serviceId'=> 16,
            'name' => 'Collecte des informations et remplir le questionnaire étudiant',
            'name_en' => 'Information collection and student questionnaire completion'
        ],
        [
            'serviceId'=> 16,
            'name' => 'Analyse des informations et recommandations Clear 1001',
            'name_en' => 'Clear 1001 information analysis and recommendations'
        ],
        [
            'serviceId'=> 16,
            'name' => 'Explication des documents : Devis, contrat',
            'name_en' => 'Explanation of documents: Quote, contract'
        ],
        [
            'serviceId'=> 16,
            'name' => 'Accès au portail et espace client',
            'name_en' => 'Access to the portal and customer area'
        ],
        [
            'serviceId'=> 16,
            'name' => 'Devenir membre du réseau Clear Path Canada pour recevoir toutes les nouvelles',
            'name_en' => 'Become a member of the Clear Path Canada network to receive all the news'
        ],
        // Etud - Etape2
        [
            'serviceId'=> 17,
            'name' => 'Ouverture du dossier',
            'name_en' => 'File opening'
        ],
        [
            'serviceId'=> 17,
            'name' => 'Explication de la procédure : Clear 1, Clear 2 et Clear 3',
            'name_en' => 'Explanation of the procedure: Clear 1, Clear 2 and Clear 3'
        ],
        [
            'serviceId'=> 17,
            'name' => ' Accompagnement et encadrement',
            'name_en' => 'Accompaniment and supervision'
        ],
        [
            'serviceId'=> 17,
            'name' => 'Développement du projet d\'étude',
            'name_en' => 'Development of the study project'
        ],
        [
            'serviceId'=> 17,
            'name' => 'Préparation de la demande d\'admission',
            'name_en' => 'Preparation of the application for admission'
        ],
        [
            'serviceId'=> 17,
            'name' => 'Orientation et propositions des programmes Clear 1002',
            'name_en' => 'Orientation and proposals of Clear 1002 programs'
        ],
        // second service..
        [
            'serviceId'=> 18,
            'name' => 'Vérification des documents Clear 1, Clear 2 et Clear 3',
            'name_en' => 'Verification of Clear 1, Clear 2 and Clear 3 documents'
        ],
        [
            'serviceId'=> 18,
            'name' => 'Analyse Financière des garants',
            'name_en' => 'Financial analysis of guarantors'
        ],
        [
            'serviceId'=> 18,
            'name' => 'Recommandation et solutions financières',
            'name_en' => 'Recommendation and financial solutions'
        ],
        [
            'serviceId'=> 18,
            'name' => 'Confirmation et choix des programmes',
            'name_en' => 'Confirmation and program choice'
        ],
        [
            'serviceId'=> 18,
            'name' => 'Soumission de la demande d\'admission',
            'name_en' => 'Admission application presentation'
        ],
        // Etud-Etape3
        [
            'serviceId'=> 20,
            'name' => 'Explication de la procédure : Clear 4, Clear 5 et Clear 6',
            'name_en' => 'Explanation of the procedure: Clear 4, Clear 5 and Clear 6'
        ],
        [
            'serviceId'=> 20,
            'name' => 'Accompagnement et encadrement',
            'name_en' => 'Accompaniment and supervision'
        ],
        [
            'serviceId'=> 20,
            'name' => 'Préparation de la demande du visa et permis d\'études',
            'name_en' => 'Visa application and study permit preparation'
        ],
        // second service
        [
            'serviceId'=> 21,
            'name' => 'Vérification des documents : Clear 4, Clear 5 et Clear 6',
            'name_en' => 'Document verification: Clear 4, Clear 5 and Clear 6'
        ],
        [
            'serviceId'=> 21,
            'name' => 'Signature des formulaires de visa et permis d\'études',
            'name_en' => 'Signature of visa and study permit forms'
        ],
        [
            'serviceId'=> 21,
            'name' => 'Soumission de la demande de visa et permis d\'études',
            'name_en' => 'Visa application and study permit presentation'
        ],
        // entree express - etape1
        // Data storage and completion of the student questionnaire
        [
            'serviceId'=> 1,
            'name' => 'Collecte des informations et remplir le questionnaire étudiant',
            'name_en' => 'Data storage and completion of the student questionnaire'
        ],
        [
            'serviceId'=> 1,
            'name' => 'Analyse des informations et recommandations Clear 1004',
            'name_en' => 'Analyze information and recommendations Borrar 1004'
        ],
        [
            'serviceId'=> 1,
            'name' => 'Explication des documents : Clear 10, CNP, Devis, contrat',
            'name_en' => 'Explanation of documents: Clear 10, CNP, Quote, contract'
        ],
        [
            'serviceId'=> 1,
            'name' => 'Accès au portail et espace client',
            'name_en' => 'Access to the portal and customer area'
        ],
        [
            'serviceId'=> 1,
            'name' => 'Devenir membre du réseau Clear Path Canada pour recevoir toutes les nouvelles',
            'name_en' => 'Become a member of the Clear Path Canada network to receive all the news'
        ],
        // Imm-Etape2
        [
            'serviceId'=> 2,
            'name' => 'Ouverture du dossier',
            'name_en' => 'File opening'
        ],
        [
            'serviceId'=> 2,
            'name' => 'Explication de la procédure : Clear 11, Clear 12',
            'name_en' => 'Explanation of the procedure: Clear 11, Clear 12'
        ],
        [
            'serviceId'=> 2,
            'name' => 'Développement du projet d\'immigration, accompagnement et encadrement',
            'name_en' => 'Development of the immigration project, support and supervision'
        ],
        [
            'serviceId'=> 2,
            'name' => 'Préparation de la demande d\'equivalence des diplômes',
            'name_en' => 'Preparation of the equivalence of diplomas request'
        ],
        [
            'serviceId'=> 2,
            'name' => 'Préparation des tests de langues : TCF Canada, TEF Canada et IELTS',
            'name_en' => 'Preparation for language tests: TCF Canada, TEF Canada and IELTS'
        ],
        [
            'serviceId'=> 2,
            'name' => 'Le choix du CNP',
            'name_en' => 'Choice of the CNP'
        ],
        [
            'serviceId'=> 2,
            'name' => 'Soumission de la demande d\'equivalence des diplômes',
            'name_en' => 'Equivalence of diplomas request presentation'
        ],
        // second service
        [
            'serviceId'=> 7,
            'name' => 'Valider les résultats de la demande d\'équivalence',
            'name_en' => 'Validation of the equivalence request results'
        ],
        [
            'serviceId'=> 7,
            'name' => 'Valider les résultats des test de francais',
            'name_en' => 'Validation of the language test results'
        ],
        [
            'serviceId'=> 7,
            'name' => 'Valider le choix du CNP',
            'name_en' => 'CNP choice validation'
        ],
        [
            'serviceId'=> 7,
            'name' => 'Création du profil entrée express',
            'name_en' => 'Creation of the express entry profile'
        ],
        [
            'serviceId'=> 7,
            'name' => 'Soumission de la demande du profil Entrée Express',
            'name_en' => 'Express Entry profile application and presentation'
        ],
        // Imm-Etape3
        [
            'serviceId'=> 8,
            'name' => 'Explication de la procédure : Clear 13',
            'name_en' => 'Explanation of the procedure: Clear 13'
        ],
        [
            'serviceId'=> 8,
            'name' => 'Accompagnement et encadrement',
            'name_en' => 'Accompaniment and supervision'
        ],
        [
            'serviceId'=> 8,
            'name' => 'Préparation de la demande du visa et résidence permanente',
            'name_en' => 'Visa application and permanent residence preparation'
        ],
        // second service
        [
            'serviceId'=> 9,
            'name' => 'Vérification des documents : Clear 13',
            'name_en' => 'Document Verification: Clear 13'
        ],
        [
            'serviceId'=> 9,
            'name' => 'Signature des formulaires de visa et résidence permanente',
            'name_en' => 'Visa and permanent residence forms signature'
        ],
        [
            'serviceId'=> 9,
            'name' => 'Soumission de la demande de visa et résidence permanente',
            'name_en' => 'Visa and permanent residence application and presentation'
        ],

    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*foreach ($this->serviceDetails as $phase) {
            \App\Models\ServiceDetail::where("name", "LIKE", $phase["name"])
                ->update(['labelEng' => $phase["labelEng"]]);
        }*/

        foreach ($this->serviceDetails as $serviceDetail) {
            \App\Models\ServiceDetail::create(
                [
                    'service_id' => $serviceDetail['serviceId'],
                    'name' => $serviceDetail['name'],
                    'name_en'=>$serviceDetail["name_en"]
                ]
            );
        }
    }
}
