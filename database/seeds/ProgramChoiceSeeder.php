<?php

use Illuminate\Database\Seeder;

use App\Models\ProgramChoice;
class ProgramChoiceSeeder extends Seeder
{
    private $choices = [
        ['name' => 'Soutien informatique'           ,"labelEng"=>"Computer support"],
        ['name' => 'Infirmerie'                     ,"labelEng"=>"Infirmary"],
        ['name' => 'Informatique de gestion'        ,"labelEng"=>"Management IT"],
        ['name' => 'Electronique'                   ,"labelEng"=>"Electronic"],
        ['name' => 'Administration'                 ,"labelEng"=>"Administration"],
        ['name' => 'Marketing'                      ,"labelEng"=>"Marketing"],
        ['name' => 'Finance'                        ,"labelEng"=>"Finance"],
        ['name' => 'Comptabilité'                   ,"labelEng"=>"Accounting"],
        ['name' => 'Génie'                          ,"labelEng"=>"Genie"],
        ['name' => 'Autre'                          ,"labelEng"=>"Other"],
        //
        // Colonne J Excel
        ['name'=> 'Sciences', 'labelEng'=> 'Science'],
        ['name'=> 'Economie', 'labelEng'=> 'Economy'],
        ['name'=> 'Lettre', 'labelEng'=> 'Letter'],
        ['name'=> 'Gestion', 'labelEng'=> 'Management'],
        ['name'=> 'Mécanique', 'labelEng'=> 'Mechanic'],
        ['name'=> 'Electricité', 'labelEng'=> 'Electricity'],
        ['name' => "Informatique", 'labelEng' => "Computer science"],
        ['name' => "Santé", 'labelEng' => "Health"],
        ['name' => "Education", 'labelEng' => "Education"],
        ['name' => "Fiscalité", 'labelEng' => "Taxation"],
        ['name' => "Mode", 'labelEng' => "Mode"],
        ['name' => "Commerce", 'labelEng' => "Commerce"],
        ['name' => "Droit", 'labelEng' => "Droit"],
        ['name' => "Architecture", 'labelEng' => "Architecture"],
        ['name' => "Sociologie", 'labelEng' => "Sociologie"],
        ['name' => "Mine", 'labelEng' => "Mine"],
        ['name' => "Environnement", 'labelEng' => "Environnement"],
        ['name' => "Police", 'labelEng' => "Police"],
        ['name' => "Agriculture", 'labelEng' => "Agriculture"],
        ['name' => "Construction", 'labelEng' => "Construction"],
        ['name' => "Graduate Certificate", 'labelEng' => "Graduate Certificate"],
        ['name' => "Technologies de l'information", 'labelEng' => "Technologies de l'information"],
        ['name' => "International Business", 'labelEng' => "International Business"],
        ['name' => "Design", 'labelEng' => "Design"],
        ['name' => "Logistics and Supply Chain", 'labelEng' => "Logistics and Supply Chain"],
        ['name' => "Business Administration - Management", 'labelEng' => "Business Administration - Management"],
        ['name' => "Communication", 'labelEng' => "Communication"],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->choices as $phase) {
            ProgramChoice::where("choice_label", "LIKE", $phase["name"])
                ->update(['labelEng' => $phase["labelEng"]]);
        }*/

        foreach ($this->choices as $choice) {
            ProgramChoice::create(
                [
                    'choice_label' => $choice['name'],
                    'labelEng' => $choice['labelEng']
                ]
            );
        }
    }
}
