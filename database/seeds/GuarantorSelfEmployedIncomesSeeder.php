<?php

use App\Models\GuarantorSelfEmployedIncome;
use Illuminate\Database\Seeder;

class GuarantorSelfEmployedIncomesSeeder extends Seeder
{
    private $incomes = [
        ['label_fr'=> "Un revenu de travailleur autonome", 'label_en'=> "Un revenu de travailleur autonome"],
        ['label_fr'=> "Deux revenus travailleur autonome", 'label_en'=> "Deux revenus travailleur autonome"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->incomes as $income) {
            GuarantorSelfEmployedIncome::create(
                [
                    'label_fr' => $income['label_fr'],
                    'label_en' => $income['label_en'],
                ]
            );
        }
    }
}
