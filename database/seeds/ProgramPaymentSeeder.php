<?php

use Illuminate\Database\Seeder;

use App\Models\ProgramPayment;

class ProgramPaymentSeeder extends Seeder
{
    private $payments = [
        //['Paiement 0', ["1" => [200, 4], "1" => [200, 1]]],
        [
            'Paiement 1', "2", [
                [
                    'program_id' => 4,
                    'amount' => 200,
                ],
                [
                    'program_id' => 4,
                    'amount' => 50,
                    'currency_id' => 2,
                ],
                [
                    'program_id' => 4,
                    'amount' => 50,
                    'currency_id' => 3,
                ],
                [
                    'program_id' => 1,
                    'amount' => 200,
                    'couple_amount' => 200,
                ],
                [
                    'program_id' => 1,
                    'currency_id' => 2,
                    'amount' => 50,
                    'couple_amount' => 50,
                ],
                [
                    'program_id' => 1,
                    'currency_id' => 3,
                    'amount' => 50,
                    'couple_amount' => 50,
                ]
            ]
        ],
        [
            'Paiement 2', "3", [
                [
                    'program_id' => 4,
                    'amount' => 2800,
                ],
                [
                    'program_id' => 4,
                    'amount' => 280,
                    'currency_id' => 2,
                ],
                [
                    'program_id' => 4,
                    'amount' => 280,
                    'currency_id' => 3,
                ],
                [
                    'program_id' => 1,
                    'amount' => 700,
                    'couple_amount' => 900,
                ],
                [
                    'program_id' => 1,
                    'currency_id' => 2,
                    'amount' => 130,
                    'couple_amount' => 130,
                ],
                [
                    'program_id' => 1,
                    'currency_id' => 3,
                    'amount' => 100,
                    'couple_amount' => 100,
                ]
            ]
        ],
        [
            'Paiement 3', "4", [
                [
                    'program_id' => 4,
                    'amount' => 5000,
                ],
                [
                    'program_id' => 4,
                    'amount' => 500,
                    'currency_id' => 2,
                ],
                [
                    'program_id' => 4,
                    'amount' => 500,
                    'currency_id' => 3,
                ],
                [
                    'program_id' => 1,
                    'amount' => 5000,
                    'couple_amount' => 7500,
                ],
                [
                    'program_id' => 1,
                    'currency_id' => 2,
                    'amount' => 850,
                    'couple_amount' => 850,
                ],
                [
                    'program_id' => 1,
                    'currency_id' => 3,
                    'amount' => 600,
                    'couple_amount' => 850,
                ]
            ]
        ],
        [
            'Paiement 4', "5", [
                [
                    'program_id' => 4,
                    'amount' => 6000,
                ],
                [
                    'program_id' => 4,
                    'amount' => 600,
                    'currency_id' => 2,
                ],
                [
                    'program_id' => 4,
                    'amount' => 600,
                    'currency_id' => 3,
                ],
                [
                    'program_id' => 1,
                    'amount' => 5000,
                    'couple_amount' => 6000,
                ],
                [
                    'program_id' => 1,
                    'currency_id' => 2,
                    'amount' => 700,
                    'couple_amount' => 700,
                ],
                [
                    'program_id' => 1,
                    'currency_id' => 3,
                    'amount' => 600,
                    'couple_amount' => 700,
                ]
            ]
        ],
        [
            'Paiement 5', "6", [
                [
                    'program_id' => 4,
                    'amount' => 6000,
                ],
                [
                    'program_id' => 4,
                    'amount' => 600,
                    'currency_id' => 2,
                ],
                [
                    'program_id' => 4,
                    'amount' => 600,
                    'currency_id' => 3,
                ],
                [
                    'program_id' => 1,
                    'amount' => 5000,
                    'couple_amount' => 6000,
                ],
                [
                    'program_id' => 1,
                    'currency_id' => 2,
                    'amount' => 700,
                    'couple_amount' => 700,
                ],
                [
                    'program_id' => 1,
                    'currency_id' => 3,
                    'amount' => 600,
                    'couple_amount' => 700,
                ]
            ]
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->payments as $payment) {
            $pm = ProgramPayment::create(
                [
                    'name_fr' => $payment[0],
                ]);
            $stepsToAttach = [];
            // $pm = ProgramPayment::where('name_fr', $payment[0])->first();
            foreach ($payment[2] as $values) {
                /*$stepsToAttach[intval($payment[1])] = [
                    'amount' => $values[0],
                    'program_id' => $values[1]
                ];*/
                if (array_key_exists("currency_id", $values)) {
                    $pm->steps()->attach([intval($payment[1]) =>
                        $values
                    ]);
                } else {
                    $values["currency_id"] = 1;
                    $pm->steps()->attach([intval($payment[1]) =>
                        $values
                    ]);
                }
                /*if ($values[0] == 1) {
                } else {
                    $pm->steps()->attach([intval($payment[1]) =>
                        [
                            'amount' => $values[1],
                            'program_id' => $values[0]
                        ]]);
                }*/
                /*$pm->steps()->attach([intval($payment[1]) =>
                    [
                        'amount' => $values[0],
                        'program_id' => $values[1]
                    ]]);*/
            }

            //$pm->steps()->attach($stepsToAttach);
        }
    }
}
