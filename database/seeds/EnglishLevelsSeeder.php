<?php

use App\Models\EnglishLevel;
use Illuminate\Database\Seeder;

class EnglishLevelsSeeder extends Seeder
{
    private $englishLevels = [
        ['level'=> 'débutant'   ,"level_an"=>"Beginner"],
        ['level'=> 'moyen'      ,"level_an"=>"Medium"],
        ['level'=> 'bon'        ,"level_an"=>"Good"],
        ['level'=> 'excellent'  ,"level_an"=>"Excellent"],
        ['level'=> 'maternelle' ,"level_an"=>"Mother tongue"]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->englishLevels as $phase) {
            EnglishLevel::where("english_level_name", "LIKE", $phase["level"])
                ->update(['labelEng' => $phase["level_an"]]);
        }*/

        foreach ($this->englishLevels as $englishLevel) {
            EnglishLevel::create(
                [
                    'english_level_name' => $englishLevel['level'],
                    'labelEng' => $englishLevel['level_an']
                ]
            );
        }
    }
}
