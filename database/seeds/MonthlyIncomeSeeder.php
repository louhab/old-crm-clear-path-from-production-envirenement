<?php

use Illuminate\Database\Seeder;

use App\Models\MonthlyIncome;

class MonthlyIncomeSeeder extends Seeder
{
    // Moins de 10000 DH, 15000 DH, 20000 DH, plus de 20000 DH
    private $incomes = [
        ['label'=> '<= 10000 DH'],
        ['label'=> '> 10000 DH, <= 15000 DH'],
        ['label'=> '> 15000 DH, <= 20000 DH'],
        ['label'=> '> 20000 DH'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->incomes as $income) {
            MonthlyIncome::create(
                [
                    'income' => $income['label'],
                ]
            );
        }
    }
}
