<?php

use Illuminate\Database\Seeder;

use App\Models\FrenchTest;

class FrenchTestSeeder extends Seeder
{
    // TFI, TEF, TCF
    private $tests = [
        /*['label' => 'DALF'],
        ['label' => 'DELF'],
        ['label' => 'TEF'],
        ['label' => 'TCF'],
        ['label' => 'No'],*/
        ['en' => 'DALF', 'fr' => 'DALF'],
        ['en' => 'TEF', 'fr' => 'TEF'],
        ['en' => 'TCF', 'fr' => 'TCF'],
        ['en' => 'No', 'fr' => 'Non'],
        ['en' => 'DELF', 'fr' => 'DELF'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*foreach ($this->tests as $test) {
            FrenchTest::create(
                [
                    'test_label' => $test['label'],
                ]
            );
        }*/
        $i = 0;
        foreach (FrenchTest::orderBy("id")->get() as $test) {
            $test->test_label = $this->tests[$i];
            $test->save();
            $i++;
        }
    }
}
