<?php

use App\Models\Poles;
use Illuminate\Database\Seeder;

class PolesSeeder extends Seeder
{

    private $fields = [
        [
            'name' => 'Casablanca',
            'address' => "42, rue Bachir Laalej, Casablanca - Maroc",
            'location' => "https://goo.gl/maps/nB5hMXDQKrorCoVX8"
        ],
        [
            'name' => 'El Jadida',
            'address' => "7 avenue ibn badis, premier étage, Centre ibn badis A, El Jadida - Maroc",
            'location' => "https://goo.gl/maps/n3q7do3rdrN3pQhg8"
        ],
        [
            'name' => 'Marrakech',
            'address' => "M Avenue business center Bureau 317, 3ème étage M Avenue project, 40000 Marrakech - Maroc",
            'location' => "https://goo.gl/maps/WvnBBN57T4jx75Rf8"
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->fields as $field) {
            Poles::create($field);
        }
    }
}
