<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    private $roles = [
        ['name' => 'Admin', "labelEng" => "Admin"],
        ['name' => 'Manager', "labelEng" => "Manager"],
        ['name' => 'Conseiller', "labelEng" => "Adviser"],
        ['name' => 'Agent', "labelEng" => "Agent"],
        ['name' => 'Client', "labelEng" => "Client"],
        ['name' => 'Backoffice', "labelEng" => "Backoffice"],
        ['name' => 'Support', "labelEng" => "Support"],
        ['name' => 'Super Backoffice', "labelEng" => "Super Backoffice"],
        ['name' => 'Financier', "labelEng" => "Financier"]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->roles as $role) {
            Role::create(
                [
                    'role_name' => $role['name'],
                    "labelEng" => $role["labelEng"]
                ]
            );
        }
    }
}
