<?php

use Illuminate\Database\Seeder;

class IsLanguageTestTakenSeeder extends Seeder
{
    private $labels = [
        ["label_fr" => "Oui", "label_en" => "Yes"],
        ["label_fr" => "Non", "label_en" => "No"],
        ["label_fr" => "A Faire", "label_en" => "A Faire"],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // IsLanguageTestTaken
        foreach ($this->labels as $label) {
            \App\Models\IsLanguageTestTaken::create($label);
        }
    }
}
