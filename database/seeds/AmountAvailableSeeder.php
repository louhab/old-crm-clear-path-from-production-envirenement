<?php

use Illuminate\Database\Seeder;

use App\Models\AmountAvailable;

class AmountAvailableSeeder extends Seeder
{
    // 25000  DH, 300000 DH, 35000 DH, plus de 400000 DH
    private $amounts = [
        ['label'=> '>= 200000 DH'],
        ['label'=> '250000 DH'],
        ['label'=> '300000 DH'],
        ['label'=> '350000 DH'],
        ['label'=> '>= 400000 DH'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->amounts as $amount) {
            AmountAvailable::create(
                [
                    'amount' => $amount['label'],
                ]
            );
        }
    }
}
