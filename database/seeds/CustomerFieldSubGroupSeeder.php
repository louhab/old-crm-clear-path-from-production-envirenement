<?php

use Illuminate\Database\Seeder;

class CustomerFieldSubGroupSeeder extends Seeder
{
    private $customerFieldGroups = [
        ['name_fr' => 'Époux ou conjoint de faits', 'name_en' => 'Époux ou conjoint de faits', 'order' => '1'], //1
        ['name_fr' => 'Mère', 'name_en' => 'Mother', 'order' => '2'], //2
        ['name_fr' => 'Père', 'name_en' => 'Father', 'order' => '3'], //3
        ['name_fr' => 'Enfants', 'name_en' => 'Children', 'order' => '4'], //4
        ['name_fr' => 'Frère / Sœur', 'name_en' => 'Sibling', 'order' => '5'], //5
        //
        ['name_fr' => 'Pays de résidence actuel', 'name_en' => 'Pays de résidence actuel', 'order' => '7'], //6
        ['name_fr' => 'Pays de résidence antérieure', 'name_en' => 'Pays de résidence antérieure', 'order' => '8'], //7
        ['name_fr' => 'Voyage divers', 'name_en' => 'Various travel', 'order' => '9'], //8
        ['name_fr' => 'Avez-vous déjà visité le Canada?', 'name_en' => 'Avez-vous déjà visité le Canada?', 'order' => '6'], //9
        ['name_fr' => 'La famille au Canada', 'name_en' => 'La famille au Canada', 'order' => '6'], //10
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->customerFieldGroups as $phase) {
            \App\Models\CustomerFieldGroup::where("name_fr", "LIKE", $phase["name_fr"])
                ->update(['name_en' => $phase["name_en"]]);
        }*/

        foreach ($this->customerFieldGroups as $customerFieldGroup) {
            \App\Models\CustomerFieldSubGroup::create(
                $customerFieldGroup
            );
        }
    }
}
