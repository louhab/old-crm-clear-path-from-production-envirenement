<?php

use Illuminate\Database\Seeder;

use App\Models\LeadStatus;

class LeadStatusSeeder extends Seeder
{
    private $statuss = [
        ['label_fr'=> 'Rencontre 1', 'label_en'=> 'Meeting 1', 'visible' => 1],//1
        ['label_fr'=> 'Pas Intéressé', 'label_en'=> 'Not Interested', 'visible' => 1],//2
        ['label_fr'=> 'Injoignable', 'label_en'=> 'Unreachable', 'visible' => 1],//3
        ['label_fr'=> 'RDV 1', 'label_en'=> 'RDV 1', 'visible' => 0],//4
        ['label_fr'=> 'Contrat', 'label_en'=> 'Contrat', 'visible' => 0],//5
        ['label_fr'=> 'Numero erroné', 'label_en'=> 'Wrong number', 'visible' => 1],//6
        ['label_fr'=> 'Admissible', 'label_en'=> 'Admissible', 'visible' => 0],//7
        ['label_fr'=> 'Inadmissible', 'label_en'=> 'Ineligible', 'visible' => 0],//8
        ['label_fr'=> 'No Show', 'label_en'=> 'No Show', 'visible' => 0],//9
        ['label_fr'=> 'Client', 'label_en'=> 'Client', 'visible' => 0],//10
        ['label_fr'=> 'No Contrat', 'label_en'=> 'No Contrat', 'visible' => 0],//11
        ['label_fr'=> 'No RDV 1', 'label_en'=> 'No RDV 1', 'visible' => 0], // 12
        ['label_fr'=> 'Repport', 'label_en'=> 'Repport', 'visible' => 0], // 13
        ['label_fr'=> 'Désistement', 'label_en'=> 'Disclaimer', 'visible' => 0], // 14
        // ['label_fr'=> 'Walking', 'label_en'=> 'Walking', 'visible' => 1],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->statuss as $phase) {
            LeadStatus::where("status_label_fr", "LIKE", $phase["label_fr"])
                ->update(['status_label_en' => $phase["label_en"]]);
        }*/

        foreach ($this->statuss as $status) {
            LeadStatus::create(
                [
                    'status_label_fr' => $status['label_fr'],
                    'status_label_en' => $status['label_en'],
                    'visible' => $status['visible'],
                ]
            );
        }
    }
}
