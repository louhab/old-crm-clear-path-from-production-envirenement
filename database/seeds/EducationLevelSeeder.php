<?php

use Illuminate\Database\Seeder;

use App\Models\EducationLevel;

class EducationLevelSeeder extends Seeder
{
    private $levels = [
        ['code' => 'A', 'label_fr' => 'Aucun, études secondaires non complétées (école secondaire)'                                                                                                                     , "label_an"=>"None, secondary studies not completed"],
        ['code' => 'B', 'label_fr' => 'Diplôme d\'études secondaires'                                                                                                                                                   , "label_an"=>"High school diploma"],
        ['code' => 'C', 'label_fr' => 'Programme d\'un an d\'une université, d\'un collège, d\'une école technique ou d\'une école de métier, ou d\'un autre établissement'                                             , "label_an"=>"One year program of a university/college"],
        ['code' => 'D', 'label_fr' => 'Programme de deux ans d\'une université, d\'un collège, d\'une école technique ou d\'une école de métier, ou d\'un autre établissement'                                          , "label_an"=>"Two-year program of a university/college"],
        ['code' => 'E', 'label_fr' => "Baccalauréat (programme d'une université, d'un collège, d'une école technique ou d'une école de métier, ou d'un autre établissement)"                                            , "label_an"=>"Baccalaureate (university or college program)"],
        ['code' => 'F', 'label_fr' => "Au moins deux certificats, grades ou diplômes. Au moins l'un de ces diplômes a été obtenu à la suite de la réussite d'un programme d'études postsecondaires d'au moins trois ans", "label_an"=>"At least two certificates, degrees or diplomas"],
        ['code' => 'G', 'label_fr' => 'Maîtrise ou diplôme professionnel nécessaire pour pratiquer une profession réglementée (voir Aide)'                                                                              , "label_an"=>"Masters or professional diploma required for"],
        ['code' => 'H', 'label_fr' => 'Diplôme universitaire au niveau du doctorat (Ph. D.)'                                                                                                                            , "label_an"=>"University degree at doctoral level (Ph. D"],
    ];

    //   <option value="A" "="">Aucun, études secondaires non complétées (école secondaire)</option>
    //   <option value="B" "="">Diplôme d'études secondaires</option>
    //   <option value="C" "="">Programme d'un an d'une université, d'un collège, d'une école technique ou d'une école de métier, ou d'un autre établissement</option>
    //   <option value="D" "="">Programme de deux ans d'une université, d'un collège, d'une école technique ou d'une école de métier, ou d'un autre établissement</option>
    //   <option value="E" "="">Baccalauréat (programme d'une université, d'un collège, d'une école technique ou d'une école de métier, ou d'un autre établissement)</option>
    //   <option value="F" "="">Au moins deux certificats, grades ou diplômes. Au moins l'un de ces diplômes a été obtenu à la suite de la réussite d'un programme d'études postsecondaires d'au moins trois ans</option>
    //   <option value="G" "="">Maîtrise ou diplôme professionnel nécessaire pour pratiquer une profession réglementée (voir Aide)</option>
    //   <option value="H" "="">Diplôme universitaire au niveau du doctorat (Ph. D.)</option>

    /**
     *
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*foreach ($this->levels as $phase) {
            EducationLevel::where("level_label_fr", "LIKE", $phase["label_fr"])
                ->update(['labelEng' => $phase["label_an"]]);
        }*/

        foreach ($this->levels as $level) {
            EducationLevel::create(
                [
                    'level_label_fr' => $level['label_fr'],
                    'labelEng'=> $level['label_an'],
                    'code' => $level['code'],
                ]
            );
        }
    }
}
