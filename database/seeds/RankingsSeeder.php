<?php

use App\Models\Ranking;
use Illuminate\Database\Seeder;

class RankingsSeeder extends Seeder
{
    private $rankings = [
        ['label'=> 'high'],
        ['label'=> 'medium'],
        ['label'=> 'low']
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->rankings as $ranking) {
            Ranking::create(
                [
                    'ranking_label' => $ranking['label']
                ]
            );
        }
    }
}
