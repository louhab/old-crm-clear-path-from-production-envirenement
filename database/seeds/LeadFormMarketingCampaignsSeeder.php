<?php

use App\Models\LeadFormMarketingCampaign;
use Illuminate\Database\Seeder;

class LeadFormMarketingCampaignsSeeder extends Seeder
{
    private $campaigns = [
        'Linkedin',
        'Facebook',
        'Google',
        'Influenceurs',
        'SMS',
        'Emails',
        'Siteweb',
        'BOOM',
        'Whatsapp',
        'Instagram',
        'Bureau',
        'Téléphone',
        'Représentant',
        'PM Instagram / Facebook',
        'Clients'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->campaigns as $campaign) {
            LeadFormMarketingCampaign::create(
                [
                    'campaign_name' => $campaign
                ]
            );
        }
    }
}
