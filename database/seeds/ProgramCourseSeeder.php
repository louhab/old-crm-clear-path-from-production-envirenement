<?php

use Illuminate\Database\Seeder;

use App\Models\ProgramCourse;

class ProgramCourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * [
        "",
        "High School",
        "10",
        "",
        "College",
        "Algonquin",
        "Ottawa, ON",
        "Automne/Hiver/Été",
        "",
        "Diploma",
        "Informatique",
        "Mobile Application Design and Development",
        "",
        "English",
        "2 Years",
        "95",
        "14203",
        "28406",
        "https://www.algonquincollege.com/mediaanddesign/program/mobile-application-design-and-development/",
        "",
        "Pas de test",
        "",
     * ]
     * @return void
     */
    public function run()
    {
        $columns = [
            "Budget" => "budget",
            "Niveau Scolaire" => "niveau_scolaire",
            "Note Scolaire" => "note",
            "Score" => "score",
            "Type d'établissement" => "type_etablissement",
            "Nom d'etablissement" => "nom_etablissement",
            "Ville" => "ville",
            "Session" => "session",
            "Date limite d'admission" => "date_limit_admission",
            "Diplômes" => "diplome",
            "Domaine d'études" => "domaine_etude",
            "Programmes" => "programme",
            "Concentration" => "concentration",
            "Langue" => "langue",
            "Durée" => "duree",
            "Frais d'admission" => "frais_admission",
            "Frais scolaires par année" => "prix_annuel",
            "Total des frais scolaires" => "prix_total",
            "Lien" => "lien",
            "Exigences" => "exigence",
            "Test de langue" => "test_langue",
            "Objectifs de formation et débouchés du diplôme" => "analyse",
        ];
        $file = fopen('database/seeds/csv/BD-R2-v2.csv', 'r');
        // foreach ($csv as $line_index => $line) {
        $first_line = fgetcsv($file);
        while (($line = fgetcsv($file)) !== FALSE) {
            //$line is an array of the csv elements
            $properties = array();
            $program = "";
            for ($x = 0; $x < count($first_line); $x++) {
                if ($first_line[$x] == "Programmes") {
                    $program = $line[$x];
                } else {
                    $key = trim($first_line[$x]);
                    $value = trim($line[$x]);
                    $field_name = $columns[trim($first_line[$x])];
                    $properties[$field_name]["key"] = $key;
                    $properties[$field_name]["value"] = $value;
                }
            }
            // print_r(json_encode($program));
            /*if (empty($program))
                continue;*/
            ProgramCourse::create([
                "name" => $program,
                "properties" => $properties
            ]);
            // print_r($properties);
            // break;
        }
        fclose($file);
    }
}
