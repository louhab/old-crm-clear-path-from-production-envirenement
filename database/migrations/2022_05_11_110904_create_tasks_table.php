<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('customer_case_id');
            $table->unsignedBigInteger('program_step_id');
            $table->text('note')->nullable();
            $table->string('canal')->nullable();
            $table->string('view')->nullable();
            $table->tinyInteger('is_done')->default(0);
            $table->integer('fees')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('customer_case_id')->references('id')->on('customer_cases')->onDelete('cascade');
            $table->foreign('program_step_id')->references('id')->on('program_steps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
