<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClear3SiblingFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clear3_sibling_fields', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedSmallInteger('order');
            //
            $table->string('sibling_name')->nullable();
            $table->date('sibling_birthdate')->nullable();
            $table->string('sibling_country')->nullable();
            $table->string('sibling_city')->nullable();
            $table->string('sibling_address')->nullable();
            $table->string('sibling_family_situation')->nullable();
            $table->string('sibling_current_occupation')->nullable();
            $table->string('sibling_email')->nullable();
            $table->timestamps();
            //
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clear3_sibling_fields');
    }
}
