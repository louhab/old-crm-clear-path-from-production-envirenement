<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLeftQueueToCurrentSelectedConseillersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('current_selected_conseillers', function (Blueprint $table) {
            $table->unsignedInteger("left_queue")->default(0)->after("queue");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('current_selected_conseillers', function (Blueprint $table) {
            $table->dropColumn('left_queue');
        });
    }
}
