<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVisaRequestFieldsToCustomerClear1000FieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_clear1000_fields', function (Blueprint $table) {
            // visa requests
            $table->string('visa_request_result')->nullable()->after('admission_fees');
            $table->unsignedBigInteger('visa_request_type')->nullable()->after('admission_fees');
            $table->date('visa_request_date')->nullable()->after('admission_fees');
            $table->unsignedBigInteger('visa_request')->nullable()->after('admission_fees');
            //
            $table->foreign('visa_request_type')->references('id')->on('programs');
            $table->foreign('visa_request')->references('id')->on('customer_radio_fields');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_clear1000_fields', function (Blueprint $table) {
            $table->dropColumn("visa_request_result");
            $table->dropColumn("visa_request_type");
            $table->dropColumn("visa_request_date");
            $table->dropColumn("visa_request");
        });
    }
}
