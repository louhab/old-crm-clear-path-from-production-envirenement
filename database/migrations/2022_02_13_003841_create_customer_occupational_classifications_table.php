<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerOccupationalClassificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_occupational_classifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cnp_id');
            $table->unsignedBigInteger('customer_id');
            $table->unique(['cnp_id', 'customer_id'], 'cnp_document');
            // occupational_classifications
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('cnp_id')->references('id')->on('occupational_classifications');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_occupational_classifications');
    }
}
