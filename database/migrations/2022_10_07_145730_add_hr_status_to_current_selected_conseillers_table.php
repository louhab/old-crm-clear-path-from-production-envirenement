<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHrStatusToCurrentSelectedConseillersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('current_selected_conseillers', function (Blueprint $table) {
            //
        });
        Schema::table('current_selected_conseillers', function (Blueprint $table) {
            $table->boolean("hr_status")->default(0)->after("status");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('current_selected_conseillers', function (Blueprint $table) {
            $table->dropColumn('hr_status');
        });
    }
}
