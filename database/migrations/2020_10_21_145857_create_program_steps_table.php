<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_steps', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('step_phase_id');
            $table->string('step_label', 150);
            $table->boolean('visible_for_conseiller')->default(true);
            $table->boolean('visible_for_agent')->default(true);
            $table->boolean('visible_for_backoffice')->default(true);
            $table->boolean('visible_for_customer')->default(true);
            $table->boolean('visible_for_support')->default(true);
            $table->timestamps();
            $table->softDeletes();

            //FKs
            $table->foreign('step_phase_id')->references('id')->on('step_phases');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_steps');
    }
}
