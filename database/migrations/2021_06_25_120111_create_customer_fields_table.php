<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_fields', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_field_group_id');
            $table->string('field_name');
            $table->string('template', 30);
            $table->string('step', 3)->nullable();
            $table->string('select_model')->nullable();
            $table->string('select_model_label')->nullable();
            $table->string('select_model_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            //FKs
            $table->foreign('customer_field_group_id')->references('id')->on('customer_field_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_fields');
    }
}
