<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SubStepStateProgramStep extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_step_sub_step_state', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('program_step_id');
            $table->unsignedBigInteger('sub_step_state_id');
            $table->unsignedBigInteger('program_id');
            $table->timestamps();
            $table->unique(['program_step_id', 'sub_step_state_id', 'program_id'], 'step_document_program');

            //FKs
            $table->foreign('program_step_id')->references('id')->on('program_steps');
            $table->foreign('sub_step_state_id')->references('id')->on('sub_step_states');
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_step_sub_step_state');
    }
}
