<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImmigrantExpressEntryAdmissionRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('immigrant_express_entry_admission_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_case_id');
            $table->timestamps();
            $table->softDeletes();

            //FKs
            $table->foreign('customer_case_id', 'immig_ee_admission_requests_customer_case_id_foreign')->references('id')->on('customer_cases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('immigrant_express_entry_admission_requests');
    }
}
