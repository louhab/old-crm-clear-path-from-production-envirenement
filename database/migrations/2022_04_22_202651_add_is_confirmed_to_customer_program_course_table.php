<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsConfirmedToCustomerProgramCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_program_course', function (Blueprint $table) {
            $table->boolean('is_confirmed')->default(0)->after("admis");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_program_course', function (Blueprint $table) {
            $table->dropColumn("is_confirmed");
        });
    }
}
