<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentTypeMediaSheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_type_media_sheet', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('document_type_id');
            $table->unsignedBigInteger('media_sheet_id');
            $table->unsignedBigInteger('program_id');
            $table->unsignedSmallInteger('order')->nullable();
            $table->boolean('required')->default(true);
            $table->timestamps();
            $table->unique(['document_type_id', 'media_sheet_id', 'program_id'], 'document_type_media_sheet');

            //FKs
            $table->foreign('document_type_id')->references('id')->on('document_types');
            $table->foreign('media_sheet_id')->references('id')->on('media_sheets');
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_type_media_sheet');
    }
}
