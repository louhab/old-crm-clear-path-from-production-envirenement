<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformationSheetProgramStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_sheet_program_step', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('information_sheet_id');
            $table->unsignedBigInteger('program_step_id');
            $table->unsignedBigInteger('program_id');
            $table->unsignedSmallInteger('order')->nullable();
            $table->timestamps();
            $table->unique(['information_sheet_id', 'program_step_id', 'program_id'], 'information_sheet_step_program');

            //FKs
            $table->foreign('information_sheet_id')->references('id')->on('information_sheets');
            $table->foreign('program_step_id')->references('id')->on('program_steps');
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information_sheet_program_step');
    }
}
