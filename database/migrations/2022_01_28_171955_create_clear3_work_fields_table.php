<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClear3WorkFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clear3_work_fields', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedSmallInteger('order');
            // Emploi actuelle
            $table->date('work_date_from')->nullable();
            $table->date('work_date_to')->nullable();
            $table->string('work_description')->nullable();
            $table->string('work_business_name')->nullable();
            $table->string('work_business_country')->nullable();
            $table->string('work_business_city')->nullable();
            $table->timestamps();
            $table->unique(['customer_id', 'order']);
            //
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clear3_work_fields');
    }
}
