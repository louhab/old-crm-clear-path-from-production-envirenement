<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramPaymentProgramStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_payment_program_step', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('program_payment_id');
            $table->unsignedBigInteger('program_step_id');
            $table->unsignedBigInteger('program_id');
            $table->unsignedBigInteger('currency_id')->default(1);
            $table->float('amount')->unsigned();
            $table->float("couple_amount")->unsigned()->nullable();
            $table->timestamps();
            $table->unique(['program_payment_id', 'program_step_id', 'program_id', 'currency_id'], 'program_payment_program_step_currency_unique');

            //FKs
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('program_payment_id')->references('id')->on('program_payments');
            $table->foreign('program_step_id')->references('id')->on('program_steps');
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_payment_program_step');
    }
}
