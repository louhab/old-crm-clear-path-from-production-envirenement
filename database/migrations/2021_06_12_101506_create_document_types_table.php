<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_types', function (Blueprint $table) {
            $table->id();
            $table->string('collection_name')->unique();
            $table->string('name_fr');
            $table->string('name_en');
            $table->boolean('signable')->default(false);
            $table->boolean('is_link')->default(false);
            $table->string('url')->nullable();
            $table->boolean('customer_owner')->default(false);
            $table->boolean('request_sent')->default(false);
            $table->boolean('visible_for_customer')->default(false);
            $table->boolean('pre_filled_xfa')->default(false);
            $table->string('xfa_view_name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_types');
    }
}
