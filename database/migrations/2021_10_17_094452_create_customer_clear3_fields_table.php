<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerClear3FieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_clear3_fields', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            //INFOS
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('sex')->nullable();
            $table->date('birthday')->nullable();
            $table->string('birthday_country')->nullable();
            $table->string('birthday_city')->nullable();
            $table->string('adresse_country', 150)->nullable();
            $table->string('adresse_canada', 150)->nullable();
            $table->string('phone')->nullable();
            $table->string('citizenship')->nullable();
            $table->string('current_marital_status')->nullable();
            $table->date('wedding_date')->nullable();
            $table->date('divorce_date')->nullable();
            $table->string('customer_email', 100)->nullable();
            $table->string('spoken_languages')->nullable();
            //Emploi actuelle
            $table->date('work_date_from')->nullable();
            $table->date('work_date_to')->nullable();
            $table->string('work_description')->nullable();
            $table->string('work_business_name')->nullable();
            $table->string('work_business_country')->nullable();
            $table->string('work_business_city')->nullable();

            // Scolarité
            $table->date('scholarship_date_from')->nullable();
            $table->date('scholarship_date_to')->nullable();
            $table->string('scholarship_institution_name')->nullable();
            $table->string('scholarship_country')->nullable();
            $table->string('scholarship_city')->nullable();
            $table->string('scholarship_diplomas_title')->nullable();
            $table->string('scholarship_domain')->nullable();

            //Informations memebre de la famille
            //Époux ou conjoint de faits
            $table->string('spouse_name')->nullable();
            $table->date('spouse_birthdate')->nullable();
            $table->string('spouse_country')->nullable();
            $table->string('spouse_city')->nullable();
            $table->string('spouse_address')->nullable();
            $table->string('spouse_family_situation')->nullable();
            $table->string('spouse_current_occupation')->nullable();
            $table->string('spouse_email')->nullable();
            //Mére
            $table->string('mother_name')->nullable();
            $table->date('mother_birthdate')->nullable();
            $table->string('mother_country')->nullable();
            $table->string('mother_city')->nullable();
            $table->string('mother_address')->nullable();
            $table->string('mother_family_situation')->nullable();
            $table->string('mother_current_occupation')->nullable();
            $table->string('mother_email')->nullable();
            //Pére
            $table->string('father_name')->nullable();
            $table->date('father_birthdate')->nullable();
            $table->string('father_country')->nullable();
            $table->string('father_city')->nullable();
            $table->string('father_address')->nullable();
            $table->string('father_family_situation')->nullable();
            $table->string('father_current_occupation')->nullable();
            $table->string('father_email')->nullable();
            // Enfants
            $table->string('child_name')->nullable();
            $table->date('child_birthdate')->nullable();
            $table->string('child_country')->nullable();
            $table->string('child_city')->nullable();
            $table->string('child_address')->nullable();
            $table->string('child_family_situation')->nullable();
            $table->string('child_current_occupation')->nullable();
            $table->string('child_email')->nullable();
            // Siblings
            $table->string('sibling_name')->nullable();
            $table->date('sibling_birthdate')->nullable();
            $table->string('sibling_country')->nullable();
            $table->string('sibling_city')->nullable();
            $table->string('sibling_address')->nullable();
            $table->string('sibling_family_situation')->nullable();
            $table->string('sibling_current_occupation')->nullable();
            $table->string('sibling_email')->nullable();

            //Pays de résidence et voyages
            //Avez-vous déjà visité le Canada?
            $table->string('did_visit_canada')->nullable();
            $table->date('did_visit_canada_date_from')->nullable();
            $table->date('did_visit_canada_date_to')->nullable();
            //Pays de résidence actuelle
            $table->string('current_residence_country')->nullable();
            $table->string('current_residence_city')->nullable();
            $table->string('current_residence_statut')->nullable();
            $table->date('current_residence_date_from')->nullable();
            $table->date('current_residence_date_to')->nullable();
            // Pays de résidence
            $table->string('past_residence_country')->nullable();
            $table->string('past_residence_city')->nullable();
            $table->string('past_residence_statut')->nullable();
            $table->date('past_residence_date_from')->nullable();
            $table->date('past_residence_date_to')->nullable();
            // Voyage divers
            $table->string('various_travel_country')->nullable();
            $table->string('various_travel_reason')->nullable();
            $table->date('various_travel_date_from')->nullable();
            $table->date('various_travel_date_to')->nullable();

            //La famille au Canada
            $table->string('family_in_canada')->nullable();
            $table->string('family_in_canada_relationship')->nullable();
            $table->string('family_in_canada_address')->nullable();
            $table->string('family_in_canada_phone')->nullable();
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_clear3_fields');
    }
}
