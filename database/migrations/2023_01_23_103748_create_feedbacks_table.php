<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->id();

            $table->string('first_name', 100)->nullable();
            $table->string('last_name', 100)->nullable();
            $table->string('phone', 50)->nullable();

            $table->unsignedTinyInteger("degree_of_professionalism")->nullable();
            $table->unsignedTinyInteger("quality_of_presentation")->nullable();
            $table->unsignedTinyInteger("degree_of_mastery")->nullable();
            $table->unsignedTinyInteger("speed")->nullable();
            $table->unsignedTinyInteger("overall")->nullable();
            $table->text("note")->nullable();

            $table->unsignedBigInteger('lead_id')->nullable();
            $table->unsignedBigInteger('conseiller_id')->nullable();

            $table->foreign('lead_id')->references('id')->on('leads');
            $table->foreign('conseiller_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedbacks');
    }
}
