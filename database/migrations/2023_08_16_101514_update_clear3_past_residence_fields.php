<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateClear3PastResidenceFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clear3_past_residence_fields', function (Blueprint $table) {
            $table->string('past_residence_zip', 255)->nullable()->after('past_residence_city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clear3_past_residence_fields', function (Blueprint $table) {
            $table->dropColumn(['past_residence_zip']);
        });
    }
}
