<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsLanguageTakenToCustomerClear1000FieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_clear1000_fields', function (Blueprint $table) {
            $table->unsignedBigInteger('is_language_taken')->nullable()->after('test_en');
            //FK
            $table->foreign('is_language_taken')->references('id')->on('is_language_test_takens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_clear1000_fields', function (Blueprint $table) {
            $table->dropColumn("is_language_taken");
        });
    }
}
