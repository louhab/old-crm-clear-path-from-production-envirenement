<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('program_id');
            $table->unsignedBigInteger('phase_id');
            $table->mediumText('name');
            $table->string('single_price')->nullable();
            $table->string('couple_price')->nullable();
            $table->boolean('billable');
            $table->boolean('can_edit_qte');
            $table->unsignedSmallInteger('order')->default(1);
            $table->timestamps();
            $table->softDeletes();

            //FKs
            $table->foreign('phase_id')->references('id')->on('phases');
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
