<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropUniqueCnpOccupationalClassificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('occupational_classifications', function (Blueprint $table) {
            // occupational_classifications_cnp_unique
            $table->dropUnique('occupational_classifications_cnp_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('occupational_classifications', function (Blueprint $table) {
            //
        });
    }
}
