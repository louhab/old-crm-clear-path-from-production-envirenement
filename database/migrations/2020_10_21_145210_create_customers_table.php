<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('program_id');
            $table->unsignedBigInteger('lead_id')->nullable();
            $table->unsignedBigInteger('ranking_id')->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->unsignedBigInteger('school_level_id')->nullable();
            $table->unsignedBigInteger('english_level_id')->nullable();
            $table->unsignedBigInteger('projected_diploma_type_id')->nullable();
            $table->unsignedBigInteger('professional_domain_id')->nullable();

            //$table->string('campaign', 100)->nullable();
            $table->unsignedBigInteger('campaign_id')->nullable();
            //INFOS
            $table->string('firstname', 30)->nullable();
            $table->string('lastname', 30)->nullable();
            $table->string('sex', 10)->nullable();
            $table->date('birthday')->nullable();
            $table->string('birthday_country')->nullable();
            $table->string('birthday_city')->nullable();
            $table->string('adresse_country', 150)->nullable();
            $table->string('postal_code', 10)->nullable();
            $table->string('adresse_canada', 150)->nullable();
            $table->string('phone')->unique();
            $table->string('citizenship', 15)->nullable();
            $table->string('current_marital_status', 15)->nullable();
            $table->date('wedding_date')->nullable();
            $table->date('divorce_date')->nullable();
            $table->string('email', 100)->nullable()->unique();
            $table->string('customer_email', 100)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->timestamp('password_changed_at')->nullable();
            $table->string('spoken_languages', 15)->nullable();

            //Emploi actuelle
            $table->date('work_date_from')->nullable();
            $table->date('work_date_to')->nullable();
            $table->string('work_description', 15)->nullable();
            $table->string('work_business_name', 15)->nullable();
            $table->string('work_business_country', 15)->nullable();
            $table->string('work_business_city', 15)->nullable();

            //Informations memebre de la famille
            //Époux ou conjoint de faits
            $table->string('spouse_name', 15)->nullable();
            $table->date('spouse_birthdate')->nullable();
            $table->string('spouse_country', 15)->nullable();
            $table->string('spouse_city', 15)->nullable();
            $table->string('spouse_address', 15)->nullable();
            $table->string('spouse_family_situation', 15)->nullable();
            $table->string('spouse_current_occupation', 15)->nullable();
            $table->string('spouse_email', 15)->nullable();
            //Mére
            $table->string('mother_name', 15)->nullable();
            $table->date('mother_birthdate')->nullable();
            $table->string('mother_country', 15)->nullable();
            $table->string('mother_city', 15)->nullable();
            $table->string('mother_address', 15)->nullable();
            $table->string('mother_family_situation', 15)->nullable();
            $table->string('mother_current_occupation', 15)->nullable();
            $table->string('mother_email', 15)->nullable();
            //Pére
            $table->string('father_name', 15)->nullable();
            $table->date('father_birthdate')->nullable();
            $table->string('father_country', 15)->nullable();
            $table->string('father_city', 15)->nullable();
            $table->string('father_address', 15)->nullable();
            $table->string('father_family_situation', 15)->nullable();
            $table->string('father_current_occupation', 15)->nullable();
            $table->string('father_email', 15)->nullable();

            //Pays de résidence et voyages
            //Avez-vous déjà visité le Canada?
            $table->string('did_visit_canada', 15)->nullable();
            $table->date('did_visit_canada_date_from')->nullable();
            $table->date('did_visit_canada_date_to')->nullable();
            //Pays de résidence actuelle
            $table->string('current_residence_country', 15)->nullable();
            $table->string('current_residence_city', 15)->nullable();
            $table->string('current_residence_statut', 15)->nullable();
            $table->date('current_residence_date_from')->nullable();
            $table->date('current_residence_date_to')->nullable();

            //La famille au Canada
            $table->string('family_in_canada', 15)->nullable();
            $table->string('family_in_canada_relationship', 15)->nullable();
            $table->string('family_in_canada_address', 15)->nullable();
            $table->string('family_in_canada_phone', 15)->nullable();

            $table->string('adresse_line_1', 150)->nullable();
            $table->string('adresse_line_2', 150)->nullable();


            $table->decimal('budget')->nullable();
            $table->unsignedSmallInteger('professional_experience')->nullable();
            $table->unsignedInteger('score')->nullable();

            $table->unsignedBigInteger('lead_status')->nullable();
            $table->unsignedBigInteger('admission_state')->nullable();
            $table->boolean('is_client')->default(0);



            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            //FKs
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('campaign_id')->references('id')->on('lead_form_marketing_campaigns');
            $table->foreign('lead_status')->references('id')->on('lead_statuses');
            $table->foreign('admission_state')->references('id')->on('admission_states');
            $table->foreign('lead_id')->references('id')->on('leads')->onDelete('cascade');
            $table->foreign('ranking_id')->references('id')->on('rankings');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('school_level_id')->references('id')->on('school_levels');
            $table->foreign('english_level_id')->references('id')->on('english_levels');
            $table->foreign('projected_diploma_type_id')->references('id')->on('projected_diploma_types');
            $table->foreign('professional_domain_id')->references('id')->on('professional_domains');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
