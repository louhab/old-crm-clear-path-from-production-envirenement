<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerVisaRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_visa_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('visa_request')->nullable();
            $table->date('visa_request_date')->nullable();
            $table->unsignedBigInteger('visa_request_type')->nullable();
            $table->string('visa_request_result')->nullable();
            $table->timestamps();
            //
            $table->foreign('visa_request')->references('id')->on('customer_radio_fields');
            $table->foreign('visa_request_type')->references('id')->on('programs');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_visa_requests');
    }
}
