<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipStudentGuarantorToCustomerClear1000FieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_clear1000_fields', function (Blueprint $table) {
            // relationship_student_guarantor
            $table->unsignedBigInteger('relationship_student_guarantor')->nullable()->after('business_income');
            //FK
            $table->foreign('relationship_student_guarantor')->references('id')->on('guarantor_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_clear1000_fields', function (Blueprint $table) {
            $table->dropColumn("relationship_student_guarantor");
        });
    }
}
