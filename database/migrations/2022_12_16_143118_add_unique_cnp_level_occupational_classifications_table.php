<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUniqueCnpLevelOccupationalClassificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('occupational_classifications', function (Blueprint $table) {
            $table->unique(["cnp", "level"], 'cnp_level_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('occupational_classifications', function (Blueprint $table) {
            $table->dropUnique('cnp_level_unique');
        });
    }
}
