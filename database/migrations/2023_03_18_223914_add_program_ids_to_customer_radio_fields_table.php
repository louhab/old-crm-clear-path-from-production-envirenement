<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProgramIdsToCustomerRadioFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_radio_fields', function (Blueprint $table) {
            $table->json('program_ids')->nullable()->after('value_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_radio_fields', function (Blueprint $table) {
            $table->dropColumn('program_ids');
        });
    }
}
