<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerRadioFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_radio_fields', function (Blueprint $table) {
            $table->id();
            $table->string('name', 35);
            $table->string('code', 35)->unique();
            $table->string('value_fr');
            $table->string('value_en');
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['name', 'code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_radio_fields');
    }
}
