<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentMethodToProgramPaymentCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_payment_customer', function (Blueprint $table) {
            $table->unsignedBigInteger('payment_method')->nullable();
            $table->foreign('payment_method')->references('id')->on('payment_methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program_payment_customer', function (Blueprint $table) {
            $table->dropColumn('payment_method');
        });
    }
}
