<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStateBeforeToHistoCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('histo_calls', function (Blueprint $table) {
            $table->unsignedBigInteger('state_before')->nullable()->after('lead_state_id');
            $table->foreign('state_before')->references('id')->on('lead_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('histo_calls', function (Blueprint $table) {
            $table->dropColumn('state_before');
        });
    }
}
