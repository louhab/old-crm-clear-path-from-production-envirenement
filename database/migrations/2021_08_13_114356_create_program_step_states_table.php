<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramStepStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_step_states', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('program_step_id');
            $table->unsignedBigInteger('program_id');
            $table->string('select_box_label_fr', 30);
            $table->string('select_box_label_en', 30);
            $table->text('customer_message_fr');
            $table->text('customer_message_en');
            $table->string('customer_message_style_class', 30);
            $table->timestamps();
            $table->softDeletes();

            //FKs
            $table->foreign('program_step_id')->references('id')->on('program_steps');
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_step_states');
    }
}
