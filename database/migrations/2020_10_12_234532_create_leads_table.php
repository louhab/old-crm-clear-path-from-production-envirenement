<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('program_id');
            $table->unsignedBigInteger('ranking_id')->nullable();
            $table->unsignedBigInteger('campaign_id')->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->unsignedBigInteger('school_level_id')->nullable();
            $table->unsignedBigInteger('english_level_id')->nullable();
            $table->unsignedBigInteger('lead_status_id')->nullable();
            $table->unsignedBigInteger('projected_diploma_type_id')->nullable();
            $table->unsignedBigInteger('professional_domain_id')->nullable();
            $table->string('firstname', 30);
            $table->string('lastname', 30);
            $table->string('email', 100)->nullable()->unique();
            $table->date('birthday')->nullable();
            $table->string('phone', 15)->unique();
            $table->string('adresse_line_1', 150)->nullable();
            $table->string('adresse_line_2', 150)->nullable();
            $table->string('postal_code', 10)->nullable();
            $table->decimal('budget')->nullable();
            $table->unsignedSmallInteger('professional_experience')->nullable();
            $table->unsignedBigInteger('conseiller_id')->nullable();
            $table->unsignedBigInteger('support_id')->nullable();
            $table->unsignedBigInteger('influencer')->nullable();
            $table->boolean('is_form')->default(0);
            $table->timestamps();
            $table->softDeletes();

            //FKs
            $table->foreign('campaign_id')->references('id')->on('lead_form_marketing_campaigns');
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('ranking_id')->references('id')->on('rankings');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('school_level_id')->references('id')->on('school_levels');
            $table->foreign('english_level_id')->references('id')->on('english_levels');
            $table->foreign('lead_status_id')->references('id')->on('lead_statuses');
            $table->foreign('projected_diploma_type_id')->references('id')->on('projected_diploma_types');
            $table->foreign('professional_domain_id')->references('id')->on('professional_domains');
            $table->foreign('conseiller_id')->references('id')->on('users');
            $table->foreign('influencer')->references('id')->on('lead_forms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
