<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_options', function (Blueprint $table) {
            $table->id();
            $table->string("name_fr")->unique();
            $table->unsignedBigInteger('program_payment_id');
            $table->unsignedBigInteger('program_id');
            $table->float('amount')->unsigned();
            $table->boolean('has_couple')->default(0);
            $table->timestamps();

            //FKs
            $table->foreign('program_payment_id')->references('id')->on('program_payments');
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_options');
    }
}
