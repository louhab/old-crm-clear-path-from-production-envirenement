<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGroupIdToAdmissionStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admission_states', function (Blueprint $table) {
            $table->unsignedBigInteger('admission_state_group_id')->nullable()->after('id');
            $table->unsignedSmallInteger('order')->nullable()->after('visible');
            //
            $table->foreign('admission_state_group_id')->references('id')->on('admission_state_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admission_states', function (Blueprint $table) {
            $table->dropColumn('admission_state_group_id');
            $table->dropColumn('order');
        });
    }
}
