<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateLeadStatusTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE TRIGGER tr_Lead_State_Change AFTER UPDATE ON `leads` FOR EACH ROW
            BEGIN
                IF OLD.lead_status_id <> new.lead_status_id THEN
                    INSERT INTO lead_state_logs(user_id, lead_id, state_id)
                    VALUES(old.id, new.id, new.lead_status_id);
                END IF;
                INSERT INTO role_user (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES (3, NEW.id, now(), null);
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `tr_Lead_State_Change`');
    }
}
