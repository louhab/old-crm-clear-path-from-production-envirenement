<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImmigrantLanguagesAndDiplomasInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('immigrant_languages_and_diplomas_information', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_case_id');
            $table->timestamps();
            $table->softDeletes();

            //FKs
            $table->foreign('customer_case_id', 'immig_languages_diplomas_information_customer_case_id_foreign')->references('id')->on('customer_cases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('immigrant_languages_and_diplomas_information');
    }
}
