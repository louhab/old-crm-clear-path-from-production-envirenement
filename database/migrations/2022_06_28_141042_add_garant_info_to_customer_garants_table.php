<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGarantInfoToCustomerGarantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_garants', function (Blueprint $table) {
            $table->unsignedBigInteger('goods_res')->nullable()->after("customer_id");
            $table->string('tef_test_res')->nullable()->after("customer_id");

            $table->unsignedBigInteger('guarantor_work_income')->nullable()->after("customer_id");
            $table->unsignedBigInteger('guarantor_retirement_income')->nullable()->after("customer_id");
            $table->unsignedBigInteger('guarantor_business_income')->nullable()->after("customer_id");
            $table->unsignedBigInteger('guarantor_professional_income')->nullable()->after("customer_id");
            $table->unsignedBigInteger('guarantor_self_employed_income')->nullable()->after("customer_id");
            $table->unsignedBigInteger('guarantor_rental_income')->nullable()->after("customer_id");
            $table->unsignedBigInteger('guarantor_investment_income')->nullable()->after("customer_id");

            $table->unsignedBigInteger('study_session')->nullable()->after("customer_id");
            $table->string('garant_last_name')->nullable()->after("customer_id");
            $table->string('garant_first_name')->nullable()->after("customer_id");
            // Add FKs if needed
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_garants', function (Blueprint $table) {
            $table->dropColumn(
                [
                    'study_session',
                    'garant_last_name',
                    'garant_first_name',

                    'guarantor_work_income',
                    'guarantor_retirement_income',
                    'guarantor_business_income',
                    'guarantor_professional_income',
                    'guarantor_self_employed_income',
                    'guarantor_rental_income',
                    'guarantor_investment_income',
                    'goods_res',
                    'tef_test_res',
                ]);
        });
    }
}
