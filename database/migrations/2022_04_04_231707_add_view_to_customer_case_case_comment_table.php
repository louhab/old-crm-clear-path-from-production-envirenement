<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddViewToCustomerCaseCaseCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_case_case_comment', function (Blueprint $table) {
            $table->string('view')->nullable()->after('canal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_case_case_comment', function (Blueprint $table) {
            $table->dropColumn('view');
        });
    }
}
