<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateClear3VariousTravelFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clear3_various_travel_fields', function (Blueprint $table) {
            $table->string('various_travel_city', 255)->nullable()->after('various_travel_country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clear3_various_travel_fields', function (Blueprint $table) {
            $table->dropColumn(['various_travel_city']);
        });
    }
}
