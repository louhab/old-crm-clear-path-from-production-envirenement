<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGroupToCustomerFieldInformationSheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_field_information_sheet', function (Blueprint $table) {
            $table->unsignedBigInteger('customer_field_group_id')->nullable()->after('program_id');
            $table->foreign('customer_field_group_id')->references('id')->on('customer_field_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_field_information_sheet', function (Blueprint $table) {
            $table->dropColumn('customer_field_group_id');
        });
    }
}
