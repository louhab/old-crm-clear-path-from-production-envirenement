<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerClear1000FieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_clear1000_fields', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('customer_id')->unique();
            $table->unsignedBigInteger('level_study')->nullable();
            //$table->unsignedInteger('grade')->nullable();
            $table->unsignedDecimal('grade', $precision = 4, $scale = 2)->nullable();
            $table->unsignedBigInteger('branch')->nullable();
            $table->unsignedBigInteger('lang_level')->nullable();
            $table->unsignedBigInteger('diploma_fr')->nullable();
            $table->unsignedBigInteger('test_fr')->nullable();
            $table->unsignedBigInteger('diploma_en')->nullable();
            $table->unsignedBigInteger('test_en')->nullable();
            $table->unsignedBigInteger('garant_type')->nullable();
            $table->unsignedBigInteger('income')->nullable();
            $table->unsignedBigInteger('amount')->nullable();
            $table->unsignedBigInteger('goods')->nullable();
            $table->unsignedBigInteger('rental_income')->nullable();
            $table->unsignedBigInteger('business_income')->nullable();
            $table->unsignedBigInteger('medical_visit')->nullable();
            $table->unsignedBigInteger('health')->nullable();
            $table->unsignedBigInteger('criminal_record')->nullable();
            $table->unsignedBigInteger('anthroprometric')->nullable();
            //new fields
            $table->unsignedBigInteger('tuition_fees')->nullable();
            $table->unsignedBigInteger('cpg_account')->nullable();
            $table->unsignedBigInteger('tef_test')->nullable();
            $table->unsignedBigInteger('language')->nullable();
            $table->unsignedBigInteger('city')->nullable();
            $table->unsignedBigInteger('province')->nullable();
            $table->string('training_type', 50)->nullable();
            $table->unsignedBigInteger('diplomas')->nullable();
            $table->unsignedBigInteger('program_first')->nullable();
            $table->unsignedBigInteger('program_second')->nullable();
            $table->unsignedBigInteger('program_third')->nullable();
            $table->unsignedBigInteger('annual_budget')->nullable();
            $table->unsignedBigInteger('admission_fees')->nullable();
            $table->timestamps();
            
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('level_study')->references('id')->on('level_of_studies');
            $table->foreign('branch')->references('id')->on('branches');
            $table->foreign('lang_level')->references('id')->on('language_levels');
            $table->foreign('diploma_fr')->references('id')->on('french_diplomas');
            $table->foreign('test_fr')->references('id')->on('french_tests');
            $table->foreign('diploma_en')->references('id')->on('english_diplomas');
            $table->foreign('test_en')->references('id')->on('english_tests');
            $table->foreign('garant_type')->references('id')->on('guarantor_types');
            $table->foreign('income')->references('id')->on('monthly_incomes');
            $table->foreign('amount')->references('id')->on('amount_availables');

            $table->foreign('goods')->references('id')->on('customer_radio_fields');
            $table->foreign('rental_income')->references('id')->on('customer_radio_fields');
            $table->foreign('business_income')->references('id')->on('customer_radio_fields');
            $table->foreign('medical_visit')->references('id')->on('customer_radio_fields');
            $table->foreign('health')->references('id')->on('customer_radio_fields');
            $table->foreign('criminal_record')->references('id')->on('customer_radio_fields');
            $table->foreign('anthroprometric')->references('id')->on('customer_radio_fields');

            $table->foreign('tuition_fees')->references('id')->on('customer_radio_fields');
            $table->foreign('cpg_account')->references('id')->on('customer_radio_fields');
            $table->foreign('tef_test')->references('id')->on('customer_radio_fields');
            $table->foreign('language')->references('id')->on('customer_radio_fields');
            $table->foreign('city')->references('id')->on('canada_cities');
            $table->foreign('province')->references('id')->on('provinces');
            $table->foreign('training_type')->references('code')->on('customer_radio_fields');
            $table->foreign('diplomas')->references('id')->on('customer_radio_fields');
            $table->foreign('program_first')->references('id')->on('program_choices');
            $table->foreign('program_second')->references('id')->on('program_choices');
            $table->foreign('program_third')->references('id')->on('program_choices');
            $table->foreign('annual_budget')->references('id')->on('customer_radio_fields');
            $table->foreign('admission_fees')->references('id')->on('customer_radio_fields');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_clear1000_fields');
    }
}
