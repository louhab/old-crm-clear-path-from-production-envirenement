<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerFieldProgramStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_field_program_step', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_field_id');
            $table->unsignedBigInteger('program_step_id');
            $table->unsignedBigInteger('program_id');
            $table->unsignedSmallInteger('order')->nullable();
            $table->boolean('required')->default(true);
            $table->timestamps();
            $table->unique(['customer_field_id', 'program_step_id', 'program_id'], 'customer_field_step_program');

            //FKs
            $table->foreign('customer_field_id')->references('id')->on('customer_fields');
            $table->foreign('program_step_id')->references('id')->on('program_steps');
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_field_program_step');
    }
}
