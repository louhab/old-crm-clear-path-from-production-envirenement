<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSubStepsToCustomerCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_cases', function (Blueprint $table) {
            $table->unsignedBigInteger('program_sub_step_id')->nullable()->after('program_step_id');

            $table->foreign('program_sub_step_id')->references('id')->on('program_sub_steps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_cases', function (Blueprint $table) {
            $table->dropColumn('program_sub_step_id');
        });
    }
}
