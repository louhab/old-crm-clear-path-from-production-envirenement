<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCustomerGarantIdToCustomerFieldValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_field_values', function (Blueprint $table) {
            $table->unsignedBigInteger('customer_garant_id')->nullable()->after("customer_id");
            $table->unique(['customer_id', 'field_name', 'customer_garant_id', 'model_id'], 'customer_field_garant_value');
            $table->foreign('customer_garant_id')->references('id')->on('customer_garants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_field_values', function (Blueprint $table) {
            $table->dropColumn("customer_garant_id");
        });
    }
}
