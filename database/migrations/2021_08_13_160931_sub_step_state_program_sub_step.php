<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SubStepStateProgramSubStep extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_sub_step_sub_step_state', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('program_sub_step_id');
            $table->unsignedBigInteger('sub_step_state_id');
            $table->unsignedBigInteger('program_id');
            $table->unsignedSmallInteger('order')->nullable();
            $table->timestamps();
            $table->unique(['program_sub_step_id', 'sub_step_state_id', 'program_id'], 'step_document_program');

            //FKs
            $table->foreign('program_sub_step_id')->references('id')->on('program_sub_steps');
            $table->foreign('sub_step_state_id')->references('id')->on('sub_step_states');
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_step_sub_step_state');
    }
}
