<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAmountEurAndAmountUsdToAmountAvailables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amount_availables', function (Blueprint $table) {
            $table->string('amount_eur')->nullable()->default(null)->after('amount');
            $table->string('amount_usd')->nullable()->default(null)->after('amount_eur');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amount_availables', function (Blueprint $table) {
            $table->dropColumn(['amount_eur', 'amount_usd']);
        });
    }
}
