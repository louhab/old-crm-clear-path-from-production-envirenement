<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStudentProgramIdToLeadFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lead_forms', function (Blueprint $table) {
            $table->unsignedBigInteger('student_program_id')->nullable()->after('is_students_form');
            // FK
            $table->foreign('student_program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lead_forms', function (Blueprint $table) {
            $table->dropColumn("student_program_id");
        });
    }
}
