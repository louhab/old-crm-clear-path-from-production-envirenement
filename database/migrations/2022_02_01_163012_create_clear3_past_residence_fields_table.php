<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClear3PastResidenceFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clear3_past_residence_fields', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedSmallInteger('order');
            // Pays de résidence
            $table->string('past_residence_country')->nullable();
            $table->string('past_residence_city')->nullable();
            $table->string('past_residence_statut')->nullable();
            $table->date('past_residence_date_from')->nullable();
            $table->date('past_residence_date_to')->nullable();
            $table->timestamps();
            //
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clear3_past_residence_fields');
    }
}
