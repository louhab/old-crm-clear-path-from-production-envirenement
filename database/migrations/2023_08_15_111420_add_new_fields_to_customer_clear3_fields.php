<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToCustomerClear3Fields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_clear3_fields', function (Blueprint $table) {
            $table->text('eye_colour', 255)->nullable()->after('family_in_canada_phone');
            $table->text('size', 255)->nullable()->after('eye_colour');
            $table->text('previous_spouse_firstname', 255)->nullable()->after('size');
            $table->text('previous_spouse_lastname', 255)->nullable()->after('previous_spouse_firstname');
            $table->text('passport_number', 255)->nullable()->after('previous_spouse_lastname');
            $table->date('passport_expiration_date')->nullable()->after('passport_number');

            $table->text('work_business_status', 255)->nullable()->after('work_business_city');
            $table->text('past_residence_zip', 255)->nullable()->after('past_residence_city');
            $table->text('various_travel_city', 255)->nullable()->after('various_travel_country');

            $table->text('desired_province_for_living', 255)->nullable()->after('various_travel_city');
            $table->text('desired_city_for_living', 255)->nullable()->after('desired_province_for_living');
            $table->text('has_cert_for_living', 255)->nullable()->after('desired_city_for_living');


            // section &
            $table->text('extra_received_conviction', 255)->nullable();
            $table->text('extra_committed_criminal_act', 255)->nullable();
            $table->text('extra_submitted_request', 255)->nullable();
            $table->text('extra_denied_refugee_status', 255)->nullable();
            $table->text('extra_been_refused_visa', 255)->nullable();
            $table->text('extra_took_part_in_act_genocide', 255)->nullable();
            $table->text('extra_advocated_use_armed_struggle', 255)->nullable();
            $table->text('extra_had_ties_to_group_that_used_armed_struggle', 255)->nullable();
            $table->text('extra_been_member_of_organization', 255)->nullable();
            $table->text('extra_is_subject_detention', 255)->nullable();
            $table->text('extra_suffered_from_serious_illness', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_clear3_fields', function (Blueprint $table) {
            $table->dropColumn(
                [
                    'eye_colour',
                    'size',
                    'previous_spouse_firstname',
                    'previous_spouse_lastname',
                    'passport_number',
                    'passport_expiration_date',

                    'work_business_status',
                    'past_residence_zip',
                    'various_travel_city',

                    'desired_province_for_living',
                    'desired_city_for_living',
                    'has_cert_for_living',

                    'extra_received_conviction',
                    'extra_committed_criminal_act',
                    'extra_submitted_request',
                    'extra_denied_refugee_status',
                    'extra_been_refused_visa',
                    'extra_took_part_in_act_genocide',
                    'extra_advocated_use_armed_struggle',
                    'extra_had_ties_to_group_that_used_armed_struggle',
                    'extra_been_member_of_organization',
                    'extra_is_subject_detention',
                    'extra_suffered_from_serious_illness',
                ]
            );
        });
    }
}
