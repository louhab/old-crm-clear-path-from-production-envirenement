<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCommercialToLeadFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lead_forms', function (Blueprint $table) {
            $table->unsignedBigInteger('commercial_id')->nullable();
            $table->unsignedTinyInteger('is_for_commercial')->default(false)->nullable();
            // FK
            // $table->foreign('commencial_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lead_forms', function (Blueprint $table) {
            $table->dropColumn(
                [
                    'commercial_id',
                    'is_for_commercial',
                ]
            );
        });
    }
}
