<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerClear1004FieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_clear1004_fields', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('customer_id')->unique();
            //Scoring
            $table->string('marital_status', 35)->nullable();
            $table->string('spouse_cit', 35)->nullable();
            $table->string('spouse_joining', 35)->nullable();
            $table->string('age', 35)->nullable();
            $table->string('education_level', 35)->nullable();
            $table->string('diplomas_canadien', 35)->nullable();
            $table->string('education_type', 35)->nullable();
            $table->string('lang_test_expire', 35)->nullable();
            $table->string('lang_test', 35)->nullable();
            $table->string('lang_oral_exp_test_score', 35)->nullable();
            $table->string('lang_oral_comp_test_score', 35)->nullable();
            $table->string('lang_writing_comp_test_score', 35)->nullable();
            $table->string('lang_writing_exp_test_score', 35)->nullable();
            $table->string('other_lang_test', 35)->nullable();
            $table->string('lang_speaking', 35)->nullable();
            $table->string('lang_listening', 35)->nullable();
            $table->string('lang_reading', 35)->nullable();
            $table->string('lang_writing', 35)->nullable();
            $table->string('work_experience_canada', 35)->nullable();
            $table->string('work_experience_abroad', 35)->nullable();
            $table->string('certificate_of_competence', 35)->nullable();
            $table->string('valid_job', 35)->nullable();
            $table->string('job_kind', 35)->nullable();
            $table->string('certificate_of_designation', 35)->nullable();
            $table->string('sibling_canada', 35)->nullable();
            $table->string('spouse_education_level', 35)->nullable();
            $table->string('spouse_work_experience_canada', 35)->nullable();
            $table->string('spouse_lang_test', 35)->nullable();
            $table->string('spouse_lang_speaking', 35)->nullable();
            $table->string('spouse_lang_listening', 35)->nullable();
            $table->string('spouse_lang_reading', 35)->nullable();
            $table->string('spouse_lang_writing', 35)->nullable();
            $table->timestamps();
            
            //FKs
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('marital_status')->references('code')->on('marital_statuses');
            $table->foreign('spouse_cit')->references('code')->on('customer_radio_fields');
            $table->foreign('spouse_joining')->references('code')->on('customer_radio_fields');
            $table->foreign('age')->references('code')->on('customer_ages');
            $table->foreign('education_level')->references('code')->on('education_levels');
            $table->foreign('diplomas_canadien')->references('code')->on('customer_radio_fields');

            $table->foreign('education_type')->references('code')->on('customer_radio_fields');
            $table->foreign('lang_test_expire')->references('code')->on('customer_radio_fields');
            $table->foreign('lang_test')->references('code')->on('customer_radio_fields');
            // $table->foreign('lang_oral_exp_test_score')->references('code')->on('language_test_scores');
            // $table->foreign('lang_oral_comp_test_score')->references('code')->on('language_test_scores');
            // $table->foreign('lang_writing_comp_test_score')->references('code')->on('language_test_scores');
            // $table->foreign('lang_writing_exp_test_score')->references('code')->on('language_test_scores');
            $table->foreign('other_lang_test')->references('code')->on('customer_radio_fields');
            // $table->foreign('lang_speaking')->references('code')->on('language_test_scores');
            // $table->foreign('lang_listening')->references('code')->on('language_test_scores');
            // $table->foreign('lang_reading')->references('code')->on('language_test_scores');
            // $table->foreign('lang_writing')->references('code')->on('language_test_scores');
            $table->foreign('work_experience_canada')->references('code')->on('work_experiences');
            $table->foreign('work_experience_abroad')->references('code')->on('work_experience_abroads');
            $table->foreign('certificate_of_competence')->references('code')->on('customer_radio_fields');
            $table->foreign('valid_job')->references('code')->on('customer_radio_fields');
            $table->foreign('job_kind')->references('code')->on('customer_radio_fields');
            $table->foreign('certificate_of_designation')->references('code')->on('customer_radio_fields');
            $table->foreign('sibling_canada')->references('code')->on('customer_radio_fields');
            $table->foreign('spouse_education_level')->references('code')->on('education_levels');
            $table->foreign('spouse_work_experience_canada')->references('code')->on('work_experiences');
            $table->foreign('spouse_lang_test')->references('code')->on('customer_radio_fields');
            // $table->foreign('spouse_lang_speaking')->references('code')->on('language_test_scores');
            // $table->foreign('spouse_lang_listening')->references('code')->on('language_test_scores');
            // $table->foreign('spouse_lang_reading')->references('code')->on('language_test_scores');
            // $table->foreign('spouse_lang_writing')->references('code')->on('language_test_scores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_clear1004_fields');
    }
}
