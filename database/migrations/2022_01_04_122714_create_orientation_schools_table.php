<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrientationSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orientation_schools', function (Blueprint $table) {
            $table->id();
            // Budget, Langue, Ville, Session, Date limite d'admission, Niveau Scolaire
            // Note du dernier diplôme, Test de langue, Score, Domaine d'études, Programmes
            // Diplômes, Type d'établissement, Nom d'etablissement, Durée, Frais d'admission
            // Prix annuel, Prix total, Lien, Code de programme, Exigences, Analyse, Classement des choix
            $table->string('budget');
            $table->string('langue');
            $table->string('ville');
            $table->string('session');
            $table->string('date_limit_admission');
            $table->string('niveau_scolaire');
            $table->string('note');
            $table->string('test_langue');
            $table->string('score');
            $table->string('domaine_etude');
            $table->string('programme');
            $table->string('diplome');
            $table->string('type_etablissement');
            $table->string('nom_etablissement');
            $table->string('duree');
            $table->string('frais_admission');
            $table->string('prix_annuel');
            $table->string('prix_total');
            $table->string('lien');
            $table->string('code_programme');
            $table->string('exigence');
            $table->string('Analyse');
            $table->string('classement_choix');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orientation_schools');
    }
}
