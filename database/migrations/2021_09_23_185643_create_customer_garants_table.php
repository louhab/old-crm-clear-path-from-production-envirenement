<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerGarantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_garants', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            //$table->unsignedSmallInteger('order');
            $table->unsignedBigInteger('garant_type')->nullable();
            $table->unsignedBigInteger('income')->nullable();
            $table->unsignedBigInteger('amount')->nullable();
            $table->unsignedBigInteger('goods')->nullable();
            $table->unsignedBigInteger('rental_income')->nullable();
            $table->unsignedBigInteger('business_income')->nullable();
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('garant_type')->references('id')->on('guarantor_types');
            $table->foreign('income')->references('id')->on('monthly_incomes');
            $table->foreign('amount')->references('id')->on('amount_availables');
            $table->foreign('goods')->references('id')->on('customer_radio_fields');
            $table->foreign('rental_income')->references('id')->on('customer_radio_fields');
            $table->foreign('business_income')->references('id')->on('customer_radio_fields');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_garants');
    }
}
