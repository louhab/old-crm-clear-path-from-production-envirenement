<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMeetsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->unsignedBigInteger('r1')->nullable()->after("mother_phone");
            $table->unsignedBigInteger('r2')->nullable()->after("r1");
            $table->unsignedBigInteger('r3')->nullable()->after("r2");
            $table->unsignedBigInteger('r4')->nullable()->after("r3");
            $table->unsignedBigInteger('r5')->nullable()->after("r4");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn(
                [
                    'r1', 'r2', 'r3', 'r4', 'r5'
                ]
            );
        });
    }
}
