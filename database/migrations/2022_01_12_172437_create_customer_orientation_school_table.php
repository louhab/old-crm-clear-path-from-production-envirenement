<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerOrientationSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_orientation_school', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('orientation_school_id');
            $table->unsignedBigInteger('customer_id');
            $table->timestamps();
            $table->unique(['orientation_school_id', 'customer_id'], 'orientation_school_customer_unique');

            $table->foreign('orientation_school_id')->references('id')->on('orientation_schools');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_orientation_school');
    }
}
