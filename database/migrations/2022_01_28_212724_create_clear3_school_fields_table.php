<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClear3SchoolFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clear3_school_fields', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedSmallInteger('order');
            // Scolarité
            $table->date('scholarship_date_from')->nullable();
            $table->date('scholarship_date_to')->nullable();
            $table->string('scholarship_institution_name')->nullable();
            $table->string('scholarship_country')->nullable();
            $table->string('scholarship_city')->nullable();
            $table->string('scholarship_diplomas_title')->nullable();
            $table->string('scholarship_domain')->nullable();
            $table->timestamps();
            $table->unique(['customer_id', 'order']);
            //
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clear3_school_fields');
    }
}
