<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMathGradeToCustomerClear1000FieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_clear1000_fields', function (Blueprint $table) {
            $table->unsignedDecimal('math_grade', $precision = 4, $scale = 2)->nullable()->after('grade');
            $table->string('bank_amount')->nullable()->after('amount');
            $table->string('est_name')->nullable()->after('city');
            $table->string('health_rep')->nullable()->after('health');
            $table->string('criminal_record_rep')->nullable()->after('criminal_record');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_clear1000_fields', function (Blueprint $table) {
            $table->dropColumn('math_grade');
            $table->dropColumn('bank_amount');
            $table->dropColumn('est_name');
            $table->dropColumn('health_rep');
            $table->dropColumn('criminal_record_rep');
        });
    }
}
