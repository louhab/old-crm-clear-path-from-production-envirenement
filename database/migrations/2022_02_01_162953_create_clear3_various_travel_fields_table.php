<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClear3VariousTravelFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clear3_various_travel_fields', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedSmallInteger('order');
            // Voyage divers
            $table->string('various_travel_country')->nullable();
            $table->string('various_travel_reason')->nullable();
            $table->date('various_travel_date_from')->nullable();
            $table->date('various_travel_date_to')->nullable();
            $table->timestamps();
            //
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clear3_various_travel_fields');
    }
}
