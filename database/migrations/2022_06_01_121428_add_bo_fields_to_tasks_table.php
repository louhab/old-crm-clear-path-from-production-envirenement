<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBoFieldsToTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->string('bo_view')->nullable()->after('view');
            $table->timestamp('co_updated_at')->nullable()->after('updated_at');
            $table->timestamp('bo_updated_at')->nullable()->after('co_updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropColumn(['bo_view', 'co_updated_at', 'co_updated_at']);
        });
    }
}
