<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCities2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();

            $table->string("city", 100)->nullable();
            $table->string("lat", 100)->nullable();
            $table->string("lng", 100)->nullable();
            $table->string("country", 100)->nullable();
            $table->string("iso2", 100)->nullable();
            $table->string("admin_name", 100)->nullable();
            $table->string("capital", 100)->nullable();
            $table->string("population", 100)->nullable();
            $table->string("population_proper", 100)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
