<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerScoreEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_score_evaluations', function (Blueprint $table) {

            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('customer_case_id');
            $table->unsignedBigInteger('ranking_id')->nullable();
            $table->unsignedInteger('score')->nullable();
            $table->timestamps();
            $table->softDeletes();

            //FKs
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('customer_case_id')->references('id')->on('customer_cases');
            $table->foreign('ranking_id')->references('id')->on('rankings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_score_evaluations');
    }
}
