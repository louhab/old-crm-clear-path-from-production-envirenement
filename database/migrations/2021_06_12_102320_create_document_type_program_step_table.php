<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentTypeProgramStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_type_program_step', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('program_step_id');
            $table->unsignedBigInteger('document_type_id');
            $table->unsignedBigInteger('program_id');
            $table->unsignedSmallInteger('order')->nullable();
            $table->timestamps();
            $table->unique(['program_step_id', 'document_type_id', 'program_id'], 'step_document_program');

            //FKs
            $table->foreign('program_step_id')->references('id')->on('program_steps');
            $table->foreign('document_type_id')->references('id')->on('document_types');
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_step_document_type');
    }
}
