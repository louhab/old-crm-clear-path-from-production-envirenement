<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaseStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_states', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('customer_case_id');
            $table->unsignedBigInteger('sub_step_state_id');
            $table->boolean('status')->default(true);
            $table->unsignedBigInteger('payment_method')->nullable();
            $table->timestamps();
            $table->unique(['customer_case_id', 'sub_step_state_id'], 'customer_case_sub_step_state');

            //FKs
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('payment_method')->references('id')->on('payment_methods');
            $table->foreign('customer_case_id')->references('id')->on('customer_cases')->onDelete('cascade');
            $table->foreign('sub_step_state_id')->references('id')->on('sub_step_states')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case_states');
    }
}
