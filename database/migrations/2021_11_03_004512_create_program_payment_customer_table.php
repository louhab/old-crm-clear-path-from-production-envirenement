<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramPaymentCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_payment_customer', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('program_payment_id');
            $table->unsignedBigInteger('customer_id');
            $table->integer('discount_percent')->unsigned()->default(0);
            $table->float('amount')->unsigned();
            $table->dateTime('performed_at')->nullable();
            $table->timestamps();
            $table->unique(['program_payment_id', 'customer_id'], 'program_payment_customer_unique');

            //FKs
            $table->foreign('program_payment_id')->references('id')->on('program_payments');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_payment_customer');
    }
}
