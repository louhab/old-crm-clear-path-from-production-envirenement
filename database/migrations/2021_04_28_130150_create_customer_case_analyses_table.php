<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerCaseAnalysesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_case_analyses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_case_id');
            $table->timestamps();
            //FKs
            $table->foreign('customer_case_id')->references('id')->on('customer_cases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_case_analyses');
    }
}
