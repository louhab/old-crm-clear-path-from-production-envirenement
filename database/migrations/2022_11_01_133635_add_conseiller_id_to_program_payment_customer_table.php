<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddConseillerIdToProgramPaymentCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_payment_customer', function (Blueprint $table) {
            $table->unsignedBigInteger('conseiller_id')->nullable()->after('customer_id');

            $table->foreign('conseiller_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program_payment_customer', function (Blueprint $table) {
            $table->dropColumn("conseiller_id");
        });
    }
}
