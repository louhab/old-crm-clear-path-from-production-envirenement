<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbacksHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks_history', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->unsignedBigInteger('lead_id')->nullable();
            $table->unsignedBigInteger('feedback_id')->nullable();
            $table->boolean('submitted')->default(false);

            // FK
            $table->foreign('lead_id')->references('id')->on('leads');
            $table->foreign('feedback_id')->references('id')->on('feedbacks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedbacks_history');
    }
}
