<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerCaseStepStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_case_step_state', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_case_id');
            $table->unsignedBigInteger('step_state_id');
            $table->unsignedBigInteger('program_step_id');
            $table->timestamps();
            $table->unique(['customer_case_id', 'program_step_id'], 'customer_case_program_step_unique');

            //FKs
            $table->foreign('customer_case_id')->references('id')->on('customer_cases');
            $table->foreign('step_state_id')->references('id')->on('step_states');
            $table->foreign('program_step_id')->references('id')->on('program_steps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_case_step_state');
    }
}
