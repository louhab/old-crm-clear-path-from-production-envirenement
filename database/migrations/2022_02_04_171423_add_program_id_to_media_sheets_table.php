<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProgramIdToMediaSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media_sheets', function (Blueprint $table) {
            $table->unsignedBigInteger('program_id')->nullable()->after('name_fr');
            //
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media_sheets', function (Blueprint $table) {
            $table->dropColumn('program_id');
        });
    }
}
