<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGroupToDocumentTypeMediaSheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_type_media_sheet', function (Blueprint $table) {
            $table->unsignedBigInteger('document_type_group_id')->nullable()->after('program_id');
            $table->foreign('document_type_group_id')->references('id')->on('document_type_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_type_media_sheet', function (Blueprint $table) {
            $table->dropColumn('document_type_group_id');
        });
    }
}
