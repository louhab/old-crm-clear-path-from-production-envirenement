<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerCaseCaseCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_case_case_comment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_case_id');
            $table->unsignedBigInteger('case_comment_id');
            $table->timestamps();
            $table->unique(['customer_case_id', 'case_comment_id'], 'customer_case_case_comment_unique');

            //FKs
            $table->foreign('customer_case_id')->references('id')->on('customer_cases');
            $table->foreign('case_comment_id')->references('id')->on('case_comments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_case_case_comment');
    }
}
