<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProgramIdsToCanadaCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('canada_cities', function (Blueprint $table) {
            $table->json('program_ids')->nullable()->after("label");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('canada_cities', function (Blueprint $table) {
            $table->dropColumn('program_ids');
        });
    }
}
