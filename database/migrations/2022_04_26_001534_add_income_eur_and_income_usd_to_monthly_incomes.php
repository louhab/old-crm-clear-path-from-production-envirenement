<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIncomeEurAndIncomeUsdToMonthlyIncomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monthly_incomes', function (Blueprint $table) {
            $table->string('income_eur')->nullable()->default(null)->after('income');
            $table->string('income_usd')->nullable()->default(null)->after('income_eur');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('monthly_incomes', function (Blueprint $table) {
            $table->dropColumn(['income_eur', 'income_usd']);
        });
    }
}
