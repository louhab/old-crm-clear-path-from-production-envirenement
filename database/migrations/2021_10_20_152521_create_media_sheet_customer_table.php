<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaSheetCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_sheet_customer', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('media_sheet_id');
            $table->unsignedBigInteger('customer_id');
            $table->boolean('can_edit')->default(false);
            $table->dateTime('customer_updated')->nullable();
            $table->dateTime('user_updated')->nullable();
            $table->timestamps();
            $table->unique(['media_sheet_id', 'customer_id'], 'media_sheet_customer_unique');

            //FKs
            $table->foreign('media_sheet_id')->references('id')->on('media_sheets');
            $table->foreign('customer_id')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_sheet_customer');
    }
}
