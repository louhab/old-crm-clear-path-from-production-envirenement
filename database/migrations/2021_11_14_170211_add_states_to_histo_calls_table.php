<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatesToHistoCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('histo_calls', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('lead_state_id')->nullable()->after('call_descr');
            $table->foreign('lead_state_id')->references('id')->on('lead_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('histo_calls', function (Blueprint $table) {
            //
            $table->dropColumn('lead_state_id');
        });
    }
}
