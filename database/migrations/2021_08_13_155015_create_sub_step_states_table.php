<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubStepStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_step_states', function (Blueprint $table) {
            $table->id();
            $table->string('name_fr');
            $table->string('name_en');
            //$table->boolean('status')->default(true);
            $table->unsignedBigInteger('email_notification_id')->nullable();
            $table->boolean('visible_for_conseiller')->default(true);
            $table->boolean('visible_for_agent')->default(true);
            $table->boolean('visible_for_backoffice')->default(true);
            $table->boolean('visible_for_support')->default(true);
            $table->timestamps();
            $table->softDeletes();

            //FKs
            $table->foreign('email_notification_id')->references('id')->on('email_notifications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_step_states');
    }
}
