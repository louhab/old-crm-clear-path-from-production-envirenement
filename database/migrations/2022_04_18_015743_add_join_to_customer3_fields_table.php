<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJoinToCustomer3FieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_clear3_fields', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);
            // $table->dropUnique('customer_clear3_fields_customer_id_unique');
            $table->string('join')->default('Single')->nullable()->after("customer_id");
            $table->unique(['customer_id', 'join']);
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });

        Schema::table('clear3_work_fields', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);
            $table->dropUnique('clear3_work_fields_customer_id_order_unique');
            $table->string('join')->default('Single')->after("customer_id");
            $table->unique(['customer_id', 'order', 'join']);
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });

        Schema::table('clear3_school_fields', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);
            $table->dropUnique('clear3_school_fields_customer_id_order_unique');
            $table->string('join')->default('Single')->after("customer_id");
            $table->unique(['customer_id', 'order', 'join']);
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });

        Schema::table('clear3_child_fields', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);
            $table->dropUnique('clear3_child_fields_customer_id_foreign');
            $table->string('join')->default('Single')->after("customer_id");
            $table->unique(['customer_id', 'order', 'join']);
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });

        Schema::table('clear3_sibling_fields', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);
            $table->dropUnique('clear3_sibling_fields_customer_id_foreign');
            $table->string('join')->default('Single')->after("customer_id");
            $table->unique(['customer_id', 'order', 'join']);
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });

        Schema::table('clear3_past_residence_fields', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);
            $table->dropUnique('clear3_past_residence_fields_customer_id_foreign');
            $table->string('join')->default('Single')->after("customer_id");
            $table->unique(['customer_id', 'order', 'join']);
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });

        Schema::table('clear3_various_travel_fields', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);
            $table->dropUnique('clear3_various_travel_fields_customer_id_foreign');
            $table->string('join')->default('Single')->after("customer_id");
            $table->unique(['customer_id', 'order', 'join']);
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_clear3_fields', function (Blueprint $table) {
            $table->dropColumn("join");
        });
        Schema::table('clear3_work_fields', function (Blueprint $table) {
            $table->dropColumn("join");
        });
        Schema::table('clear3_school_fields', function (Blueprint $table) {
            $table->dropColumn("join");
        });
        Schema::table('clear3_child_fields', function (Blueprint $table) {
            $table->dropColumn("join");
        });
        Schema::table('clear3_sibling_fields', function (Blueprint $table) {
            $table->dropColumn("join");
        });
        Schema::table('clear3_past_residence_fields', function (Blueprint $table) {
            $table->dropColumn("join");
        });
        Schema::table('clear3_various_travel_fields', function (Blueprint $table) {
            $table->dropColumn("join");
        });
    }
}
