<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCurrencyIdToCustomerClear1000FieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_clear1000_fields', function (Blueprint $table) {
            $table->integer('currency_id')->nullable()->after('visa_request_result');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_clear1000_fields', function (Blueprint $table) {
            $table->dropColumn('currency_id');
        });
    }
}
