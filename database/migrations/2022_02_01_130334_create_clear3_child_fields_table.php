<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClear3ChildFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clear3_child_fields', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedSmallInteger('order');
            $table->string('child_name')->nullable();
            $table->date('child_birthdate')->nullable();
            $table->string('child_country')->nullable();
            $table->string('child_city')->nullable();
            $table->string('child_address')->nullable();
            $table->string('child_family_situation')->nullable();
            $table->string('child_current_occupation')->nullable();
            $table->string('child_email')->nullable();
            $table->timestamps();
            //
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clear3_child_fields');
    }
}
