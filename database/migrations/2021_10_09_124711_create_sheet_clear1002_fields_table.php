<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSheetClear1002FieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sheet_clear1002_fields', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('admission_lang_study');
            $table->unsignedBigInteger('admission_city');
            $table->unsignedBigInteger('admission_school_level');
            $table->unsignedBigInteger('admission_est');
            $table->unsignedBigInteger('admission_est_name');
            $table->unsignedBigInteger('admission_diplomas');
            $table->unsignedBigInteger('admission_program');
            $table->timestamps();

            $table->foreign('admission_lang_study')->references('id')->on('language_of_studies')->onDelete('cascade');
            $table->foreign('admission_city')->references('id')->on('admission_cities')->onDelete('cascade');
            $table->foreign('admission_school_level')->references('id')->on('admission_school_levels')->onDelete('cascade');
            $table->foreign('admission_est')->references('id')->on('admission_establishments')->onDelete('cascade');
            $table->foreign('admission_est_name')->references('id')->on('admission_etab_names')->onDelete('cascade');
            $table->foreign('admission_diplomas')->references('id')->on('admission_diplomas')->onDelete('cascade');
            $table->foreign('admission_program')->references('id')->on('admission_programs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sheet_clear1002_fields');
    }
}
