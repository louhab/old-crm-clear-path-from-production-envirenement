<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateClear3WorkFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clear3_work_fields', function (Blueprint $table) {
            $table->string('work_business_status', 255)->nullable()->after('work_business_city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clear3_work_fields', function (Blueprint $table) {
            $table->dropColumn(['work_business_status']);
        });
    }
}
