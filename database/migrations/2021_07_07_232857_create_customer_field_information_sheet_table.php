<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerFieldInformationSheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_field_information_sheet', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_field_id');
            $table->unsignedBigInteger('information_sheet_id');
            $table->unsignedBigInteger('program_id');
            $table->unsignedSmallInteger('order')->nullable();
            $table->boolean('required')->default(true);
            $table->timestamps();
            $table->unique(['customer_field_id', 'information_sheet_id', 'program_id'], 'customer_field_information_sheet');

            //FKs
            $table->foreign('customer_field_id')->references('id')->on('customer_fields');
            $table->foreign('information_sheet_id')->references('id')->on('information_sheets');
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_field_information_sheet');
    }
}
