<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdmissionToCustomerOrientationSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_orientation_school', function (Blueprint $table) {
            $table->boolean('admis')->nullable()->after('customer_id');
            // $table->boolean('visible')->default(false)->after('label_fr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_orientation_school', function (Blueprint $table) {
            $table->dropColumn('admis');
            // $table->dropColumn('visible');
        });
    }
}
