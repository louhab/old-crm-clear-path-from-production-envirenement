<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramProgramSubStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_program_sub_step', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('program_id');
            $table->unsignedBigInteger('program_sub_step_id');
            $table->unsignedTinyInteger('order');
            $table->timestamps();

            //FKs
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('program_sub_step_id')->references('id')->on('program_sub_steps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_program_sub_step');
    }
}
