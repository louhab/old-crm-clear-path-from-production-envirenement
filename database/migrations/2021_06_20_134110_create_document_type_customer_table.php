<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentTypeCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_type_customer', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('document_type_id');
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('program_step_id');
            $table->boolean('link')->default(false);
            $table->timestamps();
            $table->unique(['document_type_id', 'customer_id', 'program_step_id'], 'document_customer_step');

            //FKs
            $table->foreign('document_type_id')->references('id')->on('document_types');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('program_step_id')->references('id')->on('program_steps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_type_customer');
    }
}
