const mix = require("laravel-mix");
require("dotenv").config();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js")
    .react()
    .js("resources/js/pages/tasks.js", "public/js/pages")
    .js("resources/js/pages/login.js", "public/js/pages")
    .js("resources/js/stopwatch.js", "public/js")
    .js("resources/js/pages/users.js", "public/js/pages")
    .js("resources/js/pages/conseillers.js", "public/js/pages")
    .js("resources/js/pages/histocalls.js", "public/js/pages")
    .js("resources/js/pages/messages.js", "public/js/pages")
    .js("resources/js/pages/emailnotifications.js", "public/js/pages")
    .js("resources/js/pages/substepnotifications.js", "public/js/pages")
    .js("resources/js/pages/countries.js", "public/js/pages")
    .js("resources/js/pages/leads/immigrants.js", "public/js/pages/leads")
    .js("resources/js/pages/leads/students.js", "public/js/pages/leads")
    .js("resources/js/pages/leads/list.js", "public/js/pages/leads")
    .js("resources/js/pages/production/list.js", "public/js/pages/production")
    .js("resources/js/pages/leads/log.js", "public/js/pages/leads")
    .js("resources/js/pages/leads/edit.js", "public/js/pages/leads")
    .js("resources/js/pages/leads/view.js", "public/js/pages/leads")
    .js("resources/js/pages/leads/create.js", "public/js/pages/leads")
    .js("resources/js/pages/programs/list.js", "public/js/pages/programs")
    .js("resources/js/pages/admissions/list.js", "public/js/pages/admissions")
    .js("resources/js/pages/admissions/view.js", "public/js/pages/admissions")
    .js("resources/js/pages/customers/list.js", "public/js/pages/customers")
    .js("resources/js/pages/customers/edit.js", "public/js/pages/customers")
    .js("resources/js/pages/customers/create.js", "public/js/pages/customers")
    .js("resources/js/pages/cases/list.js", "public/js/pages/cases")
    .js("resources/js/pages/cases/edit.js", "public/js/pages/cases")
    .js("resources/js/pages/cases/old_edit.js", "public/js/pages/cases")
    .js("resources/js/pages/cases/sheet.js", "public/js/pages/cases")
    .js("resources/js/pages/cases/sheetMedia.js", "public/js/pages/cases")
    .js("resources/js/pages/phases.js", "public/js/pages")
    .js("resources/js/pages/services.js", "public/js/pages")
    .js("resources/js/pages/campaigns.js", "public/js/pages")
    .js("resources/js/pages/leadsforms.js", "public/js/pages")
    .js("resources/js/pages/payments/discount.js", "public/js/pages/payments")
    .js("resources/js/pages/getintouch.js", "public/js/pages")
    .js("resources/js/pages/schoollevels.js", "public/js/pages")
    .js("resources/js/pages/tags/tags.js", "public/js/pages/tags")
    .js("resources/js/pages/tags/leads.js", "public/js/pages/tags")
    .js("resources/js/pages/fields/list.js", "public/js/pages/fields")
    .js("resources/js/pages/documents/list.js", "public/js/pages/documents")
    .js("resources/js/pages/documents/view.js", "public/js/pages/documents")
    .js("resources/js/pages/documents/view-steps.js", "public/js/pages/documents")
    .js("resources/js/pages/clears/list.js", "public/js/pages/clears")
    .js("resources/js/pages/sheets/list.js", "public/js/pages/sheets")
    .js("resources/js/pages/steps/list.js", "public/js/pages/steps")
    .js("resources/js/pages/steps/view.js", "public/js/pages/steps")
    .js("resources/js/pages/clears/view.js", "public/js/pages/clears")
    .js("resources/js/pages/sheets/view.js", "public/js/pages/sheets")
    .js("resources/js/pages/dashboard.js", "public/js/pages")
    .js(
        "resources/js/pages/dashboard/backoffice-clients.js",
        "public/js/pages/dashboard"
    )
    .js("resources/js/pages/myinbox.js", "public/js/pages")
    .js("resources/js/pages/mycase.js", "public/js/pages")
    .js("resources/js/pages/customer-dashboard.js", "public/js/pages")
    .js("resources/js/pages/billing/bills.js", "public/js/pages")
    .js("resources/js/pages/finances.js", "public/js/pages")
    .js("resources/js/pages/billing/quotes.js", "public/js/pages")
    .js("resources/js/pages/billing/customer-bills.js", "public/js/pages")
    .js("resources/js/pages/billing/customer-quotes.js", "public/js/pages")
    .js("resources/js/pages/calendar.js", "public/js/pages")
    .js("resources/js/pages/intl-tel.js", "public/js/pages")
    .js("resources/js/pages/dropzone.js", "public/js/pages")
    .js("resources/js/pages/wizard.js", "public/js/pages")

    .js("resources/js/oneui/app.js", "public/js/oneui.app.js")

    .js("resources/js/sw.js", "public")
    .js("resources/js/enable-push.js", "public/js")
    .js("resources/js/customer-enable-push.js", "public/js")
    .js("resources/js/utils.js", "public/js")

    .sass("resources/sass/main.scss", "public/css/oneui.css")
    .sass("resources/sass/oneui/themes/amethyst.scss", "public/css/themes/")
    .sass("resources/sass/oneui/themes/city.scss", "public/css/themes/")
    .sass("resources/sass/oneui/themes/flat.scss", "public/css/themes/")
    .sass("resources/sass/oneui/themes/modern.scss", "public/css/themes/")
    .sass("resources/sass/oneui/themes/smooth.scss", "public/css/themes/")

    /* translator plugin*/
    .copy(
        "resources/json/jquery.dataTables.fr.l10n.json",
        "public/json/jquery.dataTables.fr.l10n.json"
    )
    .copy("resources/css/app.css", "public/css/app.css")
    .copy("resources/css/stopwatch.css", "public/css/stopwatch.css")
    .copy("resources/css/component.css", "public/css/component.css")

    /* Tools */
    /*.browserSync('clearpath.ca')*/
    .disableNotifications()

    /** for jQueryUI */
    // .js('resources/js/app.js', 'public/js')
    // .sass("resources/sass/app.scss", "public/css")

    /* Options */
    .options({
        processCssUrls: false,
    })
    .webpackConfig({
        devtool: "inline-source-map",
    })
    .sourceMaps(false);
