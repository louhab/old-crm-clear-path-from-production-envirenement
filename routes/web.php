<?php

use App\Http\Controllers\Api\v1\CustomerNotificationController;
use App\Http\Controllers\Api\v1\CustomerPushSubscriptionController;
use App\Http\Controllers\Api\v1\NotificationController;
use App\Http\Controllers\Api\v1\PushSubscriptionController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use App\Http\Livewire\Program\View;

$api = "v1";

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    ['middleware' => 'web'],
    function () use ($api) {

        Route::post('lang', 'Web\SelectLanguageController@index')->name('select-language');
        // lang api
        Route::post('/lang-change', 'Api\\' . $api . '\SelectLanguageController@index');

        //GET IN TOUCH (LEAD FORM PUBLIC)
        Route::get('/getintouch/{token}', 'Web\GetInTouchController@index')->name('getintouch');
        Route::post('/getintouch', 'Api\\' . $api . '\GetInTouchController@store')->name('getintouch-store');

        // Feedbacks
        Route::get('/feedback', 'Web\FeedbacksController@index')->name('feedback');
        Route::post('/feedback', 'Api\\' . $api . '\FeedbacksController@store')->name('feedback-store');

        Route::post('/affectation-support-lead', 'Web\AffectationSupportLeadController@update')->name('affectation-support-lead');

        //SIGNEASY REDIRECT
        Route::get('/signeasy/redirect/{customer}', 'Web\DocumentSignatureController@showPage')->name('customer-signeasy-redirect-page');
        Route::post('/pandadoc', 'Web\DocumentSignatureController@toggleSigned');
        // Route::get('/signeasy/getsigningurl/{customer}/{collectionName}', 'Api\v1\DocumentSignatureController@getSigningUrl')->name('conseiller-signeasy-get-signing-url');
        Route::get('/signeasyshow/', 'Web\DocumentSignatureController@index')->name('customer-signeasy-show-iframe');
        //Media
        Route::get('/medias/{media}', 'Web\DownloadMediaController@show')->name('get-media');

        Route::get('/testClear', 'Api\\' . $api . '\CustomerController@testClearPDF');
        // lead formulaire redirect page
        Route::get('/welcome', 'Web\GetInTouchController@welcome');
        //Route::get('/', 'Web\GetInTouchController@testClearPDF');

        Route::get('/getClear11', 'Api\\' . $api . '\MediaController@getClear11')->name('clear11-download');

        /**
         * Auth routes
         */
        Route::group(
            ['namespace' => 'Auth'],
            function () {
                // Authentication Routes...
                Route::get('login', 'LoginController@showLoginForm')->name('login');
                Route::post('login', 'LoginController@login');
                Route::post('logout', 'LoginController@logout')->name('logout');
                //Customers login
                Route::get('/login/customer', 'LoginController@showCustomerLoginForm')->name('customerLogin');
                Route::post('/login/customer', 'LoginController@customerLogin');
                //customers password reset routes..
                Route::get('/changePassword', 'ResetPasswordController@showChangePasswordForm');
                Route::post('/changePassword', 'ResetPasswordController@changePassword')->name('changePassword');
            }
        );

        Route::group(
            ['middleware' => 'auth'],
            function () use ($api) {

                /**
                 * Web routes CheckBanned
                 */
                Route::get('/media/{media}', 'Api\\' . $api . '\PDFController@view')->name('view-media');
                Route::get('/customer/getMedia/{customer}/{collection}', 'Api\\' . $api . '\PDFController@getCustomerMedia');
                Route::group(['middleware' => ['access:admin,manager,backoffice,super_backoffice']], function () {
                    Route::get('/soumissions', 'Web\AdmissionController@index')->name('soumissions-index');
                    Route::get('/soumission/{customer_case}', 'Web\AdmissionController@view')->name('soumission-view');
                    Route::get('/soumission_/{customer_case}', 'Web\AdmissionController@view_');
                    Route::put('/soumission/status/{customer_case}', 'Api\v1\AdmissionController@updateState')->name('soumission-update-state');
                });
                Route::group(['middleware' => ['access:admin,manager,conseiller,agent,support']], function () {
                    //Home
                    Route::get('/', 'Api\v1\HomeController@index')->name('home');

                    //Leads
                    Route::get('/leads/create', 'Web\LeadController@create')->name('lead-create');
                    Route::get('/leads', 'Web\LeadController@index')->name('leads-index');
                    Route::get('/leads/{lead}', 'Web\LeadController@edit')->name('lead-edit');
                    Route::get('/lead/{lead}', 'Web\LeadController@view')->name('lead-view');
                });
                Route::group(['middleware' => ['access:admin,manager,conseiller,backoffice,super_backoffice']], function () {
                    // Information Sheet
                    Route::get('/sheets/{customer_case}/{information_sheet}/{join?}', 'Web\InformationSheetController@edit')->name('edit-information-sheet');
                    // Media
                    Route::get('/sheet/media/{customer_case}/{media_sheet}', 'Web\MediaSheetController@edit')->name('edit-media-sheet');
                    // Route::get('/medias/{media}', 'Web\DownloadMediaController@show')->name('get-media');
                });

                Route::group(['middleware' => ['access:admin,manager,conseiller,agent,backoffice']], function () {
                    //Cases
                    Route::get('/cases', 'Web\CaseController@index')->name('cases-list');
                    Route::get('/cases/{customer_case}', 'Web\CaseController@edit')->name('case-edit');
                    Route::get('/cases_/{customer_case}', 'Web\CaseController@edit_');
                });

                // R1 page
                // Route::get('/first-meeting/{customer_case}', 'Web\LeadController@firstMeetPresentation');
                Route::get('/first-meeting/{customerCase}', 'Web\LeadController@firstMeetPresentation');

                Route::group(['middleware' => ['access:admin,backoffice']], function () {
                    //Tasks
                    Route::get('/tasks', 'Web\TasksController@index')->name('tasks-list');
                });

                Route::group(['middleware' => ['access:admin,manager,conseiller,agent,backoffice,support']], function () {

                    //Calendar
                    Route::get('/calendar', 'Web\CalendarController@index')->name('calendar');
                    Route::post('/calendar/create', 'Web\CalendarController@create')->name('calendar.create');
                    Route::post('/calendar/update', 'Web\CalendarController@update')->name('calendar.update');
                    Route::post('/calendar/destroy/{calendar}', 'Web\CalendarController@destroy')->name('calendar.destroy');
                    Route::get('/calendar/getCustomer/{calendrier_id}', 'Web\CalendarController@getCustomer')->name('calender.customer');
                });

                Route::group(['middleware' => ['access:admin,manager,conseiller,agent,backoffice,support,super_backoffice']], function () {
                    Route::get('/profileUser', 'Web\CaseController@updateUserProfile')->name('user-profile');
                    Route::get('/sav', 'Web\UserController@sav')->name('sav');
                });

                Route::group(['middleware' => ['access:admin,manager,conseiller,agent,support,financier']], function () {
                    //DashBoard
                    Route::get('dashboard', 'Web\DashBoardController@index')->name('dashboard');
                    Route::get('dashboard/clients', 'Web\DashBoardController@clientsIndex')->name('dashboard-clients');
                    Route::group(['middleware' => ['access:admin,manager,conseiller']], function () {
                        Route::get('/pdf/bill/{bill}', 'Web\PDFController@generateBillPDF');
                        Route::get('/pdf/quote/{quote}', 'Web\PDFController@generateQuotePDF')->name('quote-download');
                        Route::get('/orientation', 'Web\ProgramCourseController@index')->name("orientation-home");
                        // viewQuotePDF
                        Route::get('/pdf/viewQuote/{quote}', 'Web\PDFController@viewQuotePDF')->name('quote-view');

                        // //Media
                        // Route::get('/medias/{media}', 'Web\DownloadMediaController@show')->name('get-media');

                        //Send Mail
                        Route::get('/sendMail', 'Web\EmailController@send')->name('send-mails');
                        // //Information Sheet
                        //HistoCalls
                        Route::get('/histocalls', 'Web\HistoCallController@index')->name('histocalls-list');
                        // Route::get('/sheets/{customer_case}/{information_sheet}', 'Web\InformationSheetController@edit')->name('edit-information-sheet');
                    });
                });

                Route::group(['middleware' => ['access:admin']], function () {
                    Route::get('/leadslog', 'Web\LeadController@log')->name("leads-log");
                    Route::get('/updateOrientation', 'Web\ProgramCourseController@updateOrientationDB');
                    Route::get('/orientation/{program}', 'Web\ProgramCourseController@view')->name("orientation-view");

                    Route::get('/production', 'Web\ProductionController@index')->name("production-list");
                    // consieller id
                    Route::get('/production/{user}', 'Web\ProductionController@view')->name("production-view");
                    // customer fields
                    Route::get('/fields', 'Web\CustomerFieldController@index')->name('field-list');
                    // document types
                    Route::get('/document/{document}', 'Web\DocumentTypeController@view')->name('doc-view');
                    Route::get('/documents', 'Web\DocumentTypeController@index')->name('documents-list');
                    // steps
                    Route::get('/steps', 'Web\SubStepController@index')->name('steps-list');
                    Route::get('/steps/{step}', 'Web\SubStepController@view')->name('step-view');
                    // clears
                    Route::get('/clear/{sheet}', 'Web\ClearController@view')->name('document-view');
                    Route::get('/clears', 'Web\ClearController@index')->name('clears-list');
                    // sheets
                    Route::get('/sheets/{sheet}', 'Web\SheetController@view')->name('sheet-view');
                    Route::get('/sheets', 'Web\SheetController@index')->name('sheet-list');
                });

                Route::group(['middleware' => ['access:admin,financier']], function () {
                    //Finances
                    Route::get('finances', 'Web\FinanceController@index')->name('finances');
                    //QUOTES
                    Route::get('quotes', 'Web\QuoteController@index')->name('quotes');
                    //BILLS
                    Route::get('bills', 'Web\BillController@index')->name('bills');
                });

                Route::group(['middleware' => ['access:admin,manager']], function () {

                    //Users
                    Route::get('/users', 'Web\UserController@index')->name('users-list');
                    Route::get('/conseillers', 'Web\ConseillerController@index')->name('conseillers-list');
                    Route::get('/rotation', 'Web\UserAssignController@index')->name('rotation-config');
                    Route::post('/rotation/update', 'Web\UserAssignController@update')->name('rotation-edit');

                    //Forms
                    Route::get('/formulaires', 'Web\LeadFormController@index')->name('forms-list');
                    // programs
                    Route::get('/programs', 'Web\ProgramController@index')->name('programs-list');
                    Route::get('/program/{program}', [View::class]);

                    // CSV Import
                    Route::get('import-leads', 'Web\LeadController@importCSV')->name('import-leads');
                    Route::post('integrate-csv', 'Web\LeadController@integrateLeads')->name('integrate-leads');

                    // payment-discount
                    Route::get('/payments', 'Web\ProgramPaymentController@index')->name('payment-discount');

                    //Messages
                    Route::get('/messages', 'Web\CustomerMessageController@index')->name('messages-list');

                    //EmailNotification
                    Route::get('/emailnotifications', 'Web\EmailNotificationController@index')->name('emailnotifications-list');

                    //EmailNotification
                    Route::get('/substepnotifications', 'Web\SubStepStateController@index')->name('substepnotifications-list');

                    //Countries
                    Route::get('/countries', 'Web\CountryController@index')->name('countries-list');

                    //Services
                    Route::get('/services', 'Web\ServiceController@index')->name('services-list');

                    //Phase
                    Route::get('/phases', 'Web\PhaseController@index')->name('phases-list');

                    //Campaings
                    Route::get('/campaigns', 'Web\CampaignController@index')->name('campaigns-list');

                    //schoolLevels
                    Route::get('/schoollevels', 'Web\SchoolLevelController@index')->name('schoollevels-list');

                    //tags
                    Route::get('/tags', 'Web\TagController@index')->name('tags-list');
                    Route::get('/tags/{id}', 'Web\TagController@leads')->name('tag-leads');
                });

                //Immigrants
                Route::get('/immigrants', 'Web\LeadController@immgIndex')->name('immigrants-list');

                //Customers
                Route::get('/customers', 'Web\CustomerController@index')->name('customers-index');
                Route::get('/customers/create', 'Web\CustomerController@create')->name('customer-create');
                Route::get('/customers/{customer}', 'Web\CustomerController@edit')->name('customer-edit');

                //Students
                Route::get('/students', 'Web\LeadController@studIndex')->name('students-list');

                // Route::get('/sheets/{customer_case}/{information_sheet}', 'Web\InformationSheetController@edit')->name('edit-information-sheet');
                // Media
                // Route::get('/sheets/media/{customer_case}/{media_sheet}', 'Web\MediaSheetController@edit')->name('edit-media-sheet');

                /**
                 * Api routes
                 */

                Route::prefix('api/' . $api)->group(function () use ($api) {

                    Route::group(['middleware' => ['access:admin']], function () use ($api) {
                        Route::delete('/users/{user}', 'Api\\' . $api . '\UserController@destroy');
                        Route::delete('/histocalls/{histocall}', 'Api\\' . $api . '\HistoCallController@destroy');
                        Route::delete('/messages/{message}', 'Api\\' . $api . '\CustomerMessageController@destroy');
                        Route::delete('/emailnotifications/{emailnotification}', 'Api\\' . $api . '\EmailNotificationController@destroy');
                        Route::delete('/countries/{country}', 'Api\\' . $api . '\CountryController@destroy');
                        Route::delete('/services/{service}', 'Api\\' . $api . '\ServiceController@destroy');
                        Route::delete('/phases/{phase}', 'Api\\' . $api . '\PhaseController@destroy');
                        Route::delete('/campaigns/{campaign}', 'Api\\' . $api . '\CampaignController@destroy');
                        Route::delete('/bills/{bill}', 'Api\\' . $api . '\BillController@destroy');
                        Route::delete('/quotes/{quote}', 'Api\\' . $api . '\QuoteController@destroy');
                        Route::delete('/schoollevels/{schollevel}', 'Api\\' . $api . '\SchoolLevelController@destroy');
                        Route::delete('/leadsforms/{leadform}', 'Api\\' . $api . '\LeadFormController@destroy');
                        //Route::delete('/leads/{lead}', 'Api\\' . $api . '\LeadController@destroy');
                        Route::delete('/case/{customer_case}/{status}', 'Api\\' . $api . '\CaseController@destroy');
                        // delete payment
                        Route::put('/payments/{case_state}', 'Api\\' . $api . '\ProgramPaymentController@destroy');
                        // program-course
                        Route::post('/program-course/{field_name}', 'Api\\' . $api . '\ProgramCourseController@addValue');
                        Route::put('/program-course/{program}', 'Api\\' . $api . '\ProgramCourseController@update');
                        Route::post('/program-course', 'Api\\' . $api . '\ProgramCourseController@store');
                        Route::get('/program-course/{program}', 'Api\\' . $api . '\ProgramCourseController@getProgram');
                        // Log Activity
                        Route::get('/logs', 'Api\\' . $api . '\LeadController@log')->name('logs');
                    });

                    // deprecated tasks crud
                    Route::group(['middleware' => ['access:admin,backoffice,conseiller']], function () use ($api) {
                        //Tasks
                        Route::post('/tasks/{customer_case}', 'Api\\' . $api . '\TasksController@store')->name('store-task');
                        Route::put('/tasks/{id}', 'Api\\' . $api . '\TasksController@update');
                    });

                    Route::group(['middleware' => ['access:admin,manager,conseiller,agent,support']], function () use ($api) {
                        //Dashboard
                        Route::get('/analytics', 'Api\\' . $api . '\DashBoardController@index')->name('analytics');
                        Route::get('/analytics/clients', 'Api\\' . $api . '\DashBoardController@clientsIndex')->name('analytics-clients');
                        Route::post('/agents', 'Api\\' . $api . '\DashBoardController@getAgents');
                        Route::post('/getPrograms', 'Api\\' . $api . '\DashBoardController@getPrograms');
                        Route::post('/getPhases', 'Api\\' . $api . '\DashBoardController@getPhases');
                        Route::post('/getSteps', 'Api\\' . $api . '\DashBoardController@getSteps');
                        Route::post('/getCampaigns', 'Api\\' . $api . '\DashBoardController@getCampaigns');
                        Route::post('/getLeadStatus', 'Api\\' . $api . '\DashBoardController@getLeadStatus');
                        // @added to timer
                        Route::get('/customerdata/{id}', 'Api\\' . $api . '\CustomerController@getCustomerData');
                        Route::get('/customer-meet/{id}/{meet}', 'Api\\' . $api . '\CustomerController@setMeetIsDone'); // should be post
                        Route::get('/customer-orientation/{id}/{orientationId}', 'Api\\' . $api . '\CustomerController@setOrientation'); // should be post
                        Route::get('/leadsdata/{id}', 'Api\\' . $api . '\CustomerController@getLeadData');
                    });

                    Route::group(['middleware' => ['access:admin,manager,conseiller,support']], function () use ($api) {
                        //clear1002
                        Route::get('/clear1002', 'Api\\' . $api . '\MediaSheetController@index');
                        // Get lead call count
                        Route::get('/histocalls/lead/{id}', 'Api\\' . $api . '\HistoCallController@show');
                    });

                    Route::group(['middleware' => ['access:admin,manager,conseiller']], function () use ($api) {
                        //HistoCalls api
                        Route::get('/histocalls', 'Api\\' . $api . '\HistoCallController@index')->name('histocalls');
                        Route::put('/histocalls/{id}', 'Api\\' . $api . '\HistoCallController@update');
                        Route::post('/histocalls', 'Api\\' . $api . '\HistoCallController@store');
                        //
                        Route::delete('/customer/media/{media}', 'Api\\' . $api . '\PDFController@destroy')->name('remove-media');
                        //
                        Route::post('/savePhone/{id}', 'Api\\' . $api . '\LeadController@savePhone');
                        // switch lead to client (new process R")
                        Route::put('/leads/switch/{lead}', 'Api\\' . $api . '\LeadController@switch');
                    });

                    Route::group(['middleware' => ['access:conseiller,support']], function () use ($api) {
                        Route::put('/leads/assign/{lead}', 'Api\\' . $api . '\LeadController@assign');
                    });

                    Route::group(['middleware' => ['access:admin,financier']], function () use ($api) {
                        //Bills api
                        Route::post('/bills', 'Api\\' . $api . '\BillController@store');
                        Route::put('/bills/{bill}', 'Api\\' . $api . '\BillController@update');
                        Route::get('/bills', 'Api\\' . $api . '\BillController@index')/*->name('bills')*/;

                        //Finances api
                        Route::get('/finances', 'Api\\' . $api . '\FinanceController@index');
                        Route::get('/finances-stats', 'Api\\' . $api . '\FinanceController@stats');
                        Route::put('/finances/{payment_id}', 'Api\\' . $api . '\FinanceController@verify');

                        //Quotes api
                        Route::post('/quotes', 'Api\\' . $api . '\QuoteController@store');
                        Route::put('/quotes/{quote}', 'Api\\' . $api . '\QuoteController@update');
                        Route::get('/quotes', 'Api\\' . $api . '\QuoteController@index');
                    });

                    Route::group(['middleware' => ['access:admin,manager']], function () use ($api) {

                        Route::delete('/leads/{lead}', 'Api\\' . $api . '\LeadController@destroy');
                        // leads/convert
                        Route::put('/leads/convert/{lead}', 'Api\\' . $api . '\LeadController@makeClient');
                        Route::post('/leads/generate/{lead}', 'Api\\' . $api . '\LeadController@generateDocs')->name("regen-docs");
                        // Case/convert
                        // soumission/convert
                        Route::put('/cases/convert/{customer}', 'Api\\' . $api . '\CaseController@convert_to_soumission');
                        Route::put('/soumission/convert/{customer}', 'Api\\' . $api . '\AdmissionController@convert_soumission');

                        // programs
                        // Route::get('/programs', 'Web\ProgramController@index')->name('programs-list');
                        Route::get('/programs', 'Api\\' . $api . '\ProgramController@index')->name('programs');
                        //Users api
                        Route::get('/users', 'Api\\' . $api . '\UserController@index')->name('users');
                        Route::put('/users/{id}', 'Api\\' . $api . '\UserController@update');
                        Route::post('/users', 'Api\\' . $api . '\UserController@store');
                        Route::get('/conseillers', 'Api\\' . $api . '\ConseillerController@index')->name('conseillers');
                        Route::post('/conseillers', 'Api\\' . $api . '\ConseillerController@getConseillers');
                        Route::get('/conseiller/{id}', 'Api\\' . $api . '\ConseillerController@attach');
                        Route::put('/conseillers/{user}', 'Api\\' . $api . '\ConseillerController@update');
                        Route::delete('/conseillers/{user}', 'Api\\' . $api . '\ConseillerController@dettach');

                        //Messages api
                        Route::get('/messages', 'Api\\' . $api . '\CustomerMessageController@index')->name('Messages');
                        Route::put('/messages/{id}', 'Api\\' . $api . '\CustomerMessageController@update');
                        Route::post('/messages', 'Api\\' . $api . '\CustomerMessageController@store');

                        //Email Notifications api
                        Route::get('/emailnotifications', 'Api\\' . $api . '\EmailNotificationController@index')->name('EmailNotifications');
                        Route::put('/emailnotifications/{id}', 'Api\\' . $api . '\EmailNotificationController@update');
                        Route::post('/emailnotifications', 'Api\\' . $api . '\EmailNotificationController@store');

                        //SubStep Notifications api
                        Route::get('/substepnotifications', 'Api\\' . $api . '\SubStepStateController@index')->name('SubStepNotifications');
                        Route::put('/substepnotifications/{id}', 'Api\\' . $api . '\SubStepStateController@update');

                        //Countries api
                        Route::get('/countries', 'Api\\' . $api . '\CountryController@index')->name('countries');
                        Route::put('/countries/{id}', 'Api\\' . $api . '\CountryController@update');
                        Route::post('/countries', 'Api\\' . $api . '\CountryController@store');

                        //Services api
                        Route::get('/services', 'Api\\' . $api . '\ServiceController@index')->name('services');
                        Route::put('/services/{id}', 'Api\\' . $api . '\ServiceController@update');
                        Route::post('/services', 'Api\\' . $api . '\ServiceController@store');
                        Route::post('/getservices', 'Api\\' . $api . '\ServiceController@getServices');

                        //Phases api
                        Route::get('/phases', 'Api\\' . $api . '\PhaseController@index')->name('phases');
                        Route::put('/phases/{id}', 'Api\\' . $api . '\PhaseController@update');
                        Route::post('/phases', 'Api\\' . $api . '\PhaseController@store');

                        //campaigns api
                        Route::get('/campaigns', 'Api\\' . $api . '\CampaignController@index')->name('campaigns');
                        Route::put('/campaigns/{id}', 'Api\\' . $api . '\CampaignController@update');
                        Route::post('/campaigns', 'Api\\' . $api . '\CampaignController@store');

                        //School levels api
                        Route::get('/schoollevels', 'Api\\' . $api . '\SchoolLevelController@index')->name('schoollevel');
                        Route::put('/schoollevels/{id}', 'Api\\' . $api . '\SchoolLevelController@update');
                        Route::post('/schoollevels', 'Api\\' . $api . '\SchoolLevelController@store');

                        //Tags api
                        Route::get('/tags', 'Api\\' . $api . '\TagController@index')->name('tag');
                        Route::put('/tags/{id}', 'Api\\' . $api . '\TagController@update');
                        Route::post('/tags', 'Api\\' . $api . '\TagController@store');
                        Route::delete('/tags/{id}', 'Api\\' . $api . '\TagController@destroy');

                        // steps
                        Route::get('/steps', 'Api\\' . $api . '\SubStepController@index');
                        Route::get('/document-steps/{id}/steps', 'Api\\' . $api . '\SubStepController@view');
                        Route::put('/document-steps/{id}/steps', 'Api\\' . $api . '\SubStepController@updatePivot');
                        Route::post('/document-steps/{step_id}/steps', 'Api\\' . $api . '\SubStepController@storePivot');
                        Route::delete('/document-steps/{document_step}/steps', 'Api\\' . $api . '\SubStepController@destroyPivot');
                        // clears
                        Route::get('/clears', 'Api\\' . $api . '\ClearController@index');
                        // sheets
                        Route::get('/sheets', 'Api\\' . $api . '\SheetController@index');
                        Route::get('/sheet-fields/{id}', 'Api\\' . $api . '\SheetController@view');
                        Route::post('/sheet-fields/{sheet_id}', 'Api\\' . $api . '\SheetController@storePivot');
                        Route::put('/sheet-fields/{id}', 'Api\\' . $api . '\SheetController@updatePivot');
                        Route::delete('/sheet-fields/{field_sheet}', 'Api\\' . $api . '\SheetController@destroyPivot');

                        // document steps
                        Route::get('/document-steps/{id}', 'Api\\' . $api . '\DocumentTypeController@viewSteps');
                        Route::put('/document-steps/{id}', 'Api\\' . $api . '\DocumentTypeController@updateStepPivot');
                        Route::post('/document-steps/{document_id}', 'Api\\' . $api . '\DocumentTypeController@storeStepPivot');
                        Route::delete('/document-steps/{document_step}', 'Api\\' . $api . '\DocumentTypeController@destroyStepPivot');
                        // fields
                        Route::get('/fields', 'Api\\' . $api . '\CustomerFieldController@index');
                        Route::put('/fields/{id}', 'Api\\' . $api . '\CustomerFieldController@update');
                        Route::post('/fields', 'Api\\' . $api . '\CustomerFieldController@store');
                        Route::delete('/fields/{field}', 'Api\\' . $api . '\CustomerFieldController@destroy');
                        // documents
                        Route::get('/document-clears/{id}/{column}', 'Api\\' . $api . '\DocumentTypeController@view');
                        Route::post('/document-clears/{id}/{column?}', 'Api\\' . $api . '\DocumentTypeController@storePivot');
                        Route::put('/document-clears/{id}/{column?}', 'Api\\' . $api . '\DocumentTypeController@updatePivot');
                        Route::delete('/document-clears/{document_clear}', 'Api\\' . $api . '\DocumentTypeController@destroyPivot');
                        // Route::get('/document-clear/{id}', 'Api\\'. $api .'\DocumentTypeController@getPivot');

                        Route::get('/documents', 'Api\\' . $api . '\DocumentTypeController@index');
                        Route::put('/documents/{id}', 'Api\\' . $api . '\DocumentTypeController@update');
                        Route::post('/documents', 'Api\\' . $api . '\DocumentTypeController@store');
                        Route::delete('/documents/{document}', 'Api\\' . $api . '\DocumentTypeController@destroy');
                        //LeadsForms api
                        Route::get('/leadsforms', 'Api\\' . $api . '\LeadFormController@index')->name('leadsforms');
                        Route::put('/leadsforms/{id}', 'Api\\' . $api . '\LeadFormController@update');
                        Route::post('/leadsforms', 'Api\\' . $api . '\LeadFormController@store');
                        Route::post('/leadsforms/setactivestate', 'Api\\' . $api . '\LeadFormController@setActiveState');
                        // affectation backoffice
                        Route::put('/customerBackoffice', 'Api\\' . $api . '\CustomerController@updateCustomerBackoffice');
                    });

                    Route::group(['middleware' => ['access:admin,manager,conseiller,agent,support']], function () use ($api) {
                        //Leads api
                        Route::post('/leads', 'Api\\' . $api . '\LeadController@store')->name('lead-store');
                        Route::post('/leads/media/{lead}/{collectionName}', 'Api\\' . $api . '\LeadController@deleteMedia')->name('lead-delete-media');
                        Route::post('/leads/{lead}', 'Api\\' . $api . '\LeadController@convert')->name('lead-convert');
                        Route::put('/leads/{id}', 'Api\\' . $api . '\LeadController@update')->name('lead-update');
                        Route::put('/leads/{lead}/{status}', 'Api\\' . $api . '\LeadController@updateLeadStatus');
                        Route::put('/leadConseiller', 'Api\\' . $api . '\LeadController@updateLeadConseiller');
                        Route::post('/getLeads', 'Api\\' . $api . '\LeadController@getLeads')->name('getLeads');
                        Route::get('/leads', 'Api\\' . $api . '\LeadController@index')->name('leads');
                        Route::get('/tag/leads/{id}', 'Api\\' . $api . '\LeadController@index')->name('tags-leads');

                        //Coutries api
                        Route::post('/getCountries', 'Api\\' . $api . '\CountryController@getCountries');
                        //Influensers
                        Route::get('/forms/{campaign}', 'Api\\' . $api . '\LeadFormController@getInfluencers');

                        // histocalls
                        Route::post('/histocalls', 'Api\\' . $api . '\HistoCallController@store');
                        Route::get('/first-meeting-slides', 'Api\\' . $api . '\MeetingShowController@index');
                    });



                    Route::group(['middleware' => ['access:admin,manager,conseiller,agent,backoffice,super_backoffice']], function () use ($api) {
                        //Cases api
                        Route::post('/cases', 'Api\\' . $api . '\CaseController@index')->name('cases');
                        Route::put('/cases/{customer_case}', 'Api\\' . $api . '\CaseController@update')->name('case-update');
                        //Route::post('/cases/{customer_case}', 'Api\\' . $api . '\CaseController@convert')->name('case-convert');
                        Route::put('/cases/{customer_case}/{program_step}', 'Api\\' . $api . '\CaseController@updateStepState')->name('step-state-update');
                        // admissions
                        Route::get('/admissions', 'Api\\' . $api . '\AdmissionController@index')->name('admissions');
                        //
                        // production list
                        Route::get('/production', 'Api\\' . $api . '\ProductionController@index')->name('production');
                        // production per advisor
                        // Route::get('/production', 'Api\\' . $api . '\ProductionController@index')->name('admissions');
                    });

                    Route::group(['middleware' => ['access:admin,manager,conseiller,agent,backoffice,support,super_backoffice']], function () use ($api) {
                        Route::put('/profileUpdate', 'Api\\' . $api . '\CaseController@updateProfile')->name('profile-update');
                    });

                    Route::group(['middleware' => ['access:admin,manager,conseiller,agent,backoffice,support']], function () use ($api) {
                        Route::get('/calendar', 'Api\\' . $api . '\CalendarController@index');
                    });

                    /*Route::put('/cases/evaluation/{customer_score_evaluation}', 'Api\\'. $api .'\CustomerScoreEvaluationController@update')->name('case-update-score');*/


                    Route::post('/admission-states', 'Api\\' . $api . '\AdmissionController@states');

                    //SIGNEASY SIGNATURE API
                    Route::post('/signeasy/{customer}/{collectionName}', 'Api\\' . $api . '\DocumentSignatureController@prepareForSigning')->name('signeasy-import-document');
                    Route::get('/signeasy/sendrequest/{customer}/{collectionName}', 'Api\\' . $api . '\DocumentSignatureController@sendForSigning')->name('signeasy-send-signing-request');
                    Route::post('/signeasy/download/{customer}/{collectionName}', 'Api\\' . $api . '\DocumentSignatureController@downloadSignedDocument')->name('signeasy-download-signed-document');
                    // Route::get('/signeasy/getsigningurl/{customer}/{collectionName}', 'Api\\'. $api .'\DocumentSignatureController@getSigningUrl')->name('conseiller-signeasy-get-signing-url');
                    // Route::get('/signeasyshow/', 'Web\DocumentSignatureController@index')->name('customer-signeasy-show-iframe');
                    // Route::post('/signeasy/getsigningurl/{customer}/{collectionName}', 'Api\v1\DocumentSignatureController@getSigningUrl')->name('conseiller-signeasy-get-signing-url');

                    // SETASIGN TEST
                    Route::post('/downloadpdf/{customer}', 'Api\\' . $api . '\SetasignPDFController@download')->name('setasign-test');

                    //Immigrants api
                    Route::get('/immigrants', 'Api\\' . $api . '\LeadController@immgIndex')->name('immigrants');
                    Route::put('/immigrants/{id}', 'Api\\' . $api . '\LeadController@update');
                    Route::post('/immigrants', 'Api\\' . $api . '\LeadController@store');

                    //Customers api
                    Route::get('/customers', 'Api\\' . $api . '\CustomerController@index')->name('customers');
                    Route::put('/customers/{id}', 'Api\\' . $api . '\CustomerController@update')->name('customer-update');
                    Route::put('/customers/clear1000/{id}', 'Api\\' . $api . '\CustomerController@updateClear1000')->name('customer-update-clear1000');
                    Route::put('/customers/clear1002/{id}', 'Api\\' . $api . '\CustomerController@updateClear1002')->name('customer-update-clear1002');
                    Route::put('/customers/clear3/{id}', 'Api\\' . $api . '\CustomerController@updateClear3')->name('customer-update-clear3');
                    Route::put('/customers/clear1004/{id}', 'Api\\' . $api . '\CustomerController@updateClear1004')->name('customer-update-clear1004');
                    Route::put('/customers/account/{id}', 'Api\\' . $api . '\CustomerController@activate')->name('customer-account-activation');
                    Route::put('/customers/account/deactivate/{id}', 'Api\\' . $api . '\CustomerController@deactivate')->name('customer-account-deactivation');
                    Route::post('/customers', 'Api\\' . $api . '\CustomerController@store')->name('customer-store');
                    Route::delete('/customers/media/{customer}/{collectionName}', 'Api\\' . $api . '\CustomerController@deleteMedia')->name('customer-delete-media');
                    Route::post('/customers/media/{customer}/{collectionName}', 'Api\\' . $api . '\CustomerController@addMedia')->name('add-media');
                    // Route::post('/customers/mediatest/{customer}', 'Api\\'. $api .'\CustomerController@addMediaTest')->name('add-media-test');
                    Route::get('/discounts', 'Api\\' . $api . '\ProgramPaymentController@index');
                    Route::post('/getCustomers', 'Api\\' . $api . '\CustomerController@getCustomers')->name('getCustomers');
                    // getClients
                    Route::post('/getClients', 'Api\\' . $api . '\CustomerController@getClients')->name('getClients');
                    // react getCallStates
                    Route::get('/getCallStates/{lead}', 'Api\\' . $api . '\CustomerController@getCallStates')->name('getCallStates');
                    Route::post('/getPayments', 'Api\\' . $api . '\ProgramPaymentController@getPayments');
                    Route::post('/getAmount', 'Api\\' . $api . '\ProgramPaymentController@getAmount');
                    // discount
                    Route::post('/discount', 'Api\\' . $api . '\ProgramPaymentController@store');
                    Route::get('/payments/{case_state}/{method}', 'Api\\' . $api . '\ProgramPaymentController@update');
                    Route::post('/addrequesteddocument/{customer}/{document_type}/{program_step}', 'Api\\' . $api . '\CustomerController@addRequestedDocument')->name('add-requested-document');
                    Route::post('/removerequesteddocument/{customer}/{document_type}/{program_step}', 'Api\\' . $api . '\CustomerController@removeRequestedDocument')->name('remove-requested-document');
                    Route::post('/togglelinkvisibility/{customer}/{document_type}/{program_step}', 'Api\\' . $api . '\CustomerController@toggleLinkVisibility')->name('toggle-link-visibility');
                    Route::post('/togglesheetvisibility/{customer}/{information_sheet}', 'Api\\' . $api . '\CustomerController@toggleSheetVisibility')->name('toggle-sheet-visibility');
                    Route::post('/togglemediasheetvisibility/{customer}/{media_sheet}', 'Api\\' . $api . '\CustomerController@toggleMediaSheetVisibility')->name('toggle-media-sheet-visibility');
                    Route::get('/togglemeet/{customer}', 'Api\\' . $api . '\CustomerController@toggleMeet')->name('toggle-meet');

                    // view.js -- payment method
                    Route::post('/getPaymentMethods', 'Api\\' . $api . '\SubStepStateController@getPaymentMethods');
                    //Comments api
                    //Route::get('/states/{customer_case}', 'Api\\'. $api .'\SubStepStateController@update');
                    Route::put('/states/{case_state}/{method?}', 'Api\\' . $api . '\SubStepStateController@updateStepState')->name('sub-step-state-update');
                    Route::get('/comments/{customer_case}', 'Api\\' . $api . '\CaseCommentController@index')->name('comments');
                    Route::post('/comments/{customer_case}/{canal?}', 'Api\\' . $api . '\CaseCommentController@store')->name('store-comment');
                    Route::post('/comments/edit/{customer_case}/{comment}/{canal?}', 'Api\\' . $api . '\CaseCommentController@update')->name('update-comment');
                    Route::post('/task-fees', 'Api\\' . $api . '\CaseCommentController@updateFees')->name('update-fees');
                    Route::post('/tags/{customer_case}', 'Api\\' . $api . '\LeadController@editTags')->name('store-tags');

                    Route::post('/togglemediaproperty/{media}', 'Api\\' . $api . '\MediaController@toggleProperty')->name('toggle-media-property');

                    // Notifications
                    Route::post('notifications', [NotificationController::class, 'store']);
                    Route::get('/notifications', [NotificationController::class, 'index']);
                    Route::patch('/notifications/{id}/read', [NotificationController::class, 'markAsRead']);
                    Route::get('/notifications/mark-all-read', [NotificationController::class, 'markAllRead']);
                    Route::post('/notifications/{id}/dismiss', [NotificationController::class, 'dismiss']);

                    // Push Subscriptions
                    Route::post('/subscriptions', [PushSubscriptionController::class, 'update']);
                    Route::post('/subscriptions/delete', [PushSubscriptionController::class, 'destroy']);
                });
            }
        );

        Route::group(['middleware' => ['auth:customer', 'access:customer', 'password.reset']], function () use ($api) {
            /**
             * Web routes
             */

            Route::get('/pdf/enableQuote', 'Api\\' . $api . '\DownloadMediaController@enableQuote')->name('enable-quote');

            //SIGNEASY
            // Route::get('/signeasyshow/', 'Web\DocumentSignatureController@index')->name('customer-signeasy-show-iframe');

            //INBOX
            Route::get('myinbox', 'Web\CustomerMessageController@customerIndex')->name('customer-inbox');

            //MY CASE
            Route::get('mycase/{program_step}', 'Web\CaseController@index')->name('customer-mycase');
            Route::get('mycase', 'Web\CaseController@indexCustomer')->name('customer-case');

            //MY QUOTES
            Route::get('myquotes', 'Web\QuoteController@index')->name('customer-myquotes');

            //MY BILLS
            Route::get('mybills', 'Web\BillController@index')->name('customer-mybills');

            //pdfs
            Route::get('/pdf/mybill/{bill}', 'Web\PDFController@generateBillPDF');
            Route::get('/pdf/myquote/{quote}', 'Web\PDFController@generateQuotePDF');


            // Media
            Route::get('/customer/sheets/media/{customer_case}/{media_sheet}', 'Web\MediaSheetController@edit')->name('customer-edit-media-sheet');

            // Information Sheet
            Route::get('/customer/sheets/{customer_case}/{information_sheet}/{join?}', 'Web\InformationSheetController@edit')->where('customer_case', '[0-9]+')->name('customer-edit-information-sheet');
            // Route::get('/sheets/{customer_case}/{information_sheet}', 'Web\InformationSheetController@edit')->name('edit-information-sheet');

            // profile
            Route::get('/profile', 'Web\CaseController@updateProfile')->name('customer-profile');
            //
            Route::delete('/customer/media/{media}', 'Api\\' . $api . '\PDFController@destroy')->name('customer-remove-media');
            Route::get('/customer/media/{media}', 'Api\\' . $api . '\PDFController@view')->name('customer-view-media');

            /**
             * Api routes
             */

            Route::prefix('customer/api/' . $api)->group(function () use ($api) {

                Route::post('/signeasy/getsigningurl/{customer}/{collectionName}', 'Api\\' . $api . '\DocumentSignatureController@getSigningUrl')->name('customer-signeasy-get-signing-url');
                Route::post('/signeasy/download/{customer}/{collectionName}', 'Api\\' . $api . '\DocumentSignatureController@downloadSignedDocument')->name('customer-signeasy-download-signed-document');

                Route::post('/customers/media/{customer}/{collectionName}', 'Api\\' . $api . '\CustomerController@addMedia')->name('customer-add-media');

                // Notifications
                Route::post('notifications', [CustomerNotificationController::class, 'store']);
                Route::get('/notifications', [CustomerNotificationController::class, 'index']);
                Route::patch('/notifications/{id}/read', [CustomerNotificationController::class, 'markAsRead']);
                Route::get('/notifications/mark-all-read', [CustomerNotificationController::class, 'markAllRead']);
                Route::post('/notifications/{id}/dismiss', [CustomerNotificationController::class, 'dismiss']);

                // Push Subscriptions
                Route::post('/subscriptions', [CustomerPushSubscriptionController::class, 'update']);
                Route::post('/subscriptions/delete', [CustomerPushSubscriptionController::class, 'destroy']);

                Route::put('/customers/{id}', 'Api\\' . $api . '\CustomerController@update')->name('customer-customer-update');
                Route::put('/customer', 'Api\\' . $api . '\CaseController@updateProfile')->name('customer-profile-update');
            });
        });
    }
);

Route::get('/getAvailableDateTime', 'Web\GetInTouchController@getAvailableDateTime')->name('getAvailableDateTime');

// new endpoint between hr and cpc-crm

Route::post('api/v1/hr-lead', 'Api\\' . $api . '\GetInTouchController@storeHRLead');

Route::post('/api/stripe/webhook', 'Web\WebhookController@handle');

// have a empty the caches :
// Route::get('/clear-cache', function() {
//     Artisan::call('cache:clear');
//     Artisan::call('config:cache');
//     return 'DONE'; //Return anything
// });

/*Route::get('/affectationRoutine',function(){
    $months = [
       ['2021-09-01','2021-09-31'],
       ['2021-10-01','2021-10-31'],
       ['2021-11-01','2021-11-31']
    ];
    $statuses = [1,2,3,4,6,7,8,9];
    $conseillers = [0,0,0,0,0];

    foreach($months as $month){
       foreach($statuses as $status){
           $count = \App\Models\Lead::where('lead_status_id',$status)->whereBetween('created_at',[$month[0],$month[1]])->count();
           $limit = intval($count/count($conseillers));
           $offset = 0;
           $rest = $count%count($conseillers);
           foreach($conseillers as $conseiller){
               DB::statement("
                 UPDATE leads SET conseiller_id=$conseiller
                     WHERE id IN (
                         SELECT id FROM (
                             SELECT id FROM leads
                             where created_at between '".$month[0]."' and '".$month[1]. "' " .
                             "and lead_status_id = $status
                             LIMIT $limit
                             offset $offset
                         ) tmp
                     )
               ");
               $offset += $limit;
           }
           for($i=0;$i<$rest;$i++){
               DB::statement("
                 UPDATE leads SET conseiller_id=$conseiller
                     WHERE id IN (
                         SELECT id FROM (
                             SELECT id FROM leads
                             where created_at between '".$month[0]."' and '".$month[1]. "' " .
                             "and lead_status_id = $status
                             LIMIT 1
                             offset " . ($offset+$i) .
                         ") tmp
                     )
               ");
           }
       }
    }
});*/
