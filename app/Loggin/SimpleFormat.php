<?php
namespace App\Loggin ;

use Monolog\Formatter\LineFormatter;

class SimpleFormat {


    public function __invoke($logger)
    {   
        foreach($logger->getHandlers() as $handler){
            $handler->setFomatter(
                new LineFormatter('[%datetime%] : %message% %contexte%')
            );  
        }
    }
}
