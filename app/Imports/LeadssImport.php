<?php

namespace App\Imports;

use App\Models\Lead;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\LeadStateLog;
use App\Models\Customer;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use Exception;

class LeadssImport implements WithHeadingRow, ToModel
{

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        // $name = trim($row["first_name"]);
        $f_name = trim($row["first_name"]);
        $l_name = trim($row["last_name"]);
        if (!Lead::where('phone', trim($row["phone"]))->exists()) {
            $lead = Lead::create([
                "program_id" => trim($row["program"]) == "Etudiant" ? 4 : 1,
                "email" => trim($row["email"]),
                "firstname" => trim($f_name),
                "lastname" => trim($l_name),
                "phone" => trim($row["phone"]),
                "campaign_id" => 27,
                "influencer" => 396,
                'conseiller_id' => 3,
            ]);

            Customer::create([
                "lead_id" => $lead->id,
                "program_id" => $lead->program_id,
                "firstname" => $lead->firstname,
                "lastname" => $lead->lastname,
                "campaign_id" => $lead->campaign_id,
                "lead_status" => $lead->lead_status_id,
                "customer_email" => $lead->email,
                "email" => $lead->email,
                "birthday" => $lead->birthday,
                "phone" => $lead->phone,
                "adresse_line_1" => $lead->adresse_line_1,
                "adresse_line_2" => $lead->adresse_line_2,
                "postal_code" => $lead->postal_code,
                "budget" => $lead->budget,
                "ranking_id" => $lead->ranking_id,
                "country_id" => $lead->country_id,
                "city" => $lead->city,
                "english_level_id" => $lead->english_level_id,
                "school_level_id" => $lead->school_level_id,
                "projected_diploma_type_id" => $lead->projected_diploma_type_id,
                "professional_domain_id" => $lead->professional_domain_id,
                "professional_experience" => $lead->professional_experience,
            ]);

            LeadStateLog::create([
                'user_id' => 61,
                'lead_id' => $lead->id,
                'state_id' => $lead->lead_status_id,
            ]);
            return $lead;
        }
        return;
    }
}
