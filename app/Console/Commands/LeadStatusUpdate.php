<?php

namespace App\Console\Commands;

use App\Models\Bill;
use App\Models\Customer;
use App\Models\HistoCall;
use App\Models\Lead;
use App\Models\ProgramPayment;
use App\Models\Quote;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class LeadStatusUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leads:status_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        #266
        Lead::where('lead_status_id', 5)
            ->where('status_changed_at', '<=', Carbon::now()->subDays(1))
            ->update(['recycled' => 1, 'status_changed_at' => Carbon::now(), 'lead_status_id' => 11]);

        #267
        Lead::where('lead_status_id', 4)
            ->where('status_changed_at', '<=', Carbon::now()->subHours(4))
            ->where('to_office', 'conference')
            ->update(['recycled' => 1, 'status_changed_at' => Carbon::now(), 'lead_status_id' => 12]);

        #268
        $leads = Lead::where('lead_status_id', 1)
            ->where('status_changed_at', '<=', Carbon::now()->subDays(3));

        $leads_ids = $leads->get()->pluck('id')->toArray();
        $leads->whereIn('id',$leads_ids)->update(['recycled' => 1, 'status_changed_at' => Carbon::now(), 'lead_status_id' => 9]);

        #269
        /*$leads = Lead::where(function($query){
            $query->whereIn('lead_status_id', [2, 6])
                ->orWhereNull('lead_status_id');
            })
            ->whereBetween('created_at',['2021-08-01','2021-10-31'])
            ->where('status_changed_at', '<=', Carbon::now()->subDays(1))
            ->limit(480);

        $leads_ids = $leads->get()->pluck('id')->toArray();
        $leads->whereIn('id',$leads_ids)->update(['recycled' => 1, 'created_at' => Carbon::now(), 'status_changed_at' => Carbon::now(), 'lead_status_id' => null]);

        // delete calls
        HistoCall::whereIn('lead_id',$leads_ids)->delete();

        // delete media
        $customers_ids = Customer::whereIn('lead_id',$leads_ids)->where('is_client',0)->get()->pluck('id')->toArray();
        Media::whereIn('model_id',$customers_ids)->delete();

        // delete customers + calendar + bill + payment + quote
        Bill::whereIn('customer_id',$customers_ids)->forceDelete();
        Quote::whereIn('customer_id',$customers_ids)->forceDelete();
        Customer::whereIn('id',$customers_ids)->forceDelete();*/

        #270
        /*$leads = Lead::where('lead_status_id', 3)
            // ->whereBetween('created_at',['2021-11-01','2021-11-31'])
            ->where('status_changed_at', '<=', Carbon::now()->subDays(1));
        $leads_ids = $leads->get()->pluck('id')->toArray();
        $leads->whereIn('id',$leads_ids)->update(['recycled' => 1, 'created_at' => Carbon::now(), 'status_changed_at' => Carbon::now(), 'lead_status_id' => null]);

        // delete calls
        HistoCall::whereIn('lead_id',$leads_ids)->delete();

        // delete media
        $customers_ids = Customer::whereIn('lead_id',$leads_ids)->where('is_client',0)->get()->pluck('id')->toArray();
        Media::whereIn('model_id',$customers_ids)->delete();

        // delete customers + calendar + bill + payment + quote
        Bill::whereIn('customer_id',$customers_ids)->forceDelete();
        Quote::whereIn('customer_id',$customers_ids)->forceDelete();
        foreach (Customer::whereIn('id',$customers_ids)->get() as $cr) {
            $cr->requestedDocuments()->delete();
        }
        Customer::whereIn('id',$customers_ids)->forceDelete();*/

        #271
        /*$leads = Lead::whereNull('lead_status_id')
            // ->whereBetween('created_at',['2021-11-01','2021-11-31'])
            ->where('created_at', '<=', Carbon::now()->subDays(2));

        $leads_ids = $leads->get()->pluck('id')->toArray();
        $leads->whereIn('id',$leads_ids)->update(['recycled' => 1, 'created_at' => Carbon::now(), 'status_changed_at' => Carbon::now(), 'lead_status_id' => null]);*/
    }
}
