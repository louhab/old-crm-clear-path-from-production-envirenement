<?php

namespace App\Console\Commands;

use App\Models\CurrentSelectedConseiller;
use App\Models\Lead;
use App\Models\Role;
use App\Models\SelectedLeadsSupport;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SupportAffectationLeadsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leads:affectation_support';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $supports = User::where('role_id',Role::Support)->where('status',1)->get()->pluck('id')->toArray();
        $supports_id = CurrentSelectedConseiller::whereIn('conseiller_id',$supports)->get()->pluck('queue','conseiller_id')->toArray();
        $leads_id = Lead::whereNull('support_id')->get()->pluck('id')->toArray();
        
        print_r($supports_id);

        $offset = 0;
        foreach($supports_id as $key => $value){
            echo $key.'--'.$value.'--\n';
            echo '-----'.Lead::whereIn('id',array_slice($leads_id, $offset, $value))->update(['support_id'=>$key]);
            $offset += $value;
        }


        echo "\n-----\n";
        $leads_id = Lead::where('lead_status_id',4)->Where('created_at', '<=', Carbon::now()->subDays(7))->get()->pluck('id')->toArray();

        $offset = 0;
        foreach($supports_id as $key => $value){
            echo '-----'.Lead::whereIn('id',array_slice($leads_id, $offset, $value))->update(['lead_status_id'=>9,'support_id'=>$key]);
            $offset += $value;
        }
    }
}
