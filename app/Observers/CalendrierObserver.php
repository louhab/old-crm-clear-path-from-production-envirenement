<?php

namespace App\Observers;

use App\Models\Calendrier;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;

class CalendrierObserver
{
    /**
     * Handle the calendrier "created" event.
     *
     * @param  \App\Calendrier  $calendrier
     * @return void
     */
    public function created(Calendrier $calendrier)
    {
        //
        $customer = Customer::find($calendrier->customer_id);
        if ($customer) {
            $step = $customer->case->program_step_id;
            if(str_starts_with($calendrier->title, "Recontre 1") || str_starts_with($calendrier->title, "Rencontre 1")) {
                $step = 2;
            }
            elseif(str_starts_with($calendrier->title, "Rencontre 2")) {
                $step = 3;
            }
            elseif(str_starts_with($calendrier->title, "Rencontre 3")) {
                $step = 4;
            }
            if ($customer->is_client) {
                $step = 3;
            }
            // elseif (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'case-edit')
            DB::table('calendriers')->where('id', $calendrier->id)->update(['program_step_id' => $step]);
        }
    }

    /**
     * Handle the calendrier "updated" event.
     *
     * @param  \App\Calendrier  $calendrier
     * @return void
     */
    public function updated(Calendrier $calendrier)
    {
        //
    }

    /**
     * Handle the calendrier "deleted" event.
     *
     * @param  \App\Calendrier  $calendrier
     * @return void
     */
    public function deleted(Calendrier $calendrier)
    {
        //
    }

    /**
     * Handle the calendrier "restored" event.
     *
     * @param  \App\Calendrier  $calendrier
     * @return void
     */
    public function restored(Calendrier $calendrier)
    {
        //
    }

    /**
     * Handle the calendrier "force deleted" event.
     *
     * @param  \App\Calendrier  $calendrier
     * @return void
     */
    public function forceDeleted(Calendrier $calendrier)
    {
        //
    }
}
