<?php

namespace App\Observers;

use App\Models\Lead;
use App\Models\LeadStatus;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LeadObserver
{
    /**
     * Handle the lead "created" event.
     *
     * @param  \App\Lead  $lead
     * @return void
     */
    public function created(Lead $lead)
    {
        //
        DB::table('leads')->where('id',$lead->id)->update(['processed_at' => null,'status_changed_at' => null]);
        //$lead->processed_at = Carbon::now();
        //$lead->status_changed_at = Carbon::now();
        //$lead->save();
        $y = LeadStatus::find($lead->lead_status_id);
        activity('lead-status-log')
            ->performedOn($lead)
            ->causedBy(Auth::user())
            ->withProperties(['status' => ($y ? $y->id: null)])
            ->log('Lead Created');
    }

    /**
     * Handle the lead "updated" event.
     *
     * @param  \App\Lead  $lead
     * @return void
     */
    public function updated(Lead $lead)
    {
        //
        if($lead->isDirty('lead_status_id')) {
            DB::table('leads')->where('id',$lead->id)->update(['status_changed_at' => Carbon::now(),'processed_at' => Carbon::now()]);
            //$lead->status_changed_at = Carbon::now();
            //$lead->save();
            $y = LeadStatus::find($lead->lead_status_id);
            activity('lead-status-log')
                ->performedOn($lead)
                ->causedBy(Auth::user())
                ->withProperties(['status' => ($y ? $y->id: null)])
                ->log('Status Updated');
        }
    }

    /**
     * Handle the lead "deleted" event.
     *
     * @param  \App\Lead  $lead
     * @return void
     */
    public function deleted(Lead $lead)
    {
        //
    }

    /**
     * Handle the lead "restored" event.
     *
     * @param  \App\Lead  $lead
     * @return void
     */
    public function restored(Lead $lead)
    {
        //
    }

    /**
     * Handle the lead "force deleted" event.
     *
     * @param  \App\Lead  $lead
     * @return void
     */
    public function forceDeleted(Lead $lead)
    {
        //
    }
}
