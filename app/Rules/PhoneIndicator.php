<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PhoneIndicator implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $value_err = '';
    private $messages = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

    public function __construct($message)
    {
        $this->value_err = $message;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /*if (str_contains($value, '+212'))
            return !str_contains($value, '+2120');*/
        return preg_match("/^(\+212)[0-9]{9,9}$/", $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Numéro erroné -> ' . $this->value_err . '<br />(format: +212650123456)';
    }
}
