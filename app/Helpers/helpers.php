<?php

use Illuminate\Support\Facades\Auth;

if(!function_exists('isCustomer')) {
    function isCustomer() {
        if(Auth::guard('customer')->user()) return true;
        return false;
    }
}

if(!function_exists('getRankingTextColor')) {
    function getRankingTextColor($rankingId) {
        switch ($rankingId) {
            case 1 : return 'Green';
            case 2 : return 'Orange';
            case 3 : return 'Red';
        }
    }
}

if(!function_exists('getSigningRouteName')) {
    function getSigningRouteName($media) {

        $origial_file_id = $media->getCustomProperty('original_file_id');
        $pending_file_id = $media->getCustomProperty('pending_file_id');
        $is_signed = $media->getCustomProperty('is_signed');

        if ($is_signed) return Auth::guard('customer')->user() ? 'customer-signeasy-download-signed-document' : 'signeasy-download-signed-document';

        if (strlen($pending_file_id) > 1) return 'customer-signeasy-get-signing-url';
        else if($origial_file_id > 0) return 'signeasy-send-signing-request';
        return 'signeasy-import-document';
    }
}

if(!function_exists('getSigningLabel')) {
    function getSigningLabel($media) {

        $original_file_id = $media->getCustomProperty('original_file_id');
        $pending_file_id = $media->getCustomProperty('pending_file_id');
        $is_signed = $media->getCustomProperty('is_signed');

        if($is_signed) return 'Télécharger le document signé';
        //dd(strlen($pending_file_id), strlen(0));

        if (strlen($pending_file_id) > 1) {
            return 'Signer le document';
        } else if($original_file_id > 0) return 'Envoyer la demande de signature';
        return 'Préparer pour signature';
    }
}

if(!function_exists('isMediaSignable')) {
    function isMediaSignable($collectionName) {
        return true;
        //return strpos($collectionName, '_SIGNABLE');
    }
}

if(!function_exists('isCustomerOwner')) {
    function isCustomerOwner(\App\Models\Customer $customer, $collectionName) {

        return $customer->getFirstMedia($collectionName)->getCustomProperty('cust_owner');

    }
}
