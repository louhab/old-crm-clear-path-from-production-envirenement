<?php

namespace App\Http\Livewire\Program;

use App\Models\Program;
use Livewire\Component;

class View extends Component
{
    public $program;

    public function mount(Program $program)
    {
        $this->program = $program;
    }
    public function render()
    {
        return view('livewire.program.view')
            ->layout('layouts.backend')
            ->slot('content');
    }
}
