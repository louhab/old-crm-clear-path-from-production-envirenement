<?php

namespace App\Http\Livewire\Block;

use App\Models\Lead;
use App\Models\LeadStateLog;
use App\Models\HistoCall;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class CallCount extends Component
{
    public $lead;
    public $states;
    public $calls;

    public function mount(Lead $lead) {
        $this->lead = $lead;
        $this->states = LeadStateLog::where('lead_id', $lead->id)->get()->toArray();
        $this->calls = HistoCall::where('lead_id', $lead->id)
            ->select('lead_state_id', DB::raw('count(*) as total'))
            ->groupBy("lead_state_id")
            ->get()
            ->toArray();
        // dd($this->calls);
    }

    public function render()
    {
        return view('livewire.block.call-count');
    }
}
