<?php

namespace App\Http\Livewire\Block;

use App\Models\CaseState;
use App\Models\CustomerCase;
use Livewire\Component;

class Message extends Component
{
    public $customer_case;
    public $case_state;

    public function mount(CustomerCase $customer_case){
        $this->customer_case = $customer_case;
        $this->case_state = CaseState::where("sub_step_state_id", 2)
            ->where("customer_case_id", $this->customer_case->id)
            ->first();
    }
    public function render()
    {
        return view('livewire.block.message');
    }
}
