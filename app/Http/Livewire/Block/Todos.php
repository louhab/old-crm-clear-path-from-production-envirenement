<?php

namespace App\Http\Livewire\Block;

use Livewire\Component;
use App\Models\CaseComment;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\Task as TaskModel;

class Todos extends Component
{
    public $admission;
    public $caq;
    public $cpg;
    public $facture;
    public $prise_en_charge;
    public $imm;

    public $total;

    public $todo_today = [];
    public $overdue_task = [];
    public $follow_up_today = [];
    public $overdue_follow_up = [];

    public function mount(){

        $this->tasks = TaskModel::join('users', 'users.id', '=', 'tasks.user_id')
        ->join('roles', 'roles.id', '=', 'users.role_id')
        ->join('customer_cases', 'customer_cases.id', '=', 'tasks.customer_case_id')
        ->join('customers', 'customers.id', '=', 'customer_cases.customer_id')
        ->join('leads', 'leads.id', '=', 'customers.lead_id')
        ->leftJoin('users as u2', 'u2.id', '=', 'customers.backoffice_id')
        ->where(function ($query) {
            $query->where('role_name', '=', "Admin")
                    ->orWhere('role_name', '=', "Conseiller")
                    ->orWhere('role_name', '=', "Manager");
        });
        // ->where("tasks.is_done", "<>", "1");

        if(Auth::user()->isConseiller()) {
            $this->tasks = $this->tasks->where('leads.conseiller_id', Auth::id());
        }

        $this->tasks = $this->tasks->get(['u2.name', 'tasks.created_at','tasks.note','tasks.is_done as is_done', 'tasks.is_relaunched as is_relaunched' ,'tasks.canal', 'leads.firstname', 'leads.lastname', 'tasks.customer_case_id']);
        $this->tasks = $this->filter_tasks($this->tasks);

        // calculat total tasks
        $this->total =  count($this->todo_today) + count($this->overdue_task) + count($this->follow_up_today) + count($this->overdue_follow_up);
        // $this->total =  count($this->tasks);
    }


    private function filter_tasks ($tasks) {
        foreach($tasks as $task) {
            if (str_starts_with($task->canal, "course") ) {
                $task->canal = str_replace("course", "admission", $task->canal);
            }

            if ($task->is_done == 0 && $task->is_relaunched == 0) {
                $currentTime = Carbon::now();
                $issueTime = $this->getCreatedAtDate($task["created_at"])->addHours(24);
                if($currentTime->lte($issueTime)) {
                    $task->msg = "Tache à faire";
                    $task->status_class = "bg-success-light text-success";
                    $this->todo_today[] = $task;
                } else {
                    $task->msg = "Tache en retard";
                    $task->status_class = "bg-warning-light text-warning";
                    $this->overdue_task[] = $task;
                }
            } else {
                $currentTime = Carbon::now();
                $issueTime = $this->getCreatedAtDate($task["created_at"])->addHours(12);

                if($currentTime->lte($issueTime)) {
                    $task->msg = "Suivi à faire";
                    $task->status_class = "bg-info-light text-info";
                    $this->follow_up_today[] = $task;
                } else {
                    $task->msg = "Suivi en retard";
                    $task->status_class = "bg-danger-light text-danger";
                    $this->overdue_follow_up[] = $task;
                }
            }
        }
    }

    private function getCreatedAtDate($date)
    {
        return Carbon::parse($date);
    }

    public function render()
    {
        return view('livewire.block.todos');
    }
}
