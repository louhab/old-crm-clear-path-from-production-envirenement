<?php

namespace App\Http\Livewire\Block;

use Illuminate\Support\Facades\Route;
use Livewire\Component;
use Livewire\WithFileUploads;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Result extends Component
{
    use WithFileUploads;
    public $documents;
    public $customer_case;
    public $currentRouteName;
    public $substep;

    public function mount($customer_case, $meet) {
        // case-edit - soumission-view
        $this->currentRouteName = Route::currentRouteName();
        $this->customer_case = $customer_case;
        switch ($meet) {
            case 'R1':
                $this->substep = \App\Models\ProgramSubStep::find(8);
                break;
            case 'R4':
                $this->substep = \App\Models\ProgramSubStep::find(9);
                break;
            case 'R5':
                $this->substep = \App\Models\ProgramSubStep::find(22);
                break;
        }
    }
    public function updatedDocuments()
    {
        foreach ($this->documents as $pid => $document) {
            foreach ($this->customer_case->customer->getMedia($pid) as $media) $media->delete();
            $this->customer_case->customer
                ->addMedia($document)
                ->toMediaCollection($pid);
        }
        return redirect(request()->header('Referer'));
    }
    public function deleteMedia($collection){
        foreach ($this->customer_case->customer->getMedia($collection) as $media) $media->delete();
        return redirect(request()->header('Referer'));
    }
    public function toggleProperty($mediaId){
        $media = Media::find($mediaId);
        // dd($media->getCustomProperty('visible_for_customer'), !$media->getCustomProperty('visible_for_customer'));
        $media->setCustomProperty('visible_for_customer', !$media->getCustomProperty('visible_for_customer'));
        $media->save();
        // dd($media->getCustomProperty('visible_for_customer'));
        return redirect(request()->header('Referer'));
    }
    public function render()
    {
        return view('livewire.block.result');
    }
}
