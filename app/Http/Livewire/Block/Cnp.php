<?php

namespace App\Http\Livewire\Block;

use App\Models\OccupationalClassification;
use Livewire\Component;

class Cnp extends Component
{
    public $customer_case;
    public $cnp;
    public $level;

    public function mount($customer_case)
    {
        $this->customer_case = $customer_case;
        if ($this->customer_case->customer->classifications()->count() > 0) {
            $class = $this->customer_case->customer->classifications()->get()->last();
            $this->cnp = $class->cnp;
            $this->level = $class->level;
        }
    }
    public function save()
    {
        $cnp = OccupationalClassification::where('cnp', $this->cnp)->where("level", $this->level)->first();
        // dd($cnp);
        if ($cnp) {
            $customer_cnp = $this->customer_case->customer->classifications()->wherePivot('cnp_id', '=', $cnp->id);
            if ($customer_cnp->count() == 0)
                $this->customer_case->customer->classifications()->attach($cnp);
        } else {
            $cnp = OccupationalClassification::create([
                'cnp' => $this->cnp,
                'level' => $this->level
            ]);
            $this->customer_case->customer->classifications()->detach();
            $this->customer_case->customer->classifications()->attach($cnp);
        }
        return redirect(request()->header('Referer'));
        // dd('test');
    }
    public function render()
    {
        return view('livewire.block.cnp');
    }
}
