<?php

namespace App\Http\Livewire\Block;

use Livewire\Component;
use Illuminate\Support\Facades\DB;

class AdvisorStats extends Component
{
    public $user;
    public $stats;

    public function mount($user) {
        $this->user = $user;
        $this->stats = DB::table('program_payment_customer as pc')
        ->leftJoin('customers as c', 'c.id', '=', 'pc.customer_id')
        ->leftJoin('leads', 'leads.id', '=', 'c.lead_id')
        ->leftJoin('program_payments as pp', 'pp.id', '=', 'pc.program_payment_id')
        ->leftJoin('programs', 'programs.id', '=', 'c.program_id')
        ->leftJoin('currencies', 'currencies.id', '=', 'pc.currency_id')
        ->select([DB::raw("SUM(pc.amount) AS total_amount"), DB::raw("COUNT(pc.program_payment_id) AS total_meets"), "currencies.iso", "currencies.id", 'pp.name_fr as payment', "programs.name as program_name", "pc.performed_at"])
        ->where("leads.conseiller_id", "=", $user->id)
        ->groupBy('pc.currency_id',"pc.program_payment_id")
        ->orderBy('pp.name_fr', 'asc')
        ->get()->toArray();

        $int_array = [];
        $data_array = [];

        foreach ($this->stats as $key => $value) {
            if (!in_array($value->payment, $int_array)) {
                $value->total_amouts = $value->total_amount . " " . $value->iso;
                $data_array[$value->payment] = $value;
                array_push($int_array, $value->payment);
            } else {
                $data_array[$value->payment]->total_amouts .= ", " . $value->total_amount . " " . $value->iso;
                $data_array[$value->payment]->total_meets += $value->total_meets;
            }
        }
        $this->stats = collect($data_array);
    }

    public function render()
    {
        return view('livewire.block.advisor-stats');
    }
}
