<?php

namespace App\Http\Livewire\Block;

use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media as CustomerMedia;

class ClearDocument extends Component
{
    public $customerCase;
    public $mediaSheet;
    public $garants;

    protected $listeners = [
        "delMedia" => "updateMedias"
    ];

    public function updateMedias()
    {
        return redirect(request()->header('Referer'));
    }

    public function mount($customerCase, $mediaSheet)
    {
        $this->garants = \App\Models\CustomerGarant::where('customer_id', $customerCase->customer_id)->orderBy('id', 'ASC')->get();
        $this->mediaSheet   = $mediaSheet;
        $this->customerCase = $customerCase;
    }
    public function downloadMedia(CustomerMedia $media)
    {
        return $media;
    }

    public function removeMedia(CustomerMedia $media)
    {
        $media->delete();
        $this->emit('delMedia');
    }

    public function render()
    {
        return view('livewire.block.clear-document');
    }
}
