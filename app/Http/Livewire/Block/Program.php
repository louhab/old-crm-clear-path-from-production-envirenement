<?php

namespace App\Http\Livewire\Block;

use App\Models\OrientationSchool;
use Illuminate\Support\Facades\Route;
use Livewire\Component;
use Livewire\WithFileUploads;

class Program extends Component
{
    use WithFileUploads;
    public $customer_case;
    public $stepstate;
    public $case_state;
    public $documents;
    public $currentRouteName;
    // public $collection;

    public function mount($customer_case) {
        $this->customer_case = $customer_case;
        $this->currentRouteName = Route::currentRouteName();
        $this->stepstate = \App\Models\SubStepState::find(16);
        $this->case_state = \App\Models\CaseState::where("sub_step_state_id", 16)
            ->where("customer_case_id", $customer_case->id)
            ->first();
        // $collection = "Accusé d'envoie";
    }
    public function updateProgram($program, $value) {
        if ($value == 'Admis') {
            $this->customer_case->customer->schools()->updateExistingPivot($program['id'], array('admis' => 1), false);
        } elseif ($value == 'Refus') {
            $this->customer_case->customer->schools()->updateExistingPivot($program['id'], array('admis' => 0), false);
        } elseif ($value == 'Reset') {
            $this->customer_case->customer->schools()->updateExistingPivot($program['id'], array('admis' => null), false);
        }
        // foreach ($this->customer_case->customer->getMedia($program['id'].".Lettre Acceptation") as $media) $media->delete();
    }
    public function updatedDocuments()
    {
        // dd($this->documents);
        foreach ($this->documents as $collection => $document) {
            foreach ($this->customer_case->customer->getMedia($collection) as $media) $media->delete();
            $this->customer_case->customer
                ->addMedia($document)
                ->withCustomProperties(['visible_for_customer' => 1])
                ->toMediaCollection($collection);
        }
        return redirect(request()->header('Referer'));
        /*foreach ($this->documents as $pid => $document) {
            $pr = OrientationSchool::find($pid);
            // dd($pr);
            $collection = "Accusé d'envoie";
            foreach ($this->customer_case->customer->getMedia($pid.".".$collection) as $media) $media->delete();
            $this->customer_case->customer
                ->addMedia($document)
                ->toMediaCollection($pid.".".$collection);
        }*/
        // return redirect(request()->header('Referer'));
    }
    public function deleteMedia($collection){
        foreach ($this->customer_case->customer->getMedia($collection) as $media) $media->delete();
        return redirect(request()->header('Referer'));
    }
    public function render()
    {
        return view('livewire.block.program');
    }
}
