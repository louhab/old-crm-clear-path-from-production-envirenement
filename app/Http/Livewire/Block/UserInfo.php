<?php

namespace App\Http\Livewire\Block;

use Livewire\Component;

class UserInfo extends Component
{
    public $user;

    public function mount($user) {
        $this->user = $user;
    }

    public function render()
    {
        return view('livewire.block.user-info');
    }
}
