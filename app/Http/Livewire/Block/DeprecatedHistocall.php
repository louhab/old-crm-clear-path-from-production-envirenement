<?php

namespace App\Http\Livewire\Block;

use App\Models\Lead;
use Livewire\Component;
use App\Models\HistoCall as HistoCallModel;

class DeprecatedHistocall extends Component
{
    public $lead;
    public $calls;

    public function mount(Lead $lead) {
        $this->lead = $lead;
        $this->calls = HistoCallModel::where("lead_id", $lead->id)->get();
    }

    public function render()
    {
        return view('livewire.block.deprecated-histocall');
    }
}
