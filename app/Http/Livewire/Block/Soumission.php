<?php

namespace App\Http\Livewire\Block;

use App\Models\OrientationSchool;
use Illuminate\Support\Facades\Route;
use Livewire\Component;
use Livewire\WithFileUploads;

class Soumission extends Component
{
    use WithFileUploads;
    public $documents;
    public $customer_case;
    public $stepstate;
    public $case_state;
    public $currentRouteName;
    public $score;

    public function mount($customer_case) {
        // case-edit - soumission-view
        $this->currentRouteName = Route::currentRouteName();
        $this->customer_case = $customer_case;
        $this->stepstate = \App\Models\SubStepState::find(16);
        $this->case_state = \App\Models\CaseState::where("sub_step_state_id", 16)
            ->where("customer_case_id", $customer_case->id)
            ->first();
        $this->score = $customer_case->customer->score;
    }

    public function updateProgram($program, $value) {
        if ($value == 'Admis') {
            // dd($program);
            $this->customer_case->customer->schools()->updateExistingPivot($program['id'], array('admis' => 1), false);
        } elseif ($value == 'Refus') {
            $this->customer_case->customer->schools()->updateExistingPivot($program['id'], array('admis' => 0), false);
        } elseif ($value == 'Reset') {
            $this->customer_case->customer->schools()->updateExistingPivot($program['id'], array('admis' => null), false);
        }
        foreach ($this->customer_case->customer->getMedia($program['id'].".Lettre Acceptation") as $media) $media->delete();
    }
    public function updatedDocuments()
    {
        foreach ($this->documents as $collection => $document) {
            foreach ($this->customer_case->customer->getMedia($collection) as $media) $media->delete();
            $this->customer_case->customer
                ->addMedia($document)
                ->withCustomProperties(['visible_for_customer' => 1])
                ->toMediaCollection($collection);
        }
        return redirect(request()->header('Referer'));
    }

    public function updated($propertyName)
    {
        $this->customer_case->customer->update(["score" => $this->score]);
        $this->addError('score', 'modification réussite!');
    }

    public function deleteMedia($pid){
        if ($this->customer_case->program_id == 4){
            $collection = $pid.".Lettre Acceptation";
        }
        else {
            $collection = $pid;
        }
        foreach ($this->customer_case->customer->getMedia($collection) as $media) $media->delete();
        return redirect(request()->header('Referer'));
    }
    public function render()
    {
        return view('livewire.block.soumission');
    }
}
