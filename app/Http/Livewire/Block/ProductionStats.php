<?php

namespace App\Http\Livewire\Block;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProductionStats extends Component
{
    public $payments_stats;
    public $calls_stats;
    public $meets_stats;

    public $visio_meets;
    public $office_meets;
    public $total_meets;

    protected $listeners = ["filterChanged" => "update_amount"];

    public function mount() {
        // payment related stats
        $this->payments_stats = DB::table('program_payment_customer as pc')
        ->selectRaw('SUM(pc.amount) AS total_amount_payment, COUNT(pc.id) as total_payments, currencies.iso')
        ->leftJoin('customers', "customers.id", '=','pc.customer_id')
        ->leftJoin('leads','leads.id','=','customers.lead_id')
        ->leftJoin('users','users.id','=','leads.conseiller_id')
        ->leftjoin("roles", "roles.id", "=", "users.role_id")
        ->leftjoin("currencies", "currencies.id", "=", "pc.currency_id")
        ->where("users.role_id", "=", "3") // only for advisors (conseiller)
        ->groupBy("pc.currency_id")
        ->get();

        // calls stats
        $this->calls_stats = DB::table('users')
        ->selectRaw('users.id, COUNT(DISTINCT histo_calls.id) AS total_calls')
        ->leftjoin("roles", "users.role_id", "=", "roles.id")
        ->leftJoin('leads','leads.conseiller_id','=','users.id')
        ->leftJoin('customers','customers.lead_id','=','leads.id')
        ->leftJoin('histo_calls','histo_calls.user_id','=',"users.id")
        ->where("users.role_id", "=", "3") // only for advisors (conseiller)
        ->groupBy("users.id")
        ->first();

        $this->calls_stats = $this->calls_stats->total_calls;

        /**
            SELECT COUNT(calendriers.id) as total_meets, calendriers.to_office FROM `calendriers`
            LEFT JOIN customers ON customers.id = calendriers.customer_id
            LEFT JOIN leads ON leads.id = customers.lead_id
            LEFT JOIN users ON users.id = leads.conseiller_id
            LEFT JOIN roles ON roles.id = users.role_id
            WHERE users.role_id = 3
            GROUP BY calendriers.to_office;
         */

        // meets stats
        $this->meets_stats = DB::table('calendriers as cal')
        ->selectRaw('COUNT(cal.id) as total_meets, cal.to_office')
        ->leftjoin("customers", "customers.id", "=", "cal.customer_id")
        ->leftJoin('leads','leads.id','=','customers.lead_id')
        ->leftJoin('users','users.id','=','leads.conseiller_id')
        ->leftjoin("roles", "roles.id", "=", "users.role_id")
        ->where("users.role_id", "=", "3")
        ->groupBy("cal.to_office")
        ->get()->toArray();

        $this->total_meets = 0;
        $this->office_meets = 0;
        $this->visio_meets = 0;

        foreach ($this->meets_stats as $key => $value)  {
            if ($value->to_office == "office") {
                $this->office_meets = $value->total_meets ?: 0;
            } elseif ($value->to_office == "conference") {
                $this->visio_meets = $value->total_meets ?: 0;
            }
            $this->total_meets += $value->total_meets;
        }
    }

    public function update_amount($users, $datarange, $programs) {
        $this->payments_stats = DB::table('program_payment_customer as pc')
        ->selectRaw('SUM(pc.amount) AS total_amount_payment, COUNT(pc.id) as total_meets, currencies.iso')
        ->leftJoin('customers', "customers.id", '=','pc.customer_id')
        ->leftJoin('leads','leads.id','=','customers.lead_id')
        ->leftJoin('users','users.id','=','leads.conseiller_id')
        ->leftjoin("roles", "roles.id", "=", "users.role_id")
        ->leftjoin("currencies", "currencies.id", "=", "pc.currency_id")
        ->where(function ($query) use ($users, $datarange, $programs) {
            if(isset($users) && !empty($users)) {
                $query->whereIn('leads.conseiller_id', $users);
                if(in_array('n/d', $users)) {
                    $query->orWhereNull('leads.conseiller_id');
                }
            }
            if(isset($datarange) && !empty($datarange)) {
                $datarange = explode(" - ", $datarange);
                $start = Carbon::parse($datarange[0]);
                // where operation created at
                $query->where('pc.performed_at', '>=', $start->format('Y-m-d').' 00:00:00');
                $end = Carbon::parse($datarange[1])->addDay();
                // where operation created at
                $query->where('pc.performed_at', '<=', $end->format('Y-m-d').' 00:00:00');
            }

            if(isset($programs) && !empty($programs)) {
                $query->whereIn('customers.program_id', $programs);
            }
        })
        ->where("users.role_id", "=", "3") // only for advisors (conseiller)
        ->groupBy("pc.currency_id")
        ->get();

        // calls stats
        $this->calls_stats = DB::table('users')
        ->selectRaw('users.id, COUNT(DISTINCT histo_calls.id) AS total_calls')
        ->leftjoin("roles", "users.role_id", "=", "roles.id")
        ->leftJoin('leads','leads.conseiller_id','=','users.id')
        ->leftJoin('customers','customers.lead_id','=','leads.id')
        ->leftJoin('histo_calls','histo_calls.user_id','=',"users.id")
        ->where(function ($query) use ($users, $datarange, $programs) {
            if(isset($users) && !empty($users)) {
                $query->whereIn('leads.conseiller_id', $users);
                if(in_array('n/d', $users)) {
                    $query->orWhereNull('leads.conseiller_id');
                }
            }

            if(isset($datarange) && !empty($datarange)) {
                $datarange = explode(" - ", $datarange);
                $start = Carbon::parse($datarange[0]);
                // where operation created at
                $query->where('histo_calls.created_at', '>=', $start->format('Y-m-d').' 00:00:00');
                $end = Carbon::parse($datarange[1])->addDay();
                // where operation created at
                $query->where('histo_calls.created_at', '<=', $end->format('Y-m-d').' 00:00:00');
            }

            if(isset($programs) && !empty($programs)) {
                $query->whereIn('customers.program_id', $programs);
            }
        })
        ->where("users.role_id", "=", "3") // only for advisors (conseiller)
        ->groupBy("users.id")
        ->first();
        // dd($this->calls_stats);

        $this->calls_stats = $this->calls_stats ? $this->calls_stats->total_calls : 0;

        $this->meets_stats = DB::table('calendriers as cal')
        ->selectRaw('COUNT(cal.id) as total_meets, cal.to_office')
        ->leftjoin("customers", "customers.id", "=", "cal.customer_id")
        ->leftJoin('leads','leads.id','=','customers.lead_id')
        ->leftJoin('users','users.id','=','leads.conseiller_id')
        ->leftjoin("roles", "roles.id", "=", "users.role_id")
        ->where(function ($query) use ($users, $datarange, $programs) {
            if(isset($users) && !empty($users)) {
                $query->whereIn('leads.conseiller_id', $users);
                if(in_array('n/d', $users)) {
                    $query->orWhereNull('leads.conseiller_id');
                }
            }

            if(isset($datarange) && !empty($datarange)) {
                $datarange = explode(" - ", $datarange);
                $start = Carbon::parse($datarange[0]);
                // where operation created at
                $query->where('cal.created_at', '>=', $start->format('Y-m-d').' 00:00:00');
                $end = Carbon::parse($datarange[1])->addDay();
                // where operation created at
                $query->where('cal.created_at', '<=', $end->format('Y-m-d').' 00:00:00');
            }

            if(isset($programs) && !empty($programs)) {
                $query->whereIn('customers.program_id', $programs);
            }
        })
        ->where("users.role_id", "=", "3")
        ->groupBy("cal.to_office")
        ->get()->toArray();

        $this->total_meets = 0;
        $this->office_meets = 0;
        $this->visio_meets = 0;

        foreach ($this->meets_stats as $key => $value)  {
            if ($value->to_office == "office") {
                $this->office_meets = $value->total_meets ?: 0;
            } elseif ($value->to_office == "conference") {
                $this->visio_meets = $value->total_meets ?: 0;
            }
            $this->total_meets += $value->total_meets;
        }
    }

    public function render()
    {
        return view('livewire.block.production-stats');
    }
}
