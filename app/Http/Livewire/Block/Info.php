<?php

namespace App\Http\Livewire\Block;

use Livewire\Component;

class Info extends Component
{
    public $customer_case;
    public $substep;
    public $customer_errors;
    public $conseiller;
    public $support;
    public $lang;
    protected $listeners = ["toggleMeetType" => "toggleMeetType"];

    public function mount($customer_case, $errors)
    {
        $this->conseiller = \App\Models\User::where('id', $customer_case->customer->lead->conseiller_id)->first();
        $this->support = \App\Models\User::where('id', $customer_case->customer->lead->support_id)->first();
        $this->lang = session()->get('locale') === 'en' ? "en" : "fr";
        $this->customer_case = $customer_case;
        $this->substep = \App\Models\ProgramSubStep::where("id", 1)->first();
        $this->customer_errors = false;
        $fields = $this->substep->fields()->wherePivot('program_id', '=', $customer_case->program_id)->get();
        // dd($fields);
        foreach ($fields as $field) {
            if (isset($errors) && $errors->has($field->field_name)) {
                $this->customer_errors = true;
                break;
            }
        }
    }

    public function toggleMeetType()
    {
        // $meetType = $this->customer_case->customer->lead->to_office;
        if ($this->customer_case->customer->lead->to_office == "office")
            $this->customer_case->customer->lead->update(["to_office" => "conference"]);
        else
            $this->customer_case->customer->lead->update(["to_office" => "office"]);
        return redirect(request()->header('Referer'));
    }

    public function render()
    {
        return view('livewire.block.info');
    }
}
