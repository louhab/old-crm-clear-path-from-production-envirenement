<?php

namespace App\Http\Livewire\Block;

use App\Models\CustomerCase;
use Livewire\Component;

class Document extends Component
{
    public $customer_case;
    public $location;
    public $pane;
    public $groupedArray;

    public function mount(CustomerCase $customer_case, $location=null)
    {
        $this->customer_case = $customer_case;
        $this->location = $location;
        if (is_null($location)) {
            $this->pane = -1;
            $groupedArray = array();
            $mediasheets = \App\Models\MediaSheet::where('program_id', $customer_case->program_id)->orderBy('order')->get();
            foreach($mediasheets as $mediaSheet) {
                $docs = $mediaSheet->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get();
                foreach($docs as $doc) {
                    if($customer_case->customer->getMedia($doc->collection_name)->count()) {
                        foreach($customer_case->customer->getMedia($doc->collection_name) as $media) {
                            if (!array_key_exists($mediaSheet->id, $groupedArray))
                                $groupedArray[$mediaSheet->id] = array();
                            $groupedArray[$mediaSheet->id][] = $media->id;
                        }
                    }
                }
            }
            $this->groupedArray = $groupedArray;
            // dd($groupedArray);
        }
    }

    public function togglePane($pane) {
        $this->pane = $pane;
    }
    public function render()
    {
        return view('livewire.block.document');
    }
}
