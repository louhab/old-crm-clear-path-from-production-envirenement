<?php

namespace App\Http\Livewire\Block;

use Livewire\Component;
use Illuminate\Support\Facades\DB;

class MeetList extends Component
{
    public $user;
    public $meets;
    public $filtred_meets;

    public function mount($user) {
        $this->user = $user;
        $this->meets = DB::table('program_payment_customer as pc')
        ->selectRaw("CONCAT(c.firstname, ' ', c.lastname) AS name, pp.name_fr as payment, programs.name as program_name, pc.performed_at")
        ->leftJoin('customers as c', 'c.id', '=', 'pc.customer_id')
        ->leftJoin('leads', 'leads.id', '=', 'c.lead_id')
        ->leftJoin('program_payments as pp', 'pp.id', '=', 'pc.program_payment_id')
        ->leftJoin('programs', 'programs.id', '=', 'c.program_id')
        ->where("leads.conseiller_id", "=", $this->user->id)
        ->orderBy('pc.performed_at', 'DESC')
        ->get();

        $this->filtred_meets = [];
        foreach ($this->meets as $key => $value) {
                $this->filtred_meets[$value->program_name][] = $value;
        }
    }

    public function render()
    {
        return view('livewire.block.meet-list');
    }
}
