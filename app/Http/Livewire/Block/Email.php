<?php

namespace App\Http\Livewire\Block;

use App\Mail\sendingEmail;
use App\Models\CustomerCase;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class Email extends Component
{
    public $customer_case;
    public $email;
    public $message;

    public function mount(CustomerCase $customer_case)
    {
        $this->customer_case = $customer_case;
        $this->message = "";
    }
    public function render()
    {
        return view('livewire.block.email');
    }

    public function send() {
        if ($this->email == "1"){
            // dd($this->email);
            $customer = $this->customer_case->customer;
            $customer->password = Hash::make($customer->phone);
            $customer->save();


            $conseiller = User::find($customer->lead->conseiller_id);
            if(session()->get('locale') === 'en')
                $lead_lang = "en";
            else
                $lead_lang = "fr";

            $object = $lead_lang === "fr" ? 'Portail Clear Path Canada' : 'Clear Path Canada Portal';
            $message = "";


            $this->message = $message;

            $data = array(

                'name' => $customer->firstname . ' ' . $customer->lastname,
                "elements" => [
                    "lead_lang" => $lead_lang,
                    "conseiller" => $conseiller,
                    "customer" => $customer,
                    "email_phase" => 2,
                ],
                'object' => $object,
                'message' => $message
            );

            if ($customer->email) {
                try {
                    Mail::to($customer->email)->send(new sendingEmail($data));
                } catch (\Swift_TransportException $e) {
                    // echo 'Exception reçue : ',  $e->getMessage(), "\n";
                }
            }
            session()->flash('message', 'Email successfully sent.');
        }
        elseif ($this->email == "2" && $this->customer_case->program_id == 1){
            $object = 'Guide des tests de langue';
            $data = array(
                'name' => $this->customer_case->customer->firstname . ' ' . $this->customer_case->customer->lastname,
                'object' => $object,
                'message' => $this->message,
                "elements" => [
                    "email_phase" => 3,
                ],
            );

            if ($this->customer_case->customer->email) {
                try {
                    Mail::to($this->customer_case->customer->email)->send(new sendingEmail($data));
                } catch (\Swift_TransportException $e) {
                    // echo 'Exception reçue : ',  $e->getMessage(), "\n";
                }
            }
            session()->flash('message', 'Email successfully sent.');
        }
    }
    public function change()
    {
        // $this->emit('hallChanged', $value);
        if ($this->email == "1") {
            $customer = $this->customer_case->customer;
            $customer->password = Hash::make($customer->phone);
            $customer->save();
            $conseiller = User::find($customer->lead->conseiller_id);
           //Si french
           if(session()->get('locale')=='fr'){
            $this->message = "
                Nous vous remercions d’avoir fait confiance à CLEAR PATH CANADA et pour l’intérêt que vous portez à notre Agence.
                Je suis ".$conseiller->name." votre ".($conseiller->gender == "female" ? "conseillère":"conseiller").", je vous invite à rejoindre votre espace client en vous connectant sur notre Portail à travers le lien ci-dessous :
                https://crm.clearpathcanada.com/login/customer
                Nom d’utilisateur : ". $customer->phone . PHP_EOL .
                "Mot de passe : ". $customer->phone . PHP_EOL .
                "Restez connecter pour toutes informations sur l’état d’avancement de votre dossier. Vous pourriez ainsi communiquer avec moi à travers ce portail.
                Bien à vous.
                ".$conseiller->name."
                ".$conseiller->phone."
                +212 7 02 05 09 09
                ".$conseiller->email;
           }
           if(session()->get('locale')=='en'){
            $this->message = "
                We thank you for trusting CLEAR PATH CANADA.
                I am ".$conseiller->name." your ".($conseiller->gender == "female" ? "advisor":"advisor").", I invite you to join the client area through the following link::
                https://crm.clearpathcanada.com/login/customer
                User : ". $customer->phone . PHP_EOL ."
                Password : ". $customer->phone . PHP_EOL .
                "In this way, you can access all the information related to the progress of your case at any
                time.
                We wish you have a nice day,
                ".$conseiller->name."
                Clear Path Canada advisor
                ".$conseiller->phone."
                +212 7 02 05 09 09
                ".$conseiller->email;
           }

        }
        elseif ($this->email == "2" && $this->customer_case->program_id==1) {
            $this->message = "Bonjour Mer ".$this->customer_case->customer->firstname .",<br><br>
                        Comme convenu je vous fais part de la procédure ainsi que les ressources pour préparer vos tests d'anglais et français. <br>Il faudra s'inscrire aux tests dès que possible et se préparer pour l'examen. Enfin, une fois que vous avez les résultats des tests vous pouvez nous les transmettre.<br><br>
                        Voilà le lien pour des informations sur le TEF Canada et TCF Canada (Frais de 3000 DHS par personne) <br>https://www.lefrancaisdesaffaires.fr/centres/maroc/<br>
                        Voilà le lien pour des information sur le IELTS (General)<br>
                        https://www.ielts.org/book-a-test/find-a-test-location/location-list/morocco/tv (Frais de 3000 DHS par personne)<br>
                        Ressource de préparation pour le IELTS (General), TEF Canada, TCF<br>
                        IELTS <br>
                        https://drive.google.com/drive/folders/1xqbB3ImVlC8f-FJKRXQ5Ynjs_GEkfKcS?usp=sharing
                        TEF<br>
                        https://drive.google.com/drive/folders/1Cz2ZFUA3nMPeooPqbVN-AKUgJLPkVVW9<br>
                        TCF<br>
                        https://drive.google.com/drive/folders/1x9EjLe4qhrIMmf2JRFZzWjMFt67pR1jH<br>
                        <br>
                        Je vous souhaite une bonne préparation.<br>
                        Cordialement,<br>
                        ";
        }
        else {
            $this->message = "";
        }
    }
}
