<?php

namespace App\Http\Livewire\Block\Visa;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Livewire\Component;
use Livewire\WithFileUploads;

class Soumission extends Component
{
    use WithFileUploads;
    public $documents;
    public $customer_case;
    public $soumission;
    public $docs;
    public $currentRouteName;

    public function mount($customer_case, $soumission)
    {
        // case-edit - soumission-view
        $this->currentRouteName = Route::currentRouteName();
        $this->soumission = $soumission;
        $this->customer_case = $customer_case;
        if ($soumission) {
            $substep = \App\Models\ProgramSubStep::find(29);
            // dd($substep);
            // $step = \App\Models\ProgramStep::where("id", $substep->step_id)->first();
            $this->docs = $substep->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot('order')->orderBy('order', 'asc')->get()->toArray();
            // dd($this->docs);
        } else {
            $this->docs = DB::table('document_types as dt')
                ->leftJoin('document_type_program_sub_step as dp', 'dp.document_type_id', '=', 'dt.id')
                ->whereIn('dp.program_sub_step_id', [32, 33])
                ->where('dp.program_id', $customer_case->program_id)
                ->orderBy('dp.order')
                ->get()
                ->toArray();
        }
    }
    public function updatedDocuments()
    {
        foreach ($this->documents as $pid => $document) {
            foreach ($this->customer_case->customer->getMedia($pid) as $media) $media->delete();
            $this->customer_case->customer
                ->addMedia($document)
                ->toMediaCollection($pid);
        }
        return redirect(request()->header('Referer'));
    }
    public function deleteMedia($collection)
    {
        foreach ($this->customer_case->customer->getMedia($collection) as $media) $media->delete();
        return redirect(request()->header('Referer'));
    }
    public function render()
    {
        return view('livewire.block.visa.soumission');
    }
}
