<?php

namespace App\Http\Livewire\Block;

use Livewire\Component;
use App\Models\CaseComment;

class CommentList extends Component
{
    public $user;
    public $comments;

    public function mount($user) {
        $this->user = $user;
        $this->comments = CaseComment::orderBy('created_at', 'DESC')->where('user_id', '=', $user->id)->get();
    }

    public function render()
    {
        return view('livewire.block.comment-list');
    }
}
