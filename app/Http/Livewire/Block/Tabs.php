<?php

namespace App\Http\Livewire\Block;

use App\Models\CustomerCase;
use App\Models\OrientationSchool;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Tabs extends Component
{
    public $customer_case;
    public $active;
    public $programs;
    public $schools;
    public $cprograms;
    public $cschools;
    public $groupedResultsArray;
    public $selected;
    public $choices;
    public $orientation_schools;

    protected $listeners = [
        'filterUpdated' => 'updateResults'
    ];

    public function mount(CustomerCase $customer_case)
    {
        $this->customer_case = $customer_case;
        $this->active = 1;
        $this->selected = array();
        $this->choices = array();
        $this->choices['programs'] = array();
        $this->choices['schools'] = array();
        // $this->programs = DB::table('orientation_schools')
        //     ->groupBy('programme')
        //     ->pluck('programme')
        //     ->toArray();

        $this->programs = DB::table('orientation_schools')
            // ->groupBy('programme')
            ->pluck('programme')
            ->toArray();
        $this->cprograms = count($this->programs);

        $this->schools = DB::table('orientation_schools')
            ->groupBy('nom_etablissement')
            ->pluck('nom_etablissement')
            ->toArray();
        $this->cschools = count($this->schools);

        $this->groupedResultsArray = [];
        $filter_columns = ['budget', 'langue', 'ville', 'session', 'diplome', 'type_etablissement', 'prix_total'];
        foreach($filter_columns as $column){
            $this->groupedResultsArray['admission_' . $column] = array();
            $this->selected['admission_' . $column] = array();
        }
        foreach ($this->customer_case->customer->schools()->get() as $item) {
            $this->choices['programs'][] = $item->programme;
        }

    }
    public function render()
    {
        return view('livewire.block.tabs');
    }
    public function toggleTab($tab) {
        $this->active = $tab;
    }

    public function updateResults($role, $column, $data) {
        if ($role == 'unselect') {
            $this->selected[$column] = array();
        }

        if($column=='admission_budget'){
            // dd($data);
            $this->selected['admission_budget'][] = $data;
        }else{
            foreach ($data as $selected) {
                if (array_key_exists($column, $this->selected) && !in_array($selected["text"], $this->selected[$column])) {
                    $this->selected[$column][] = $selected["text"];
                }
            }
        }
        // dd($this->selected);
        $dbquery = DB::table('orientation_schools')
            ->where(function($query) {
                foreach ($this->selected as $field_name => $values) {
                    if($field_name =='admission_budget' && !empty($values)){
                        continue;
                    }
                    $query->where(function($query) use ($field_name, $values) {
                        foreach ($values as $idx => $value) {
                            if ($idx >= 1) {
                                $query->orWhere(str_replace('admission_', '', $field_name), 'LIKE', "%".$value."%");
                                /*$query->orWhere(function($query) use ($field_name, $value) {
                                });*/

                            } else {
                                $query->where(str_replace('admission_', '', $field_name), 'LIKE', "%".$value."%");
                                /*$query->where(function($query) use ($field_name, $value) {
                                });*/
                            }
                        }
                    });
                }
                if(array_key_exists('admission_budget', $this->selected)){
                    $budget = empty($this->selected['admission_budget']) ? 0 : $this->selected['admission_budget'][0];
                    $query->orWhere(DB::raw("convert(budget , signed)"), "<=", $budget);
                }
            });
        // dd((clone $dbquery)->groupBy('programme')->get());
        // $this->programs = (clone $dbquery)->groupBy('programme')
        //     ->pluck('programme')
        //     ->toArray();
        $this->programs = (clone $dbquery)->groupBy('programme')
            ->pluck('programme')
            ->toArray();
        // dd($this->programs);
        $this->schools = (clone $dbquery)->groupBy('nom_etablissement')
            ->pluck('nom_etablissement')
            ->toArray();
        $this->cprograms = count($this->programs);
        $this->cschools = count($this->schools);
    }
    public function selectProgram($programId) {
        $program = OrientationSchool::find($programId);
        $text = $program->programme;
        if (in_array($text, $this->choices['programs'])) {
            $key = array_search($text, $this->choices['programs']);
            unset($this->choices['programs'][$key]);
        } else {

            $this->choices['programs'][] = $text;

        }
    }
    public function selectSchool($text) {
        if (in_array($text, $this->choices['schools'])) {
            $key = array_search($text, $this->choices['schools']);
            unset($this->choices['schools'][$key]);
        } else {
            $this->choices['schools'][] = $text;
        }
    }
    public function saveChoices() {
        $ids = [];
        /*foreach ($this->choices['schools'] as $school) {
            $s = OrientationSchool::where("nom_etablissement", 'LIKE', "%".$school."%")->first();
            if ($s && !in_array($s->id, $ids)) {
                $ids[] = $s->id;
            }
        }*/
        foreach ($this->choices['programs'] as $program) {
            $s = OrientationSchool::where("programme", 'LIKE', "%".$program."%")->first();
            if ($s && !in_array($s->id, $ids)) {
                $ids[] = $s->id;
            }
        }
        // dd($ids);
        $this->customer_case->customer->schools()->sync($ids);
        session()->flash('message', 'prorams successfully updated.');
        return redirect()->to('/sheets/'.$this->customer_case->id.'/4');
    }
}
