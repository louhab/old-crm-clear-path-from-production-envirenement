<?php

namespace App\Http\Livewire\Block\Customer;

use Livewire\Component;
use App\Models\Service;
use App\Models\ProgramSubStep;
use App\Models\SubStepState;

class Meet extends Component
{
    public $customer_case;
    public $title;
    public $meet;
    public $lang;
    public $document_to_consult;
    public $document_to_upload;
    public $state;
    public $substeps;
    public $sheet_steps;
    private $states =  [3, 5, 10, 20, 29];
    public $steps  =  [2, 3, 4, 5, 6, 7];

    public function mount($customer_case, $meet, $lang = null)
    {
        $this->customer_case = $customer_case;
        $this->meet = $meet;
        $this->lang = $lang;
        $this->sheet_steps = [];

        if ($customer_case->program_id === 4) $values = [5, 6, 7, 8];
        else $values = [1, 2, 3, 4];

        if ($meet !== "R6")
            $this->title = Service::where("program_payment_id", intval(str_replace("R", "", $meet)))->whereIn("phase_id", $values)->first()[$lang === "fr" ? "name" : "name_en"];
        else
            $this->title = "R6";

        switch ($meet) {
            case 'R6':
            case 'R5':
                $this->steps = [22]; // [11, 12, 13];
                $this->substeps = [ProgramSubStep::find(29)];
                $this->state = SubStepState::find(20);
                break;

            case 'R4':
                $this->steps = [18, 21, 22, 23]; // [11, 12, 13];
                $this->substeps = [ProgramSubStep::find(5)];
                if ($customer_case->program_id == 4)
                    $this->substeps = [ProgramSubStep::find(9), ProgramSubStep::find(22)];
                $this->state = SubStepState::find(20);
                break;

            case 'R3':
                // $this->title = $lang === "fr" ? "Rencontre 3: Préparation de la demande d’admission" : "3";
                $this->steps = [10, 11, 12, 13]; // [11, 12, 13];
                if ($customer_case->program_id === 4) {
                    $this->substeps = [ProgramSubStep::find(5)];
                    $this->state = SubStepState::find(5);
                } else {
                    $this->substeps = [ProgramSubStep::find(17)];
                    $this->state = SubStepState::find(9);
                }
                $this->sheet_steps = [11, 12, 13];

                break;
            case 'R2':
                if ($customer_case->program_id == 4) {
                    // $this->title = $lang === "fr" ? "Rencontre 2: Orientation pour la demande d’admission" : "Meeting 2: Preparation of the admission request";
                    $this->steps = [10, 11, 12, 13];
                    $this->substeps = [ProgramSubStep::find(5)];
                } else {
                    // $this->title = $lang === "fr" ? "Rencontre 2: Préparation de la demande d’équivalence et les tests de langues" : "Meeting 2: Preparation of the equivalence request and language tests";
                    $this->steps = [10];
                    $this->substeps = [ProgramSubStep::find(9)];
                }
                $this->sheet_steps = [11, 12, 13];
                $this->state = SubStepState::find(5);
                break;
            case 'R1':
                // $this->title = $lang === "fr" ? "Rencontre 1: Conseil" : "MEETING 1: ADVICE";
                // $this->sheet_steps = [11, 12, 13];
                // $this->steps = [3, 4, 5, 6, 7];
                $this->substeps = [ProgramSubStep::find(6), ProgramSubStep::find(8)];
                $this->state = SubStepState::find(3);
                // $step = \App\Models\ProgramStep::where("id", $this->substep->step_id)->first();
                if ($customer_case->customer->is_client) {
                    $docs = $this->substeps[0]->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot('order')->where('customer_owner', 1)->orderBy('order', 'asc')->get();
                    foreach ($docs as $documentType) {
                        if ($customer_case->customer->requestedDocuments()->wherePivot('document_type_id', $documentType->id)->get()->isEmpty()) {
                            $customer_case->customer->requestedDocuments()->attach([
                                $documentType->id => ['program_step_id' => $this->substeps[0]->step_id]
                            ]);
                        }
                    }
                }
                break;
        }
    }

    public function confirmProgram($program)
    {
        if ($this->meet == "R3") {
            if ($program["pivot"]["is_confirmed"] == 0) {
                $this->customer_case->customer->courses()->updateExistingPivot($program['id'], array('is_confirmed' => 1), false);
                if ($this->customer_case->customer->admission_state == 2) {
                    $this->customer_case->customer->update(["admission_state" => 6]);
                }
            } else {
                $this->customer_case->customer->courses()->updateExistingPivot($program['id'], array('is_confirmed' => 0), false);
            }
            return redirect(request()->header('Referer'));
        }
        // $this->program = $this->customer_case->customer->courses()->wherePivot("program_course_id", $program['id'])->withPivot('admis', 'is_confirmed')->first();
        // dd($this->program);
        // $this->isConfirmed = $this->program->pivot->is_confirmed;
        // $this->emit("programConfirmed");
    }

    public function render()
    {
        return view('livewire.block.customer.meet');
    }
}
