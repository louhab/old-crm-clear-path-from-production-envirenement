<?php

namespace App\Http\Livewire\Block;

use App\Models\CustomerCase;
use App\Models\DocumentType;
use App\Models\PaymentMethod;
use App\Models\ProgramPayment;
use App\Models\ProgramSubStep;
use App\Models\SubStepState;
use App\Models\CaseState;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Payment extends Component
{
    public $customer_case;
    public $payment_step;
    public $payment_amount;
    public $paymentMethod;
    public $pms;
    public $case_state;
    public $recu = [];
    public $bill = [];
    public $hasContrat;
    public $payment;
    public $states;
    public $options;

    public function mount(CustomerCase $customer_case, $payment)
    {
        $this->customer_case = $customer_case;
        $this->payment = $payment;
        $program_payment = ProgramPayment::find($payment);

        // if ($payment == 6)
        //     dd($program_payment);

        // Get payment Options
        $this->options = $program_payment->options()->where('program_id', $customer_case->program_id == 4 ? 1 : 2)->where('optional', 1)->get();

        // getting the payments of the user
        $this->pms = DB::table('program_payment_customer as pc')
            ->where("pc.customer_id", $this->customer_case->customer_id)
            ->where("pc.program_payment_id", $payment)
            ->whereNotNull('performed_at')
            ->whereNotNull('payment_method')
            ->whereNotIn("pc.discount_percent", [0])
            ->select("pc.*", "pc.amount", "pc.discount_percent as discount")
            ->get()
            ->toArray();
        $pid = 4;

        if ($this->customer_case->program_id != 4) {
            $pid = 1;
        }
        $this->payment_step = (array) DB::table('program_payment_program_step as ps')
            ->where("ps.program_payment_id", $payment)
            ->where("ps.program_id", $pid)
            ->where("ps.currency_id", $this->customer_case->customer->currency_id)
            ->first();

        // dd($this->payment_step);
        // second step

        $this->states = [0, 3, 5, 10, 20, 29, 30];
        $this->case_state = CaseState::where("sub_step_state_id", $this->states[$payment])
            ->where("customer_case_id", $customer_case->id)
            ->first();


        // $methods = PaymentMethod::all();
        $this->paymentMethod = PaymentMethod::find($this->case_state->payment_method);

        $doc = DocumentType::where('name_fr', 'Reçu')->first();
        $doc_ = DocumentType::where('name_fr', 'Facture')->first();
        $this->recu["doc"] = $doc;
        $this->bill["doc"] = $doc_;

        $payment_name = SubStepState::find($this->states[$payment])->name_fr;
        $recu = $payment_name . $doc->collection_name;
        $bill = $payment_name . $doc_->collection_name;
        $media = $customer_case->customer->getFirstMedia($recu);
        if (empty($media)) {
            $recu = $doc->collection_name;
            $media = $customer_case->customer->getFirstMedia($recu);
        }
        $this->recu["collection"] = $recu;
        $this->recu["media"] = $media;
        $media_ = $customer_case->customer->getFirstMedia($bill);
        if (empty($media_)) {
            $bill = $doc_->collection_name;
            $media_ = $customer_case->customer->getFirstMedia($bill);
        }
        $this->bill["collection"] = $bill;
        $this->bill["media"] = $media_;
        // --
        // $substep = ProgramSubStep::find(6);
        // $doc = $substep->docs()->wherePivot('program_id', '=', $pid)->withPivot('order')->orderBy('order', 'asc')->get()->last();
        $doc = DocumentType::find(5);
        $this->hasContrat = !empty($customer_case->customer->getFirstMedia($doc->collection_name));
        if ($customer_case->customer->lead->to_office == "conference") {
            $this->hasContrat = true;
        }
    }
    public function delete()
    {
        $step_state = SubStepState::find($this->states[$this->payment]);
        $doc_recu = DocumentType::where('name_fr', 'Reçu')->first();
        $doc_bill = DocumentType::where('name_fr', 'Facture')->first();
        foreach ($this->customer_case->customer->getMedia($step_state->name_fr . $doc_recu->collection_name) as $media) $media->delete();
        foreach ($this->customer_case->customer->getMedia($step_state->name_fr . $doc_bill->collection_name) as $media) $media->delete();
        $this->case_state->status = 0;
        $this->case_state->save();
        // payment_method
        $this->customer_case->customer->payments()->sync([
            $this->payment => [
                'performed_at' => null,
                'payment_method' => null
            ]
        ]);
        $this->emit('paymentDeleted', $this->payment);
    }
    public function render()
    {
        return view('livewire.block.payment');
    }
}
