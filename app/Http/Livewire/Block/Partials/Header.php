<?php

namespace App\Http\Livewire\Block\Partials;

use Livewire\Component;

class Header extends Component
{
    public $meet;

    public function mount($meet) {
        $this->meet = $meet;
    }
    public function render()
    {
        return view('livewire.block.partials.header');
    }
}
