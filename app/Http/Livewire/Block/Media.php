<?php

namespace App\Http\Livewire\Block;

use App\Models\Calendrier;
use App\Models\CustomerCase;
use App\Models\InformationSheet;
use App\Models\Lead;
use App\Models\LeadStateLog;
use App\Models\LeadStatus;
use App\Models\ProgramSubStep;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Media extends Component
{
    public $customer_case;
    public $informationSheet;
    public $docs;
    public $substep;
    public $checkClear;
    public $groupedResultsArray;
    public $filled;
    public $sheet_visited;
    public $currentRouteName;

    protected $listeners = ["modalEdited" => "toggleVisitedModal"];

    public function mount(CustomerCase $customer_case)
    {
        $this->currentRouteName = \Route::currentRouteName();
        $this->customer_case = $customer_case;
        // FIXME
        if ($this->customer_case->program_id == 4 || $this->customer_case->program_id == 31 || $this->customer_case->program_id == 32)
            $this->informationSheet = InformationSheet::find(1);
        else
            $this->informationSheet = InformationSheet::find(7);

        // dd($this->informationSheet);

        $this->sheet_visited = false;
        $sheet_pivot = $customer_case->customer->sheets()->wherePivot('information_sheet_id', '=', $this->informationSheet->id);

        if (count($sheet_pivot->get())) {
            if (count($sheet_pivot->wherePivot('user_updated', '!=', null)->get())) {
                $this->sheet_visited = true;
            }
        } else {
            $customer_case->customer->sheets()->attach([
                $this->informationSheet->id => [
                    'can_edit' => true,
                    'user_updated' => null
                ]
            ]);
        }

        // end modal visibility
        $this->substep = ProgramSubStep::where("id", 6)->first();
        if ($customer_case->program_id == 5) {
            $pid = 1;
        } else {
            $pid = $customer_case->program_id;
        }
        // $this->docs = $this->substep->docs()->wherePivot('program_id', '=', $pid)->withPivot('order')->where('customer_owner', 0)->where('name_fr', 'not like', 'CV')->orderBy('order', 'asc')->get();
        $this->docs = $this->substep->docs()->wherePivot('program_id', '=', $pid)->withPivot('order')->where('customer_owner', 0)->where('name_fr', 'not like', 'CV')->orderBy('order', 'asc')->get();
        // dd($this->docs);
        // dd($customer_case->customer->getMedia($this->docs[0]->collection_name)->count());
        $this->checkClear = $this->statusClear();
        // check fields
        $form = \App\Models\ProgramSubStep::where("id", 1)->first();
        $groupedResultsArray = [];
        $filledArray = [];
        if ($customer_case->program_id == 5) {
            $pid = 1;
        } else {
            $pid = $customer_case->program_id;
        }
        foreach ($form->fields()->wherePivot('program_id', '=', $pid)->withPivot(['required', 'order'])->where('customer_field_group_id', 1)->orderBy('order')->get() as $field) {
            $field_name = $field->field_name;
            $groupedResultsArray[$field->customer_field_group_id][] = $field;
            $filledArray[$field_name] = !is_null($customer_case->customer->$field_name) && !empty($customer_case->customer->$field_name);
        }
        $this->groupedResultsArray = $groupedResultsArray;
        // dd($this->groupedResultsArray, $filledArray);
        $filled = array_filter($filledArray, function ($v, $k) {
            return $v;
        }, ARRAY_FILTER_USE_BOTH);
        $this->filled =  count($filled) == count($filledArray);
    }

    private function statusClear()
    {
        $states = \App\Models\LeadStatus::all();
        $newStates = $states->filter(function ($state) {
            return $this->customer_case->customer->lead->lead_status_id && $state->id == $this->customer_case->customer->lead->lead_status_id && ($state->status_label_fr == "Contrat" || $state->status_label_fr == "Rencontre 1");
        });
        $ms = \App\Models\Calendrier::all()->filter(function ($meet) {
            return $meet->customer_id == $this->customer_case->customer->id && (str_starts_with($meet->title, "Recontre 1") || str_starts_with($meet->title, 'Rencontre 1') || str_starts_with($meet->title, str_replace('Rencontre ', 'R', 'Rencontre 1')));
        });
        return ($this->customer_case->customer->lead->to_office == 'office') || ($newStates->count() || $ms->count());
    }

    public function render()
    {
        return view('livewire.block.media');
    }

    public function toggleVisitedModal()
    {
        $sheet_pivot = $this->customer_case->customer->sheets()->wherePivot('information_sheet_id', '=', $this->informationSheet->id);
        if (count($sheet_pivot->get())) {
            $this->customer_case->customer->sheets()->updateExistingPivot($this->informationSheet->id, [
                'can_edit' => true,
                'user_updated' => Carbon::now()
            ]);
        } else {
            $this->customer_case->customer->sheets()->attach([
                $this->informationSheet->id => [
                    'can_edit' => true,
                    'user_updated' => Carbon::now()
                ]
            ]);
        }
        $this->sheet_visited = false;
    }

    public function toggleStatus($status)
    {
        $lead = Lead::find($this->customer_case->customer->lead_id);
        $new_state = 0;
        $old_state = 0;
        switch ($status) {
            case 'Admissible':
                // if($lead->)
                /*$meets = Calendrier::all()->filter(function ($meet) {
                    return $meet->customer_id == $this->customer_case->customer->id && (str_starts_with($meet->title, 'Rencontre 1') || str_starts_with($meet->title, str_replace('Rencontre ', 'R', 'Rencontre 1'))) ;
                });*/
                $old_state = $lead->lead_status_id;
                if ($this->customer_case->customer->payments()->withPivot('performed_at')->wherePivot('program_payment_id', 1)->wherePivot('performed_at', '!=', null)->count() == 0) {
                    $new_state = 7;
                    $lead->lead_status_id = 7;
                } else {
                    $new_state = LeadStatus::Contrat;
                    $this->customer_case->customer->lead_status = LeadStatus::Contrat;
                    $this->customer_case->customer->lead->update(array('lead_status_id' => LeadStatus::Contrat));
                    $this->customer_case->customer->save();
                }
                break;
            case 'Inadmissible':
                $old_state = $lead->lead_status_id;
                $new_state = 8;
                $lead->lead_status_id = 8;
                break;
        }
        $lead->save();
        if ($new_state != 0 && $old_state != $new_state) {
            LeadStateLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'state_id' => $lead->lead_status_id,
            ]);
        }
        return redirect(request()->header('Referer'));
    }
}
