<?php

namespace App\Http\Livewire\Block;

use App\Models\Calendrier;
use App\Models\CustomerCase;
use App\Models\User;
use Livewire\Component;

class Meet extends Component
{
    public $customer_case;
    public $prefix;
    public $short_prefix;
    public $en_prefix;
    public $meets;
    public $conseillerName;
    private $states =  [3, 5, 10, 20, 29];
    public $payment_status;
    public $meet_is_done;

    public function mount(CustomerCase $customer_case, $prefix, $en_prefix)
    {
        $this->customer_case = $customer_case;
        $this->prefix = $prefix;
        $this->short_prefix = str_replace("Rencontre ", "r", $prefix);
        $this->en_prefix = $en_prefix;
        $conseiller = User::find($customer_case->customer->lead->conseiller_id);
        $this->conseillerName = $conseiller ? $conseiller->name : '--';
        $this->meets = Calendrier::all()->filter(function ($meet) {
            $a = str_replace('Rencontre ', 'R', $this->prefix);
            return $meet->customer_id == $this->customer_case->customer->id && (str_starts_with($meet->title, $this->prefix) || str_starts_with($meet->title, $a) || str_starts_with($meet->title, 'RDV ' . $a));
        });
        $index = intval(str_replace('Rencontre ', '', $this->prefix));
        $this->meet_is_done = false;

        switch ($this->short_prefix) {
            case "r1":
                if ($customer_case->customer->r1) $this->meet_is_done = true;
                break;
            case "r2":
                if ($customer_case->customer->r2) $this->meet_is_done = true;
                break;
            case "r3":
                if ($customer_case->customer->r3) $this->meet_is_done = true;
                break;
            case "r4":
                if ($customer_case->customer->r4) $this->meet_is_done = true;
                break;
            case "r5":
                if ($customer_case->customer->r5) $this->meet_is_done = true;
                break;
        }

        if ($index == 1)
            $this->payment_status = 1;
        else {
            $payment = \App\Models\CaseState::where("sub_step_state_id", $this->states[$index - 2])
                ->where("customer_case_id", $customer_case->id)
                ->first();
            $this->payment_status = $payment->status;
        }
    }

    public function render()
    {
        return view('livewire.block.meet');
    }
}
