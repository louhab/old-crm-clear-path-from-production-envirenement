<?php

namespace App\Http\Livewire\Block\Admission;

use App\Models\AdmissionUrl;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithFileUploads;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Program extends Component
{
    use WithFileUploads;
    public $documents;
    public $urls;
    public $programs;
    public $customer_case;
    public $currentRouteName;

    public function mount($customer_case) {
        $this->customer_case = $customer_case;
        $this->currentRouteName = \Route::currentRouteName();
        // dd($this->currentRouteName);
        $this->urls = [];
        $this->programs = [];
        $admissions = AdmissionUrl::where('customer_id', $this->customer_case->customer_id)->get();
        foreach ($admissions as $admission) {
            $this->urls[$admission->admission_id] = $admission->url;
            $this->programs[$admission->admission_id] = $admission->program;
        }
    }
    public function updatedDocuments()
    {
        // dd($this->documents);
        foreach ($this->documents as $collection => $document) {
            foreach ($this->customer_case->customer->getMedia($collection) as $media) $media->delete();
            $this->customer_case->customer
                ->addMedia($document)
                ->withCustomProperties(['visible_for_customer' => 1])
                ->toMediaCollection($collection);
        }
        return redirect(request()->header('Referer'));
    }
    public function deleteMedia($collection){
        foreach ($this->customer_case->customer->getMedia($collection) as $media) $media->delete();
        return redirect(request()->header('Referer'));
    }
    public function toggleProperty($mediaId){
        $media = Media::find($mediaId);
        // dd($media->getCustomProperty('visible_for_customer'), !$media->getCustomProperty('visible_for_customer'));
        $media->setCustomProperty('visible_for_customer', !$media->getCustomProperty('visible_for_customer'));
        $media->save();
        // dd($media->getCustomProperty('visible_for_customer'));
        return redirect(request()->header('Referer'));
    }
    public function updateURL($admission){
        // dd($this->urls);
        // program
        $url = AdmissionUrl::where('admission_id', $admission)->where('customer_id', $this->customer_case->customer_id)->first();
        if ($url) {
            $url->update(['url' => $this->urls[$admission], 'program' => $this->programs[$admission]]);
        } else {
            if (!isset($this->programs[$admission]))
                $this->programs[$admission] = '';
            if (!isset($this->urls[$admission]))
                $this->urls[$admission] = '';
            $url = AdmissionUrl::create([
                'admission_id' => $admission,
                'customer_id' => $this->customer_case->customer_id,
                'url' => $this->urls[$admission],
                'program' => $this->programs[$admission]
            ]);
        }
        if (!empty($url->program)) {
            // lien + suivie = ouvert
            if ($this->customer_case->customer->admission_state == 2) {
                $this->customer_case->customer->update(["admission_state" => 6]);
            }
            // lien + comment + admission = ouvert
            // $canal = 'soumission-admission-' . $admission
            /*if ($this->customer_case->customer->admission_state == 1) {
                $comments = $this->customer_case->comments()->wherePivot('canal', '=', 'soumission-admission-' . $admission)->count();
                if ($comments > 0)
                    $this->customer_case->customer->update(["admission_state" => 6]);
            }*/
        }
        $this->customer_case->customer->update(array('soumission_processed_message' => 'admission lien/nom modifié', 'soumission_processed_at' => Carbon::now()));
        $this->customer_case->customer->update(array('processed_message' => 'admission lien/nom modifié', 'processed_at' => Carbon::now()));
        return redirect(request()->header('Referer'));
    }
    public function render()
    {
        return view('livewire.block.admission.program');
    }
}
