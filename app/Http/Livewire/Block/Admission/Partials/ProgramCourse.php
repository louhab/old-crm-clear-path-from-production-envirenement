<?php

namespace App\Http\Livewire\Block\Admission\Partials;

use App\Models\CustomerCase;
use Illuminate\Support\Facades\Route;
use Livewire\Component;

class ProgramCourse extends Component
{
    public $customer_case;
    public $programId;
    public $currentRouteName;

    public function mount(CustomerCase $customer_case, $programId) {
        $this->customer_case = $customer_case;
        $this->programId = $programId;
        $this->currentRouteName = Route::currentRouteName();
    }
    public function updateProgram($program, $value) {
        if ($value == 'Admis') {
            $this->customer_case->customer->courses()->updateExistingPivot($program['id'], array('admis' => 1), false);
        } elseif ($value == 'Refus') {
            $this->customer_case->customer->courses()->updateExistingPivot($program['id'], array('admis' => 0), false);
        } elseif ($value == 'Reset') {
            $this->customer_case->customer->courses()->updateExistingPivot($program['id'], array('admis' => null), false);
        }
        // foreach ($this->customer_case->customer->getMedia($program['id'].".Lettre Acceptation") as $media) $media->delete();
    }
    public function render()
    {
        return view('livewire.block.admission.partials.program-course');
    }
}
