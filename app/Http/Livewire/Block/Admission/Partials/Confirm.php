<?php

namespace App\Http\Livewire\Block\Admission\Partials;

use App\Models\CustomerCase;
use Illuminate\Support\Facades\Route;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Illuminate\Support\Facades\DB;

class Confirm extends Component
{
    public $customer_case;
    public $programId;
    public $program;
    public $currentRouteName;
    public $loopIndex;
    public $isConfirmed;
    // protected $listeners = ["confirmCourseProgram" => "confirmCourseProgram"];

    public function mount(CustomerCase $customer_case, $programId, $loopIndex)
    {
        $this->customer_case = $customer_case;
        $this->programId = $programId;
        $this->program = $this->customer_case->customer->courses()->wherePivot("program_course_id", $programId)->withPivot('admis', 'is_confirmed')->first();
        $this->isConfirmed = $this->program->pivot->is_confirmed;
        $this->loopIndex = $loopIndex;
        $this->currentRouteName = Route::currentRouteName();
    }
    public function confirmCourseProgram($program)
    {
        dd($program);
        if ($program != $this->programId)
            return true;
        $program = $this->customer_case->customer->courses()->wherePivot("program_course_id", $program)->withPivot('admis', 'is_confirmed')->first()->toArray();
        if ($program["pivot"]["is_confirmed"] == 0) {
            $this->customer_case->customer->courses()->updateExistingPivot($program, array('is_confirmed' => 1), false);
        } else {
            $this->customer_case->customer->courses()->updateExistingPivot($program, array('is_confirmed' => 0), false);
        }
    }
    public function confirmProgram($program)
    {
        if ($program["pivot"]["is_confirmed"] == 0) {
            if (count($this->customer_case->customer->courses()->wherePivot('is_confirmed', 1)->get()) >= 1) {
                // todo ❌ should fix this
                // DB::table('program_payment_customer')->insert([
                //     'customer_id' => $this->customer_case->customer->id,
                //     'program_payment_id' => 11,
                //     'discount_percent' => 0,
                //     'amount' => 1000,
                //     'currency_id' => 1,
                //     'performed_at' => \Carbon\Carbon::now(),
                //     'payment_method' => 4,
                //     "conseiller_id" => $this->customer_case->customer->lead->conseiller_id
                // ]);
            }
            $this->customer_case->customer->courses()->updateExistingPivot($program['id'], array('is_confirmed' => 1), false);
            if ($this->customer_case->customer->admission_state == 2) {
                $this->customer_case->customer->update(["admission_state" => 6]);
            }
        } else {
            $this->customer_case->customer->courses()->updateExistingPivot($program['id'], array('is_confirmed' => 0), false);
        }
        $this->program = $this->customer_case->customer->courses()->wherePivot("program_course_id", $program['id'])->withPivot('admis', 'is_confirmed')->first();
        $this->isConfirmed = $this->program->pivot->is_confirmed;
        // $this->emit("programConfirmed");
    }
    public function toggleProperty($mediaId)
    {
        $media = Media::find($mediaId);
        $media->setCustomProperty('visible_for_customer', !$media->getCustomProperty('visible_for_customer'));
        $media->save();
        return redirect(request()->header('Referer'));
    }

    public function deleteMedia($collection)
    {
        foreach ($this->customer_case->customer->getMedia($collection) as $media) $media->delete();
        return redirect(request()->header('Referer'));
    }

    public function render()
    {
        return view('livewire.block.admission.partials.confirm');
    }

    public function safeCheck($to_look_for, $to_look_in)
    {
        if (array_key_exists('key', $to_look_in[$to_look_for]) && array_key_exists('value', $to_look_in[$to_look_for])) {
            return $to_look_in[$to_look_for]["value"];
        } else {
            if (count($to_look_in[$to_look_for]["values"])) return $to_look_in[$to_look_for]["values"][0];
            return "";
        }
        return "";
    }
}
