<?php

namespace App\Http\Livewire\Block\Admission;

use Illuminate\Support\Facades\Route;
use Livewire\Component;

class State extends Component
{
    public $customer_case;
    public $currentRouteName;

    public function mount($customer_case) {
        $this->customer_case = $customer_case;
        $this->currentRouteName = Route::currentRouteName();
    }

    public function render()
    {
        return view('livewire.block.admission.state');
    }
}
