<?php

namespace App\Http\Livewire\Block\Log;

use Livewire\Component;
use App\Models\AssignmentLog as ALog;
use App\Models\Lead;

class AssignmentLog extends Component
{
    public $lead;
    public $states;

    public function mount(Lead $lead)
    {
        $this->lead = $lead;
        $this->assignements = ALog::where('lead_id', $lead->id)->get();
    }
    public function render()
    {
        return view('livewire.block.log.assignment-log');
    }
}
