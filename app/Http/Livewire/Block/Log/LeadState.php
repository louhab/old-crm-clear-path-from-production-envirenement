<?php

namespace App\Http\Livewire\Block\Log;

use App\Models\Lead;
use App\Models\LeadStateLog;
use Livewire\Component;

class LeadState extends Component
{
    public $lead;
    public $states;

    public function mount(Lead $lead) {
        $this->lead = $lead;
        $this->states = LeadStateLog::where('lead_id', $lead->id)->get();
    }

    public function render()
    {
        return view('livewire.block.log.lead-state');
    }
}
