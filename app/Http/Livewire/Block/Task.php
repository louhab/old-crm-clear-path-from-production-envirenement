<?php

namespace App\Http\Livewire\Block;

use App\Models\ProgramTask;
use Livewire\Component;

class Task extends Component
{
    public $customer_case;
    public $canal;
    public $task;
    protected $rules = [
        'task.title' => 'required|string|min:1',
        'task.state' => 'required|boolean',
    ];

    public function mount($customer_case, $canal) {
        $this->customer_case = $customer_case;
        $this->canal = $canal;
        $task = ProgramTask::where("customer_id", $customer_case->customer->id)->where("canal", $canal)->first();
        if (!$task) {
            $task = ProgramTask::create([
                "customer_id" => $customer_case->customer->id,
                "canal" => $canal,
                "title" => $canal
            ]);
        }
        $this->task = $task;
        // dd($task->toArray());
        // $this->canal = $customer_case->customer->score;
    }

    public function updated($propertyName)
    {
        // dd($propertyName, $this->canal);
        /*$this->customer_case->customer->update(["score" => $this->score]);
        $this->addError('score', 'modification réussite!');*/
        $this->task->save();
    }

    public function render()
    {
        return view('livewire.block.task');
    }
}
