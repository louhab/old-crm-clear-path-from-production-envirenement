<?php

namespace App\Http\Livewire\Block;

use App\Models\CaseComment;
use App\Models\CustomerCase;
use Illuminate\Support\Facades\Route;
use Livewire\Component;

class Comment extends Component
{
    public $customer_case;
    public $limit;
    public $comments;
    public $total;
    public $canal;
    public $hidden;
    public $currentRouteName;

    protected $listeners = ["deleteComment" => "delete"];

    public function mount(CustomerCase $customer_case, $canal = null)
    {
        $this->customer_case = $customer_case;
        $this->limit = 5;
        $this->canal = $canal;
        $this->comments = $this->customer_case->comments()->wherePivot('canal', '=', $canal)->orderByDesc('case_comment_id')->limit($this->limit)->get();
        $this->total = $this->customer_case->comments()->wherePivot('canal', '=', $canal)->count();
        // hide backoffice notes pour les nouveau lead
        $this->hidden = true;
        /*if ($canal == "soumission" && count($this->comments) == 0)
            $this->hidden = false;*/
        // dd(count($this->comments));
        $this->currentRouteName = Route::currentRouteName();
        // dd($this->currentRouteName);
    }

    public function render()
    {
        return view('livewire.block.comment');
    }

    public function loadMore()
    {
        $this->limit += 5;
        $this->comments = $this->customer_case->comments()->wherePivot('canal', '=', $this->canal)->orderByDesc('case_comment_id')->limit($this->limit)->get();
    }

    public function delete($commentId)
    {
        $comment = CaseComment::find($commentId);
        // dd($comment);
        if (auth()->user()->isAdmin() || $comment->user_id == auth()->id() || auth()->user()->isVerificateur()) {
            $this->customer_case->comments()->detach($comment);
            // $comment->delete();
        }
        return redirect(request()->header('Referer'));
    }
    public function deleteComment($commentId)
    {
        // $this->dispatchBrowserEvent();
        // $comment = CaseComment::find($commentId);
        $comment = $this->customer_case->comments()
            ->withPivot("canal")
            ->where('case_comments.id', $commentId)
            ->first();

        if ($comment && $comment->pivot->canal == $this->canal) {
            // dd($commentId, $comment, $this->canal);
            if (auth()->user()->isAdmin() || $comment->user_id == auth()->id() || auth()->user()->isVerificateur()) {
                $this->customer_case->comments()->detach($comment->id);
                $comment->delete();
            }
            return redirect(request()->header('Referer'));
        }
    }
}
