<?php

namespace App\Http\Livewire\Customer\Partials;

use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Illuminate\Support\Str;

class Show extends Component
{
    public $customer_case;
    public $collectionName;
    public $media;
    public $signable;
    public $formId;
    public $media_name;

    public function mount($customer_case, $collectionName, $media, $media_name = null)
    {
        $this->media = $media;
        $this->customer_case = $customer_case;
        $this->collectionName = $collectionName;
        $this->signable = $media->getCustomProperty('signable');
        $this->formId = Str::random(10);
        $this->media_name = $media_name;
    }
    public function render()
    {
        return view('livewire.customer.partials.show');
    }
    public function deleteMedia($collection)
    {
        foreach ($this->customer_case->customer->getMedia($collection) as $media) $media->delete();
        return redirect(request()->header('Referer'));
    }
    public function toggleProperty($mediaId)
    {
        $media = Media::find($mediaId);
        // dd($media->getCustomProperty('visible_for_customer'), !$media->getCustomProperty('visible_for_customer'));
        $media->setCustomProperty('visible_for_customer', !$media->getCustomProperty('visible_for_customer'));
        $media->save();
        // dd($media->getCustomProperty('visible_for_customer'));
        return redirect(request()->header('Referer'));
    }
}
