<?php

namespace App\Http\Livewire\Customer\Partials;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\Customer;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Upload extends Component
{
    use WithFileUploads;
    public $document;
    public $customer_case;
    public $collectionName;
    public $from;

    public function mount($customer_case, $collectionName, $from = null) {
        $this->customer_case = $customer_case;
        $this->collectionName = $collectionName;
        $this->from = $from;


    }
    public function updatedDocument()
    {
        if (strpos($this->collectionName, "course")) {
            // les admission => program_tasks (no need)
            // les programe => program_courses (name)

            $chunks = explode("-", $this->collectionName);

            $doc = DB::table('program_courses')->selectRaw('name')
                ->where("id", "=", $chunks[2])
                ->first();

            if ($doc) {
                $case = Customer::where('id', $this->customer_case->customer->id)->first();
                $case->soumission_processed_message = $chunks[0] . " a été uploader pour " . $doc->name;
                $case->soumission_processed_at = Carbon::now();
                $case->save();

                $task = Task::where([
                    ['customer_case_id', $this->customer_case->id],
                    ["canal", "=", $chunks[1] . "-" . $chunks[2]]
                ])->first();

                if ($task) {
                    $task->bo_updated_at = Carbon::now();
                    $task->save();
                }
            }
        } else {
            $doc = DB::table('document_types')->selectRaw('name_fr')->where("collection_name", "=", $this->collectionName)->first();
            if ($doc) {
                $case = Customer::where('id', $this->customer_case->customer->id)->first();
                $case->soumission_processed_message = $doc->name_fr . " a été uploader";
                $case->soumission_processed_at = Carbon::now();
                $case->save();

                $task = Task::where([
                    ['customer_case_id', $this->customer_case->id],
                    ["canal", 'LIKE', "%" . $doc->name_fr . "%"]
                ])->first();

                if ($task) {
                    $task->bo_updated_at = Carbon::now();
                    $task->save();
                }
            }
        }

        foreach ($this->customer_case->customer->getMedia($this->collectionName) as $media) $media->delete();
        $this->customer_case->customer
            ->addMedia($this->document)
            ->withCustomProperties(['visible_for_customer' => 1])
            ->toMediaCollection($this->collectionName);

        return redirect(request()->header('Referer'));
    }
    public function render()
    {
        return view('livewire.customer.partials.upload');
    }
}
