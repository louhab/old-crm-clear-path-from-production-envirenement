<?php

namespace App\Http\Livewire\Elements;

use App\Models\CaseComment;
use Illuminate\Support\Facades\Route;
use Livewire\Component;

class Comment extends Component
{
    public $customer_case;
    public $canal;
    public $comments;
    public $currentRouteName;
    protected $listeners = [
        "deleteAdmissionComment" => "deleteComment",
        "updateAdmissionComment" => "updateComment"
    ];

    public function mount($customer_case, $canal)
    {
        $this->customer_case = $customer_case;
        $this->canal = $canal;
        $this->comments = $this->customer_case->comments()->wherePivot('canal', '=', $canal)->orderBy('created_at', 'DESC')->get();;
        $this->currentRouteName = Route::currentRouteName();
    }
    public function render()
    {
        return view('livewire.elements.comment');
    }

    public function updateComment($canal)
    {
        if ($canal == $this->canal) {
            $this->comments = $this->customer_case->comments()->wherePivot('canal', '=', $this->canal)->orderBy('created_at', 'DESC')->get();;
        }
    }
    public function deleteComment($commentId)
    {
        // $this->dispatchBrowserEvent();
        // $comment = CaseComment::find($commentId);
        $comment = $this->customer_case->comments()->withPivot("canal")->where('case_comments.id', $commentId)->first();
        // dd($comment);
        if ($comment && $comment->pivot->canal == $this->canal) {
            // dd($commentId, $comment, $this->canal);
            if (auth()->user()->isAdmin() || $comment->user_id == auth()->id() || auth()->user()->isVerificateur()) {
                $this->customer_case->comments()->detach($comment->id);
                $comment->delete();
            }
            return redirect(request()->header('Referer'));
        }
    }
}
