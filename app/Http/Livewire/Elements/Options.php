<?php

namespace App\Http\Livewire\Elements;

use Livewire\Component;

class Options extends Component
{
    public $customer_case;
    public $options;
    public $selected_options;
    public $payment;
    public $payment_id;

    protected $listeners = [
        'optionChanged' => 'toggle',
        'paymentDeleted' => 'updatePayment'
    ];

    public function mount($customer_case, $options, $payment) {
        $this->customer_case = $customer_case;
        $this->options = $options;
        $this->payment_id = $payment;
        // $this->payment = $payment;
        // if (count($options)) dd($customer_case->customer->options()->get());
        // $selectedOptions = ;
        $this->selected_options = $customer_case->customer->options()->get()->pluck('id')->toArray();
        $this->payment = $customer_case->customer->payments()->withPivot('performed_at')->wherePivot('program_payment_id', $payment)->first();
        if ($this->payment)
            $this->payment = $this->payment->toArray();
        // $this->selected_options = array_fill_keys($optionIds, true);
        // dd($optionIds);
    }

    public function updatePayment($payment) {
        if ($payment == $this->payment_id){
            $this->payment = $this->customer_case->customer->payments()->withPivot('performed_at')->wherePivot('program_payment_id', $payment)->first();
            if ($this->payment)
                $this->payment = $this->payment->toArray();
        }
    }
    public function toggle($optionId)
    {
        // $opt = $this->customer_case->customer->options()->wherePivot('service_id', $optionId)->first();
        if (in_array($optionId, $this->selected_options)) {
            $key = array_search($optionId, $this->selected_options);
            unset($this->selected_options[$key]);
        } else {
            $this->selected_options[] = $optionId;
        }
        $this->customer_case->customer->options()->sync($this->selected_options);
        // dd($this->customer_case->customer->options());
        // dd($this->selected_options);
    }
    public function render()
    {
        return view('livewire.elements.options');
    }
}
