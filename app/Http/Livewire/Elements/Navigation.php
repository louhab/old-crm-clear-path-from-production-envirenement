<?php

namespace App\Http\Livewire\Elements;

use Livewire\Component;

class Navigation extends Component
{
    public function render()
    {
        return view('livewire.elements.navigation');
    }
}
