<?php

namespace App\Http\Livewire\Elements;

use App\Models\Task as TaskModel;
use Livewire\Component;
use Illuminate\Support\Facades\Route;

class Task extends Component
{
    public $customer_case;
    public $canal;
    protected $task;
    public $currentRouteName;
    // public $empty_arr;
    // public $hasTaskMng;
    // public $hasTaskBo;

    // public $mng_tasks;
    // public $bo_tasks;

    // public $mng_empty;
    // public $bo_empty;


    public function mount($customer_case, $canal) {

        // Conseiller -> BO
        // A faire => À faire (tache-mng) form mng
        // Relance => Suivi à faire (suivi-mng)

        // BO -> Conseiller

        // A faire => Suivi à faire (tache-bo) from bo
        // Rejet fiche => Fiche non remplie (suivi-fiche)
        // Rejet documents => Documents incomplets (suivi-docs)


        $this->customer_case = $customer_case;
        $this->canal = $canal;

        $this->task = TaskModel::where([
            ["customer_case_id", $customer_case->id],
            ['canal', '=', $canal],
        ]
        )->first();

        if ($this->task) {
            $this->task->is_exists = 1;
            // $this->task = $this->task;
        } else {

            $this->task = (object) [
                "is_exists" => 0,
                "is_relaunched" => 0,
                "is_done" => 0
            ];
        }

        // var_dump($this->task);
        // die();


        $this->currentRouteName = Route::currentRouteName();

        // mng tasks
        // $this->empty = [];
        // $this->empty[] = $this->findKey($this->tasks, "note", "tache") ? [] : ["note" => "tache"];

        // $this->empty = array_map('array_filter', $this->empty);
        // $this->empty = array_filter($this->empty);

        // $this->hasTaskMng = $this->findKey($this->mng_tasks, "note", "tache-mng");

    }

    public function render()
    {
        return view('livewire.elements.task', ['task' => $this->task]);
    }

    private function findKey($array, $keySearch, $valSearch)
    {
        foreach ($array as $key => $item) {
            if ($item[$keySearch] === $valSearch) {
                return true;
            } elseif (is_array($item) && $this->findKey($item, $keySearch, $valSearch)) {
                return true;
            }
        }
        return false;
    }
}
