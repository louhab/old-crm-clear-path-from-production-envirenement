<?php

namespace App\Http\Livewire\Button;

use Livewire\Component;
use App\Models\CaseState;

class Soumission extends Component
{
    public $customer_case;
    public $admis;
    public $soumission;
    protected $listeners = [
        'soumissionCheck' => 'updateBtn',
    ];

    public function mount($customer_case) {
        $this->customer_case = $customer_case;
        $this->admis = in_array($customer_case->customer->admission_state, [1, 8]);
        $substep = \App\Models\ProgramSubStep::where('id', 15)->first();
        $substates = $substep->substates()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot('order')->orderBy('order', 'asc')->get();
        $this->soumission = true;
        // dd($substates);
        foreach($substates as $stat) {
            $case_state = CaseState::where("sub_step_state_id", $stat->id)
                ->where("customer_case_id", $customer_case->id)
                ->first();
            if($case_state->status == 0){
                $this->soumission = false;
                break;
            }
        }
    }
    public function updateBtn() {
        $this->admis = in_array($this->customer_case->customer->admission_state, [1, 8]);
    }
    public function render()
    {
        return view('livewire.button.soumission');
    }
}
