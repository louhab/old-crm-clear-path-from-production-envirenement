<?php

namespace App\Http\Livewire\Button;

use Livewire\Component;
use Livewire\WithFileUploads;
use Spatie\MediaLibrary\InteractsWithMedia;

class Media extends Component
{
    use WithFileUploads;
    public $document;
    public $customer_case;
    public $collection;
    public $lead;
    public $checkMedia;
    public $documentType;

    public function mount($customer_case, $collection) {
        $this->customer_case = $customer_case;
        $this->documentType = \App\Models\DocumentType::where("name_fr", $collection)->first();
        $this->collection = $this->documentType->collection_name;
        $this->checkMedia = $customer_case->customer->getMedia($this->collection)->count() >= 1;
        /*//
        $lead = \App\Models\Lead::where('id', $customer_case->customer->lead_id)->first();
        if ($collection == 'CV') {
            $this->collection = 'CVs';
            $this->checkMedia = $customer_case->customer->lead->cv_id;
        } else {
            $this->documentType = \App\Models\DocumentType::where("name_fr", $collection)->first();
            $this->collection = $this->documentType->collection_name;
            $this->checkMedia = $customer_case->customer->getMedia($this->collection)->count() >= 1;
        }
        $substep = \App\Models\ProgramSubStep::find(6);
        $docs = $substep->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot('order')->where('customer_owner', 1)->orderBy('order', 'asc')->get();*/
        // dd($docs->pluck('name_fr'));
    }

    public function updatedDocument()
    {
        /*$this->validate([
            'document' => 'pdf',
        ]);*/
        // visible_for_customer
        $this->customer_case->customer
            ->addMedia($this->document)
            ->withCustomProperties(['visible_for_customer' => 1])
            ->toMediaCollection($this->collection);
        return redirect(request()->header('Referer'));
    }

    public function save()
    {
        // ...
        dd('test');
    }
    public function delete($collection) {
        foreach ($this->customer_case->customer->getMedia($collection) as $media) $media->delete();
        return redirect(request()->header('Referer'));
    }
    public function render()
    {
        return view('livewire.button.media');
    }
}
