<?php

namespace App\Http\Livewire\Button;

use Livewire\Component;
use Livewire\WithFileUploads;
use Spatie\MediaLibrary\InteractsWithMedia;
use App\Models\ProgramPayment;
use Illuminate\Support\Facades\DB;

class Paiement extends Component
{
    use WithFileUploads;
    public $document;
    public $customer_case;
    public $collection;
    public $lead;
    public $checkMedia;
    public $documentType;

    public function mount($customer_case, $collection)
    {
        $this->customer_case = $customer_case;
        $this->documentType = \App\Models\DocumentType::where("name_fr", $collection)->first();
        // $this->collection = $this->documentType;
        $this->checkMedia = $customer_case->customer->getMedia($this->collection)->count() >= 1;
    }

    public function updatedDocument()
    {
        $this->customer_case->customer
            ->addMedia($this->document)
            ->withCustomProperties(['visible_for_customer' => 1])
            ->toMediaCollection($this->collection);
        // dd($this->document->getClientOriginalName());
        // dd($this->documentType->name_fr);

        // dd($this->customer_case->customer->id);
        $data = ProgramPayment::select("id")
            ->where('name_fr', $this->documentType->name_fr)
            ->first();
        // dd($this->documentType);
        // dd($data->id);
        $payment = DB::table('program_payment_customer as pc')
            ->select('pc.id')->where([
                'pc.program_payment_id' => $data->id,
                'pc.customer_id' => $this->customer_case->customer->id,
            ])->first();
        if ($payment) {
            $updatePayment = DB::table('program_payment_customer as pc')
                ->where([
                    'pc.program_payment_id' => $data->id,
                    'pc.customer_id' => $this->customer_case->customer->id,
                ])->update(
                    [
                        'proof' => $this->document->getClientOriginalName()
                    ]
                );
        }

        //     DB::table('users')
        //   ->where('id', 2)
        //   ->update(['name' =>DB::raw("'Heena Khan'")]);
        return redirect(request()->header('Referer'));
    }

    public function delete($collection)
    {
        foreach ($this->customer_case->customer->getMedia($collection) as $media) $media->delete();
        return redirect(request()->header('Referer'));
    }
    public function render()
    {
        return view('livewire.button.paiement');
    }
}
