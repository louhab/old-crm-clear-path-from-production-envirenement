<?php

namespace App\Http\Livewire\Button;

use App\Models\CustomerClear1000Fields;
use App\Models\LevelOfStudy;
use Carbon\Carbon;
use Livewire\Component;

class Document extends Component
{
    public $customerCase;
    public $allDocsExists;

    public function mount($customerCase) {
        $this->customerCase = $customerCase;
        $docs = [];
        $this->allDocsExists = true;
        if ($this->customerCase->customer->program_id == 4) {
            $clear = CustomerClear1000Fields::where('customer_id', $this->customerCase->customer_id)->first();
            if($clear && $clear->level_study) {
                //
                $level = LevelOfStudy::find($clear->level_study);
                $docs =  $level->docs()
                    ->withPivot(['order'])
                    ->orderBy('order')
                    ->get();
            }
            //$this->allDocsExists = true;
            if($docs){
                foreach($docs->pluck("name_fr", "collection_name") as $collection => $name) {
                    if($customerCase->customer->getMedia($collection)->count() == 0)
                        $this->allDocsExists = false;
                }
            }
        }
        $media_sheet = \App\Models\MediaSheet::find(2);
        $docs = $media_sheet->docs()
            ->wherePivot('program_id', '=', $this->customerCase->program_id)
            ->withPivot(['required', 'order'])
            ->orderBy('order')
            ->get();
        foreach($docs->pluck("name_fr", "collection_name") as $collection => $name) {
            if($customerCase->customer->getMedia($collection)->count() == 0)
                $this->allDocsExists = false;
        }
    }
    public function render()
    {
        return view('livewire.button.document');
    }
    public function confirmDocs(){
        if ($this->customerCase->customer->payments()->where('program_payments.id', 3)->first()){
            $this->customerCase->customer->update(['admission_state' => 1]);
            if (is_null($this->customerCase->customer->admis_at))
                $this->customerCase->customer->update(['admis_at' => Carbon::now()]);
            $this->emit('soumissionCheck');
        }
        //return redirect(route('case-edit', ['customer_case' => $this->customerCase]));
    }
}
