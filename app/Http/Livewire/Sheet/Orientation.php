<?php

namespace App\Http\Livewire\Sheet;

use App\Models\ProgramCourse;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Orientation extends Component
{
    public $form, $customer_case;
    public $groupedResultsArray;
    public $filter_columns;
    /*protected $listeners = [
        'programDeleted' => 'updateFilter',
    ];*/

    public function mount($form, $customer_case)
    {
        $this->form = $form;
        $this->customer_case = $customer_case;
        $this->groupedResultsArray = [];
        $filter_columns = ['prix_total', 'domaine_etude', 'test_langue','langue', 'ville', 'session', 'diplome', 'type_etablissement'];
        $this->filter_columns = $filter_columns;
        foreach($filter_columns as $column){
            $pf = \App\Models\ProgramCourseField::where('field_name', $column)->first();
            if ($pf) {
                $values = $pf->values;
            } else {
                $values = ProgramCourse::where('properties->' . $column . '->value', 'NOT LIKE', "")->select('properties->' . $column . '->value')->get()->toArray();
                $values = array_map(function ($item){
                    return array_values($item)[0];
                }, $values);
            }
            $values = array_unique($values);

            //dd($column, array_unique($values));
            if ($column == 'prix_total'){
                array_walk($values, array($this, 'test_alter'));
                sort($values);
            }
            /*else {
                asort($values);
            }*/
            $this->groupedResultsArray['admission_' . $column] = $values;
        }
        // $programs = ProgramCourse::pluck('name')->toArray();
        $programs = DB::table('program_courses')->selectRaw("TRIM(program_courses.name) as name")
        // ->where('visible', 0)
        ->groupBy(DB::raw('TRIM(program_courses.name)'))
        ->orderBy("name")
        ->get()->toArray();

        $i = 0;
        $final_programs = [];
        foreach ($programs as $key => $val) {
            $final_programs[$i] = $val->name;
            $i++;
        }

        // asort($programs);
        // dd($final_programs);
        $this->groupedResultsArray['admission_programme'] = $final_programs;
    }
    /*public function updateFilter(){
        $this->groupedResultsArray = [];
        foreach($this->filter_columns as $column){
            //$this->groupedResultsArray[$field->customer_field_group_id][] = $field;
            $values = DB::table('orientation_schools')
                ->where($column, 'NOT LIKE', "")
                ->groupBy($column)
                ->pluck($column)
                ->toArray();

            //dd($values);
            if ($column == 'prix_total'){
                array_walk($values, array($this, 'test_alter'));
                sort($values);
            } else {
                asort($values);
                // dd($values);
            }
            $this->groupedResultsArray['admission_' . $column] = $values;
            // dd($field->field_name);
        }
    }*/
    public function test_alter(&$item, $key)
    {
        $item = intval($item);
    }
    public function render()
    {
        return view('livewire.sheet.orientation');
    }
}
