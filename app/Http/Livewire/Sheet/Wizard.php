<?php

namespace App\Http\Livewire\Sheet;

use App\Models\Clear3WorkFields;
use App\Models\CustomerClear3Fields;
use App\Models\CustomerField;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Livewire\Component;

class Wizard extends Component
{
    public $form, $customer_case, $customer_clear;
    public $tabActive;
    public $sheet_id;
    public $groupedResultsArray;
    public $groupNames;
    public $grouping;
    public $filled;
    public $filledArray;
    public $groupsArray;
    public $lang;
    public $join;

    protected $listeners = [
        'fieldUpdated' => 'updateFilled',
        'fieldsDeleted' => 'initFilledArray'
    ];

    public function mount($form, $customer_case, $join = "Single")
    {
        // dd($join);
        // session(['join' => $join]);
        $this->join = $join;
        $this->form = $form;
        $this->lang = session()->get('locale') === 'en' ? "en" : "fr";
        // $this->count = 0;
        $this->customer_case = $customer_case;
        $this->tabActive = 1;
        $this->sheet_id = null;
        if ($this->form instanceof \App\Models\InformationSheet) {
            $this->sheet_id = $this->form->id;
        }
        //
        $this->groupedResultsArray = [];
        $this->groupNames = [];
        $this->grouping = [];
        $this->filledArray = [];
        $customer_clear = CustomerClear3Fields::where('customer_id', $this->customer_case->customer->id)
            ->where('join', $this->join)
            ->first();
        if (!$customer_clear) {
            $customer_clear = CustomerClear3Fields::create([
                'customer_id' => $this->customer_case->customer->id,
                'join' => $this->join
            ]);
        }
        $this->customer_clear = $customer_clear;
        foreach ($this->form->fields()->wherePivot('program_id', '=', $this->customer_case->program_id)->withPivot(['required', 'order'])->orderBy('customer_fields.id')->get() as $field) {
            $this->groupedResultsArray[$field->customer_field_group_id][] = $field;
            $this->groupNames[$field->customer_field_group_id] = $this->lang === "fr" ? $field->group->name_fr : $field->group->name_en;
            // $field_name = $field->field_name;
            /*if (is_null($customer_clear->$field_name) || empty($customer_clear->$field_name))
                $this->filledArray[$field->customer_field_group_id][$field_name] = false;
            else
                $this->filledArray[$field->customer_field_group_id][$field_name] = true;*/
        }
        //
        foreach ($this->groupNames as $idG => $nameG) {
            if ($idG == 2) {
                $this->grouping[$idG] = array('work', 'clear3_work_fields', 'App\Models\Clear3WorkFields');
                // $this->filledArray[2][$field_name] = false;
            } elseif ($idG == 3) {
                $this->grouping[$idG] = array('school', 'clear3_school_fields', 'App\Models\Clear3SchoolFields');
            }
        }
        $this->groupsArray[2]['work'] = array('clear3_work_fields', 'App\Models\Clear3WorkFields');
        $this->groupsArray[3]['school'] = array('clear3_school_fields', 'App\Models\Clear3SchoolFields');
        $this->groupsArray[4]['child'] = array('clear3_child_fields', 'App\Models\Clear3ChildFields');
        $this->groupsArray[4]['sibling'] = array('clear3_sibling_fields', 'App\Models\Clear3SiblingFields');
        $this->groupsArray[5]['past_residence'] = array('clear3_past_residence_fields', 'App\Models\Clear3PastResidenceFields');
        $this->groupsArray[5]['various_travel'] = array('clear3_various_travel_fields', 'App\Models\Clear3VariousTravelFields');

        $this->initFilledArray();
        // dd($this->filledArray);
        // $this->filledArray = $this->filled;
        // $this->updateFilledArray();
        //dd($this->filled, $this->filledArray, $this->sheet_id, $this->groupNames);
        //$this->emit('tabActive', ['tabId' => $this->tabActive]);
    }

    public function initFilledArray()
    {
        foreach ($this->form->fields()->wherePivot('program_id', '=', $this->customer_case->program_id)->withPivot(['required', 'order'])->orderBy('customer_fields.id')->get() as $field) {
            $field_name = $field->field_name;
            if (is_null($this->customer_clear->$field_name) || empty($this->customer_clear->$field_name))
                $this->filledArray[$field->customer_field_group_id][$field_name] = false;
            else
                $this->filledArray[$field->customer_field_group_id][$field_name] = true;
        }
        foreach ($this->groupsArray as $idG => $groups) {
            //dd($groups);
            $skip = array('id', 'customer_id', 'order', 'updated_at', 'created_at');
            foreach ($groups as $group) {
                foreach (DB::table($group[0])->where('customer_id', $this->customer_case->customer_id)->where('join', $this->join)->get() as $row) {
                    $columns = array_diff(Schema::getColumnListing($group[0]), $skip);
                    $field_order = $row->order;
                    foreach ($this->form->fields()->wherePivot('program_id', '=', $this->customer_case->program_id)->where('customer_fields.customer_field_group_id', $idG)->withPivot(['required', 'order'])->orderBy('customer_fields.id')->get() as $field) {
                        $field_name = $field->field_name;
                        if (in_array($field_name, $columns)) {
                            if (is_null($row->$field_name) || empty($row->$field_name))
                                $this->filledArray[$idG][$field_name . "." . $field_order] = false;
                            else
                                $this->filledArray[$idG][$field_name . "." . $field_order] = true;
                        }
                    }
                }
            }
        }
        $this->updateFilledArray();
    }
    private function updateFilledArray()
    {
        $this->filled = array_filter($this->filledArray, function ($v, $k) {
            $filled = array_filter($v, function ($v, $k) {
                return $v;
            }, ARRAY_FILTER_USE_BOTH);
            return count($filled) == count($v);
        }, ARRAY_FILTER_USE_BOTH);
        /*$groupIdArray = [1, 2, 3, 4, 5];
        $current = 0;
        foreach ($groupIdArray as $fid) {
            if ($current == 0 && !array_key_exists($fid, $this->filled)) {
                $this->filled[$fid] = "current";
                $current = $fid;
                continue;
            }
            if ($current > 0) {
                unset($this->filled[$fid]);
            }

        }*/
    }
    public function updateFilled($field_name, $field_group_id, $order = null, $group = null)
    {
        // $customer_clear = CustomerClear3Fields::where('customer_id', $this->customer_case->customer->id)->first();
        /*if (is_null($this->customer_clear->$field_name) || empty($this->customer_clear->$field_name))
            $this->filledArray[$field_group_id][$field_name] = false;
        else
            $this->filledArray[$field_group_id][$field_name] = true;*/
        if (is_null($order))
            $this->filledArray[$field_group_id][$field_name] = !is_null($this->customer_clear->$field_name) && !empty($this->customer_clear->$field_name);
        else {
            $customer_clear = DB::table($this->groupsArray[$field_group_id][$group][0])->where('customer_id', $this->customer_case->customer_id)->where('order', $order)->where('join', $this->join)->first();
            $this->filledArray[$field_group_id][$field_name . "." . $order] = !is_null($customer_clear->$field_name) && !empty($customer_clear->$field_name);
        }
        $this->updateFilledArray();
        // dd($this->filled);
        if ($this->sheet_id == 9)
            $this->emit('tabActive', ['tabId' => 1]);
        else
            $this->emit('tabActive', ['tabId' => $field_group_id]);
    }

    public function submit()
    {
        if (count($this->filled) == 5)
            return redirect()->to('/cases/' . $this->customer_case->id);
        elseif ($this->sheet_id == 9 && count($this->filled) == 1)
            return redirect()->to('/cases/' . $this->customer_case->id);
        else
            $this->emit('makeFlash', 'Veuillez renseigner tous les informations!');
    }

    public function render()
    {
        return view('livewire.sheet.wizard');
    }
}
