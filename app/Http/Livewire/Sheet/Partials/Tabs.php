<?php

namespace App\Http\Livewire\Sheet\Partials;

use Livewire\Component;
use App\Models\ActionLog;
use App\Models\DocumentType;
use Livewire\WithPagination;
use App\Models\ProgramCourse;
use App\Models\OrientationSchool;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;


class Tabs extends Component
{
    use WithPagination;
    public $customer_case;
    public $programs;
    public $activeTab;
    public $choices;
    public $program_modal;
    public $action;
    public $columns;

    protected $listeners = [
        'filterUpdated' => 'updateResults',
        'programSelected' => 'selectProgram',
        'resetForm' => 'clearFormInputs',
        'confirmDelete' => 'deleteProgram'
    ];

    public function mount($customer_case)
    {
        $this->customer_case = $customer_case;
        $this->activeTab = 1;

        // $pcv1 = DB::table('program_courses')
        //     ->where('visible', 1)
        //     ->groupBy('name')
        //     ->pluck('name','id')
        //     ->toArray();

        $pcv0 = DB::table('program_courses')->selectRaw("id")
            // ->where('visible', 0)
            ->groupBy(DB::raw('TRIM(program_courses.name)'))
            ->orderBy("name")
            ->get()->toArray();

        // dd(array_values($pcv0));
        $i = 0;
        foreach ($pcv0 as $key => $val) {
            $this->programs[$i] = $val->id;
            $i++;
        }

        // dd($this->programs);

        // $this->programs = array_keys($pcv1) + array_keys($pcv0);
        // $this->programs = array_values($pcv0);
        /*dd($this->programs = DB::table('program_courses')
            ->where("visible", 1)
            ->pluck('id'));*/
        // $this->updateProgramV2();
        // programmes selectionnee
        $this->choices = [];

        // dd($this->customer_case->customer->get());
        if (!is_null($this->customer_case)) {
            foreach ($this->customer_case->customer->courses()->get() as $item) {
                $this->choices[] = $item->id;
            }
        }
        $this->program_modal = [];
        // $this->action = 'add';
        $this->columns = [
            "Budget" => "budget",
            "Niveau Scolaire" => "niveau_scolaire",
            "Note Scolaire" => "note",
            "Type d'établissement" => "type_etablissement",
            "Nom d'etablissement" => "nom_etablissement",
            "Ville" => "ville",
            "Session" => "session",
            "Date limite d'admission" => "date_limit_admission",
            "Diplômes" => "diplome",
            "Domaine d'études" => "domaine_etude",
            "Programmes" => "programme",
            "Concentration" => "concentration",
            "Langue" => "langue",
            "Durée" => "duree",
            "Frais d'admission" => "frais_admission",
            "Frais scolaires par année" => "prix_annuel",
            "Total des frais scolaires" => "prix_total",
            "Lien" => "lien",
            "Exigences" => "exigence",
            "Test de langue" => "test_langue",
            "Score" => "score",
            "Objectifs de formation et débouchés du diplôme" => "analyse",
        ];
    }

    public function updateProgramV2()
    {
        $programs = $this->programs;
        $counts = [];
        foreach ($programs as $index => $id) {
            $pm = ProgramCourse::find($id);
            // dd($pm);
            $programMerged = ProgramCourse::where('name', 'LIKE', $pm->name)
                // ->where('visible', 1)
                ->count();
            if ($programMerged != 0) {
                $counts[] = $programMerged;
            }
            if ($programMerged > 0) {
                $ppm = ProgramCourse::where('name', 'LIKE', $pm->name)
                    // ->where('visible', 1)
                    ->first();
                if ($ppm->id != $id)
                    unset($this->programs[$index]);
            }
        }
        // dd($counts);
        // dd($this->programs);
        // $this->programs = array_slice($this->programs, 0, 100);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updateResults($role, $data)
    {
        // dd("7bess", $role, $data);
        $programs = [];
        $is_filter_empty = true;
        $filter_count = 0;
        foreach ($data as $field_name => $values) {
            $column = str_replace('admission_', '', $field_name);
            $programs_ = [];
            if ($column == "prix_total") {
                // dd($values > 0);
                if ($values > 0) {
                    $is_filter_empty = false;
                    $filter_count++;
                    $programs_ = ProgramCourse::where("properties->" . $column . "->value", '<=', $values)
                        // ->where("properties->" . $column . "->value", 'not LIKE', "%\u%")
                        // ->where("properties->" . $column . "->value" . ' <> ', '')
                        ->pluck("id")
                        ->toArray();
                }
            } elseif ($column == "programme") {
                if (count($values)) {
                    $is_filter_empty = false;
                    $filter_count++;
                    $programs_ = ProgramCourse::whereIn("name", $values)
                        ->pluck("id")
                        ->groupBy('name')
                        ->toArray();
                }
            } else {
                if (count($values)) {
                    $is_filter_empty = false;
                    $filter_count++;
                    // dd("properties->" . $column . "->value");
                    $programs_ = ProgramCourse::whereIn("properties->" . $column . "->value", $values)
                        ->pluck("id", "name")
                        // ->groupBy('name')
                        ->toArray();
                    $programs_1 = ProgramCourse::whereJsonContains("properties->" . $column . "->values", $values)
                        ->pluck("id", "name")
                        // ->groupBy('name')
                        ->toArray();
                    // dd($programs_, $programs_1);
                    $programs_ = array_merge($programs_, $programs_1);
                }
            }
            if (count($programs_)) {
                if ($filter_count == 1) {
                    $programs = array_merge($programs, $programs_);
                } else {
                    $programs = array_intersect($programs, $programs_);
                }
            }
        }
        // dd($programs, $data);
        // array_unique( array_diff_assoc( $programs, array_unique( $programs ) ) )
        $this->programs = $programs;
        // dd($programs);
        if ($role == "unselect" && $is_filter_empty == true)
            $this->programs = DB::table('program_courses')
                // ->limit(100)
                ->groupBy(DB::raw('TRIM(program_courses.name)'))
                ->pluck('id');
        // dd($programs);
        // $this->updateProgramV2();
        // dd($this->programs);
        // $this->updatingSearch();
    }
    public function toggleTab($tab)
    {
        $this->activeTab = $tab;
    }
    public function selectProgram($programId)
    {
        if (!is_null($this->customer_case)) {
            if (in_array($programId, $this->choices)) {
                $key = array_search($programId, $this->choices);
                unset($this->choices[$key]);
            } else {
                // if ($this->customer_case->customer->admission_state == 2)
                $this->choices[] = $programId;
                /*if ($this->customer_case->customer->admission_state == 2) {
                    $this->customer_case->customer->update(["admission_state" => 6]);
                }*/
            }
            $this->customer_case->customer->courses()->sync($this->choices);
        }
    }
    public function savePDF()
    {
        $document = DocumentType::find(45);
        // dd($document);
        $this->addMediaPDF($this->customer_case->customer, $document);
        // request()->header('Referer')
        return redirect()->route('case-edit', ['customer_case' => $this->customer_case]);
    }
    public function showModal($pid, $action)
    {
        $program = ProgramCourse::find($pid);
        // dd($program->properties);
        $this->program_modal = array_map(function ($item) {
            return $item["value"];
        }, $program->properties);
        $this->program_modal["id"] = $program->id;
        $this->program_modal["programme"] = $program->name;
        //$program->toArray(); $this->columns
        $this->action = $action;
        // dd($this->program_modal);
        $this->emit('programDetails');
    }
    public function clearFormInputs()
    {
        $this->program_modal = [];
        $this->action = 'add';
    }
    public function deleteProgram($pid)
    {
        // dd("Computer Engineering");
        // Computer Engineering
        $program = ProgramCourse::find($pid);
        ActionLog::create([
            'action'=>'suppression',
            'program_id'=>$program->id,
            'user_id'=>Auth::user()->id
        ]);
        Log::channel('logger_clear_1002')
        ->notice('le programme avec id : '.$program->id . ' est supprimé par '. Auth::user()->name);
        if ($program) {
            // Delete any related customer program course records
            // if ($program->customers()->count())
            // $program->customers()->detach();
            $program->delete();
            return redirect(request()->header('Referer'));
        }
    }
    public function updateProgram()
    {
        // dd($this->program_modal);
        if (array_key_exists("lien", $this->program_modal) && array_key_exists("programme", $this->program_modal)) {
            // OrientationSchool::create($this->program_modal);
            $p = ProgramCourse::find($this->program_modal["id"]);
            if ($p) {
                $properties = [];
                foreach ($this->columns as $label => $key) {
                    if ($key == "date_limit_admission") {
                        if (!isset($this->program_modal[$key]["day"])) $dd = 0;
                        else $dd = $this->program_modal[$key]["day"];
                        if (!isset($this->program_modal[$key]["month"])) $mm = 0;
                        else $mm = $this->program_modal[$key]["month"];
                        $properties[$key]["key"] = $label;
                        $properties[$key]["value"] = array("day" => $dd, "month" => $mm);
                    } elseif ($label == "Programmes") {
                        $p->name = $this->program_modal["programme"];
                    } else {
                        $value = trim($this->program_modal[$key]);
                        $properties[$key]["key"] = $label;
                        $properties[$key]["value"] = $value;
                    }
                }
                $p->properties = $properties;
                $p->save();
                // $p->update($this->program_modal);
            }
            $this->emit('closeModal');
            return redirect(request()->header('Referer'));
        } else {
            $errors = $this->getErrorBag();
            // With this error bag instance, you can do things like this:
            if (!array_key_exists("lien", $this->program_modal)) {
                $errors->add('lien', 'le lien est obligatoire');
            }
            if (!array_key_exists("programme", $this->program_modal)) {
                $errors->add('programme', 'le programme est obligatoire');
            }
        }
    }
    public function addProgram()
    {
        // foreach ($this->columns as $column), programme
        // dd($this->program_modal);
        if (array_key_exists("lien", $this->program_modal) && array_key_exists("programme", $this->program_modal)) {
            $properties = [];
            foreach ($this->columns as $label => $key) {
                if ($key == "date_limit_admission") {
                    if (!isset($this->program_modal["date_limit_admission"]["day"])) $dd = 0;
                    else $dd = $this->program_modal["date_limit_admission"]["day"];
                    if (!isset($this->program_modal["date_limit_admission"]["month"])) $mm = 0;
                    else $mm = $this->program_modal["date_limit_admission"]["month"];
                    $properties[$key]["key"] = $label;
                    $properties[$key]["value"] = array("day" => $dd, "month" => $mm);
                } elseif ($label != "Programmes") {
                    if (!isset($this->program_modal[$key]))
                        $value = "";
                    else $value = trim($this->program_modal[$key]);
                    $properties[$key]["key"] = $label;
                    $properties[$key]["value"] = $value;
                }
            }
            ProgramCourse::create([
                "name" => $this->program_modal["programme"],
                "properties" => $properties
            ]);
            $this->emit('closeModal');
        } else {
            $errors = $this->getErrorBag();
            // With this error bag instance, you can do things like this:
            if (!array_key_exists("lien", $this->program_modal)) {
                $errors->add('lien', 'le lien est obligatoire');
            }
            if (!array_key_exists("programme", $this->program_modal)) {
                $errors->add('programme', 'le programme est obligatoire');
            }
        }
    }
    protected function addMediaPDF($customer, $documentType)
    {
        $collectionName = $documentType->collection_name;

        $pathToFile = 'clear1002-' . str_replace(" ", "_", $customer->lastname) . '-' . str_replace(" ", "_", $customer->firstname) . ' ' . '.pdf';
        $path = "C:\\Users\\admin\\Desktop\\work\\temp\\";

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        // If the user file in existing directory already exist, delete it
        else if (file_exists($path . $pathToFile)) {
            unlink($path . $pathToFile);
        }
        $pdf_clear = PDF::loadView('pdfs.clear1002', compact('customer'));
        $pdf_clear->save($path . $pathToFile);

        foreach ($customer->getMedia($collectionName) as $media) $media->delete();

        $customer->addMedia($path . $pathToFile)
            ->withCustomProperties([
                'original_file_id' => 0,
                'pending_file_id' => 0,
                'visible_for_customer' => 1,
                'signable' => $documentType->signable,
                'customer_owner' => $documentType->customer_owner,
                'is_signed' => 0,
                'is_requested' => 0,
            ])
            ->toMediaCollection($collectionName);
    }
    public function render()
    {
        return view('livewire.sheet.partials.tabs', [
            'pagination_programs' => ProgramCourse::whereIn("id", $this->programs)->paginate(10),
        ]);
    }
}
