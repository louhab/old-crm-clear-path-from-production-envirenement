<?php

namespace App\Http\Livewire\User\Rotation;

use Livewire\Component;

class Assign extends Component
{
    public function render()
    {
        return view('livewire.user.rotation.assign');
    }
}
