<?php

namespace App\Http\Livewire\Tables;

use App\Models\CaseState;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media as CustomerMedia;

class ClearDocumentV2 extends Component
{
    public $customerCase;
    public $mediaSheet;
    public $garant;
    public $groupedDocuments;
    public $states;

    protected $listeners = [
        "mediaAdded" => "updateMedias"
    ];

    public function updateMedias()
    {
        // $this->emit('refreshComponent');
        return redirect(request()->header('Referer'));
    }

    public function mount($customerCase, $mediaSheet, $garant = null)
    {
        $this->mediaSheet = $mediaSheet;
        $this->customerCase = $customerCase;
        $this->garant = $garant;
        $this->groupedDocuments = [];

        // dd("test", $mediaSheet->docs()->wherePivot('program_id', $customerCase->customer->program_id)->withPivot('document_type_group_id', 'order')->orderBy('order')->get());

        // dd($customerCase->customer->payments()->withPivot('performed_at')->get()->toArray());
        foreach ($mediaSheet->docs()->wherePivot('program_id', $customerCase->customer->program_id)->withPivot('document_type_group_id', 'order')->orderBy('order')->get() as $document) {
            $doc_is_visible = true;
            if (!is_null($document->conditions) && count($document->conditions) > 0) {
                foreach ($document->conditions as $index => $condition) {
                    if (array_key_exists("type", $condition) && $condition["type"] == "payment") {
                        $this->states = [0, 3, 5, 10, 20, 29];
                        $case_state = CaseState::where("sub_step_state_id", $this->states[$condition["values"][0]])
                            ->where("customer_case_id", $customerCase->id)
                            ->first();
                        // dd($case_state);
                        if ($case_state->status != 1) {
                            $doc_is_visible = false;
                            break;
                        }
                        // dd($case_state);
                    } else {
                        $customer_field = \App\Models\CustomerField::where("field_name", $condition["field_name"])->first();
                        $field_name = $condition["field_name"];
                        $customerValuesQB = \App\Models\CustomerFieldValue::where('customer_id', $this->customerCase->customer_id)
                            ->where('field_name', $field_name);
                        // $garants = \App\Models\CustomerGarant::where('customer_id', $customerCase->customer_id)->orderBy('id', 'ASC')->get();
                        // dd($customerValuesQB->get()->toArray(), $garant);
                        if (!is_null($garant) && $garant > 0) {
                            $garants = \App\Models\CustomerGarant::where('customer_id', $customerCase->customer_id)->orderBy('id', 'ASC')->get();
                            $customerValuesQB->where('customer_garant_id', $garants[$garant - 1]->id);
                        }
                        if (is_null($garant) || $garant == 0) {
                            $customerValuesQB->whereNull('customer_garant_id');
                        }
                        $customer_values = $customerValuesQB->pluck('model_id');
                        // dd($customer_values->toArray());
                        $temp = false;
                        if (count($customer_values->toArray()) == 0) {
                            $customer_values = array();
                            /*if (in_array($field_name, \Illuminate\Support\Facades\Schema::getColumnListing('customer_garants'))) {
                                $garants = \App\Models\CustomerGarant::where('customer_id', $customerCase->customer_id)
                                    ->orderBy('id', 'ASC')
                                    ->get()
                                    ->toArray();
                                dd($garants);
                            }*/
                            $clear_fields = \App\Models\CustomerClear1000Fields::where('customer_id', $customerCase->customer->id)->first();
                            $customer_values[] = $clear_fields->$field_name;
                            $temp = true;
                            // dd($customer_values);
                        }
                        // if (count($customer_values) == 0) break;
                        if (str_ends_with($customer_field->select_model, "CustomerRadioField")) {
                            $values = $customer_field->select_model::where('name', $customer_field->field_name)
                                ->whereIn($customer_field->select_model_id, $customer_values)
                                ->pluck($customer_field->select_model_label, $customer_field->select_model_id);
                        } else $values = $customer_field->select_model::whereIn($customer_field->select_model_id, $customer_values)
                            ->pluck($customer_field->select_model_label, $customer_field->select_model_id);
                        if (count($values->toArray()) == 0)
                            break;
                        $result = array_intersect($condition["values"], $values->toArray());
                        /*if ($temp == true)
                            dd($values, $condition["values"], $result, $document);
                        */
                        if (count($result) == 0) {
                            $doc_is_visible = false;
                            break;
                        }
                    }
                }
            }
            if ($document->pivot->document_type_group_id && $doc_is_visible == true) {
                $this->groupedDocuments[$document->pivot->document_type_group_id][] = $document->toArray();
            } else {
                // just for fixing bug but latly should be working (add new group for this docs)
                $this->groupedDocuments[$document->pivot->media_sheet_id][] = $document->toArray();
            }
        }
    }
    public function removeMedia(CustomerMedia $media)
    {
        $media->delete();
        $this->emit('mediaAdded');
    }

    public function downloadMedia(CustomerMedia $media)
    {
        return $media;
    }

    public function render()
    {
        return view('livewire.tables.clear-document-v2');
    }
}
