<?php

namespace App\Http\Livewire\Tables;

use App\Models\DocumentType;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media as CustomerMedia;

class ClearDocument extends Component
{
    public $customerCase;
    public $mediaSheet;
    public $docs;
    public $docsWithoutTags;
    public $groupedDocs;
    public $tags;
    public $medias;
    public $garant;
    protected $listeners = [
        "mediaAdded" => "updateMedias"
    ];

    public function updateMedias()
    {
        $this->emit('refreshComponent');
    }

    public function mount($customerCase, $mediaSheet, $garant=null) {
        $this->mediaSheet   = $mediaSheet;
        $this->customerCase = $customerCase;
        $this->tags = [];
        $this->garant = $garant;
        $this->groupedDocs = [];
        /*$this->docs = new Collection;
        $this->docs = $this->docs->merge(
            $this->mediaSheet->docs()
                ->wherePivot('program_id', '=', $this->customerCase->program_id)
                ->withPivot(['required', 'order'])
                ->orderBy('order')
                ->orderBy('name_fr')
                ->get()
        );
        $this->docs = $this->docs->sortBy(function ($document, $key) {
            return $document->pivot->order;
        })->toArray();*/
        // document by tag
        $documents = DocumentType::with('tagged', 'sheets')
            ->get();
        $this->docsWithoutTags = [];
        $filtered = $documents->filter(function ($value, $key) {
            /*if (count($value->sheets) > 0 && in_array($mediaSheet->id, $value->sheets->pluck('id')->toArray()))
                dd($value->sheets->pluck('pivot.program_id')->toArray());*/
            // count($value->tagged) > 0 &&
            $checked = count($value->sheets) > 0 &&
                in_array($this->mediaSheet->id, $value->sheets->pluck('id')->toArray()) &&
                in_array($this->customerCase->program_id, $value->sheets->pluck('pivot.program_id')->toArray());
            if ($checked && count($value->tagged) > 0)
                $this->tags = array_merge($this->tags, $value->tagNames());
            if ($checked && count($value->tagged) == 0)
                $this->docsWithoutTags[] = $value->toArray();
            return $checked && count($value->tagged) > 0;
        });
        // dd($filtered, $this->docsWithoutTags);
        $this->docs = $filtered->toArray();
        /*->sortBy(function ($document, $key) {
            $this->tags = array_merge($this->tags, $document->tagNames());
            return $document->sheets->first()->pivot->order;
        })->toArray();*/
        /*$tags = $filtered->groupBy(function ($item) {
            dd($item->tagNames());
            return $item['roles'];
        });*/
        $this->tags = array_unique($this->tags);
        // dd($this->tags, $this->docs, $filtered);
    }
    public function render()
    {
        return view('livewire.tables.clear-document');
    }

    public function downloadMedia(CustomerMedia $media) {
        return $media;
    }
    public function removeMedia(CustomerMedia $media) {
        $media->delete();
        $this->emit('mediaAdded');
    }
}
