<?php

namespace App\Http\Livewire\Input;

use Livewire\Component;

class ProgramCourse extends Component
{
    public $key;
    public $label;
    public $selectValues;

    // protected $listeners = ["selectInit" => "setSelected"];

    public function mount($label, $key) {
        $this->label = $label;
        $this->key = $key;
        $this->selectValues = \App\Models\ProgramCourseField::where('field_name', $key)->first()->values;
        // foreach(['domaine_etude', 'test_langue','langue', 'ville', 'session', 'diplome', 'type_etablissement'] as $key)
    }

    /*public function setSelected($program) {
        if (in_array($this->key, $program)) {
            dd($program);
        }
    }*/
    public function updateSelect($key) {
        if ($this->key == $key)
            $this->selectValues = \App\Models\ProgramCourseField::where('field_name', $key)->first()->values;
    }

    public function render()
    {
        return view('livewire.input.program-course');
    }
}
