<?php

namespace App\Http\Livewire\Input;

use App\Models\LeadForm;
use Livewire\Component;

class Source extends Component
{
    public $campaigns;

    protected $listeners = [
        "campaignChanged" => "updateSource"
    ];

    public function mount() {
        $this->campaigns = LeadForm::all()->pluck('id')->toArray();
    }
    public function updateSource($data) {
        // dd($data);
        if (count($data) == 0) {
            $this->campaigns = LeadForm::all()->pluck('id')->toArray();
        } else $this->campaigns = $data;
            $this->emit("updatedSource");
    }

    public function render()
    {
        return view('livewire.input.source');
    }
}
