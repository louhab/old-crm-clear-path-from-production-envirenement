<?php

namespace App\Http\Livewire\Input\Document;

use App\Models\CustomerField;
use App\Models\DocumentType;
use Livewire\Component;

class Condition extends Component
{
    public $values;
    public $documentID;
    public $conditionType;
    public $selectedValues;
    public $field_name;
    public $order;

    protected $listeners = [
        "changedConditions" => "updateCondition",
        "fieldChanged" => "setValues",
    ];

    public function mount($order, $documentID)
    {
        $this->order = $order;
        $this->updateCondition($documentID);
        // $this->count =  ? 0: count($document->conditions);
        // dd($this->selectedValues, $document->conditions[$order]["values"]);
        // $this->emit("initDocumentConditionSelect");
    }

    /*public function updated($name, $value)
    {
        // $this->emit("initDocumentConditionSelect");
    }*/

    public function updateCondition($documentID) {
        $this->documentID = $documentID;
        $document = DocumentType::find($this->documentID);
        $this->selectedValues = [];
        $this->field_name = null;
        $this->values = [];
        $this->conditionType = "question";
        if (!is_null($document->conditions) && array_key_exists($this->order, $document->conditions)) {
            if (array_key_exists("type", $document->conditions[$this->order]))
                $this->conditionType = $document->conditions[$this->order]["type"];
            if ($this->conditionType == "question") {
                $this->selectedValues = array_key_exists("values", $document->conditions[$this->order]) ? $document->conditions[$this->order]["values"]: [];
                $this->field_name = $document->conditions[$this->order]["field_name"];
                $this->getFieldValues();
            } elseif ($this->conditionType == "payment") {
                $this->selectedValues[] = intval($document->conditions[$this->order]["values"][0]);
            }
        }
        if ($this->conditionType == "question") {
            $this->emit('conditionSelect2', $this->order);
            // $this->emit('conditionSelect2Values', $this->order, $this->selectedValues);
        }
        // dd($document->conditions);
    }

    public function updated() {
        if ($this->conditionType == "question")
            $this->emit('conditionSelect2', $this->order);
    }

    public function setValues($order, $field_name)
    {
        if ($order == $this->order) {
            $this->field_name = $field_name;
            $this->getFieldValues();
            $this->emit('conditionSelect2', $this->order);
        }
        // $this->emit("initDocumentConditionSelect");
        // $this->emit('refreshComponent');
    }
    private function getFieldValues() {
        $customer_field = CustomerField::where("field_name", $this->field_name)->first();
        // dd($customer_field->select_model);
        if (str_ends_with($customer_field->select_model, "CustomerRadioField"))
            $this->values = $customer_field->select_model::where('name', $customer_field->field_name)
                ->pluck($customer_field->select_model_label, $customer_field->select_model_id);
        else $this->values = $customer_field->select_model::pluck($customer_field->select_model_label, $customer_field->select_model_id);
    }
    public function render()
    {
        return view('livewire.input.document.condition');
    }
}
