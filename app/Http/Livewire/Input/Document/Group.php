<?php

namespace App\Http\Livewire\Input\Document;

use App\Models\DocumentType;
use Livewire\Component;

class Group extends Component
{
    public $count;
    // public $document;
    // public $selectedValues;
    public $documentID;

    protected $listeners = [
        "setConditions" => "initConditions",
        // 'refreshComponent' => '$refresh',
    ];

    public function mount() {
        $this->count = 0;
        // $this->selectedValues = [];
    }

    /*public function hydrate()
    {
        $this->emit('refreshComponent');
    }*/

    public function initConditions($document_id) {
        $this->documentID = $document_id;
        $document = DocumentType::find($document_id);
        $this->count = is_null($document->conditions) ? 0: count($document->conditions);
        // dd($document->conditions);
        $this->emit('changedConditions', $document_id);
    }
    public function increase()
    {
        $this->count++;
        $this->emit('changedConditions', $this->documentID);
        // $this->emit('countChanged', $this->count);
    }
    public function render()
    {
        return view('livewire.input.document.group');
    }
}
