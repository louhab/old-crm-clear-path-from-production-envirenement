<?php

namespace App\Http\Livewire\Input;

use Livewire\Component;

class ToggleState extends Component
{
    public $name;

    public function mount($name) {
        $this->name = $name;
    }
    public function render()
    {
        return view('livewire.input.toggle-state');
    }
}
