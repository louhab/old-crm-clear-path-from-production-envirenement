<?php

namespace App\Http\Livewire\Input;

use Livewire\Component;
use App\Models\LeadFormMarketingCampaign;

class Campaign extends Component
{
    public $campaign_id;
    public $campaigns;

    public function mount($selected=null) {
        // dd($selected);
        $this->campaigns = LeadFormMarketingCampaign::pluck('campaign_name', 'id');
        $this->campaign_id = $selected;
        $this->emit('campChanged', $this->campaign_id);
    }

    public function render()
    {
        return view('livewire.input.campaign');
    }

    public function change() {
        $this->emit('campChanged', $this->campaign_id);
    }
}
