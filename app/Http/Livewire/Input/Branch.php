<?php

namespace App\Http\Livewire\Input;

use Livewire\Component;
use App\Models\CustomerClear1000Fields;

class Branch extends Component
{
    public $customer_case;
    public $customer_field;
    public $visible;
    public $value;

    protected $listeners = ['branchSelected' => 'addBranch'];

    public function mount($customer_case, $field) {
        $this->customer_case = $customer_case;
        $this->customer_field = $field;
        $customer = CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first();
        if ($customer && ($customer->branch == 11)) {
            $this->visible = true;
            $this->value = $customer->other_branch;
        } else {
            $this->visible = false;
        }
    }

    public function addBranch($selected) {
        // dd('test');
        if ($selected == 11) {
            $this->visible = true;
        } else {
            $this->visible = false;
        }
    }

    public function render()
    {
        return view('livewire.input.branch');
    }
}
