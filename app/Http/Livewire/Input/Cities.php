<?php

namespace App\Http\Livewire\Input;

use App\Models\Cities as ModelsCities;
use Livewire\Component;

class Cities extends Component
{
    public $cities;
    public $value;

    public function mount($value = null)
    {
        $this->value = $value;
        $this->cities = ModelsCities::select("id", "city")->orderBy('city', 'asc')->pluck("city", "id")->toArray();
    }


    public function render()
    {
        return view('livewire.input.cities');
    }
}
