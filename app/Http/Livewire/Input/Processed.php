<?php

namespace App\Http\Livewire\Input;

use Livewire\Component;

class Processed extends Component
{
    public $selected;
    public function mount($selected=null) {
        $this->selected = $selected;
    }
    public function render()
    {
        return view('livewire.input.processed');
    }
}
