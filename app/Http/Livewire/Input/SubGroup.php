<?php

namespace App\Http\Livewire\Input;

use App\Models\CustomerClear3Fields;
use App\Models\CustomerFieldGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Livewire\Component;

class SubGroup extends Component
{
    public $tmp;


    public $customer_case;
    public $fields;
    public $groupId;
    public $groupName;
    public $clear;
    public $groupedResultsArray;
    public $groupNames;
    public $grouping;
    public $orders;
    public $join;

    protected $listeners = ['addBlock' => 'multipleBlock'];

    protected function rules()
    {
        $skip = array('id', 'customer_id', 'updated_at', 'created_at');
        $columns = array_diff(Schema::getColumnListing('customer_clear3_fields'), $skip);
        $rules = [];
        foreach ($columns as $col) {
            $rules['clear.' . $col] = 'required';
        }
        return $rules;
    }

    public function mount($customer_case, $fields, $groupId, $join)
    {
        $this->tmp = CustomerFieldGroup::find($groupId);
        // if (!$this->tmp) dd($groupId);
        $this->customer_case = $customer_case;
        $this->join = $join;
        $this->groupId = $groupId;
        $this->groupName = CustomerFieldGroup::find($groupId)->name_fr;
        // dd("okey stop here");
        $this->fields = $fields;
        $customer_clear = CustomerClear3Fields::where('customer_id', $this->customer_case->customer->id)->where('join', $this->join)->first();
        if (!$customer_clear) {
            $customer_clear = CustomerClear3Fields::create([
                'customer_id' => $this->customer_case->customer->id,
                'join' => $this->join
            ]);
        }

        // dd($customer_clear);

        $this->clear = $customer_clear;
        $this->groupedResultsArray = [];
        $this->groupNames = [];
        $this->grouping = [];
        // dd($fields);
        foreach ($fields as $field) {
            // if (!$field->subGroup) dd($field->subGroup);
            $this->groupedResultsArray[$field->customer_field_sub_group_id][] = $field;
            // if ($field->subGroup)
            $this->groupNames[$field->customer_field_sub_group_id] = $field->subGroup;
            // $this->cc[$field->customer_field_sub_group_id] = 0;
        }

        // dd($this->groupNames);
        $this->orders = [];
        foreach ($this->groupNames as $idG => $nameG) {
            switch ($idG) {
                case 4:
                    $this->grouping[$idG] = array('child', 'clear3_child_fields', 'App\Models\Clear3ChildFields');
                    $this->orders[$idG] = DB::table('clear3_child_fields')
                        ->where('customer_id', $customer_case->customer_id)
                        ->where('join', $this->join)
                        ->orderBy('order')
                        ->pluck('order')
                        ->toArray();
                    /*$this->cc[4] = DB::table('clear3_child_fields')
                        ->where('customer_id', $customer_case->customer_id)
                        ->count();*/
                    break;
                case 5:
                    $this->grouping[$idG] = array('sibling', 'clear3_sibling_fields', 'App\Models\Clear3SiblingFields');
                    $this->orders[$idG] = DB::table('clear3_sibling_fields')
                        ->where('customer_id', $customer_case->customer_id)
                        ->where('join', $this->join)
                        ->orderBy('order')
                        ->pluck('order')
                        ->toArray();
                    /*$this->cc[5] = DB::table('clear3_sibling_fields')
                        ->where('customer_id', $customer_case->customer_id)
                        ->count();*/
                    break;
                case 7:
                    // clear3_various_travel_fields
                    $this->grouping[$idG] = array('past_residence', 'clear3_past_residence_fields', 'App\Models\Clear3PastResidenceFields');
                    $this->orders[$idG] = DB::table('clear3_past_residence_fields')
                        ->where('customer_id', $customer_case->customer_id)
                        ->where('join', $this->join)
                        ->orderBy('order')
                        ->pluck('order')
                        ->toArray();
                    /*$this->cc[7] = DB::table('clear3_past_residence_fields')
                        ->where('customer_id', $customer_case->customer_id)
                        ->count();*/
                    break;
                case 8:
                    $this->grouping[$idG] = array('various_travel', 'clear3_various_travel_fields', 'App\Models\Clear3VariousTravelFields');
                    $this->orders[$idG] = DB::table('clear3_various_travel_fields')
                        ->where('customer_id', $customer_case->customer_id)
                        ->where('join', $this->join)
                        ->orderBy('order')
                        ->pluck('order')
                        ->toArray();
                    /*$this->cc[8] = DB::table('clear3_various_travel_fields')
                        ->where('customer_id', $customer_case->customer_id)
                        ->count();*/
                    break;
            }
        }
        // dd(array_keys($this->groupedResultsArray));
    }

    public function updated($propertyName)
    {
        $this->emit('tabActive', ['tabId' => $this->groupId]);
        $this->clear->save();
    }

    public function deleteBlock($group, $order)
    {
        $key = array_search($order, $this->orders[$group]);
        unset($this->orders[$group][$key]);
        $this->grouping[$group][2]::where('order', $order)->where('join', $this->join)->where('customer_id', $this->customer_case->customer->id)->delete();
        $this->emit("fieldsDeleted");
    }

    public function addBlock($group)
    {
        // dd($groupId);
        // dd($this->rules());
        // $this->cc[$group] += 1;
        $last = end($this->orders[$group]);
        $this->orders[$group][] = $last + 1;
        foreach ($this->fields as $field) {
            if ($field["customer_field_sub_group_id"] == $group)
                $this->emit('fieldUpdated', $field["field_name"], $field["customer_field_group_id"], $last + 1, $this->grouping[$group][0]);
        }
    }
    public function render()
    {
        return view('livewire.input.sub-group');
    }
}
