<?php

namespace App\Http\Livewire\Input;

use Illuminate\Support\Facades\DB;
use Livewire\Component;

class CallsCount extends Component
{
    public $counts;

    public function mount() {
        $this->counts = DB::table('leads as l')
            ->leftJoin(DB::raw('(SELECT `lead_id`, COUNT(`lead_id`) AS `calls`, `lead_state_id` FROM `histo_calls`, `leads` WHERE `leads`.`id`=`histo_calls`.`lead_id` AND `lead_status_id`=`lead_state_id` GROUP BY `lead_id`) hc'), function($join){
                $join->on('hc.lead_id', '=', 'l.id');
            })
            ->whereNotNull('hc.calls')
            ->select('hc.calls')
            ->groupBy('hc.calls')
            ->get()
            ->toArray();
        // dd($this->counts);
    }
    public function render()
    {
        return view('livewire.input.calls-count');
    }
}
