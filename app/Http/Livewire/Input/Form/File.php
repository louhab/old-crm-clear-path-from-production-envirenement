<?php

namespace App\Http\Livewire\Input\Form;

use Illuminate\Support\Facades\Route;
use Livewire\Component;
use Livewire\WithFileUploads;

class File extends Component
{
    use WithFileUploads;
    public $customer_case;
    public $document;
    public $collection;
    public $name;

    public function mount($customer_case, $collection) {
        $this->customer_case = $customer_case;
        $this->collection = $collection;
        $this->name = explode('-admission-', $collection)[0];
    }

    public function updatedDocument()
    {
        //dd($this->document);
        foreach ($this->customer_case->customer->getMedia($this->collection) as $media) $media->delete();
        $this->customer_case->customer
            ->addMedia($this->document)
            ->withCustomProperties(['visible_for_customer' => 1])
            ->toMediaCollection($this->collection);
        return redirect(request()->header('Referer'));
    }
    public function render()
    {
        return view('livewire.input.form.file');
    }
}
