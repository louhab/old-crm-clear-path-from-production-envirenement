<?php

namespace App\Http\Livewire\Input\Form;

use Livewire\Component;

class Text extends Component
{
    public $selected;
    public $field;
    public $index;
    public $container;
    public $hidden_by_def;

    public function mount($field, $selected, $index, $container="Garant") {
        $this->container = $container;
        $this->index = $index;
        $this->field = $field;
        // $this->values = $field["select_model"]::pluck($field["select_model_label"], $field["select_model_id"]);
        $this->selected = $selected;
        $this->hidden_by_def = [
            "criminal_record_rep",
            "health_rep",
            "tef_test_res",
        ];
    }

    public function updatedSelected($value)
    {
        $this->emit("selectFieldUpdated". $this->container, $this->field, $this->index, $this->selected);
    }

    public function render()
    {
        return view('livewire.input.form.text');
    }
}
