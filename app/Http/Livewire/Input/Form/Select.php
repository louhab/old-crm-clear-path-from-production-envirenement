<?php

namespace App\Http\Livewire\Input\Form;

use Livewire\Component;

class Select extends Component
{
    public $selected;
    public $index;
    public $field;
    public $values;
    public $is_multiple;
    public $hidden_by_def;
    public $container;
    public $program_id;

    protected $listeners = [
        'refreshComponent' => '$refresh'
    ];

    public function mount($field, $selected, $is_multiple, $index, $container = "Garant", $program_id = null)
    {
        $this->container = $container;
        $this->index = $index;
        $this->field = $field;
        $this->is_multiple = $is_multiple;
        $this->program_id = $program_id;

        // if (str_ends_with($field["select_model"]))
        if (str_ends_with($field["select_model"], "CustomerRadioField"))
            $this->values = $field["select_model"]::where('name', $field["field_name"])
                ->pluck($field["select_model_label"], $field["select_model_id"]);
        else $this->values = $field["select_model"]::pluck($field["select_model_label"], $field["select_model_id"]);
        // $this->values = $field["select_model"]::pluck($field["select_model_label"], $field["select_model_id"]);
        $this->selected = $selected;
        $this->hidden_by_def = [
            "guarantor_work_income",
            "guarantor_retirement_income",
            "guarantor_business_income",
            "guarantor_professional_income",
            "guarantor_self_employed_income",
            "guarantor_rental_income",
            "guarantor_investment_income",
            "goods_res"
        ];
    }

    public function updatedSelected($value)
    {
        // dd($value);
        $this->emit("selectFieldUpdated" . $this->container, $this->field, $this->index, $this->selected);
    }

    public function render()
    {
        return view('livewire.input.form.select');
    }
}
