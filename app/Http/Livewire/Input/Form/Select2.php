<?php

namespace App\Http\Livewire\Input\Form;

use App\Models\CustomerFieldValue;
use Livewire\Component;

class Select2 extends Component
{
    public $customer_case;
    public $customer_field;
    public $field_name;
    public $attributes;
    public $values;
    public $selected;

    /*
     * diploma_fr
     * diploma_en
     * language
     * training_type
     * diplomas
     * province
     * city
     * */

    public function mount($customer_case, $customer_field)
    {
        $this->customer_case = $customer_case;
        $this->customer_field = $customer_field;
        $this->field_name = $customer_field->field_name;
        $field_name = $this->field_name;
        // select tag attributes
        $attributes = [
            // 'class' => 'form-control form-control-alt' . (isset($errors) && $errors->has($field_name) ? ' is-invalid' : (isset($errors) && $errors->has($field_name) ? 'true' : 'false')),
            'aria-describedby' => $field_name . '-error',
            'aria-invalid' => ''
        ];
        $attributes['data-placeholder'] = __('lang.' . $field_name) . '..';
        $attributes['multiple'] = "multiple";
        $attributes['name'] = $field_name . "[]";
        // $attributes['placeholder'] = __('lang.' . $field_name) . '..';
        $this->attributes = $attributes;
        // load select values and set selected
        $old_value = \App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first()->$field_name;
        //dd($old_value);
        $lang = session()->get('locale') === 'en' ? "en" : "fr";

        // {{-- should be fix 🚀 language_tests[✅], language[✅], province[🧪], city[✅], training_type[❌], diplomas[❌] --}}
        // add spanich to customer_radio_fields where name (field_name) == "language" and mdify the program_ids
        // Espagnol => Spanish
        $multi_programs_fields = ["language_tests", "city"];
        // TODO add training_type[❌], diplomas[❌]
        $customer_fields_by_program = ["language"];
        if (str_ends_with($customer_field->select_model, 'CustomerRadioField')) {
            switch ($customer_field->select_model_label) {
                case "value_fr":
                    $select_model_label = $lang === "fr" ? "value_fr" : "value_en";
                    break;
                case "label":
                    $select_model_label = $lang === "fr" ? "label" : "labelEng";
                default:
                    $select_model_label = $customer_field->select_model_label;
            }

            if (in_array($customer_field->field_name, $customer_fields_by_program)) {
                switch ($this->customer_case->program_id) {
                        // program_ids for UK
                    case 31:
                        $values_ = $customer_field->select_model::whereJsonContains('program_ids', 31)->pluck($customer_field->select_model_label, $customer_field->select_model_id);
                        break;
                        // program_ids for ES
                    case 32:
                        $values_ = $customer_field->select_model::whereJsonContains('program_ids', 32)->pluck($customer_field->select_model_label, $customer_field->select_model_id);
                        break;
                        // program_ids for CA
                    default:
                        $values_ = $customer_field->select_model::whereJsonContains('program_ids', 4)->pluck($customer_field->select_model_label, $customer_field->select_model_id);
                        break;
                }
            } else {
                $array = $customer_field->select_model::where("name", "=", $customer_field->field_name)->pluck($select_model_label, $customer_field->select_model_id);
                $values_ = array();
                foreach ($array as $key => $value) {
                    if (str_starts_with($key, $customer_field->field_name . '-')) {
                        $key_ = explode("-", $key)[1];
                        $values_[$key_] = $value;
                    } else {
                        $values_[$key] = $value;
                    }
                }
            }

            if (str_starts_with($customer_case->customer->$field_name, $customer_field->field_name . '-')) {
                $old_value = explode("-", $customer_case->customer->$field_name)[1];
            }
        } else {
            // 🥳 province Also should be modified after
            if (str_ends_with($customer_field->select_model, 'Province')) {
                $select_model_label = $lang === "fr" ? "label" : "labelEng";
                $values_ = $customer_field->select_model::pluck($select_model_label, $customer_field->select_model_id);
            } else if (in_array($customer_field->field_name, $multi_programs_fields)) {
                // dd($customer_field->select_model);
                switch ($this->customer_case->program_id) {
                        // program_ids for UK
                    case 31:
                        $values_ = $customer_field->select_model::whereJsonContains('program_ids', 31)->pluck($customer_field->select_model_label, $customer_field->select_model_id);
                        break;
                        // program_ids for ES
                    case 32:
                        $values_ = $customer_field->select_model::whereJsonContains('program_ids', 32)->pluck($customer_field->select_model_label, $customer_field->select_model_id);
                        break;
                        // program_ids for CA
                    default:
                        $values_ = $customer_field->select_model::whereJsonContains('program_ids', 4)->pluck($customer_field->select_model_label, $customer_field->select_model_id);
                        break;
                }
            } else {
                $values_ = $customer_field->select_model::pluck($customer_field->select_model_label, $customer_field->select_model_id);
            }
        }
        $this->values = $values_;
        // dd($this->values);
        //old values
        $this->selected = CustomerFieldValue::where('customer_id', $customer_case->customer->id)->where('field_name', $field_name)->whereNull('customer_garant_id')->pluck('model_id');
        if (count($this->selected) == 0) {
            $this->selected = $old_value;
        }
    }
    public function render()
    {
        return view('livewire.input.form.select2');
    }
}
