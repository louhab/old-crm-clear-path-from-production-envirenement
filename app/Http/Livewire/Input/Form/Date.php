<?php

namespace App\Http\Livewire\Input\Form;

use Livewire\Component;

class Date extends Component
{
    public $selected;
    public $index;
    public $field;
    public $values;
    public $container;

    protected $listeners = [
        'refreshComponent' => '$refresh'
    ];

    public function mount($field, $selected, $index, $container="Garant") {
        $this->container = $container;
        $this->index = $index;
        $this->field = $field;
        $this->selected = $selected;
    }

    public function updatedSelected($value)
    {
        $this->emit("selectFieldUpdated". $this->container, $this->field, $this->index, $this->selected);
    }

    public function render()
    {
        return view('livewire.input.form.date');
    }
}
