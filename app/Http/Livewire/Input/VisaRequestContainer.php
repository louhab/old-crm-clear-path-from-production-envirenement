<?php

namespace App\Http\Livewire\Input;

use App\Models\CustomerVisaRequest;
use Livewire\Component;

class VisaRequestContainer extends Component
{
    public $customer_case;
    public $program_id;
    public $sheet_id;
    public $fields;
    public $clear_fields;
    public $visa_requests;
    public $container;

    protected $listeners = [
        "selectFieldUpdatedVisaRequest" => "updateModel"
    ];

    public function mount($customer_case, $fields, $sheet_id = 1)
    {
        $this->container = "visa_request";
        $this->sheet_id = $sheet_id;
        $this->customer_case = $customer_case;
        $this->program_id = $customer_case->program_id;
        $this->fields = $fields;
        $this->visa_requests = CustomerVisaRequest::where('customer_id', $customer_case->customer_id)->orderBy('id', 'ASC')->get();
        $this->clear_fields = \App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first();
    }

    public function addRequest()
    {
        CustomerVisaRequest::create([
            'customer_id' => $this->customer_case->customer_id,
        ]);
        $this->visa_requests = CustomerVisaRequest::where('customer_id', $this->customer_case->customer_id)->orderBy('id', 'ASC')->get();
    }

    public function updateModel($field, $index, $value)
    {
        if (is_null($index))
            $this->clear_fields->update([$field["field_name"] => $value]);
        else
            $this->visa_requests[$index]->update([$field["field_name"] => $value]);
        if ($field["field_name"] == "visa_request")
            $this->emit('toggleVisaRequestVisibility', is_null($index) ? 0 : $this->visa_requests[$index]->id, $value);
    }

    public function render()
    {
        return view('livewire.input.visa-request-container');
    }
}
