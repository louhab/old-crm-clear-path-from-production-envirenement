<?php

namespace App\Http\Livewire\Input;

use Livewire\Component;

class Form extends Component
{
    public $campaigns;
    protected $listeners = [
        "campChanged" => "updateSelect"
    ];

    public function mount() {
        $this->campaigns = [7, 16, 20];
    }
    public function render()
    {
        return view('livewire.input.form');
    }

    public function updateSelect($campaign) {
        $this->campaigns = [$campaign];
    }
}
