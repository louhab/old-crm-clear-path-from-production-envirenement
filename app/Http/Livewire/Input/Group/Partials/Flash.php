<?php

namespace App\Http\Livewire\Input\Group\Partials;

use Livewire\Component;

class Flash extends Component
{
    public $message;
    public $visible;

    protected $listeners = ['makeFlash' => 'showFlash'];

    public function mount() {
        $this->visible = false;
    }

    public function showFlash($message) {
        $this->visible = true;
        $this->message = $message;
    }
    public function render()
    {
        return view('livewire.input.group.partials.flash');
    }
}
