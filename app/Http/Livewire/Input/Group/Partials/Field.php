<?php

namespace App\Http\Livewire\Input\Group\Partials;

use App\Models\CustomerClear3Fields;
use Livewire\Component;

class Field extends Component
{
    public $customer_case;
    // public $field;
    public $field_name;
    public $field_template;
    public $value;
    public $field_group_id;
    public $order;
    public $group;
    public $groupData;
    public $join;

    public function mount($customer_case, $field_name, $field_template, $field_group_id, $join, $order = null, $groupData = null)
    {
        $this->customer_case = $customer_case;
        $this->join = $join;
        $this->field_name = $field_name;
        $this->field_template = $field_template;
        $this->field_group_id = $field_group_id;
        $this->order = $order || null;
        $this->groupData = $groupData || null;
        if (is_null($order)) {
            $customer_clear = CustomerClear3Fields::where('customer_id', $this->customer_case->customer->id)
                ->where('join', $this->join)
                ->first();
            if (!$customer_clear) {
                $customer_clear = CustomerClear3Fields::create([
                    'customer_id' => $this->customer_case->customer->id,
                    'join' => $this->join
                ]);
            }
            $this->value = $customer_clear->$field_name;
        } else {

            // for multiple groups of field
            $customer_clear = $groupData[2]::where('order', $order)
                ->where('customer_id', $this->customer_case->customer->id)
                ->where('join', $this->join)
                ->first();
            if (!$customer_clear) {
                $customer_clear = $groupData[2]::create([
                    'customer_id' => $this->customer_case->customer_id,
                    'order' => $order,
                    'join' => $this->join
                ]);
            }
            $this->value = $customer_clear->$field_name;
        }
    }

    public function updated($propertyName)
    {
        if (is_null($this->order)) {
            CustomerClear3Fields::where('customer_id', $this->customer_case->customer->id)
                ->where('join', $this->join)
                ->update([$this->field_name => $this->value]);
            $this->emit('fieldUpdated', $this->field_name, $this->field_group_id);
        } else {
            CustomerClear3Fields::where('customer_id', $this->customer_case->customer->id)
            ->where('join', $this->join)
            ->update([$this->field_name => $this->value]);
        $this->emit('fieldUpdated', $this->field_name, $this->field_group_id);

            // $this->groupData[2]::where('customer_id', $this->customer_case->customer->id)
            //     ->where('order', $this->order)
            //     ->where('join', $this->join)
            //     ->update([$this->field_name => $this->value]);
            // $this->emit('fieldUpdated', $this->field_name, $this->field_group_id, $this->order, $this->groupData[0]);
        }
        // $this->validate();
        // if ($this->clear->isDirty())
        // $this->clear->save();
        // $this->emit('tabActive', ['tabId' => $this->groupId]);
    }

    public function render()
    {
        return view('livewire.input.group.partials.field');
    }
}
