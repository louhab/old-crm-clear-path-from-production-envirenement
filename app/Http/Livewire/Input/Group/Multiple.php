<?php

namespace App\Http\Livewire\Input\Group;

use App\Models\CustomerClear3Fields;
use App\Models\InformationSheet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Livewire\Component;
use App\Models\CustomerFieldGroup;

class Multiple extends Component
{
    public $customer_case;
    public $fields;
    public $groupId;
    public $groupName;
    // public $clear;
    // public $cc;
    public $orders;
    public $grouping;
    public $join;
    public $extraFields;

    protected $listeners = [
        'addBlock' => 'multipleBlock'
    ];

    public function mount($customer_case, $fields, $groupId, $grouping, $join) {
        // $this->cc = 0;
        $this->grouping = $grouping;
        $this->join = $join;
        if(array_key_exists($groupId, $grouping)) {
            /*$this->cc = DB::table($grouping[$groupId][1])
                ->where('customer_id', $customer_case->customer_id)
                ->count();*/
            $this->orders = DB::table($grouping[$groupId][1])
                ->where('customer_id', $customer_case->customer_id)
                ->where('join', $this->join)
                ->orderBy('order')
                ->pluck('order')
                ->toArray();
            // dd($this->join, $this->orders);
        }
        $this->customer_case = $customer_case;
        $this->groupId = $groupId;
        $this->groupName = CustomerFieldGroup::find($groupId)->name_fr;
        $this->fields = $fields;
        /*$customer_clear = CustomerClear3Fields::where('customer_id', $this->customer_case->customer->id)->first();
        if (!$customer_clear) {
            $customer_clear = CustomerClear3Fields::create([
                'customer_id' => $this->customer_case->customer->id,
            ]);
        }
        $this->clear = $customer_clear;*/
        $this->extraFields = [];
        $form = InformationSheet::find(1);
        foreach($form->fields()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->where('customer_fields.customer_field_group_id', 13)->orderBy('order')->get() as $field) {
            $this->extraFields[] = $field;
        }
    }

    /*public function updated($propertyName)
    {
        if ($this->clear->isDirty())
            $this->clear->save();
        $this->emit('tabActive', ['tabId' => $this->groupId]);
    }*/

    public function deleteBlock($order) {
        $key = array_search($order, $this->orders);
        unset($this->orders[$key]);
        $this->grouping[$this->groupId][2]::where('order', $order)->where('join', $this->join)->where('customer_id', $this->customer_case->customer->id)->delete();
        $this->emit("fieldsDeleted");
    }

    public function addBlock() {
        // dd($groupId);
        // dd($this->grouping[$this->groupId]);
        // $this->cc += 1;
        $last = end($this->orders);
        $this->orders[] = $last + 1;
        foreach ($this->fields as $field) {
            $this->emit('fieldUpdated', $field["field_name"], $field["customer_field_group_id"], $last+1, $this->grouping[$this->groupId][0]);
        }
    }

    public function render()
    {
        return view('livewire.input.group.multiple');
    }
}
