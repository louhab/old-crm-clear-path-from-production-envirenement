<?php

namespace App\Http\Livewire\Input\Group;

use Illuminate\Support\Facades\Schema;
use Livewire\Component;

class Element extends Component
{
    public $customer_case;
    public $fields;
    public $work;
    public $order;
    public $groupData;
    public $join;

    protected $listeners = ['addBlock' => 'multipleBlock'];

    protected function rules()
    {
        // dd($this->groupData);
        $skip = array('id', 'customer_id', 'updated_at', 'created_at');
        $columns = array_diff(Schema::getColumnListing($this->groupData[1]), $skip);
        $rules = [];
        foreach ($columns as $col) {
            $rules['work.' . $col] = 'required';
        }
        return $rules;
    }

    public function mount($customer_case, $fields, $order, $groupId, $grouping, $join) {
        $this->groupData = $grouping[$groupId];
        $this->customer_case = $customer_case;
        $this->join = $join;
        $this->fields = $fields;
        $this->order = $order;
        $customer_work = $this->groupData[2]::where('order', $order)->where('join', $this->join)->where('customer_id', $this->customer_case->customer->id)->first();
        if (!$customer_work) {
            // $customer_work = new Clear3WorkFields;
            /*$customer_work->customer_id = $this->customer_case->customer_id;
            $customer_work->order = $order;*/
            $customer_work = $this->groupData[2]::create([
                'customer_id' => $this->customer_case->customer_id,
                'order' => $order,
                'join'=> $this->join
            ]);
        }
        $this->work = $customer_work;
    }
    public function updated($propertyName)
    {
        // dd($propertyName);
        $this->work->save();
    }
    public function render()
    {
        return view('livewire.input.group.element');
    }
}
