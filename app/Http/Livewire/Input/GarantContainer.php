<?php

namespace App\Http\Livewire\Input;

use App\Models\CustomerFieldValue;
use App\Models\CustomerGarant;
use Livewire\Component;

class GarantContainer extends Component
{
    public $customer_case;
    public $sheet_id;
    public $fields;
    public $garants;

    protected $listeners = [
        "selectFieldUpdatedGarant" => "updateModel",
        'refreshComponent' => '$refresh'
    ];

    public function mount($customer_case, $fields)
    {
        $this->customer_case = $customer_case;
        $this->fields = $fields;
        $this->sheet_id = 1;
        $this->garants = \App\Models\CustomerGarant::where('customer_id', $customer_case->customer_id)->orderBy('id', 'ASC')->get();
        // dd($this->garants);
        // dd($fields);
    }

    public function hydrate()
    {
        // $this->emit('dehydrateGarantIncomeType');
    }

    public function updateModel($field, $index, $value)
    {
        //bdd($value);
        if (is_array($value)) {
            $customer_values = CustomerFieldValue::where('customer_id', $this->customer_case->customer_id)
                ->where('field_name', $field["field_name"])
                ->where('customer_garant_id', $this->garants[$index]->id);
            // dd($customer_values->get()->toArray(), $this->garants[$index]->id);
            foreach ($customer_values->get() as $val) $val->delete();
            foreach ($value as $val) {
                CustomerFieldValue::create([
                    'customer_id' => $this->customer_case->customer_id,
                    'customer_garant_id' => $this->garants[$index]->id,
                    'field_name' => $field["field_name"],
                    'model_id' => $val
                ]);
            }
        } else $this->garants[$index]->update([$field["field_name"] => $value]);
        // $this->garants[$index]->save();
        $this->garants = \App\Models\CustomerGarant::where('customer_id', $this->customer_case->customer_id)->orderBy('id', 'ASC')->get();
        // $this->emit('refreshComponent');
        $this->emit('dehydrateGarantIncomeType');
    }
    public function addGarant()
    {
        CustomerGarant::create([
            'customer_id' => $this->customer_case->customer_id,
        ]);
        $this->garants = \App\Models\CustomerGarant::where('customer_id', $this->customer_case->customer_id)->orderBy('id', 'ASC')->get();
    }
    public function render()
    {
        return view('livewire.input.garant-container');
    }
}
