<?php

namespace App\Http\Livewire\Wizard;

use App\Models\CustomerClear1000Fields;
use App\Models\CustomerFieldValue;
use App\Models\DocumentType;
use App\Models\GuarantorIncomeType;
use App\Models\GuarantorType;
use App\Models\LevelOfStudy;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media as CustomerMedia;

class Media extends Component
{
    public $customerCase;
    public $mediaSheet;
    public $docs;
    public $tags;
    public $groupedArray;
    public $dropzoneAction;
    public $medias;
    public $tabActive;

    protected $listeners = [
        "mediaAdded" => "updateMedias"
    ];

    public function mount($customerCase, $mediaSheet) {
        $this->mediaSheet   = $mediaSheet;
        $this->customerCase = $customerCase;
        $this->docs = new Collection;
        $this->docs = $this->docs->merge(
            $this->mediaSheet->docs()
                ->wherePivot('program_id', '=', $this->customerCase->program_id)
                ->withPivot(['required', 'order'])
                ->orderBy('order')
                ->orderBy('name_fr')
                ->get()
        );
        /*foreach ($this->docs as $docum) {
            dd($docum->tagNames());
        }*/
        $this->docs =$this->docs->sortBy(function ($document, $key) {
            // dd($document->pivot->order);
            return $document->pivot->order;
        })->toArray();
        $this->groupedArray = [];
        $clear_fields = \App\Models\CustomerClear1000Fields::where('customer_id', $customerCase->customer->id)->first();
        $relationship_guarantor = null;
        if ($clear_fields && !is_null($clear_fields->relationship_student_guarantor)) {
            $relationship_guarantor = GuarantorType::find($clear_fields->relationship_student_guarantor);
            if ($relationship_guarantor) $relationship_guarantor = $relationship_guarantor->garant_label_fr;
            else $relationship_guarantor = null;
        }
        $level = null;
        if ($clear_fields && !is_null($clear_fields->level_study)) {
            $level = LevelOfStudy::find($clear_fields->level_study);
            if ($level) $level = $level->level_label_fr;
            else $level = null;
        }
        $incomes = CustomerFieldValue::where('customer_id', $customerCase->customer->id)->where('field_name', "guarantor_income_type")->pluck('model_id');
        $income_types = null;
        if (count($incomes)) {
            $income_types = GuarantorIncomeType::whereIn("id", $incomes)->pluck("label");
        }
        // dd($income_types);
        foreach ($this->docs as $index => $doc) {
            $this->groupedArray[$doc["document_type_group_id"]][] = $doc;
            $ddt = DocumentType::find($doc["id"]);
            if (count($ddt->tagNames()) > 0){
                if (!is_null($relationship_guarantor)) {
                    // dd($level->level_label_fr, $ddt->tagNames());
                    $res = array_filter($ddt->tagNames(), function ($item) use ($relationship_guarantor) {
                        return strtolower($item) == strtolower($relationship_guarantor);
                    });
                    if (count($res)>0) $this->docs[$index]["isVisible"] = true;
                    else $this->docs[$index]["isVisible"] = false;
                }
                if (!is_null($level)) {
                    // dd($level->level_label_fr, $ddt->tagNames());
                    $res = array_filter($ddt->tagNames(), function ($item) use ($level) {
                        return strtolower($item) == strtolower($level);
                    });
                    if (count($res)>0) $this->docs[$index]["isVisible"] = true;
                    else $this->docs[$index]["isVisible"] = false;
                }
                if (!is_null($income_types) && count($income_types)) {
                    foreach ($income_types as $income_type) {
                        $res = array_filter($ddt->tagNames(), function ($item) use ($income_type) {
                            return strtolower($item) == strtolower($income_type);
                        });
                        if (count($res)>0) {
                            $this->docs[$index]["isVisible"] = true;
                            break;
                        }
                        //else $this->docs[$index]["isVisible"] = false;
                    }
                    if (!isset($this->docs[$index]["isVisible"])) $this->docs[$index]["isVisible"] = false;
                }
            }
        }
        // dd($this->docs);
        // dd($level->level_label_fr,$this->docs);
        if (count($this->docs)) {
            $this->dropzoneAction = route(
                isCustomer() ? 'customer-add-media' : 'add-media',
                [
                    "customer" => $this->customerCase->customer,
                    "collectionName" => $this->docs[0]["collection_name"]
                ]
            );
        }
        $document = DocumentType::find($this->docs[0]["id"]);
        $this->tags = implode(",", $document->tagNames());
        // dd($this->docs);
        $this->tabActive = 0;
        $this->updateMedias();
        // $this->emit('eventName');
        // $this->emit('toggleDropzone', ['count' => count($this->medias)]);
    }

    public function updateDocumentTags() {
        // dd($this->docs[$index], $index);
        $document = DocumentType::find($this->docs[$this->tabActive]["id"]);
        $document->retag(explode(",", $this->tags));
        // dd(), $document->tags);
    }

    public function toggle($index) {
        if (count($this->docs) == 0) {
            return 0;
        }
        $this->tabActive = $index;
        // dd($this->docs[$index], $index);
        $collection = $this->docs[$index]["collection_name"];
        $this->dropzoneAction = route(
            isCustomer() ? 'customer-add-media' : 'add-media',
            [
                "customer" => $this->customerCase->customer,
                "collectionName" => $collection
            ]
        );
        $this->updateMedias();
        // $this->emit('documentChanged', $this->docs[$index]->id);
        // $this->emit('tabActive', ['tabId' => $index]);
    }

    public function downloadMedia(CustomerMedia $media) {
        return $media;
    }

    public function updateMedias() {
        // dd(count($this->docs));
        if (count($this->docs) == 0) {
            return 0;
        }
        $document = $this->docs[$this->tabActive];
        $collection = $document["collection_name"];
        $this->medias = $this->customerCase->customer->getMedia($collection);
        $this->emit('toggleDropzone', ['count' => count($this->medias), 'is_multiple' => $document["is_multiple"]]);
        $document = DocumentType::find($this->docs[$this->tabActive]["id"]);
        $this->tags = implode(",", $document->tagNames());
    }

    public function removeMedia(CustomerMedia $media) {
        $media->delete();
        $this->updateMedias();
        $this->emit('deletedMedia', ['mediaId' => $media->id]);
    }

    public function render()
    {
        return view('livewire.wizard.media');
    }
}
