<?php

namespace App\Http\Livewire\Modal;

use Livewire\Component;

class ProgramCourse extends Component
{
    public $columns;
    public $program_modal;
    public $action;
    public $selectValues;
    public $filter_keys;

    protected $listeners =[
        // "tagAdded" => "updateValues",
        "toggleModalAction" => "toggleAction",
        "loadProgramModal" => "setProgramModal",
    ];
    public function mount($columns) {
        $this->columns = $columns;
        $this->program_modal = [];
        $this->action = "add";
        $this->selectValues = [];
        $this->filter_keys = ['domaine_etude', 'test_langue','langue', 'ville', 'session', 'diplome', 'type_etablissement',
            'concentration', 'nom_etablissement', 'niveau_scolaire', 'prix_total', 'prix_annuel', 'frais_admission',
            'score', 'note', 'duree', 'lien', 'exigence', 'analyse', 'budget'];
        foreach ($this->filter_keys as $key) {
            $pcf = \App\Models\ProgramCourseField::where('field_name', $key)->first();
            $this->selectValues[$key] = $pcf->values;
        }
    }

    public function hydrate() {
        // $this->emit('selectInit', $this->program_modal);
    }

    public function render()
    {
        return view('livewire.modal.program-course');
    }
    public function setProgramModal($pid) {
        $program = \App\Models\ProgramCourse::find($pid);
        // dd($program, $pid);
        // $this->program_modal = $program["properties"];
        $this->program_modal = array_map(function ($item){
            if (array_key_exists('values', $item) && is_array($item["values"]) && count($item["values"])) {
                return $item["values"];
            } elseif (array_key_exists('value', $item))
                return $item["value"];
            else return "";
        }, $program->properties);
        $this->program_modal["id"] = $program->id;
        $this->program_modal["programme"] = $program->name;
        // dd($program);
        // $this->program_modal["programme"]["value"] = $program["name"];
    }
    /*public function updateValues($key , $value) {
        // dd($key , $value);
        $field = \App\Models\ProgramCourseField::where('field_name', $key)->first();
        if ($field){
            $values = $field->values;
            if (in_array($value, $values)) {
                $values[] = $value;
                $field->values = $values;
                $field->save();
            }
            // $this->selectValues[$key] = \App\Models\ProgramCourseField::where('field_name', $key)->first()->values;
        }
        //dd($values);
    }*/

    public function toggleAction($action) {
        $this->action = $action;
        if ($action == "add") {
            $this->program_modal = [];
        }
        if ($action == "update" || $action == "add") {
            $this->emit('selectInit', $this->program_modal);
        }
    }
    public function updateProgram() {
        // dd($this->program_modal);
        if (array_key_exists("lien", $this->program_modal) && array_key_exists("programme", $this->program_modal)) {
            // OrientationSchool::create($this->program_modal);
            $p = \App\Models\ProgramCourse::find($this->program_modal["id"]);
            if ($p) {
                $properties = [];
                foreach ($this->columns as $label => $key) {
                    if ($key == "date_limit_admission") {
                        if (!isset($this->program_modal[$key]["day"])) $dd = 0;
                        else $dd = $this->program_modal[$key]["day"];
                        if (!isset($this->program_modal[$key]["month"])) $mm = 0;
                        else $mm = $this->program_modal[$key]["month"];
                        $properties[$key]["key"] = $label;
                        $properties[$key]["value"] = array("day" => $dd, "month" => $mm);
                    }
                    elseif ($label == "Programmes") {
                        $p->name = $this->program_modal["programme"];
                    } else {
                        $value = trim($this->program_modal[$key]);
                        $properties[$key]["key"] = $label;
                        $properties[$key]["value"] = $value;
                    }
                }
                $p->properties = $properties;
                $p->save();
                // $p->update($this->program_modal);
            }
            $this->emit('closeModal');
            // return redirect(request()->header('Referer'));
        } else {
            $errors = $this->getErrorBag();
            // With this error bag instance, you can do things like this:
            if (!array_key_exists("lien", $this->program_modal)) {
                $errors->add('lien', 'le lien est obligatoire');
            }
            if (!array_key_exists("programme", $this->program_modal)) {
                $errors->add('programme', 'le programme est obligatoire');
            }
        }
    }
    public function addProgram() {
        dd("add");
    }
}
