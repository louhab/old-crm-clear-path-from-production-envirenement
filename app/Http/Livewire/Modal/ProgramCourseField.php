<?php

namespace App\Http\Livewire\Modal;

use Livewire\Component;
use App\Models\ProgramCourse;

class ProgramCourseField extends Component
{
    public $field;
    // public $values;
    public $title;
    public $value;

    protected $listeners = ["programCourseField" => "setField"];

    public function mount()
    {
        $this->field = null;
        $this->title = null;
    }
    public function deleteValue($value)
    {
        dd("tut");
        $value = stripslashes($value);
        if (!is_null($this->field)) {
            $values = $this->field->values;
            $key = array_search($value, $values);
            if ($key !== false) {
                // dd($key, $values);
                unset($values[$key]);
                $this->field->values = array_values($values);
                $this->field->save();
                $this->emit("closeProgramCourseField", ["valueDeleted", $this->field->field_name, $value]);
                $this->emit("showProgramCourseField");
            }
            // dd($this->value, $values);
        }
    }
    public function addValue()
    {
        if (!empty($this->value)) {
            $values = $this->field->values;
            $values[] = $this->value;
            $this->field->values = $values;
            $this->field->save();
            // dd($this->value, $values);
        }
        /*else {
            $this->emit("showProgramCourseField");
        }*/
        $this->emit("closeProgramCourseField", $this->field->field_name);
    }
    public function setField($field_name, $title)
    {
        // $this->field_name = $field_name;
        $this->field = \App\Models\ProgramCourseField::where('field_name', $field_name)->first();
        $this->title = $title;
        // ProgramCourse::where("name", $field_name)->first();
        // dd($this->field["field_name"], $this->title);
        $this->emit("showProgramCourseField");
        // dd($this->field);
    }
    public function render()
    {
        return view('livewire.modal.program-course-field');
    }
}
