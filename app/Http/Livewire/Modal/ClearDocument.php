<?php

namespace App\Http\Livewire\Modal;

use App\Models\DocumentType;
use Livewire\Component;

class ClearDocument extends Component
{
    public $customerCase;
    public $mediaSheet;
    public $dropzoneAction;
    public $title;
    public $garant;
    public $tag;

    protected $listeners = ["modalShown" => "updateDropzoneAction"];

    public function mount($customerCase, $mediaSheet) {
        $this->customerCase = $customerCase;
        $this->mediaSheet = $mediaSheet;
        $this->dropzoneAction = "/";
        $this->title = "";
        $this->garant = null;
        $this->tag = "";
    }

    public function updateDropzoneAction($document_id, $garant){
        $document = DocumentType::find($document_id);
        $this->title = $document->name_fr;
        $this->garant = $garant;
        // dd($garant);
        if ($this->mediaSheet->id != 2) $this->garant = null;
        $tags = $document->tagNames();
        if (count($tags)) $this->tag = $tags[0];
        else $this->tag = "Document partagés";
        $this->dropzoneAction = route(
            isCustomer() ? 'customer-add-media' : 'add-media',
            [
                "customer" => $this->customerCase->customer,
                "collectionName" => $document->collection_name
            ]
        );
        $this->emit("showClearModal");
    }

    public function render()
    {
        return view('livewire.modal.clear-document');
    }
}
