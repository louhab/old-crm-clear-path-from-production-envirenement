<?php

namespace App\Http\Livewire\Modal;

use App\Models\Customer;
use App\Models\CustomerCase;
use App\Models\User;
use Carbon\Carbon;
use Livewire\Component;

class CommentInfo extends Component
{
    public $customer_case;
    public $comment;
    public $canal;

    protected $listeners = [
        "commentInfo" => "setCustomerCase",
        "clearCommentInfo" => "clear"
    ];

    public function mount() {
        $this->customer_case = null;
        $this->comment = null;
        $this->canal = null;
    }

    public function render()
    {
        return view('livewire.modal.comment-info');
    }

    public function clear($id) {
        // clearCommentInfo
        $this->customer_case = null;
        $this->comment = null;
        $this->canal = null;
    }
    public function setCustomerCase($id, $process) {
        $this->customer_case = CustomerCase::find($id);
        $this->comment = $this->customer_case->comments()->withPivot("canal", "view")->wherePivot("view", $process)->orderBy("created_at", "DESC")->first();
        // $this->comment = $this->comment;
        if ($this->comment) $this->canal = $this->comment->pivot->canal;
        // $this->comment->pivot->canal
        // if ($this->comment->created_at->toDateString() == Carbon::today()->toDateString()) $this->emit("setCellColor");
    }
}
