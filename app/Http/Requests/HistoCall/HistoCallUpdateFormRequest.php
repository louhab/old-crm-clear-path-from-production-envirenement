<?php

namespace App\Http\Requests\HistoCall;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class HistoCallUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit-form-call_dt'=> 'required',
            'edit-form-duration'=> 'required',
            'edit-form-call_descr'=> 'required',
        ];
    }
}
