<?php

namespace App\Http\Requests\HistoCall;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class HistoCallAddFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'add-form-calldt'=> '',
            'add-form-duration'=> 'required',
            //'add-form-call_descr'=> 'required',
            'add-form-lead_id'=> 'required',
        ];
    }
}
