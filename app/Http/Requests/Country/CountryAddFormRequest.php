<?php

namespace App\Http\Requests\Country;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CountryAddFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'add-form-code' => 'required',
            'add-form-alpha2' => 'required',
            'add-form-alpha3' => 'required',
            'add-form-name_fr_fr' => 'required',
            'add-form-name_en_gb' => 'required',
        ];
    }
}
