<?php

namespace App\Http\Requests\Country;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CountryUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit-form-code' => 'required',
            'edit-form-alpha2' => 'required',
            'edit-form-alpha3' => 'required',
            'edit-form-name_fr_fr' => 'required',
            'edit-form-name_en_gb' => 'required',
        ];
    }
}
