<?php

namespace App\Http\Requests\LeadForm;

use Illuminate\Foundation\Http\FormRequest;

class GetInTouchFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'program' => 'required',
            'firstname' => 'required|min:3|max:30',
            'lastname' => 'required|min:3|max:30',
            'email' => 'required|email',
            'birthday' => 'nullable|date',
            'phone' => 'nullable|digits_between:9,15',
            'file-input-cv' => 'nullable|file|mimes:pdf,doc,docx,ods',
        ];
    }
}
