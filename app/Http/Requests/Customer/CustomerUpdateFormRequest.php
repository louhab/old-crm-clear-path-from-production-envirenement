<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CustomerUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'program' => 'required',
            // 'budget' => 'nullable|numeric|between:0,99999999.99',
            // 'experience' => 'nullable|numeric|min:0',//
            'firstname' => 'required_if:sheet,3|required_without_all:sheet|min:3|max:30',
            'lastname' => 'required_if:sheet,3|required_without_all:sheet|min:3|max:30',
            'email' => 'required_if:sheet,3|required_without_all:sheet|email',
            'phone' => 'required_if:sheet,3|required_without_all:sheet',
            // 'file-input-cv' => 'nullable|file|mimes:pdf,doc,docx,ods',
            // |size:10|regex:/(0)\d{9}/
            'campaign_id' => 'required_if:sheet,3|required_without_all:sheet|exists:lead_form_marketing_campaigns,id',
            'lead_status' => 'required_if:sheet,3|required_without_all:sheet|exists:lead_statuses,id',

            //sheet 1
            // 'level_study' => 'required_if:sheet,1',
            // 'branch' => 'required_if:sheet,1',
            // 'grade' => 'required_if:sheet,1',
            // 'lang_level' => 'required_if:sheet,1',
            // 'diploma_fr' => 'required_if:sheet,1',
            // 'diploma_en' => 'required_if:sheet,1',
            // 'test_fr' => 'required_if:sheet,1',
            // 'test_en' => 'required_if:sheet,1',
            // 'garant_type' => 'required_if:sheet,1',
            // 'income' => 'required_if:sheet,1',
            // 'amount' => 'required_if:sheet,1',
            // 'goods' => 'required_if:sheet,1',
            // 'business_income' => 'required_if:sheet,1',
            // 'rental_income' => 'required_if:sheet,1',
            // 'medical_visit' => 'required_if:sheet,1',
            // 'health' => 'required_if:sheet,1',
            // 'anthroprometric' => 'required_if:sheet,1',
            // 'criminal_record' => 'required_if:sheet,1',
            // '' => 'required_if:sheet,1',
            // '' => 'required_if:sheet,1',
        ];
    }
}
