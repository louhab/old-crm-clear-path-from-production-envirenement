<?php

namespace App\Http\Requests\Lead;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class LeadImmigrantUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit-form-lastname' => 'required',
            'edit-form-firstname' => 'required',
            'edit-form-email' => 'required|email',
            'edit-form-languagelevel' => 'required',
            'edit-form-schoollevel' => 'required',
            'edit-form-country' => 'required',
            'edit-form-adresse1' => 'required',
            'edit-form-experience' => 'required|numeric'
        ];
    }
}
