<?php

namespace App\Http\Requests\Lead;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class LeadImmigrantAddFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'add-form-lastname' => 'required',
            'add-form-firstname' => 'required',
            'add-form-email' => 'required|email',
            'add-form-languagelevel' => 'required',
            'add-form-schoollevel' => 'required',
            'add-form-country' => 'required',
            'add-form-adresse1' => 'required',
            'add-form-experience' => 'required|numeric'
        ];
    }
}
