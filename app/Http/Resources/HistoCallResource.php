<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HistoCallResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'lead_id' => $this->lead_id,
            'customer_id' => $this->customer_id,
            "call_dt" => $this->call_dt,
            "duration" => $this->duration,
            "call_descr" => $this->call_descr,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
