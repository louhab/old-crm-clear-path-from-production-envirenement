<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'alpha2' => $this->alpha2,
            'alpha3' => $this->alpha3,
            'name_fr_fr' => $this->name_fr_fr,
            'name_en_gb' => $this->name_en_gb,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
