<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\Service;
use App\Models\Bill;
use App\Models\User;
use Illuminate\Validation\ValidationException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function createBill($customer)
    {
        //devis
        if ($customer->program_id == 4) {
            $program_id = 1;
        } else {
            $program_id = 2;
        }
        $services = Service::with(['phase', 'details'])
            ->where('program_id', $program_id)
            ->where('billable', 1)
            ->get();

        $bill = NULL;
        DB::transaction(function () use ($customer, $services, &$bill) {
            $billArr = [
                'user_id' => Auth::id(),
                'customer_id' => $customer->id
            ];

            //if($request->has('ap')) $billArr['applied_price'] = $request['ap'];

            $bill = Bill::create($billArr);

            $servicesToAttach = [];

            foreach ($services as $service) {
                $servicesToAttach[$service->id] = [
                    'billed_price' => $service->single_price,
                    'qte' => 1
                ];
            }

            $bill->services()->sync($servicesToAttach);
        });
        return $bill;
    }
    protected function addMediaPDF($customer, $documentType, $prefix, $quote, $step_state)
    {
        $collectionName = $documentType->collection_name;
        $pathToFile = $prefix . '-' . str_replace(" ", "_", $customer->lastname) . '-' . str_replace(" ", "_", $customer->firstname) . ' '  . '.pdf';
        $path = "C:\\Users\\admin\\Desktop\\work\\temp\\";
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        // If the user file in existing directory already exist, delete it
        else if (file_exists($path . $pathToFile)) {
            unlink($path . $pathToFile);
        }

        if ($prefix == 'bill') {
            // $bill = $quote;
            // $pdf_clear = PDF::loadView('pdfs.' . $prefix, compact('bill', 'step_state'));
            // FIXME @@@
            if ($customer->program_id == 4) {
                $pdf_clear = PDF::loadView('pdfs.' . $prefix . '-es', compact('customer'));
            } else if ($customer->program_id == 31) {
                $pdf_clear = PDF::loadView('pdfs.' . $prefix . '-uk', compact('customer'));
            } else if ($customer->program_id == 32) {
                $pdf_clear = PDF::loadView('pdfs.' . $prefix . '-es', compact('customer'));
            } else {
                $clear1004 = \App\Models\CustomerClear1004Fields::where("customer_id", $customer->id)->first();
                if ($clear1004 && $clear1004->spouse_joining == "spouse_joining-B") {
                    $pdf_clear = PDF::loadView('pdfs.' . $prefix . '-couple-1', compact('customer'));
                } else
                    $pdf_clear = PDF::loadView('pdfs.' . $prefix . '-single-1', compact('customer'));
            }
            //dd(gettype($step_state->name_fr),gettype($collectionName));
            $collectionName = $step_state->name_fr . $documentType->collection_name;
        } elseif ($prefix == 'clear1004') {
            //$bill = $quote;
            $scoring = $quote;
            $pdf_clear = PDF::loadView('pdfs.' . $prefix, compact('customer', 'scoring'));
        } elseif ($prefix == 'devis') {
            // FIXME add UK and ES
            if ($customer->program_id == 4)
                $pdf_clear = PDF::loadView('pdfs.' . $prefix . '-es', compact('customer'));
            else {
                $clear1004 = \App\Models\CustomerClear1004Fields::where("customer_id", $customer->id)->first();
                if ($clear1004 && $clear1004->spouse_joining == "spouse_joining-B") {
                    $pdf_clear = PDF::loadView('pdfs.devis-couple-1', compact('customer'));
                } else
                    $pdf_clear = PDF::loadView('pdfs.devis-single-1', compact('customer'));
            }
        } elseif ($prefix == 'contrat') {
            if ($customer->program_id == 4)
                $pdf_clear = PDF::loadView('pdfs.contrat-4', compact('quote', 'customer'));
            else {
                $clear1004 = \App\Models\CustomerClear1004Fields::where("customer_id", $customer->id)->first();
                if ($clear1004 && $clear1004->spouse_joining == "spouse_joining-B") {
                    $pdf_clear = PDF::loadView('pdfs.contrat-couple-1', compact('quote', 'customer'));
                } else
                    $pdf_clear = PDF::loadView('pdfs.contrat-single-1', compact('quote', 'customer'));
            }
        } elseif (is_null($quote) || $quote === "null") {
            if ($prefix == "recu") {
                // dd($step_state);
                $pdf_clear = PDF::loadView('pdfs.' . $prefix, compact('customer', 'step_state'));
                $pdf_clear->setPaper([0, 0, 100.98, 120.85], 'landscape');
                $collectionName = $step_state->name_fr . $documentType->collection_name;
            } else {
                $pdf_clear = PDF::loadView('pdfs.' . $prefix, compact('customer'));
            }
        } else {
            $pdf_clear = PDF::loadView('pdfs.' . $prefix, compact('quote'));
        }
        $pdf_clear->save($path . $pathToFile);

        foreach ($customer->getMedia($collectionName) as $media) $media->delete();

        $customer->addMedia($path . $pathToFile)
            ->withCustomProperties([
                'original_file_id' => 0,
                'pending_file_id' => 0,
                'visible_for_customer' => 1,
                'signable' => $documentType->signable,
                'customer_owner' => $documentType->customer_owner,
                'is_signed' => 0,
                'is_requested' => 0,
            ])
            ->toMediaCollection($collectionName);
        if ($prefix == 'contrat') {
            // $this->prepareForSigning($customer, $collectionName);
        }
        return $pdf_clear;
    }
    protected function prepareSigningDocument(Customer $customer, string $collectionName)
    {
        $conseiller = User::where('id', '=', $customer->lead->conseiller_id)->first();
        if ($conseiller === null) {
            // user doesn't exist
            return false;
        }
        // $this->createAccessToken();
        $media = $customer->getFirstMedia($collectionName);
        if ($media === null)
            return false;
        $mediaPath = $customer->getFirstMediaPath($collectionName);
        $mediaUrl = $customer->getFirstMediaUrl($collectionName);
        // api_key 7414162f9b6e0649b7ed809c7eabe96eff976101
        $headers = [
            'Authorization' => 'e0a299fe4f471abfd82a52ebff1c49c237f4dd5b',
            'Authorization' => 'API-Key 7414162f9b6e0649b7ed809c7eabe96eff976101',
            'Content-Type'  => 'multipart/form-data'
        ];

        $client = new Client([
            'headers' => $headers
        ]);
        if (!$media->hasCustomProperty('file_id')) {
            try {
                $res = $client->request('POST', 'https://api.pandadoc.com/public/v1/documents', [
                    'multipart' => [
                        [
                            'name' => 'file',
                            'contents' => file_get_contents($mediaPath),
                            'filename' => basename($mediaPath)
                        ],
                        [
                            'name' => 'data',
                            'contents' => json_encode([
                                "name" => basename($mediaPath),
                                "url" => $mediaUrl,
                                "recipients" => [
                                    [
                                        "email" => $customer->email,
                                        "first_name" => $customer->firstname,
                                        "last_name" => $customer->lastname,
                                        "role" => "signer",
                                        "signing_order" => 1
                                    ],
                                    [
                                        "email" => "oussama@boom-digital.ma",
                                        "first_name" => "clear",
                                        "last_name" => "path",
                                        "role" => "user",
                                        "signing_order" => 2
                                    ],
                                ],
                                "fields" => [
                                    "signature" => [
                                        "value" => "",
                                        "role" => "user"
                                    ]
                                ],
                                "parse_form_fields" => true
                            ])
                        ]
                    ],
                ]);
            } catch (\Exception $e) {
                return false;
            }
            $response = json_decode($res->getBody()->getContents(), true);

            if (is_array($response) && array_key_exists('id', $response)) {
                $media->setCustomProperty('file_id', $response['id']);
                $media->save();
            }
        }
        do {
            $res = $client->get('https://api.pandadoc.com/public/v1/documents/' . $media->getCustomProperty('file_id'));
            $res = json_decode($res->getBody()->getContents(), true);
        } while (is_array($res) && array_key_exists('status', $res) && $res['status'] == 'document.uploaded');
        // if (is_array($response) && array_key_exists('id', $response)) {
        if ($res['status'] == 'document.draft') {
            $res = $client->post('https://api.pandadoc.com/public/v1/documents/' . $media->getCustomProperty('file_id') . '/send', [
                'json' => [
                    "message" => "Hello! This document was sent from the Clearpath CRM.",
                    "silent" => true
                ]
            ]);
            $res = json_decode($res->getBody()->getContents(), true);
        }
        // get session_id
        if ($res['status'] == 'document.sent' || $res['status'] == 'document.viewed') {
            $media->setCustomProperty('original_file_id', $media->getCustomProperty('file_id'));
            $media->setCustomProperty('pending_file_id', $media->getCustomProperty('file_id'));
            $media->save();
            /*[
              "id" => "KenPKvYcJsY95t53S3aed2"
              "name" => "contrat-Azour-Yassine-2021-12-24-15:39:37.pdf"
              "status" => "document.sent"
              "date_created" => "2021-12-24T16:23:03.073992Z"
              "date_modified" => "2021-12-24T16:24:35.968015Z"
              "date_completed" => null
              "expiration_date" => "2022-02-22T16:24:35.843720Z"
              "version" => "2"
              "uuid" => "KenPKvYcJsY95t53S3aed2"
              "recipients" => array:2 [▶]
            ]*/
            // return redirect()->route('signeasy-send-signing-request', ['customer' => $customer->id, 'collectionName' => $collectionName]);
            return true;
        } else {
            return false;
        }
    }
}
