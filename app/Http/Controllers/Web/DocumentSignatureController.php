<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Lead;
use App\Models\LeadStateLog;
use App\Models\LeadStatus;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class DocumentSignatureController extends Controller
{
    public function index() {
        return view('easysign.sign-iframe');
    }

    public function showPage(Customer $customer) {
        $collectionName = \App\Models\DocumentType::where('name_fr', 'Contrat client')->first()->collection_name;
        $media = $customer->getFirstMedia($collectionName);
        $media->setCustomProperty('is_signed', 1);
        $media->save();
        //$caseID = $customer->case->id;
        //$customer = \App\Models\CustomerCase::where("id", $caseId)->first();
        $customer->lead_status = LeadStatus::Client;
        $customer->is_client = 1;
        $customer->save();
        $lead = Lead::find($customer->lead_id)->first();
        $old_state = $lead->lead_status_id;
        $lead->lead_status_id = LeadStatus::Client;
        $lead->save();
        if ($old_state != LeadStatus::Client) {
            LeadStateLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'state_id' => $lead->lead_status_id,
            ]);
        }
        return view('easysign.redirect-page')->with(['program_step' => 2]);
    }
    public function toggleSigned(Request $request) {
        // $media->setCustomProperty('is_signed', 1);
        $data = $request->all()[0];
        $media = Media::where('custom_properties->file_id', $data['data']['id'])->first();
        if ($media && $data['data']['status'] == 'document.completed') {
            $media->setCustomProperty('is_signed', 1);
            $media->save();
            return response()->json(['customer_id' => $media->model_id, 'file_id' => $data['data']['id']]);
        }
        return response()->json(['file_id' => $data['data']['id'], 'status' => $data['data']['status']]);
    }

}
