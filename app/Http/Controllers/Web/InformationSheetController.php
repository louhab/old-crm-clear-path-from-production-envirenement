<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\CustomerCase;
use App\Models\InformationSheet;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class InformationSheetController extends Controller
{
    /**
     * Display & Edit information sheet.
     *
     * @return Application|Factory|Response|View
     */
    public function edit(CustomerCase $customerCase, InformationSheet $informationSheet, $join = null)
    {
        // dd("test conflict -- from information", count($customerCase->customer->sheets()->wherePivot('information_sheet_id', '=', $informationSheet->id)->wherePivot('can_edit', '=', true)->get()));
        if (isCustomer() && !count($customerCase->customer->sheets()->wherePivot('information_sheet_id', '=', $informationSheet->id)->wherePivot('can_edit', '=', true)->get()))
            abort(404);
        $data = ['informationSheet', 'customerCase'];
        if (!is_null($join)) {
            $data[] = 'join';
        }
        return view('cases.sheet', compact($data));
    }
}
