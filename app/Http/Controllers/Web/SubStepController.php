<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ProgramSubStep;

// use App\Models\MediaSheet;
// use Illuminate\Http\Request;

class SubStepController extends Controller
{
    public function index()
    {
        return view('steps.index');
    }
    public function view(ProgramSubStep $step)
    {
        // dd($sheet);
        return view('steps.view', compact('step'));
    }
}
