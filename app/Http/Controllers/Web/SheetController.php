<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\InformationSheet;
use App\Models\MediaSheet;

// use App\Models\MediaSheet;
// use Illuminate\Http\Request;

class SheetController extends Controller
{
    public function index()
    {
        return view('sheets.index');
    }
    public function view(InformationSheet $sheet)
    {
        // dd($sheet);
        return view('sheets.view', compact('sheet'));
    }
}
