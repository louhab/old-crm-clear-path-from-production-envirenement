<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\CustomerField;
use Illuminate\Http\Request;

class CustomerFieldController extends Controller
{
    public function index()
    {
        return view('fields.index');
    }
    public function view(CustomerField $field)
    {
        return view('documents.view-steps', compact('field'));
    }
}
