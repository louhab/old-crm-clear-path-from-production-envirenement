<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\CustomerCase;
use App\Models\CustomerClear1000Fields;
use App\Models\CustomerFieldValue;
use App\Models\CustomerRadioField;
use App\Models\ProgramCourse;
use App\Models\ProgramCourseField;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

use App\Models\Customer;

class ProgramCourseController extends Controller
{
    private $filter_keys = [
        'domaine_etude', 'test_langue', 'langue', 'ville', 'session', 'diplome', 'type_etablissement',
        'concentration', 'nom_etablissement', 'niveau_scolaire', 'prix_total', 'prix_annuel', 'frais_admission',
        'score', 'note', 'duree', 'lien', 'exigence', 'analyse', 'budget'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function view(ProgramCourse $program)
    {
        return view('orientation.view', compact('program'));
    }

    public function index()
    {
        return view('orientation.sheet');
    }

    public function view1(CustomerCase $customer_case)
    {
        if (Auth::user()->isManager()) {
            return redirect()->route('case-edit', ['customer_case' => $customer_case->id]);
        }
        // $customer_case = \App\Models\CustomerCase::where('customer_id', $client->id)->first();
        return View::make('admissions.view', compact('customer_case'));
    }
}
