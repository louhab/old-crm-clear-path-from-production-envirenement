<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\SelectedLeadsSupport;
use Illuminate\Http\Request;

class AffectationSupportLeadController extends Controller
{
    public function update(Request $request){
        SelectedLeadsSupport::where('id',1)->update(['number'=>$request->input('edit-form-lead-support')]);
        return redirect()->back();
    }
}
