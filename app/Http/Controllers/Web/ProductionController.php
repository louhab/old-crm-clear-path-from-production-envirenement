<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;

class ProductionController extends Controller
{
    public function index()
    {
        return view('production.index');
    }

    public function view(User $user)
    {
        return view('production.view', compact('user'));
    }
}
