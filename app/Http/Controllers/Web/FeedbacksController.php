<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\FeedbackHistory;
use Illuminate\Http\Request;

class FeedbacksController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('id'))
            $feedback = FeedbackHistory::where("uuid", $request->input('id'))->where("submitted", 0)->firstOrFail();
        else
            $feedback = null;

        if ($request->has('ville')) {
            $city = $request->input('ville');
        } else
            $city = null;

        return view('feedback.index', compact('city', 'feedback'));
    }
}
