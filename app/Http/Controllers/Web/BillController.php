<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Bill;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        if(Auth::guard('customer')->user()) {
            $bills = Bill::with(['customer', 'services'])
                ->where('customer_id', Auth::id())
                ->paginate(15);
            return view('billing.mybills', compact('bills'));
        }
        return view('billing.bills');
    }
}
