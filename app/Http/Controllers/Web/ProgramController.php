<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Stevebauman\Location\Facades\Location;

class ProgramController extends Controller
{
    public function index()
    {
        $position = Location::get(\Request::ip());
        $langue = \Request::server('HTTP_ACCEPT_LANGUAGE');
        dd($langue, $position);
        return view('programs.index');
    }
}
