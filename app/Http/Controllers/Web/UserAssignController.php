<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\UserAssign;
use Illuminate\Http\Request;

class UserAssignController extends Controller
{
    public function index()
    {
        return view('users.rotation');
    }

    public function update(Request $request) {
        // dd($request->input());
        $rules = [
            'add-form-rotation' => 'required|in:3,6,7',
            'add-form-user' => 'required|exists:users,id',
            'add-form-lang' => 'required|in:fr,en',
            'add-form-country' => 'required|exists:countries,id',
        ];
        $this->validate($request, $rules);
        $rotation = UserAssign::where("role_id", $request["add-form-rotation"])->first();
        /*if (!$rotation) {
            $rotation = UserAssign::create([
                "role_id", $request["add-form-rotation"]
            ]);
        }*/
        dd($request->input());
    }
}
