<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Quote;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {

        if(Auth::guard('customer')->user()) {
            $quotes = Quote::with(['customer', 'services'])
                ->where('customer_id', Auth::id())
                ->paginate(15);
            return view('billing.myquotes', compact('quotes'));
        }
        return view('billing.quotes');
    }
}
