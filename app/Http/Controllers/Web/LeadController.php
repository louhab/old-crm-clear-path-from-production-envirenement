<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Imports\LeadssImport;
use App\Models\AssignmentLog;
use App\Models\CaseState;
use App\Models\DocumentType;
use App\Models\Lead;
use App\Models\Customer;
use App\Models\CustomerCase;
use App\Models\SubStepState;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
//use Illuminate\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Models\CurrentSelectedConseiller;
use App\Models\User;
use App\Models\Role;

use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index(Request $request)
    {
        // dd($request->input());
        request()->request->remove('_token');
        return view('leads.index')->with("dashboard_filter", $request->input());
    }

    public function log()
    {
        return view('leads.log');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        $language = \Request::server('HTTP_ACCEPT_LANGUAGE');
        if (!session()->has('locale')) {
            if (str_starts_with($language, "fr")) {
                // App::setlocale('fr');
                session()->put('locale', 'fr');
            }
            if (str_starts_with($language, "en")) {
                // App::setlocale('en');
                session()->put('locale', 'en');
            }
        }
        // dd(session()->get('locale'));
        return view('leads.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Lead  $id
     * @return Application|Factory|Response|View
     */
    public function edit(Lead $lead)
    {
        return view('leads.edit', compact('lead'));
    }
    public function view(Request $request, Lead $lead)
    {
        // TBD ⏳
        // if (Auth::user()->isConseiller() || Auth::user()->isAgent() || Auth::user()->isManager()) {
        //     if (in_array($lead->lead_status_id, [null])) {
        //         abort(403);
        //     }
        // }

        // add affectation logs here
        // Edit 23 June => except "RDV1" from auto affectation
        if ($request->has('ref')) {
            if ($request->get('ref') == "true" && Auth::user()->isConseiller() && $lead->lead_status_id !== 4 && $lead->to_office === 'conference') {
                $lead->conseiller_id = Auth::id();
                AssignmentLog::create([
                    'user_id' => auth()->user()->id,
                    'lead_id' => $lead->id,
                    'conseiller_id' => Auth::id(),
                ]);
                $lead->save();
            }
        }
        if (!isset($lead->conseiller_id)) {
            // disabled
            if ($lead->program_id === 9 && false) {
                // Get hrGlobe Conseillers
                $conseillers_managers = User::whereIn('role_id', [Role::Conseiller, Role::Manager])->where('status', 1)->where(function ($query) {
                    $query->where("email", "like", "%hrglobe%")
                        ->orWhere("email", "=", "i.benyoub@cpcanada.ma");
                })->get()->pluck('id')->toArray();
                $conseillers = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->orderby('id', 'asc')->pluck('conseiller_id')->toArray();

                $current_conseiller = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("hr_status", 1)->first();
                $index = $current_conseiller ? array_search($current_conseiller->conseiller_id, $conseillers) : false;

                if ($index == count($conseillers) - 1 || $index === false) {
                    $next_conseiller = $conseillers[0];
                } else {
                    $next_conseiller = $conseillers[$index + 1];
                }
                CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("hr_status", 1)->update(['hr_status' => 0]);
                CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("conseiller_id", $next_conseiller)->update(['hr_status' => 1]);
            } else {
                $conseillers_managers = User::whereIn('role_id', [Role::Conseiller, Role::Manager])->where('status', 1)->where("email", "not like", "%hrglobe%")->where("email", "!=", "i.benyoub@cpcanada.ma")->where('lang', 'LIKE', 'fr')->get()->pluck('id')->toArray();
                $conseillers = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->orderby('id', 'asc')->pluck('conseiller_id')->toArray();

                $current_conseiller = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("status", 1)->first()->conseiller_id;
                $index = array_search($current_conseiller, $conseillers);

                if ($index == count($conseillers) - 1 || $index === false) {
                    $next_conseiller = $conseillers[0];
                } else {
                    $next_conseiller = $conseillers[$index + 1];
                }
                CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("status", 1)->first()->update(['status' => 0]);
                CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("conseiller_id", $next_conseiller)->first()->update(['status' => 1]);
            }
            $lead->conseiller_id = $next_conseiller;
            $lead->save();
            AssignmentLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'conseiller_id' => Auth::id(),
            ]);
        }

        /**
         * Queue for the support
         */
        if (!isset($lead->support_id)) {
            $supports = User::where('role_id', Role::Support)->where('status', 1)->get()->pluck('id')->toArray();
            $Qsupports = CurrentSelectedConseiller::whereIn('conseiller_id', $supports)->where("queue", "<>", 0)->orderby('id', 'asc')->pluck('queue', 'conseiller_id')->toArray();
            $LQsupports = CurrentSelectedConseiller::whereIn('conseiller_id', $supports)->where("left_queue", "<>", 0)->orderby('id', 'asc')->pluck('left_queue', 'conseiller_id')->toArray();

            $flag = 0;
            $bis_flag = 0;
            if (!!$LQsupports) {
                $current_support = CurrentSelectedConseiller::whereIn('conseiller_id', array_keys($LQsupports))->where("status", 1);
                if ($current_support->count()) {
                    $flag = 1;
                    $current_support = $current_support->first()->conseiller_id;
                    $index = array_search($current_support, array_keys($LQsupports));

                    if ((count($LQsupports) - 1 !== 0 && $index == count(array_keys($LQsupports)) - 1) || ($index == count(array_keys($LQsupports)) - 1 && count(array_keys($LQsupports)) - 1 === 0 && array_values($LQsupports)[$index] - 1 > 0)) {
                        $next_support = array_keys($LQsupports)[0];
                    } elseif (count($LQsupports) - 1 !== 0 && count($LQsupports) - 1 >= $index && array_values($LQsupports)[$index + 1] - 1 >= 0) {
                        $next_support = array_keys($LQsupports)[$index + 1];
                    } else {
                        // restart the cycle
                        $bis_flag = 1;
                        $next_support = array_keys($Qsupports)[0];
                    }

                    $left_queue = array_values($LQsupports)[$index];
                    // substitute the left_query
                    if ($flag) {
                        CurrentSelectedConseiller::whereIn('conseiller_id', array_keys($LQsupports))->where("status", 1)->first()->update(['status' => 0]);
                        CurrentSelectedConseiller::where("conseiller_id", $current_support)->update(['left_queue' => $left_queue - 1]);
                        CurrentSelectedConseiller::where("conseiller_id", $next_support)->update(['status' => 1]);
                    }
                    if ($bis_flag) {
                        foreach ($Qsupports as $key => $value) {
                            CurrentSelectedConseiller::where('conseiller_id', $key)->update(['left_queue' => $value]);
                        }
                    }
                }
            }

            // initialise the cycle if there is no queue set
            if (!!$LQsupports === false || $flag === 0) {
                $no_flag = 0;
                $index = 0;
                // set up the current support as the first element of the array
                if ($flag === 0 && !!$LQsupports) {
                    $current_support = array_keys($LQsupports)[0];
                } elseif (!!$LQsupports === false || $no_flag) {
                    foreach ($Qsupports as $key => $value) {
                        CurrentSelectedConseiller::where('conseiller_id', $key)->update(['left_queue' => $value]);
                    }
                    $current_support = array_keys($Qsupports)[0];
                }

                if ((count($LQsupports) - 1 !== 0 && $index == count(array_keys($LQsupports)) - 1) || ($index == count(array_keys($LQsupports)) - 1 && count(array_keys($LQsupports)) - 1 === 0 && array_values($LQsupports)[$index] - 1 > 0)) {
                    $next_support = array_keys($LQsupports)[0];
                } elseif (count($LQsupports) - 1 !== 0 && count($LQsupports) - 1 >= $index && array_values($LQsupports)[$index + 1] - 1 >= 0) {
                    $next_support = array_keys($LQsupports)[$index + 1];
                } else {
                    // restart the cycle
                    $next_support = array_keys($Qsupports)[0];
                    $no_flag = 1;
                }

                $left_queue = array_values($LQsupports)[$index];
                CurrentSelectedConseiller::where("conseiller_id", $current_support)->update(['left_queue' => $left_queue - 1]);
                CurrentSelectedConseiller::where("conseiller_id", $next_support)->update(['status' => 1]);
            }
            $lead->support_id = $current_support;
            $lead->save();
            AssignmentLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'conseiller_id' => $current_support,
            ]);
        }

        // save the lead with the new support_id

        //
        $customer = Customer::where('lead_id', $lead->id)->first();
        if (!$customer) {
            if ($lead->program_id > 4) {
                $program = \App\Models\Program::find(1);
                $program_step_id = $program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id;
            } else {
                $program_step_id = $lead->program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id;
            }
            $customer = Customer::create([
                "lead_id" => $lead->id,
                "program_id" => $lead->program_id,
                "program_step_id" => $program_step_id,
                "firstname" => $lead->firstname,
                "lastname" => $lead->lastname,
                "campaign_id" => $lead->campaign_id,
                "lead_status" => $lead->lead_status_id,
                "customer_email" => $lead->email,
                "email" => $lead->email,
                "password" => Hash::make('123456'),
                "birthday" => $lead->birthday,
                "phone" => $lead->phone,
                "adresse_line_1" => $lead->adresse_line_1,
                "adresse_line_2" => $lead->adresse_line_2,
                "postal_code" => $lead->postal_code,
                "budget" => $lead->budget,
                "ranking_id" => $lead->ranking_id,
                "country_id" => $lead->country_id,
                "english_level_id" => $lead->english_level_id,
                "school_level_id" => $lead->school_level_id,
                "projected_diploma_type_id" => $lead->projected_diploma_type_id,
                "professional_domain_id" => $lead->professional_domain_id,
                "professional_experience" => $lead->professional_experience,
                "currency_id" => $lead->lang === "en" ? 2 : 1
            ]);
            $customer->save();

            if ($lead->cv_id)
                $lead->getMedia('CVs')[0]->copy($customer, 'CVs');

            //$case = CustomerCase::where('customer_id', $customer->first()->id)->first();
        }

        if ($customer->is_client == "1") abort(404);

        $cc = \App\Models\CustomerClear1000Fields::firstOrCreate([
            'customer_id' => $customer->id,
        ]);
        //
        $cc1 = \App\Models\CustomerClear1004Fields::firstOrCreate([
            'customer_id' => $customer->id,
        ]);
        $case = CustomerCase::where('customer_id', $customer->id)->first();
        if (!$case) {
            //dd(count($case->get()));
            if ($customer->lead->program_id > 4) {
                $program = \App\Models\Program::find(1);
                $program_step_id = $program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id;
            } else {
                $program_step_id = $customer->program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id;
            }
            $case = CustomerCase::create([
                'user_id' => Auth::id(),
                'customer_id' => $customer->id,
                'program_id' => $customer->program_id,
                'program_step_id' => $program_step_id,
            ]);
            $case->save();
        }
        //dd($case);
        //dd($customer->lead_status, $lead->lead_status_id);
        //return view('leads.view', compact('lead', 'case'));
        //$customer = \App\Models\Customer::where('lead_id', $lead->id)->first();
        if ($customer->lead_status != $lead->lead_status_id) {
            $customer->lead_status = $lead->lead_status_id;
            $customer->save();
        }

        if (CaseState::where("customer_case_id", $case->id)->count() == 0) {
            foreach (SubStepState::all() as $state) {
                CaseState::create([
                    'user_id' => Auth::id(),
                    'customer_case_id' => $case->id,
                    'sub_step_state_id' => $state->id,
                    'status' => false,
                ]);
            }
        }
        // automate contrat preparing
        /*$contrat = DocumentType::find(5);
        if($contrat && (count($customer->getMedia($contrat->collection_name))>0)){
            $media = $customer->getMedia($contrat->collection_name)[0];
            if (!$media->hasCustomProperty('file_id') || ($media->getCustomProperty('file_id') == 0))
                $this->prepareForSigning($customer, $contrat->collection_name);
        }*/
        // $this->prepareForSigning($customer, $collectionName);
        //$lead->lead_status_id = $customer->lead_status;
        return View::make('leads.view')
            ->with(compact('lead'))
            ->with('customer_case', $case);
        // ->with('customer', $customer);
    }

    public function formIndex()
    {
        return view('leads.forms.index');
    }

    public function firstMeetPresentation(CustomerCase $customerCase)
    {
        if (isCustomer() && !count($customerCase->customer->sheets()->wherePivot('information_sheet_id', '=', 1)->wherePivot('can_edit', '=', true)->get())) abort(404);
        return view('leads.presentations.r1');
    }

    public function importCSV()
    {
        return view('leads.import');
    }

    public function integrateLeads(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'nullable|file|mimes:csv,txt',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with("error", "Fichier insuportable.");
        }
        try {
            Excel::import(new LeadssImport, $request->file);
        } catch (\Exception $e) {
            return back()->with("error", "Fichier scheama incompatible.");
        }

        return back()->with('success', 'Vous avez importé les leads avec succès.');
    }
}
