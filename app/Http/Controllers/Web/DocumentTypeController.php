<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\DocumentType;
use Illuminate\Http\Request;

class DocumentTypeController extends Controller
{
    public function index()
    {
        return view('documents.index');
    }
    public function view(DocumentType $document)
    {
        return view('documents.view-steps', compact('document'));
    }
}
