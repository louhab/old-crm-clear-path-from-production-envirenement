<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Resources\BillResource;
use App\Models\Bill;
use App\Models\Currency;
use App\Models\DocumentType;
use App\Models\Quote;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class PDFController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Bill $bill
     * @return \Illuminate\Http\Response
     */
    public function generateBillPDF(Request $request, Bill $bill, Currency $currency)
    {
        $pdf = PDF::loadView('billing.pdfs.bill', compact('bill', 'currency'));

        return $pdf->download('facture-'. $bill->customer->firstname . '-' . $bill->customer->firstname . ' ' .date('Y-m-d H:i:s') . '.pdf');
        //return $pdf->stream('devis-'. $quote->customer->firstname . '-' . $quote->customer->firstname . ' ' .date('Y-m-d H:i:s') . '.pdf', array("Attachment" => false));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Quote $quote
     * @return \Illuminate\Http\Response
     */
    public function generateQuotePDF(Request $request,Quote $quote)
    {
        $pdf = PDF::loadView('billing.pdfs.quote', compact('quote'));

        return $pdf->download('devis-'. $quote->customer->firstname . '-' . $quote->customer->firstname . ' ' .date('Y-m-d H:i:s') . '.pdf');
        //return $pdf->stream('devis-'. $quote->customer->firstname . '-' . $quote->customer->firstname . ' ' .date('Y-m-d H:i:s') . '.pdf', array("Attachment" => false));
    }

    public function viewQuotePDF(Request $request, Quote $quote) {
        // $customer = ;
        $doc = DocumentType::where('name_fr', 'Devis')->first();
        $pdf = $this->addMediaPDF($quote->customer, $doc, 'devis', $quote, null);
        return $pdf->stream();
    }
}
