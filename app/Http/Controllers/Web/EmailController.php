<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\EmailNotification;
use App\Models\SubStepState;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendingEmail;

class EmailController extends Controller
{
    public function send($_data)
    {
        $emailnotification_id = SubStepState::find($_data['substep_id'])->email_notification_id;
        $object = EmailNotification::find($emailnotification_id)->object;
        $message = EmailNotification::find($emailnotification_id)->message;

        $data = array(
            'name' => $_data['name'],
            'object' => $object,
            'message' => $message,
            'elements' => "",
        );

        // catch \Swift_TransportException and avoid 500 error
        if ($_data['email']) {
            try {
                Mail::to($_data['email'])->send(new sendingEmail($data));
            } catch (\Swift_TransportException $e) {
                // echo 'Exception reçue : ',  $e->getMessage(), "\n";
            }
        }
        return 1; //success
    }

}
