<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\CustomerCase;
use App\Models\ProgramSubStep;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

use App\Models\Customer;

class AdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        if (Auth::user()->isManager()) {
            return redirect()->route('dashboard');
        }
        return view('admissions.index');
    }

    public function view(CustomerCase $customer_case)
    {
        $docs = new \Illuminate\Database\Eloquent\Collection();
        $substeps = [ProgramSubStep::find(8)];

        foreach ($substeps as $substep) {
            // $docs = $docs->merge($substep->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot('order')->where('customer_owner', 1)->orderBy('order', 'asc')->get());
            foreach ($substep->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot('order')->where('customer_owner', 1)->orderBy('order', 'asc')->get() as $doc) {
                if ($customer_case->customer->requestedDocuments()->wherePivot('document_type_id', $doc->id)->get()->isEmpty()) {
                    $customer_case->customer->requestedDocuments()->attach([
                        $doc->id => ['program_step_id' => $substep->step_id]
                    ]);
                }
            }
        }
        // dd($docs);


        if (Auth::user()->isManager()) {
            return redirect()->route('case-edit', ['customer_case' => $customer_case->id]);
        }
        // $customer_case = \App\Models\CustomerCase::where('customer_id', $client->id)->first();
        return View::make('admissions.view', compact('customer_case'));
    }

    public function view_(CustomerCase $customer_case)
    {
        $docs = new \Illuminate\Database\Eloquent\Collection();
        $substeps = [ProgramSubStep::find(8)];

        foreach ($substeps as $substep) {
            // $docs = $docs->merge($substep->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot('order')->where('customer_owner', 1)->orderBy('order', 'asc')->get());
            foreach ($substep->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot('order')->where('customer_owner', 1)->orderBy('order', 'asc')->get() as $doc) {
                if ($customer_case->customer->requestedDocuments()->wherePivot('document_type_id', $doc->id)->get()->isEmpty()) {
                    $customer_case->customer->requestedDocuments()->attach([
                        $doc->id => ['program_step_id' => $substep->step_id]
                    ]);
                }
            }
        }


        if (Auth::user()->isManager()) {
            return redirect()->route('case-edit', ['customer_case' => $customer_case->id]);
        }
        // $customer_case = \App\Models\CustomerCase::where('customer_id', $client->id)->first();
        return View::make('admissions.view_', compact('customer_case'));
    }
}
