<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerCase;
use App\Models\InformationSheet;
use App\Models\MediaSheet;
use App\Models\ProgramStep;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index(ProgramStep $program_step)
    {
        if (Auth::guard('customer')->user()) {

            $customer_case = Auth::guard('customer')->user()->case()->get()[0];

            if (!$program_step->visible_for_customer) abort(404);

            // $program_steps_elq = $customer_case->program->steps();

            /*$currentStepOrder = $program_steps_elq->withPivot('order')->where('visible_for_customer', '=', true)->wherePivot('program_step_id', '=', $customer_case->program_step_id)->get();

            if(!count($currentStepOrder)) $currentStepOrder = 0;
            else $currentStepOrder = $currentStepOrder[0]->order;

            $paramStepOrder = $program_steps_elq->withPivot('order')->where('visible_for_customer', '=', true)->wherePivot('program_step_id', '=', $program_step->id)->get();

            if(!count($paramStepOrder)) $paramStepOrder = 0;
            else $paramStepOrder = $paramStepOrder[0]->order;

            if($paramStepOrder > $currentStepOrder) abort(404);*/
            Auth::guard('customer')->user()->mediaSheets()->sync(array(1, 2));

            // $program_steps = $customer_case->program->steps()->where('visible_for_customer', '=', true)->get();
            $payment2 = \App\Models\CaseState::where("sub_step_state_id", 5)
                ->where("customer_case_id", $customer_case->id)
                ->where("status", 1)
                ->whereNotNull("payment_method")
                ->first();
            if ($payment2) {
                $sheets = \App\Models\InformationSheet::with('substeps');
                foreach ($sheets->get() as $sheet)
                    if (!$sheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                        $this->toggleSheetVisibility($customer_case->customer, $sheet);
                //dd($customer_case->customer->sheets()->get());
                $media_sheets = \App\Models\MediaSheet::with('substeps');
                foreach ($media_sheets->get() as $mediaSheet)
                    if (!$mediaSheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                        $this->toggleMediaSheetVisibility($customer_case->customer, $mediaSheet);
            }

            return view('mycase.index', compact('customer_case', 'program_step'));
        }
        return view('cases.index');
    }

    public function indexCustomer(Request $request)
    {
        if (Auth::guard('customer')->user()) {

            $customer_case = Auth::guard('customer')->user()->case()->get()[0];

            // Auth::guard('customer')->user()->mediaSheets()->sync(array(1, 2));

            return view('mycase.index', compact('customer_case'));
        }
        abort(404);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param CustomerCase $customer_case
     * @return Application|Factory|Response|View
     */
    public function edit(CustomerCase $customer_case)
    {
        //TODO send also order of current step
        // $step_state = \App\Models\SubStepState::where("name_fr", "Paiement 2: 2300DH")->first();
        $payment2 = \App\Models\CaseState::where("sub_step_state_id", 5)
            ->where("customer_case_id", $customer_case->id)
            ->where("status", 1)
            ->whereNotNull("payment_method")
            ->first();
        if ($payment2) {
            $sheets = \App\Models\InformationSheet::with('substeps');
            foreach ($sheets->get() as $sheet)
                if (!$sheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                    $this->toggleSheetVisibility($customer_case->customer, $sheet);
            //dd($customer_case->customer->sheets()->get());
            $media_sheets = \App\Models\MediaSheet::with('substeps');
            foreach ($media_sheets->get() as $mediaSheet)
                if (!$mediaSheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                    $this->toggleMediaSheetVisibility($customer_case->customer, $mediaSheet);
        }
        return view('cases.edit', compact('customer_case'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param CustomerCase $customer_case
     * @return Application|Factory|Response|View
     */
    public function edit_(CustomerCase $customer_case)
    {
        //TODO send also order of current step
        // $step_state = \App\Models\SubStepState::where("name_fr", "Paiement 2: 2300DH")->first();
        $payment2 = \App\Models\CaseState::where("sub_step_state_id", 5)
            ->where("customer_case_id", $customer_case->id)
            ->where("status", 1)
            ->whereNotNull("payment_method")
            ->first();
        if ($payment2) {
            $sheets = \App\Models\InformationSheet::with('substeps');
            foreach ($sheets->get() as $sheet)
                if (!$sheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                    $this->toggleSheetVisibility($customer_case->customer, $sheet);
            //dd($customer_case->customer->sheets()->get());
            $media_sheets = \App\Models\MediaSheet::with('substeps');
            foreach ($media_sheets->get() as $mediaSheet)
                if (!$mediaSheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                    $this->toggleMediaSheetVisibility($customer_case->customer, $mediaSheet);
        }
        return view('cases.edit_', compact('customer_case'));
    }

    public function updateProfile(Request $request)
    {
        return view('mycase.profile.edit');
    }
    public function updateUserProfile(Request $request)
    {
        return view('profile');
    }

    private function toggleSheetVisibility(Customer $customer, InformationSheet $informationSheet)
    {
        $sheet = $customer->sheets()->wherePivot('information_sheet_id', '=', $informationSheet->id);

        if (count($sheet->get()))
            if (count($sheet->wherePivot('can_edit', '=', true)->get()))
                $customer->sheets()->updateExistingPivot($informationSheet->id, [
                    'can_edit' => true
                ]);
            else
                $customer->sheets()->updateExistingPivot($informationSheet->id, [
                    'can_edit' => true
                ]);
        else
            $customer->sheets()->attach([
                $informationSheet->id => [
                    'can_edit' => true
                ]
            ]);
    }
    private function toggleMediaSheetVisibility(Customer $customer, MediaSheet $mediaSheet)
    {
        $sheet = $customer->mediaSheets()->wherePivot('media_sheet_id', '=', $mediaSheet->id);

        if (count($sheet->get()))
            if (count($sheet->wherePivot('can_edit', '=', true)->get()))
                $customer->mediaSheets()->updateExistingPivot($mediaSheet->id, [
                    'can_edit' => true
                ]);
            else
                $customer->mediaSheets()->updateExistingPivot($mediaSheet->id, [
                    'can_edit' => true
                ]);
        else
            $customer->mediaSheets()->attach([
                $mediaSheet->id => [
                    'can_edit' => true
                ]
            ]);
    }
}
