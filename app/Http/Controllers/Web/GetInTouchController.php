<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Calendrier;
use App\Models\LeadForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Stevebauman\Location\Facades\Location;

class GetInTouchController extends Controller
{
    public function index($token)
    {
        // $position = Location::get(\Request::ip());
        $language = \Request::server('HTTP_ACCEPT_LANGUAGE');
        // \Request::ip(),
        // Request::server('HTTP_ACCEPT_LANGUAGE')
        // $request->server('HTTP_ACCEPT_LANGUAGE')
        // dd(session()->get('locale'), \Config::get('app.locale'));
        if (strlen($token) == 60) {
            $formPage = LeadForm::where('token', $token)->get();
            if (!$formPage->count() || !$formPage[0]->active) abort(404);
            $is_students_form = $formPage[0]->is_students_form;
            $is_multi_program = $formPage[0]->is_multi_program;
            $student_program_id = $formPage[0]->student_program_id;
            // lang condition
            if (in_array($formPage[0]->lang, ['fr', 'en'])) {
                App::setlocale($formPage[0]->lang);
                session()->put('locale', $formPage[0]->lang);
            } else {
                if (!session()->has('locale')) {
                    if (str_starts_with($language, "fr")) {
                        App::setlocale('fr');
                        session()->put('locale', 'fr');
                    }
                    if (str_starts_with($language, "en")) {
                        App::setlocale('en');
                        session()->put('locale', 'en');
                    }
                }
            }
            return view('leads.forms.leadform-index', compact('token', 'is_students_form', "is_multi_program", "student_program_id"));
        } else {
            abort(404);
        }
    }

    public function welcome(Request $request)
    {
        $to_office = $request->session()->get('to_office');
        return view('leads.welcome', compact('to_office'));
    }

    public function getAvailableDateTime(Request $request)
    {
        $times = Calendrier::select('time', 'time_end')->whereDate('date', $request['date'])->get()->toArray();
        $available_times = [
            ['begin' => '10:00', 'end' => '10:45'],
            ['begin' => '10:45', 'end' => '11:30'],
            ['begin' => '11:30', 'end' => '12:15'],
            ['begin' => '12:15', 'end' => '13:00'],
            ['begin' => '13:00', 'end' => '13:45'],
            ['begin' => '13:45', 'end' => '14:30'],
            ['begin' => '14:30', 'end' => '15:15'],
            ['begin' => '15:15', 'end' => '16:00'],
            ['begin' => '16:00', 'end' => '16:45'],
            ['begin' => '16:45', 'end' => '17:30'],
            ['begin' => '17:30', 'end' => '18:15']
        ];
        $returned_times = [];
        $i = 0;
        if (date('D', strtotime($request['date'])) == 'Sat') {
            array_pop($available_times); // remove ['begin'=>'17:30','end'=>'18:15']
            array_pop($available_times); // remove ['begin'=>'16:45','end'=>'17:30']
        }
        foreach ($available_times as $available_time) {
            foreach ($times as $time) {
                if (($time['time'] <= $available_time['begin'] && $available_time['begin'] <= $time['time_end']) || ($time['time'] <= $available_time['end'] && $available_time['end'] <= $time['time_end'])
                    || ($available_time['begin'] <= $time['time'] && $time['time'] <= $available_time['end']) || ($available_time['end'] <= $time['time'] && $time['time_end'] <= $available_time['end'])
                ) {
                    unset($available_times[$i]);
                }
            }
            $i++;
        }
        foreach (array_values($available_times) as $available_time) {
            array_push($returned_times, $available_time['begin'] . ' - ' . $available_time['end']);
        }
        return json_encode($returned_times);
    }
}
