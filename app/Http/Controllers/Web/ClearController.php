<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\MediaSheet;

// use App\Models\MediaSheet;
// use Illuminate\Http\Request;

class ClearController extends Controller
{
    public function index()
    {
        return view('clears.index');
    }
    public function view(MediaSheet $sheet)
    {
        // dd($sheet);
        return view('clears.view', compact('sheet'));
    }
}
