<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\CustomerCase;
use App\Models\MediaSheet;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class MediaSheetController extends Controller
{
    /**
     * Display & Edit information sheet.
     *
     * @return Application|Factory|Response|View
     */
    public function edit(CustomerCase $customerCase, MediaSheet $mediaSheet)
    {
        // dd("test route conflict -- from media");
        // dd("holla 🚀");
        if (isCustomer() && !count($customerCase->customer->mediaSheets()->wherePivot('media_sheet_id', '=', $mediaSheet->id)->wherePivot('can_edit', '=', true)->get()))
            abort(404);
        $data = ['mediaSheet', 'customerCase'];
        return view('cases.sheet-media', compact($data));
    }
}
