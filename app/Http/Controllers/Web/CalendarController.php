<?php

namespace App\Http\Controllers\Web;

use App\Events\CustomerActionsUpdated;
use App\Events\LeadProspectionUpdated;
use App\Http\Controllers\Controller;
use App\Models\Calendrier;
use App\Models\CustomerCase;
use App\Models\LeadStateLog;
use App\Models\LeadStatus;
use App\Models\ProgramPayment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Carbon\Carbon;

class CalendarController extends Controller
{
    // Display a listing of the resource
    public function index()
    {
        $calendar = DB::table('calendriers as cal')
            ->leftJoin('customers as c', 'cal.customer_id', '=', 'c.id')
            ->leftJoin('leads as l', 'l.id', '=', 'c.lead_id')
            ->select('cal.id as id', 'cal.title as title', 'l.id as lead_id', DB::raw('Year(cal.date) as year'), DB::raw('Month(cal.date) as month'), DB::raw('Day(cal.date) as day'), DB::raw("TIME_FORMAT(cal.time, '%H') as hour"), DB::raw("TIME_FORMAT(cal.time, '%i') as minute"), DB::raw("TIME_FORMAT(cal.time_end, '%H') as hourEnd"), DB::raw("TIME_FORMAT(cal.time_end, '%i') as minuteEnd"), 'cal.time as time_start', 'cal.time_end as time_end', 'cal.description as description', 'c.id as customer_id',  DB::raw("CONCAT(c.firstname, ' ', c.lastname) AS customer_name"), 'c.is_client', 'cal.event_type as event_type')
            ->where(function ($query) {
                $query->whereNull('cal.deleted_at');
                $query->where(function ($query) {
                    $query->whereNull('l.to_office');
                });
                $query->orWhere(function ($query) {
                    $query->where('l.to_office', 'office');
                });
                $query->orWhere(function ($query) {
                    $query->whereIn('c.id', ProgramPayment::find(1)->customers()->get()->pluck('id')->toArray());
                });
            });

        $calendar->whereNull('cal.deleted_at');
        if (Auth::user()->isConseiller()) {
            $advisor_name = User::find(Auth::id())->name;
            $calendar = $calendar->where('cal.title', 'like', "%$advisor_name%");
        }
        return Response::view('calendar.index', ['calendar' => $calendar->get()]);
    }

    public function create(Request $request)

    {
        // $rules = [
        //     'add-form-date' => 'required',
        //     'add-form-title' => 'required',
        //     'add-form-time' => 'required',
        //     'add-form-lead' => 'required',
        // ];

        $rules = [
            'add-form-date' => 'required',
            // 'add-form-title' => 'required',
            'add-form-time' => 'required',
            // 'add-form-lead' => 'required',
            'add-form-type' => 'required',
        ];

        $this->validate($request, $rules);

        // prevent from creating more than meet for the same client in the same day with the same
        if ($request->input('add-form-lead')) {
            $calendarCount = DB::table('calendriers as cal')->where("cal.customer_id", $request->input('add-form-lead'))->where("user_id", Auth::id())->whereDate('date', Carbon::createFromFormat('Y-m-d', $request->input('add-form-date')))->count();
            if ($calendarCount >= 1) {
                return redirect()->back()->with(['error' => 'Le créneau que vous avez chsoit est déjà pris! prier de chosir un autre ']);
            }
        }

        $case = CustomerCase::where("customer_id", $request->input('add-form-lead'))->first();
        $customer = \App\Models\Customer::where('id', $request['add-form-lead'])->first();
        if ($customer && $customer->is_client == 0 && ($customer->lead->lead_status_id < LeadStatus::Rencontre1)) {
            $old_state = $customer->lead->lead_status_id;
            $customer->lead_status = LeadStatus::Rencontre1;
            $customer->lead->update(array('lead_status_id' => LeadStatus::Rencontre1));
            $customer->save();
            if ($old_state != LeadStatus::Rencontre1) {
                LeadStateLog::create([
                    'user_id' => auth()->user()->id,
                    'lead_id' => $customer->lead->id,
                    'state_id' => $customer->lead->lead_status_id,
                ]);
            }
        }
        if ($case) {
            if ((str_starts_with($request['add-form-title'], "Recontre 1") || str_starts_with($request['add-form-title'], "Rencontre 1")) && $case->program_step_id < 2) {
                if ($customer->lead->lead_status_id < LeadStatus::Rencontre1) {
                    $case->program_step_id = 2;
                    $customer = \App\Models\Customer::where('id', $request['add-form-lead'])->first();
                    $customer->lead_status = 4;
                    $customer->save();
                    $lead = \App\Models\Lead::where("id", $customer->lead_id)->first();
                    $old_state = $lead->lead_status_id;
                    $lead->lead_status_id = 4;
                    $lead->save();
                    if ($old_state != 4) {
                        LeadStateLog::create([
                            'user_id' => auth()->user()->id,
                            'lead_id' => $lead->id,
                            'state_id' => $lead->lead_status_id,
                        ]);
                    }
                }
            } elseif (str_starts_with($request['add-form-title'], "Rencontre 2") && $case->program_step_id < 3) {
                $case->program_step_id = 3;
            } elseif (str_starts_with($request['add-form-title'], "Rencontre 3") && $case->program_step_id < 4) {
                $case->program_step_id = 4;
            }
            $case->save();
        }
        // dd($request->input('add-form-title-hidden'));
        $calendar = new Calendrier();
        $calendar->time = $request['add-form-time'];
        $calendar->time_end = $request['add-form-time_end'];
        if (($request->input('add-form-type') == "rencontre" || $request->input('add-form-type') == "rappel") && $request->input('add-form-title-hidden')) {
            $adv_name = User::find($customer->lead->conseiller_id)->name;
            $calendar->title = $request->input('add-form-title-hidden') . " : " . $customer->lead->firstname . " " . $customer->lead->lastname . " - " . $adv_name;
        } else if ($request->input('add-form-type') == "rencontre") {
            $calendar->title = $request->input('add-form-title');
        } else {
            $calendar->title = $request->input('add-form-title-bis') . " - " . Auth::user()->name;
        }
        $calendar->date = $request->input('add-form-date');
        $calendar->customer_id = $request->input('add-form-lead');
        $calendar->description = $request->input('add-form-description');
        $calendar->event_type = $request->input('add-form-type');
        $calendar->to_office = $request['add-form-to-office'] == "1" ? "office" : "conference";
        $calendar->user_id = Auth::id();
        $calendar->save();
        if ($case) {
            $case->customer->lead->update(array('processed_message' => 'Appel effectuée', 'processed_at' => Carbon::now()));
            $case->customer->update(array('processed_message' => 'Rencontre Crée', 'processed_at' => Carbon::now()));
        }
        /*if (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'lead-view') {
            LeadProspectionUpdated::dispatch($case->customer->lead, 'Rencontre Crée');
        }*/
        /*if (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'case-edit') {
            CustomerActionsUpdated::dispatch($case->customer, 'Rencontre Crée');
        }*/
        return redirect()->back()->with(['success' => 'Vos modifications ont été enregistrées avec succès']);
    }

    public function update(Request $request)
    {

        // dd($request->input());
        $calendar = Calendrier::find($request->input('edit-form-id'));
        $calendar->update([
            'title' => $request->input('edit-form-title'),
            'date' => $request->input('edit-form-date'),
            'time' => $request->input('edit-form-time'),
            'time_end' => $request->input('edit-form-time_end'),
            'customer_id' => $request->input('edit-form-lead'),
            'description' => $request->input('edit-form-description')
        ]);
        //$calendar->save();
        // dd($calendar);
        return redirect()->back()->with(['success' => 'Vos modifications ont été enregistrées avec succès']);
    }

    public function destroy($calendar)
    {
        Calendrier::find($calendar)->delete();
        return redirect()->back()->with(['success' => 'La planification a été supprimé avec succès']);
    }
}
