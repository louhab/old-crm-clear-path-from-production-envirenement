<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashBoardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $quote_sum = 0;
        // $bill_sum = 0;
        // foreach(\App\Models\Quote::all() as $quote){
        //     $quote_sum += $quote->getAmountSumAttribute();
        // }
        // foreach(\App\Models\Bill::all() as $bill){
        //     $bill_sum += $bill->getAmountSumAttribute();
        // }

        //$cases = \App\Models\CustomerCase::all();
        //$customers = count(\App\Models\Customer::all());
        //$leads = count(\App\Models\Lead::all());
        // $leads_ext = count(\App\Models\Lead::whereNotNull('campaign_id')->get());
        // $calls = \App\Models\HistoCall::all();
        // $messages = count(\App\Models\CustomerMessage::all());
        //$quotes = Quote::where():
        return view('dashboard.' . (isCustomer() ? 'customer' : 'backoffice'));
    }

    public function clientsIndex() {
        return view('dashboard.backoffice-clients');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function customerIndex()
    {
        return view('dashboard.customer');
    }
}
