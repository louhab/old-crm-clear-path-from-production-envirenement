<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Livewire\Input\Group\Partials\Field;
use App\Http\Requests\DocumentType\DocumentTypeAddFormRequest;
use App\Http\Requests\DocumentType\DocumentTypeUpdateFormRequest;
use App\Http\Resources\DocumentTypeResource;
use App\Models\CustomerField;
use App\Models\DocumentType;
use App\Models\FieldSheet;
use App\Models\InformationSheet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Yajra\DataTables\Facades\DataTables;

class SheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $clearsQB = DB::table('information_sheets as sheet')
            // ->leftJoin('document_type_groups as group', 'group.id', '=', 'dt.document_type_group_id')
            // ->leftJoin('programs as p', 'p.id', '=', 'sheet.program_id')
            // ->leftJoin('media_sheets as clear', 'group.id', '=', 'dt.document_type_group_id')
            ->where(function ($query) use ($request) {
                /*if(isset($request['name_en']) && !empty($request['name_en'])) {
                    $query->where(function($query) use ($request) {
                        $query->where('dt.name_en', 'LIKE', "%".$request['name_en']."%");
                    });
                }
                if(isset($request['name_fr']) && !empty($request['name_fr'])) {
                    $query->where(function($query) use ($request) {
                        $query->where('dt.name_fr', 'LIKE', "%".$request['name_fr']."%");
                    });
                }*/
            })
            ->select(['sheet.*']);

        $actions = [
            ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light clear-show", "icon" => "fas fa-eye"],
            /*["action" => "copy", "title" => "Copier Lien", "available" => 1, "className" => "btn btn-info leadsforms-table-copy", "icon" => "fas fa-copy"],*/
            /*["action" => "edit", "islink" => false, "title" => "Editer Clear", "available" => 1, "className" => "btn btn-primary clears-table-edit", "icon" => "fas fa-edit"],*/
            /*["action" => "delete", "islink" => false, "title" => "Supprimer Formulaire", "available" => Auth::user()->isAdmin() ? 1 : 0, "className" => "btn btn-danger documents-table-delete", "icon" => "fas fa-trash"],*/
        ];

        // return DataTables()->eloquent($leadformsQB)
        //     ->addColumn('actions', $actions)
        //     ->toJson();

        return DataTables::of($clearsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    public function view(Request $request, $id)
    {
        $documentsQB = DB::table('customer_field_information_sheet as dc')
            ->where('dc.information_sheet_id', $id)
            ->leftJoin('information_sheets as sheet', 'sheet.id', '=', 'dc.information_sheet_id')
            ->leftJoin('customer_fields as field', 'field.id', '=', 'dc.customer_field_id')
            ->leftJoin('customer_field_groups as group', 'group.id', '=', 'field.customer_field_group_id')
            ->leftJoin('customer_field_groups as sheet_group', 'sheet_group.id', '=', 'dc.customer_field_group_id')
            ->leftJoin('programs as p', 'p.id', '=', 'dc.program_id')
            ->where(function ($query) use ($request) {
                if (isset($request['field']) && !empty($request['field'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('field.field_name', 'LIKE', "%" . $request['field'] . "%");
                    });
                }
                if (isset($request['group']) && !empty($request['group'])) {
                    $query->where(function ($query) use ($request) {
                        if ($request['group'] == "n/d") $query->whereNull('dc.document_type_group_id');
                        else $query->where('field.customer_field_group_id', $request['group']);
                    });
                }
                if (isset($request['program']) && !empty($request['program'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('dc.program_id', $request['program']);
                    });
                }
            })
            ->select(['dc.*', 'sheet.name_fr as sheet_name', 'p.name as program_name', 'field.field_name as field_name', 'group.name_fr as field_group', 'sheet_group.name_fr as sheet_group']);
        /*if(isset($request['clear']) && !empty($request['clear'])) {
            $documentsQB->leftJoin('document_type_media_sheet as ds', 'ds.document_type_id', '=', 'dt.id')
                ->where(function($query) use ($request) {
                    $query->where('ds.media_sheet_id', $request['clear']);
                });
        }*/
        $documentsQB->orderBy("dc.order", "asc");

        $actions = [
            // ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light document-type-show", "icon" => "fas fa-eye"],
            /*["action" => "copy", "title" => "Copier Lien", "available" => 1, "className" => "btn btn-info leadsforms-table-copy", "icon" => "fas fa-copy"],*/
            ["action" => "edit", "islink" => false, "title" => "Editer Formulaire", "available" => 1, "className" => "btn btn-primary documents-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "islink" => false, "title" => "Supprimer Formulaire", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger documents-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables::of($documentsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return DocumentTypeResource
     */
    public function store(DocumentTypeAddFormRequest $request)
    {
        // dd($request["add-form-redirect"]);
        $is_customer_owner = (isset($request['add-form-redirect']) && !empty($request['add-form-redirect']) && ($request['add-form-redirect'] == "on")) ? 1 : 0;
        return new DocumentTypeResource(DocumentType::create([
            "collection_name" => Str::random(30),
            "name_fr" => $request['add-form-title-fr'],
            "name_en" => $request['add-form-title-en'],
            "signable" => 0,
            "customer_owner" => $is_customer_owner,
            "visible_for_customer" => 1,
        ]));
    }
    public function storePivot(Request $request, $sheet_id)
    {
        $rules = [
            'add-form-field' => 'required',
            'add-form-program' => 'required',
            'add-form-order' => 'required',
        ];

        $this->validate($request, $rules);

        $field_sheet = FieldSheet::create([
            "customer_field_id" => $request['add-form-field'],
            "information_sheet_id" => $sheet_id,
            "program_id" => $request['add-form-program'],
            "order" => $request['add-form-order'],
        ]);
        return response()->json($field_sheet->toArray());
    }
    /**
     * edit resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return DocumentTypeResource
     */
    public function update(DocumentTypeUpdateFormRequest $request, $id)
    {
        $document = DocumentType::findOrFail($id);
        if (isset($request['edit-form-redirect']) && !empty($request['edit-form-redirect']) && $request['edit-form-redirect'] == "on")
            $document->customer_owner = 1;
        else
            $document->customer_owner = 0;

        $document->name_en = $request['edit-form-title-en'];
        $document->name_fr = $request['edit-form-title-fr'];
        // $document->customer_owner = $request['edit-form-campaign'];

        $document->save();
        return new DocumentTypeResource($document);
    }
    public function updatePivot(Request $request, $id)
    {
        $rules = [
            'edit-form-field' => 'required',
            'edit-form-program' => 'required',
            'edit-form-order' => 'required',
        ];

        $this->validate($request, $rules);

        $field_sheet = FieldSheet::findOrFail($id);

        $field_sheet->customer_field_id = $request['edit-form-field'];
        $field_sheet->program_id = $request['edit-form-program'];
        $field_sheet->order = $request['edit-form-order'];

        $field_sheet->save();
        return response()->json($field_sheet->toArray());
    }

    public function destroy(InformationSheet $sheet)
    {
        return $sheet->delete();
    }

    public function destroyPivot(FieldSheet $field_sheet)
    {
        return $field_sheet->delete();
    }
}
