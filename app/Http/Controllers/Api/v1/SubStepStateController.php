<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\EmailController;
use App\Http\Requests\SubStepState\SubStepStateUpdateFormRequest;
use App\Http\Resources\SubStepStateResource;
use App\Mail\sendingEmail;
use App\Models\Customer;
use App\Models\CustomerCase;
use App\Models\CustomerClear1000Fields;
use App\Models\LevelOfStudy;
use App\Models\ProgramSubStep;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\SubStepState;
use App\Models\CaseState;
use App\Models\DocumentType;
use App\Models\PaymentMethod;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class SubStepStateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $emailNotificationQB = DB::table('sub_step_states as s')
            ->leftJoin('program_sub_step_sub_step_state as ps', 'ps.sub_step_state_id', '=', 's.id')
            ->leftJoin('program_sub_steps as pss', 'pss.id', 'ps.program_sub_step_id')
            ->leftJoin('program_steps as p', 'pss.step_id', '=', 'p.id')
            ->leftJoin('email_notifications as e', 'e.id', '=', 's.email_notification_id')
            ->select('s.*', 'e.object', 'p.step_label')
            ->distinct();
        if (isset($request['name_en']) && !empty($request['name_en'])) {
            $emailNotificationQB->where('s.name_en', 'like', '%' . $request['name_en'] . '%');
        }
        if (isset($request['name_fr']) && !empty($request['name_fr'])) {
            $emailNotificationQB->where('s.name_fr', 'like', '%' . $request['name_fr'] . '%');
        }

        $actions = [
            ["action" => "edit", "title" => "Modifier Sub Step", "available" => 1, "className" => "btn btn-primary substepnotification-table-edit", "icon" => "fas fa-edit"],
        ];

        return DataTables()->of($emailNotificationQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    private function allDocsExist(CustomerCase $customerCase)
    {
        $allDocsExists = true;
        if ($customerCase->customer->program_id == 4) {
            $clear = CustomerClear1000Fields::where('customer_id', $customerCase->customer_id)->first();
            if ($clear && $clear->level_study) {
                //
                $level = LevelOfStudy::find($clear->level_study);
                $docs =  $level->docs()
                    ->withPivot(['order'])
                    ->orderBy('order')
                    ->get();
            }
            //$allDocsExists = true;
            foreach ($docs->pluck("name_fr", "collection_name") as $collection => $name) {
                if ($customerCase->customer->getMedia($collection)->count() == 0)
                    $allDocsExists = false;
            }
        }
        $media_sheet = \App\Models\MediaSheet::find(2);
        $docs = $media_sheet->docs()
            ->wherePivot('program_id', '=', $customerCase->program_id)
            ->withPivot(['required', 'order'])
            ->orderBy('order')
            ->get();
        foreach ($docs->pluck("name_fr", "collection_name") as $collection => $name) {
            if ($customerCase->customer->getMedia($collection)->count() == 0)
                $allDocsExists = false;
        }
        return $allDocsExists;
    }

    private function makePayment($payment, $method)
    {
        // dd($payment->conseiller_id);
        if ($method != null && PaymentMethod::where('id', $method)->first()) {
            $payment->payment_method = $method;
        }
        $customer = Customer::find(CustomerCase::find($payment->customer_case_id)->customer_id);
        // not working 👇
        // $payment->conseiller_id = $customer->lead->conseiller_id;
        $admis = SubStepState::where("id", $payment->sub_step_state_id);
        if ($admis->where("name_fr", "Paiement 2: 2300DH")->first()) {
            if (is_null($customer->admission_state)) {
                //$customer->admission_state = 5;
                $customer->save();
            }
        }
        if ($admis->where("name_fr", "Paiement 3: 5800DH")->first()) {
            if ($this->allDocsExist(CustomerCase::find($payment->customer_case_id))) {
                $customer->admission_state = 1;
                if (is_null($customer->admis_at))
                    $customer->admis_at = Carbon::now();
                $customer->save();
            }
        }
        if ($payment->sub_step_state_id == 3) {

            $customer->password = Hash::make($customer->phone);
            $customer->save();

            $customer->case->substep()->associate(7);
            $customer->case->save();

            $object = 'Portail Clear Path Canada';
            $languser =  $customer->lead->lang;

            $conseiller = User::find($customer->lead->conseiller_id);
            $message = "";

            $data = array(
                'name' => $customer->firstname . ' ' . $customer->lastname,
                'object' => $object,
                'message' => $message,
                "elements" => [
                    "lead_lang" => $languser,
                    "conseiller" => $conseiller,
                    "customer" => $customer,
                    "email_phase" => 2,
                ],
            );

            if ($customer->email) {
                try {
                    Mail::to($customer->email)->send(new sendingEmail($data));
                } catch (\Swift_TransportException $e) {
                    // echo 'Exception reçue : ',  $e->getMessage(), "\n";
                }
            }
        } elseif ($payment->sub_step_state_id == 5) {
            $customer->case->substep()->associate(8);
            $customer->case->save();
        } elseif ($payment->sub_step_state_id == 10) {
            $customer->case->substep()->associate(10);
            $customer->case->save();
        }
    }

    public function updateStepState(CaseState $case_state, $method = null)
    {
        $step_state = SubStepState::find($case_state->sub_step_state_id);
        // dd($case_state->sub_step_state_id);
        $customer = Customer::find(CustomerCase::find($case_state->customer_case_id)->customer_id);
        $doc_recu = DocumentType::where('name_fr', 'Reçu')->first();
        $doc_bill = DocumentType::where('name_fr', 'Facture')->first();

        if ($case_state->status) {
            if ($case_state->sub_step_state_id != 2) {
                $case_state->status = 0;
                // payment_method
                foreach ($customer->getMedia($step_state->name_fr . $doc_recu->collection_name) as $media) $media->delete();
                foreach ($customer->getMedia($step_state->name_fr . $doc_bill->collection_name) as $media) $media->delete();
            }
        } else {
            $case_state->status = 1;
            $customer_case = \App\Models\CustomerCase::where("id", $case_state->customer_case_id)->first();
            //paiement 200 dh => client account making + email notif
            if (str_starts_with($step_state->name_fr, 'Paiement')) {

                // dd("stopping here  before making payment ::");
                $this->makePayment($case_state, $method);
                if ($case_state->payment_method) {
                    if ($customer_case->program_step_id < 2) {
                        $customer_case->program_step_id = 2;
                        $customer_case->save();
                    }


                    // make some logic here
                    // set BO @ahaloua
                    // dd("hello");
                    $customer->save();

                    $bill = $this->createBill($customer);
                    $this->addMediaPDF($customer, $doc_bill, 'bill', $bill, $step_state);
                    $this->addMediaPDF($customer, $doc_recu, 'recu', "null", $step_state);
                }
            }

            if ($step_state->email_notification_id) {
                $emailController = new EmailController();
                $emailController->send([
                    'name' => $customer->firstname . ' ' . $customer->lastname,
                    'email' => $customer->email,
                    'substep_id' => $step_state->id
                ]);
            }
            // clear1002
            if ($step_state->id == 6) {
                $customer_case->substep()->associate(9);
                $customer_case->save();
            }
        }
        $case_state->save();
        //$step_state = SubStepState::where("id", $case_state->sub_step_state_id)->first();
        if ($case_state->sub_step_state_id == 2) {
            // return Redirect::to($url);
            if ($customer->phone) {
                $case_state->status = 1;
                $lead = \App\Models\Lead::where("id", $customer->lead_id)->first();
                $conseil = User::find($customer->lead->conseiller_id);
                return response()->json(
                    array(
                        "status" => $case_state->status,
                        "name" => $step_state->name_fr,
                        "phone" => $customer->phone,
                        "firstname" => $customer->firstname,
                        "lastname" => $customer->lastname,
                        "conseiller" => $conseil->name,
                        "conseiller_phone" => $conseil->phone,
                        "gender" => $conseil->gender,
                        "user_role" => $conseil->role_id,
                        "lead_lang" => $lead->lang
                    )
                );
            } else {
                $case_state->status = 0;
            }
            $case_state->save();
        }
        return response()->json(array("status" => $case_state->status, "name" => $step_state->name_fr));
    }

    public function getPaymentMethods(Request $request)
    {
        $data = [];

        if ($request->has('q')) {
            $search = isset($request->q['term']) ? $request->q['term'] : '';
            $data = PaymentMethod::select("id", DB::raw("method_label_fr AS name"))
                ->where(function ($query) use ($search) {
                    $query->where('method_label_fr', 'LIKE', "%$search%");
                })
                ->get();
        }

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return SubStepStateResource
     */
    public function update(SubStepStateUpdateFormRequest $request, $id)
    {
        $substepnotification = SubStepState::findOrFail($id);
        $substepnotification->name_en = $request['edit-form-name_en'];
        $substepnotification->name_fr = $request['edit-form-name_fr'];
        $substepnotification->email_notification_id = $request['edit-form-email_notification_id'];
        $substepnotification->save();

        return new SubStepStateResource($substepnotification);
    }
}
