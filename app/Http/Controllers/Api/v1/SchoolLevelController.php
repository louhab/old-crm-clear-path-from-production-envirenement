<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\SchoolLevel\SchoolLevelAddFormRequest;
use App\Http\Requests\SchoolLevel\SchoolLevelUpdateFormRequest;
use App\Http\Resources\SchoolLevelResource;
use App\Models\SchoolLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SchoolLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $schoollevelsQB = DB::table('school_levels')
            ->select('*');

        if (isset($request['school_level_name']) && !empty($request['school_level_name'])) {
            $schoollevelsQB->where('school_level_name', 'like', '%' . $request['school_level_name'] . '%');
        }

        $actions = [
            ["action" => "edit", "title" => "Modifier campaign", "available" => 1, "className" => "btn btn-primary schoollevel-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer campaign", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger schoollevel-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables()->of($schoollevelsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return SchoolLevelResource
     */
    public function store(SchoolLevelAddFormRequest $request)
    {
        return new SchoolLevelResource(SchoolLevel::create([
            "school_level_name" => $request['add-form-school_level_name'],
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return SchoolLevelResource
     */
    public function update(SchoolLevelUpdateFormRequest $request, $id)
    {
        $schoollevel = SchoolLevel::findOrFail($id);
        $schoollevel->school_level_name = $request['edit-form-school_level_name'];
        $schoollevel->save();

        return new SchoolLevelResource($schoollevel);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  SchoolLevel $schoollevel
     * @return int
     * @throws \Exception
     */
    public function destroy(int $schoollevel)
    {
        $_schoollevel = SchoolLevel::find($schoollevel);
        return $_schoollevel->delete();
    }
}
