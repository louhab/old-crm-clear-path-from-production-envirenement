<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\ProgramSubStep;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query\Builder;

use App\Models\User;
use App\Models\CustomerCase;
use App\Models\Program;
use App\Models\ProgramStep;
use App\Models\StepPhase;
use App\Models\Lead;
use App\Models\LeadStatus;
use App\Models\Customer;
use App\Models\HistoCall;
use App\Models\CustomerMessage;
use App\Models\Quote;
use App\Models\Bill;

use App\Models\LeadFormMarketingCampaign;

use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class DashBoardController extends Controller
{

    public function index(Request $request)
    {
        $ids = Customer::where('is_client', 1)->pluck('lead_id')->toArray();
        $leads = DB::table('leads as l')
            // ->leftJoin('customers as cr', 'l.id', '=', 'cr.lead_id')
            ->whereNotIn('l.id', $ids)
            ->whereNull('l.deleted_at');
        $calls = DB::table('histo_calls as hc')
            ->leftJoin('leads as l','l.id','=','hc.lead_id')
            ->whereNull('hc.deleted_at')
            ->whereNull('l.deleted_at');
        $messages = DB::table('customer_messages as cm')
            ->leftJoin('customers as c','c.id','=','cm.customer_id')
            ->leftJoin('leads as l','l.id','=','c.lead_id')
            ->whereNull('cm.deleted_at')
            ->whereNull('c.deleted_at')
            ->whereNull('l.deleted_at');
        $ca_encaisse = DB::table('program_payment_customer as pc')
            ->leftJoin('customers as c','c.id','=','pc.customer_id')
            ->leftJoin('leads as l','l.id','=','c.lead_id')
            ->select(DB::raw('SUM(pc.amount) as amount'), DB::raw('COUNT(*) as count'))
            ->whereNull('c.deleted_at')
            ->whereNull('l.deleted_at');

        if(Auth::user()->isConseiller() || Auth::user()->isManager()){
            $leads->where('l.conseiller_id', Auth::id());
            $calls->where("hc.user_id", Auth::id());
            $messages->where("cm.user_id", Auth::id());
            $ca_encaisse->where("l.conseiller_id", Auth::id());
        }

        if(Auth::user()->isSupport()){
            $leads->where('l.support_id', Auth::id());
            $calls->where("hc.user_id", Auth::id());
            $messages->where("cm.user_id", Auth::id());
            $ca_encaisse->where("l.support_id", Auth::id());
        }

        // state
        if(isset($request['state']) && !empty($request['state'])) {
            if($request['state'] == "n/d"){
                $leads->whereNull('l.lead_status_id');
                $calls->whereNull('l.lead_status_id');
                $messages->whereNull('l.lead_status_id');
                $ca_encaisse->whereNull('l.lead_status_id');
            } else {
                $leads->where('l.lead_status_id', '=',$request['state']);
                $calls->where('l.lead_status_id', '=',$request['state']);
                $messages->where('l.lead_status_id', '=',$request['state']);
                $ca_encaisse->where('l.lead_status_id', '=',$request['state']);
            }
        }

        // program
        if(isset($request['program']) && !empty($request['program'])) {
            $leads->where("l.program_id", $request['program']);
            $calls->where("l.program_id", $request['program']);
            $messages->where("c.program_id", $request['program']);
            $ca_encaisse->where("c.program_id", $request['program']);
        }

        // Campaign
        if(isset($request['campaign']) && !empty($request['campaign'])) {
            $leads->where("l.campaign_id", $request['campaign']);
            $calls->where("l.campaign_id", $request['campaign']);
            $messages->where("c.campaign_id", $request['campaign']);
            $ca_encaisse->where("c.campaign_id", $request['campaign']);
        }

        // users
        if( (isset($request['agent']) && !empty($request['agent']))) {
            $agent = $request['agent'] > 0 ? $request['agent'] : null;

            $col = "conseiller_id";
            if (strtolower($request['agent-group']) == 'support') {
                $col = "support_id";
            }
            $leads->where('l.'.$col, $agent);
            $calls->where("hc.user_id", $agent);
            $messages->where("cm.user_id", $agent);
            $ca_encaisse->where("l.".$col, $agent);
        }

        // date
        if(isset($request['date_start']) && !empty($request['date_start'])) {
            $start = $request['date_start'] . ' 00:00:00';
            $end = $request['date_end'] . ' 23:59:59';
        } else {
            $start = date('Y-m-d') . ' 00:00:00';
            $end = date('Y-m-d') . ' 23:59:59';
        }

        $processedLeads = clone $leads;
        $statusLeads = clone $leads;
        $processedLeads
            ->whereBetween('l.created_at',[$start,$end]);
            // ->whereNotNull('l.lead_status_id');

        $statusLeads = $statusLeads->whereBetween('l.status_changed_at',[$start,$end]);

        $leads->whereBetween('l.created_at',[$start,$end]);
        $calls->whereBetween('hc.created_at',[$start,$end]);
        $messages->whereBetween('cm.created_at',[$start,$end]);
        $ca_encaisse->whereBetween('pc.performed_at',[$start,$end]);

        if(Auth::user()->isSupport() || Auth::user()->isConseiller() || Auth::user()->isManager() || Auth::user()->isAgent()){
            $calls->where('user_id',Auth::id());
        }

        $campaigns = clone $leads;
        $ca_encaisse_200 = clone $ca_encaisse;
        $ca_encaisse_200->where('program_payment_id',1);
        // dd((clone $processedLeads)->whereNotNull('l.processed_at')->where('l.recycled', 0)->get());
        // dd($start,$end);
        // dd($leads->where('l.recycled', 0)->count());

        return response()->json(
            array(
                "leads"                     => $leads->count(),
                "AllLeads"                  => (clone $leads)->count(),
                "AllRecycledLeads"          => (clone $leads)->where('l.recycled', 1)->count(),
                "processedLeads"            => (clone $processedLeads)->whereNotNull('l.processed_at')->count(),
                "statusLeads"               => $statusLeads->selectRaw('COUNT(*) AS count, l.lead_status_id')->groupBy("l.lead_status_id")->get()->toArray(),
                "calls"                     => $calls->count(),
                "durations"                 => $calls->sum('hc.duration'),
                "messages"                  => $messages->count(),
                "campaigns"                 => $campaigns->selectRaw('COUNT(*) AS count, l.campaign_id')->groupBy("l.campaign_id")->get()->toArray(),
                "ca_encaisse"               => $ca_encaisse->get()->toArray()[0],
                "ca_encaisse_200"           => $ca_encaisse_200->get()->toArray()[0],
            )
        );
    }

    public function clientsIndex(Request $request)
    {

        // $leads = DB::table('leads as l');
        $customers = DB::table('customers as c')
            ->join("customer_cases as cc", "cc.customer_id", "=", "c.id")
            ->where('c.is_client', 1)
            ->select('cc.program_step_id', DB::raw('COUNT(cc.program_step_id) as count'));
        $calls = DB::table('histo_calls as hc');
        $messages = DB::table('customer_messages as cm')
            ->join('customers as c', 'c.id', '=', 'cm.customer_id');

        $ca_encaisse = DB::table('program_payment_customer as pc')
            ->join('customers as cc', 'cc.id', '=', 'pc.customer_id')
            ->join('customer_cases as case', 'case.customer_id', '=', 'cc.id')
            ->join('program_payment_program_step as ps', 'ps.program_payment_id', '=', 'pc.program_payment_id')
            ->where('cc.is_client', 1)
            ->whereNotNull('pc.payment_method')
            ->whereNotNull('pc.performed_at')
            // ->select("pc.amount");
            ->select('pc.program_payment_id', DB::raw('SUM(pc.amount) as amount'), DB::raw('COUNT(*) as count'));

        // phase
        /*if(isset($request['phase']) && !empty($request['phase'])) {
            $customers->join('program_steps as step', 'step.id', '=', 'cc.program_step_id')
                ->where("step.step_phase_id", $request['phase']);
            $ca_encaisse->join('program_steps as step', 'step.id', '=', 'case.program_step_id')
                ->where("step.step_phase_id", $request['phase']);
        }*/
        if(isset($request['campaign']) && !empty($request['campaign'])) {
            $customers->where("c.campaign_id", $request["campaign"]);
            $ca_encaisse->where("cc.campaign_id", $request['campaign']);
        }
        if(isset($request['program']) && !empty($request['program'])) {
            // $leads->where("l.program_id", $request['program']);
            $customers->where("c.program_id", $request['program']);
            $messages->where("c.program_id", $request['program']);

            $calls->where("c.program_id", $request['program']);

            $ca_encaisse->where("cc.program_id", $request['program']);
        }
        if( (isset($request['agent']) && !empty($request['agent'])) || Auth::user()->isConseiller() ) {
            $agent = $request['agent'] > 0 ? $request['agent'] : null;
            if( Auth::user()->isConseiller() ) {
                $agent = Auth::id();
            }
            //$uu = User::find($agent);
            // dd($uu->isSupport());
            $col = "conseiller_id";
            if (strtolower($request['agent-group']) == 'support') {
                $col = "support_id";
            }
            $calls->where("hc.user_id", $agent);
            $messages->where("cm.user_id", $agent);
            $customers->join('leads as ll', 'll.id', '=', 'c.lead_id')
                ->where("ll.".$col, $agent);
            // $leads->where('l.'.$col, $agent);
            $ca_encaisse->join('leads as l', 'l.id', '=', 'cc.lead_id')
                ->where("l.".$col, $agent);
        }

        if(isset($request['date_start']) && !empty($request['date_start'])) {
            $start = Carbon::parse($request['date_start']);
            $end = Carbon::parse($request['date_end'])->addDay();
        } else {
            $start = Carbon::now()->firstOfMonth();
            $end   = Carbon::now()->lastOfMonth();
        }
        $customers->where('c.created_at', '>=', $start->format('Y-m-d').' 00:00:00');
        $calls->where('hc.created_at', '>=', $start->format('Y-m-d').' 00:00:00');
        $messages->where('cm.created_at', '>=', $start->format('Y-m-d').' 00:00:00');
        $ca_encaisse->where('pc.performed_at', '>=', $start->format('Y-m-d').' 00:00:00');

        $customers->where('c.created_at', '<=', $end->format('Y-m-d').' 00:00:00');
        $calls->where('hc.created_at', '<=', $end->format('Y-m-d').' 00:00:00');
        $messages->where('cm.created_at', '<=', $end->format('Y-m-d').' 00:00:00');
        $ca_encaisse->where('pc.performed_at', '<=', $end->format('Y-m-d').' 00:00:00');

        //
        $campaigns = (clone $customers)->select('c.campaign_id', DB::raw('COUNT(c.campaign_id) as camps'))->groupBy("c.campaign_id")->get()->toArray();
        // dd($campaigns);

        // toSql
        return response()->json(
            array(
                //"leads"               => $leads->count(),
                "customers"           => $customers->count(),
                "calls"               => $calls->count(),
                "durations"           => $calls->sum('hc.duration'),
                "messages"            => $messages->count(),
                "ca_encaisse"         => $ca_encaisse->sum("pc.amount"),
                "step_amount"         => $ca_encaisse->groupBy("pc.program_payment_id")->get()->toArray(),
                "clients"             => $customers->addSelect("cc.program_step_id", DB::raw("COUNT(cc.program_step_id) as step_count"))->groupBy("cc.program_step_id")->get()->toArray(),
                "campaigns"           => $campaigns
            ));
    }

    private function hasJoin(Builder $builder, $table)
    {
        if ( $builder->joins ) {

            foreach($builder->joins as $JoinClause)
            {
                if($JoinClause->table == $table)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public function getAgents(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = isset($request->q['term']) ? $request->q['term'] : '';
            $data = User::select("id", "name", "role_id")
                ->where(function ($query) use ($search) {

                    $query->where('name','LIKE',"%$search%");
                    // $query->where('role_id', '=', 1);

                    $query->where(function ($query) {
                        if (app('router')->getRoutes()->match(app('request')->create(url()->previous()))->getName() == "soumissions-index") {
                            $query->where('role_id', '=', 6);
                        } else {
                            $query->where('role_id', '=', 3);
                            $query->orWhere('role_id', '=', 2);
                            $query->orWhere('role_id', '=', 7);
                            $query->orWhere('role_id', '=', 4);
                        }
                    });
                    $query->where('status', '=', 1);
                })
                ->get();
        }
        $userGroupsArray = [];
        foreach ($data as $user) {
            $role_name = Role::find($user->role_id)->role_name;
            $userGroupsArray[$role_name][] = array("id" => $user->id, "name" => $user->name);
        }
        //dd($data, $userGroupsArray);
        return response()->json($userGroupsArray);
    }
    public function getPrograms(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = isset($request->q['term']) ? $request->q['term'] : '';
            //dd($search);
            $data = Program::select("id", "name")
                ->where(function ($query) use ($search) {

                    $query->where('name','LIKE',"%$search%");

                })
                ->get();
        }

        return response()->json($data);
    }
    // getEtapes
    public function getSteps(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = isset($request->q['term']) ? $request->q['term'] : '';
            //dd($search);
            $data = ProgramStep::select("id", "step_label")
                ->where(function ($query) use ($search) {

                    $query->where('step_label','LIKE',"%$search%");

                })
                //->limit(10)
                ->get();
        }

        return response()->json($data);
    }
    // getPhases
    public function getPhases(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = isset($request->q['term']) ? $request->q['term'] : '';
            //dd($search);
            $data = StepPhase::select("id", "phase_label_fr")
                ->where(function ($query) use ($search) {

                    $query->where('phase_label_fr','LIKE',"%$search%");

                })
                ->get();
        }

        return response()->json($data);
    }
    public function getCampaigns(Request $request) {
        $data = [];

        if($request->has('q')){
            $search = isset($request->q['term']) ? $request->q['term'] : '';
            //dd($search);
            $data = LeadFormMarketingCampaign::select("id", "campaign_name")
                ->where(function ($query) use ($search) {

                    $query->where('campaign_name','LIKE',"%$search%");

                })
                ->get();
        }

        return response()->json($data);
    }
    // getLeadStatus
    public function getLeadStatus(Request $request) {
        $data = [];

        if($request->has('q')){
            $search = isset($request->q['term']) ? $request->q['term'] : '';
            //dd($search);
            $data = LeadStatus::select("id", "status_label_fr")
                ->where(function ($query) use ($search) {

                    $query->where('status_label_fr','LIKE',"%$search%");

                })
                ->get();
        }

        return response()->json($data);
    }
}
