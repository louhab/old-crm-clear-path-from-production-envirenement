<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Phase\PhaseAddFormRequest;
use App\Http\Requests\Phase\PhaseUpdateFormRequest;
use App\Http\Resources\PhaseResource;
use App\Models\Phase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PhaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $phasesQB = DB::table('phases as ph')
            ->leftJoin('programs as p', 'p.id', '=', 'ph.program_id')
            ->select('ph.*', 'p.name as pname')
            ->where(function ($query) use ($request) {
                if (isset($request['name']) && !empty($request['name'])) {
                    $query->where('ph.name', 'like', '%' . $request['name'] . '%');
                }
                if (isset($request['ranking']) && !empty($request['ranking'])) {
                    $query->where('ph.program_id', $request['ranking']);
                }
            });

        $actions = [
            ["action" => "edit", "title" => "Modifier phase", "available" => 1, "className" => "btn btn-primary phase-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer phase", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger phase-table-delete", "icon" => "fas fa-trash"],
        ];


        return DataTables()->of($phasesQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return PhaseResource
     */
    public function store(PhaseAddFormRequest $request)
    {
        return new PhaseResource(Phase::create([
            "program_id" => $request['add-form-program_id'],
            "name" => $request['add-form-name'],
            "order" => $request['add-form-order'],
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return PhaseResource
     */
    public function update(PhaseUpdateFormRequest $request, $id)
    {
        $phase = Phase::findOrFail($id);
        $phase->program_id = $request['edit-form-program_id'];
        $phase->name = $request['edit-form-name'];
        $phase->order = $request['edit-form-order'];
        $phase->save();

        return new PhaseResource($phase);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Phase $phase
     * @return int
     * @throws \Exception
     */
    public function destroy(Phase $phase)
    {
        return $phase->delete();
    }
}
