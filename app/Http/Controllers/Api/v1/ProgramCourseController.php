<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\ActionLog ;
use Illuminate\Http\Request;
use App\models\LogAction ;
use App\Models\ProgramCourse;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class ProgramCourseController extends Controller
{
    private $filter_keys = [
        'domaine_etude', 'test_langue', 'langue', 'ville', 'session', 'diplome',
        'type_etablissement', 'concentration', 'nom_etablissement', 'niveau_scolaire', 'prix_total',
        'prix_annuel', 'frais_admission', 'score', 'note', 'duree', 'lien', 'exigence', 'analyse', 'budget'
    ];

    private $columns = [
        "Budget" => "budget",
        "Niveau Scolaire" => "niveau_scolaire",
        "Note Scolaire" => "note",
        "Type d'établissement" => "type_etablissement",
        "Nom d'etablissement" => "nom_etablissement",
        "Ville" => "ville",
        "Session" => "session",
        "Date limite d'admission" => "date_limit_admission",
        "Diplômes" => "diplome",
        "Domaine d'études" => "domaine_etude",
        "Programmes" => "programme",
        "Concentration" => "concentration",
        "Langue" => "langue",
        "Durée" => "duree",
        "Frais d'admission" => "frais_admission",
        "Frais scolaires par année" => "prix_annuel",
        "Total des frais scolaires" => "prix_total",
        "Lien" => "lien",
        "Exigences" => "exigence",
        "Test de langue" => "test_langue",
        "Score" => "score",
        "Objectifs de formation et débouchés du diplôme" => "analyse",
    ];
    public function getProgram(Request $request, ProgramCourse $program)
    {
        return $program;
    }
    public function update(Request $request, ProgramCourse $program)
    {
        $rules = [
            'lien' => 'required',
            'programme' => 'required',
        ];
        // dd($request->input());
        $this->validate($request, $rules);

        $properties = $program->properties;
        foreach ($request->input() as $key => $value) {
            if (is_array($value)) {
                // if ($key === "exigence") dd($value, $properties[$key]["values"]);
                // dd($properties[$key]["values"], $key);
                $properties[$key]["values"] = $value;
            } elseif ($key == "programme") {
                $program->name = $value;
            } elseif (str_starts_with($key, "date_limit_admission-")) {
                if (!is_null($value)) {
                    if (!is_array($properties["date_limit_admission"]["value"])) {
                        $properties["date_limit_admission"]["value"] = array("day" => 0, "month" => 0);
                    }
                    $dd = str_replace("date_limit_admission-", "", $key);
                    $properties["date_limit_admission"]["value"][$dd] = $value;
                }
            } else {
                $properties[$key]["value"] = $value;
            }
        }
        $program->properties = $properties;
        $program->save();
        ActionLog::create([
            'action'=>'modification',
            'program_id'=>$program->id,
            'user_id'=>Auth::user()->id
        ]);
        Log::channel('logger_clear_1002')
        ->notice('le programme avec id : '.$program->id . ' est modifié par '. Auth::user()->name);
        return $program;
    }

    public function store(Request $request)
    {

        $rules = [
            'lien' => 'required',
            'programme' => 'required',
        ];
        $this->validate($request, $rules);

        $properties = [];
        foreach ($this->columns as $label => $key) {
            if ($key == "date_limit_admission") {
                if (!isset($request["date_limit_admission-day"])) $dd = 0;
                else $dd = $request["date_limit_admission-day"];
                if (!isset($request["date_limit_admission-month"])) $mm = 0;
                else $mm = $request["date_limit_admission-month"];
                $properties[$key]["key"] = $label;
                $properties[$key]["value"] = array("day" => $dd, "month" => $mm);
            } elseif (in_array($key, $this->filter_keys)) {
                $properties[$key]["values"] = array();
                if (isset($request[$key]) && is_array($request[$key])) {
                    $properties[$key]["values"] = $request[$key];
                    // $properties[$key]["value"] = $request[$key][0];
                }
            } elseif ($label != "Programmes") {
                if (!isset($request[$key])) $value = "";
                else $value = trim($request[$key]);
                $properties[$key]["key"] = $label;
                $properties[$key]["value"] = $value;
            }
        }
        $newProgram =  ProgramCourse::create([
            "name" => $request["programme"],
            "properties" => $properties
        ]);
        ActionLog::create([
            'action'=>'creation',
            'program_id'=>$newProgram->id,
            'user_id'=>Auth::user()->id
        ]);
        Log::channel('logger_clear_1002')
        ->notice('le programme avec id : '.$newProgram->id . ' est créé par '. Auth::user()->name);
        return $newProgram ;
    }
    public function addValue(Request $request, $field_name)
    {
        $field = \App\Models\ProgramCourseField::where('field_name', $field_name)->first();
        // if (!empty($value)) {
        $values = $field->values;
        foreach ($request["values"] as $value) {
            if (empty($value)) continue;
            $key = array_search($value, $values);
            if ($key === false) {
                $values[] = $value;
                $field->values = $values;
                $field->save();
            }
        }
        return $field;
    }
}
