<?php
namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MediaSheetController extends Controller
{
    public function index(Request $request)
    {
        $selected = array();
        $langs = DB::table('language_of_studies as lf');
        $cities = DB::table('admission_cities as ac');
        $levels = DB::table('admission_school_levels as al');
        $etabs = DB::table('admission_establishments as ae');
        $etabNames = DB::table('admission_etab_names as aen');
        $diploms = DB::table('admission_diplomas as ad');
        $programs = DB::table('admission_programs as ap');

        $cities->join('sheet_clear1002_fields as field', 'field.admission_city', '=', 'ac.id');
        $levels->join('sheet_clear1002_fields as field', 'field.admission_school_level', '=', 'al.id');
        $etabs->join('sheet_clear1002_fields as field', 'field.admission_est', '=', 'ae.id');
        $etabNames->join('sheet_clear1002_fields as field', 'field.admission_est_name', '=', 'aen.id');
        $diploms->join('sheet_clear1002_fields as field', 'field.admission_diplomas', '=', 'ad.id');
        $programs->join('sheet_clear1002_fields as field', 'field.admission_program', '=', 'ap.id');
        //$langs = \App\Models\LanguageOfStudy::pluck('label', 'id');
        //$cities = $langs->cities();
        //$cities = \App\Models\AdmissionCity
        // $attributes = [
        //     "admission_lang_study" => "language_id",
        //     "admission_city" => "city_id",
        //     "admission_school_level" => "level_id",
        //     "admission_est" => "etab_id",
        //     "admission_est_name" => "etab_name_id",
        //     "admission_diplomas" => "diplomas_id",
        //     "admission_program" => "program_id"
        // ];
        foreach($request->input() as $key => $value) {
            //
            if(str_starts_with($key, "admission_")){
                foreach($request[$key] as $index => $selection){
                    $selected[$key][] = $selection;
                    //$col_name = "admission_" . $key;
                    if ($index === array_key_first($request[$key])) {
                        $cities->where('field.'.$key, $selection);
                        $levels->where('field.'.$key, $selection);
                        $etabs->where('field.'.$key, $selection);
                        $etabNames->where('field.'.$key, $selection);
                        $diploms->where('field.'.$key, $selection);
                        $programs->where('field.'.$key, $selection);
                    } else {
                        $cities->orWhere('field.'.$key, $selection);
                        $levels->orWhere('field.'.$key, $selection);
                        $etabs->orWhere('field.'.$key, $selection);
                        $etabNames->orWhere('field.'.$key, $selection);
                        $diploms->orWhere('field.'.$key, $selection);
                        $programs->orWhere('field.'.$key, $selection);
                    }
                }
            }
        }
        // if(isset($request['admission_lang_study']) && !empty($request['admission_lang_study'])){
        // }

        // toSql
        return response()->json(
            array(
                "lang_study"  => $langs->pluck('lf.label', 'lf.id'),
                "city" => $cities->pluck('ac.label', 'ac.id'),
                "school_level" => $levels->pluck('al.label', 'al.id'),
                "est" => $etabs->pluck('ae.label', 'ae.id'),
                "est_name"  => $etabNames->pluck('aen.label', 'aen.id'),
                "diplomas" => $diploms->pluck('ad.label', 'ad.id'),
                "program" => $programs->pluck('ap.label', 'ap.id'),
                "selected" => $selected,
                "count" => implode(",", $cities->pluck("field.id")->toArray())
            )
        );
    }
}
?>