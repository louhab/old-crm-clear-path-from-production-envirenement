<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\DocumentType\DocumentTypeAddFormRequest;
use App\Http\Requests\DocumentType\DocumentTypeUpdateFormRequest;
use App\Http\Resources\DocumentTypeResource;
use App\Models\DocumentSubStep;
use App\Models\DocumentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Yajra\DataTables\Facades\DataTables;

class SubStepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $documentsQB = DB::table('program_sub_steps as ps')
            // ->leftJoin('document_type_groups as group', 'group.id', '=', 'dt.document_type_group_id')
            // ->leftJoin('document_type_media_sheet as document_clear', 'document_clear.document_type_id', '=', 'dt.id')
            // ->leftJoin('media_sheets as clear', 'group.id', '=', 'dt.document_type_group_id')
            ->where(function ($query) use ($request) {
                if (isset($request['name_en']) && !empty($request['name_en'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('dt.name_en', 'LIKE', "%" . $request['name_en'] . "%");
                    });
                }
                if (isset($request['name_fr']) && !empty($request['name_fr'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('dt.name_fr', 'LIKE', "%" . $request['name_fr'] . "%");
                    });
                }
                if (isset($request['group']) && !empty($request['group'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('dt.document_type_group_id', $request['group']);
                    });
                }
            })
            ->select(['ps.*']); //->orderBy("dt.id", "desc");
        if (isset($request['clear']) && !empty($request['clear'])) {
            $documentsQB->leftJoin('document_type_media_sheet as ds', 'ds.document_type_id', '=', 'dt.id')
                ->where(function ($query) use ($request) {
                    $query->where('ds.media_sheet_id', $request['clear']);
                });
        }

        $actions = [
            ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light document-type-show", "icon" => "fas fa-eye"],
            /*["action" => "copy", "title" => "Copier Lien", "available" => 1, "className" => "btn btn-info leadsforms-table-copy", "icon" => "fas fa-copy"],*/
            // ["action" => "edit", "islink" => false, "title" => "Editer Formulaire", "available" => 1, "className" => "btn btn-primary documents-table-edit", "icon" => "fas fa-edit"],
            // ["action" => "delete", "islink" => false, "title" => "Supprimer Formulaire", "available" => Auth::user()->isAdmin() ? 1 : 0, "className" => "btn btn-danger documents-table-delete", "icon" => "fas fa-trash"],
        ];

        // return DataTables()->eloquent($leadformsQB)
        //     ->addColumn('actions', $actions)
        //     ->toJson();

        return DataTables::of($documentsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }
    public function view(Request $request, $id)
    {
        $documentsQB = DB::table('document_type_program_sub_step as ds')
            ->where('ds.program_sub_step_id', $id)
            ->leftJoin('program_sub_steps as step', 'step.id', '=', 'ds.program_sub_step_id')
            ->leftJoin('document_types as document', 'document.id', '=', 'ds.document_type_id')
            ->leftJoin('document_type_groups as group', 'group.id', '=', 'document.document_type_group_id')
            ->leftJoin('programs as p', 'p.id', '=', 'ds.program_id')
            ->where(function ($query) use ($request) {
                if (isset($request['document']) && !empty($request['document'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('document.name_fr', 'LIKE', "%" . $request['document'] . "%");
                    });
                }
                if (isset($request['group']) && !empty($request['group'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('document.document_type_group_id', $request['group']);
                    });
                }
                if (isset($request['program']) && !empty($request['program'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('ds.program_id', $request['program']);
                    });
                }
            })
            ->select(['ds.*', 'step.step_label as step_name', 'p.name as program_name', 'document.name_fr as document_name', 'group.name_fr as document_group']);
        $documentsQB->orderBy("ds.order", "asc");

        $actions = [
            // ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light document-type-show", "icon" => "fas fa-eye"],
            /*["action" => "copy", "title" => "Copier Lien", "available" => 1, "className" => "btn btn-info leadsforms-table-copy", "icon" => "fas fa-copy"],*/
            ["action" => "edit", "islink" => false, "title" => "Editer Formulaire", "available" => 1, "className" => "btn btn-primary documents-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "islink" => false, "title" => "Supprimer Formulaire", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger documents-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables::of($documentsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return DocumentTypeResource
     */
    public function storePivot(Request $request, $step_id)
    {
        $rules = [
            'add-form-document' => 'required',
            'add-form-program' => 'required',
            'add-form-order' => 'required',
        ];

        $this->validate($request, $rules);

        $document_clear = DocumentSubStep::create([
            "document_type_id" => $request['add-form-document'],
            "program_sub_step_id" => $step_id,
            "program_id" => $request['add-form-program'],
            "order" => $request['add-form-order'],
        ]);
        return response()->json($document_clear->toArray());
    }
    /**
     * edit resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return DocumentTypeResource
     */
    public function updatePivot(Request $request, $id)
    {
        $rules = [
            'edit-form-document' => 'required',
            'edit-form-program' => 'required',
            'edit-form-order' => 'required',
        ];

        $this->validate($request, $rules);

        $document_step = DocumentSubStep::findOrFail($id);

        $document_step->document_type_id = $request['edit-form-document'];
        $document_step->program_id = $request['edit-form-program'];
        $document_step->order = $request['edit-form-order'];

        $document_step->save();
        return response()->json($document_step->toArray());
    }

    public function destroyPivot(DocumentSubStep $document_step)
    {
        return $document_step->delete();
    }
}
