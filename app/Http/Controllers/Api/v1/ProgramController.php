<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ProgramController extends Controller
{
    public function index(Request $request)
    {
        $programs = DB::table('programs as p')
            // ->leftJoin('lead_form_marketing_campaigns as lm', 'lm.id', '=', 'lf.lead_form_marketing_campaign_id')
            ->where(function ($query) use ($request) {
                $query->where(function ($query) use ($request) {
                    $query->where('p.name', 'LIKE', "%" . $request['name'] . "%");
                });
            })
            ->select(['p.id', 'p.name', 'p.active as status']);

        $actions = [
            // ["action" => "copy", "title" => "Copier Lien", "available" => 1, "className" => "btn btn-info leadsforms-table-copy", "icon" => "fas fa-copy"],
            ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light", "icon" => "fas fa-eye"],
            ["action" => "edit", "title" => "Editer Programme", "available" => 1, "className" => "btn btn-primary programs-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer Programme", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger programs-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables::of($programs)
            ->addColumn('actions', $actions)
            ->toJson();
    }
}
