<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\ValidationException;
use SetaPDF_Core_Document;
use SetaPDF_Core_Writer_File;
use SetaPDF_Core_Writer_HttpStream;
use SetaPDF_FormFiller;

class SetasignPDFController extends Controller
{
    public function download(Customer $customer) {

// Your variables
        $filename = storage_path('app/public/imm5476f.pdf');
        $xml = <<<XML
<xfa:data>
  <IMM_5476>
    <Page1>
      <Reason>
        <appoint>0</appoint>
        <cancel>0</cancel>
      </Reason>
      <SectionA>
        <AppFamily>$customer->lastname</AppFamily>
        <AppGiven>$customer->firstname</AppGiven>
        <currentDate>$customer->birthday</currentDate>
        <TextField2>bureau 19</TextField2>
        <TextField2>prorogation du permis d'études</TextField2>
        <TextField2/>
      </SectionA>
      <SectionB>
        <List>
          <Item xfa:dataNode="dataGroup"/>
          <Item xfa:dataNode="dataGroup"/>
          <Item xfa:dataNode="dataGroup"/>
        </List>
        <AppFamily/>
        <AppGiven/>
        <comporUnComp>
          <uncomp>
            <isA>4</isA>
            <other>aaaaa</other>
          </uncomp>
          <Comp>
            <Member>
              <isMember/>
            </Member>
            <One>
              <memId/>
            </One>
            <Two>
              <whichProv/>
              <memId1/>
            </Two>
            <Three>
              <memId2/>
            </Three>
          </Comp>
        </comporUnComp>
        <RepInfo>
          <nameFirm/>
          <sup_lawyer/>
          <membership_ID/>
          <apt/>
          <Number/>
          <homeAddress/>
          <city/>
          <province/>
          <city/>
          <postalCode/>
          <phone>
            <Rextension/>
            <Rphone/>
          </phone>
          <fax>
            <Rextension/>
            <Rphone/>
          </fax>
          <TextField2/>
        </RepInfo>
        <List>
          <LI xfa:dataNode="dataGroup"/>
          <LI xfa:dataNode="dataGroup"/>
        </List>
        <TextField2/>
        <currentDate/>
      </SectionB>
      <CectionC>
        <AppFamily/>
        <AppGiven/>
        <TextField2/>
      </CectionC>
      <SectionD>
        <List>
          <LI xfa:dataNode="dataGroup"/>
          <LI xfa:dataNode="dataGroup"/>
        </List>
        <TextField2/>
        <currentDate/>
        <TextField2/>
        <currentDate/>
      </SectionD>
    </Page1>
  </IMM_5476>
</xfa:data>
XML;

// Create a file writer
        $writer = new \SetaPDF_Core_Writer_File($customer->firstname . '-' . $customer->lastname . '-contract.pdf');
// Load document by filename
        $document = SetaPDF_Core_Document::loadByFilename($filename, $writer);
// Create a form filler instance
        $formFiller = new SetaPDF_FormFiller($document);

// Get the XFA helper instance
        $xfa = $formFiller->getXfa();
        if ($xfa === false) {
            echo "false";
        }

// Pass the data to the document
        $xfa->setData($xml);
// Synchronize AcroForm fields
        $xfa->syncAcroFormFields();

// Finish the document
        $document->save()->finish();

        $file = public_path()."/" . $customer->firstname . '-' . $customer->lastname . '-contract.pdf';
        $headers = array('Content-Type: application/pdf',);
        return response()->download($file, $customer->firstname . '-' . $customer->lastname . '-contract.pdf', $headers)->deleteFileAfterSend(true);

    }
}
