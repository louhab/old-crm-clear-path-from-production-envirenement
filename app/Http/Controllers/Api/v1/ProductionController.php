<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProductionController extends Controller
{
    public $stats;
    public $calls;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($request->all());
        // know how much the advisor did (RDV, payments, calls)
        // date of each action

        $result = DB::table('users')
        ->selectRaw('users.id, users.name, users.created_at, SUM(program_payment_customer.amount) AS total_amount_payment, currencies.iso')
        ->leftjoin("roles", "users.role_id", "=", "roles.id")
        ->leftJoin('leads','leads.conseiller_id','=','users.id')
        ->leftJoin('customers','customers.lead_id','=','leads.id')
        ->leftJoin('program_payment_customer','program_payment_customer.customer_id','=','customers.id')
        ->join("currencies", "currencies.id", "=", "program_payment_customer.currency_id")
        ->where(function ($query) use ($request) {
            if(isset($request['conseiller']) && !empty($request['conseiller'])) {
                $rc = $request['conseiller'];
                $query->whereIn('leads.conseiller_id', $rc);
                if(in_array('n/d', $request['conseiller'])) {
                    $query->orWhereNull('leads.conseiller_id');
                }
            }
            if(isset($request['date_start']) && !empty($request['date_start'])) {
                $start = Carbon::parse($request['date_start']);
                // where operation created at
                $query->where('program_payment_customer.performed_at', '>=', $start->format('Y-m-d').' 00:00:00');
            }
            if(isset($request['date_end']) && !empty($request['date_end'])) {
                $end = Carbon::parse($request['date_end'])->addDay();
                // where operation created at
                $query->where('program_payment_customer.performed_at', '<=', $end->format('Y-m-d').' 00:00:00');
            }

            if(isset($request['program']) && !empty($request['program'])) {
                $query->whereIn('customers.program_id', $request['program']);
            }
        })
        ->where("users.role_id", "=", "3") // only for advisors (conseiller)
        ->groupBy("users.id", "program_payment_customer.currency_id")
        ->get()->toArray();


        $rdv = DB::table('calendriers as cal')
            ->selectRaw('users.id, COUNT(cal.id) as total_meets, cal.to_office')
            ->leftjoin("customers", "customers.id", "=", "cal.customer_id")
            ->leftJoin('leads','leads.id','=','customers.lead_id')
            ->leftJoin('users','users.id','=','leads.conseiller_id')
            ->leftjoin("roles", "roles.id", "=", "users.role_id")
            ->where(function ($query) use ($request) {
                if(isset($request['conseiller']) && !empty($request['conseiller'])) {
                    $rc = $request['conseiller'];
                    $query->whereIn('leads.conseiller_id', $rc);
                    if(in_array('n/d', $request['conseiller'])) {
                        $query->orWhereNull('leads.conseiller_id');
                    }
                }
                if(isset($request['date_start']) && !empty($request['date_start'])) {
                    $start = Carbon::parse($request['date_start']);
                    // where operation created at
                    $query
                    ->where('cal.created_at', '>=', $start->format('Y-m-d').' 00:00:00');
                }
                if(isset($request['date_end']) && !empty($request['date_end'])) {
                    $end = Carbon::parse($request['date_end'])->addDay();
                    // where operation created at
                    $query->where('cal.created_at', '<=', $end->format('Y-m-d').' 00:00:00');
                }

                if(isset($request['program']) && !empty($request['program'])) {
                    $query->whereIn('customers.program_id', $request['program']);
                }
            })
        ->where("users.role_id", "=", "3") // only for advisors (conseiller)
        ->groupBy("users.id")
        ->get()->toArray();

        // dd($rdv);


        $calls = DB::table('users')
        ->selectRaw('users.id, COUNT(DISTINCT histo_calls.id) AS total_calls, lead_statuses.status_label_fr')
        ->leftjoin("roles", "users.role_id", "=", "roles.id")
        ->leftJoin('leads','leads.conseiller_id','=','users.id')
        ->leftJoin('customers','customers.lead_id','=','leads.id')
        ->leftJoin('histo_calls','histo_calls.user_id','=',"users.id")
        ->leftJoin('lead_statuses','lead_statuses.id','=',"histo_calls.lead_state_id")
        ->where(function ($query) use ($request) {
            if(isset($request['conseiller']) && !empty($request['conseiller'])) {
                $rc = $request['conseiller'];
                $query->whereIn('leads.conseiller_id', $rc);
                if(in_array('n/d', $request['conseiller'])) {
                    $query->orWhereNull('leads.conseiller_id');
                }
            }
            if(isset($request['date_start']) && !empty($request['date_start'])) {
                $start = Carbon::parse($request['date_start']);
                // where operation created at
                $query
                ->where('histo_calls.created_at', '>=', $start->format('Y-m-d').' 00:00:00');
            }
            if(isset($request['date_end']) && !empty($request['date_end'])) {
                $end = Carbon::parse($request['date_end'])->addDay();
                // where operation created at
                $query->where('histo_calls.created_at', '<=', $end->format('Y-m-d').' 00:00:00');
            }
            if(isset($request['program']) && !empty($request['program'])) {
                $query->whereIn('customers.program_id', $request['program']);
            }
        })
        ->where("users.role_id", "=", "3") // only for advisors (conseiller)
        ->groupBy("users.id", "histo_calls.lead_state_id")
        ->get()->toArray();


        $int_array = [];
        $data_array = [];

        foreach ($result as $key => $value) {
            if (!in_array($value->id, $int_array)) {
                $value->total_amouts = $value->total_amount_payment . " " . $value->iso;
                $value->total_calls = "";
                foreach ($calls as $sub_key => $sub_value) {
                    if ($sub_value->id === $value->id) {
                        $value->total_calls .= ", " . $sub_value->status_label_fr . " : " . $sub_value->total_calls;
                    }
                }
                $value->total_calls = trim($value->total_calls, " ,");

                foreach ($rdv as $bis_key => $bis_value) {
                    if ($bis_value->id === $value->id) {
                        $value->total_RDV = $bis_value->total_meets;
                    }
                }

                $data_array[$value->id] = $value;
                array_push($int_array, $value->id);
            } else {
                $data_array[$value->id]->total_amouts .= ", " . $value->total_amount_payment . " " . $value->iso;
            }
        }

        $new = collect($data_array);

        // Group by program id
        // select program_payments.name_fr
        // JOIN program_payments ON program_payments.id = program_payment_customer.program_payment_id
        //  program_payments.name_fr,

        $actions = [
            ["action" => "afficher", "title" => "Voir conseiller", "available" => 1, "edit" => 1, "className" => "btn btn-sm btn-light advisor-show", "icon" => "fas fa-eye"],
        ];



        return DataTables()->of($new)
        ->addColumn('actions', $actions)
        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
