<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\DocumentType\DocumentTypeAddFormRequest;
use App\Http\Requests\DocumentType\DocumentTypeUpdateFormRequest;
use App\Http\Resources\DocumentTypeResource;
use App\Models\DocumentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Yajra\DataTables\Facades\DataTables;

class ClearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $clearsQB = DB::table('media_sheets as ms')
            // ->leftJoin('document_type_groups as group', 'group.id', '=', 'dt.document_type_group_id')
            ->leftJoin('programs as p', 'p.id', '=', 'ms.program_id')
            // ->leftJoin('media_sheets as clear', 'group.id', '=', 'dt.document_type_group_id')
            ->where(function ($query) use ($request) {
                /*if(isset($request['name_en']) && !empty($request['name_en'])) {
                    $query->where(function($query) use ($request) {
                        $query->where('dt.name_en', 'LIKE', "%".$request['name_en']."%");
                    });
                }
                if(isset($request['name_fr']) && !empty($request['name_fr'])) {
                    $query->where(function($query) use ($request) {
                        $query->where('dt.name_fr', 'LIKE', "%".$request['name_fr']."%");
                    });
                }*/
            })
            ->select(['ms.*', 'p.name as program_name']);

        $actions = [
            ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light clear-show", "icon" => "fas fa-eye"],
            /*["action" => "copy", "title" => "Copier Lien", "available" => 1, "className" => "btn btn-info leadsforms-table-copy", "icon" => "fas fa-copy"],*/
            /*["action" => "edit", "islink" => false, "title" => "Editer Clear", "available" => 1, "className" => "btn btn-primary clears-table-edit", "icon" => "fas fa-edit"],*/
            /*["action" => "delete", "islink" => false, "title" => "Supprimer Formulaire", "available" => Auth::user()->isAdmin() ? 1 : 0, "className" => "btn btn-danger documents-table-delete", "icon" => "fas fa-trash"],*/
        ];

        // return DataTables()->eloquent($leadformsQB)
        //     ->addColumn('actions', $actions)
        //     ->toJson();

        return DataTables::of($clearsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return DocumentTypeResource
     */
    public function store(DocumentTypeAddFormRequest $request)
    {
        // dd($request["add-form-redirect"]);
        $is_customer_owner = (isset($request['add-form-redirect']) && !empty($request['add-form-redirect']) && ($request['add-form-redirect'] == "on")) ? 1 : 0;
        return new DocumentTypeResource(DocumentType::create([
            "collection_name" => Str::random(30),
            "name_fr" => $request['add-form-title-fr'],
            "name_en" => $request['add-form-title-en'],
            "signable" => 0,
            "customer_owner" => $is_customer_owner,
            "visible_for_customer" => 1,
        ]));
    }
    /**
     * edit resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return DocumentTypeResource
     */
    public function update(DocumentTypeUpdateFormRequest $request, $id)
    {
        $document = DocumentType::findOrFail($id);
        if (isset($request['edit-form-redirect']) && !empty($request['edit-form-redirect']) && $request['edit-form-redirect']=="on")
            $document->customer_owner = 1;
        else
            $document->customer_owner = 0;

        $document->name_en = $request['edit-form-title-en'];
        $document->name_fr = $request['edit-form-title-fr'];
        // $document->customer_owner = $request['edit-form-campaign'];

        $document->save();
        return new DocumentTypeResource($document);
    }

    public function destroy(DocumentType $document)
    {
        return $document->delete();
    }
}
