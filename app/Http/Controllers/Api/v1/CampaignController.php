<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Campaign\CampaignAddFormRequest;
use App\Http\Requests\Campaign\CampaignUpdateFormRequest;
use App\Http\Resources\CampaignResource;
use App\Models\LeadFormMarketingCampaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $campaignsQB = DB::table('lead_form_marketing_campaigns')
            ->select('*');

        if (isset($request['name']) && !empty($request['name'])) {
            $campaignsQB->where('campaign_name', 'like', '%' . $request['name'] . '%');
        }

        $actions = [
            ["action" => "edit", "title" => "Modifier campaign", "available" => 1, "className" => "btn btn-primary campaign-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer campaign", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger campaign-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables()->of($campaignsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return CampaignResource
     */
    public function store(CampaignAddFormRequest $request)
    {
        return new CampaignResource(LeadFormMarketingCampaign::create([
            "campaign_name" => $request['add-form-campaign_name'],
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return CampaignResource
     */
    public function update(CampaignUpdateFormRequest $request, $id)
    {
        $campaign = LeadFormMarketingCampaign::findOrFail($id);
        $campaign->campaign_name = $request['edit-form-campaign_name'];
        $campaign->save();

        return new CampaignResource($campaign);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  LeadFormMarketingCampaign $campaign
     * @return int
     * @throws \Exception
     */
    public function destroy(LeadFormMarketingCampaign $campaign)
    {
        return $campaign->delete();
    }
}
