<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Message\MessageAddFormRequest;
use App\Http\Requests\Message\MessageUpdateFormRequest;
use App\Http\Resources\MessageResource;
use App\Models\CustomerMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CustomerMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $customersmessagesQB = DB::table('customer_messages as m')
            ->leftJoin('customers as c', 'c.id', '=', 'm.customer_id')
            ->select('m.*', 'c.firstname', 'c.lastname', 'c.email')
            ->where(function ($query) use ($request) {
                if (isset($request['name']) && !empty($request['name'])) {
                    $query->where('c.firstname', 'like', '%' . $request['name'] . '%')
                        ->orWhere('c.lastname', 'like', '%' . $request['name'] . '%');
                }
                if (isset($request['email']) && !empty($request['email'])) {
                    $query->where('c.email', 'like', '%' . $request['email'] . '%');
                }
            });

        $actions = [
            ["action" => "edit", "title" => "Modifier message", "available" => 1, "className" => "btn btn-primary message-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer message", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger message-table-delete", "icon" => "fas fa-trash"],
        ];


        return DataTables()->of($customersmessagesQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return MessageResource
     */
    public function store(MessageAddFormRequest $request)
    {
        return new MessageResource(CustomerMessage::create([
            "object" => $request['add-form-object'],
            "message" => $request['add-form-message'],
            "customer_id" => $request['add-form-customer_id'],
            "user_id" => Auth::id()
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return MessageResource
     */
    public function update(MessageUpdateFormRequest $request, $id)
    {
        $message = CustomerMessage::findOrFail($id);
        $message->object = $request['edit-form-object'];
        $message->message = $request['edit-form-message'];
        $message->customer_id = $request['edit-form-customer_id'];
        $message->user_id = Auth::id();
        $message->save();

        return new MessageResource($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  CustomerMessage $message
     * @return int
     */
    public function destroy(CustomerMessage $message)
    {
        return $message->delete();
    }
}
