<?php

namespace App\Http\Controllers\Api\v1;

use App\Events\LeadProspectionUpdated;
use App\Events\CustomerActionsUpdated;
use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\CustomerAccountActivationFormRequest;
use App\Http\Requests\Customer\CustomerStoreFormRequest;
use App\Http\Requests\Customer\CustomerUpdateFormRequest;
use App\Mail\FeedbackEmail;
use App\Mail\sendingEmail;
use App\Models\AssignmentLog;
use App\Models\Branch;
use App\Models\Customer;
use App\Models\CaseState;
use App\Models\CustomerCase;
use App\Models\CustomerFieldValue;
use App\Models\CustomerGarant;
use App\Models\CustomerVisaRequest;
use App\Models\DocumentType;
use App\Models\FeedbackHistory;
use App\Models\InformationSheet;
use App\Models\LeadStateLog;
use App\Models\MediaSheet;
use App\Models\ProgramStep;
use App\Models\Quote;
use App\Models\Lead;
use App\Models\Role;
use App\Models\Service;
use App\Models\SubStepState;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Rule;

use App\Rules\PhoneIndicator;

use Barryvdh\DomPDF\Facade as PDF;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $customersQB = Customer::with(['program', 'ranking', 'country', 'case', 'case.step'])
            ->where(function ($query) use ($request) {
            });

        $actions = [
            ["action" => "edit", "islink" => true, "title" => "éditer", "available" => 1, "className" => "btn btn-primary customer-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "islink" => false, "title" => "supprimer", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger customer-delete", "icon" => "fas fa-trash"],
        ];


        return DataTables()->eloquent($customersQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CustomerStoreFormRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CustomerStoreFormRequest $request)
    {
        $customer = Customer::create([
            "program_id" => $request['program'],
            "firstname" => $request['firstname'],
            "lastname" => $request['lastname'],
            "customer_email" => $request['email'],
            "birthday" => $request['birthday'],
            "phone" => $request['phone'],
            "adresse_line_1" => $request['adresse1'],
            "adresse_line_2" => $request['adresse2'],
            "postal_code" => $request['postal'],
            "budget" => $request['budget'],
            "ranking_id" => $request['ranking'],
            "country_id" => $request['country'],
            "english_level_id" => $request['english'],
            "school_level_id" => $request['school'],
            "projected_diploma_type_id" => $request['projected'],
            "professional_domain_id" => $request['pdomain'],
            "professional_experience" => $request['experience']
        ]);

        if ($request->hasFile('file-input-cv')) {
            $customer->addMediaFromRequest('file-input-cv')->toMediaCollection('CVs');
        }

        return back()->with(['status' => 'Client crée avec succés!']);
    }

    public function testClearPDF()
    {
        $customer = Customer::where('id', 17823)->first();
        $quote = Quote::where('customer_id', $customer->id)->first();
        $scoring = '[{"title":"Facteurs de base/du capital humain","factors":{"keys":["Age","Niveau de scolarité","Langues officielles","Première langue officielle","Deuxième langue officielle","Expérience de travail au Canada"],"values":["105","90",116,116,0,40]},"total":351},{"title":"Facteurs de l’époux ou du conjoint de fait","factors":{"keys":["Niveau de scolarité","Première langue officielle","Expérience de travail au Canada"],"values":[0,0,0]},"total":0},{"title":"Facteurs liés au transfert des compétences","factors":{"groups":[{"title":"Études (jusqu’à un maximum de 50 points)","factors":{"keys":["A) Maîtrise de une langue officielle et niveau de scolarité","B) Expérience de travail au Canada et niveau de scolarité"],"values":[13,13]},"total":26},{"title":"Expérience de travail à l’étranger (jusqu’à un maximum de 50 points)","factors":{"keys":["A) Maîtrise de une langue officielle et de l’expérience de travail à l’étranger","B) Expérience de travail au Canada et à l’étranger"],"values":[13,13]},"total":26}],"keys":["Certificat de compétence (pour les personnes exerçant un métier spécialisé)"],"values":[50]},"total":100},{"title":"Points supplémentaires (jusqu’à un maximum de 600 points)","factors":{"keys":["Désignation provinciale","Offre d’emploi","Études au Canada","Frère/sœur au Canada","Compétences linguistiques en français"],"values":[600,50,15,0,0]},"total":600}]';
        // $pdf = PDF::loadView('pdfs.clear1002', compact('customer'));
        $pdf = PDF::loadView('pdfs.clear1001', compact('customer'));
        // $bill = $quote;
        // $pdf = PDF::loadView('pdfs.bill-couple-1');
        $customPaper = array(0, 0, 360, 360);
        // $pdf->setPaper($customPaper);
        return $pdf->stream();
    }

    public function updateClear1002(Request $request, $id)
    {

        $rules = [
            "admission_lang_study" => 'required',
            "admission_city" => 'required',
            "admission_school_level" => 'required',
            "admission_est" => 'required',
            "admission_est_name" => 'required',
            "admission_diplomas" => 'required',
            "admission_program" => 'required',
            "combs" => 'required'
        ];
        //dd($request["combs"]);
        $this->validate($request, $rules);

        $customer = Customer::findOrFail($id);
        \App\Models\CustomerClear1002Fields::where('customer_id', $customer->id)->delete();
        foreach (explode(",", $request["combs"]) as $combination) {
            //$customer_ = \App\Models\CustomerClear1002Fields::where('customer_id', $customer->id)->where("combination_id", $combination)->first();
            //if(!$customer_){
            \App\Models\CustomerClear1002Fields::create([
                'customer_id' => $customer->id,
                'combination_id' => $combination
            ]);
            //}
        }
        // $array = array();
        // foreach($request->input() as $key => $value) {
        //     if (array_key_exists($key, $customer_->toArray())) {
        //         $array[$key] = $value;
        //     }
        // }
        //$customer_->update($array);
        $doc = DocumentType::where('name_fr', 'Clear1002 - Orientation')->first();
        $this->addMediaPDF($customer, $doc, 'clear1002', null, null);

        $case_state = \App\Models\CaseState::where("sub_step_state_id", 6)
            ->where("customer_case_id", $customer->case->id)
            ->first();
        $case_state->status = 1;
        $case_state->save();

        return redirect()->route('case-edit', ['customer_case' => $customer->case->id]);
    }

    public function updateClear3(Request $request, $id)
    {

        $customer = Customer::findOrFail($id);
        $phone_rules = [];
        // Rule::unique('customers')->ignore($customer->id)
        if (isset($request['phone_error']) && !empty($request['phone_error'])) {
            //dd($request['phone_error']);
            array_push($phone_rules, new PhoneIndicator($request['phone_error']));
        }
        $rules = [
            //     'firstname' => 'required',
            //     'lastname' => 'required',
            //     'sex' => 'required',
            //     'birthday' => 'required',
            //     'birthday_country' => 'required',
            //     'birthday_city' => 'required',
            //     'adresse_country' => 'required',
            //     'adresse_canada' => 'required',
            'phone' => $phone_rules,
            //     'citizenship' => 'required',
            //     'current_marital_status' => 'required',
            //     'wedding_date' => 'required',
            //     'divorce_date' => 'required',
            //     'customer_email' => 'required',
            //     'spoken_languages' => 'required',
        ];

        $this->validate($request, $rules);

        $customer_ = \App\Models\CustomerClear3Fields::where('customer_id', $customer->id)->first();
        if (!$customer_) {
            $customer_ = \App\Models\CustomerClear3Fields::create([
                'customer_id' => $customer->id,
            ]);
            $customer_->save();
        }
        $array = array();
        foreach ($request->input() as $key => $value) {
            if (array_key_exists($key, $customer_->toArray())) {
                $array[$key] = $value;
            }
        }
        $customer_->update($array);
        // dd($request->input(), $customer_->toArray());
        return redirect()->route('case-edit', ['customer_case' => $customer->case->id]);
    }

    public function updateClear1000(Request $request, $id)
    {
        // dd($request->all());
        $customer = Customer::findOrFail($id);
        $rules = [
            'level_study' => 'required',
            'branch' => 'required',
            'other_branch' => Rule::requiredIf(function () use ($request) {
                return $request['branch'] === 11;
            }),

            // "garant_last_name" => null,
            // "garant_first_name" => null,

            // "guarantor_investment_income" => null,
            // "guarantor_rental_income" => null,
            // "guarantor_self_employed_income" => null,
            // "guarantor_professional_income" => null,
            // "guarantor_business_income" => null,
            // "guarantor_retirement_income" => null,
            // "guarantor_work_income" => null,

            // "goods_res" => null,

            // "tef_test_res" => null,

            'grade' => 'required|numeric|between:0,20.00',
            'math_grade' => 'required|numeric|between:0,20.00',
            'lang_level' => 'required',
            // 'diploma_fr' => 'required',
            // 'diploma_en' => 'required',
            'language' => 'required',
            'est_name' => 'required',
            'training_type' => 'required',

            'diplomas' => 'required',
            'program_first' => 'required',
            'program_second' => 'required',
            'annual_budget' => 'required',
            'province' => 'required',
            'city' => 'required',

            'tuition_fees' => 'required',
            'cpg_account' => 'required',
            // req only for CANADA
            'tef_test' => Rule::requiredIf(function () use ($customer) {
                return $customer->program_id == 4;
            }),
            'tef_test_res' => Rule::requiredIf(function () use ($request) {
                return $request['tef_test'] == 3;
            }),
            // '' => 'required',
            'study_session' => 'required',
            'garant_last_name' => 'required',
            'garant_first_name' => 'required',
            'is_language_taken' => 'required',
            'language_tests' => Rule::requiredIf(function () use ($request) {
                return $request['is_language_taken'] != 2;
            }),
            // 'test_fr' => 'required',
            // 'required_unless:diploma_fr,1',
            // 'test_en' => 'required',
            'garant_type' => 'required',

            // 'income' => 'required',
            'bank_amount' => 'required',
            'amount' => 'required',
            'goods' => 'required',
            'goods_res' => Rule::requiredIf(function () use ($request) {
                return $request['goods'] == 9;
            }),
            'graduation_year' => 'required',
            // 'business_income' => 'required',
            'guarantor_income_type' => 'required',
            // 'relationship_student_guarantor' => 'required',

            'rental_income' => Rule::requiredIf(function () use ($request) {
                return $request['goods'] == 9;
            }),

            'health' => 'required',
            'health_rep' => Rule::requiredIf(function () use ($request) {
                return $request['health'] == 4;
            }),
            'criminal_record' => 'required',
            'criminal_record_rep' => Rule::requiredIf(function () use ($request) {
                return $request['criminal_record'] == 6;
            }),
            /*'visa_request' => 'required',
            'visa_request_date' => Rule::requiredIf(function () use ($request) {
                return $request['visa_request'] == 82;
            }),
            'visa_request_type' => Rule::requiredIf(function () use ($request) {
                return $request['visa_request'] == 82;
            }),
            'visa_request_result' => Rule::requiredIf(function () use ($request) {
                return $request['visa_request'] == 82;
            }),*/
            // "currency_id" => 'required',
        ];
        // dd($request['visa_request'], $request['visa_request_date']);
        // dd($request);
        // $this->validate($request, $rules);
        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            // $messages = $validator->messages();
            // $failed = $validator->failed();
            // dd($messages, $failed);
            $this->validate($request, $rules);
        }

        // save informations to CustomerClear1000Fields
        $customer = Customer::findOrFail($id);
        $customer_ = \App\Models\CustomerClear1000Fields::where('customer_id', $customer->id)->first();
        if (!$customer_) {
            $customer_ = \App\Models\CustomerClear1000Fields::create([
                'customer_id' => $customer->id,
            ]);
            $customer_->save();
        }
        $array = array();
        $garants = array();
        $garants_vs = array();
        $visa_requests = array();
        $remarks = array();
        // dd($request->input());
        foreach ($request->input() as $key => $value) {
            if (is_array($value) && count($value) > 0) {
                if (!str_starts_with($key, 'dyn-')) {
                    $customer_values = CustomerFieldValue::where('customer_id', $customer->id)->where('field_name', $key);
                    foreach ($customer_values->get() as $val) $val->delete();
                    foreach ($value as $val) {
                        CustomerFieldValue::create([
                            'customer_id' => $customer->id,
                            'field_name' => $key,
                            'model_id' => $val
                        ]);
                    }
                    $value = $value[0];
                }
            }
            if (array_key_exists($key, $customer_->toArray())) {
                $array[$key] = $value;
            }
            if (str_starts_with($key, 'remark-')) {
                $remarks[preg_replace('/remark-/', '', $key, 1)] = $value;
            }
            if (str_starts_with($key, 'dyn-')) {
                $new = preg_replace('/dyn-/', '', $key, 1);
                $pos = strrpos($new, "_");
                $str1 = substr($new, 0, $pos);
                $str2 = substr($new, $pos + 1);
                if (in_array($str1, ["visa_request", "visa_request_date", "visa_request_type", "visa_request_result"]))
                    $visa_requests[$str2][$str1] = $value;
                else {
                    $garants[$str2][$str1] = $value;
                }
            }
        }
        $customer_->update($array);
        // ajouter nouveau branche
        /*if (isset($request['new-branch']) && !empty($request['new-branch'])) {
            $branchs = Branch::where('branch_label_fr', 'like', $request['new-branch'])
                ->orWhere('branch_label_en','like', $request['new-branch']);
            if ($branchs->count() == 0 ) {
                $branch = Branch::create(
                    [
                        'branch_label_fr' => $request['new-branch'],
                        'branch_label_en' => $request['new-branch'],
                    ]
                );
                $customer_->update(['branch' => $branch->id]);
            }
        }*/
        $garants_ = CustomerGarant::where('customer_id', $customer->id)->orderBy('id', 'ASC')->get();
        // $visa_requests_ = CustomerVisaRequest::where('customer_id', $customer->id)->orderBy('id', 'ASC')->get();
        //dd($garants);
        $ind = 0;
        foreach ($garants as $garant) {
            $garant["customer_id"] = $customer->id;
            if (isset($garants_[$ind])) {
                //dd($garant);
                $garants_[$ind]->update($garant);
            } else {
                CustomerGarant::create($garant);
            }
            $ind += 1;
        }
        // $visa_requests
        $visa_requests_ = CustomerVisaRequest::where('customer_id', $customer->id)->orderBy('id', 'ASC')->get();
        $ind = 0;
        foreach ($visa_requests as $vrequest) {
            $vrequest["customer_id"] = $customer->id;
            if (isset($visa_requests_[$ind])) {
                $visa_requests_[$ind]->update($vrequest);
            } else {
                CustomerVisaRequest::create($vrequest);
            }
            $ind += 1;
        }

        foreach ($remarks as $key => $value) {
            $field = \App\Models\CustomerField::where("field_name", $key)->first();

            if ($field) {
                $conseill = \App\Models\CustomerFieldRemark::where('customer_id', $customer->id)->where('field_id', $field->id)->first();
                if ($conseill) {
                    $conseill->value = $value;
                    $conseill->remark_id = $value;
                    $conseill->save();
                } else {
                    //dd($key, $field);
                    $conseill  =  \App\Models\CustomerFieldRemark::create([
                        'customer_id' => $customer->id,
                        'field_id' => $field->id,
                        'remark_id' => $value,
                        'value' => $value
                    ]);
                    $conseill->save();
                    //dd($remark);
                }
            }
        }
        $doc = DocumentType::where('name_fr', 'Clear1001 – Conseil')->first();
       $this->addMediaPDF($customer, $doc, 'clear1001', null, null);

        //devis
        if ($customer->program_id == 4) {
            $program_id = 1;
        } else {
            $program_id = 2;
        }
        $services = Service::with(['phase', 'details'])
            ->where('program_id', $program_id)
            ->where('billable', 1)
            ->get();

        //dd($customer);
        $quote = NULL;

        DB::transaction(function () use ($customer, $services, &$quote) {
            // $quote = Quote::create($quoteArr);
            $quote = Quote::where('customer_id', $customer->id)->first();
            if (!$quote) {
                $quoteArr = [
                    'user_id' => Auth::id(),
                    'customer_id' => $customer->id
                ];
                $quote = Quote::create($quoteArr);
            }

            $servicesToAttach = [];

            foreach ($services as $service) {
                $servicesToAttach[$service->id] = [
                    'billed_price' => $service->single_price,
                    'qte' => 1
                ];
            }

            $quote->services()->sync($servicesToAttach);
        });

        $doc = DocumentType::where('name_fr', 'Devis')->first();
        $this->addMediaPDF($customer, $doc, 'devis', $quote, null);

        //contrat
        $doc = DocumentType::where('name_fr', 'Contrat client')->first();
      $this->addMediaPDF($customer, $doc, 'contrat', $quote, null);
        /*if (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'lead-view') {
            LeadProspectionUpdated::dispatch($customer->lead, 'Clear modifiée');
        }*/
        $customer->lead->update(array('processed_message' => 'Clear modifiée', 'processed_at' => Carbon::now()));
        return redirect()->route('lead-view', ['lead' => $customer->lead_id]);
    }

    /**
     * Update the clear1004 scoring.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateClear1004(Request $request, $id)
    {
        // dd($request->input());
        $customer = Customer::findOrFail($id);
        $customer_ = \App\Models\CustomerClear1004Fields::where('customer_id', $customer->id)->first();
        if (!$customer_) {
            $customer_ = \App\Models\CustomerClear1004Fields::create([
                'customer_id' => $customer->id,
            ]);
            $customer_->save();
        }
        //dd($customer_->toArray());
        $array = array();
        $remarks = array();
        foreach ($request->input() as $key => $value) {
            if (array_key_exists($key, $customer_->toArray()) && !empty($value) && ($value != 'badvalue')) {
                $field = \App\Models\CustomerField::where('field_name', $key)->first();
                if ($field && $field->select_model == '\App\Models\CustomerRadioField') {
                    // if (!empty($value) && $value != 'badvalue')
                    $array[$key] = $key . "-" . $value;
                } else {
                    $array[$key] = $value;
                }
            }
            if (str_starts_with($key, 'remark-')) {
                $remarks[preg_replace('/remark-/', '', $key, 1)] = $value;
            }
        }
        // dd($array);
        $customer->update($array);
        $customer_->update($array);
        if (!isset($request['other_lang_test']) || empty($request['other_lang_test']) || $request['other_lang_test'] == "C") {
            //dd($request['other_lang_test']);
            $customer_->lang_speaking = null;
            $customer_->lang_listening = null;
            $customer_->lang_reading = null;
            $customer_->lang_writing = null;
            $customer_->save();
        }
        // spouse_lang_test
        if (!isset($request['spouse_lang_test']) || empty($request['spouse_lang_test']) || $request['spouse_lang_test'] == "E") {
            //dd($request['spouse_lang_test']);
            $customer_->spouse_lang_speaking = null;
            $customer_->spouse_lang_listening = null;
            $customer_->spouse_lang_reading = null;
            $customer_->spouse_lang_writing = null;
            $customer_->save();
        }

        foreach ($remarks as $key => $value) {
            $field = \App\Models\CustomerField::where("field_name", $key)->first();

            if ($field) {
                $conseill = \App\Models\CustomerFieldRemark::where('customer_id', $customer->id)->where('field_id', $field->id)->first();
                if ($conseill) {
                    $conseill->value = $value;
                    $conseill->save();
                } else {
                    $conseill  =  \App\Models\CustomerFieldRemark::create([
                        'customer_id' => $customer->id,
                        'field_id' => $field->id,
                        'value' => $value
                    ]);
                    $conseill->save();
                }
            }
        }

        $doc = DocumentType::where('name_fr', 'Clear1004 - Rapport Scoring')->first();
        $this->addMediaPDF($customer, $doc, 'clear1004', $request['scoring'], null);
        //devis
        if ($customer->program_id == 4) {
            $program_id = 1;
        } else {
            $program_id = 2;
        }
        $services = Service::with(['phase', 'details'])
            ->where('program_id', $program_id)
            ->where('billable', 1)
            ->get();
        //dd($customer);
        $quote = NULL;

        DB::transaction(function () use ($customer, $services, &$quote) {

            $quote = Quote::where('customer_id', $customer->id)->first();
            if (!$quote) {
                $quoteArr = [
                    'user_id' => Auth::id(),
                    'customer_id' => $customer->id
                ];
                $quote = Quote::create($quoteArr);
            }

            $servicesToAttach = [];

            foreach ($services as $service) {
                $servicesToAttach[$service->id] = [
                    'billed_price' => $service->single_price,
                    'qte' => 1
                ];
            }

            $quote->services()->sync($servicesToAttach);
        });

        $doc = DocumentType::where('name_fr', 'Devis')->first();
       $this->addMediaPDF($customer, $doc, 'devis', $quote, null);

        //contrat
        $doc = DocumentType::where('name_fr', 'Contrat client')->first();
        $this->addMediaPDF($customer, $doc, 'contrat', $quote, null);

        /*if (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'lead-view') {
            LeadProspectionUpdated::dispatch($customer->lead, 'Clear modifiée');
        }*/
        $customer->lead->update(array('processed_message' => 'Clear modifiée', 'processed_at' => Carbon::now()));

        return response()->json(array("lead_id" => $customer->lead_id));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  CustomerUpdateFormRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id) //CustomerUpdateFormRequest
    {
        // dd($request->all());
        $customer = Customer::findOrFail($id);
        $lead = Lead::findOrFail($customer->lead_id);
        // dd($lead);

        $phone_rules = [
            // removed required cuz unshown fiels @@
            // Rule::requiredIf(function () use ($request) {
            //     return Auth::user()->isAdmin() || Auth::user()->isManager();
            // }),
            Rule::unique('customers')->ignore($customer->id),
            Rule::unique('leads')->ignore($customer->lead_id)
        ];
        if (isset($request['phone_error']) && !empty($request['phone_error'])) {
            //dd($request['phone_error']);
            array_push($phone_rules, new PhoneIndicator($request['phone_error']));
        }

        $rules = [
            'firstname'      => 'min:3|max:30|nullable', // removed required cuz unshown fiels @@
            'lastname'       => 'required|min:3|max:30',
            'email'          => 'required|email|unique:leads,email,' . $customer->lead_id . '|unique:customers,email,' . $customer->id,
            'phone'          => $phone_rules,
            'father-phone'   => [Rule::requiredIf(function () use ($customer, $request) {
                return $customer->program_id == 4 && $request["popup"] == "true";
            }), new PhoneIndicator($request['father-phone_error'])],
            'mother-phone'   => [Rule::requiredIf(function () use ($customer, $request) {
                return $customer->program_id == 4 && $request["popup"] == "true";
            }), new PhoneIndicator($request['mother-phone_error'])],
            'program_field'  => 'nullable',
            'campaign_id' => Rule::requiredIf(function () use ($request, $customer) {
                $canal = \App\Models\LeadFormMarketingCampaign::find($customer->campaign_id);
                return !$canal || $canal->visible != 0;
            }),
            //'optional|exists:lead_form_marketing_campaigns,id',
            'form'        => Rule::requiredIf(function () use ($request) {
                $cmps = \App\Models\LeadFormMarketingCampaign::all();
                $newCmps = $cmps->filter(function ($cmp) use ($request) {
                    return ($cmp->campaign_name == "Reference" || $cmp->campaign_name == "Influenceurs") && $request['campaign_id'] == $cmp->id;
                });
                return $newCmps->count();
            }),
            'birthday' => 'nullable',
            'lead_status' => 'nullable|exists:lead_statuses,id',
            'file-input-cv' => 'nullable|file|mimes:pdf,doc,docx,ods',
            'file-input-cin' => 'nullable|file|mimes:pdf,doc,docx,ods'
        ];

        $array = array();
        $leadArray = array();
        foreach (array_filter($request->input()) as $key => $value) {
            if (array_key_exists($key, $customer->toArray())) {
                // do stuff
                $field = \App\Models\CustomerField::where('field_name', $key)->first();
                $array[$key] = $value;
            }
            if (array_key_exists($key, $lead->toArray())) {
                $leadArray[$key] = $value;
            }
        }
        // $request["popup"]
        if (isset($request["popup"]) && isset($request['mother-phone']) && !empty($request['mother-phone'])) {
            $array["mother_phone"] = $request['mother-phone'];
        }
        if (isset($request["popup"]) && isset($request['father-phone']) && !empty($request['father-phone'])) {
            $array["father_phone"] = $request['father-phone'];
        }

        // dd($array,$leadArray);

        // dd("mafhmtch 1");

        $customer->update($array);
        $lead->update($leadArray);
        if (isset($request["password"]) && !empty($request["password"])) {
            $customer->password = Hash::make($request["password"]);
            $customer->save();
        }

        // $lead = Lead::where("id", $customer->lead_id)->first();
        // dd("mafhmtch");
        if (isset($request['program_id']) && !empty($request['program_id'])) {
            // dd("test");
            /*$customer->program_id = $request['program'];
            $customer->save();*/
            // $lead->program_id = $request['program'];
            $case = \App\Models\CustomerCase::where('customer_id', $customer->id)->first();
            if ($request['program_id'] != $case->program_id) {
                $substep = \App\Models\ProgramSubStep::where("id", 6)->first();
                $docs = $substep->docs()->wherePivot('program_id', '=', $case->program_id)->withPivot('order')->orderBy('order', 'asc')->get();
                foreach ($docs as $doc) {
                    foreach ($customer->getMedia($doc->collection_name) as $media) $media->delete();
                }
            }
            $case->program_id = $request['program_id'];
            $case->save();
        }
        $old_state = $lead->lead_status_id;
        $lead->lead_status_id = $request['lead_status'];
        $customer->lead_status = $request['lead_status'];
        $customer->save();
        // if(isset($request['program_id']) && !empty($request['program_id'])) {}
        if ($old_state != $request['lead_status']) {
            LeadStateLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'state_id' => $lead->lead_status_id,
            ]);
        }
        // dd($lead->lead_status_id, $customer->lead_status);
        /*if(isset($request['lead_status']) && !empty($request['lead_status']) && $request['lead_status'] != null) {
        }*/
        $lead->program_field = $request['program_field'];
        // support
        if (isset($request['support']) && !empty($request['support'])) {
            $lead->support_id = $request['support'];
        }

        if ($request->hasFile('file-input-cv')) {
            $lead->addMediaFromRequest('file-input-cv')->toMediaCollection('CVs');
        }
        if ($request->hasFile('file-input-cin')) {
            $lead->addMediaFromRequest('file-input-cin')->toMediaCollection('CINs');
        }
        if (isset($request['form']) && !empty($request['form'])) {
            $lead->influencer = $request['form'];
        }
        if (isset($request['conseiller']) && !empty($request['conseiller'])) {
            $lead->conseiller_id = $request['conseiller'];
        }
        if (isset($request['radios-office']) && !empty($request['radios-office'])) {
            $lead->to_office = $request['radios-office'];
        }
        $lead->save();


        if (isset($request['conseiller']) && !empty($request['conseiller'])) {
            AssignmentLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'conseiller_id' => $request['conseiller'],
            ]);
        }

        if (isset($request['support']) && !empty($request['support'])) {
            AssignmentLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'conseiller_id' => $request['support'],
            ]);
        }


        /*if (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'lead-view') {
            LeadProspectionUpdated::dispatch($lead, 'Fiche modifiée');
        }
        if (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'case-edit') {
            CustomerActionsUpdated::dispatch($customer, 'Fiche modifiée');
        }*/
        // $customer->update(array('processed_message' => 'Fiche modifiée', 'processed_at' => Carbon::now()));
        $customer->lead->update(array('processed_message' => 'Fiche modifiée', 'processed_at' => Carbon::now()));

        //dd($lead);
        return back()->with(['status' => 'Fiche modifiée avec succés!']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CustomerAccountActivationFormRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(CustomerAccountActivationFormRequest $request, $id)
    {
        $customer = Customer::findOrFail($id);

        $customer->email = $request['email'];
        $customer->password = Hash::make($request['password']);
        $customer->save();

        return back()->with(['status' => 'Compte Client activé avec succés!']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);

        $customer->password = NULL;
        $customer->save();

        return back()->with(['status' => 'Compte Client désactivé avec succés!']);
    }

    public function updateCustomerBackoffice(Request $request)
    {
        $custmers_ids = $request['custmers_ids'];
        $users = $request['resultBackofficeArray'];
        $backoffices = [];
        foreach ($users as $user) {
            if (User::find($user)->role->id == Role::Backoffice) {
                array_push($backoffices, $user);
            }
        }
        $offset_b = 0;
        foreach ($custmers_ids as $customers_id) {
            if ($offset_b == count($backoffices)) {
                $offset_b = 0;
            }
            $customer = Customer::findOrFail($customers_id);

            if (count($backoffices))
                $customer->backoffice_id = $backoffices[$offset_b];

            $customer->save();
            $offset_b++;
        }
        return true;
    }
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function getCallStates(Request $request, Lead $lead)
    {
        if (Auth::id() == 1) {
            $statusAll = \App\Models\LeadStatus::all()->pluck('status_label_fr', 'id')->toArray();
            $statusAll['No_Chnage'] = 'No Change';
            $statusAll['Rappel'] = 'Rappel';
        } else {
            $statusAll = \App\Models\LeadStatus::where('visible', 1)->get()->pluck('status_label_fr', 'id')->toArray();
            if (in_array($lead->lead_status_id, [5, 11, 13, 14])) {
                $statusAll = \App\Models\LeadStatus::whereIn('id', [13, 14])->get()->pluck('status_label_fr', 'id')->toArray();
            }
            if ($lead->lead_status_id == 1 || is_null($lead->lead_status_id)) {
                $statusAll = \App\Models\LeadStatus::whereIn('id', [1, 2, 3, 6])->get()->pluck('status_label_fr', 'id')->toArray();
            }
            $statusAll['No_Chnage'] = 'No Change';
            $statusAll['Rappel'] = 'Rappel';
        }

        // // hide status : Rencontre 1 pour agent et conseiller, manager
        // if (Auth::user()->isManager() || Auth::user()->isAgent()) {
        //     unset($statusAll[array_search("Rencontre 1", $statusAll)]);
        // }
        // // hide status : Injoignable pour agent et conseiller, manager
        // if (Auth::user()->isManager() || Auth::user()->isConseiller() || Auth::user()->isAgent()) {
        //     unset($statusAll[array_search("Injoignable", $statusAll)]);
        // }

        return response()->json($statusAll);
    }
    public function getClients(Request $request)
    {
        // request
        // term == firstname & lastname
        // phone == phonenumber

        $leadsQuery = DB::table("leads as l")
            ->leftJoin('customers as c', function ($join) {
                $join->on('c.lead_id', '=', 'l.id');
            })
            ->where(function ($query) use ($request) {
                if (isset($request['term']) && !empty($request['term'])) {
                    $search = $request["term"];
                    $query->where(function ($query) use ($search) {
                        $query->where('c.firstname', 'LIKE', "%$search%");
                        $query->orWhere('c.lastname', 'LIKE', "%$search%");
                        $query->orWhere('l.firstname', 'LIKE', "%$search%");
                        $query->orWhere('l.lastname', 'LIKE', "%$search%");
                        $query->orWhere('l.phone', 'like', "%$search%");
                        $query->orWhere(DB::raw("CONCAT(c.firstname, ' ', c.lastname)"), 'LIKE', "%$search%");
                        $query->orWhere(DB::raw("CONCAT(c.lastname, ' ', c.firstname)"), 'LIKE', "%" . $search . "%");
                        $query->orWhere(DB::raw("CONCAT(l.firstname, ' ', l.lastname)"), 'LIKE', "%" . $search . "%");
                        $query->orWhere(DB::raw("CONCAT(l.lastname, ' ', l.firstname)"), 'LIKE', "%" . $search . "%");
                        // $query->orWhere('c.phone', 'like', '%'. $search .'%');
                    });
                }

                if (Auth::user()->isConseiller()) {
                    $query->where('l.conseiller_id', Auth::id());
                }
                if (Auth::user()->isSupport()) {
                    $query->where('l.support_id', Auth::id());
                    $query->where('c.is_client', 0);
                }
            })
            ->select("l.id", DB::raw("CONCAT(l.firstname, ' ', l.lastname) AS name"), "c.is_client as is_client", "l.phone", "l.conseiller_id as conseiller", "l.support_id as support", "c.city as city")
            ->limit(10);
        $data = $leadsQuery->get();
        return response()->json($data);
    }
    public function getCustomers(Request $request)
    {
        $data = [];

        if ($request->has('q')) {
            $search = isset($request->q['term']) ? $request->q['term'] : '';
            $data = Customer::select("id", DB::raw("CONCAT(firstname, ' ', lastname) AS name"), "is_client")
                /*$data = DB::table('customers as c')
                    ->leftJoin('customer_cases as case', 'case.customer_id', '=', 'c.id')
                    ->select("c.id", DB::raw("CONCAT(c.firstname, ' ', c.lastname) AS name"), "case.program_step_id")*/

                ->where(function ($query) use ($search, $request) {

                    if (isset($request['pid']) && !empty($request['pid'])) {
                        if ($request['pid'] == 4) {
                            $query->where('program_id', $request['pid']);
                        } else {
                            $query->where('program_id', '!=', 4);
                        }
                    }
                    if (isset($request['cal']) && !empty($request['cal'])) {
                        $query->whereNotNull('lead_id');
                    }
                })

                ->where(function ($query) use ($search) {

                    $query->where('firstname', 'LIKE', "%$search%");
                    $query->orWhere('lastname', 'LIKE', "%$search%");
                })

                ->where(function ($query) use ($search) {

                    $query->where('firstname', 'LIKE', "%$search%");
                    $query->orWhere('lastname', 'LIKE', "%$search%");
                })
                ->get();
        }

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Customer $lead
     * @return int
     * @throws \Exception
     */
    public function destroy($id)
    {
        //dd($customer);
        $customer = Customer::findOrFail($id);
        return $customer->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Customer $lead
     * @param  String $collectionName
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteMedia(Customer $customer, $collectionName)
    {
        foreach ($customer->getMedia($collectionName) as $media) $media->delete();
        return back()->with(['status' => 'Document supprimé avec succés!']);
    }

    /**
     * Add media to model.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Customer $lead
     * @param  String $collectionName
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */

    /*public function addMediaTest(Request  $request, Customer $customer)
    {
        if($request->hasFile('file-input-media')) {

            foreach ($customer->getMedia($collectionName) as $media) $media->delete();

            $documentType = DocumentType::where('collection_name', $collectionName)->get()[0];

            $customer->addMediaFromRequest('file-input-media')
                ->withCustomProperties([
                    'original_file_id' => 0,
                    'pending_file_id' => 0,
                    'visible_for_customer' => 1,
                    'signable' => $documentType->signable,
                    'customer_owner' => $documentType->customer_owner,
                    'is_signed' => 0,
                    'is_requested' => 0,
                ])
                ->toMediaCollection($collectionName);
            return response()->json(['success'=>'Document ajouté avec succés!']);
        }
    }*/
    public function addMedia(Request  $request, Customer $customer, $collectionName)
    {
        /*$mediaSheet = MediaSheet::find(2);
        $mediaSheet->docs()
            ->wherePivot('program_id', '=', 4)
            ->withPivot(['required', 'order'])
            ->get();*/
        if ($request->hasFile('file-input-media')) {
            // foreach ($customer->getMedia($collectionName) as $media) $media->delete();
            if (DocumentType::where('collection_name', $collectionName)->count() > 0) {
                $documentType = DocumentType::where('collection_name', $collectionName)->first();
                // dd($request->garant, implode(",", $documentType->tagNames()), $documentType->sheets()->wherePivot('media_sheet_id', 2)->first()->toArray());
                $customProperties = [
                    'original_file_id' => 0,
                    'pending_file_id' => 0,
                    'visible_for_customer' => 1,
                    'signable' => $documentType->signable,
                    'customer_owner' => $documentType->customer_owner,
                    'is_signed' => 0,
                    'is_requested' => 0,
                ];
                if ($documentType->sheets()->wherePivot('media_sheet_id', 2)->count() > 0) {
                    if (count($documentType->tagNames())) $tag = $documentType->tagNames()[0];
                    else $tag = "Other";
                    $customProperties["tag"] = $tag;
                    $customProperties["garant"] = $request['garant'];
                }
                $customer->addMediaFromRequest('file-input-media')
                    ->withCustomProperties($customProperties)
                    ->toMediaCollection($collectionName);
                return response()->json(['success' => 'Document ajouté avec succés!', 'document' => $documentType->id]);
            } else {
                return response()->json('Document not found!', 404);
                /*$customer->addMediaFromRequest('file-input-media')
                    ->withCustomProperties([
                        'original_file_id' => 0,
                        'pending_file_id' => 0,
                        'visible_for_customer' => 1,
                        'signable' => 0,
                        'customer_owner' => 0,
                        'is_signed' => 0,
                        'is_requested' => 0,
                    ])
                    ->toMediaCollection($collectionName);
                return response()->json(['success'=>'Document ajouté avec succés!', 'document' => $collectionName]);*/
            }
        }
    }

    public function addRequestedDocument(Customer $customer, DocumentType $documentType, ProgramStep $programStep)
    {
        $customer->requestedDocuments()->attach([
            $documentType->id => ['program_step_id' => $programStep->id]
        ]);
        return back()->with(['status' => 'Demande de document envoyée avec succés!']);
    }

    public function removeRequestedDocument(Customer $customer, DocumentType $documentType, ProgramStep $programStep)
    {
        $customer->requestedDocuments()->detach([
            $documentType->id
        ]);
        return back()->with(['status' => 'Demande de document retirée avec succés!']);
    }

    public function toggleLinkVisibility(Customer $customer, DocumentType $documentType, ProgramStep $programStep)
    {
        $link = $customer->requestedDocuments()->wherePivot('program_step_id', '=', $programStep->id)->wherePivot('document_type_id', '=', $documentType->id)->wherePivot('link', '=', true);

        if (count($link->get()))
            $link->detach($documentType->id);
        else
            $customer->requestedDocuments()->attach([
                $documentType->id => [
                    'program_step_id' => $programStep->id,
                    'link' => true,
                ]
            ]);

        return back()->with(['status' => 'Etat lien modifiée avec succés!']);
    }

    public function toggleSheetVisibility(Customer $customer, InformationSheet $informationSheet)
    {
        $sheet = $customer->sheets()->wherePivot('information_sheet_id', '=', $informationSheet->id);

        if (count($sheet->get()))
            if (count($sheet->wherePivot('can_edit', '=', true)->get()))
                $customer->sheets()->updateExistingPivot($informationSheet->id, [
                    'can_edit' => false
                ]);
            else
                $customer->sheets()->updateExistingPivot($informationSheet->id, [
                    'can_edit' => true
                ]);
        else
            $customer->sheets()->attach([
                $informationSheet->id => [
                    'can_edit' => true
                ]
            ]);

        return back()->with(['status' => 'Visibilité Fiche modifiée avec succés!']);
    }
    public function toggleMediaSheetVisibility(Customer $customer, MediaSheet $mediaSheet)
    {
        $sheet = $customer->mediaSheets()->wherePivot('media_sheet_id', '=', $mediaSheet->id);

        if (count($sheet->get())) {
            if (count($sheet->wherePivot('can_edit', '=', true)->get()))
                $customer->mediaSheets()->updateExistingPivot($mediaSheet->id, [
                    'can_edit' => false
                ]);
            else
                $customer->mediaSheets()->updateExistingPivot($mediaSheet->id, [
                    'can_edit' => true
                ]);
        } else {
            $customer->mediaSheets()->attach([
                $mediaSheet->id => [
                    'can_edit' => true
                ]
            ]);
        }

        return back()->with(['status' => 'Visibilité Fiche modifiée avec succés!']);
    }

    public function toggleMeet(Customer $customer)
    {
        // toggle media sheet visibility
        /*$mediaSheets = \App\Models\MediaSheet::with('substeps')->get();
        foreach ($mediaSheets as $mediaSheet) {
            if($mediaSheet->substeps()->wherePivot('program_id', $customer->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                continue;
            $sheet = $customer->mediaSheets()->wherePivot('media_sheet_id', '=', $mediaSheet->id);

            if(count($sheet->get()))
                $customer->mediaSheets()->updateExistingPivot($mediaSheet->id, [
                    'can_edit' => true
                ]);
            else
                $customer->mediaSheets()->attach([
                    $mediaSheet->id => [
                        'can_edit' => true
                    ]
                ]);
        }*/
        // $sheets = \App\Models\InformationSheet::with('substeps')->get();
        /*foreach ($sheets as $sheet) {
            if($sheet->substeps()->wherePivot('program_id', $customer->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                continue;
            $sheet = $customer->mediaSheets()->wherePivot('media_sheet_id', '=', $mediaSheet->id);

            if(count($sheet->get()))
                $customer->mediaSheets()->updateExistingPivot($mediaSheet->id, [
                    'can_edit' => true
                ]);
            else
                $customer->mediaSheets()->attach([
                    $mediaSheet->id => [
                        'can_edit' => true
                    ]
                ]);
        }*/
        // send Email
        if ($customer->program_id != 4) {
            $languser = auth()->user()->lang;
            if (session()->get('locale') == 'en' || $languser == 'en') {
                $object = "Language test guide";
                $message = "
                Hello:" . $customer->firstname . ",
                As agreed, I provide you with the procedure and resources to get prepared for your English/French exam. It will be necessary to apply for the tests as soon as possible and be prepared for the exam.
                Finally, once you have the test results, you can send them to us.
                ENGLISH / FRENCH TEST (suggested links to apply for the test):
                - Links IELTS in Spain
                https://www.ielts.org/for-test-takers/book-a-test/location-list/spain/tv
                - Links TCF and TEF
                https://www.france-education-international.fr/centres-d-examen/carte?type-centre=tcf
                RESOURCES IELTS (general), TCF and TEF Canada
                - TEF resources:
                https://drive.google.com/drive/folders/1Cz2ZFUA3nMPeooPqbVN-AKUgJLPkVVW9
                - TCF resources:
                https://drive.google.com/drive/folders/1x9EjLe4qhrIMmf2JRFZzWjMFt67pR1jH
                - IELTS resources:
                https://drive.google.com/drive/folders/1woO1uNc-NojNUxBHa1BRzHXHZ4n3J8T8
                We wish you an optimal preparation.
                Sincerely,
                ";
            } else {
                $object = 'Guide des tests de langue';
                $message = "
                    Bonjour Mer " . $customer->firstname . ",
                    Comme convenu je vous fais part de la procédure ainsi que les ressources pour préparer vos tests d'anglais et français. Il faudra s'inscrire aux tests dès que possible et se préparer pour l'examen. Enfin, une fois que vous avez les résultats des tests vous pouvez nous les transmettre.
                    Voilà le lien pour des informations sur le TEF Canada et TCF Canada (Frais de 3000 DHS par personne)
                    https://www.lefrancaisdesaffaires.fr/centres/maroc/
                    Voilà le lien pour des information sur le IELTS (General)
                    https://www.ielts.org/book-a-test/find-a-test-location/location-list/morocco/tv (Frais de 3000 DHS par personne)
                    Ressource de préparation pour le IELTS (General), TEF, TCF Canada
                    - IELTS
                    https://drive.google.com/drive/folders/1xqbB3ImVlC8f-FJKRXQ5Ynjs_GEkfKcS
                    - TEF
                    https://drive.google.com/drive/folders/1Cz2ZFUA3nMPeooPqbVN-AKUgJLPkVVW9
                    - TCF
                    https://drive.google.com/drive/folders/1x9EjLe4qhrIMmf2JRFZzWjMFt67pR1jH

                    Je vous souhaite une bonne préparation.
                    Cordialement,
                    ";
            }
            $data = array(
                'name' => $customer->firstname . ' ' . $customer->lastname,
                'object' => $object,
                'message' => $message,
                'elements' => "",
            );

            if ($customer->email) {
                try {
                    Mail::to($customer->email)->send(new sendingEmail($data));
                } catch (\Swift_TransportException $e) {
                    // echo 'Exception reçue : ',  $e->getMessage(), "\n";
                }
            }

            // try {
            //     Mail::to($customer->email)->send(new sendingEmail($data));
            // } catch (Exception $e) {
            //     // echo 'Exception reçue : ',  $e->getMessage(), "\n";
            // }
        }
        // Enable payment for Agent
        $step_state = SubStepState::where("name_fr", "Rencontre 2: Terminer Appel")->first();
        $meet = CaseState::where("sub_step_state_id", $step_state->id)
            ->where("customer_case_id", $customer->case->id)
            ->first();
        if (!$meet)
            $meet = CaseState::create([
                'user_id' => Auth::id(),
                'customer_case_id' => $customer->case->id,
                'sub_step_state_id' => $step_state->id,
                'status' => true,
            ]);
        else
            $meet->status = true;

        $meet->save();
        if (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'case-edit') {
            CustomerActionsUpdated::dispatch($customer, 'Terminer appel');
        }

        return back()->with(['status' => 'Visibilité Fiche modifiée avec succés!']);
    }

    /**
     * Get Lead data
     */
    public function getLeadData($id = null)
    {
        $lead = Lead::findOrFail($id);
        $customer = Customer::where('lead_id', $id)->first();
        return response()->json([
            "id" => $id,
            "phone" => $lead->phone,
            "city" => $customer ? $customer->city : $lead->city,
        ]);
    }

    /**
     * Get Customer data
     */
    public function getCustomerData($id = null)
    {
        $case = CustomerCase::findOrFail($id);
        return response()->json([
            "id" => $id,
            "phone" => $case->customer->phone,
        ]);
    }
    /**
     * Update meet stat
     */
    public function setMeetIsDone($id = null, $meet = null)
    {
        $customer = Customer::findOrFail($id);
        switch ($meet) {
            case "r1":
                $customer->r1 = true;
                $customer->save();
                break;
            case "r2":
                $customer->r2 = true;
                $customer->save();
                break;
            case "r3":
                $customer->r3 = true;
                $customer->save();
                break;
            case "r4":
                $customer->r4 = true;
                $customer->save();
                break;
            case "r5":
                $customer->r5 = true;
                $customer->save();
                break;
        }

        $feedback = FeedbackHistory::create([
            "lead_id" => $customer->lead_id,
        ]);

        $data = array(
            'name' => $customer->firstname . ' ' . $customer->lastname,
            'id' => $feedback->uuid,
        );

        if ($customer->email) {
            try {
                Mail::to($customer->email)->send(new FeedbackEmail($data));
            } catch (\Swift_TransportException $e) {
            }
        }

        return back()->with(['status' => 'Etat rencontre modifiée avec succés!']);
    }

    /**
     * Update Orientation
     */
    public function setOrientation($id = null, $orientationId = null)
    {
        // dd($id);
        $customer = Customer::findOrFail($id);
        // dd($customer);
        $customer->orientation_id = $orientationId;
        $customer->save();
        return back()->with(['status' => 'Etat Orientation modifiée avec succés!']);
    }
}
