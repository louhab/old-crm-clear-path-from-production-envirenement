<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\CustomerScoreEvaluation;
use Illuminate\Http\Request;

class CustomerScoreEvaluationController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  CustomerScoreEvaluation  $customer_score_evaluation
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request,CustomerScoreEvaluation $customer_score_evaluation)
    {
        $customer_score_evaluation->ranking_id = $request['ranking'];
        $customer_score_evaluation->score = $request['score'];

        $customer_score_evaluation->save();

        return back()->with(['status' => 'Evaluation modifié avec succés!']);
    }
}
