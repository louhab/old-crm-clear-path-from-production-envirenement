<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SelectLanguageController extends Controller
{
    /**
     * Select Language.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request){
        App::setlocale($request['locale']);
        session()->put('locale', $request['locale']);
        return response()->json(['success' => 'success'], 200);;
    }
}
