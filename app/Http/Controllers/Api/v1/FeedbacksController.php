<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Mail\HappyEmail;
use App\Models\Feedback;
use App\Models\FeedbackHistory;
use App\Models\Lead;
use App\Models\User;
use Illuminate\Http\Request;
use App\Rules\PhoneIndicator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;

class FeedbacksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $phone_rules = [Rule::requiredIf(function () use ($request) {
            return $request->feedback == 0;
        })];
        if (isset($request['phone_error']) && !empty($request['phone_error'])) {
            array_push($phone_rules, new PhoneIndicator($request['phone_error']));
        }
        $rules = [
            'first_name' =>  [Rule::requiredIf(function () use ($request) {
                return $request->feedback == 0;
            }), "min:3", 'max:30'],
            'last_name' => [Rule::requiredIf(function () use ($request) {
                return $request->feedback == 0;
            }), "min:3", 'max:30'],
            'conseiller_id' => Rule::requiredIf(function () use ($request) {
                return $request->feedback == 0;
            }),
            'phone' => $phone_rules,
        ];


        $this->validate($request, $rules);

        if ($request->feedback == 0) {
            $lead = Lead::where('phone', "like", "%" . $request['phone'] . "%")->first();
        } else {
            $feed = FeedbackHistory::where('id', $request->feedback)->first();
            $lead = Lead::where('id', $feed->lead_id)->first();
        }

        $feedback = Feedback::create([
            "first_name" => $request['first_name'],
            "last_name" => $request['last_name'],
            "phone" => $request['phone'],
            "conseiller_id" => $request['conseiller_id'],
            "lead_id" => $lead ? $lead->id : null,
            "degree_of_professionalism" => $request['degree_of_professionalism'],
            "quality_of_presentation" => $request['quality_of_presentation'],
            "degree_of_mastery" => $request['degree_of_mastery'],
            "speed" => $request['speed'],
            "overall" => $request['overall'],
            "note" => $request['note'],
            "city" => $request['city'],
        ]);

        if ($request->feedback == 0) {
            $advisor = User::where('id', $request['conseiller_id'])->first();
            $data = array(
                'name' => $request['first_name'] . " " . $request['last_name'],
                'phone' => $request['phone'],
                'advisor' => $advisor->name,
                "q1" => $request['degree_of_professionalism'],
                "q2" => $request['quality_of_presentation'],
                "q3" => $request['degree_of_mastery'],
                "q4" => $request['speed'],
                "q5" => $request['overall'],
                "note" => $request['note'],
            );
        } else {
            $feed->submitted = 1;
            $feed->feedback_id = $feedback->id;
            $feed->save();
            $advisor = User::where('id', $lead->conseiller_id)->first();
            $data = array(
                'name' => $lead->firstname . " " . $lead->lastname,
                'phone' => $lead->phone,
                'advisor' => $advisor->name,
                "q1" => $request['degree_of_professionalism'],
                "q2" => $request['quality_of_presentation'],
                "q3" => $request['degree_of_mastery'],
                "q4" => $request['speed'],
                "q5" => $request['overall'],
                "note" => $request['note'],
            );
        }

        try {
            Mail::to("happy@cpcanada.ma")->send(new HappyEmail($data));
        } catch (\Swift_TransportException $e) {
        }


        // redirect to thank you message
        // return back()->with(['status' => 'Done']);
        return response()->json([
            "status" => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
