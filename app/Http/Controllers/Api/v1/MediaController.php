<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MediaController extends Controller
{
    public function toggleProperty(Request $request, Media $media)
    {
        $media->setCustomProperty($request['property'], !$media->getCustomProperty($request['property']));
        $media->save();

        return back()->with(['status' => 'Propriété du document modifiée avec succés!']);
    }

    public function getClear11()
    {
        //PDF file is stored under project/public/download/info.pdf
        $file = public_path() . "/pdf/Clear11 - Phase 1-Procédure pour demander l'équivalence des diplomes.pdf";

        $headers = array(
            'Content-Type: application/pdf',
        );

        return response()->download($file, 'filename.pdf', $headers);
    }
}
