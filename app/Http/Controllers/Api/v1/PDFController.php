<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\BillResource;
use App\Models\Bill;
use App\Models\Customer;
use App\Models\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class PDFController extends Controller
{
    public function view(Media $media)
    {
        //
        // dd($media->getPath());
        // $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
        // return PDF::loadFile($media->getPath())->stream('view_file.pdf', array("Attachment" => false));
        // return PDF::loadFile($media->getFullUrl())->stream('view_file.pdf', array("Attachment" => false));
        return response()->file($media->getPath());
    }

    public function getCustomerMedia(Customer $customer, $collection)
    {
        $data = [];
        foreach ($customer->getMedia($collection) as $media) {
            // array_push($data, $media->getFullUrl());
            $data[$media->id] = $media->getFullUrl();
        }
        return response()->json($data);
    }

    public function destroy(Media $media)
    {
        // dd((auth()->user() instanceof User)  );
        if ((Auth::guard('customer')->user() && (Auth::id() == $media->model->id)) || ((auth()->user() instanceof User) && (auth()->user()->isAdmin() || Auth::user()->isVerificateur() || auth()->user()->isManager() || auth()->user()->isConseiller()))) {
            return $media->delete();
        }
        return response()->json([]);
    }
}
