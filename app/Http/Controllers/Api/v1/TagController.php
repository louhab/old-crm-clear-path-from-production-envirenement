<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tag\TagAddFormRequest;
use App\Http\Requests\Tag\TagUpdateFormRequest;
use App\Http\Resources\TagResource;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $tagsQB = DB::table('tags')
            ->select('*');

        if (isset($request['tag_name']) && !empty($request['tag_name'])) {
            $tagsQB->where('tag_name', 'like', '%' . $request['tag_name'] . '%');
        }

        $actions = [
            ["action" => "edit", "title" => "Modifier Mot clé", "available" => 1, "className" => "btn btn-primary tag-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer Mot clé", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger tag-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables()->of($tagsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return TagResource
     */
    public function store(TagAddFormRequest $request)
    {
        return new TagResource(Tag::create([
            "tag_name" => $request['add-form-tag_name'],
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return TagResource
     */
    public function update(TagUpdateFormRequest $request, $id)
    {
        $tag = Tag::findOrFail($id);
        $tag->tag_name = $request['edit-form-tag_name'];
        $tag->save();

        return new TagResource($tag);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Tag $tag
     * @return int
     * @throws \Exception
     */
    public function destroy(int $tag)
    {
        $_tag = Tag::find($tag);
        return $_tag->delete();
    }
}
