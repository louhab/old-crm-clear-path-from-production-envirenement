<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\LeadForm\LeadFormAddFormRequest;
use App\Http\Requests\LeadForm\LeadFormUpdateFormRequest;
use App\Http\Resources\LeadFormResource;
use App\Models\LeadForm;
use App\Models\LeadFormMarketingCampaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Yajra\DataTables\Facades\DataTables;

class LeadFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $leadformsQB = DB::table('lead_forms as lf')
            ->leftJoin('lead_form_marketing_campaigns as lm', 'lm.id', '=', 'lf.lead_form_marketing_campaign_id')
            ->where(function ($query) use ($request) {
                if (isset($request['campaign-type']) && !empty($request['campaign-type'])) {
                    $query->where('lf.lead_form_marketing_campaign_id', '=', $request['campaign-type']);
                }
            })
            ->select(['lf.id', 'lf.title', 'lf.redirect_to', 'lf.lang', 'lm.campaign_name', 'lm.id as campaign_id', 'lf.token', 'lf.is_students_form', "lf.student_program_id", "lf.is_multi_program", "lf.is_for_commercial", "lf.commercial_id"])->orderBy('lf.id', 'desc');

        $actions = [
            ["action" => "copy", "title" => "Copier Lien", "available" => 1, "className" => "btn btn-info leadsforms-table-copy", "icon" => "fas fa-copy"],
            ["action" => "edit", "title" => "Editer Formulaire", "available" => 1, "className" => "btn btn-primary leadsforms-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer Formulaire", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger leadsforms-table-delete", "icon" => "fas fa-trash"],
        ];

        // return DataTables()->eloquent($leadformsQB)
        //     ->addColumn('actions', $actions)
        //     ->toJson();

        return DataTables::of($leadformsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return LeadFormResource
     */
    public function store(LeadFormAddFormRequest $request)
    {

        $is_students_form = (isset($request['add-form-is-for']) && !empty($request['add-form-is-for']) && in_array($request['add-form-is-for'], ['1', '2'], true)) ? $request['add-form-is-for'] : null;

        $is_for_commercial = (isset($request['add-form-is-for-commercial']) && !empty($request['add-form-is-for-commercial']) && $request['add-form-is-for-commercial'] === "yes") ? 1 : 0;

        return new LeadFormResource(LeadForm::create([
            "user_id" => Auth::id(),
            "title" => $request['add-form-title'],
            "is_students_form" => $is_students_form,
            "student_program_id" => $is_students_form == 1 ? $request['add-form-student-program-id'] : null,
            "is_multi_program" => !$is_students_form ? $request['add-form-is-multiple-program'] : false,
            "lang" => $request['add-form-lang'],
            "redirect_to" => $request['add-form-redirect'],
            "is_for_commercial" => $is_for_commercial,
            "commercial_id" => $request['add-form-commercial'],
            "lead_form_marketing_campaign_id" => $request['add-form-campaign'],
            "token" => Str::random(60),
        ]));
    }
    /**
     * edit resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return LeadFormResource
     */
    public function update(LeadFormUpdateFormRequest $request, $id)
    {
        // dd($request->all());
        $form = LeadForm::findOrFail($id);
        if (isset($request['edit-form-redirect']) && !empty($request['edit-form-redirect']) && $request['edit-form-redirect'] == "on")
            $form->redirect_to = 1;
        else
            $form->redirect_to = 0;

        /**
         * for is_students_form column


        if (isset($request['edit-form-is-students-form']) && !empty($request['edit-form-is-students-form']) && $request['edit-form-is-students-form'] === "on")
            $form->is_students_form = 1;
        else
            $form->is_students_form = 0;
         *
         * */

        $is_for_commercial = (isset($request['edit-form-is-for-commercial']) && !empty($request['edit-form-is-for-commercial']) && $request['edit-form-is-for-commercial'] === "yes") ? 1 : 0;

        if (isset($request['edit-form-is-for']) && !empty($request['edit-form-is-for']) && in_array($request['edit-form-is-for'], ['1', '2'], true))
            $form->is_students_form = $request['edit-form-is-for'];
        else
            $form->is_students_form = null;


        if (isset($request['edit-form-lang']) && !empty($request['edit-form-lang']) && in_array($request['edit-form-lang'], ['fr', 'en']))
            $form->lang = $request['edit-form-lang'];
        else
            $form->lang = null;
        $form->title = $request['edit-form-title'];
        $form->lead_form_marketing_campaign_id = $request['edit-form-campaign'];
        $form->student_program_id = isset($request['edit-form-is-for']) && $request['edit-form-is-for'] == 1 ? $request['edit-form-student-program-id'] : null;
        $form->is_multi_program = isset($request['edit-form-is-for']) && !$request['edit-form-is-for'] ? $request['edit-form-is-multiple-program'] : false;


        $form->is_for_commercial = $is_for_commercial;
        $form->commercial_id = $request['edit-form-commercial'];

        $form->save();
        return new LeadFormResource($form);
    }

    /**
     * get influcers for specific lead form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LeadFormMarketingCampaign $campaign
     * @return json
     */
    public function getInfluencers(Request $request, LeadFormMarketingCampaign $campaign)
    {
        /*if( $campaign->campaign_name != "Influenceurs" && $campaign->campaign_name != "Représentant" ) {
            return response()->json(array());
        }*/
        $forms = LeadForm::where("lead_form_marketing_campaign_id", $campaign->id)->orderBy('title')->pluck('title', 'id')->toArray();

        return response()->json($forms);
    }

    public function destroy(LeadForm $leadform)
    {
        return $leadform->delete();
    }

    /**
     * Update active state.
     *
     * @param \Illuminate\Http\Request $request
     * @return LeadFormResource
     */
    public function setActiveState(Request $request)
    {
        $leadForm = LeadForm::findOrFail($request['id']);
        $leadForm->active = $request['state'];
        $leadForm->save();
        return new LeadFormResource($leadForm);
    }
}
