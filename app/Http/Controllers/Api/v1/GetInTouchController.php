<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\LeadForm\GetInTouchFormRequest;
use App\Mail\sendingEmail;
use App\Models\AssignmentLog;
use App\Models\Calendrier;
use App\Models\CurrentSelectedConseiller;
use App\Models\Customer;
use App\Models\Lead;
use App\Models\LeadForm;
use App\Models\LeadFormMarketingCampaign;
use App\Models\LeadStateLog;
use App\Models\Poles;
use App\Models\Program;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Rules\PhoneIndicator;
use Illuminate\Support\Facades\Session;

class GetInTouchController extends Controller
{

    public function store(Request $request) // GetInTouchForm
    {
        $request->merge(['phone' => str_replace('-', '', $request->input('phone'))]);
        // Detect
        // dd($request->all());
        if (!isset($request['select'])) $request['select'] = "office";
        $phone_rules = ['required', 'unique:leads'];
        if (isset($request['phone_error']) && !empty($request['phone_error'])) {
            //dd($request['phone_error']);
            array_push($phone_rules, new PhoneIndicator($request['phone_error']));
        }
        $rules = [
            // 'select' => 'required|in:office,conference',
            'program' => 'required',
            'firstname' => 'required|min:3|max:30',
            'lastname' => 'required|min:3|max:30',
            'email' => 'nullable|email|unique:leads',
            /*'birthday' => 'nullable|date',
            'program_field' => 'required',
            'city' => 'required',
            'country' => 'required',*/
            'phone' => $phone_rules,
            // 'file-input-cv' => 'nullable|file|mimes:pdf,doc,docx,ods',
            /*'date' => 'required_if:select,conference',
            'time' => 'required_if:select,conference'*/
        ];

        $this->validate($request, $rules);
        // dd($request["select"]);
        $formId = null;
        $form = null;
        if ($request['token']) {
            $form = LeadForm::where('token', '=', $request['token'])->first();
            $formId = $form->id;
            $c = $form->campaign()->first()->id;
        } else {
            if (LeadFormMarketingCampaign::where('campaign_name', 'like', '%website%')->count()) {
                $c = LeadFormMarketingCampaign::where('campaign_name', 'like', '%website%')->first()->id;
            } else {
                $c = 10;
            }
        }

        // $conseillers = User::where('role_id',Role::Conseiller)->where("status", 1)->orderby('id','asc')->get()->pluck('id')->toArray();

        // $current_conseiller = CurrentSelectedConseiller::first()->conseiller_id;
        // $index = array_search($current_conseiller,$conseillers);

        // if($index == count($conseillers) - 1 || $index === false)
        //     $next_conseiller = $conseillers[0];
        // else
        //     $next_conseiller =  $conseillers[$index+1];

        // CurrentSelectedConseiller::find(1)->update(['conseiller_id' => $next_conseiller]);


        //id:144 country maroc
        //id:75 country france
        // if (session()->get('locale') === 'en' && !str_starts_with($request['phone'], "+212")) {
        //     $id_lang_from_users_table = DB::table('users')->where('lang', 'en')->first();
        //     $next_conseiller = $id_lang_from_users_table->id;
        // } else {

        // 54 => CASA (1)
        // 72 => EL jadida (2)
        // 128 => Kech (3)
        // 157 => Rabat (4)

        $commercial_flag = false;
        $current_conseiller = null;
        $current_support = null;
        $lead_status_id = null;

        if ($form->is_for_commercial == 1) {
            $commercial_flag = true;
            $current_support = 53;
            $current_conseiller = $form->commercial_id;
            $lead_status_id = 5;
        }

        // dd(
        //     $commercial_flag,
        //     $current_support,
        //     $current_conseiller,
        //     $lead_status_id
        // );

        /**
         * set support id : 53 => Boom5
         * status => contract
         */



        if ($commercial_flag == false) {
            $pole_cities = [54, 72, 128, 157];
            if ($request['country_id'] == "144" && in_array($request['city2'], $pole_cities)) {
                $pole_id = array_search($request['city2'], $pole_cities) + 1;
                $conseillers_managers = User::whereIn('role_id', [Role::Conseiller, Role::Manager])->where('status', 1)->where("email", "not like", "%hrglobe%")->where("pole_id", $pole_id)->get()->pluck('id')->toArray();
                $conseillers = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->orderby('id', 'asc')->pluck('conseiller_id')->toArray();

                // dd($conseillers_managers);
                $current_conseiller = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("status", 1)->first();

                // check if there
                if ($current_conseiller) {
                    $current_conseiller = $current_conseiller->conseiller_id;
                    CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("status", 1)->first()->update(['status' => 0]);
                } else {
                    $current_conseiller = $conseillers[array_rand($conseillers)];
                }

                $index = array_search($current_conseiller, $conseillers);

                if ($index == count($conseillers) - 1 || $index === false) {
                    $next_conseiller = $conseillers[0];
                } else {
                    $next_conseiller = $conseillers[$index + 1];
                }

                CurrentSelectedConseiller::where("conseiller_id", $next_conseiller)->first()->update(['status' => 1]);

                // $all_advs = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("status", 1)->first();
                // $only_advs = CurrentSelectedConseiller::where("conseiller_id", $next_conseiller)->first();

                // dd("pole_id", $pole_id, "conseillers_managers", $conseillers_managers, "conseillers", $conseillers, "current_conseiller", $current_conseiller, "next_conseiller", $next_conseiller, "all_advs", $all_advs, "only_advs",  $only_advs);

            } else {
                $conseillers_managers = User::whereIn('role_id', [Role::Conseiller, Role::Manager])->where('status', 1)->where("email", "not like", "%hrglobe%")->get()->pluck('id')->toArray();
                $conseillers = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->orderby('id', 'asc')->pluck('conseiller_id')->toArray();
                $current_conseiller = $conseillers[array_rand($conseillers)];
            }
        }


        /**
         * Queue for the support
         */
        if ($commercial_flag == false) {
            $supports = User::where('role_id', Role::Support)->where('status', 1)->get()->pluck('id')->toArray();
            $Qsupports = CurrentSelectedConseiller::whereIn('conseiller_id', $supports)->where("queue", "<>", 0)->orderby('id', 'asc')->pluck('queue', 'conseiller_id')->toArray();
            $LQsupports = CurrentSelectedConseiller::whereIn('conseiller_id', $supports)->where("left_queue", "<>", 0)->orderby('id', 'asc')->pluck('left_queue', 'conseiller_id')->toArray();

            $flag = 0;
            $bis_flag = 0;
            if (!!$LQsupports) {
                $current_support = CurrentSelectedConseiller::whereIn('conseiller_id', array_keys($LQsupports))->where("status", 1);
                if ($current_support->count()) {
                    $flag = 1;
                    $current_support = $current_support->first()->conseiller_id;
                    $index = array_search($current_support, array_keys($LQsupports));

                    if ((count($LQsupports) - 1 !== 0 && $index == count(array_keys($LQsupports)) - 1) || ($index == count(array_keys($LQsupports)) - 1 && count(array_keys($LQsupports)) - 1 === 0 && array_values($LQsupports)[$index] - 1 > 0)) {
                        $next_support = array_keys($LQsupports)[0];
                    } elseif (count($LQsupports) - 1 !== 0 && count($LQsupports) - 1 >= $index && array_values($LQsupports)[$index + 1] - 1 >= 0) {
                        $next_support = array_keys($LQsupports)[$index + 1];
                    } else {
                        // restart the cycle
                        $bis_flag = 1;
                        $next_support = array_keys($Qsupports)[0];
                    }

                    $left_queue = array_values($LQsupports)[$index];
                    // substitute the left_query
                    if ($flag) {
                        CurrentSelectedConseiller::whereIn('conseiller_id', array_keys($LQsupports))->where("status", 1)->first()->update(['status' => 0]);
                        CurrentSelectedConseiller::where("conseiller_id", $current_support)->update(['left_queue' => $left_queue - 1]);
                        CurrentSelectedConseiller::where("conseiller_id", $next_support)->update(['status' => 1]);
                    }
                    if ($bis_flag) {
                        foreach ($Qsupports as $key => $value) {
                            CurrentSelectedConseiller::where('conseiller_id', $key)->update(['left_queue' => $value]);
                        }
                    }
                }
            }

            // initialise the cycle if there is no queue set
            if (!!$LQsupports === false || $flag === 0) {
                $no_flag = 0;
                $index = 0;
                // set up the current support as the first element of the array
                if ($flag === 0 && !!$LQsupports) {
                    $current_support = array_keys($LQsupports)[0];
                } elseif (!!$LQsupports === false || $no_flag) {
                    foreach ($Qsupports as $key => $value) {
                        CurrentSelectedConseiller::where('conseiller_id', $key)->update(['left_queue' => $value]);
                    }
                    $current_support = array_keys($Qsupports)[0];
                }

                if ((count($LQsupports) - 1 !== 0 && $index == count(array_keys($LQsupports)) - 1) || ($index == count(array_keys($LQsupports)) - 1 && count(array_keys($LQsupports)) - 1 === 0 && array_values($LQsupports)[$index] - 1 > 0)) {
                    $next_support = array_keys($LQsupports)[0];
                } elseif (count($LQsupports) - 1 !== 0 && count($LQsupports) - 1 >= $index && array_values($LQsupports)[$index + 1] - 1 >= 0) {
                    $next_support = array_keys($LQsupports)[$index + 1];
                } else {
                    // restart the cycle
                    $next_support = array_keys($Qsupports)[0];
                    $no_flag = 1;
                }

                $left_queue = array_values($LQsupports)[$index];
                CurrentSelectedConseiller::where("conseiller_id", $current_support)->update(['left_queue' => $left_queue - 1]);
                CurrentSelectedConseiller::where("conseiller_id", $next_support)->update(['status' => 1]);
            }
        }

        // test support id here

        if (session()->get('locale') === 'fr') {
            // dd($request['country']);
            $lead_lang = 'fr';
        } else {
            $lead_lang = 'en';
        }

        // Check code

        $select2 = isset($request['select2']) ? $request['select2'] : null;
        $select_bis = $request['select_bis'] ?? $request['program'];

        $lead = Lead::create([
            "program_id" => $select2 != "immigrant" ? $select_bis : $request['program'],
            "campaign_id" => $c,
            "to_office" => $request["select"],
            "firstname" => $request['firstname'],
            "lastname" => $request['lastname'],
            "email" => $request['email'],
            "influencer" => $formId,
            "program_field" => $request['program_field'],
            "lang" => $lead_lang,
            "phone" => $request['phone'],
            "country_id" => $request['country_id'],
            "city" => $request['city'],
            "city2" => $request['city2'],
            'conseiller_id' => $current_conseiller,
            'support_id' => $current_support,
            'is_form' => 1,
            "lead_status_id" => $lead_status_id
        ]);

        LeadStateLog::create([
            'user_id' => Auth::id(),
            'lead_id' => $lead->id,
            'state_id' => $lead->lead_status_id,
        ]);

        AssignmentLog::create([
            'user_id' => Auth::id(),
            'lead_id' => $lead->id,
            'conseiller_id' => $current_conseiller,
        ]);

        if ($lead && $request['select'] == 'conference') {
            Customer::create([
                "lead_id" => $lead->id,
                "program_id" => $lead->program_id,
                "program_step_id" => $lead->program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id,
                "firstname" => $lead->firstname,
                "lastname" => $lead->lastname,
                "campaign_id" => $lead->campaign_id,
                "lead_status" => $lead->lead_status_id,
                "customer_email" => $lead->email,
                "email" => $lead->email,
                "password" => Hash::make('123456'),
                "birthday" => $lead->birthday,
                "phone" => $lead->phone,
                "adresse_line_1" => $lead->adresse_line_1,
                "adresse_line_2" => $lead->adresse_line_2,
                "postal_code" => $lead->postal_code,
                "budget" => $lead->budget,
                "ranking_id" => $lead->ranking_id,
                "country_id" => $lead->country_id,
                "city" => $lead->city,
                "english_level_id" => $lead->english_level_id,
                "school_level_id" => $lead->school_level_id,
                "projected_diploma_type_id" => $lead->projected_diploma_type_id,
                "professional_domain_id" => $lead->professional_domain_id,
                "professional_experience" => $lead->professional_experience,
                "currency_id" => $lead_lang === "en" ? 2 : 1
            ]);

            /*Calendrier::create([
                "title" => "Rencontre 1 : " . $request['firstname'] . ' ' . $request['lastname'],
                "date" => $request['date'],
                "time" => explode(' - ', $request['time'])[0],
                "time_end" => explode(' - ', $request['time'])[1],
                'customer_id' => $customer->id,
                'user_id' => $next_conseiller,
                'description' => 'Video Conference',
            ]);*/
        }
        // $campaign = $form->campaign();
        // if ($campaign) {
        //     $lead->compaign_id = $compaign->id;
        // }
        /*if($request->hasFile('file-input-cv')) {
            $lead->addMediaFromRequest('file-input-cv')->toMediaCollection('CVs');
        }*/

        $program_name = $lead_lang === "fr" ? Program::find($lead->program_id)->name : Program::find($lead->program_id)->labelEng;
        $conseiller = User::find($lead->conseiller_id);

        // dd($conseiller, $lead->conseiller_id);
        $pole = null;
        if ($conseiller->pole_id) {
            $pole = Poles::find($conseiller->pole_id);
        }

        if ($lead->to_office == "office") {
            if ($lead_lang === 'en') {
                $object = $program_name . ' - First meeting';
                $message = "";
                // $message = '
                // Hello, Mr./Mrs.' . $request['firstname'] . ' ' . $request['lastname'] . ', I am '.$conseiller->name.', your '.($conseiller->gender === 'female' ? 'advisor,' : 'advisor,').'
                // It is our pleasure to welcome you to Clear Path Canada.
                // I would like to invite you to send me a message by email or Whatsapp, letting me know the
                // time that suits you better to set a meeting at our office.
                // <strong>Whatsapp number</strong>: '.$conseiller->phone.'
                // Address: 150, Ciutat de Granada St., 3rd floor, 08018, Barcelona
                // Location : https://goo.gl/maps/G8oDCYfYFyiuFjqf7
                // From Monday to Friday, from 9:00 am to 18:30 pm.
                // See you soon!
                // ' . $conseiller->name . '
                // Clear Path Canada advisor
                // Tel : ' . $conseiller->phone . '
                // Tel : (+34) 645 90 17 17
                // E-mail : ' . $conseiller->email . '';
            } else {
                $object = $program_name . ' - Première rencontre';
                $message = "";
                // Je suis '.$conseiller->name.' votre '.($conseiller->gender == 'female' ? 'conseillère':'conseiller').' chez Clear Path Canada. Je serai ravi de vous accueillir dans l’agence à l’heure qui vous convient.
                // A très bientôt !

                // Adresse : 42, rue Bachir Laalej, Casablanca - Maroc
                // Localisation : https://goo.gl/maps/nB5hMXDQKrorCoVX8
                // Heures d’ouverture : Lundi au vendredi de 9h30-19h30. Samedi de 10h-18h30

                // ' . $conseiller->name . '
                // Conseiller Clear Path Canada
                // Tel : ' . $conseiller->phone . '
                // Tel : +212702050909
                // E-mail : ' . $conseiller->email . '';
            }
        } else {
            if ($lead_lang === 'en') {
                $object = $program_name . ' -  First meeting by videoconference';
                $message = "";
                // I am ' . $conseiller->name . ' your '.($conseiller->gender == 'female' ? 'advisor':'advisor').'
                // It is our pleasure to welcome you to Clear Path Canada.
                // I would like to invite you to send me a message by email or WatsApp, letting me know the
                // time that suits you better to set a meeting by video-conference.
                // Whatsapp number:'.$conseiller->phone.'
                // Address: 150, Ciutat de Granada St., 3rd floor, 08018, Barcelona
                // Location : https://goo.gl/maps/G8oDCYfYFyiuFjqf7
                // From Monday to Friday, from 9:00 am to 18:30 pm.
                // See you soon!
                // ' . $conseiller->name . '
                // Clear Path Canada advisor
                // Tel : ' . $conseiller->phone . '
                // Tel : (+34) 645 90 17 17
                // E-mail : ' . $conseiller->email;
            } else {
                $object = $program_name . ' - Première rencontre par vidéoconférence';
                $message = "";
                // Je suis ' . $conseiller->name . ' votre '.($conseiller->gender == 'female' ? 'conseillère':'conseiller').' chez Clear Path Canada. Je vous invite à m\'envoyer l\'heure qui vous convient par e-mail ou message WhatsApp pour passer la rencontre par vidéo-conférence.
                // A très bientôt !

                // Email : ' . $conseiller->email . '
                // Numéro WhatsApp : ' . $conseiller->phone . '
                // Heures d’ouverture : Lundi au vendredi de 9h30-19h30. Samedi de 10h-18h30

                // ' . $conseiller->name . '
                // Conseiller Clear Path Canada
                // Tel : ' . $conseiller->phone . '
                // Tel : +212702050909
                // E-mail : ' . $conseiller->email . '
                // Adresse : 42, rue Bachir Laalej, Casablanca - Maroc
                // Localisation : https://goo.gl/maps/nB5hMXDQKrorCoVX8';
            }
        }

        // $object = 'Candidature Clear Path Canada';
        /*$message = '
        Nous vous remercions de nous avoir contactés. Veuillez nous confirmer votre numéro de téléphone et votre numéro de WhatsApp, un     conseiller spécialisé vous contactera dans les brefs délais.
        Nous vous invitons également à visiter nos locaux pour une rencontre qualifiée et enrichissante grâce à l’expertise de nos conseillers spécialisés.
        Adresse au Maroc : 42, rue Bachir Laalej, Casablanca, Maroc
        Adresse au Canada : 2000 McGill College Avenue, 6th Floor, Montréal, Québec, H3A 3H3
        Localisation : https://maps.google.com/?q=33.571827,-7.627207
        Nous vous remercions de l’attention portée à cette demande et nous vous prions d’accepter l’expression de nos sincères salutations.
        Cordialement,
        ' . User::find($next_conseiller)->name . '
        Conseiller Clear Path Canada
        ' . User::find($next_conseiller)->phone;*/

        $data = array(
            'name' => $request['firstname'] . ' ' . $request['lastname'],
            "elements" => [
                "lead_lang" => $lead_lang,
                "conseiller" => $conseiller,
                "pole" => $pole,
                "lead" => $lead,
                "email_phase" => 1,
            ],
            'object' => $object,
            'message' => $message
        );


        if ($request['email']) {
            try {
                Mail::to($request['email'])->send(new sendingEmail($data));
            } catch (\Swift_TransportException $e) {
                // echo 'Exception reçue : ',  $e->getMessage(), "\n";
            }
        }

        if (session()->get('locale') == 'en') {
            Session::put('status', 'Successfully applied for!');
        } else {
            Session::put('status', 'Candidature crée avec succés!');
        }
        if ($request['token']) {
            $form = LeadForm::where('token', '=', $request['token'])->first();
            if ($form->redirect_to == 0)
                return redirect()->back();
        }
        $done = "yes";
        return view('leads.welcome', ['complete' => $done])->with(['lead' => $lead]);
    }

    public function storeHRLead(Request $request)
    {
        try {
            $phone_rules = ['required', 'unique:leads'];
            if (isset($request['phone_error']) && !empty($request['phone_error'])) {
                array_push($phone_rules, new PhoneIndicator($request['phone_error']));
            }
            $rules = [
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'nullable|email|unique:leads',
                'phone' => $phone_rules,
            ];

            $this->validate($request, $rules);
            // Get hrGlobe Conseillers --- add imane to hrglobe advisors
            $conseillers_managers = User::whereIn('role_id', [Role::Conseiller, Role::Manager])->where('status', 1)->where(function ($query) {
                $query->where("email", "like", "%hrglobe%")
                    ->orWhere("email", "=", "i.benyoub@cpcanada.ma");
            })->get()->pluck('id')->toArray();
            $conseillers = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->orderby('id', 'asc')->pluck('conseiller_id')->toArray();

            $current_conseiller = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("hr_status", 1)->first();
            $index = $current_conseiller ? array_search($current_conseiller->conseiller_id, $conseillers) : false;

            if ($index == count($conseillers) - 1 || $index === false) {
                $next_conseiller = $conseillers[0];
            } else {
                $next_conseiller = $conseillers[$index + 1];
            }
            CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("hr_status", 1)->update(['hr_status' => 0]);
            CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("conseiller_id", $next_conseiller)->update(['hr_status' => 1]);

            $lead = Lead::create([
                "program_id" => 9,
                "email" => $request->input('email'),
                "firstname" => $request->input('firstname'),
                "lastname" => $request->input('lastname'),
                "phone" => $request->input('phone'),
                "campaign_id" => 4,
                'conseiller_id' => $next_conseiller,
            ]);

            Customer::create([
                "lead_id" => $lead->id,
                "program_id" => $lead->program_id,
                "firstname" => $lead->firstname,
                "lastname" => $lead->lastname,
                "campaign_id" => $lead->campaign_id,
                "lead_status" => $lead->lead_status_id,
                "customer_email" => $lead->email,
                "email" => $lead->email,
                "birthday" => $lead->birthday,
                "phone" => $lead->phone,
                "adresse_line_1" => $lead->adresse_line_1,
                "adresse_line_2" => $lead->adresse_line_2,
                "postal_code" => $lead->postal_code,
                "budget" => $lead->budget,
                "ranking_id" => $lead->ranking_id,
                "country_id" => $lead->country_id,
                "city" => $lead->city,
                "english_level_id" => $lead->english_level_id,
                "school_level_id" => $lead->school_level_id,
                "projected_diploma_type_id" => $lead->projected_diploma_type_id,
                "professional_domain_id" => $lead->professional_domain_id,
                "professional_experience" => $lead->professional_experience,
            ]);


            LeadStateLog::create([
                'user_id' => 61,
                'lead_id' => $lead->id,
                'state_id' => $lead->lead_status_id,
            ]);

            return response()->json([
                'message' => "lead has been created!",
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
