<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Lead;
use App\Models\LeadStateLog;
use App\Models\LeadStatus;
use Illuminate\Support\Facades\Auth;

class DownloadMediaController extends Controller
{
    public function enableQuote()
    {
        $doc = \App\Models\DocumentType::find(4);
        //$doc->
        $customer = \App\Models\Customer::where('id', Auth::id())->first();
        // $customer->is_client = 1;
        $customer->save();
        $lead = Lead::find($customer->lead_id)->first();
        $old_state = $lead->lead_status_id;
        $lead->lead_status_id = LeadStatus::Devis;
        $media = $customer->getFirstMedia($doc->collection_name);
        $media->setCustomProperty('devis', 'accepted');
        $media->save();
        $lead->save();
        if ($old_state != LeadStatus::Devis) {
            LeadStateLog::create([
                'user_id' => null,
                'lead_id' => $lead->id,
                'state_id' => $lead->lead_status_id,
            ]);
        }
        return back()->with(['status' => 'Client crée avec succés!']);
    }
}
