<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserAddFormRequest;
use App\Http\Requests\User\UserUpdateFormRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Rules\PhoneIndicator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $usersQB = User::with(['role', 'country'])
            ->where(function ($query) use ($request) {

                if (isset($request['role']) && !empty($request['role'])) {
                    $role = $request['role'];
                    $query->where('role_id', $role);
                }

                if (isset($request['name']) && !empty($request['name'])) {
                    $name = $request['name'];
                    $query->where('name', 'like', "%$name%");
                }

                if (isset($request['email']) && !empty($request['email'])) {
                    $email = $request['email'];
                    $query->where('email', 'like', "%$email%");
                }
            });

        $usersQB->whereNull("deleted_at");
        // dd($usersQB->count());
        $actions = [
            ["action" => "edit", "title" => "Modifier client", "available" => 1, "className" => "btn btn-primary user-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer client", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger user-table-delete", "icon" => "fas fa-trash"],
        ];


        return DataTables()->eloquent($usersQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return UserResource
     */
    public function store(Request $request)
    {
        $phone_rules = ['required'];
        if (isset($request['add-form-phone_error']) && !empty($request['add-form-phone_error'])) {
            //dd($request['phone_error']);
            array_push($phone_rules, new PhoneIndicator($request['add-form-phone_error']));
        }
        $rules = [
            'add-form-name' => 'required',
            'add-form-email' => 'required|email|unique:users,email',
            'add-form-phone' => $phone_rules,
            'add-form-gender' => 'required|in:male,female',
            'add-form-password' => 'required',
            'add-form-role' => 'required'
        ];
        $this->validate($request, $rules);

        if (Auth::user()->isAdmin() || Auth::user()->isVerificateur()) {
            if (isset($request['add-form-status']) && !empty($request['add-form-status']) && $request['add-form-status'] == 1) {
                $status = 1;
            } else {
                $status = 0;
            }
        } else {
            $status = 0;
        }
        return new UserResource(User::create([
            "name" => $request['add-form-name'],
            "email" => $request['add-form-email'],
            "phone" => $request['add-form-phone'],
            "gender" => $request['add-form-gender'],
            "role_id" => $request['add-form-role'],
            "password" => Hash::make($request['add-form-password']),
            "status" => $status
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateFormRequest $request
     * @param int $id
     * @return UserResource
     */
    public function update(Request $request, $id)
    {
        $phone_rules = ['required'];
        if (isset($request['phone_error']) && !empty($request['phone_error'])) {
            //dd($request['phone_error']);
            array_push($phone_rules, new PhoneIndicator($request['phone_error']));
        }
        $rules = [
            'edit-form-name' => 'required',
            'edit-form-email' => 'required|email',
            'phone' => $phone_rules,
            'edit-form-gender' => 'required|in:male,female',
            'edit-form-role' => 'required'
        ];

        $this->validate($request, $rules);


        $user = User::findOrFail($id);
        $user->name = $request['edit-form-name'];
        $user->email = $request['edit-form-email'];
        $user->phone = $request['phone'];
        $user->gender = $request['edit-form-gender'];
        $user->role_id = $request['edit-form-role'];
        $user->country_id = $request['edit-form-country'];
        $user->lang = $request['edit-form-lang'];
        if (isset($request['edit-form-password']) && !empty($request['edit-form-password']))
            $user->password = Hash::make($request['edit-form-password']);
        /*if(isset($request['edit-form-country']) && !empty($request['edit-form-country']))
            $user->country_id = $request['edit-form-country'];*/
        /*if(in_array($request['edit-form-lang'], ['fr', 'en']))
            $user->lang = $request['edit-form-lang'];*/

        if (Auth::user()->isAdmin() || Auth::user()->isVerificateur()) {
            if (isset($request['edit-form-status']) && !empty($request['edit-form-status']) && $request['edit-form-status'] == 1)
                $user->status = 1;
            else
                $user->status = 0;
        }
        $user->save();

        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return int
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        return $user->delete();
    }
}
