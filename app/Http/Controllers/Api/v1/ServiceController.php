<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Service\ServiceAddFormRequest;
use App\Http\Requests\Service\ServiceUpdateFormRequest;
use App\Http\Resources\ServiceCollection;
use App\Http\Resources\ServiceResource;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $phasesQB = Service::with(['Phase'])
            ->where(function ($query) use ($request) {
            });

        $actions = [
            ["action" => "edit", "title" => "Modifier service", "available" => 1, "className" => "btn btn-primary service-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer service", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger service-table-delete", "icon" => "fas fa-trash"],
        ];


        return DataTables()->eloquent($phasesQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Return a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ServiceCollection
     */
    public function getServices(Request $request)
    { //dd($request['pid']);
        $services = Service::with(['phase', 'details'])
            ->where('program_id', $request['pid'])
            ->where(function ($query) use ($request) {

                if (!isset($request['isquote'])) {
                    $query->where('billable', 1);
                }
            })->get();

        return new ServiceCollection($services);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ServiceResource
     */
    public function store(ServiceAddFormRequest $request)
    {
        return new ServiceResource(Service::create([
            "phase_id" => $request['add-form-phase_id'],
            "name" => $request['add-form-name'],
            "couple_price" => $request['add-form-couple_price'],
            "single_price" => $request['add-form-single_price']
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ServiceUpdateFormRequest $request
     * @param int $id
     * @return ServiceResource
     */
    public function update(ServiceUpdateFormRequest $request, $id)
    {
        $service = Service::findOrFail($id);
        // dd($service->couple_price, $service->getTranslations("couple_price"));
        $service->phase_id = $request['edit-form-phase_id'];
        $service->name = $request['edit-form-name'];
        $single_price = $service->getTranslations("single_price");
        $couple_price = $service->getTranslations("couple_price");
        foreach (["MAD", "EUR", "USD"] as $key) {
            if (!empty($single_price) && array_key_exists($key, $single_price)) {
                $single_price[$key] = $request["edit-form-single_price-" . strtolower($key)];
            }
            if (!empty($couple_price) && array_key_exists($key, $couple_price)) {
                $couple_price[$key] = $request["edit-form-couple_price-" . strtolower($key)];
            }
        }
        $service->single_price = $single_price;
        $service->couple_price = $couple_price;
        $service->save();

        return new ServiceResource($service);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Service $service
     * @return int
     * @throws \Exception
     */
    public function destroy(Service $service)
    {
        return $service->delete();
    }
}
