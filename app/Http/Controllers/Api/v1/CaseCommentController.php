<?php

namespace App\Http\Controllers\Api\v1;

use App\Events\LeadProspectionUpdated;
use App\Http\Controllers\Controller;
use App\Models\AdmissionUrl;
use App\Models\CaseComment;
use App\Models\CustomerCase;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;

class CaseCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, CustomerCase $customer_case)
    {
        if ($request->ajax()) {
            if (!$request['lid']) {
                $comments = $customer_case->comments()->orderByDesc('case_comment_id')->limit(5)->get();
            } else {
                $comments = $customer_case->comments()->where('case_comment_id', '<', $request['lid'])->orderByDesc('case_comment_id')->limit(5)->get();
            }

            return view('cases.steps.partials.comments-ajax', compact('comments'));
        } else abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, CustomerCase $customer_case, CaseComment $comment, $canal = null)
    {
        if (!$request['comment']) throw ValidationException::withMessages(['comment' => 'This value is incorrect']);

        $dataArray = [
            'user_id' => Auth::id(),
            'customer_case_id' => $customer_case->id,
            'program_step_id' => $customer_case->program_step_id,
            'comment' => $request['comment'],
        ];
        // $comment = CaseComment::create($dataArray);
        if (auth()->user()->isAdmin() || auth()->user()->isVerificateur() || $comment->user_id == auth()->id()) {
            $comment->comment = $request['comment'];
            $comment->save();
        }
        // $customer_case->comments()->attach($comment);
        return back()->with(['status' => 'Commentaire modifié avec succés!']);
    }
    public function store(Request $request, CustomerCase $customer_case, $canal = null)
    {
        if (!$request['comment']) throw ValidationException::withMessages(['comment' => 'This value is incorrect']);


        // dd($customer_case, $request['comment'], $canal);
        // count($customer_case->comments()->withPivot('admis')->get());

        $dataArray = [
            'user_id' => Auth::id(),
            'customer_case_id' => $customer_case->id,
            'program_step_id' => $customer_case->program_step_id,
            'comment' => $request['comment'],
        ];

        $comment = CaseComment::create($dataArray);
        $processed_message = null;
        if ($canal == 'soumission') {
            $customer_case->comments()->save($comment, ['canal' => 'soumission', 'view' => $request['view']]);
            // $processed_message = "note ajouté";
            // $customer_case->customer->update(array('soumission_processed_message' => 'note ajouté', 'soumission_processed_at' => Carbon::now()));
            // $customer_case->customer->update(array('processed_message' => 'note ajouté', 'processed_at' => Carbon::now()));
        } elseif (str_starts_with($canal, "admission-") || str_starts_with($canal, "course-")) {
            // soumission-view
            // case-edit
            if (($customer_case->comments()->wherePivot('canal', '=', $canal)->wherePivot('view', '=', 'case-edit')->count() == 0) && ($request['view'] == "case-edit")) {
                // dd("first comment ", $canal);
                // Check if there is an old payment

                // for admission
                // bug ❌ @todo fix
                // DB::table('program_payment_customer')->insert([
                //     'customer_id' => $customer_case->customer->id,
                //     'program_payment_id' => 7,
                //     'discount_percent' => 0,
                //     'amount' => 2000,
                //     'currency_id' => 1,
                //     'performed_at' => \Carbon\Carbon::now(),
                //     'payment_method' => 4,
                //     "conseiller_id" => $customer_case->customer->lead->conseiller_id
                // ]);

                // $customer_case->customer->payments()->sync([
                //     $payment->id => [
                //         'discount_percent' => 0,
                //         'amount' => $payment->pivot->amount,
                //         'currency_id' => $customer->currency_id,
                //         'performed_at' => \Carbon\Carbon::now(),
                //         'payment_method' => $method,
                //         "conseiller_id" => $customer->lead->conseiller_id
                //     ]
                // ], false);

                // unset admission status ❌
                // $customer_case->customer->update(["admission_state" => 1]);
            } elseif ($request['view'] == "soumission-view") {
                $customer_case->customer->update(["admission_state" => 6]);
            }
            if ($customer_case->customer->admission_state == 2) {
                $customer_case->customer->update(["admission_state" => 6]);
            }
            $customer_case->comments()->save($comment, ['canal' => $canal, 'view' => $request['view']]);
            $processed_message = "admission note ajouté(" . $canal . ")";
            // $customer_case->customer->update(array('soumission_processed_message' => 'admission note ajouté', 'soumission_processed_at' => Carbon::now()));
            // $customer_case->customer->update(array('processed_message' => 'admission note ajouté', 'processed_at' => Carbon::now()));
        } elseif (str_starts_with($canal, "soumission-")) {
            // soumission-admission-
            // Admisison + lien + 1 comment par back office = admisison ouverte
            // soumission-CAQ
            /*if (str_starts_with($canal, "soumission-admission-")) {
                if ($customer_case->customer->admission_state == 1) {
                    $admission = intval(str_replace("soumission-admission-", "", $canal));
                    $url = AdmissionUrl::where('admission_id', $admission)->where('customer_id', $customer_case->customer_id)->first();
                    if ($url && !empty($url->program))
                        $customer_case->customer->update(["admission_state" => 6]);
                }
                if ($customer_case->customer->admission_state == 2) {
                    $customer_case->customer->update(["admission_state" => 6]);
                }
            }*/
            // soumission-Demande de note
            $tasks = ["soumission-CAQ", "soumission-VISA PE", "soumission-WES", "soumission-PROFIL", "soumission-VISA"];
            $states = [[7, 8, 9], [12, 13, 14], [17, 18, 19], [22, 23, 24], [27, 28, 29]];
            $index = array_search($canal, $tasks);
            if ($index !== false) {
                if (($customer_case->comments()->wherePivot('canal', '=', $canal)->wherePivot('view', '=', 'case-edit')->count() == 0) && ($request['view'] == "case-edit")) {
                    $customer_case->customer->update(["admission_state" => $states[$index][0]]);


                    // pour CAQ && visa
                    // $customer_case->customer->payments()->sync([
                    //     $payment->id => [
                    //         'discount_percent' => 0,
                    //         'amount' => $payment->pivot->amount,
                    //         'currency_id' => $customer->currency_id,
                    //         'performed_at' => \Carbon\Carbon::now(),
                    //         'payment_method' => $method,
                    //         "conseiller_id" => $customer->lead->conseiller_id
                    //     ]
                    // ], false);
                    // FIXME ❌
                    // if ($canal == "soumission-VISA PE" || $canal == "soumission-CAQ") {
                    //     DB::table('program_payment_customer')->insert([
                    //         'customer_id' => $customer_case->customer->id,
                    //         'program_payment_id' => $canal == "soumission-VISA PE" ? 10 : 9,
                    //         'discount_percent' => 0,
                    //         'amount' => $canal == "soumission-VISA PE" ? 1900 : 1000,
                    //         'currency_id' => 1,
                    //         'performed_at' => \Carbon\Carbon::now(),
                    //         'payment_method' => 4,
                    //         "conseiller_id" => $customer_case->customer->lead->conseiller_id
                    //     ]);
                    // }

                    // 'customer_id' => $customer_case->customer->id,
                    // 'program_payment_id' => 7,
                    // 'discount_percent' => 0,
                    // 'amount' => 2000,
                    // 'currency_id' => 1,
                    // 'performed_at' => \Carbon\Carbon::now(),
                    // 'payment_method' => 4,
                    // "conseiller_id" => $customer_case->customer->lead->conseiller_id


                } elseif ($request['view'] == "soumission-view") {
                    $customer_case->customer->update(["admission_state" => $states[$index][1]]);
                }
                if ($customer_case->customer->admission_state == $states[$index][2]) {
                    $customer_case->customer->update(["admission_state" => $states[$index][1]]);
                }
            }
            if (str_starts_with($canal, "soumission-admission-")) {
                if (($customer_case->comments()->wherePivot('canal', '=', $canal)->wherePivot('view', '=', 'case-edit')->count() == 0) && ($request['view'] == "case-edit")) {
                    // unset admission status ❌
                    // $customer_case->customer->update(["admission_state" => 1]);
                } elseif ($request['view'] == "soumission-view") {
                    $customer_case->customer->update(["admission_state" => 6]);
                }
                if ($customer_case->customer->admission_state == 2) {
                    $customer_case->customer->update(["admission_state" => 6]);
                }
            }
            $tasks = ["soumission-Prise en charge", "soumission-Imm5645", "soumission-Imm5476", "soumission-CPG", "soumission-Facture scolaire"];
            if (in_array($canal, $tasks)) {
                if (($customer_case->comments()->wherePivot('canal', '=', $canal)->wherePivot('view', '=', 'case-edit')->count() == 0) && ($request['view'] == "case-edit")) {
                    $customer_case->customer->update(["admission_state" => 12]);

                    // bug ❌ @todo fix
                    // pour CPG && Facture
                    // if ($canal == "soumission-CPG" || $canal == "soumission-Facture scolaire") {
                    //     DB::table('program_payment_customer')->insert([
                    //         'customer_id' => $customer_case->customer->id,
                    //         'program_payment_id' => $canal == "soumission-CPG" ? 6 : 8,
                    //         'discount_percent' => 0,
                    //         'amount' => 1000,
                    //         'currency_id' => 1,
                    //         'performed_at' => \Carbon\Carbon::now(),
                    //         'payment_method' => 4,
                    //         "conseiller_id" => $customer_case->customer->lead->conseiller_id
                    //     ]);
                    // }
                } elseif ($request['view'] == "soumission-view") {
                    $customer_case->customer->update(["admission_state" => 13]);
                }
                if ($customer_case->customer->admission_state == 14) {
                    $customer_case->customer->update(["admission_state" => 13]);
                }
            }
            if ($canal == "soumission-Demande de note" && (Auth::user()->isAdmin() || Auth::user()->isVerificateur() || Auth::user()->isManager() || Auth::user()->isConseiller())) {
                if ($customer_case->customer->is_client == 0)
                    // unset admission status ❌
                    // $customer_case->customer->update(["is_client" => 1, "admission_state" => 1]);
                    $customer_case->customer->update(["is_client" => 1]);
                // elseif (is_null($customer_case->customer->admission_state))
                // unset admission status ❌
                // $customer_case->customer->update(["admission_state" => 1]);
            }
            $customer_case->comments()->save($comment, ['canal' => $canal, 'view' => $request['view']]);
            $processed_message = 'note ' . $canal . ' ajouté';
            // $customer_case->customer->update(array('soumission_processed_message' => 'note '.$canal.' ajouté', 'soumission_processed_at' => Carbon::now()));
            // $customer_case->customer->update(array('processed_message' => 'note '.$canal.' ajouté', 'processed_at' => Carbon::now()));
        } else {
            // $customer_case->comments()->attach($comment);
            $customer_case->comments()->save($comment, ['view' => $request['view']]);
            // $processed_message = null;
        }
        if (!is_null($processed_message)) {
            if ($request['view'] == "soumission-view")
                $customer_case->customer->update(array('soumission_processed_message' => $processed_message, 'soumission_processed_at' => Carbon::now()));
            if ($request['view'] == "case-edit")
                $customer_case->customer->update(array('processed_message' => $processed_message, 'processed_at' => Carbon::now()));
        }

        /*if (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'lead-view') {
            LeadProspectionUpdated::dispatch($customer_case->customer->lead, 'Commentaire ajouté');
        }
        if (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'case-edit') {
            CustomerActionsUpdated::dispatch($customer_case->customer, 'Commentaire ajouté');
        }*/

        return back()->with(['status' => 'Commentaire ajouté avec succés!']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateFees(Request $request)
    {
        // form-add-admision-fees-rate=9821321.12321 &add-form-fees-comment-id=15
        if (!isset($request['form-add-admision-fees-rate'])) throw ValidationException::withMessages(['comment' => 'This value is incorrect']);

        $comment = CaseComment::findOrFail($request['add-form-fees-comment-id']);
        $comment->fees = $request['form-add-admision-fees-rate'];
        $comment->save();

        return back()->with(['status' => 'Commentaire modifié avec succés!']);
    }
}
