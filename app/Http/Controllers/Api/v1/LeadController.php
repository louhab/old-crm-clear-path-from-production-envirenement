<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Lead\LeadUpdateFormRequest;
use App\Http\Requests\Lead\LeadStoreFormRequest;
use App\Http\Resources\CustomerResource;
use App\Http\Resources\LeadResource;
use App\Mail\sendingEmail;
use App\Models\AssignmentLog;
use App\Models\CurrentSelectedConseiller;
use App\Models\Customer;
use App\Models\CustomerCase;
use App\Models\HistoCall;
use App\Models\Lead;
use App\Models\LeadForm;
use App\Models\LeadFormMarketingCampaign;
use App\Models\LeadStateLog;
use App\Models\LeadStatus;
use App\Models\Program;
use App\Models\ProgramStep;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Cities;

use \App\Models\DocumentType;
use App\Models\Poles;
use Illuminate\Validation\Rule;

use App\Rules\PhoneIndicator;

class LeadController extends Controller
{
    private $is_search_flag;
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $id = null)
    {
        // $city= Cities::find($request['city2']) ;
        $subQuery = DB::table('case_states')
            ->select('id')
            ->whereIn('sub_step_state_id', [0, 3, 5, 10, 20, 29])
            ->orderByDesc('sub_step_state_id');

        $leadsQB = DB::table('leads as l')
            ->join('programs as p', 'l.program_id', '=', 'p.id')
            ->leftJoin('lead_form_marketing_campaigns as lf', 'lf.id', '=', 'l.campaign_id')
            ->leftJoin('lead_forms as lff', 'lff.id', '=', 'l.influencer')
            ->leftJoin('countries as c', 'l.country_id', '=', 'c.id')
            ->leftJoin('users as u', 'l.conseiller_id', '=', 'u.id')
            ->leftJoin('users as sup', 'l.support_id', '=', 'sup.id')
            ->leftJoin('lead_statuses as ls', 'l.lead_status_id', '=', 'ls.id')
            ->leftJoin('leads_tags as lt', 'l.id', '=', 'lt.lead_id')
            ->leftJoin('tags as t', 't.id', '=', 'lt.tag_id')
            ->leftJoin('cities', 'cities.id', '=', 'l.city2')
            ->leftJoin('customers as cr', function ($join) {
                $join->on('l.id', '=', 'cr.lead_id')
                    ->leftJoin('customer_cases as cc', 'cr.id', '=', 'cc.customer_id');
            })
            ->leftJoin('case_states as css', function ($join) {
                $join->on('cc.id', '=', 'css.customer_case_id')
                    ->whereIn('css.sub_step_state_id', [3])
                    ->where('css.status', 1)
                    ->whereNotNull("css.payment_method")
                    ->where('css.id', '=', function ($query) {
                        $query->select('css2.id')
                            ->from('case_states as css2')
                            ->whereColumn('cc.id', 'css2.customer_case_id')
                            ->whereIn('css2.sub_step_state_id', [3])
                            ->where('css2.status', 1)
                            ->whereNotNull("css.payment_method")
                            ->orderBy('css2.sub_step_state_id', 'desc')
                            ->limit(1);
                    });
            })
            ->leftJoin(DB::raw('(SELECT `lead_id`, COUNT(`lead_id`) AS `calls`, `lead_state_id` FROM `histo_calls`, `leads` WHERE `leads`.`id`=`histo_calls`.`lead_id` AND `lead_status_id`=`lead_state_id` GROUP BY `lead_id`) hc'), function ($join) {
                $join->on('hc.lead_id', '=', 'l.id');
            })
            ->leftJoin('program_payment_customer as pay', 'pay.customer_id', '=', 'cr.id')

            ->where(function ($query) use ($request, $id) {
                if (isset($request['lead']) && !empty($request['lead'])) {

                    $query->where(function ($query) use ($request) {

                        // $query->where(function ($query) use ($request) {
                        //     $query->where('l.firstname', 'LIKE', "%" . $request['lead'] . "%");
                        // });

                        // $query->orWhere(function ($query) use ($request) {
                        //     $query->where('l.lastname', 'LIKE', "%" . $request['lead'] . "%");
                        // });
                        // $query->orWhere(function ($query) use ($request) {
                        //     $query->where(DB::raw("CONCAT(`l`.`firstname`, ' ', `l`.`lastname`)"), 'LIKE', "%" . $request['lead'] . "%");
                        // });
                        // $query->orWhere(function ($query) use ($request) {
                        //     $query->where(DB::raw("CONCAT(`l`.`lastname`, ' ', `l`.`firstname`)"), 'LIKE', "%" . $request['lead'] . "%");
                        // });
                        $query->orWhere(function ($query) use ($request) {
                            $query->where(DB::raw("CONCAT(`l`.`firstname`, ' ', `l`.`lastname`)"),  $request['lead']);
                        });
                        $query->orWhere(function ($query) use ($request) {
                            $query->where(DB::raw("CONCAT(`l`.`lastname`, ' ', `l`.`firstname`)"),  $request['lead']);
                        });
                    });
                    $lead_sub_query = $query;
                    $this->is_search_flag = $lead_sub_query->count() === 1;
                }

                if (isset($request['program']) && !empty($request['program'])) {
                    $query->whereIn('l.program_id', $request['program']);
                }

                // Domaine d’activité
                if (isset($request['field-activity']) && !empty($request['field-activity'])) {
                    $query->whereIn('l.program_field', $request['field-activity']);
                }

                if (isset($request['phone']) && !empty($request['phone'])) {
                    $query->where('l.phone',  $request['phone']);
                    $phone_sub_query = $query;
                    $this->is_search_flag = $phone_sub_query->count() === 1;
                }
                if (isset($request['email']) && !empty($request['email'])) {
                    $query->where('l.email',  trim($request['email']));
                    $email_sub_query = $query;
                    $this->is_search_flag = $email_sub_query->count() === 1;
                }
                if (isset($request['status']) && !empty($request['status'])) {
                    $query->where(function ($query) use ($request) {
                        $key = array_search('n/d', $request['status']);
                        if ($key !== false) {
                            $query->whereNull('l.lead_status_id');
                            if (count($request['status']) > 1) {
                                $query->orWhereIn('l.lead_status_id', array_diff($request['status'], ["n/d"]));
                            }
                        } else {
                            $query->whereIn('l.lead_status_id', $request['status']);
                        }
                    });
                }

                if (isset($request["call_by_support"]) && !empty($request["call_by_support"])) {
                    $lsids = array_unique(HistoCall::whereIn("user_id", User::whereIn("role_id", [7])->pluck("id")->toArray())->pluck("lead_id")->toArray());
                    if ($request["call_by_support"])
                        $query->whereIn('l.id', $lsids);
                    else
                        $query->whereNotIn('l.id', $lsids);
                }
                if (isset($request["call_by_conseiller"]) && !empty($request["call_by_conseiller"])) {
                    $lcids = array_unique(HistoCall::whereIn("user_id", User::whereIn("role_id", [3])->pluck("id")->toArray())->pluck("lead_id")->toArray());
                    if ($request["call_by_conseiller"])
                        $query->whereIn('l.id', $lcids);
                    else
                        $query->whereNotIn('l.id', $lcids);
                }
                // with payment filter
                if (isset($request["with_payment"])) {
                    $query->where(function ($query) use ($request) {
                        if ($request["with_payment"]) {
                            $query->whereRaw('cc.program_step_id > 1');
                        } elseif ($request["with_payment"] === "0") {
                            $query->whereRaw('cc.program_step_id = 1');
                        }
                    });
                }

                // with R1 filter (payment as the filter :) -)
                if (isset($request["with_R1"])) {
                    $query->where(function ($query) use ($request) {
                        if ($request["with_R1"]) {
                            $query->whereRaw('cc.program_step_id > 1');
                        } elseif ($request["with_R1"] === "0") {
                            $query->whereRaw('cc.program_step_id = 1');
                        }
                    });
                }

                if (isset($request['city2'])) {
                    $cityModel = Cities::find($request["city2"]);

                    if ($cityModel) {
                        $city = $cityModel->city;

                        $query->where(function ($query) use ($city) {
                            $query->where('l.city', 'LIKE', "%" . $city . "%");
                        });
                    }
                }

                if (isset($request["payment_proof"])) {
                    if ($request["payment_proof"]) { {
                            $query->whereIn("cr.id", function ($sub) {
                                return $sub->fromSub(function ($subQuery) {
                                    $subQuery->select("c.id")
                                        ->leftJoin('program_payment_customer as ppc', 'ppc.customer_id', '=', 'c.id')
                                        ->from('customers as c')
                                        ->whereNotNull("ppc.status")
                                        ->orderBy("ppc.status", "desc");
                                }, 'sq');
                            });
                        }
                    } elseif ($request["payment_proof"] === "0") {
                        $query->whereIn("cr.id", function ($sub) {
                            return $sub->fromSub(function ($subQuery) {
                                $subQuery->select("c.id")
                                    ->leftJoin('program_payment_customer as ppc', 'ppc.customer_id', '=', 'c.id')
                                    ->from('customers as c')
                                    ->whereNull("ppc.status");
                            }, 'sq');
                        });
                    }
                }

                if (isset($request['processed']) && !empty($request['processed'])) {
                    $key = array_search('unprocessed', $request['processed']);
                    if ($key !== false) {
                        $query->whereNull('l.processed_at');
                    } else {
                        $query->whereNotNull('l.processed_at');
                    }
                }
                if (isset($request['conseiller']) && !empty($request['conseiller'])) {
                    if ($request['conseiller'] == "n/d") {
                        $query->whereNull('l.conseiller_id');
                    } else {
                        $query->whereIn('l.conseiller_id', $request['conseiller']);
                    }
                }

                if (isset($request['support']) && !empty($request['support'])) {
                    $key = array_search('n/d', $request['support']);
                    if ($key !== false) {
                        $query->whereNull('l.support_id');
                        if (count($request['support']) > 1) {
                            $query->orWhereIn('l.support_id', $request['support']);
                        }
                    } else {
                        $query->whereIn('l.support_id', $request['support']);
                    }
                }

                if (isset($request['calls-count']) && !empty($request['calls-count'])) {
                    $query->where(function ($query) use ($request) {
                        $key = array_search('0', $request['calls-count']);
                        $callCounts = $request['calls-count'];
                        if ($key !== false) {
                            // dd($callCounts);
                            unset($callCounts[$key]);
                            $query->whereNull('hc.calls');
                            $query->orWhereIn('hc.calls', $callCounts);
                        } else {
                            $query->whereIn('hc.calls', $callCounts);
                        }
                    });
                }

                if (isset($request['tags']) && !empty($request['tags'])) {
                    $query->whereIn('lt.tag_id', $request['tags']);
                }

                if ($id) {
                    $query->where('lt.tag_id', $id);
                }

                if (isset($request['campaign']) && !empty($request['campaign'])) {
                    $query->whereIn('l.campaign_id', $request['campaign']);
                }
                // influencer
                if (isset($request['influencer']) && !empty($request['influencer'])) {
                    // dd("influencer", $request['influencer']);
                    $query->whereIn('l.influencer', $request['influencer']);
                }
                // source
                if (isset($request['source']) && !empty($request['source'])) {
                    // dd("source", $request['source']);
                    $query->whereIn('l.influencer', $request['source']);
                }
                // if(isset($request['recycled']) && !empty($request['recycled'])) {
                //     // dd($request['recycled']);
                //     if ($request['recycled'] == 'recycled') {
                //         $query->where('l.recycled', 1);
                //     } else {
                //         $query->where('l.recycled', 0);
                //     }
                // }
                if (isset($request['date_start']) && !empty($request['date_start'])) {
                    $col = 'l.created_at';
                    if (isset($request['status']) && !empty($request['status']) && (count($request['status']) >= 1)) {
                        $col = 'l.status_changed_at';
                    }
                    if (isset($request['status']) && !empty($request['status']) && (count($request['status']) >= 1 && in_array('n/d', $request['status']))) {
                        $col = 'l.created_at';
                    }
                    // $col='l.status_changed_at';
                    $start = Carbon::parse($request['date_start']);
                    // $query->whereNotNull('al.created_at');
                    $query->where($col, '>=', $start->format('Y-m-d') . ' 00:00:00');
                }
                /*else {
                    $start = Carbon::now()->firstOfMonth();
                    $query->where('l.created_at', '>=', $start->format('Y-m-d').' 00:00:00');
                }*/
                if (isset($request['date_end']) && !empty($request['date_end'])) {
                    $col = 'l.created_at';
                    if (isset($request['status']) && !empty($request['status']) && (count($request['status']) >= 1)) {
                        $col = 'l.status_changed_at';
                    }
                    if (isset($request['status']) && !empty($request['status']) && (count($request['status']) >= 1 && in_array('n/d', $request['status']))) {
                        $col = 'l.created_at';
                    }
                    // $col='l.status_changed_at';
                    $end = Carbon::parse($request['date_end'])->addDay();
                    // $query->whereNotNull('al.created_at');
                    $query->where($col, '<=', $end->format('Y-m-d') . ' 00:00:00');
                }
                /*else {
                    $end   = Carbon::now()->lastOfMonth();
                    $query->where('l.created_at', '<=', $end->format('Y-m-d').' 00:00:00');
                }*/
                // office
                if (isset($request['office']) && !empty($request['office'])) {
                    if ($request['office'] == "n/d") {
                        $query->whereNull('l.to_office');
                    } else {
                        $query->whereIn('l.to_office', $request['office']);
                    }
                }
            })->distinct()
            ->select(['l.id as lead_id', 'l.recycled', 'l.firstname', 'l.lastname', 'l.email', 'l.to_office', 'l.phone', 'p.name as pname', 'p.id as program_id', 'lf.campaign_name as canal', 'lff.title as influencer', 'u.name as conseiller', 'sup.name as support', 'cc.id as case_id', 'l.processed_at as processed', 'l.status_changed_at', 'l.processed_message', 'l.created_at as created', 'ls.status_label_fr as status_label', 'ls.id as lead_status_id', 'hc.calls', 'l.reference', "cities.city as city2", "l.city as city", "l.country_id as country", "css.sub_step_state_id as sub_step_state_id", "css.updated_at as meet_date"]);

        // for showing auto assignment btn
        if (Auth::user()->isConseiller()) {
            if (!$this->is_search_flag) {
                $leadsQB = $leadsQB->where('l.conseiller_id', Auth::id());
            }
            if ((!isset($request['lead']) || empty($request['lead'])) && (!isset($request['phone']) || empty($request['phone']))) {
                $leadsQB = $leadsQB->whereNotNull('l.lead_status_id');
                $leadsQB = $leadsQB->whereNotIn('l.lead_status_id', [1, 2, 3, 4, 6, 14, 15]);
            }
        }

        if (Auth::user()->isAgent()) {
            if ((!isset($request['lead']) || empty($request['lead'])) && (!isset($request['phone']) || empty($request['phone']))) {
                $leadsQB = $leadsQB->whereNotNull('l.lead_status_id');
                $leadsQB = $leadsQB->whereNotIn('l.lead_status_id', [1, 2, 3, 6, 5, 11, 13, 14, 15]);
            }
        }

        if (Auth::user()->isSupport()) {
            if (!$this->is_search_flag) {
                $leadsQB = $leadsQB->where('l.support_id', Auth::id());
            }
            if ((!isset($request['lead']) || empty($request['lead'])) && (!isset($request['phone']) || empty($request['phone']))) {
                $leadsQB = $leadsQB->where(function ($query) use ($request, $id) {
                    $query->where('l.lead_status_id', 3);
                    $query->orWhereNull('l.lead_status_id');
                });
            }
        }

        if (Auth::user()->isManager() && Auth::id() != 31) {
            $leadsQB = $leadsQB
                ->where(function ($query) use ($request) {
                    $query->where(function ($query) use ($request) {
                        $query->where('l.conseiller_id', Auth::id());
                        if (isset($request['conseiller']) && !empty($request['conseiller'])) {
                            $query->orWhere('l.conseiller_id', '=', $request['conseiller']);
                        }
                    });

                    $query->orWhere(function ($query) {
                        $query->where('u.role_id', 3);
                    });
                });
            // Edit restrictions
            if ((!isset($request['lead']) || empty($request['lead'])) && (!isset($request['phone']) || empty($request['phone']))) {
                $leadsQB = $leadsQB->whereNotNull('l.lead_status_id');
                $leadsQB = $leadsQB->whereNotIn('l.lead_status_id', [1, 2, 3, 4, 6, 14]);
            }
        }

        // hide clients from list leads
        $leadsQB->where(function ($query) {
            $cls = Customer::where('is_client', 1)->get()->pluck('lead_id')->toArray();
            $query->whereNotIn('l.id', $cls);
        });

        // set order
        // $leadsQB->orderBy('l.id', 'desc')->orderBy('l.created_at', 'desc');
        $column_idx = $request->input('order')[0]["column"];
        $direction = $request->input('order')[0]["dir"];
        $column = $request->input('columns')[$column_idx];
        // dd($request->input('order'), $column);
        $leadsQB->orderBy($column["name"], $direction);

        $actions = [
            ["action" => "call", "title" => "faire appel", "available" => 1, "edit" => 0, "className" => "btn btn-success btn-sm", "icon" => "fas fa-phone"],
            ["action" => "convert", "islink" => false, "title" => "convertir", "available" => (Auth::user()->isAdmin() || Auth::user()->isVerificateur() || Auth::user()->isManager()) ? 1 : 0, "className" => "btn btn-sm btn-light lead-convert", "icon" => "far fa-copyright"],
            ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light lead-show", "icon" => "fas fa-eye"],
            ["action" => "delete", "islink" => false, "title" => "supprimer", "available" => (Auth::user()->isAdmin() || Auth::user()->isVerificateur() || Auth::user()->isManager()) ? 1 : 0, "className" => "btn btn-sm btn-light lead-delete", "icon" => "fas fa-trash"],
            $this->is_search_flag ? ["action" => "assignment", "islink" => false, "title" => "Auto Affect", "available" => (Auth::user()->isSupport() || Auth::user()->isConseiller()) ? 1 : 0, "className" => "btn btn-sm btn-light lead-assignment", "icon" => "fas fa-user-plus"] : "",
            ["action" => "copy", "islink" => false, "title" => "Copier le lien", "available" => Auth::user()->isAgent() ? 1 : 0, "className" => "btn btn-sm btn-light lead-copy", "icon" => "fas fa-copy"],
        ];
        return DataTables::of($leadsQB)
            ->addColumn('cv_id', function ($lead) {
                return Lead::find($lead->lead_id)->cv_id;
            })
            ->addColumn('is_agent', Auth::user()->isAgent())
            ->addColumn('actions', $actions)
            /*->with('calls', function() use ($leadsQB) {
                return $leadsQB->sum('h.duration');
            })*/
            ->toJson();
    }

    public function log(Request $request, $id = null)
    {
        $logsQB = DB::table('activity_log as al')
            ->leftJoin('leads as l', 'l.id', '=', 'al.subject_id')
            ->join('programs as p', 'l.program_id', '=', 'p.id')
            ->leftJoin('lead_form_marketing_campaigns as lf', 'lf.id', '=', 'l.campaign_id')
            ->leftJoin('lead_forms as lff', 'lff.id', '=', 'l.influencer')
            ->leftJoin('countries as c', 'l.country_id', '=', 'c.id')
            ->leftJoin('users as u', 'l.conseiller_id', '=', 'u.id')
            ->leftJoin('users as sup', 'l.support_id', '=', 'sup.id')
            ->leftJoin('users as causer', 'al.causer_id', '=', 'causer.id')
            ->leftJoin('lead_statuses as ls', 'al.properties->status', '=', 'ls.id')
            ->leftJoin('leads_tags as lt', 'l.id', '=', 'lt.lead_id')
            ->leftJoin('tags as t', 't.id', '=', 'lt.tag_id')
            ->leftJoin('customers as cr', function ($join) {
                $join->on('l.id', '=', 'cr.lead_id')
                    ->leftJoin('customer_cases as cc', 'cr.id', '=', 'cc.customer_id');
            })
            ->where('log_name', 'lead-status-log')
            ->leftJoin(DB::raw('(SELECT `lead_id`, COUNT(`lead_id`) AS `calls` FROM `histo_calls` GROUP BY `lead_id`) hc'), 'hc.lead_id', '=', 'l.id')
            ->where(function ($query) use ($request, $id) {

                if (isset($request['lead']) && !empty($request['lead'])) {

                    $query->where(function ($query) use ($request) {

                        $query->where(function ($query) use ($request) {
                            $query->where('l.firstname', 'LIKE', "%" . $request['lead'] . "%");
                        });

                        $query->orWhere(function ($query) use ($request) {
                            $query->where('l.lastname', 'LIKE', "%" . $request['lead'] . "%");
                        });
                        $query->orWhere(function ($query) use ($request) {
                            $query->where(DB::raw("CONCAT(`l`.`firstname`, ' ', `l`.`lastname`)"), 'LIKE', "%" . $request['lead'] . "%");
                        });
                        $query->orWhere(function ($query) use ($request) {
                            $query->where(DB::raw("CONCAT(`l`.`lastname`, ' ', `l`.`firstname`)"), 'LIKE', "%" . $request['lead'] . "%");
                        });
                    });
                    /*$query->where('l.firstname', 'LIKE', "%".$request['lead']."%");
                    $query->orWhere('l.lastname', 'LIKE', "%".$request['lead']."%");
                    $query->orWhere(DB::raw("CONCAT(`l`.`firstname`, ' ', `l`.`lastname`)"), 'LIKE', "%".$request['lead']."%");
                    $query->orWhere(DB::raw("CONCAT(`l`.`lastname`, ' ', `l`.`firstname`)"), 'LIKE', "%".$request['lead']."%");*/
                }

                if (isset($request['program']) && !empty($request['program'])) {
                    $query->where('l.program_id', '=', $request['program']);
                }

                if (isset($request['phone']) && !empty($request['phone'])) {
                    $query->where('l.phone', 'like', '%' . $request['phone'] . '%');
                }
                if (isset($request['email']) && !empty($request['email'])) {
                    $query->where('l.email', 'like', '%' . $request['email'] . '%');
                }
                if (isset($request['status']) && !empty($request['status'])) {
                    if ($request['status'] == "n/d") {
                        $query->whereNull('al.properties->status');
                    } else {
                        $query->where('al.properties->status', '=', $request['status']);
                    }
                }
                if (isset($request['conseiller']) && !empty($request['conseiller'])) {
                    if ($request['conseiller'] == "n/d") {
                        $query->whereNull('l.conseiller_id');
                    } else {
                        $query->where('l.conseiller_id', '=', $request['conseiller']);
                    }
                }

                if (isset($request['support']) && !empty($request['support'])) {
                    if ($request['support'] == "n/d") {
                        $query->whereNull('l.support_id');
                    } else {
                        $query->where('l.support_id', '=', $request['support']);
                    }
                }

                if (isset($request['tags']) && !empty($request['tags'])) {
                    $query->whereIn('lt.tag_id', $request['tags']);
                }

                if ($id) {
                    $query->where('lt.tag_id', $id);
                }

                if (isset($request['campaign']) && !empty($request['campaign'])) {
                    $query->where('l.campaign_id', '=', $request['campaign']);
                }
                // influencer
                if (isset($request['influencer']) && !empty($request['influencer'])) {
                    $query->where('l.influencer', '=', $request['influencer']);
                }
                if (isset($request['date_start']) && !empty($request['date_start'])) {
                    //dd($request['date_start']);
                    $start = Carbon::parse($request['date_start']);
                    $query->where('al.created_at', '>=', $start->format('Y-m-d') . ' 00:00:00');
                }
                if (isset($request['date_end']) && !empty($request['date_end'])) {
                    $end = Carbon::parse($request['date_end'])->addDay();
                    $query->where('al.created_at', '<=', $end->format('Y-m-d') . ' 00:00:00');
                }
                // office
                if (isset($request['office']) && !empty($request['office'])) {
                    if ($request['office'] == "n/d") {
                        $query->whereNull('l.to_office');
                    } else {
                        $query->where('l.to_office', $request['office']);
                    }
                }
            })
            ->select(['l.id as lead_id', 'l.recycled', 'l.firstname', 'l.lastname', 'l.email', 'l.to_office', 'l.phone', 'p.name as pname', 'p.id as program_id', 'lf.campaign_name as canal', 'lff.title as influencer', 'u.name as conseiller', 'sup.name as support', 'cc.id as case_id', 'l.processed_at as processed', 'l.processed_message', 'al.created_at as created', 'causer.name as causer_name', 'ls.status_label_fr as status_label', 'ls.id as lead_status_id', 'hc.calls']);


        $actions = [
            /*["action" => "convert", "islink" => false, "title" => "convertir", "available" => (Auth::user()->isAdmin()) ? 1 : 0, "className" => "btn btn-sm btn-light lead-convert", "icon" => "far fa-copyright"],*/
            ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light lead-show", "icon" => "fas fa-eye"],
            /*["action" => "delete", "islink" => false, "title" => "supprimer", "available" => (Auth::user()->isAdmin() || Auth::user()->isManager()) ? 1 : 0, "className" => "btn btn-sm btn-light lead-delete", "icon" => "fas fa-trash"],*/
        ];

        return DataTables::of($logsQB->orderBy('al.created_at', 'desc'))
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  LeadStoreFormRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $phone_rules = ['required', 'unique:leads'];
        if (isset($request['phone_error']) && !empty($request['phone_error'])) {
            //  dd($phone_rules);
            array_push($phone_rules, new PhoneIndicator($request['phone_error']));
        }
        //dd($request);
        $rules = [
            'select' => 'required|in:office,conference',
            'program' => 'required',
            'firstname' => 'required|min:3|max:30',
            'lastname' => 'required|min:3|max:30',
            'email' => 'nullable|email|unique:leads',
            'phone' => $phone_rules,
            'program_field' => 'nullable',
            'file-input-cv' => 'nullable|file|mimes:pdf,doc,docx,ods',
            'campaign' => 'required',
            // 'influencer' => 'required',
            // 'influencer' => Rule::requiredIf(function () use ($request) {
            //     $y = LeadFormMarketingCampaign::where('id', $request['campaign'])->whereIn('id', [7, 16]);
            //     return $request->filled('campaign') && $y->count();
            // }),
            'reference' => Rule::requiredIf(function () use ($request) {
                return $request->filled('influencer') && ($request['influencer'] == 'other');
            }),
            // 'custom-reference' => Rule::requiredIf(function () use ($request) {
            //     return $request->filled('influencer') && ($request['influencer'] == 'other');
            // }),
            'reference' => Rule::requiredIf(function () use ($request) {
                return $request->filled('campaign') && ($request['campaign'] == 23);
            }),
            'currency_id' => 'required|exists:currencies,id',
            'lang' => 'required',
        ];
        $this->validate($request, $rules);
        // dd("true", $request['campaign']);
        // campaign
        //  && in_array($request['campaign'], [7, 16]) not working any more
        if (isset($request['campaign']) && !empty($request['campaign'])) {
            if (($request['influencer'] == 'other') && isset($request['custom-reference']) && !empty($request['custom-reference'])) {
                $form = LeadForm::create([
                    "user_id" => Auth::id(),
                    "title" => $request['custom-reference'],
                    "lead_form_marketing_campaign_id" => $request['campaign'],
                    "token" => Str::random(60),
                ]);
                $request['influencer'] = $form->id;
            }
        }
        //id:144 country maroc

        if (Auth::user()->isConseiller()) {
            $next_conseiller = Auth::id();
        } elseif (isset($request['conseiller']) && !empty($request['conseiller'])) {
            $next_conseiller = $request['conseiller'];
        } elseif ($request['lang'] == "en" && !str_starts_with($request['phone'], "+212")) {
            $id_lang_from_users_table = DB::table('users')->where('lang', 'en')->first();
            $next_conseiller = $id_lang_from_users_table->id;
        } elseif (Auth::user()->isAgent() || Auth::user()->isAdmin() || Auth::user()->isVerificateur() || Auth::user()->isSupport()) {
            // disabled
            if ($request['program'] === "9" && false) {
                // Get hrGlobe Conseillers
                $conseillers_managers = User::whereIn('role_id', [Role::Conseiller, Role::Manager])->where('status', 1)->where(function ($query) {
                    $query->where("email", "like", "%hrglobe%")
                        ->orWhere("email", "=", "i.benyoub@cpcanada.ma");
                })->get()->pluck('id')->toArray();
                $conseillers = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->orderby('id', 'asc')->pluck('conseiller_id')->toArray();

                $current_conseiller = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("hr_status", 1)->first();
                $index = $current_conseiller ? array_search($current_conseiller->conseiller_id, $conseillers) : false;

                if ($index == count($conseillers) - 1 || $index === false) {
                    $next_conseiller = $conseillers[0];
                } else {
                    $next_conseiller = $conseillers[$index + 1];
                }
                CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("hr_status", 1)->update(['hr_status' => 0]);
                CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("conseiller_id", $next_conseiller)->update(['hr_status' => 1]);
            } else {
                $conseillers_managers = User::whereIn('role_id', [Role::Conseiller, Role::Manager])->where('status', 1)->where("email", "not like", "%hrglobe%")->where("email", "!=", "i.benyoub@cpcanada.ma")->where('lang', 'LIKE', 'fr')->get()->pluck('id')->toArray();
                $conseillers = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->orderby('id', 'asc')->pluck('conseiller_id')->toArray();

                $current_conseiller = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("status", 1)->first()->conseiller_id;
                $index = array_search($current_conseiller, $conseillers);

                if ($index == count($conseillers) - 1 || $index === false) {
                    $next_conseiller = $conseillers[0];
                } else {
                    $next_conseiller = $conseillers[$index + 1];
                }
                CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("status", 1)->first()->update(['status' => 0]);
                CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->where("conseiller_id", $next_conseiller)->first()->update(['status' => 1]);
            }
        }

        $support_id = null;
        if (Auth::user()->isSupport()) {
            $support_id = Auth::id();
        } else {
            /**
             * Queue for the support
             */

            $supports = User::where('role_id', Role::Support)->where('status', 1)->get()->pluck('id')->toArray();
            $Qsupports = CurrentSelectedConseiller::whereIn('conseiller_id', $supports)->where("queue", "<>", 0)->orderby('id', 'asc')->pluck('queue', 'conseiller_id')->toArray();
            $LQsupports = CurrentSelectedConseiller::whereIn('conseiller_id', $supports)->where("left_queue", "<>", 0)->orderby('id', 'asc')->pluck('left_queue', 'conseiller_id')->toArray();

            $flag = 0;
            $bis_flag = 0;
            if (!!$LQsupports) {
                $current_support = CurrentSelectedConseiller::whereIn('conseiller_id', array_keys($LQsupports))->where("status", 1);
                if ($current_support->count()) {
                    $flag = 1;
                    $current_support = $current_support->first()->conseiller_id;
                    $index = array_search($current_support, array_keys($LQsupports));

                    if ((count($LQsupports) - 1 !== 0 && $index == count(array_keys($LQsupports)) - 1) || ($index == count(array_keys($LQsupports)) - 1 && count(array_keys($LQsupports)) - 1 === 0 && array_values($LQsupports)[$index] - 1 > 0)) {
                        $next_support = array_keys($LQsupports)[0];
                    } elseif (count($LQsupports) - 1 !== 0 && count($LQsupports) - 1 >= $index && array_values($LQsupports)[$index + 1] - 1 >= 0) {
                        $next_support = array_keys($LQsupports)[$index + 1];
                    } else {
                        // restart the cycle
                        $bis_flag = 1;
                        $next_support = array_keys($Qsupports)[0];
                    }

                    $left_queue = array_values($LQsupports)[$index];
                    // substitute the left_query
                    if ($flag) {
                        CurrentSelectedConseiller::whereIn('conseiller_id', array_keys($LQsupports))->where("status", 1)->first()->update(['status' => 0]);
                        CurrentSelectedConseiller::where("conseiller_id", $current_support)->update(['left_queue' => $left_queue - 1]);
                        CurrentSelectedConseiller::where("conseiller_id", $next_support)->update(['status' => 1]);
                    }
                    if ($bis_flag) {
                        foreach ($Qsupports as $key => $value) {
                            CurrentSelectedConseiller::where('conseiller_id', $key)->update(['left_queue' => $value]);
                        }
                    }
                    $support_id = $current_support;
                }
            }

            // initialise the cycle if there is no queue set
            if (!!$LQsupports === false || $flag === 0) {
                $no_flag = 0;
                $index = 0;
                // set up the current support as the first element of the array
                if ($flag === 0 && !!$LQsupports) {
                    $current_support = array_keys($LQsupports)[0];
                } elseif (!!$LQsupports === false || $no_flag) {
                    foreach ($Qsupports as $key => $value) {
                        CurrentSelectedConseiller::where('conseiller_id', $key)->update(['left_queue' => $value]);
                    }
                    $current_support = array_keys($Qsupports)[0];
                }

                if ((count($LQsupports) - 1 !== 0 && $index == count(array_keys($LQsupports)) - 1) || ($index == count(array_keys($LQsupports)) - 1 && count(array_keys($LQsupports)) - 1 === 0 && array_values($LQsupports)[$index] - 1 > 0)) {
                    $next_support = array_keys($LQsupports)[0];
                } elseif (count($LQsupports) - 1 !== 0 && count($LQsupports) - 1 >= $index && array_values($LQsupports)[$index + 1] - 1 >= 0) {
                    $next_support = array_keys($LQsupports)[$index + 1];
                } else {
                    // restart the cycle
                    $next_support = array_keys($Qsupports)[0];
                    $no_flag = 1;
                }

                $left_queue = array_values($LQsupports)[$index];
                CurrentSelectedConseiller::where("conseiller_id", $current_support)->update(['left_queue' => $left_queue - 1]);
                CurrentSelectedConseiller::where("conseiller_id", $next_support)->update(['status' => 1]);
                $support_id = $current_support;
            }
        }

        // @ahaloua
        // test add new support id here

        $lead = Lead::create([
            "program_id" => $request['program'],
            "campaign_id" => $request['campaign'],
            "to_office" => $request["select"],
            "firstname" => $request['firstname'],
            "lastname" => $request['lastname'],
            "email" => $request['email'],
            "birthday" => $request['birthday'],
            "phone" => $request['phone'],
            "country_id" => $request['country'],
            "city" => $request['city'],
            "lead_status_id" => $request['lead_status'],
            "conseiller_id" => $next_conseiller,
            "support_id" => $support_id,
            "influencer" => $request['influencer'],
            "reference" => $request['reference'],
            "program_field" => $request['program_field'],
            "lang" => $request['lang']
        ]);

        LeadStateLog::create([
            'user_id' => auth()->user()->id,
            'lead_id' => $lead->id,
            'state_id' => $lead->lead_status_id,
        ]);

        //For conseiller
        AssignmentLog::create([
            'user_id' => auth()->user()->id,
            'lead_id' => $lead->id,
            'conseiller_id' => $next_conseiller,
        ]);

        //for support
        AssignmentLog::create([
            'user_id' => auth()->user()->id,
            'lead_id' => $lead->id,
            'conseiller_id' => $support_id,
        ]);

        $lead->tags()->attach($request->input('tags'));
        //dd("teste");
        //activity()->log('Look, I logged something');
        if ($request->hasFile('file-input-cv')) {
            $lead->addMediaFromRequest('file-input-cv')->toMediaCollection('CVs');
        }
        $customer = Customer::where('lead_id', $lead->id)->first();
        if (!$customer) {
            $customer = Customer::create([
                "lead_id" => $lead->id,
                "program_id" => $lead->program_id,
                "program_step_id" => $lead->program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id,
                "firstname" => $lead->firstname,
                "lastname" => $lead->lastname,
                "campaign_id" => $lead->campaign_id,
                "lead_status" => $lead->lead_status_id,
                "customer_email" => $lead->email,
                "email" => $lead->email,
                "password" => Hash::make('123456'),
                "birthday" => $lead->birthday,
                "phone" => $lead->phone,
                "adresse_line_1" => $lead->adresse_line_1,
                "adresse_line_2" => $lead->adresse_line_2,
                "postal_code" => $lead->postal_code,
                "budget" => $lead->budget,
                "ranking_id" => $lead->ranking_id,
                "country_id" => $lead->country_id,
                "city" => $lead->city,
                "english_level_id" => $lead->english_level_id,
                "school_level_id" => $lead->school_level_id,
                "projected_diploma_type_id" => $lead->projected_diploma_type_id,
                "professional_domain_id" => $lead->professional_domain_id,
                "professional_experience" => $lead->professional_experience,
                "currency_id" => $lead->lang === "en" ? 2 : 1
            ]);
            $customer->save();
            // $cc = \App\Models\CustomerClear1000Fields::create([
            //     'customer_id' => $customer->id,
            // ]);
            // $cc->save();
            // $cc1 = \App\Models\CustomerClear1004Fields::create([
            //     'customer_id' => $customer->id,
            // ]);
            // $cc1->save();

            if ($lead->cv_id)
                $lead->getMedia('CVs')[0]->copy($customer, 'CVs');
        }
        // update currency
        // if(isset($request['currency_id']) && !empty($request['currency_id'])) {}
        $customer->update(["currency_id" => $request['currency_id']]);

        $case = CustomerCase::where('customer_id', $customer->id)->first();
        if (!$case) {
            //dd(count($case->get()));
            $case = CustomerCase::create([
                'user_id' => Auth::id(),
                'customer_id' => $customer->id,
                'program_id' => $customer->program_id,
                'program_step_id' => $customer->program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id,
            ]);
            $case->save();
        }

        $program_name = $lead->lang === "fr" ? Program::find($lead->program_id)->name : Program::find($lead->program_id)->labelEng;
        $conseiller = User::find($next_conseiller);
        $pole = null;
        if ($conseiller->pole_id) {
            $pole = Poles::find($conseiller->pole_id);
        }
        $languser = auth()->user()->lang;
        if ($lead->to_office == "office") {
            //Si French
            // //Si English
            if ($lead->lang === 'en') {
                $object = $program_name . ' - First meeting';
                $message = "";
                // I am '.$conseiller->name.' your '.($conseiller->gender == 'female' ? 'advisor':'advisor').'
                // It is our pleasure to welcome you to Clear Path Canada.
                // I would like to invite you to send me a message by email or WatsApp, letting me know the
                // time that suits you better to set a meeting at our office.
                // Whatsapp number:'.$conseiller->phone.'
                // Address: 150, Ciutat de Granada St., 3rd floor, 08018, Barcelona
                // Location: https://goo.gl/maps/G8oDCYfYFyiuFjqf7
                // From Monday to Friday, from 9:00 am to 18:30 pm.
                // See you soon!
                // ' . $conseiller->name . '
                // Clear Path Canada advisor
                // Tel : ' . $conseiller->phone . '
                // Tel : (+34) 645 90 17 17
                // E-mail : ' . $conseiller->email . '';
            } else {
                $object = $program_name . ' - Première rencontre';
                $message = "";
                // Je suis '.$conseiller->name.' votre '.($conseiller->gender == 'female' ? 'conseillère':'conseiller').' chez Clear Path Canada. Je serai ravi'.($conseiller->gender == 'female' ? 'e':'').' de vous accueillir dans l’agence à l’heure qui vous convient.
                // A très bientôt !
                // Adresse : 42, rue Bachir Laalej, Casablanca - Maroc
                // Localisation : https://goo.gl/maps/nB5hMXDQKrorCoVX8
                // Heures d’ouverture : Lundi au vendredi de 9h30-19h30. Samedi de 10h-18h30
                // ' . $conseiller->name . '
                // '.($conseiller->gender == 'female' ? 'conseillère':'conseiller').' Clear Path Canada
                // Tel : ' . $conseiller->phone . '
                // Tel : +212702050909
                // E-mail : ' . $conseiller->email . '';
            }
        } else {
            //Si French
            //Si English
            if (session()->get('locale') == 'en' || $languser == 'en') {
                $object = $program_name . ' -  First meeting by videoconference';
                $message = "";
                // I am ' . $conseiller->name . ' your '.($conseiller->gender == 'female' ? 'advisor':'advisor').'
                // It is our pleasure to welcome you to Clear Path Canada.
                // I would like to invite you to send me a message by email or WatsApp, letting me know the
                // time that suits you better to set a meeting by video-conference.
                // Whatsapp number:'.$conseiller->phone.'
                // Address: 150, Ciutat de Granada St., 3rd floor, 08018, Barcelona
                // Location: https://goo.gl/maps/G8oDCYfYFyiuFjqf
                // From Monday to Friday, from 9:00 am to 18:30 pm.
                // See you soon!
                // ' . $conseiller->name . '
                // Clear Path Canada advisor
                // Tel : ' . $conseiller->phone . '
                // Tel : (+34) 645 90 17 17
                // E-mail : ' . $conseiller->email . '';

            } else {
                $object = $program_name . ' - Première rencontre par vidéoconférence';
                $message = "";
                // Je suis ' . $conseiller->name . ' votre '.($conseiller->gender == 'female' ? 'conseillère':'conseiller').' chez Clear Path Canada. Je vous invite à m\'envoyer l\'heure qui vous convient par e-mail ou message WhatsApp pour passer la rencontre par vidéo-conférence.
                // A très bientôt !
                // Email : ' . $conseiller->email . '
                // Numéro WhatsApp : ' . $conseiller->phone . '
                // Heures d’ouverture : Lundi au vendredi de 9h30-19h30. Samedi de 10h-18h30
                // ' . $conseiller->name . '
                // '.($conseiller->gender == 'female' ? 'conseillère':'conseiller').' Clear Path Canada
                // Tel : ' . $conseiller->phone . '
                // Tel : +212702050909
                // E-mail : ' . $conseiller->email . '
                // Adresse : 42, rue Bachir Laalej, Casablanca - Maroc
                // Localisation : https://goo.gl/maps/nB5hMXDQKrorCoVX8';
            }
        }
        /*$object = 'Candidature Clear Path Canada';
        $message = '
        Nous vous remercions de nous avoir contactés. Veuillez nous confirmer votre numéro de téléphone et votre numéro de WhatsApp, un     conseiller spécialisé vous contactera dans les brefs délais.
        Nous vous invitons également à visiter nos locaux pour une rencontre qualifiée et enrichissante grâce à l’expertise de nos conseillers spécialisés.
        Adresse au Maroc : 42, rue Bachir Laalej, Casablanca, Maroc
        Adresse au Canada : 2000 McGill College Avenue, 6th Floor, Montréal, Québec, H3A 3H3
        Localisation : https://maps.google.com/?q=33.571827,-7.627207
        Nous vous remercions de l’attention portée à cette demande et nous vous prions d’accepter l’expression de nos sincères salutations.
        Cordialement,
        ' . User::find($next_conseiller)->name . '
        Conseiller Clear Path Canada
        ' . User::find($next_conseiller)->phone;*/

        $data = array(
            'name' => $request['firstname'] . ' ' . $request['lastname'],
            'object' => $object,
            'message' => $message,
            "elements" => [
                "lead_lang" => $lead->lang,
                "conseiller" => $conseiller,
                "pole" => $pole,
                "lead" => $lead,
                "email_phase" => 1,
            ],
        );

        if ($request['email']) {
            try {
                Mail::to($request['email'])->send(new sendingEmail($data));
            } catch (\Swift_TransportException $e) {
                // echo 'Exception reçue : ',  $e->getMessage(), "\n";
            }
        }

        return redirect()->route('leads-index')->with(['status' => 'Lead crée avec succés!']);
    }

    public function makeClient($lead)
    {
        // convert lead to client
        $lead = Lead::findOrFail($lead);
        $old_state = $lead->lead_status_id;
        $lead->lead_status_id = LeadStatus::Client;
        if ($old_state != LeadStatus::Client) {
            LeadStateLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'state_id' => $lead->lead_status_id,
            ]);
        }
        $lead->save();
        $customer = Customer::where('lead_id', $lead->id)->first();
        if (!$customer) {
            //dd($customer);
            $customer = Customer::create([
                "lead_id" => $lead->id,
                "program_id" => $lead->program_id,
                "program_step_id" => $lead->program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id,
                "firstname" => $lead->firstname,
                "lastname" => $lead->lastname,
                "campaign_id" => $lead->campaign_id,
                "lead_status" => $lead->lead_status_id,
                "customer_email" => $lead->email,
                "email" => $lead->email,
                "password" => Hash::make('123456'),
                "birthday" => $lead->birthday,
                "phone" => $lead->phone,
                "adresse_line_1" => $lead->adresse_line_1,
                "adresse_line_2" => $lead->adresse_line_2,
                "postal_code" => $lead->postal_code,
                "budget" => $lead->budget,
                "ranking_id" => $lead->ranking_id,
                "country_id" => $lead->country_id,
                "english_level_id" => $lead->english_level_id,
                "school_level_id" => $lead->school_level_id,
                "projected_diploma_type_id" => $lead->projected_diploma_type_id,
                "professional_domain_id" => $lead->professional_domain_id,
                "professional_experience" => $lead->professional_experience,
                "currency_id" => $lead->lang === "en" ? 2 : 1
            ]);
        }
        $customer->is_client = 1;
        $customer->save();
        $y = LeadStatus::find($lead->lead_status_id);
        //Add to activity log
        activity('lead-status-log')
            ->performedOn($lead)
            ->causedBy(Auth::user())
            ->withProperties(['status' => ($y ? $y->id : null)])
            ->log('Switch to client');
        $case = CustomerCase::where('customer_id', $customer->id)->first();
        if (!$case) {
            $case = CustomerCase::create([
                'user_id' => Auth::id(),
                'customer_id' => $customer->id,
                'program_id' => $customer->program_id,
                'program_step_id' => 2,
            ]);
            $case->save();
        }
        return new LeadResource($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  LeadUpdateFormRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function savePhone(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $rules = [
            'form-father-phone'   => [Rule::requiredIf(function () use ($customer) {
                return $customer->program_id == 4;
            }), new PhoneIndicator($request['form-father-phone_error'])],
            'form-mother-phone'   => [Rule::requiredIf(function () use ($customer) {
                return $customer->program_id == 4;
            }), new PhoneIndicator($request['form-mother-phone_error'])],
        ];
        $this->validate($request, $rules);
        $customer->father_phone = $request["form-father-phone"];
        $customer->mother_phone = $request["form-mother-phone"];
        //dd('tes');
        $customer->save();
    }
    public function updateLeadStatus($lead, $status)
    {
        $lead = Lead::findOrFail($lead);
        $old_state = $lead->lead_status_id;
        $lead->lead_status_id = $status;
        if ($old_state != $status) {
            LeadStateLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'state_id' => $lead->lead_status_id,
            ]);
        }
        $lead->save();
        return new LeadResource($lead);
    }

    public function updateLeadConseiller(Request $request)
    {
        $leads_ids = $request['leads_ids'];
        $users = $request['resultConseilArray'];
        $supports = [];
        $managers_conseillers = [];
        if (is_array($users) || is_object($users)) {
            foreach ($users as $user) {
                if (User::find($user)->role->id == Role::Support) {
                    array_push($supports, $user);
                } else {
                    array_push($managers_conseillers, $user);
                }
            }
        }
        $offset_s = 0;
        $offset_mc = 0;
        foreach ($leads_ids as $leads_id) {
            if ($offset_s == count($supports)) {
                $offset_s = 0;
            }
            if ($offset_mc == count($managers_conseillers)) {
                $offset_mc = 0;
            }
            $lead = Lead::findOrFail($leads_id);

            if (count($supports)) {
                $lead->support_id = $supports[$offset_s];
                AssignmentLog::create([
                    'user_id' => auth()->user()->id,
                    'lead_id' => $lead->id,
                    'conseiller_id' => $supports[$offset_s],
                ]);
            }

            if (count($managers_conseillers)) {
                $lead->conseiller_id = $managers_conseillers[$offset_mc];

                AssignmentLog::create([
                    'user_id' => auth()->user()->id,
                    'lead_id' => $lead->id,
                    'conseiller_id' => $managers_conseillers[$offset_mc],
                ]);
            }

            $lead->save();
            $offset_s++;
            $offset_mc++;
        }
        return true;
    }

    public function update(Request $request, $id)
    {
        $phone_rules = ['required'];
        if (isset($request['phone_error']) && !empty($request['phone_error'])) {
            //dd($request['phone_error']);
            array_push($phone_rules, new PhoneIndicator($request['phone_error']));
        }
        $rules = [
            'program' => 'required',
            'firstname' => 'required|min:3|max:30',
            'lastname' => 'required|min:3|max:30',
            'email' => 'required|email',
            'phone' => $phone_rules,
            'file-input-cv' => 'nullable|file|mimes:pdf,doc,docx,ods',
            'campaign' => 'nullable',
        ];
        $this->validate($request, $rules);

        $lead = Lead::findOrFail($id);
        $old_state = $lead->lead_status_id;

        $lead->program_id = $request['program'];
        $lead->firstname = $request['firstname'];
        $lead->campaign_id = $request['campaign'];
        $lead->lastname = $request['lastname'];
        $lead->email = $request['email'];
        $lead->birthday = $request['birthday'];
        $lead->phone = $request['phone'];
        $lead->influencer = $request['influencer'];
        // $lead->adresse_line_1 = $request['adresse1'];
        $lead->lead_status_id = $request['lead_status'];
        if (isset($request['conseiller']) && !empty($request['conseiller'])) {
            $lead->conseiller_id = $request['conseiller'];
        }
        if (isset($request['support']) && !empty($request['support'])) {
            $lead->support_id = $request['support'];
        }

        // $lead->school_level_id = $request['school'];
        // $lead->projected_diploma_type_id = $request['projected'];
        // $lead->professional_domain_id = $request['pdomain'];
        // $lead->professional_experience = $request['experience'];
        $lead->save();
        if ($old_state != $request['lead_status']) {
            LeadStateLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'state_id' => $lead->lead_status_id,
            ]);
        }

        //update conseiller
        if (isset($request['conseiller']) && !empty($request['conseiller'])) {
            $lead->conseiller_id = $request['conseiller'];
            AssignmentLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'conseiller_id' => $request['conseiller'],
            ]);
        }
        //update support
        if (isset($request['support']) && !empty($request['support'])) {
            $lead->support = $request['conseiller'];
            AssignmentLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'conseiller_id' => $request['support'],
            ]);
        }

        if (isset($request['program']) && !empty($request['program'])) {
            $customer = Customer::where("lead_id", $lead->id)->first();
            if ($customer) {
                $customer->program_id = $request['program'];
                $customer->lead_status = $lead->lead_status_id;
                $customer->save();
                //dd($customer);
            }
        }
        //dd($customer);
        if ($request->hasFile('file-input-cv')) {
            $lead->addMediaFromRequest('file-input-cv')->toMediaCollection('CVs');
        }

        return redirect()->route('leads-index')->with(['status' => 'Lead modifié avec succés!']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Lead  $lead
     * @return CustomerResource
     */
    public function convert(Lead $lead)
    {
        //$customer =
        // if($lead->campaign_id) {
        //     $campaign = LeadFormMarketingCampaign::where('id', '=', $lead->campaign_id)->first();
        // }
        //$campaign = LeadFormMarketingCampaign::where('id', '=', $lead->campaign_id)->first();
        //dd($lead->program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id);
        //dd($lead);
        //if (Customer::where())
        $customer = Customer::create([
            "lead_id" => $lead->id,
            "program_id" => $lead->program_id,
            "program_step_id" => $lead->program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id,
            "firstname" => $lead->firstname,
            "lastname" => $lead->lastname,
            "lead_status" => $lead->lead_status_id,
            "customer_email" => $lead->email,
            "email" => $lead->email,
            "password" => Hash::make('123456'),
            "birthday" => $lead->birthday,
            "phone" => $lead->phone,
            "adresse_line_1" => $lead->adresse_line_1,
            "adresse_line_2" => $lead->adresse_line_2,
            "postal_code" => $lead->postal_code,
            "budget" => $lead->budget,
            "ranking_id" => $lead->ranking_id,
            "country_id" => $lead->country_id,
            "english_level_id" => $lead->english_level_id,
            "school_level_id" => $lead->school_level_id,
            "projected_diploma_type_id" => $lead->projected_diploma_type_id,
            "professional_domain_id" => $lead->professional_domain_id,
            "professional_experience" => $lead->professional_experience,
            "currency_id" => $lead->lang === "en" ? 2 : 1
        ]);
        //dd($campaign);
        if (isset($lead->campaign_id)) {
            //$campaign = LeadFormMarketingCampaign::where('id', '=', $lead->campaign_id)->first();
            $customer->campaign_id = $lead->campaign_id;
            //$customer->fill(['campaign' => $campaign->campaign_name]);
        }
        $customer->save();
        if ($lead->cv_id)
            $lead->getMedia('CVs')[0]->copy($customer, 'CVs');

        //$tt = new CustomerResource($customer);
        //dd($tt);
        return new CustomerResource($customer);
    }

    public function assign($lead)
    {
        $lead = Lead::findOrFail($lead);
        if (Auth::user()->isConseiller()) {
            //conseiller
            $lead->conseiller_id = Auth::id();
            AssignmentLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'conseiller_id' => Auth::id(),
            ]);
        } else {
            //support
            $lead->support_id = Auth::id();
            AssignmentLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $lead->id,
                'conseiller_id' => Auth::id(),
            ]);
        }
        $lead->save();
        $customer = Customer::where('lead_id', $lead->id)->first();
        if (!$customer) {
            $customer = Customer::create([
                "lead_id" => $lead->id,
                "program_id" => $lead->program_id,
                "program_step_id" => $lead->program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id,
                "firstname" => $lead->firstname,
                "lastname" => $lead->lastname,
                "campaign_id" => $lead->campaign_id,
                "lead_status" => $lead->lead_status_id,
                "customer_email" => $lead->email,
                "email" => $lead->email,
                "password" => Hash::make('123456'),
                "birthday" => $lead->birthday,
                "phone" => $lead->phone,
                "adresse_line_1" => $lead->adresse_line_1,
                "adresse_line_2" => $lead->adresse_line_2,
                "postal_code" => $lead->postal_code,
                "budget" => $lead->budget,
                "ranking_id" => $lead->ranking_id,
                "country_id" => $lead->country_id,
                "english_level_id" => $lead->english_level_id,
                "school_level_id" => $lead->school_level_id,
                "projected_diploma_type_id" => $lead->projected_diploma_type_id,
                "professional_domain_id" => $lead->professional_domain_id,
                "professional_experience" => $lead->professional_experience,
                "currency_id" => $lead->lang === "en" ? 2 : 1
            ]);
        }
        $customer->save();
        $case = CustomerCase::where('customer_id', $customer->id)->first();
        if (!$case) {
            $case = CustomerCase::create([
                'user_id' => Auth::id(),
                'customer_id' => $customer->id,
                'program_id' => $customer->program_id,
            ]);
            $case->save();
        }
        return new LeadResource($customer);
    }

    public function switch($lead)
    {
        $lead = Lead::findOrFail($lead);
        $customer = Customer::where('lead_id', $lead->id)->first();
        $customer->is_client = 1;
        $y = LeadStatus::find($lead->lead_status_id);
        $customer->save();
        //Add to activity log
        activity('lead-status-log')
            ->performedOn($lead)
            ->causedBy(Auth::user())
            ->withProperties(['status' => ($y ? $y->id : null)])
            ->log('Lead to R2');
        $case = CustomerCase::where('customer_id', $customer->id)->first();



        return response()->json($case);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLeads(Request $request)
    {
        $data = [];

        if ($request->has('q')) {
            $search = isset($request->q['term']) ? $request->q['term'] : '';
            $data = Lead::select("id", DB::raw("CONCAT(firstname, ' ', lastname) AS name"))
                ->where(function ($query) use ($search) {

                    $query->where('firstname', 'LIKE', "%$search%");
                    $query->orWhere('lastname', 'LIKE', "%$search%");
                })
                ->get();
        }

        return response()->json($data);
    }

    public function editTags(Request $request, $lead_id)
    {
        $lead = Lead::find($lead_id);
        $lead->tags()->sync($request->input('tags'));
        return back()->with(['status' => 'Mots clés modifié avec succés!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Lead $lead
     * @return int
     * @throws \Exception
     */
    public function destroy(Lead $lead)
    {
        if ($lead->customer) {
            $lead->customer->mediaSheets()->detach();
            $lead->customer->payments()->detach();
        }
        return $lead->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Lead $lead
     * @param  String $collectionName
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteMedia(Lead $lead, $collectionName)
    {
        foreach ($lead->getMedia($collectionName) as $media) $media->delete();
        return back()->with(['status' => 'Document supprimé avec succés!']);
    }


    /**
     * Regenerate Pdf Docs based on customer currency
     *
     * @param  Lead $lead
     * @param  String $collectionName
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function generateDocs(Lead $lead)
    {
        $docs = [
            "clear1001" => "Clear1001 – Conseil",
            // "clear1004" => "Clear1004 - Rapport Scoring",
            "contrat"   => "Contrat client",
            "devis"     => "Devis",
            "bill"      => "Facture",
            "recu"      => "Reçu"
        ];

        foreach ($docs as $doc_name => $doc_type) {
            if ($lead->customer->program_id === 4 && $doc_name === "clear1004") {
                continue;
            } elseif ($lead->customer->program_id !== 4 && $doc_name === "clear1001") {
                continue;
            } elseif ($doc_name === "bill") {
                $step_state = \App\Models\SubStepState::find(3);
                $bill = \App\Models\Bill::where('customer_id', $lead->customer->id)->first();
                $doc = DocumentType::where('name_fr', $doc_type)->first();
                $this->addMediaPDF($lead->customer, $doc, $doc_name, $bill, $step_state);
            } elseif ($doc_name === "recu") {
                $step_state = \App\Models\SubStepState::find(3);
                $doc = DocumentType::where('name_fr', $doc_type)->first();
                // check collection name
                $this->addMediaPDF($lead->customer, $doc, $doc_name, null, $step_state);
            } else {
                $doc = DocumentType::where('name_fr', $doc_type)->first();
                $this->addMediaPDF($lead->customer, $doc, $doc_name, null, null);
            }
        }
        return back()->with(['status' => 'Document régénéré avec succés!']);
    }
}
