<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmailNotification\EmailNotificationAddFormRequest;
use App\Http\Requests\EmailNotification\EmailNotificationUpdateFormRequest;
use App\Http\Resources\EmailNotificationResource;
use App\Models\EmailNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmailNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $emailNotificationQB = DB::table('email_notifications')
            ->select('*');
        if (isset($request['object']) && !empty($request['object'])) {
            $emailNotificationQB->where('object', 'like', '%' . $request['object'] . '%');
        }
        if (isset($request['message']) && !empty($request['message'])) {
            $emailNotificationQB->where('message', 'like', '%' . $request['message'] . '%');
        }

        $actions = [
            ["action" => "edit", "title" => "Modifier message", "available" => 1, "className" => "btn btn-primary emailnotification-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer message", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger emailnotification-table-delete", "icon" => "fas fa-trash"],
        ];


        return DataTables()->of($emailNotificationQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return EmailNotificationResource
     */
    public function store(EmailNotificationAddFormRequest $request)
    {
        return new EmailNotificationResource(EmailNotification::create([
            "object" => $request['add-form-object'],
            "message" => $request['add-form-message'],
            "user_id" => Auth::id()
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return EmailNotificationResource
     */
    public function update(EmailNotificationUpdateFormRequest $request, $id)
    {
        $emailnotification = EmailNotification::findOrFail($id);
        $emailnotification->object = $request['edit-form-object'];
        $emailnotification->message = $request['edit-form-message'];
        $emailnotification->user_id = Auth::id();
        $emailnotification->save();

        return new EmailNotificationResource($emailnotification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  EmailNotification $emailnotification
     * @return int
     */
    public function destroy(EmailNotification $emailnotification)
    {
        return $emailnotification->delete();
    }
}
