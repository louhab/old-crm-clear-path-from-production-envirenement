<?php

namespace App\Http\Controllers\Api\v1;

use App\Events\LeadProspectionUpdated;
use App\Http\Controllers\Controller;
use App\Http\Resources\LeadFormResource;
use App\Mail\sendingEmail;
use App\Models\Calendrier;
use App\Models\CaseState;
use App\Models\Customer;
use App\Models\CustomerCase;
use App\Models\DocumentType;
use App\Models\Lead;
use App\Models\LeadStateLog;
use App\Models\LeadStatus;
use App\Models\ProgramPayment;
use App\Models\ProgramStep;
use App\Models\SubStepState;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\Facades\DataTables;

class ProgramPaymentController extends Controller
{
    private $states =  [3, 5, 10, 20, 29];
    private $steps  =  [2, 3, 4, 5, 6];
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $discounts = DB::table('program_payment_customer as pc')
            ->leftJoin('customers as c', 'c.id', '=', 'pc.customer_id')
            ->leftJoin('program_payments as pp', 'pp.id', '=', 'pc.program_payment_id')
            ->whereNotIn("pc.discount_percent", [0])
            /*->where(function ($query) use ($request) {
                if(isset($request['campaign-type']) && !empty($request['campaign-type'])) {
                    $query->where('lf.lead_form_marketing_campaign_id', '=', $request['campaign-type']);
                }
            })*/
            ->select(['pc.id', 'pc.discount_percent as discount', DB::raw("CONCAT(c.firstname, ' ', c.lastname) AS name"), 'pp.name_fr as payment']);

        $actions = [
            ["action" => "copy", "title" => "Copier Lien", "available" => 1, "className" => "btn btn-info leadsforms-table-copy", "icon" => "fas fa-copy"],
            ["action" => "edit", "title" => "Editer Formulaire", "available" => 1, "className" => "btn btn-primary leadsforms-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer Formulaire", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger leadsforms-table-delete", "icon" => "fas fa-trash"],
        ];

        // return DataTables()->eloquent($leadformsQB)
        //     ->addColumn('actions', $actions)
        //     ->toJson();

        return DataTables::of($discounts)
            // ->addColumn('actions', $actions)
            ->toJson();
    }
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPayments(Request $request)
    {
        $data = [];

        if ($request->has('q')) {
            $search = isset($request->q['term']) ? $request->q['term'] : '';
            $data = ProgramPayment::select("id", "name_fr")
                ->where('name_fr', 'LIKE', "%$search%")
                ->get();
        }

        return response()->json($data);
    }

    public function getAmount(Request $request)
    {
        $rules = [
            'add-form-lead' => 'required',
            'add-form-payment' => 'required',
        ];

        $this->validate($request, $rules);

        $payment = ProgramPayment::findOrFail($request['add-form-payment']);
        $customer = Customer::findOrFail($request['add-form-lead']);
        // $data = [];
        $pid = $customer->program_id;
        if ($pid != 4)
            $pid = 1;
        return response()->json($payment->steps()->where("program_id", $pid)->select("amount")->first());
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return LeadFormResource
     */
    public function store(Request $request)
    {
        $rules = [
            'add-form-lead' => 'required',
            'add-form-payment' => 'required',
            'add-form-amount' => 'required',
            'add-form-percent' => 'required',
        ];

        $this->validate($request, $rules);

        // dd($request->all());


        $payment = ProgramPayment::findOrFail($request['add-form-payment']);

        $sub_steps = [1 => 3, 2 => 5, 3 => 10, 4 => 20, 5 => 29];
        $step_state = SubStepState::find($sub_steps[$request['add-form-payment']]);
        $doc_recu = DocumentType::where('name_fr', 'Reçu')->first();


        $customer = Customer::findOrFail($request['add-form-lead']);
        // dd("sir jib dora hhhh", $test->case()->);

        $payment->customers()->sync([$request['add-form-lead'] => ['discount_percent' => $request['add-form-percent'], 'amount' => $request['add-form-amount']]], false);

        foreach ($customer->getMedia($step_state->name_fr . $doc_recu->collection_name) as $media) $media->delete();
        $this->addMediaPDF($customer, $doc_recu, 'recu', "null", $step_state);
        //$campaign = LeadForm::findOrFail($request['add-form-campaign']);
        return response()->json($payment->customers()->where("customer_id", $request['add-form-lead'])->get());
    }
    /**
     * edit resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return
     */
    public function update(CaseState $case_state, $method)
    {
        $customer = Customer::find(CustomerCase::find($case_state->customer_case_id)->customer_id);
        if ($case_state->sub_step_state_id == 3 && $customer->lead->lead_status_id == 7) {
            /*$meets = Calendrier::all()->filter(function ($meet) use ($customer) {
                return $meet->customer_id == $customer->id && (str_starts_with($meet->title, 'Rencontre 1') || str_starts_with($meet->title, str_replace('Rencontre ', 'R', 'Rencontre 1'))) ;
            });
            if ($customer->lead->to_office == "conference" && $meets->count()) {
                $customer->lead_status = LeadStatus::Contrat;
                $customer->lead->update(array('lead_status_id' => LeadStatus::Contrat));
                $customer->save();
            }
            elseif ($customer->lead->to_office == "office") {
            }*/
            $old_state = $customer->lead->lead_status_id;
            $customer->lead_status = LeadStatus::Contrat;
            $customer->lead->update(array('lead_status_id' => LeadStatus::Contrat));
            $customer->save();
            if ($old_state != LeadStatus::Contrat) {
                LeadStateLog::create([
                    'user_id' => auth()->user()->id,
                    'lead_id' => $customer->lead->id,
                    'state_id' => $customer->lead->lead_status_id,
                ]);
            }
            /*else{
                // handling error
                session();
            }*/
            //$lead = Lead::find($customer->lead_id)->first();
            //$lead->lead_status_id = LeadStatus::Contrat;
            //ead->save();
        }

        $stepIndex = array_search($case_state->sub_step_state_id, $this->states);
        if ($stepIndex !== false) {
            $step = ProgramStep::find($this->steps[$stepIndex]);
            $pid = $customer->program_id;
            if ($pid != 4) {
                $pid = 1;
            }
            $payment = $step->payments()->withPivot("program_id", "amount")
                ->where("program_payment_program_step.program_id", $pid)
                ->where("program_payment_program_step.currency_id", $customer->currency_id)
                ->first();
            $languser = auth()->user()->lang;

            if ($payment->id == 2 && $customer->program_id != 4) {
                $customer->admission_state = 17;
                $customer->admis_at = Carbon::now();
                $customer->processed_at = Carbon::now();
                $customer->save();
            }

            if ($payment->id == 3 && $customer->program_id == 4) {
                $customer->admission_state = 1;
                $customer->admis_at = Carbon::now();
                $customer->processed_at = Carbon::now();
                $customer->save();
            }


            if ($payment->id == 2) {
                if ($customer->program_id != 4) {
                    if (session()->get('locale') == 'en' || $languser == 'en') {
                        $object = "Language test guide";
                        $message = "
                        Hello:" . $customer->firstname . ",
                        As agreed, I provide you with the procedure and resources to get prepared for your English/French exam. It will be necessary to apply for the tests as soon as possible and be prepared for the exam.
                        Finally, once you have the test results, you can send them to us.
                        ENGLISH / FRENCH TEST (suggested links to apply for the test):
                        - Links IELTS in Spain
                        https://www.ielts.org/for-test-takers/book-a-test/location-list/spain/tv
                        - Links TCF and TEF
                        https://www.france-education-international.fr/centres-d-examen/carte?type-centre=tcf
                        RESOURCES IELTS (general), TCF and TEF Canada, TCF
                        - TEF resources:
                        https://drive.google.com/drive/folders/1Cz2ZFUA3nMPeooPqbVN-AKUgJLPkVVW9
                        - TCF resources:
                        https://drive.google.com/drive/folders/1x9EjLe4qhrIMmf2JRFZzWjMFt67pR1jH
                        - IELTS resources:
                        https://drive.google.com/drive/folders/1woO1uNc-NojNUxBHa1BRzHXHZ4n3J8T8
                        We wish you an optimal preparation.
                        Sincerely,
                        ";
                    } else {
                        $object = 'Guide des tests de langue';
                        $message = "
                            Bonjour Mer " . $customer->firstname . ",
                            Comme convenu je vous fais part de la procédure ainsi que les ressources pour préparer vos tests d'anglais et français. Il faudra s'inscrire aux tests dès que possible et se préparer pour l'examen. Enfin, une fois que vous avez les résultats des tests vous pouvez nous les transmettre.
                            Voilà le lien pour des informations sur le TEF Canada et TCF Canada (Frais de 3000 DHS par personne) https://www.lefrancaisdesaffaires.fr/centres/maroc/
                            Voilà le lien pour des information sur le IELTS (General)
                            https://www.ielts.org/book-a-test/find-a-test-location/location-list/morocco/tv (Frais de 3000 DHS par personne)
                            Ressource de préparation pour le IELTS (General), TEF Canada
                            - IELTS
                            https://drive.google.com/drive/folders/1xqbB3ImVlC8f-FJKRXQ5Ynjs_GEkfKcS?usp=sharing
                            - TEF
                            https://drive.google.com/drive/folders/1Cz2ZFUA3nMPeooPqbVN-AKUgJLPkVVW9<br>
                            - TCF
                            https://drive.google.com/drive/folders/1x9EjLe4qhrIMmf2JRFZzWjMFt67pR1jH<br>
                            Je vous souhaite une bonne préparation.
                            Cordialement,
                            ";
                    }

                    $data = array(
                        'name' => $customer->firstname . ' ' . $customer->lastname,
                        'object' => $object,
                        'message' => $message,
                        'elements' => "",
                    );

                    if ($customer->email) {
                        try {
                            Mail::to($customer->email)->send(new sendingEmail($data));
                        } catch (\Swift_TransportException $e) {
                            // echo 'Exception reçue : ',  $e->getMessage(), "\n";
                        }
                    }
                }
                // Enable payment for Agent
                /*$step_state = SubStepState::where("name_fr", "Rencontre 2: Terminer Appel")->first();
                $meet = CaseState::where("sub_step_state_id", $step_state->id)
                    ->where("customer_case_id", $customer->case->id)
                    ->first();
                if (!$meet)
                    $meet = CaseState::create([
                        'user_id' => Auth::id(),
                        'customer_case_id' => $customer->case->id,
                        'sub_step_state_id' => $step_state->id,
                        'status' => true,
                    ]);
                else
                    $meet->status = true;

                $meet->save();*/
            }
            $pms = DB::table("program_payment_customer as pc")
                ->where("pc.customer_id", $customer->id)
                ->where("pc.program_payment_id", $payment->id)
                ->whereNotIn("pc.discount_percent", [0]);
            if ($pms->count()) {
                $customer->payments()->sync([
                    $payment->id => [
                        'performed_at' => \Carbon\Carbon::now(),
                        'payment_method' => $method,
                        'currency_id' => $customer->currency_id,
                        "conseiller_id" => $customer->lead->conseiller_id
                    ]
                ], false);
            } else {
                $customer->payments()->sync([
                    $payment->id => [
                        'discount_percent' => 0,
                        'amount' => $payment->pivot->amount,
                        'currency_id' => $customer->currency_id,
                        'performed_at' => \Carbon\Carbon::now(),
                        'payment_method' => $method,
                        "conseiller_id" => $customer->lead->conseiller_id
                    ]
                ], false);
            }
            // show sheets to customer
            $mediaSheets = \App\Models\MediaSheet::with('substeps')->get();
            foreach ($mediaSheets as $mediaSheet) {
                if ($mediaSheet->substeps()->wherePivot('program_id', $customer->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                    continue;
                $sheet = $customer->mediaSheets()->wherePivot('media_sheet_id', '=', $mediaSheet->id);

                if (count($sheet->get()))
                    $customer->mediaSheets()->updateExistingPivot($mediaSheet->id, [
                        'can_edit' => true
                    ]);
                else
                    $customer->mediaSheets()->attach([
                        $mediaSheet->id => [
                            'can_edit' => true
                        ]
                    ]);
            }
            // generate recu et facture
            /*$step_state = SubStepState::find($case_state->sub_step_state_id);
            $doc_recu = DocumentType::where('name_fr', 'Reçu')->first();
            $doc_bill = DocumentType::where('name_fr', 'Facture')->first();
            foreach ($customer->getMedia($step_state->name_fr . $doc_recu->collection_name) as $media) $media->delete();
            foreach ($customer->getMedia($step_state->name_fr . $doc_bill->collection_name) as $media) $media->delete();
            $bill = $this->createBill($customer);
            $this->addMediaPDF($customer, $doc_bill, 'bill', $bill, $step_state);
            $this->addMediaPDF($customer, $doc_recu, 'recu', "null", $step_state);*/

            return response()->json(["success" => "updated", "payment" => $payment->name_fr]);
        }
        return response()->json([]);
    }

    public function destroy(CaseState $case_state)
    {
        $customer = Customer::find(CustomerCase::find($case_state->customer_case_id)->customer_id);

        $stepIndex = array_search($case_state->sub_step_state_id, $this->states);
        if ($stepIndex !== false) {
            $step = ProgramStep::find($this->steps[$stepIndex]);
            $pid = $customer->program_id;
            if ($pid != 4) {
                $pid = 1;
            }
            $payment = $step->payments()->withPivot("program_id", "amount")->where("program_payment_program_step.program_id", $pid)->first();
            $customer->payments()->sync([
                $payment->id => [
                    'performed_at' => null,
                    'payment_method' => null
                ]
            ]);
            /*if (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'lead-view') {
                LeadProspectionUpdated::dispatch($customer->lead, 'Paiement effectuée');
            }*/
            $customer->lead->update(array('processed_message' => 'Paiement supprimé', 'processed_at' => Carbon::now()));
            return response()->json(["success" => "updated", "payment" => $payment->name_fr]);
        }
        return response()->json(["status" => "error"]);
    }
}
