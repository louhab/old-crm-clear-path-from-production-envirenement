<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\QuoteResource;
use App\Models\Quote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $quotes = Quote::with(['user', 'customer', 'services'])
            ->where(function ($query) use ($request) {
                if (isset($request['cid']) && !empty($request['cid'])) {
                    $query->where('customer_id', $request['cid']);
                }
            });

        $actions = [
            ["action" => "edit", "title" => "Modifier devis", "available" => 1, "className" => "btn btn-primary quote-table-edit", "icon" => "fas fa-edit"],
            ["action" => "view", "title" => "Visualiser devis", "available" => 1, "downloadable" => 1, "target" => 1, "className" => "btn btn-info", "icon" => "fas fa-eye"],
            /*["action" => "download", "title" => "Télécharger devis", "available" => 1,"downloadable" => 1, "className" => "btn btn-info", "icon" => "fas fa-download"],*/
            ["action" => "delete", "title" => "Supprimer devis", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger quote-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables()->eloquent($quotes)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return QuoteResource
     */
    public function store(Request $request)
    {

        //TODO add program/customer validation

        $services = $request['services'];
        //dd("test::==>;;");
        $quote = NULL;

        DB::transaction(function () use ($request, $services, &$quote) {
            //$quote = new Quote;
            $quoteArr = [
                'user_id' => Auth::id(),
                'customer_id' => $request['cid']
            ];
            //$quote->user_id = Auth::id();
            if ($request->has('ap')) $quoteArr['applied_price'] = $request['ap'];

            $quote = Quote::create($quoteArr);



            // $quote->customer_id = $request->cid;

            // $quote->save();

            $servicesToAttach = [];

            foreach ($services as $service) {
                $servicesToAttach[$service[0]] = [
                    'billed_price' => $service[1],
                    'qte' => $service[2]
                ];
            }

            $quote->services()->sync($servicesToAttach);
        });

        return new QuoteResource($quote);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Quote $quote
     * @return QuoteResource
     */
    public function update(Request $request, Quote $quote)
    {
        $services = $request['services'];

        DB::transaction(function () use ($request, $services, $quote) {

            if ($request->has('ap')) {
                $quote->applied_price = $request['ap'];
                $quote->save();
            }


            $servicesToAttach = [];

            foreach ($services as $service) {
                $servicesToAttach[$service[0]] = [
                    'billed_price' => $service[1],
                    'qte' => $service[2]
                ];
            }

            $quote->services()->sync($servicesToAttach);
        });

        return new QuoteResource($quote);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Quote $quote
     * @return int
     */
    public function destroy(Quote $quote)
    {
        return $quote->delete();
    }
}
