<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
// use App\Http\Requests\Lead\LeadUpdateFormRequest;
// use App\Http\Requests\Lead\LeadStoreFormRequest;
// use App\Http\Resources\CustomerResource;
// use App\Http\Resources\LeadResource;
// use App\Mail\sendingEmail;
// use App\Models\CurrentSelectedConseiller;
use App\Models\Customer;
// use App\Models\CustomerCase;
// use App\Models\Lead;
// use App\Models\LeadFormMarketingCampaign;
// use App\Models\ProgramStep;
// use App\Models\Role;
// use App\Models\User;
use App\Models\AdmissionState;
use App\Models\AdmissionStateGroup;
use App\Models\CaseComment;
use App\Models\CustomerCase;
use App\Models\OrientationSchool;
use App\Models\ProgramCourse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Yajra\DataTables\Facades\DataTables;

use Illuminate\Validation\Rule;

use App\Rules\PhoneIndicator;

class AdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        //TODO refacto DB::table

        // dd($request->all());
        $admissionsQB = DB::table('customers as c')
            ->leftJoin('customer_cases as cc', 'cc.customer_id', '=', 'c.id')
            /*->leftJoin('customer_case_case_comment as comment', function($query) {
                $comments = DB::table('customer_case_case_comment as customer_comment')
                    ->leftJoin('case_comments as case_comment', 'customer_comment.case_comment_id', '=', 'case_comment.id')
                    ->whereDate('case_comment.created_at', '>=', Carbon::today())
                    ->where('customer_comment.view', "soumission-view")
                    ->get()
                    ->pluck("case_comment_id")
                    ->toArray();
                $query->on('comment.customer_case_id', '=', 'cc.id');
                if (count($comments)) {
                    $query->whereIn('comment.case_comment_id', array_unique($comments));
                } else {
                    $query->whereNull('comment.case_comment_id');
                }
            })*/
            /*->leftJoin('customer_case_case_comment as client_comment', function($query) {
                $comments = DB::table('customer_case_case_comment as customer_comment')
                    ->leftJoin('case_comments as case_comment', 'customer_comment.case_comment_id', '=', 'case_comment.id')
                    ->whereDate('case_comment.created_at', '>=', Carbon::today())
                    ->where('customer_comment.view', "case-edit")
                    ->get()
                    ->pluck("case_comment_id")
                    ->toArray();
                $query->on('client_comment.customer_case_id', '=', 'cc.id');
                if (count($comments)) {
                    $query->whereIn('client_comment.case_comment_id', array_unique($comments));
                } else {
                    $query->whereNull('client_comment.case_comment_id');
                }

            })*/
            ->leftJoin('leads as le', 'le.id', '=', 'c.lead_id')
            ->leftJoin('users as u', 'le.conseiller_id', '=', 'u.id')
            ->leftJoin('users as us', 'le.support_id', '=', 'us.id')
            ->leftJoin('users as ub', 'c.backoffice_id', '=', 'ub.id')
            ->leftJoin('lead_forms as lff', 'lff.id', '=', 'le.influencer')
            ->leftJoin('lead_form_marketing_campaigns as lf', 'lf.id', '=', 'c.campaign_id')
            ->leftJoin('programs as p', 'p.id', '=', 'cc.program_id')
            ->leftJoin('program_steps as s', 's.id', '=', 'cc.program_step_id')
            ->leftJoin('currencies as cur', 'cur.id', '=', 'c.currency_id')
            ->leftJoin(DB::raw('(SELECT `lead_id`, COUNT(`lead_id`) AS `calls` FROM `histo_calls` GROUP BY `lead_id`) hc'), 'hc.lead_id', '=', 'c.lead_id')
            ->leftJoin(DB::raw('(SELECT `customer_case_id`, SUM(`fees`) AS `total_fees` FROM `case_comments` GROUP BY `customer_case_id`) fees'), 'fees.customer_case_id', '=', 'cc.customer_id')
            ->leftJoin('admission_states as ads', 'c.admission_state', '=', 'ads.id')
            ->leftJoin('cities', 'cities.id', '=', 'le.city2')
            // ->select(['c.id as client_id', 'c.firstname', 'c.lastname', 'p.name as pname', 'p.id as program_id', 'c.phone', 'ads.label as admission_state', 'ads.id as admission_state_id']);
            // ->leftJoin(DB::raw('(SELECT case_comment.`customer_case_id`, case_comment.`case_comment_id`, case_comment.`view`, case_comment.`canal`, CAST(MAX(comment.`created_at`) AS DATE) AS creation FROM `customer_case_case_comment` AS case_comment LEFT JOIN `case_comments` AS comment ON comment.`id` = case_comment.`case_comment_id` WHERE CAST(comment.`created_at` AS DATE)=CAST(NOW() AS DATE) AND case_comment.`view`="case-edit" GROUP BY case_comment.`customer_case_id`) comment_client_today'), function($join){
            //     $join->on('comment_client_today.customer_case_id', '=', 'cc.id');
            // })
            // tasks instead of comments
            // ->leftJoin(DB::raw('SELECT tasks.id as today_task'))->where('updated_at', '>=', Carbon::now()->subHours(24)->toDateTimeString()) , function($join){
            //     $join->on('tasks.customer_case_id', '=', 'cc.id');
            // })
            // ->leftJoin('tasks as soumission_task', 'soumission_task.customer_case_id', '=', 'cc.id')

            // ----------
            // ->leftJoin('tasks as soumission_task', function ($query) {
            //     $query->on('soumission_task.customer_case_id', '=', 'cc.id')
            //         ->whereRaw('soumission_task.updated_at IN (select MAX(t2.updated_at) from tasks as t2 join customer_cases as cc2 on cc2.id = t2.customer_case_id group by cc2.id)');
            // })
            // ----------

            // ->leftJoin('tasks as case_task', 'case_task.customer_case_id', '=', 'cc.id')

            // ->leftJoin(DB::raw('(SELECT MAX(tasks.updated_at) updated_at FROM tasks') , function($join){
            //     $join->on('tasks as case_task', 'case_task.customer_case_id', '=', 'cc.id');
            // })

            // -------
            // ->select('cc.*', 'cc.id as case_id', 'c.id as client_id', 'c.firstname', 'c.lastname', 'c.email', 'c.phone', 'c.admis_at as admission_date', 'p.name as pname', 'p.id as pid', 'ads.label as admission_state', 'ads.order as admission_order', 'ads.id as admission_state_id', 's.step_label', 'lf.campaign_name as canal', 'lff.title as influencer', 'ub.name as backoffice', 'u.name as conseiller', 'us.name as support', 'c.processed_at as processed', 'c.soumission_processed_at as soumission_processed', 'c.soumission_processed_message as processed_message', 'c.processed_message as processed_message_client', 'cur.iso as currency')
            ->select('cc.*', 'cc.id as case_id', 'c.id as client_id', 'c.firstname', 'c.lastname', 'c.email', 'c.phone', 'c.admis_at as admission_date', 'p.name as pname', 'p.id as pid', 'ads.label as admission_state', 'ads.order as admission_order', 'ads.id as admission_state_id', 's.step_label', 'lf.campaign_name as canal', 'lff.title as influencer', 'ub.name as backoffice', 'u.name as conseiller', 'us.name as support', 'c.processed_at as processed', 'c.soumission_processed_at as soumission_processed', 'c.soumission_processed_message as processed_message', 'c.processed_message as processed_message_client', 'hc.calls', 'fees.total_fees', 'cur.iso as currency', "cities.city as city2", "le.city as city", "le.country_id as country")
            // ->select('cc.*', "soumission_task.updated_at as g_updated_at", 'soumission_task.co_updated_at as co_update_time', 'soumission_task.bo_updated_at as bo_update_time', 'cc.id as case_id', 'c.id as client_id', 'c.firstname', 'c.lastname', 'c.email', 'c.phone', 'c.admis_at as admission_date', 'p.name as pname', 'p.id as pid', 'ads.label as admission_state', 'ads.order as admission_order', 'ads.id as admission_state_id', 's.step_label', 'lf.campaign_name as canal', 'lff.title as influencer', 'ub.name as backoffice', 'u.name as conseiller', 'us.name as support', 'c.processed_at as processed', 'c.soumission_processed_at as soumission_processed', 'c.soumission_processed_message as processed_message', 'c.processed_message as processed_message_client', 'hc.calls', 'fees.total_fees', 'cur.iso as currency')
            // -------
            ->where(function ($query) use ($request) {
                // search by name
                if (isset($request['customer']) && !empty($request['customer'])) {
                    $query->where(function ($query) use ($request) {

                        $query->where(function ($query) use ($request) {
                            $query->where('c.firstname', 'LIKE', "%" . $request['customer'] . "%");
                        });

                        $query->orWhere(function ($query) use ($request) {
                            $query->where('c.lastname', 'LIKE', "%" . $request['customer'] . "%");
                        });
                        $query->orWhere(function ($query) use ($request) {
                            $query->where(DB::raw("CONCAT(`c`.`firstname`, ' ', `c`.`lastname`)"), 'LIKE', "%" . $request['customer'] . "%");
                        });
                        $query->orWhere(function ($query) use ($request) {
                            $query->where(DB::raw("CONCAT(`c`.`lastname`, ' ', `c`.`firstname`)"), 'LIKE', "%" . $request['customer'] . "%");
                        });
                    });
                }
                // search by phone
                if (isset($request['phone']) && !empty($request['phone'])) {
                    $query->where('c.phone', 'like', '%' . $request['phone'] . '%');
                }
                // search by program
                if (isset($request['program']) && !empty($request['program'])) {
                    $query->whereIn('c.program_id', $request['program']);
                }
                // search by canal
                if (isset($request['campaign']) && !empty($request['campaign'])) {
                    $query->where('c.campaign_id', '=', $request['campaign']);
                }
                // search by referent
                if (isset($request['influencer']) && !empty($request['influencer'])) {
                    $query->where('le.influencer', '=', $request['influencer']);
                }
                // conseiller
                /*if(isset($request['conseiller']) && !empty($request['conseiller'])) {
                    if($request['conseiller'] == 'n/d')
                        $query->whereNull('le.conseiller_id');
                    else
                        $query->where('le.conseiller_id', '=',$request['conseiller']);
                }*/
                if (isset($request['conseiller']) && !empty($request['conseiller'])) {
                    $rc = $request['conseiller'];
                    $query->whereIn('le.conseiller_id', $rc);
                    if (in_array('n/d', $request['conseiller'])) {
                        // $query->whereNull('le.conseiller_id');
                        // $rc = array_merge($request['conseiller'], [null]);
                        $query->orWhereNull('le.conseiller_id');
                    }
                }
                // backoffice
                if (isset($request['backoffice']) && !empty($request['backoffice'])) {
                    $rb = $request['backoffice'];
                    $query->whereIn('c.backoffice_id', $rb);
                    if (in_array('n/d', $request['backoffice'])) {
                        // $query->whereNull('le.conseiller_id');
                        // $rb = array_merge($request['backoffice'], [null]);
                        $query->orWhereNull('c.backoffice_id');
                    }
                }
                if (isset($request['task_soumission']) && !empty($request['task_soumission'])) {
                    $mc = array();
                    $query->where(function ($query) use ($request, $mc) {
                        foreach ($request['task_soumission'] as $tt) {
                            if ($tt == "Admission")
                                continue;
                            elseif ($tt == "Visa&PE")
                                $mc[] = 'note soumission-Visa PE ajouté';
                            else
                                $mc[] = 'note soumission-' . $tt . ' ajouté';
                        }
                        $query->whereIn('c.soumission_processed_message', $mc);
                        if (in_array("Admission", $request['task_soumission'])) {
                            $query->orWhere('c.soumission_processed_message', 'like', 'note soumission-admission-_ ajouté');
                            $query->orWhere('c.soumission_processed_message', 'like', 'admission note ajouté%');
                        }
                    });
                }
                if (isset($request['task_client']) && !empty($request['task_client'])) {
                    $mc = array();
                    $query->where(function ($query) use ($request, $mc) {
                        foreach ($request['task_client'] as $tt) {
                            if ($tt == "Admission")
                                continue;
                            if ($tt == "Visa&PE")
                                $mc[] = 'note soumission-Visa PE ajouté';
                            else
                                $mc[] = 'note soumission-' . $tt . ' ajouté';
                        }
                        $query->whereIn('c.processed_message', $mc);
                        if (in_array("Admission", $request['task_client'])) {
                            $query->orWhere('c.processed_message', 'like', 'note soumission-admission-_ ajouté');
                            $query->orWhere('c.processed_message', 'like', 'admission note ajouté%');
                        }
                    });
                }
                // date
                if (!isset($request['customer']) || empty($request['customer'])) {
                    if (!Auth::user()->isBackoffice()) {
                        if (isset($request['date_start']) && !empty($request['date_start'])) {
                            $start = Carbon::parse($request['date_start']);
                            $query->where('c.soumission_processed_at', '>=', $start->format('Y-m-d') . ' 00:00:00');
                        }
                        if (isset($request['date_end']) && !empty($request['date_end'])) {
                            $end = Carbon::parse($request['date_end'])->addDay();
                            $query->where('c.soumission_processed_at', '<=', $end->format('Y-m-d') . ' 00:00:00');
                        }
                    }
                }
                // office
                if (isset($request['office']) && !empty($request['office'])) {
                    if ($request['office'] == "n/d") {
                        $query->whereNull('le.to_office');
                    } else {
                        $query->where('le.to_office', $request['office']);
                    }
                }
                // support
                if (isset($request['support']) && !empty($request['support'])) {
                    if ($request['support'] == "n/d") {
                        $query->whereNull('le.support_id');
                    } else {
                        $query->where('le.support_id', '=', $request['support']);
                    }
                }
                //
                if (isset($request['status']) && !empty($request['status'])) {
                    if (in_array("n/d", $request['status'])) {
                        $query->whereNull('c.admission_state');
                    }
                    $query->whereIn('c.admission_state', $request['status']);
                }

                if (isset($request["city2"])) {
                    $query->where(function ($query) use ($request) {
                        if ($request["city2"]) {
                            $query->where('le.city2', $request["city2"])->whereNull("le.city");
                        }
                    });
                }

                /*if(isset($request['status']) && !empty($request['status'])) {
                    if($request['status'] == "n/d"){
                        $query->whereNull('c.admission_state');
                    } else {
                        $query->where('c.admission_state', '=',$request['status']);
                    }
                }*/
                $query->whereNotNull('c.admission_state');
                $query->where('c.is_client', 1);
                // 15 && 16 are for visa accepted & refused
                //$query->whereNotIn('c.admission_state', [30, 31]);
                // $query->orWhere(function ($query) {
                //     $query->orWhere('soumission_task.bo_updated_at', '>=', Carbon::now()->subHours(24)->toDateTimeString());
                // });
                // $query->orWhere([
                //     ['soumission_task.co_updated_at', '>=', Carbon::now()->subHours(24)->toDateTimeString()],
                //     ['soumission_task.view', '=', 'cases-list'],
                // ]);
                // $query->orWhere([
                //     ['soumission_task.bo_updated_at', '>=', Carbon::now()->subHours(24)->toDateTimeString()],
                //     ['soumission_task.bo_view', '=', 'soumission-view'],
                // ]);
                // $query->whereNotNull('soumission_task.bo_updated_at');
            });


        // total des frais for each user



        // end total fees

        /*if(Auth::user()->isConseiller()){
            $casesQB = $admissionsQB->where('le.conseiller_id',Auth::id());
        }*/
        if (Auth::user()->isBackoffice()) {
            $admissionsQB = $admissionsQB->where('c.backoffice_id', Auth::id());
        }
        // admission filter
        // admission_programme
        $filter_columns = ['programme', 'nom_etablissement', 'session', 'date_limit_admission'];
        $arrayIds = [];
        $checkCol = false;
        foreach ($filter_columns as $column) {
            if (isset($request['admission_' . $column]) && !empty($request['admission_' . $column])) {
                $checkCol = true;
                // $customer_case->customer->schools()->get();
                /*$programs = OrientationSchool::where($column, $request['admission_' . $column])->get();
                foreach ($programs as $program) {
                    $ids = $program->customers()->select("customers.id")->pluck("id")->toArray();
                    $arrayIds = array_unique(array_merge($ids, $arrayIds), SORT_REGULAR);
                }*/
                // dd($arrayIds);
                $programs = ProgramCourse::where("properties->" . $column . "->value", $request['admission_' . $column])->get();
                foreach ($programs as $program) {
                    $ids = $program->customers()->select("customers.id")->pluck("id")->toArray();
                    $arrayIds = array_unique(array_merge($ids, $arrayIds), SORT_REGULAR);
                    // $arrayIds
                }
            }
        }
        if ($checkCol)
            $admissionsQB = $admissionsQB->orWhereIn('c.id', $arrayIds);
        $ids_in = [];
        $checkCol = false;
        $ids_not_in = [];
        foreach (["Lettre modèle", 'Accusé de réception', 'Reçu', 'Lettre de suivi', "Lettre d'acceptation", "Lettre de refus"] as $index => $doc) {
            if (isset($request['admission_document_' . $index]) && !empty($request['admission_document_' . $index])) {
                // $admissionsQB = $admissionsQB->where(function($query) use ($request, $doc, $index) {
                $checkCol = true;
                $ids = Media::where("model_type", "App\Models\Customer")
                    ->where("collection_name", "LIKE", $doc . "-course-%")
                    ->orWhere("collection_name", "LIKE", $doc . "-admission-%")
                    ->pluck("model_id")
                    ->toArray();
                if ($request['admission_document_' . $index] == 1) {
                    if (count($ids_in)) $ids_in = array_intersect($ids_in, $ids);
                    else $ids_in = array_merge($ids_in, $ids);
                    // $query->orWhereIn('c.id', array_unique($ids));
                } elseif ($request['admission_document_' . $index] == 2) {
                    if (count($ids_not_in)) $ids_not_in = array_intersect($ids_not_in, $ids);
                    else $ids_not_in = array_merge($ids_not_in, $ids);
                }
                /*$admissionsQB->where(function ($query) use ($request, $doc, $index) {

                    if ($request['admission_document_' . $index] == 1)
                        $query->orWhereIn('c.id', $ids);
                    elseif ($request['admission_document_' . $index] == 2)
                        $query->orWhereNotIn('c.id', $ids);
                });*/
            }
        }
        if ($checkCol) {
            // dd($ids_in, $ids_not_in);
            $admissionsQB->whereIn('c.id', array_unique(array_diff($ids_in, $ids_not_in)));
        }
        // filter caq...
        $docs = new \Illuminate\Database\Eloquent\Collection();
        foreach ([9, 22] as $substepId) {
            $substep = \App\Models\ProgramSubStep::find($substepId);
            $docs = $docs->merge($substep->docs()->withPivot('order')->where('customer_owner', 0)->orderBy('order', 'asc')->get());
        }
        $ids_in = [];
        $ids_not_in = [];
        $checkCol = false;
        foreach ($docs as $index => $doc) {
            if (isset($request['soumission_document_' . $index]) && !empty($request['soumission_document_' . $index])) {
                $checkCol = true;
                // $admissionsQB = $admissionsQB->where(function($query) use ($request, $doc, $index) {
                $ids = Media::where("model_type", "App\Models\Customer")
                    ->where("collection_name", $doc->collection_name)
                    ->pluck("model_id")
                    ->toArray();
                // dd($request['soumission_document_' . $index]);
                if ($request['soumission_document_' . $index] == 1) {
                    if (count($ids_in)) $ids_in = array_intersect($ids_in, $ids);
                    else $ids_in = array_merge($ids_in, $ids);
                    // $query->orWhereIn('c.id', array_unique($ids));
                } elseif ($request['soumission_document_' . $index] == 2) {
                    if (count($ids_not_in)) $ids_not_in = array_intersect($ids_not_in, $ids);
                    else $ids_not_in = array_merge($ids_not_in, $ids);
                    // $ids_in = array_diff($ids_in, $ids);
                    // $query->orWhereNotIn('c.id', array_unique($ids));
                }
                /*$admissionsQB->where(function ($query) use ($request, $doc, $index) {

                });*/
            }
        }
        if ($checkCol) {
            // dd($ids_in, $ids_not_in);
            $admissionsQB->whereIn('c.id', array_unique(array_diff($ids_in, $ids_not_in)));
        }
        $column_idx = $request->input('order')[0]["column"];
        $direction = $request->input('order')[0]["dir"];
        $column = $request->input('columns')[$column_idx];
        // dd($request->input('order'), $column);
        // $admissionsQB->groupBy("cc.id");
        $admissionsQB->orderBy('processed', 'desc')->orderBy($column["name"], $direction);
        // dd();
        $actions = [
            ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light admission-show", "icon" => "fas fa-eye"],
            ["action" => "convert", "islink" => false, "title" => "convertir", "available" => (Auth::user()->isAdmin() || Auth::user()->isVerificateur()) ? 1 : 0, "className" => "btn btn-secondary btn-sm soumission-convert", "icon" => "far fa-copyright"],
            // ["action" => "edit", "islink" => true, "title" => "éditer", "available" => 1, "className" => "btn btn-sm btn-light admission-edit", "icon" => "fas fa-edit"],
            // ["action" => "delete", "islink" => false, "title" => "supprimer", "available" => (Auth::user()->isAdmin() || Auth::user()->isManager()) ? 1 : 0, "className" => "btn btn-sm btn-light admission-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables::of($admissionsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    public function convert_soumission($customer)
    {
        $customer = Customer::find($customer);
        if ($customer->admission_state !== null) {
            $customer->admis_at = Carbon::now();
            $customer->admission_state = null;
            $customer->backoffice_id = null;
            $customer->save();
            return "updated";
        } else {
            return "Already updated";
        }
    }
    public function updateState(Request $request, CustomerCase $customer_case)
    {
        //$this->validate($request, ['admission_status' => 'required|exists:admission_states,id']);
        $y = AdmissionState::find($request['admission_status']);
        $state = $y ? $y->id : 1;
        // dd($state, $request['view']);
        // $customer_case->customer->update(array('admission_state' => $state));
        // $customer_case->customer->update(array('admission_state' => $state, 'processed_message' => 'soumission status modifié', 'processed_at' => Carbon::now()));
        $customer_case->customer->update(array('admission_state' => $state, 'soumission_processed_message' => 'soumission status modifié', 'soumission_processed_at' => Carbon::now()));
        return back()->with(['status' => 'Soumission status modifiée avec succés!']);
    }

    public function states(Request $request)
    {
        $data = [];

        if ($request->has('q')) {
            $search = isset($request->q['term']) ? $request->q['term'] : '';

            $data = DB::table('admission_states as status')
                // ->leftJoin('admission_state_groups as group', "group.id", "=", "status.admission_state_group_id")
                ->select("status.id", "status.label", "status.admission_state_group_id")
                ->where(function ($query) use ($search, $request) {
                    $query->where('status.label', 'LIKE', "%$search%");
                    if (isset($request['pid']) && !empty($request['pid'])) {
                        $query->where('status.program_id', $request['pid']);
                    }
                })
                ->get();
        }
        $statusGroupsArray = [];
        foreach ($data as $status) {
            $group_name = AdmissionStateGroup::find($status->admission_state_group_id)->label_fr;
            $statusGroupsArray[$group_name][] = array("id" => $status->id, "name" => $status->label);
        }

        return response()->json($statusGroupsArray);
    }
    // {
    //     $data = [];

    //     if($request->has('q')){
    //         $search = isset($request->q['term']) ? $request->q['term'] : '';
    //         $data =Lead::select("id", DB::raw("CONCAT(firstname, ' ', lastname) AS name"))
    //             ->where(function ($query) use ($search) {

    //                 $query->where('firstname','LIKE',"%$search%");
    //                 $query->orWhere('lastname','LIKE',"%$search%");

    //             })
    //             ->limit(10)
    //             ->get();
    //     }

    //     return response()->json($data);
    // }
}
