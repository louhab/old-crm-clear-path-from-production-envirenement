<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserAddFormRequest;
use App\Http\Requests\User\UserUpdateFormRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Models\CurrentSelectedConseiller;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use Yajra\DataTables\Facades\DataTables;
use App\Rules\PhoneIndicator;

class ConseillerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $usersQB = DB::table('users as u')
            ->join('roles as r', "r.id", "=", "u.role_id")
            ->join('current_selected_conseillers as cu', "cu.conseiller_id", "=", "u.id")
            ->where(function ($query) use ($request) {

                $query->whereIn('u.role_id', [Role::Conseiller, Role::Manager, Role::Support]);
                $query->whereNull('u.deleted_at');
                $query->where('u.status', 1);

                if (isset($request['name']) && !empty($request['name'])) {
                    $name = $request['name'];
                    $query->where('u.name', 'like', "%$name%");
                }

                if (isset($request['email']) && !empty($request['email'])) {
                    $email = $request['email'];
                    $query->where('u.email', 'like', "%$email%");
                }
            })
            ->select(['u.id as user_id', 'u.name', 'u.role_id', 'cu.id as queue_id', 'cu.queue', 'u.email', 'u.phone']);

        $actions = [
            ["action" => "edit", "title" => "Modifier client", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-primary user-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer client", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger user-table-delete", "icon" => "fas fa-trash"],
        ];


        return DataTables::of($usersQB)
            // return DataTables()->eloquent($usersQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateFormRequest $request
     * @param int $id
     * @return UserResource
     */
    public function update(Request $request, $id)
    {
        $user = CurrentSelectedConseiller::findOrFail($id);
        $user->queue = $request['edit-form-queue'];
        $user->save();

        return new UserResource($user);
    }

    public function getConseillers(Request $request)
    {
        $data = [];

        if ($request->has('q')) {
            $search = isset($request->q['term']) ? $request->q['term'] : '';
            //dd($search);
            //dd($search);
            $data = DB::table('users as u')
                ->leftJoin('current_selected_conseillers as cu', "cu.conseiller_id", "=", "u.id")
                ->select("u.id", "u.name")
                ->where(function ($query) use ($search) {

                    $query->whereNotIn("u.id", CurrentSelectedConseiller::pluck("conseiller_id")->toArray());
                    $query->where('u.name', 'LIKE', "%$search%");
                    $query->whereIn('u.role_id', [2, 3, Role::Support]);
                    // $query->where('u.role_id', '=', 3);
                    $query->where('u.status', '=', 1);
                })
                ->get();
        }

        return response()->json($data);
    }

    public function attach(Request $request, $id)
    {

        $users = DB::table("users as u")
            ->join('current_selected_conseillers as cu', "cu.conseiller_id", "=", "u.id")
            ->where(function ($query) use ($id) {

                $query->where('u.id', $id);
                $query->whereIn('u.role_id', [Role::Conseiller, Role::Manager, Role::Support]);
                //$query->where('u.status', '=', 1);
            });
        if ($users->count() == 1) {
            $user = $users->first();
        } elseif ($users->count() == 0) {
            //
            $current_conseiller = CurrentSelectedConseiller::where("status", 1)->first();
            if (!$current_conseiller) {
                $status = 1;
            } else {
                $status = 0;
            }
            if (User::find($id)) {
                $user = CurrentSelectedConseiller::create([
                    "conseiller_id" => $id,
                    "status" => $status
                ]);
            }
        } else $user = null;

        return new UserResource($user);
    }

    public function dettach(User $user)
    {
        $conseiller = CurrentSelectedConseiller::where("conseiller_id", $user->id)->first();
        if (!$conseiller) {
            return response()->json(array(
                'code'      =>  404,
                'message'   =>  "conseiller not found!"
            ), 404);
        }
        if ($conseiller->status == 1) {
            $conseillers_managers = User::whereIn('role_id', [Role::Conseiller, Role::Manager])->where('status', 1)->get()->pluck('id')->toArray();
            $conseillers = CurrentSelectedConseiller::whereIn('conseiller_id', $conseillers_managers)->orderby('id', 'asc')->pluck('conseiller_id')->toArray();

            // $current_conseiller = CurrentSelectedConseiller::where("status", 1)->first()->conseiller_id;
            $index = array_search($conseiller->conseiller_id, $conseillers);

            if ($index == count($conseillers) - 1 || $index === false)
                $next_conseiller = $conseillers[0];
            else
                $next_conseiller = $conseillers[$index + 1];

            CurrentSelectedConseiller::where("status", 1)->first()->update(['status' => 0]);
            CurrentSelectedConseiller::where("conseiller_id", $next_conseiller)->first()->update(['status' => 1]);
        }
        // return $conseiller->delete();
        return $conseiller->delete();
    }
}
