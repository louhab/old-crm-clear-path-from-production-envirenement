<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\DocumentTypeResource;
use App\Models\CustomerField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class CustomerFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $documentsQB = DB::table('customer_fields as cf')
            ->leftJoin('customer_field_groups as group', 'group.id', '=', 'cf.customer_field_group_id')
            // ->leftJoin('customer_field_information_sheet as field_sheet', 'field_sheet.customer_field_id', '=', 'cf.id')
            // ->leftJoin('media_sheets as clear', 'group.id', '=', 'dt.document_type_group_id')
            ->where(function ($query) use ($request) {
                if (isset($request['name_en']) && !empty($request['name_en'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('cf.field_name', 'LIKE', "%" . $request['name_en'] . "%");
                    });
                }
                /*if(isset($request['name_fr']) && !empty($request['name_fr'])) {
                    $query->where(function($query) use ($request) {
                        $query->where('dt.name_fr', 'LIKE', "%".$request['name_fr']."%");
                    });
                }*/
                if (isset($request['group']) && !empty($request['group'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('cf.customer_field_group_id', $request['group']);
                    });
                }
            })
            ->select(['cf.*', 'group.name_fr as group_name']); //->orderBy("dt.id", "desc");
        if (isset($request['sheet']) && !empty($request['sheet'])) {
            $documentsQB->leftJoin('customer_field_information_sheet as cs', 'cs.customer_field_id', '=', 'cf.id')
                ->where(function ($query) use ($request) {
                    $query->where('cs.information_sheet_id', $request['sheet']);
                    if (isset($request['program']) && !empty($request['program'])) {
                        $query->where(function ($query) use ($request) {
                            $query->where('cs.program_id', $request['program']);
                        });
                    }
                });
        }

        $actions = [
            // ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light document-type-show", "icon" => "fas fa-eye"],
            /*["action" => "copy", "title" => "Copier Lien", "available" => 1, "className" => "btn btn-info leadsforms-table-copy", "icon" => "fas fa-copy"],*/
            ["action" => "edit", "islink" => false, "title" => "Editer Formulaire", "available" => 1, "className" => "btn btn-primary fields-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "islink" => false, "title" => "Supprimer Formulaire", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger fields-table-delete", "icon" => "fas fa-trash"],
        ];

        // return DataTables()->eloquent($leadformsQB)
        //     ->addColumn('actions', $actions)
        //     ->toJson();

        return DataTables::of($documentsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return DocumentTypeResource
     */
    public function store(Request $request)
    {
        $rules = [
            'add-form-title-fr' => 'required',
            'add-form-group' => 'required',
            'add-form-template' => 'required|in:text,date,tel,number,email,radio,select,textarea',
            'add-form-model' => Rule::requiredIf(function () use ($request) {
                return $request['add-form-template'] == "select";
            }),
            'add-form-model-label' => Rule::requiredIf(function () use ($request) {
                return $request['add-form-template'] == "select";
            }),
            'add-form-model-id' => Rule::requiredIf(function () use ($request) {
                return $request['add-form-template'] == "select";
            }),
        ];

        $this->validate($request, $rules);

        return CustomerField::create([
            'customer_field_group_id' => $request['add-form-group'],
            'field_name' => $request['add-form-title-fr'],
            'template' => $request['add-form-template'],
            'select_model' => $request['add-form-template'] == "select" ? $request['add-form-model'] : null,
            'select_model_label' => $request['add-form-template'] == "select" ? $request['add-form-model-label'] : null,
            'select_model_id' => $request['add-form-template'] == "select" ? $request['add-form-model-id'] : null,
        ]);
    }

    /**
     * edit resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return DocumentTypeResource
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'edit-form-title-fr' => 'required',
            'edit-form-group' => 'required',
            'edit-form-template' => 'required|in:text,date,tel,number,email,radio,select,textarea',
            'edit-form-model' => Rule::requiredIf(function () use ($request) {
                return $request['edit-form-template'] == "select";
            }),
            'edit-form-model-label' => Rule::requiredIf(function () use ($request) {
                return $request['edit-form-template'] == "select";
            }),
            'edit-form-model-id' => Rule::requiredIf(function () use ($request) {
                return $request['edit-form-template'] == "select";
            }),
        ];

        $this->validate($request, $rules);
        $field = CustomerField::findOrFail($id);

        $field->customer_field_group_id = $request['edit-form-group'];
        $field->field_name = $request['edit-form-title-fr'];
        $field->template = $request['edit-form-template'];

        // $document->customer_owner = $request['edit-form-campaign'];
        if ($request['edit-form-template'] == "select") {
            $field->select_model = $request['edit-form-model'];
            $field->select_model_label = $request['edit-form-model-label'];
            $field->select_model_id = $request['edit-form-model-id'];
        }

        $field->save();
        return response()->json($field->toArray());
    }

    public function destroy(CustomerField $field)
    {
        return $field->delete();
    }
}
