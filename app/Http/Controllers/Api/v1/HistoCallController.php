<?php

namespace App\Http\Controllers\Api\v1;

use App\Events\LeadProspectionUpdated;
use App\Http\Controllers\Controller;
use App\Http\Requests\HistoCall\HistoCallAddFormRequest;
use App\Http\Requests\HistoCall\HistoCallUpdateFormRequest;
use App\Http\Resources\HistoCallResource;
use App\Models\Customer;
use App\Models\HistoCall;
use App\Models\Lead;
use App\Models\LeadStateLog;
use App\Models\LeadStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class HistoCallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        // dd("????");
        $histocallsQB = DB::table('histo_calls as h')
            ->leftJoin('customers as c', 'c.id', '=', 'h.customer_id')
            ->leftJoin('leads as l', 'l.id', '=', 'h.lead_id')
            ->leftJoin('users as u', 'u.id', '=', 'h.user_id')
            ->leftJoin('lead_statuses as ls', 'h.lead_state_id', '=', 'ls.id')
            ->leftJoin('lead_statuses as lsb', 'h.state_before', '=', 'lsb.id')
            ->select('h.*', 'lsb.status_label_fr as status_label_before', 'ls.status_label_fr as status_label', 'c.firstname as cfirstname', 'c.lastname as clastname', 'l.firstname as lfirstname', 'l.lastname as llastname', 'u.name as username')
            ->where(function ($query) use ($request) {
                if (isset($request['name']) && !empty($request['name'])) {
                    $query->where('c.firstname', 'like', '%' . $request['name'] . '%')
                        ->orWhere('c.lastname', 'like', '%' . $request['name'] . '%')
                        ->orwhere('l.firstname', 'like', '%' . $request['name'] . '%')
                        ->orWhere('l.lastname', 'like', '%' . $request['name'] . '%');
                }
                if ((isset($request['agent']) && !empty($request['agent'])) || Auth::user()->isConseiller()) {
                    $agent = $request['agent'];
                    if (Auth::user()->isConseiller()) {
                        $agent = Auth::id();
                    }
                    $query->where('h.user_id', $agent);
                }
                if (isset($request['status']) && !empty($request['status'])) {
                    if ($request['status'] == "n/d") {
                        $query->whereNull('h.lead_state_id');
                    } elseif ($request['status'] == "processed") {
                        $query->whereNotNull('h.lead_state_id');
                    } else {
                        $query->where('h.lead_state_id', $request['status']);
                    }
                }
                if (isset($request['state_before']) && !empty($request['state_before'])) {
                    if ($request['state_before'] == "n/d") {
                        $query->whereNull('h.state_before');
                    } elseif ($request['state_before'] == "processed") {
                        $query->whereNotNull('h.state_before');
                    } else {
                        $query->where('h.state_before', $request['state_before']);
                    }
                }
                if (isset($request['date_start']) && !empty($request['date_start'])) {
                    $start = Carbon::parse($request['date_start']);
                    $end = Carbon::parse($request['date_end']);
                    // ->addDay();
                    $query->where('h.created_at', '>=', $start->format('Y-m-d') . ' 00:00:00');
                    //end
                    $query->where('h.created_at', '<=', $end->format('Y-m-d') . ' 23:59:59');
                } else {
                    $query->where('h.created_at', '>', now()->subDays(1)->endOfDay());
                }
            });
        // $histocallsQB->orderBy("h.created_at", "desc");

        if (Auth::user()->isConseiller()) {
            // $histocallsQB = $histocallsQB->where('l.conseiller_id', Auth::id());
        }

        $column_idx = $request->input('order')[0]["column"];
        $direction = $request->input('order')[0]["dir"];
        $column = $request->input('columns')[$column_idx];
        // dd($request->input('order'), $column);
        $histocallsQB->orderBy($column["name"], $direction);

        // dd($histocallsQB->toSql());

        $actions = [
            ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light lead-show", "icon" => "fas fa-eye"],
            ["action" => "edit", "title" => "Modifier historique", "available" => 1, "className" => "btn btn-primary histocall-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer historique", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger histocall-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables()->of($histocallsQB)
            ->addColumn('actions', $actions)
            ->with('total', function () use ($histocallsQB) {
                return $histocallsQB->sum('h.duration');
            })
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return HistoCallResource
     */
    public function store(HistoCallAddFormRequest $request)
    {
        if (!isset($request['add-form-call_dt']) || empty($request['add-form-call_dt'])) {
            $call_dt = Carbon::now()->toDateTimeString();
        }
        $lead = Lead::find($request['add-form-lead_id']);
        $old_state = $lead->lead_status_id;
        if (array_key_exists('lead_status', $request->all()) && ($request['lead_status'] != 'No_Chnage') && ($request['lead_status'] != 'Rappel')) {
            $lead->lead_status_id = $request['lead_status'];
            $lead->save();
            if ($old_state != $request['lead_status']) {
                LeadStateLog::create([
                    'user_id' => auth()->user()->id,
                    'lead_id' => $lead->id,
                    'state_id' => $lead->lead_status_id,
                ]);
            }
        } elseif (array_key_exists('lead_state_call', $request->all()) && ($request['lead_state_call'] != 'No_Chnage') && ($request['lead_state_call'] != 'Rappel')) {
            $lead->lead_status_id = $request['lead_state_call'];
            $lead->save();
            if ($old_state != $request['lead_state_call']) {
                LeadStateLog::create([
                    'user_id' => auth()->user()->id,
                    'lead_id' => $lead->id,
                    'state_id' => $lead->lead_status_id,
                ]);
            }
        }

        if (array_key_exists('add-form-call_city', $request->all())) {
            $customer = Customer::where('lead_id', $lead->id);
            if ($customer) {
                $customer->update(["city" => $request['add-form-call_city']]);
            } else {
                $lead->city = $request['add-form-call_city'];
                $lead->save();
            }
        }
        /*if ($old_state == $lead->lead_status_id) {
            // $old_state
            LeadStateLog::where('lead_id', $lead->id)
        }*/
        /*$logs = LeadStateLog::where('lead_id', $lead->id)->orderBy("id", "DESC")->get();
        if (count($logs) > 1) {
            $logs->shift();
            $old_state = $logs->first()->state_id;
        } else $old_state = null;*/
        $call = HistoCall::create([
            "call_dt" => $call_dt,
            "duration" => $request['add-form-duration'],
            "call_descr" => $request['add-form-call_descr'],
            "lead_id" => $request['add-form-lead_id'],
            "lead_state_id" => $lead->lead_status_id,
            "state_before" => $old_state,
            "customer_id" => $request['add-form-customer_id'],
            "user_id" => Auth::id()
        ]);

        $lead->update(array('processed_message' => 'Appel effectuée', 'processed_at' => Carbon::now()));
        /*if (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'lead-view') {
            LeadProspectionUpdated::dispatch($lead, 'Appel effectuée');
        }*/
        return new HistoCallResource($call);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $histocall = HistoCall::where('lead_id', $id)->get();
        $histocall = $histocall->count();
        return $histocall;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return HistoCallResource
     */
    public function update(HistoCallUpdateFormRequest $request, $id)
    {
        $histocall = HistoCall::findOrFail($id);
        $histocall->call_dt = $request['edit-form-call_dt'];
        $histocall->duration = $request['edit-form-duration'];
        $histocall->call_descr = $request['edit-form-call_descr'];
        $histocall->lead_id = $request['edit-form-lead_id'];
        $histocall->customer_id = $request['edit-form-customer_id'];
        $histocall->user_id = Auth::id();
        $histocall->save();

        return new HistoCallResource($histocall);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  HistoCall $lead
     * @return int
     * @throws \Exception
     */
    public function destroy(HistoCall $histocall)
    {
        return $histocall->delete();
    }
}
