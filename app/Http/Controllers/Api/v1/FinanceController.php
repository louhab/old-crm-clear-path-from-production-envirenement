<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Finance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FinanceController extends Controller
{
    public $payments_stats;
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        // dd($request->all());
        $financeQB = DB::table('program_payment_customer as f')
            ->leftJoin('customers as c', 'c.id', '=', 'f.customer_id')
            ->leftJoin('customer_cases as cs', 'cs.id', '=', 'f.customer_id')
            ->leftJoin('leads as l', 'l.id', '=', 'c.lead_id')
            ->leftJoin('currencies as d', 'd.id', '=', 'f.currency_id')
            ->leftJoin('program_payments as pp', 'pp.id', '=', 'f.program_payment_id')
            ->leftJoin('payment_methods as pm', 'pm.id', '=', 'f.payment_method')
            ->leftJoin('users as us', 'us.id', '=', 'f.conseiller_id')
            ->leftJoin('admission_states as ads', 'c.admission_state', '=', 'ads.id')
            ->leftJoin('lead_statuses as ls', 'l.lead_status_id', '=', 'ls.id')
            ->select('f.*', 'd.iso as devise', 'c.firstname as firstname', 'l.id as lead_id', 'c.lastname as lastname', 'l.lead_status_id', 'ls.status_label_fr as status_label', 'pp.name_fr', 'pm.method_label_fr', "f.status as is_verified", "us.name as adviser", "c.is_client", "ads.id as admission_state_id", "ads.label as admission_label")
            ->where(function ($query) use ($request) {
                if (isset($request['name']) && !empty($request['name'])) {
                    $query->where('c.firstname', 'like', '%' . $request['name'] . '%')
                        ->orWhere('c.lastname', 'like', '%' . $request['name'] . '%')
                        ->orwhere('l.firstname', 'like', '%' . $request['name'] . '%')
                        ->orWhere('l.lastname', 'like', '%' . $request['name'] . '%');
                }
                if ((isset($request['agent']) && !empty($request['agent']))) {
                    $agent = $request['agent'];
                    $query->where('l.conseiller_id', $agent);
                }
                // @new
                if (isset($request['conseiller']) && !empty($request['conseiller'])) {
                    if ($request['conseiller'] == "n/d") {
                        $query->whereNull('l.conseiller_id');
                    } else {
                        $query->whereIn('l.conseiller_id', $request['conseiller']);
                    }
                }

                if (isset($request['support']) && !empty($request['support'])) {
                    $key = array_search('n/d', $request['support']);
                    if ($key !== false) {
                        $query->whereNull('l.support_id');
                        if (count($request['support']) > 1) {
                            $query->orWhereIn('l.support_id', $request['support']);
                        }
                    } else {
                        $query->whereIn('l.support_id', $request['support']);
                    }
                }

                if (isset($request['status']) && !empty($request['status'])) {
                    if ($request['status'] == "n/d") {
                        $query->whereNull('l.lead_status_id');
                    } elseif ($request['status'] == "processed") {
                        $query->whereNotNull('l.lead_status_id');
                    } else {
                        $query->where('l.lead_status_id', $request['status']);
                    }
                }

                if (isset($request['meeting']) && !empty($request['meeting'])) {
                    $query->where('pp.id', $request['meeting']);
                }

                if (isset($request['date_start']) && !empty($request['date_start'])) {
                    //dd($request['date_start']);
                    $start = Carbon::parse($request['date_start']);
                    $end = Carbon::parse($request['date_end'])->addDay();
                    $query->where('f.performed_at', '>=', $start->format('Y-m-d') . ' 00:00:00');
                    //end
                    $query->where('f.performed_at', '<=', $end->format('Y-m-d') . ' 00:00:00');
                } else {
                    $query->where('f.performed_at', '>', now()->subDays(60)->endOfDay());
                }
                // method-select
                if ((isset($request['method-select']) && !empty($request['method-select']))) {
                    $query->where('f.payment_method', $request['method-select']);
                }
                // check-select
                if ((isset($request['check-select']) && !empty($request['check-select']))) {
                    // dd("Okey this is ", $request['check-select']);
                    if ($request['check-select'] == 1) {
                        $query->where('f.status', 1);
                    } else {
                        $query->whereNull('f.status');
                    }
                }
                // end new select
            });

        return DataTables()->of($financeQB)
            // ->addColumn("stats", $payments_stats)
            ->toJson();
    }

    public function stats(Request $request)
    {
        $payments_stats = DB::table('program_payment_customer as pc')
            ->selectRaw('SUM(pc.amount) AS total_amount_payment, COUNT(pc.id) as total_payments, currencies.iso')
            ->leftjoin("currencies", "currencies.id", "=", "pc.currency_id")
            ->leftJoin('customers as c', 'c.id', '=', 'pc.customer_id')
            ->leftJoin('customer_cases as cs', 'cs.id', '=', 'c.id')
            ->leftJoin('leads as l', 'l.id', '=', 'c.lead_id')
            ->leftJoin('program_payments as pp', 'pp.id', '=', 'pc.program_payment_id')
            ->leftJoin('users as us', 'us.id', '=', 'pc.conseiller_id')
            ->leftJoin('lead_statuses as ls', 'l.lead_status_id', '=', 'ls.id')
            ->where(function ($query) use ($request) {
                if (isset($request['name']) && !empty($request['name'])) {
                    $query->where('c.firstname', 'like', '%' . $request['name'] . '%')
                        ->orWhere('c.lastname', 'like', '%' . $request['name'] . '%')
                        ->orwhere('l.firstname', 'like', '%' . $request['name'] . '%')
                        ->orWhere('l.lastname', 'like', '%' . $request['name'] . '%');
                }
                // method-select
                if ((isset($request['method-select']) && !empty($request['method-select']))) {
                    $query->where('pc.payment_method', $request['method-select']);
                }
                // check-select
                if ((isset($request['check-select']) && !empty($request['check-select']))) {
                    // dd("Okey this is ", $request['check-select']);
                    if ($request['check-select'] == 1) {
                        $query->where('pc.status', 1);
                    } else {
                        $query->whereNull('pc.status');
                    }
                }

                if (isset($request['meeting']) && !empty($request['meeting'])) {
                    $query->where('pp.id', $request['meeting']);
                }

                // end new select
                if ((isset($request['agent']) && !empty($request['agent']))) {
                    $agent = $request['agent'];
                    $query->where('l.conseiller_id', $agent);
                }

                // new
                if (isset($request['conseiller']) && !empty($request['conseiller'])) {
                    if ($request['conseiller'] == "n/d") {
                        $query->whereNull('l.conseiller_id');
                    } else {
                        $query->whereIn('l.conseiller_id', $request['conseiller']);
                    }
                }

                if (isset($request['support']) && !empty($request['support'])) {
                    $key = array_search('n/d', $request['support']);
                    if ($key !== false) {
                        $query->whereNull('l.support_id');
                        if (count($request['support']) > 1) {
                            $query->orWhereIn('l.support_id', $request['support']);
                        }
                    } else {
                        $query->whereIn('l.support_id', $request['support']);
                    }
                }

                if (isset($request['status']) && !empty($request['status'])) {
                    if ($request['status'] == "n/d") {
                        $query->whereNull('l.lead_status_id');
                    } elseif ($request['status'] == "processed") {
                        $query->whereNotNull('l.lead_status_id');
                    } else {
                        $query->where('l.lead_status_id', $request['status']);
                    }
                }

                if (isset($request['date_start']) && !empty($request['date_start'])) {
                    $start = Carbon::parse($request['date_start']);
                    $end = Carbon::parse($request['date_end'])->addDay();
                    $query->where('pc.performed_at', '>=', $start->format('Y-m-d') . ' 00:00:00');
                    $query->where('pc.performed_at', '<=', $end->format('Y-m-d') . ' 00:00:00');
                }
            })
            ->groupBy("pc.currency_id")
            ->get();

        return response()->json($payments_stats);
    }

    public function verify($payment_id)
    {
        $payment = DB::table('program_payment_customer')->where("id", $payment_id)->update(['status' => 1]);
        if (!$payment) {
            return response('failed', 404)->header('Content-Type', 'text/plain');
        }
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
