<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;
use App\Models\CustomerCase;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, CustomerCase $customer_case)
    {
        Task::create([
            'user_id' => Auth::id(),
            'customer_case_id' => $customer_case->id,
            'program_step_id' => $customer_case->program_step_id,
            // 'note' => $request['note'],
            'canal' => $request['canal'],
            'view' => $request['view'],
            "co_updated_at" => Carbon::now()
        ]);

        // updating the status
        $canal = $request['canal'];

        $processed_message = null;

        if (str_starts_with($canal, "admission-") || str_starts_with($canal, "course-")) {
            if ((DB::table('tasks')->selectRaw('tasks.id')->where([["canal", "=", $canal], ['view', '=', 'case-edit']])->count() == 0) && ($request['view'] == "case-edit")) {
                $customer_case->customer->update(["admission_state" => 1]);
            } elseif ($request['view'] == "soumission-view") {
                $customer_case->customer->update(["admission_state" => 6]);
            }

            if ($customer_case->customer->admission_state == 2) {
                $customer_case->customer->update(["admission_state" => 6]);
            }

            $processed_message = "admission note ajouté(" . $canal . ")";
        } elseif (str_starts_with($canal, "soumission-")) {

            $tasks = ["soumission-CAQ", "soumission-VISA PE", "soumission-WES", "soumission-PROFIL", "soumission-VISA"];
            $states = [[7, 8, 9], [12, 13, 14], [17, 18, 19], [22, 23, 24], [27, 28, 29]];
            $index = array_search($canal, $tasks);
            if ($index !== false) {
                if ((DB::table('tasks')->selectRaw('tasks.id')->where([["canal", "=", $canal], ['view', '=', 'case-edit']])->count() == 0) && ($request['view'] == "case-edit")) {
                    $customer_case->customer->update(["admission_state" => $states[$index][0]]);
                } elseif ($request['view'] == "soumission-view") {
                    $customer_case->customer->update(["admission_state" => $states[$index][1]]);
                }
                if ($customer_case->customer->admission_state == $states[$index][2]) {
                    $customer_case->customer->update(["admission_state" => $states[$index][1]]);
                }
            }
            if (str_starts_with($canal, "soumission-admission-")) {
                if ((DB::table('tasks')->selectRaw('tasks.id')->where([["canal", "=", $canal], ['view', '=', 'case-edit']])->count() == 0) && ($request['view'] == "case-edit")) {
                    $customer_case->customer->update(["admission_state" => 1]);
                } elseif ($request['view'] == "soumission-view") {
                    $customer_case->customer->update(["admission_state" => 6]);
                }
                if ($customer_case->customer->admission_state == 2) {
                    $customer_case->customer->update(["admission_state" => 6]);
                }
            }

            $tasks = ["soumission-Prise en charge", "soumission-Imm5645", "soumission-Imm5476", "soumission-CPG", "soumission-Facture scolaire"];
            if (in_array($canal, $tasks)) {
                if ((DB::table('tasks')->selectRaw('tasks.id')->where([["canal", "=", $canal], ['view', '=', 'case-edit']])->count() == 0) && ($request['view'] == "case-edit")) {
                    $customer_case->customer->update(["admission_state" => 12]);
                } elseif ($request['view'] == "soumission-view") {
                    $customer_case->customer->update(["admission_state" => 13]);
                }
                if ($customer_case->customer->admission_state == 14) {
                    $customer_case->customer->update(["admission_state" => 13]);
                }
            }
            if ($canal == "soumission-Demande de note" && (Auth::user()->isAdmin() || Auth::user()->isVerificateur() || Auth::user()->isManager() || Auth::user()->isConseiller())) {
                if ($customer_case->customer->is_client == 0)
                    $customer_case->customer->update(["is_client" => 1, "admission_state" => 1]);
                elseif (is_null($customer_case->customer->admission_state))
                    $customer_case->customer->update(["admission_state" => 1]);
            }
            $processed_message = "note " . $canal . " ajouté";
        }
        if (!is_null($processed_message)) {
            if ($request['view'] == "soumission-view")
                $customer_case->customer->update(array('soumission_processed_message' => $processed_message, 'soumission_processed_at' => Carbon::now()));
            if ($request['view'] == "case-edit")
                $customer_case->customer->update(array('processed_message' => $processed_message, 'processed_at' => Carbon::now()));
        }

        return back()->with(['status' => 'tache ajouté avec succés!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);
        $customer_case = CustomerCase::find($request["customer"]);
        $task->is_done = in_array($request["note"], ["C'est fait", "Rejet fiche", "Rejet document"]) ? 1 : 0;
        $task->is_relaunched = in_array($request["note"], ["Relance", "Refresh"]) ? 1 : 0;
        $task->note = $request["note"];

        if (!Auth::user()->isBackoffice()) $task->co_updated_at = Carbon::now();
        if (Auth::user()->isBackoffice()) $task->bo_updated_at = Carbon::now();
        if (Auth::user()->isBackoffice()) $task->bo_view = $request['view'];
        if (!Auth::user()->isBackoffice()) $task->view = $request['view'];

        $task->save();

        // updating the status
        $canal = $request['canal'];

        $processed_message = null;

        if (str_starts_with($canal, "admission-") || str_starts_with($canal, "course-")) {
            if ((DB::table('tasks')->selectRaw('tasks.id')->where([["canal", "=", $canal], ['view', '=', 'case-edit']])->count() == 0) && ($request['view'] == "case-edit")) {
                $customer_case->customer->update(["admission_state" => 1]);
            } elseif ($request['view'] == "soumission-view") {
                $customer_case->customer->update(["admission_state" => 6]);
            }

            if ($customer_case->customer->admission_state == 2) {
                $customer_case->customer->update(["admission_state" => 6]);
            }

            $processed_message = "admission note ajouté(" . $canal . ")";
        } elseif (str_starts_with($canal, "soumission-")) {

            $tasks = ["soumission-CAQ", "soumission-VISA PE", "soumission-WES", "soumission-PROFIL", "soumission-VISA"];
            $states = [[7, 8, 9], [12, 13, 14], [17, 18, 19], [22, 23, 24], [27, 28, 29]];
            $index = array_search($canal, $tasks);
            if ($index !== false) {
                if ((DB::table('tasks')->selectRaw('tasks.id')->where([["canal", "=", $canal], ['view', '=', 'case-edit']])->count() == 0) && ($request['view'] == "case-edit")) {
                    $customer_case->customer->update(["admission_state" => $states[$index][0]]);
                } elseif ($request['view'] == "soumission-view") {
                    $customer_case->customer->update(["admission_state" => $states[$index][1]]);
                }
                if ($customer_case->customer->admission_state == $states[$index][2]) {
                    $customer_case->customer->update(["admission_state" => $states[$index][1]]);
                }
            }
            if (str_starts_with($canal, "soumission-admission-")) {
                if ((DB::table('tasks')->selectRaw('tasks.id')->where([["canal", "=", $canal], ['view', '=', 'case-edit']])->count() == 0) && ($request['view'] == "case-edit")) {
                    $customer_case->customer->update(["admission_state" => 1]);
                } elseif ($request['view'] == "soumission-view") {
                    $customer_case->customer->update(["admission_state" => 6]);
                }
                if ($customer_case->customer->admission_state == 2) {
                    $customer_case->customer->update(["admission_state" => 6]);
                }
            }

            $tasks = ["soumission-Prise en charge", "soumission-Imm5645", "soumission-Imm5476", "soumission-CPG", "soumission-Facture scolaire"];
            if (in_array($canal, $tasks)) {
                if ((DB::table('tasks')->selectRaw('tasks.id')->where([["canal", "=", $canal], ['view', '=', 'case-edit']])->count() == 0) && ($request['view'] == "case-edit")) {
                    $customer_case->customer->update(["admission_state" => 12]);
                } elseif ($request['view'] == "soumission-view") {
                    $customer_case->customer->update(["admission_state" => 13]);
                }
                if ($customer_case->customer->admission_state == 14) {
                    $customer_case->customer->update(["admission_state" => 13]);
                }
            }
            if ($canal == "soumission-Demande de note" && (Auth::user()->isAdmin() || Auth::user()->isManager() || Auth::user()->isConseiller())) {
                if ($customer_case->customer->is_client == 0)
                    $customer_case->customer->update(["is_client" => 1, "admission_state" => 1]);
                elseif (is_null($customer_case->customer->admission_state))
                    $customer_case->customer->update(["admission_state" => 1]);
            }
            $processed_message = "note " . $canal . " ajouté";
        }
        if (!is_null($processed_message)) {
            if ($request['view'] == "soumission-view")
                $customer_case->customer->update(array('soumission_processed_message' => $processed_message, 'soumission_processed_at' => Carbon::now()));
            if ($request['view'] == "case-edit")
                $customer_case->customer->update(array('processed_message' => $processed_message, 'processed_at' => Carbon::now()));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Task::find($id)->delete();
    }
}
