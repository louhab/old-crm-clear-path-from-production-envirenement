<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Country\CountryAddFormRequest;
use App\Http\Requests\Country\CountryUpdateFormRequest;
use App\Http\Resources\CountryResource;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $countriesQB = DB::table('countries')
            ->select('*');

        if (isset($request['name']) && !empty($request['name'])) {
            $countriesQB->where('name_en_gb', 'like', '%' . $request['name'] . '%')
                ->orWhere('name_fr_fr', 'like', '%' . $request['name'] . '%');
        }

        $actions = [
            ["action" => "edit", "title" => "Modifier pays", "available" => 1, "className" => "btn btn-primary country-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "title" => "Supprimer pays", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger country-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables()->of($countriesQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return CountryResource
     */
    public function store(CountryAddFormRequest $request)
    {
        return new CountryResource(Country::create([
            "code" => $request['add-form-code'],
            "alpha2" => $request['add-form-alpha2'],
            "alpha3" => $request['add-form-alpha3'],
            "name_fr_fr" => $request['add-form-name_fr_fr'],
            "name_en_gb" => $request['add-form-name_en_gb'],
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return CountryResource
     */
    public function update(CountryUpdateFormRequest $request, $id)
    {
        $country = Country::findOrFail($id);
        $country->code = $request['edit-form-code'];
        $country->name_fr_fr = $request['edit-form-name_fr_fr'];
        $country->name_en_gb = $request['edit-form-name_en_gb'];
        $country->alpha2 = $request['edit-form-alpha2'];
        $country->alpha3 = $request['edit-form-alpha3'];
        $country->save();

        return new CountryResource($country);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountries(Request $request)
    {
        $data = [];

        if ($request->has('q')) {
            $search = isset($request->q['term']) ? $request->q['term'] : '';
            $data = Country::select("id", 'name_fr_fr as name')
                ->where('name_fr_fr', 'LIKE', "%$search%")
                ->get();
        }

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Country $country
     * @return int
     * @throws \Exception
     */
    public function destroy(Country $country)
    {
        return $country->delete();
    }
}
