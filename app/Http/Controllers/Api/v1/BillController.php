<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\BillResource;
use App\Models\Bill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $bills = Bill::with(['user', 'customer', 'services'])
            ->where(function ($query) use ($request) {
                if (isset($request['cid']) && !empty($request['cid'])) {
                    $query->where('customer_id', $request['cid']);
                }
            });

        $actions = [
            ["action" => "edit", "title" => "Modifier facture", "available" => 1, "className" => "btn btn-primary bill-table-edit", "icon" => "fas fa-edit"],
            ["action" => "download", "title" => "Télécharger facture", "available" => 1, "downloadable" => 1, "className" => "btn btn-info", "icon" => "fas fa-download"],
            ["action" => "delete", "title" => "Supprimer facture", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger bill-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables()->eloquent($bills)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return BillResource
     */
    public function store(Request $request)
    {

        //TODO add program/customer validation

        $services = $request['services'];

        $bill = NULL;

        DB::transaction(function () use ($request, $services, &$bill) {

            $billArr = [
                'user_id' => Auth::id(),
                'customer_id' => $request['cid']
            ];

            if ($request->has('ap')) $billArr['applied_price'] = $request['ap'];

            $bill = Bill::create($billArr);

            $servicesToAttach = [];

            foreach ($services as $service) {
                $servicesToAttach[$service[0]] = [
                    'billed_price' => $service[1],
                    'qte' => $service[2]
                ];
            }

            $bill->services()->sync($servicesToAttach);
        });

        return new BillResource($bill);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Bill $bill
     * @return BillResource
     */
    public function update(Request $request, Bill $bill)
    {
        $services = $request['services'];

        DB::transaction(function () use ($request, $services, $bill) {



            if ($request->has('ap')) {
                $bill->applied_price = $request['ap'];
                $bill->save();
            }


            $servicesToAttach = [];

            foreach ($services as $service) {
                $servicesToAttach[$service[0]] = [
                    'billed_price' => $service[1],
                    'qte' => $service[2]
                ];
            }

            $bill->services()->sync($servicesToAttach);
        });

        return new BillResource($bill);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Bill $bill
     * @return int
     */
    public function destroy(Bill $bill)
    {
        return $bill->delete();
    }
}
