<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerCase\CustomerCaseUpdateFormRequest;
use App\Models\Calendrier;
use App\Models\CaseComment;
use App\Models\Customer;
use App\Models\CustomerCase;
use App\Models\HistoCall;
use App\Models\LeadStateLog;
use App\Models\LeadStatus;
use App\Models\ProgramStep;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Cities;

use Illuminate\Validation\Rule;

use App\Rules\PhoneIndicator;

class CaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $id = null)
    {
        $casesQB = DB::table('customer_cases as cc')
            ->leftJoin('customers as c', 'c.id', '=', 'cc.customer_id')
            ->leftJoin('program_payment_customer as pc', 'pc.customer_id', '=', 'cc.id')
            ->leftJoin('program_payments as pp', 'pp.id', '=', 'pc.program_payment_id')
            /*->leftJoin('customer_case_case_comment as comment', function($query) {
                $comments = DB::table('customer_case_case_comment as customer_comment')
                    ->leftJoin('case_comments as case_comment', 'customer_comment.case_comment_id', '=', 'case_comment.id')
                    ->whereDate('case_comment.created_at', '>=', Carbon::today())
                    ->where('customer_comment.view', "soumission-view")
                    ->get()
                    ->pluck("case_comment_id")
                    ->toArray();
                $query->on('comment.customer_case_id', '=', 'cc.id');
                if (count($comments)) {
                    $query->whereIn('comment.case_comment_id', array_unique($comments));
                } else {
                    $query->whereNull('comment.case_comment_id');
                }
            })
            ->leftJoin('customer_case_case_comment as client_comment', function($query) {
                $comments = DB::table('customer_case_case_comment as customer_comment')
                    ->leftJoin('case_comments as case_comment', 'customer_comment.case_comment_id', '=', 'case_comment.id')
                    ->whereDate('case_comment.created_at', '>=', Carbon::today())
                    ->where('customer_comment.view', "case-edit")
                    ->get()
                    ->pluck("case_comment_id")
                    ->toArray();
                $query->on('client_comment.customer_case_id', '=', 'cc.id');
                if (count($comments)) {
                    $query->whereIn('client_comment.case_comment_id', array_unique($comments));
                } else {
                    $query->whereNull('client_comment.case_comment_id');
                }

            })*/
            ->leftJoin('leads as le', 'le.id', '=', 'c.lead_id')
            ->leftJoin('leads_tags as lt', 'le.id', '=', 'lt.lead_id')
            ->leftJoin('users as u', 'le.conseiller_id', '=', 'u.id')
            ->leftJoin('users as ub', 'c.backoffice_id', '=', 'ub.id')
            ->leftJoin('lead_forms as lff', 'lff.id', '=', 'le.influencer')
            ->leftJoin('lead_form_marketing_campaigns as lf', 'lf.id', '=', 'c.campaign_id')
            ->leftJoin('programs as p', 'p.id', '=', 'cc.program_id')
            ->leftJoin('program_steps as s', 's.id', '=', 'cc.program_step_id')
            ->leftJoin('currencies as cur', 'cur.id', '=', 'c.currency_id')
            ->leftJoin(DB::raw('(SELECT `customer_case_id`, SUM(`fees`) AS `total_fees` FROM `case_comments` GROUP BY `customer_case_id`) fees'), 'fees.customer_case_id', '=', 'cc.customer_id')
            // ->leftJoin(DB::raw('(SELECT `case_comment_id`, `customer_case_id` FROM `customer_case_case_comment` AS a2 JOIN `case_comments` AS u2 ON u2.id = a2.case_comment_id ) comments'), 'comments.customer_case_id', '=', 'cc.id')
            ->leftJoin(DB::raw('(SELECT `lead_id`, COUNT(`lead_id`) AS `calls` FROM `histo_calls` GROUP BY `lead_id`) hc'), 'hc.lead_id', '=', 'c.lead_id')
            ->leftJoin('admission_states as ads', 'c.admission_state', '=', 'ads.id')
            ->leftJoin('cities', 'cities.id', '=', 'le.city2')

            // Added lately
            // ->leftJoin('case_states as css', 'cc.id', '=', 'css.customer_case_id')

            // should be changed 👇

            // ->leftJoin('case_states as css2', function ($join) use ($cs) {
            //     $join->on('cs.id', '=', 'css2.customer_case_id')
            //         ->whereIn('css2.sub_step_state_id', [3, 5, 10, 20, 29])
            //         ->where('css2.sub_step_state_id', '=', DB::raw("(select max(sub_step_state_id) from case_states where customer_case_id = cs.id and sub_step_state_id in (3, 5, 10, 20, 29))"))
            // })
            // ->leftJoin('case_states as css', function ($join) {
            //     $join->on('css.customer_case_id', '=', 'cc.id')
            //         ->whereIn('sub_step_state_id', [29, 20, 10, 5, 3])
            //         ->orderBy('sub_step_state_id', 'DESC')
            //         ->limit(1);
            // })
            ->leftJoin('case_states as css', function ($join) {
                $join->on('cc.id', '=', 'css.customer_case_id')
                    ->whereIn('css.sub_step_state_id', [3, 5, 10, 20, 29])
                    ->where('css.status', 1)
                    ->whereNotNull("css.payment_method")
                    ->where('css.id', '=', function ($query) {
                        $query->select('css2.id')
                            ->from('case_states as css2')
                            ->whereColumn('cc.id', 'css2.customer_case_id')
                            ->whereIn('css2.sub_step_state_id', [3, 5, 10, 20, 29])
                            ->where('css2.status', 1)
                            ->whereNotNull("css.payment_method")
                            ->orderBy('css2.sub_step_state_id', 'desc')
                            ->limit(1);
                    });
            })
            // ->leftJoin(DB::raw('(SELECT case_comment.`customer_case_id`, case_comment.`case_comment_id`, case_comment.`view`, case_comment.`canal`, CAST(MAX(comment.`created_at`) AS DATE) AS creation FROM `customer_case_case_comment` AS case_comment LEFT JOIN `case_comments` AS comment ON comment.`id` = case_comment.`case_comment_id` WHERE CAST(comment.`created_at` AS DATE)=CAST(NOW() AS DATE) AND case_comment.`view`="case-edit" GROUP BY case_comment.`customer_case_id`) comment_client_today'), function($join){
            //     $join->on('comment_client_today.customer_case_id', '=', 'cc.id');
            // })
            // ->leftJoin(DB::raw('(SELECT case_comment.`customer_case_id`, case_comment.`case_comment_id`, case_comment.`view`, case_comment.`canal`, CAST(MAX(comment.`created_at`) AS DATE) AS creation FROM `customer_case_case_comment` AS case_comment LEFT JOIN `case_comments` AS comment ON comment.`id` = case_comment.`case_comment_id` WHERE CAST(comment.`created_at` AS DATE)=CAST(NOW() AS DATE) AND case_comment.`view`="soumission-view" GROUP BY case_comment.`customer_case_id`) comment_soumission_today'), function($join){
            //     $join->on('comment_soumission_today.customer_case_id', '=', 'cc.id');
            // })

            ->leftJoin('tasks as soumission_task', function ($query) {
                $query->on('soumission_task.customer_case_id', '=', 'cc.id')
                    ->whereRaw('soumission_task.updated_at IN (select MAX(t2.updated_at) from tasks as t2 join customer_cases as cc2 on cc2.id = t2.customer_case_id group by cc2.id)');
            })
            ->select('cc.*', 'c.lead_id as lead_id', "soumission_task.updated_at as g_updated_at", 'soumission_task.co_updated_at as co_update_time', 'soumission_task.bo_updated_at as bo_update_time', 'c.firstname', 'c.lastname', 'c.email', 'c.phone', 'p.name as pname', 's.step_label', 'ads.label as admission_state', 'ads.id as admission_state_id', 'lf.campaign_name as canal', 'lff.title as influencer', 'ub.name as backoffice',  'u.name as conseiller', 'c.soumission_processed_at as processed', 'c.soumission_processed_message as processed_message', 'c.processed_message as processed_message_client', 'hc.calls', 'fees.total_fees', 'cur.iso as currency', 'le.reference', "cities.city as city2", "le.city as city", "le.country_id as country", "css.sub_step_state_id as sub_step_state_id", "css.updated_at as meet_date", "css.customer_case_id as customer_case")
            ->where(function ($query) use ($request, $id) {
                // search by name
                if (isset($request['customer']) && !empty($request['customer'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where(function ($query) use ($request) {
                            $query->where('c.firstname', 'LIKE', "%" . $request['customer'] . "%");
                        });

                        $query->orWhere(function ($query) use ($request) {
                            $query->where('c.lastname', 'LIKE', "%" . $request['customer'] . "%");
                        });
                        $query->orWhere(function ($query) use ($request) {
                            $query->where(DB::raw("CONCAT(`c`.`firstname`, ' ', `c`.`lastname`)"), 'LIKE', "%" . $request['customer'] . "%");
                        });
                        $query->orWhere(function ($query) use ($request) {
                            $query->where(DB::raw("CONCAT(`c`.`lastname`, ' ', `c`.`firstname`)"), 'LIKE', "%" . $request['customer'] . "%");
                        });
                    });
                }

                // search by referent
                if (isset($request['field-activity']) && !empty($request['field-activity'])) {
                    $query->whereIn('le.program_field', $request['field-activity']);
                }

                // search by email
                if (isset($request['email']) && !empty($request['email'])) {
                    $query->where('c.email', 'like', '%' . $request['email'] . '%');
                }
                // search by phone
                if (isset($request['phone']) && !empty($request['phone'])) {
                    $query->where('c.phone', 'like', '%' . $request['phone'] . '%');
                }
                // search by tag
                if (isset($request['tags']) && !empty($request['tags'])) {
                    $query->whereIn('lt.tag_id', $request['tags']);
                }
                if ($id) {
                    $query->where('lt.tag_id', $id);
                }
                // search by program
                if (isset($request['program']) && !empty($request['program'])) {
                    $query->whereIn('c.program_id', $request['program']);
                }
                // search by canal
                if (isset($request['campaign']) && !empty($request['campaign'])) {
                    $query->whereIn('c.campaign_id', $request['campaign']);
                }
                // search by referent
                if (isset($request['influencer']) && !empty($request['influencer'])) {
                    $query->whereIn('le.influencer', $request['influencer']);
                }
                // conseiller
                if (isset($request['conseiller']) && !empty($request['conseiller'])) {
                    if ($request['conseiller'] == 'n/d')
                        $query->whereNull('le.conseiller_id');
                    else
                        $query->whereIn('le.conseiller_id', $request['conseiller']);
                }
                // source
                if (isset($request['source']) && !empty($request['source'])) {
                    // dd("source", $request['source']);
                    $query->whereIn('le.influencer', $request['source']);
                }
                if (isset($request['date_start']) && !empty($request['date_start'])) {
                    $start = Carbon::parse($request['date_start']);
                    $query->where('c.created_at', '>=', $start->format('Y-m-d') . ' 00:00:00');
                }
                if (isset($request['date_end']) && !empty($request['date_end'])) {
                    $end = Carbon::parse($request['date_end'])->addDay();
                    $query->where('c.created_at', '<=', $end->format('Y-m-d') . ' 00:00:00');
                }
                // office
                if (isset($request['office']) && !empty($request['office'])) {
                    if ($request['office'] == "n/d") {
                        $query->whereNull('le.to_office');
                    } else {
                        $query->whereIn('le.to_office', $request['office']);
                    }
                }
                //
                if (isset($request['recycled']) && !empty($request['recycled'])) {
                    // dd($request['recycled']);
                    if ($request['recycled'] == 'recycled') {
                        $query->where('le.recycled', 1);
                    } else {
                        $query->where('le.recycled', 0);
                    }
                }
                // support
                if (isset($request['support']) && !empty($request['support'])) {
                    // if($request['support'] == "n/d"){
                    //     $query->whereNull('le.support_id');
                    // } else {
                    //     $query->where('le.support_id',$request['support']);
                    // }

                    $key = array_search('n/d', $request['support']);
                    if ($key !== false) {
                        $query->whereNull('le.support_id');
                    } else {
                        $query->where('le.support_id', $request['support']);
                    }
                }
                if (isset($request['status']) && !empty($request['status'])) {
                    /*$query->whereIn('c.admission_state',$request['status']);
                    if(in_array("n/d", $request['status'])){
                        $query->orWhereNull('c.admission_state');
                    }*/
                    $query->where(function ($query) use ($request) {
                        $key = array_search('n/d', $request['status']);
                        if ($key !== false) {
                            $query->whereNull('c.admission_state');
                            if (count($request['status']) > 1) {
                                $query->orWhereIn('c.admission_state', array_diff($request['status'], ["n/d"]));
                            }
                        } else {
                            $query->whereIn('c.admission_state', $request['status']);
                        }
                    });
                }

                // Edit R1 .. R6 filter [3, 5, 10, 20, 29]
                if (isset($request['meeting']) && !empty($request['meeting'])) {
                    $eq_array = [];
                    foreach ($request['meeting'] as $key => $value) {
                        switch ($value) {
                            case "1":
                                $eq_array[] = 3;
                                break;
                            case "2":
                                $eq_array[] = 5;
                                break;
                            case "3":
                                $eq_array[] = 10;
                                break;
                            case "4":
                                $eq_array[] = 20;
                                break;
                            case "5":
                                $eq_array[] = 29;
                                break;
                        }
                    }
                    $query->whereIn('sub_step_state_id', $eq_array);
                    // $query->where('c.r5', 1);
                }

                if (isset($request['city2'])) {
                    $cityModel = Cities::find($request["city2"]);

                    if ($cityModel) {
                        $city = $cityModel->city;

                        $query->where(function ($query) use ($city) {
                            $query->where('le.city', 'LIKE', "%" . $city . "%");
                        });
                    }
                }

                // if (isset($request["call_by_support"]))
                if (isset($request["call_by_support"]) && !empty($request["call_by_support"])) {
                    $lsids = array_unique(HistoCall::whereIn("user_id", User::whereIn("role_id", [7])->pluck("id")->toArray())->pluck("lead_id")->toArray());
                    if ($request["call_by_support"])
                        $query->whereIn('c.lead_id', $lsids);
                    else
                        $query->whereNotIn('c.lead_id', $lsids);
                }
                if (isset($request["call_by_conseiller"]) && !empty($request["call_by_conseiller"])) {
                    $lcids = array_unique(HistoCall::whereIn("user_id", User::whereIn("role_id", [3])->pluck("id")->toArray())->pluck("lead_id")->toArray());
                    if ($request["call_by_conseiller"])
                        $query->whereIn('c.lead_id', $lcids);
                    else
                        $query->whereNotIn('c.lead_id', $lcids);
                }

                if (isset($request["payment_proof"])) {
                    if ($request["payment_proof"]) { {
                            $query->whereIn("c.id", function ($sub) {
                                return $sub->fromSub(function ($subQuery) {
                                    $subQuery->select("c.id")
                                        ->leftJoin('program_payment_customer as ppc', 'ppc.customer_id', '=', 'c.id')
                                        ->from('customers as c')
                                        ->whereNotNull("ppc.status")
                                        ->orderBy("ppc.status", "desc");
                                }, 'sq');
                            });
                        }
                    } elseif ($request["payment_proof"] === "0") {
                        $query->whereIn("c.id", function ($sub) {
                            return $sub->fromSub(function ($subQuery) {
                                $subQuery->select("c.id")
                                    ->leftJoin('program_payment_customer as ppc', 'ppc.customer_id', '=', 'c.id')
                                    ->from('customers as c')
                                    ->whereNull("ppc.status");
                            }, 'sq');
                        });
                    }
                }


                $query->where('c.is_client', 1);
                // $query->whereNotIn('c.admission_state', [15, 16, 30, 31]);

            });

        if (Auth::user()->isConseiller()) {
            $casesQB = $casesQB->where('le.conseiller_id', Auth::id());
        }

        if (Auth::user()->isAgent()) {
            if (str_contains(Auth::user()->email, 'hrglobe')) {
                $casesQB = $casesQB->where('le.program_id', "=", 9);
            } else {
                $casesQB = $casesQB->where('le.program_id', "!=", 9);
            }
        }

        $column_idx = $request->input('order')[0]["column"];
        $direction = $request->input('order')[0]["dir"];
        $column = $request->input('columns')[$column_idx];
        $casesQB->orderBy('processed', 'DESC')->orderBy($column["name"], $direction)->groupBy('c.lead_id')->get();

        $actions = [
            ["action" => "call", "title" => "faire appel", "available" => 1, "edit" => 0, "className" => "btn btn-success btn-sm", "icon" => "fas fa-phone"],
            ["action" => "convert", "islink" => false, "title" => "convertir", "available" => (Auth::user()->isAdmin() || Auth::user()->isVerificateur() || Auth::user()->isManager()) ? 1 : 0, "className" => "btn btn-secondary btn-sm client-convert", "icon" => "far fa-copyright"],
            ["action" => "edit", "title" => "Voir client", "available" => 1, "edit" => 1, "className" => "btn btn-sm btn-light lead-show", "icon" => "fas fa-eye"],
            ["action" => "delete", "title" => "Supprimer client", "available" => (Auth::user()->isAdmin() || Auth::user()->isVerificateur() || Auth::user()->isManager()) ? 1 : 0, "edit" => 0, "className" => "btn btn-danger btn-sm case-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables()->of($casesQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Convert Client to Soumission.
     *
     * @param  Customer  $customer
     */
    public function convert_to_soumission($customer)
    {
        $customer = Customer::find($customer);
        if ($customer->admission_state !== 1) {
            $customer->admis_at = Carbon::now();
            if ($customer->program_id == 4) {
                $customer->admission_state = 1;
            } else {
                $customer->admission_state = 17;
            }
            // $customer->admission_state = 1;
            $customer->update(array('soumission_processed_message' => 'soumission status modifié(converted)', 'soumission_processed_at' => Carbon::now()));
            // $customer->update(array('processed_message' => 'soumission status modifié(converted)', 'processed_at' => Carbon::now()));
            $customer->processed_at = Carbon::now();
            $customer->processed_message = "de client à soumission";
            $customer->save();
            return "updated";
        } else {
            return "Already updated";
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  CustomerCaseUpdateFormRequest  $customer_case
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CustomerCaseUpdateFormRequest $request, CustomerCase $customer_case)
    {
        $customer_case->program_step_id = $request['step'];
        $customer_case->save();

        return back()->with(['status' => 'Etat dossier modifié avec succés!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  CustomerCase $customer_case
     * @return int
     * @throws \Exception
     */
    public function destroy(CustomerCase $customer_case, $status = null)
    {
        // dd($status);
        $y = LeadStatus::find($status);
        $status_id = $y ? $y->id : null;
        $meets = Calendrier::all()->filter(function ($meet) use ($customer_case) {
            return $meet->customer_id == $customer_case->customer->id && str_starts_with($meet->title, "Rencontre 2");
        });
        $meets->each(function ($meet, $key) {
            $meet->delete();
        });
        $old_state = $customer_case->customer->lead->lead_status_id;
        $customer_case->customer->lead->update(array('lead_status_id' => $status_id));
        $customer_case->customer->update(array('lead_status' => $status_id, 'is_client' => 0));
        if ($old_state != $status_id) {
            LeadStateLog::create([
                'user_id' => auth()->user()->id,
                'lead_id' => $customer_case->customer->lead_id,
                'state_id' => $customer_case->customer->lead->lead_status_id,
            ]);
        }
        return $customer_case->delete();
    }

    /**
     * Update the specified step state.
     *
     * @param \Illuminate\Http\Request $request
     * @param CustomerCase $customer_case
     * @param ProgramStep $step
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateStepState(Request $request, CustomerCase $customer_case, ProgramStep $program_step)
    {
        $stepId = $program_step->id; //Step Id
        $stateId = $request['state_' . $program_step->id] ?? null; //State Id

        $states = $customer_case->states()->wherePivot('program_step_id', '=', $stepId);
        //dd("test");
        if (count($states->get())) {

            $states->detach();

            if ($stateId) {
                $customer_case->states()->attach([
                    $stateId => [
                        'program_step_id' => $stepId,
                        'created_at' => Carbon::now(),
                    ]
                ]);
            }
        } else if ($stateId) {

            $customer_case->states()->attach([
                $stateId => [
                    'program_step_id' => $stepId,
                    'created_at' => Carbon::now(),
                ]
            ]);
        }

        return back()->with(['status' => 'Etat étape modifié avec succés!']);
    }

    public function updateProfile(Request $request)
    {
        //
        if (Auth::guard('customer')->user()) {
            $customer = Auth::guard('customer')->user();
            $emailValidation = 'email|unique:customers,email,' . $customer->id;
            $phone_rules = [Rule::unique('customers')->ignore($customer->id)];
            $gender = '';
        } else {
            $user = Auth::user();
            $emailValidation = 'email|unique:users,email,' . $user->id;
            $phone_rules = [Rule::unique('users')->ignore($user->id)];
            $gender = 'required|in:male,female';
        }

        if (isset($request['phone_error']) && !empty($request['phone_error'])) {
            //dd($request['phone_error']);
            array_push($phone_rules, new PhoneIndicator($request['phone_error']));
        }
        $rules = [
            // 'image' => 'nullable|image|mimes:jpeg,png',
            'name'   => 'min:3|max:30',
            'firstname'   => 'min:3|max:30',
            'lastname'    => 'min:3|max:30',
            'email'       => $emailValidation,
            'phone'       => $phone_rules,
            'gender'      => $gender
        ];

        $this->validate($request, $rules);

        // abort(500, 'Could not upload image :(');
        /*$input = $request->all();
        $client = Client::create($input);*/
        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
            Auth::user()->clearMediaCollection('avatar');
            Auth::user()->addMediaFromRequest('avatar')->toMediaCollection('avatar');
        }
        // return redirect()->route('client');

        if (Auth::guard('customer')->user()) {
            if (isset($request["password"]) && !empty($request["password"])) {
                $customer->password = Hash::make($request["password"]);
                $customer->save();
            }
            $customer->firstname = $request["firstname"];
            $customer->lastname = $request["lastname"];
            $customer->email = $request["email"];
            if (isset($request["phone"]) && !empty($request["phone"])) {
                $customer->phone = $request["phone"];
            }
            return view('mycase.profile.edit');
        } else {
            if (isset($request["password"]) && !empty($request["password"])) {
                $user->password = Hash::make($request["password"]);
            }
            $user->name = $request["name"];
            $user->phone = $request["phone"];
            $user->email = $request["email"];
            $user->gender = $request["gender"];
            $user->save();
            // return redirect('profileUser');
        }
        return \Redirect::back();
    }
}
