<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\DocumentType\DocumentTypeAddFormRequest;
use App\Http\Requests\DocumentType\DocumentTypeUpdateFormRequest;
use App\Http\Resources\DocumentTypeResource;
use App\Models\DocumentClear;
use App\Models\DocumentSubStep;
use App\Models\DocumentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class DocumentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $documentsQB = DB::table('document_types as dt')
            ->leftJoin('document_type_groups as group', 'group.id', '=', 'dt.document_type_group_id')
            // ->leftJoin('document_type_media_sheet as document_clear', 'document_clear.document_type_id', '=', 'dt.id')
            // ->leftJoin('media_sheets as clear', 'group.id', '=', 'dt.document_type_group_id')
            ->where(function ($query) use ($request) {
                if (isset($request['name_en']) && !empty($request['name_en'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('dt.name_en', 'LIKE', "%" . $request['name_en'] . "%");
                    });
                }
                if (isset($request['name_fr']) && !empty($request['name_fr'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('dt.name_fr', 'LIKE', "%" . $request['name_fr'] . "%");
                    });
                }
                if (isset($request['group']) && !empty($request['group'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('dt.document_type_group_id', $request['group']);
                    });
                }
            })
            ->select(['dt.*', 'group.name_fr as group_name']); //->orderBy("dt.id", "desc");
        if (isset($request['clear']) && !empty($request['clear'])) {
            $documentsQB->leftJoin('document_type_media_sheet as ds', 'ds.document_type_id', '=', 'dt.id')
                ->where(function ($query) use ($request) {
                    $query->where('ds.media_sheet_id', $request['clear']);
                });
        }

        $actions = [
            ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light document-type-show", "icon" => "fas fa-eye"],
            /*["action" => "copy", "title" => "Copier Lien", "available" => 1, "className" => "btn btn-info leadsforms-table-copy", "icon" => "fas fa-copy"],*/
            ["action" => "edit", "islink" => false, "title" => "Editer Formulaire", "available" => 1, "className" => "btn btn-primary documents-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "islink" => false, "title" => "Supprimer Formulaire", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger documents-table-delete", "icon" => "fas fa-trash"],
        ];

        // return DataTables()->eloquent($leadformsQB)
        //     ->addColumn('actions', $actions)
        //     ->toJson();

        return DataTables::of($documentsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }
    public function view(Request $request, $id, $column)
    {
        $documentsQB = DB::table('document_type_media_sheet as dc')
            ->where('dc.' . $column . '_id', $id)
            ->leftJoin('media_sheets as sheet', 'sheet.id', '=', 'dc.media_sheet_id')
            ->leftJoin('document_types as document', 'document.id', '=', 'dc.document_type_id')
            ->leftJoin('document_type_groups as group', 'group.id', '=', 'dc.document_type_group_id')
            ->leftJoin('document_type_groups as dgroup', 'dgroup.id', '=', 'document.document_type_group_id')
            ->leftJoin('programs as p', 'p.id', '=', 'dc.program_id')
            ->where(function ($query) use ($request) {
                if (isset($request['document']) && !empty($request['document'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('document.name_fr', 'LIKE', "%" . $request['document'] . "%");
                    });
                }
                if (isset($request['group']) && !empty($request['group'])) {
                    $query->where(function ($query) use ($request) {
                        if ($request['group'] == "n/d") $query->whereNull('dc.document_type_group_id');
                        else $query->where('dc.document_type_group_id', $request['group']);
                    });
                }
                if (isset($request['program']) && !empty($request['program'])) {
                    $query->where(function ($query) use ($request) {
                        $query->where('dc.program_id', $request['program']);
                    });
                }
            })
            ->select(['dc.*', 'sheet.name_fr as clear_name', 'p.name as program_name', 'document.name_fr as document_name', 'group.name_fr as document_group', 'dgroup.name_fr as d_group']);
        /*if(isset($request['clear']) && !empty($request['clear'])) {
            $documentsQB->leftJoin('document_type_media_sheet as ds', 'ds.document_type_id', '=', 'dt.id')
                ->where(function($query) use ($request) {
                    $query->where('ds.media_sheet_id', $request['clear']);
                });
        }*/
        if ($column == "media_sheet")
            $documentsQB->orderBy("dc.order", "asc");
        elseif ($column == "document_type")
            $documentsQB->orderBy("dc.media_sheet_id", "asc");

        $actions = [
            // ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light document-type-show", "icon" => "fas fa-eye"],
            /*["action" => "copy", "title" => "Copier Lien", "available" => 1, "className" => "btn btn-info leadsforms-table-copy", "icon" => "fas fa-copy"],*/
            ["action" => "edit", "islink" => false, "title" => "Editer Formulaire", "available" => 1, "className" => "btn btn-primary documents-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "islink" => false, "title" => "Supprimer Formulaire", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger documents-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables::of($documentsQB->orderBy("dc.media_sheet_id", "asc"))
            ->addColumn('actions', $actions)
            ->toJson();
    }
    public function viewSteps(Request $request, $id)
    {
        // dd("test", $id);
        $documentsQB = DB::table('document_type_program_sub_step as dc')
            ->where('dc.document_type_id', $id)
            ->leftJoin('program_sub_steps as step', 'step.id', '=', 'dc.program_sub_step_id')
            ->leftJoin('document_types as document', 'document.id', '=', 'dc.document_type_id')
            // ->leftJoin('document_type_groups as group', 'group.id', '=', 'dc.document_type_group_id')
            ->leftJoin('programs as p', 'p.id', '=', 'dc.program_id')
            ->where(function ($query) use ($request) {
                /*if(isset($request['document']) && !empty($request['document'])) {
                    $query->where(function($query) use ($request) {
                        $query->where('document.name_fr', 'LIKE', "%".$request['document']."%");
                    });
                }
                if(isset($request['group']) && !empty($request['group'])) {
                    $query->where(function($query) use ($request) {
                        $query->where('dc.document_type_group_id', $request['group']);
                    });
                }
                if(isset($request['program']) && !empty($request['program'])) {
                    $query->where(function($query) use ($request) {
                        $query->where('dc.program_id', $request['program']);
                    });
                }*/
            })
            ->select(['dc.*', 'step.step_label as step_name', 'p.name as program_name', 'document.name_fr as document_name']);
        /*if(isset($request['clear']) && !empty($request['clear'])) {
            $documentsQB->leftJoin('document_type_media_sheet as ds', 'ds.document_type_id', '=', 'dt.id')
                ->where(function($query) use ($request) {
                    $query->where('ds.media_sheet_id', $request['clear']);
                });
        }*/
        $documentsQB->orderBy("dc.program_sub_step_id", "asc");

        $actions = [
            // ["action" => "view", "islink" => true, "title" => "afficher", "available" => 1, "className" => "btn btn-sm btn-light document-type-show", "icon" => "fas fa-eye"],
            /*["action" => "copy", "title" => "Copier Lien", "available" => 1, "className" => "btn btn-info leadsforms-table-copy", "icon" => "fas fa-copy"],*/
            ["action" => "edit", "islink" => false, "title" => "Editer Formulaire", "available" => 1, "className" => "btn btn-primary documents-table-edit", "icon" => "fas fa-edit"],
            ["action" => "delete", "islink" => false, "title" => "Supprimer Formulaire", "available" => Auth::user()->isAdmin() || Auth::user()->isVerificateur() ? 1 : 0, "className" => "btn btn-danger documents-table-delete", "icon" => "fas fa-trash"],
        ];

        return DataTables::of($documentsQB)
            ->addColumn('actions', $actions)
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return DocumentTypeResource
     */
    public function store(DocumentTypeAddFormRequest $request)
    {
        // dd($request["add-form-redirect"]);
        $is_customer_owner = (isset($request['add-form-redirect']) && !empty($request['add-form-redirect']) && ($request['add-form-redirect'] == "on")) ? 1 : 0;
        // $is_customer_owner = (isset($request['add-form-group']) && !empty($request['add-form-group']) && ($request['add-form-redirect'] == "on")) ? 1 : 0;
        return new DocumentTypeResource(DocumentType::create([
            "collection_name" => Str::random(30),
            "document_type_group_id" => $request['add-form-group'],
            "name_fr" => $request['add-form-title-fr'],
            "name_en" => $request['add-form-title-en'],
            "signable" => 0,
            "customer_owner" => $is_customer_owner,
            "visible_for_customer" => 1,
        ]));
    }
    public function storePivot(Request $request, $id, $column = "document_type")
    {
        $rules = [
            // 'add-form-clear' => 'required',
            'add-form-document' => Rule::requiredIf(function () use ($column) {
                return $column == "media_sheet";
            }),
            'add-form-clear' => Rule::requiredIf(function () use ($column) {
                return $column == "document_type";
            }),
            'add-form-program' => 'required',
            'add-form-order' => 'required',
        ];

        $this->validate($request, $rules);
        if ($column == "media_sheet") {
            $document_id = $request['add-form-document'];
            $sheet_id = $id;
        }
        if ($column == "document_type") {
            $sheet_id = $request['add-form-clear'];
            $document_id = $id;
        }
        $document_clear = DocumentClear::create([
            "document_type_id" => $document_id,
            "media_sheet_id" => $sheet_id,
            "program_id" => $request['add-form-program'],
            "document_type_group_id" => $request['add-form-group'],
            "order" => $request['add-form-order'],
            "required" => 1,
        ]);
        return response()->json($document_clear->toArray());
    }
    public function storeStepPivot(Request $request, $document_id)
    {
        $rules = [
            'add-form-step' => 'required',
            'add-form-program' => 'required',
            'add-form-order' => 'required',
        ];

        $this->validate($request, $rules);

        $document_clear = DocumentSubStep::create([
            "document_type_id" => $document_id,
            "program_sub_step_id" => $request['add-form-step'],
            "program_id" => $request['add-form-program'],
            "order" => $request['add-form-order'],
        ]);
        return response()->json($document_clear->toArray());
    }
    public function getPivot($id)
    {
        // dd($request["add-form-redirect"]);
        $data = DB::table('document_type_media_sheet as ds')->where('ds.id', $id)->first();
        // dd($data);
        return response()->json($data);
    }
    /**
     * edit resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return DocumentTypeResource
     */
    public function update(DocumentTypeUpdateFormRequest $request, $id)
    {
        // dd($request->input());
        $document = DocumentType::findOrFail($id);
        if (is_null($document->conditions))  $conditions = array();
        else $conditions = $document->conditions;
        foreach ($request->input() as $k => $v) {
            if (str_starts_with($k, "edit-form-condition_type_")) {
                $order = str_replace("edit-form-condition_type_", "", $k);
                if (!array_key_exists($order, $conditions))
                    $conditions[$order] = array();
                $conditions[$order]["type"] = $v;
                // dd($document->conditions, $order);
            } elseif (str_starts_with($k, "edit-form-values_")) {
                $order = str_replace("edit-form-values_", "", $k);
                if (!array_key_exists($order, $conditions))
                    $conditions[$order] = array();
                $conditions[$order]["values"] = $v;
            } elseif (str_starts_with($k, "edit-form-condition_")) {
                $order = str_replace("edit-form-condition_", "", $k);
                if (!array_key_exists($order, $conditions))
                    $conditions[$order] = array();
                $conditions[$order]["field_name"] = $v;
                // dd($document->conditions, $order);
            } elseif (str_starts_with($k, "edit-form-payment_condition_")) {
                $order = str_replace("edit-form-payment_condition_", "", $k);
                if (!array_key_exists($order, $conditions))
                    $conditions[$order] = array();
                $conditions[$order]["values"] = $v;
                // dd($document->conditions, $order);
            }
        }
        $document->conditions = $conditions;
        // dd($document->conditions);
        // if (isset($request['edit-form-redirect']) && !empty($request['edit-form-redirect']))
        if (isset($request['edit-form-redirect']) && !empty($request['edit-form-redirect']) && $request['edit-form-redirect'] == "on")
            $document->customer_owner = 1;
        else
            $document->customer_owner = 0;

        $document->name_en = $request['edit-form-title-en'];
        $document->name_fr = $request['edit-form-title-fr'];
        // $document->customer_owner = $request['edit-form-campaign'];
        if (isset($request['edit-form-group']) && !empty($request['edit-form-group'])) {
            $document->document_type_group_id = $request['edit-form-group'];
        }

        $document->save();
        return new DocumentTypeResource($document);
    }
    public function updatePivot(Request $request, $id, $column = "document_type")
    {
        $rules = [
            'edit-form-document' => Rule::requiredIf(function () use ($column) {
                return $column == "media_sheet";
            }),
            'edit-form-clear' => Rule::requiredIf(function () use ($column) {
                return $column == "document_type";
            }),
            // 'edit-form-clear' => 'required',
            'edit-form-program' => 'required',
            'edit-form-order' => 'required',
        ];

        $this->validate($request, $rules);

        $document_clear = DocumentClear::findOrFail($id);

        if (isset($request['edit-form-clear']) && !empty($request['edit-form-clear']))
            $document_clear->media_sheet_id = $request['edit-form-clear'];
        if (isset($request['edit-form-document']) && !empty($request['edit-form-document']))
            $document_clear->document_type_id = $request['edit-form-document'];
        if (isset($request['edit-form-group']) && !empty($request['edit-form-group']))
            $document_clear->document_type_group_id = $request['edit-form-group'];
        $document_clear->program_id = $request['edit-form-program'];
        $document_clear->order = $request['edit-form-order'];

        $document_clear->save();
        return response()->json($document_clear->toArray());
    }
    public function updateStepPivot(Request $request, $id)
    {
        $rules = [
            'edit-form-step' => 'required',
            'edit-form-program' => 'required',
            'edit-form-order' => 'required',
        ];

        $this->validate($request, $rules);

        $document_step = DocumentSubStep::findOrFail($id);

        $document_step->program_sub_step_id = $request['edit-form-step'];
        $document_step->program_id = $request['edit-form-program'];
        $document_step->order = $request['edit-form-order'];

        $document_step->save();
        return response()->json($document_step->toArray());
    }

    public function destroyPivot(DocumentClear $document_clear)
    {
        return $document_clear->delete();
    }
    public function destroyStepPivot(DocumentSubStep $document_step)
    {
        return $document_step->delete();
    }

    public function destroy(DocumentType $document)
    {
        return $document->delete();
    }
}
