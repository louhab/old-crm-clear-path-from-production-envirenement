<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Route;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {

            if (strpos(Route::currentRouteName(), 'customer-') !== false) {
                return route('customerLogin');
            }

            return route('login');
        }

        /*if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        if (strpos(Route::currentRouteName(), 'customer-') !== false) {
            return route('customerLogin');
        }

        if (! $request->expectsJson()) {
            return route('login');
        }*/
    }
}
