<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AcceptJson
{
    /**
     * Accept only JSON Fromat
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $acceptedHeader = $request->header('Accept');
        if ($acceptedHeader != 'application/json') {
            return response()->json([], 400);
        }

        return $next($request);
    }
}
