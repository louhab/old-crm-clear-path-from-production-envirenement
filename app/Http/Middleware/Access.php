<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {

        $currentRouteName = Route::currentRouteName();
        // dd($currentRouteName);
        if (auth()->user() instanceof \App\Models\Customer) {
            if ($currentRouteName == "customer-edit-information-sheet" || $currentRouteName == "customer-edit-media-sheet") {
                if ($request->customer_case->customer->id != auth()->user()->id) {
                    return redirect()->route('customer-case');
                }
            }
        }
        if ((auth()->user() instanceof User) && (!auth()->user()->isAdmin() || !auth()->user()->isVerificateur()) && auth()->check() && (auth()->user()->status == 0)) {
            Auth::logout();
            //dd("test");
            $request->session()->invalidate();

            $request->session()->regenerateToken();

            return redirect()->route('login')->with('error', 'Your Account is suspended, please contact Admin.');
        }
        // dd($currentRouteName, $request->lead);
        if ($currentRouteName == "soumission-view")
            if (auth()->user()->isConseiller())
                return redirect()->route('case-edit', ['customer_case' => $request->customer_case]);
        if ($currentRouteName == "case-edit")
            if (auth()->user()->isBackoffice())
                return redirect()->route('soumission-view', ['customer_case' => $request->customer_case]);

        if ($currentRouteName == "lead-view") {
            if (auth()->user()->isSupport())
                if ($request->lead->support_id != auth()->id())
                    return redirect()->route('leads-index');
            // rm strict access
            if (auth()->user()->isConseiller())
                if ($request->lead->conseiller_id != auth()->id() && !$request->has('ref'))
                    return redirect()->route('leads-index');
        }
        if ($currentRouteName == "case-edit") {
            if (auth()->user()->isSupport())
                if ($request->customer_case->customer->lead->support_id != auth()->id())
                    return redirect()->route('cases-list');
            if (auth()->user()->isConseiller())
                if ($request->customer_case->customer->lead->conseiller_id != auth()->id())
                    return redirect()->route('cases-list');
        }
        if ($currentRouteName == "soumission-view" && auth()->user()->isBackoffice())
            if ($request->customer_case->customer->backoffice_id != auth()->id())
                return redirect()->route('soumissions-index');

        if (count($roles) !== 0 && $request->user()) {
            if (is_array($roles)) {
                foreach ($roles as $role) {
                    if ($request->user()->hasRole($role))
                        return $next($request);
                }
            } else {
                if ($request->user()->hasRole($roles))
                    return $next($request);
            }
        }

        if (Auth::user()->isAdmin() || Auth::user()->isManager() || Auth::user()->isConseiller() || Auth::user()->isAgent() || Auth::user()->isSupport() || Auth::user()->isFinancier() || Auth::user()->isVerificateur())
            return redirect()->route('dashboard');

        if (Auth::user()->isBackoffice() || Auth::user()->isSuperBackoffice())
            return redirect()->route('soumissions-index');

        // is buggy
        if (Auth::user()->isCustomer())
            return redirect()->route('customerLogin');

        return new Response("You don't have any access to enter on this page.", 403);
    }
}
