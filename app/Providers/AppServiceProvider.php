<?php

namespace App\Providers;

use App\Models\Calendrier;
use App\Models\Lead;
use App\Observers\CalendrierObserver;
use App\Observers\LeadObserver;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

use App\Models\LeadStatus;
use App\Models\Customer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Lead::observe(LeadObserver::class);
        Calendrier::observe(CalendrierObserver::class);
        \Schema::defaultStringLength(191);

        Blade::if('admin', function () {
            return Auth::user()->isAdmin() || Auth::user()->isVerificateur();
        });

        Blade::if('manager', function () {
            return Auth::user()->isManager();
        });

        Blade::if('financier', function () {
            return Auth::user()->isFinancier();
        });

        Blade::if('conseiller', function () {
            return Auth::user()->isConseiller();
        });

        Blade::if('agent', function () {
            return Auth::user()->isAgent();
        });

        Blade::if('customer', function () {
            return Auth::user()->isCustomer();
        });

        Blade::if('administration', function () {
            return Auth::user()->isAdmin() || Auth::user()->isManager() || Auth::user()->isVerificateur();
        });

        Blade::if('administration_bis', function () {
            return Auth::user()->isAdmin() || Auth::user()->isManager();
        });

        Blade::if('financial', function () {
            return Auth::user()->isAdmin() || Auth::user()->isFinancier();
        });

        Blade::if('management', function () {
            return Auth::user()->isAdmin() || Auth::user()->isManager() || Auth::user()->isConseiller() || Auth::user()->isVerificateur();
        });

        Blade::if('administration_agent_support', function () {
            return Auth::user()->isAdmin() || Auth::user()->isManager() || Auth::user()->isAgent() || Auth::user()->isSupport() || Auth::user()->isVerificateur();
        });

        Blade::if('management_agent_support', function () {
            return Auth::user()->isAdmin() || Auth::user()->isManager() || Auth::user()->isConseiller() || Auth::user()->isAgent() || Auth::user()->isSupport() || Auth::user()->isVerificateur();
        });

        Blade::if('agentsupport', function () {
            return Auth::user()->isAgent() || Auth::user()->isSupport();
        });

        Blade::if('agent', function () {
            return Auth::user()->isAdmin() || Auth::user()->isManager() || Auth::user()->isAgent() || Auth::user()->isVerificateur();
        });

        Blade::if('show_clients', function () {
            return Auth::user()->isAdmin() || Auth::user()->isManager() || Auth::user()->isConseiller() || Auth::user()->isAgent() || Auth::user()->isBackoffice() || Auth::user()->isSuperBackoffice() || Auth::user()->isVerificateur();
        });

        Blade::if('management_backoffice', function () {
            return Auth::user()->isAdmin() || Auth::user()->isManager() || Auth::user()->isConseiller() || Auth::user()->isBackoffice() || Auth::user()->isVerificateur();
        });

        Blade::if('no_manager_backoffice', function () {
            return Auth::user()->isAdmin() || Auth::user()->isBackoffice() || Auth::user()->isVerificateur();
        });

        Blade::if('support_deny', function () {
            return !Auth::user()->isSupport();
        });

        Blade::if('calendar_deny', function () {
            return !Auth::user()->isSupport() || !Auth::user()->isAgent() || !Auth::user()->isConseiller();
        });

        Blade::if('support', function () {
            return Auth::user()->isAdmin() || Auth::user()->isManager() || Auth::user()->isSupport() || Auth::user()->isVerificateur();
        });

        Blade::if('admission', function () {
            return Auth::user()->isAdmin() || Auth::user()->isBackoffice() || Auth::user()->isSuperBackoffice() || Auth::user()->isVerificateur();
        });

        Blade::if('backoffice', function () {
            return Auth::user()->isBackoffice() || Auth::user()->isSuperBackoffice();
        });

        Blade::if('statusMeet', function ($lead, $customer_case) {
            // $status = LeadStatus::where('status_label_fr', 'Intéressé')->first();
            //->where('status_label_fr', 'like', 'Rencontre 1')
            $campaign = \App\Models\LeadFormMarketingCampaign::find(14);
            /*$status = LeadStatus::where('id', $lead->lead_status_id)
                                            ->where('status_label_fr', 'not like', 'Injoignable')
                                            ->where('status_label_fr', 'not like', 'Pas Intéressé')
                                            ->where('status_label_fr', 'not like', 'Numero erroné')
                                            ->first();*/
            $check = 1;
            if ($lead->to_office == "conference") {
                $check = 0;
                $case_state_3 = \App\Models\CaseState::where("sub_step_state_id", 3)
                    ->where("customer_case_id", $customer_case->id)
                    ->first();
                if ($case_state_3 && $case_state_3->status == 1)
                    $check = 1;
            }
            // dd($case_state_3->status);
            return auth()->user()->isAdmin() || Auth::user()->isVerificateur() || ($check && ($lead->campaign_id == $campaign->id || ($lead->lead_status_id && (\App\Models\HistoCall::where("lead_id", $lead->id)->count()))));
        });
        Blade::if('statusClear', function ($lead) {
            $states = \App\Models\LeadStatus::all();
            $newStates = $states->filter(function ($state) use ($lead) {
                return $lead->lead_status_id && $state->id == $lead->lead_status_id && ($state->status_label_fr == "Contrat" || $state->status_label_fr == "Rencontre 1");
            });
            /*$ms = \App\Models\Calendrier::all()->filter(function ($meet) use ($lead) {
                return $meet->customer_id == $lead->customer->id && str_starts_with($meet->title, "Rencontre 1");
            });*/
            $ms = \App\Models\Calendrier::all()->filter(function ($meet) use ($lead) {
                return $meet->customer_id == $lead->customer->id && (str_starts_with($meet->title, "Recontre 1") || str_starts_with($meet->title, 'Rencontre 1') || str_starts_with($meet->title, str_replace('Rencontre ', 'R', 'Rencontre 1')));
            });
            // $ms = \App\Models\Calendrier::where("customer_id", Customer::where("lead_id", $lead->id)->first()->id)->get();
            // dd($newStates->count() > 0 ,$ms->count() > 0);
            // dd($lead->lead_status_id,  Customer::where("lead_id", $lead->id)->first()->lead_status);
            return $newStates->count() || $ms->count();
        });
        Blade::if('clear_done', function ($customer_case) {
            $clear_done = false;
            $substep = \App\Models\ProgramSubStep::where("id", 6)->first();
            $docs = $substep->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot('order')->orderBy('order', 'asc')->get();
            foreach ($docs as $doc) {
                $media = $customer_case->customer->getFirstMedia($doc->collection_name);
                if (!empty($media)) {
                    $clear_done = true;
                    break;
                }
            }
            return $clear_done;
        });
        // can make payment 3
        Blade::if('haveDocuments', function ($customer_case) {
            $doc1002    = \App\Models\DocumentType::where("name_fr", "Clear1002 - Orientation")->first();
            $media1002 = $customer_case->customer->getFirstMedia($doc1002->collection_name);
            if ($customer_case->program_id == 4) {
                $doc1001 = \App\Models\DocumentType::where("name_fr", "Clear1001 – Conseil")->first();
                $media1001 = $customer_case->customer->getFirstMedia($doc1001->collection_name);
                if (empty($media1001)) {
                    return 0;
                }
            } else {
                $doc1004 = \App\Models\DocumentType::where("name_fr", "Clear1004 - Rapport Scoring")->first();
                $media1004 = $customer_case->customer->getFirstMedia($doc1004->collection_name);
                if (empty($media1004)) {
                    return 0;
                }
            }
            if (empty($media1002)) {
                return 0;
            }
            $mediaSheet = \App\Models\MediaSheet::where('name_fr', 'Clear1 - Documents de l\'étudiant')->first();
            $docs = $mediaSheet->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get();
            foreach ($docs as $doc) {
                $media = $customer_case->customer->getFirstMedia($doc->collection_name);
                if (empty($media)) {
                    return 0;
                }
            }
            $mediaSheet = \App\Models\MediaSheet::where('name_fr', 'Clear2 - Documents des garants')->first();
            $docs = $mediaSheet->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get();
            foreach ($docs as $doc) {
                $media = $customer_case->customer->getFirstMedia($doc->collection_name);
                if (empty($media)) {
                    return 0;
                }
            }
            $customer_case->substep()->associate(13);
            $customer_case->save();
            return $customer_case->program_step_id > 3;
        });
    }
}
