<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CustomerCase extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['deleted_at'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($case) {
            if (Auth::check()) {
                $user_id = Auth::id();
            } else {
                $user_id = $case->customer->lead->conseiller_id;
            }
            foreach(SubStepState::all() as $state){
                CaseState::create([
                    'user_id' => $user_id,
                    'customer_case_id' => $case->id,
                    'sub_step_state_id' => $state->id,
                    'status' => false,
                ]);
            }
        });

        // static::deleting(function($case) {
        //     $case->customer->delete();
        // });
        /*static::created(function ($customer_case) {
            CustomerInitialInformation::create([
                'user_id' => Auth::id(),
                'customer_case_id' => $customer_case->id,
            ]);

            CustomerScoreEvaluation::create([
                'user_id' => Auth::id(),
                'customer_case_id' => $customer_case->id,
            ]);

            CustomerCaseAnalyse::create([
                'user_id' => Auth::id(),
                'customer_case_id' => $customer_case->id,
            ]);

            CustomerContract::create([
                'user_id' => Auth::id(),
                'customer_case_id' => $customer_case->id,
            ]);
        });*/
    }

    /**
     * Get the customer.
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function states()
    {
        return $this->hasMany('App\Models\CaseState');
    }
    /**
     * Get the program.
     */
    public function program()
    {
        return $this->belongsTo('App\Models\Program');
    }

    /**
     * Get the program step.
     */
    public function step()
    {
        return $this->belongsTo('App\Models\ProgramStep', 'program_step_id');
    }

    public function substep()
    {
        return $this->belongsTo('App\Models\ProgramSubStep', 'program_sub_step_id');
    }

    /**
     * Get the initial info.
     */
    /*public function info()
    {
        return $this->hasOne('App\Models\CustomerInitialInformation');
    }*/

    /**
     * Get the evaluation.
     */
    /*public function evaluation()
    {
        return $this->hasOne('App\Models\CustomerScoreEvaluation');
    }*/

    /**
     * The case comments.
     */
    public function comments()
    {
        return $this->belongsToMany('App\Models\CaseComment', 'customer_case_case_comment');
    }
}
