<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class HistoCall extends Model
{
    use LogsActivity;

    protected $fillable = [
        'call_dt', 'call_descr', 'lead_state_id', 'duration', 'lead_id', 'customer_id', 'user_id', 'state_before'
    ];
    /**
     * Get the customer.
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function lead()
    {
        return $this->belongsTo('App\Models\Lead');
    }
}
