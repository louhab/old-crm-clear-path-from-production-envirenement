<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramCourseField extends Model
{
    protected $guarded = [];

    protected $casts = [
        'values' => 'array'
    ];
}
