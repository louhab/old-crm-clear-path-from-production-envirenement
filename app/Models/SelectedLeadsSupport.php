<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SelectedLeadsSupport extends Model
{
    protected $table = 'selected_leads_support';
    protected $fillable = [
        'number',
    ];
}
