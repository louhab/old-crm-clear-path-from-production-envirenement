<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerVisaRequest extends Model
{
    protected $guarded = [];
}
