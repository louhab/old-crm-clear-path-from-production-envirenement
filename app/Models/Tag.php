<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'tag_name',
    ];

    public function leads(){
        return $this->belongsToMany('App\Models\Lead','leads_tags','tag_id','lead_id');
    }
}
