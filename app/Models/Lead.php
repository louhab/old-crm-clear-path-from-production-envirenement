<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Activitylog\Traits\LogsActivity;

class Lead extends Model implements HasMedia
{
    use InteractsWithMedia, LogsActivity;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['deleted_at'];

    protected $appends = [
        'cv_id',
        'cin_id'
    ];

    /**
     * Get the customer.
     */

    // protected static function booted()
    // {
    //     static::deleting(function ($lead) {
    //         $lead->customer->delete();
    //     });
    // }

    public function customer()
    {
        return $this->hasOne('App\Models\Customer');
    }
    public function campaign()
    {
        return $this->hasOne('App\Models\LeadFormMarketingCampaign', 'id');
    }

    public function tags(){
        return $this->belongsToMany('App\Models\Tag','leads_tags','lead_id','tag_id');
    }

    /**
     * Get the program.
     */
    public function program()
    {
        return $this->belongsTo('App\Models\Program');
    }

    /**
     * Get the lead ranking.
     */
    public function ranking()
    {
        return $this->belongsTo('App\Models\Ranking');
    }

    /**
     * Get the lead country.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function conseiller()
    {
        return User::where("id", $this->conseiller_id)->first();
        //return $this->belongsTo('App\Models\Country');
    }

    public function getCvIdAttribute()
    {
        return (empty($this->getMedia('CVs')[0])) ? false : $this->getMedia('CVs')[0]->id;
    }

    public function getCinIdAttribute()
    {
        return (empty($this->getMedia('CINs')[0])) ? false : $this->getMedia('CINs')[0]->id;
    }

    public function setEmailAttribute($value) {
        if ( empty($value) ) {
            $this->attributes['email'] = NULL;
        } else {
            $this->attributes['email'] = $value;
        }
    }
    // public function getFullNameAttribute()
    // {
    //     return $this->firstname . " " . $this->lastname;
    // }
    // public function getPhoneAttribute($value) {
    //     if(preg_match("/^(\+212)[0-9]{9,9}$/", $value)){
    //         return $value;
    //     } else {
    //         if(strlen($value) >= 9){
    //             $radical = substr($value, -9);
    //             if(str_starts_with($value, "6")){
    //                 return "+212" . $radical;
    //             }
    //         }
    //         return $value;
    //     }
    // }

}
