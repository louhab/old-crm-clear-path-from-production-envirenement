<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Service extends Model
{
    use HasTranslations;

    public $translatable = ['single_price', 'couple_price'];

    protected $fillable = [
        'phase_id', 'name', 'couple_price', 'single_price'
    ];

    public function phase()
    {
        return $this->belongsTo('App\Models\Phase');
    }
    public function payment()
    {
        return $this->belongsTo('App\Models\ProgramPayment');
    }

    /**
     * Get the service details.
     */
    public function details()
    {
        return $this->hasMany('App\Models\ServiceDetail');
    }

    /**
     * Get the related bills.
     */
    public function bills()
    {
        return $this->belongsToMany('App\Models\Bill');
    }
    /**
     * Get the related quotes.
     */
    public function quotes()
    {
        return $this->belongsToMany('App\Models\Quote');
    }
}
