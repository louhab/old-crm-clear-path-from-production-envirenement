<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerField extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['deleted_at'];

    /**
     * Get the customer field group.
     */
    public function group()
    {
        return $this->belongsTo('App\Models\CustomerFieldGroup', 'customer_field_group_id');
    }
    public function subGroup()
    {
        return $this->belongsTo('App\Models\CustomerFieldSubGroup', 'customer_field_sub_group_id');
    }

    /**
     * The steps having this field.
     */
    public function steps()
    {
        return $this->belongsToMany('App\Models\ProgramStep');
    }
    public function substeps()
    {
        return $this->belongsToMany('App\Models\ProgramSubStep');
    }

    /**
     * The information sheets having this field.
     */
    public function sheets()
    {
        return $this->belongsToMany('App\Models\InformationSheet');
    }
}
