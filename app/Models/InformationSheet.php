<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InformationSheet extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['deleted_at'];

    /**
     * The steps having this document.
     */
    public function steps()
    {
        return $this->belongsToMany('App\Models\ProgramStep');
    }

    public function substeps()
    {
        return $this->belongsToMany('App\Models\ProgramSubStep');
    }

    /**
     * The customer fields having this sheet.
     */
    public function fields()
    {
        return $this->belongsToMany('App\Models\CustomerField');
    }

    /**
     * The customers having this sheet.
     */
    public function customers()
    {
        return $this->belongsToMany('App\Models\InformationSheet', 'information_sheet_customer');
    }
}
