<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerFieldRemark extends Model
{
    //
    protected $fillable = ['customer_id', 'field_id', 'remark_id', 'value'];
}
