<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    use \Conner\Tagging\Taggable;
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['deleted_at'];

    protected $casts = [
        'conditions' => 'array'
    ];

    /**
     * The steps having this document.
     */
    public function steps()
    {
        return $this->belongsToMany('App\Models\ProgramStep');
    }
    public function substeps()
    {
        return $this->belongsToMany('App\Models\ProgramSubStep');
    }
    public function sheets()
    {
        return $this->belongsToMany('App\Models\MediaSheet')->withPivot("program_id", "document_type_group_id", "order");
    }
    public function groups()
    {
        return $this->belongsToMany('App\Models\DocumentTypeGroup', 'document_types_groups')->withPivot("program_id", "order");
    }
    public function levels()
    {
        return $this->belongsToMany('App\Models\LevelOfStudy', 'document_type_level_of_study');
    }
    /**
     * The customers wich documents request sent for.
     */
    public function requesters()
    {
        return $this->belongsToMany('App\Models\Customer', 'document_type_customer');
    }
    public function group()
    {
        return $this->belongsTo('App\Models\DocumentTypeGroup', 'document_type_group_id');
    }
}
