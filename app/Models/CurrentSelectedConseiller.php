<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrentSelectedConseiller extends Model
{
    protected $fillable = [
        'conseiller_id',
        'status',
    ];
}
