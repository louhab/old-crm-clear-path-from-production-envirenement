<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['deleted_at'];

    /**
     * The steps that belong to the program.
     */
    public function steps()
    {
        return $this->belongsToMany('App\Models\ProgramStep');
    }
    public function substeps()
    {
        return $this->belongsToMany('App\Models\ProgramSubStep');
    }
}
