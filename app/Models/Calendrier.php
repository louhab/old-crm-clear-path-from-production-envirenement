<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Calendrier extends Model
{
    use SoftDeletes;
    use LogsActivity;
    protected $fillable = [
        'title',
        'date',
        'description',
        'user_id',
        'customer_id',
        'time',
        'time_end'
    ];

    protected $guarded = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
