<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OccupationalClassification extends Model
{
    protected $guarded = [];
}
