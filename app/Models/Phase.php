<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
    protected $fillable = [
        'program_id', 'name', 'order',
    ];
    public function program()
    {
        return $this->belongsTo('App\Models\Program');
    }
}
