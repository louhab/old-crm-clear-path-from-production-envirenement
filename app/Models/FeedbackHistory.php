<?php

namespace App\Models;

use Spatie\MediaLibrary\MediaCollections\Models\Concerns\HasUuid;
use Illuminate\Database\Eloquent\Model;

class FeedbackHistory extends Model
{
    use HasUuid;
    protected $table = 'feedbacks_history';

    protected $fillable = [
        "lead_id",
        "submitted",
        "feedback_id"
    ];
}
