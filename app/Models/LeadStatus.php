<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use ReflectionClass;

class LeadStatus extends Model
{
    // public $timestamps = false;

    public const Interesse = 1;
    public const PasInteresse = 2;
    public const Injoignable = 3;
    public const Rencontre1 = 4;
    public const Contrat = 5;
    public const NumeroErrone = 6;
    public const Admissible = 7;
    public const Inadmissible = 8;
    public const NoShow = 9;
    public const Client = 10;
    public const Devis = 15;

    static function getConstants()
    {
        $oClass = new ReflectionClass(__CLASS__);
        $consts = $oClass->getConstants();
        if (array_key_exists("CREATED_AT", $consts))
            unset($consts["CREATED_AT"]);
        if (array_key_exists("UPDATED_AT", $consts))
            unset($consts["UPDATED_AT"]);

        return $consts;
    }
}
