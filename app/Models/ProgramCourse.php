<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProgramCourse extends Model
{
    use SoftDeletes, LogsActivity ;
    protected $guarded = ['deleted_at'];
    // protected $appends = ['prix_total', 'domaine_etude', 'programme', 'test_langue','langue', 'ville', 'session', 'diplome', 'type_etablissement'];
    // protected $keys = ['prix_total', 'domaine_etude' => "Domaine d'études", 'programme', 'test_langue','langue', 'ville', 'session', 'diplome', 'type_etablissement'];

    protected $casts = [
        'properties' => 'array'
    ];

    public function customers()
    {
        return $this->belongsToMany('App\Models\Customer', 'customer_program_course');
    }
    /* public function __call($method, $args)
    {
        if (!in_array($method, array_keys($this->functions))) {
            throw new BadMethodCallException();
        }

        array_unshift($args, $this->s);

        return call_user_func_array($this->functions[$method], $args);
    }
    public function getAttribute($key)
    {
        // Domaine d'études
        $keys = ['prix_total', 'domaine_etude' => "Domaine d'études", 'programme', 'test_langue','langue', 'ville', 'session', 'diplome', 'type_etablissement'];
        // dd($this, $keys[$key]);
        return $this->properties[$keys[$key]];
    }*/

    public function getPropertiesAttribute($array)
    {
        // dd(json_decode($array, true));
        $res = json_decode($array, true);
        if (isset($res["date_limit_admission"]["value"]) && !empty($res["date_limit_admission"]["value"]) && !is_array($res["date_limit_admission"]["value"])) {
            $date = json_decode($res["date_limit_admission"]["value"], true);
            array_walk($date, function (&$value, $key) {
                $value = intval($value);
            });
            $res["date_limit_admission"]["value"] = $date;
        }
        return $res;
    }


    /*public function setPropertiesAttribute($value)
    {
        $properties = [];

        foreach ($value as $array_item) {
            if (!is_null($array_item['key'])) {
                $properties[] = $array_item;
            }
        }

        $this->attributes['properties'] = json_encode($properties);
        $columns = [
            "Budget",
            "Niveau Scolaire",
            "Note Scolaire",
            "Score",
            "Type d'établissement",
            "Nom d'etablissement",
            "Ville",
            "Session",
            "Date limite d'admission",
            "Diplômes",
            "Domaine d'études",
            "Programmes",
            "Concentration",
            "Langue",
            "Durée",
            "Frais d'admission",
            "Frais scolaires par année",
            "Total des frais scolaires",
            "Lien",
            "Exigences",
            "Test de langue",
            "Objectifs de formation et débouchés du diplôme",
        ];
    }*/
}
