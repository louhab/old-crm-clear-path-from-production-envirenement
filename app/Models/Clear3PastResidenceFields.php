<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clear3PastResidenceFields extends Model
{
    protected $guarded = [];
}
