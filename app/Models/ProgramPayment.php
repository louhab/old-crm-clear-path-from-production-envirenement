<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramPayment extends Model
{
    public function steps()
    {
        return $this->belongsToMany('App\Models\ProgramStep');
    }
    public function customers()
    {
        return $this->belongsToMany('App\Models\Customer', 'program_payment_customer');
    }
    /**
     * Get the customer options.
     */
    /*public function options()
    {
        return $this->hasMany('App\Models\ProgramOption');
    }*/
    //
    public function options()
    {
        return $this->hasMany('App\Models\Service');
    }
}
