<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{

    protected $table = 'feedbacks';

    protected $fillable = [
        'first_name',
        'last_name',
        "phone",
        "conseiller_id",
        "lead_id",
        "degree_of_professionalism",
        "quality_of_presentation",
        "degree_of_mastery",
        "speed",
        "overall",
        "note",
    ];
}
