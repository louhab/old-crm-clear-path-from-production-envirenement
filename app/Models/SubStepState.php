<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class SubStepState extends Model
{
    use LogsActivity;

    protected $fillable = ['name_fr', 'name_en', 'status', 'email_notification_id'];
    protected $guarded = ['deleted_at'];

    /**
     * The steps having this document.
     */
    public function steps()
    {
        return $this->belongsToMany('App\Models\ProgramStep');
    }
    public function substeps()
    {
        return $this->belongsToMany('App\Models\ProgramSubStep');
    }
}
