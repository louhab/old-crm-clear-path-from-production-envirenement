<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Quote extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['user_id', 'customer_id', 'applied_price'];
    protected $guarded = ['deleted_at'];

    protected $appends = ['amount_sum'];

    /**
     * Get the customer.
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the bill services.
     */
    public function services()
    {
        return $this->belongsToMany('App\Models\Service')->withPivot('billed_price', 'qte');
    }

    public function getAmountSumAttribute()
    {
        $sum_ = $this->services()->join('services as s', 's.id', '=', 'quote_service.service_id')
                                    ->join('phases as ph', 'ph.id', '=', 's.phase_id')
                                    ->where('ph.name','not like','%Options%')
                                    ->sum(DB::raw('quote_service.billed_price * quote_service.qte'));

        //return $this->services()->sum(DB::raw('billed_price * qte'));
        return $sum_;
    }
}
