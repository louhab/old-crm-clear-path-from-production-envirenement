<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $fillable = [
        'user_id',
        'customer_case_id',
        'program_step_id',
        'note',
        'canal',
        'view',
        'bo_view',
        'co_updated_at',
        'bo_updated_at'
    ];
}
