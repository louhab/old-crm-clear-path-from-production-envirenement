<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Spatie\Activitylog\Traits\LogsActivity;

class CaseState extends Model
{
    //use LogsActivity;
    protected $guarded = ['deleted_at'];

    // public function getActivitylogOptions(): LogOptions
    // {
    //     return LogOptions::defaults()
    //     ->logOnly(['name', 'text']);
    //     // Chain fluent methods for configuration options
    // }
}
