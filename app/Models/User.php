<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Boolean;
use NotificationChannels\WebPush\HasPushSubscriptions;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements HasMedia
{
    use InteractsWithMedia;
    use Notifiable;
    use SoftDeletes;
    use HasPushSubscriptions;

    protected $softDelete = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'gender', 'password', 'role_id', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /*public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->sharpen(10)
            ->optimize();
    }*/

    /**
     * Get the user role.
     */
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function hasRole($role): bool
    {
        if (strtolower(Auth::user()->role->role_name) == strtolower(str_replace("_", " ", $role)))
            return true;
        return false;
    }

    public function isAdmin(): bool
    {
        if (Auth::user()->role->id == Role::Admin)
            return true;
        return false;
    }

    public function isVerificateur(): bool
    {
        if (Auth::user()->role->id == Role::Verificateur)
            return true;
        return false;
    }

    public function isManager(): bool
    {
        if (Auth::user()->role->id == Role::Manager)
            return true;
        return false;
    }

    public function isFinancier(): bool
    {
        if (Auth::user()->role->id == Role::Financier)
            return true;
        return false;
    }

    public function isConseiller(): bool
    {
        if (Auth::user()->role->id == Role::Conseiller)
            return true;
        return false;
    }

    public function isAgent(): bool
    {
        if (Auth::user()->role->id == Role::Agent)
            return true;
        return false;
    }

    public function isSupport(): bool
    {
        if (Auth::user()->role->id == Role::Support)
            return true;
        return false;
    }

    public function isBackoffice(): bool
    {
        if (Auth::user()->role->id == Role::Backoffice)
            return true;
        return false;
    }

    public function isSuperBackoffice(): bool
    {
        if (Auth::user()->role->id == Role::SuperBackoffice)
            return true;
        return false;
    }
}
