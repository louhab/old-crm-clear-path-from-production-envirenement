<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramStep extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['deleted_at'];

    /**
     * The programs that belong to the step.
     */
    public function programs()
    {
        return $this->belongsToMany('App\Models\Program');
    }
    public function payments()
    {
        return $this->belongsToMany('App\Models\ProgramPayment');
    }

    /**
     * The document types having this step.
     */
    public function docs()
    {
        return $this->belongsToMany('App\Models\DocumentType');
    }
    /**
     * The document types having this step.
     */
    public function substates()
    {
        return $this->belongsToMany('App\Models\SubStepState');
    }
    // public function sub_step_states()
    // {
    //     return $this->belongsToMany('App\Models\CaseState');
    // }

    /**
     * The customer fields having this step.
     */
    public function fields()
    {
        return $this->belongsToMany('App\Models\CustomerField');
    }

    /**
     * The information sheets having this step.
     */
    public function sheets()
    {
        return $this->belongsToMany('App\Models\InformationSheet');
    }

    /**
     * The step states.
     */
    public function states()
    {
        return $this->hasMany('App\Models\StepState');
    }

    /**
     * The substeps.
     */
    public function substeps()
    {
        return $this->hasMany('App\Models\ProgramSubStep', 'step_id');
    }

    /**
     * The program step states.
     */
    public function programStates()
    {
        return $this->hasMany('App\Models\ProgramStepState');
    }

    /**
     * Get the step phase.
     */
    public function phase()
    {
        return $this->belongsTo('App\Models\StepPhase', 'step_phase_id');
    }


    // public function  getSteplabelAttribute() {
    //         $tesst="";
    //         if(session()->get('locale')=='fr')
    //         {
    //           return \App\Models\ProgramStep::pluck('step_label', 'id');
    //         }


    // }

}
