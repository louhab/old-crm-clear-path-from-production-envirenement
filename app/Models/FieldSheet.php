<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FieldSheet extends Model
{
    protected $table = 'customer_field_information_sheet';
    protected $guarded = [];
}
