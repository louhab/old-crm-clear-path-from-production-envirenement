<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IsLanguageTestTaken extends Model
{
    //
    protected $guarded = [];
}
