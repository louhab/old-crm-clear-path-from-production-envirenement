<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentClear extends Model
{
    protected $table = 'document_type_media_sheet';
    protected $guarded = [];
}
