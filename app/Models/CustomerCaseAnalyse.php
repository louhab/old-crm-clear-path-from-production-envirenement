<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerCaseAnalyse extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['deleted_at'];
}
