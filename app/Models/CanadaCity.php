<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CanadaCity extends Model
{
    protected $fillable = ['program_ids'];

    public function getProgramIdsAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setProgramIdsAttribute($value)
    {
        $this->attributes['program_ids'] = json_encode($value);
    }
}
