<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrientationSchool extends Model
{
    protected $fillable = [
        'budget',
        'langue',
        'ville',
        'session',
        'date_limit_admission',
        'niveau_scolaire',
        'note',
        'test_langue',
        'score',
        'domaine_etude',
        'programme',
        'diplome',
        'type_etablissement',
        'nom_etablissement',
        'duree',
        'frais_admission',
        'prix_annuel',
        'prix_total',
        'lien',
        'code_programme',
        'exigence',
        'Analyse',
        'classement_choix',
    ];
    public function customers()
    {
        // get customers by program id
        return $this->belongsToMany('App\Models\Customer', 'customer_orientation_school');
    }
    /*public function getBudgetAttribute($value)
    {
        return intval($value);
    }*/
}
