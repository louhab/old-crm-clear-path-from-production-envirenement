<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use NotificationChannels\WebPush\HasPushSubscriptions;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Customer extends Authenticatable implements HasMedia
{
    use InteractsWithMedia;
    use Notifiable;
    use SoftDeletes;
    use HasPushSubscriptions;

    protected $guard = 'customer';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['campaign','program_id','password'];

    protected $guarded = ['deleted_at'];

    protected $hidden = ['remember_token'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        //parent::boot();
        static::created(function ($customer) {
            if (Auth::check()) {
                $user_id = Auth::id();
            } else {
                $user_id = $customer->lead->conseiller_id;
            }
            // dd($lead);
            if ($customer->lead->program_id > 4) {
                $program = \App\Models\Program::find(1);
                $program_step_id = $program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id;
            } else {
                $program_step_id = $customer->program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id;
            }
            CustomerCase::create([
                'user_id' => $user_id,
                'customer_id' => $customer->id,
                'program_id' => $customer->program_id,
                'program_step_id' => $program_step_id,
            ]);
            // CustomerClear1000Fields::create([
            //     'customer_id' => $customer->id,
            // ]);
        });

        static::updated(function ($customer) {

            if ($customer->program_id != $customer->case->program_id) {
                if ($customer->lead->program_id > 4) {
                    $program = \App\Models\Program::find(1);
                    $program_step_id = $program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id;
                } else {
                    $program_step_id = $customer->program->steps()->withPivot('order')->orderBy('order', 'asc')->first()->id;
                }
                $customer->case->program_id = $customer->program_id;
                $customer->case->program_step_id = $program_step_id;
                $customer->case->save();
            }
        });
        static::deleting(function ($customer) {
            $customer->case->delete();
            // $clear1001 = \App\Models\CustomerClear1000Fields::where('customer_id', $customer->id);
            // if($clear1001)
            //     $clear1001->delete();
            // $clear1004 = \App\Models\CustomerClear1004Fields::where('customer_id', $customer->id)->delete();
            // if($clear1004)
            //     $clear1004->delete();
        });
    }

    /**
     * Get the program.
     */
    public function program()
    {
        return $this->belongsTo('App\Models\Program', 'program_id');
    }

    public function tasks()
    {
        return $this->hasMany('App\Models\ProgramTask');
    }

    /**
     * Get the lead.
     */
    public function lead()
    {
        return $this->belongsTo('App\Models\Lead');
    }

    /**
     * Get the lead country.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * Get the customer case.
     */
    public function case()
    {
        return $this->hasOne('App\Models\CustomerCase');
    }

    /**
     * Get the customer quotes.
     */
    public function quotes()
    {
        return $this->hasMany('App\Models\Quote');
    }

    /**
     * The requested documents.
     */
    public function requestedDocuments()
    {
        return $this->belongsToMany('App\Models\DocumentType', 'document_type_customer');
    }

    /**
     * The requested documents.
     */
    public function payments()
    {
        return $this->belongsToMany('App\Models\ProgramPayment', 'program_payment_customer');
    }
    public function options()
    {
        return $this->belongsToMany('App\Models\Service', 'customer_service');
    }

    public function schools()
    {
        return $this->belongsToMany('App\Models\OrientationSchool', 'customer_orientation_school');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Models\ProgramCourse', 'customer_program_course');
    }

    public function classifications()
    {
        return $this->belongsToMany('App\Models\OccupationalClassification', 'customer_occupational_classifications', 'customer_id', 'cnp_id');
    }
    /**
     * The information sheets having this customer.
     */
    public function sheets()
    {
        return $this->belongsToMany('App\Models\InformationSheet', 'information_sheet_customer');
    }

    /**
     * The media sheets having this customer.
     */
    public function mediaSheets()
    {
        return $this->belongsToMany('App\Models\MediaSheet', 'media_sheet_customer');
    }
    /**
     * Get the user role.
     */
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function hasRole($role): bool
    {
        if (Auth::guard(strtolower($role))->check())
            return true;
        return false;
    }

    public function isCustomer(): bool
    {
        if (Auth::user()->role->id == Role::Customer)
            return true;
        return false;
    }
    // public function getPhoneAttribute($value) {
    //     return $value."tttttt";
    // }
    public function setEmailAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['email'] = NULL;
        } else {
            $this->attributes['email'] = $value;
        }
    }
}
