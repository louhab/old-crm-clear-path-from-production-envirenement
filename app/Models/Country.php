<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Country extends Model
{

    protected $fillable = [
        'code', 'alpha2', 'alpha3', 'name_en_gb', 'name_fr_fr',
    ];
}
