<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class FrenchTest extends Model
{
    use HasTranslations;

    public $translatable = ['test_label'];
}
