<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaSheet extends Model
{
    protected $guarded = [];

    public function substeps()
    {
        return $this->belongsToMany('App\Models\ProgramSubStep')->withPivot(['media_sheet_id', 'program_sub_step_id', 'program_id']);
    }
    /**
     * The customer fields having this sheet.
     */
    public function docs()
    {
        return $this->belongsToMany('App\Models\DocumentType');
    }
    /**
     * The customers having this sheet.
     */
    public function customers()
    {
        return $this->belongsToMany('App\Models\MediaSheet', 'media_sheet_customer');
    }
}
