<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public const Admin = 1;
    public const Manager = 2;
    public const Conseiller = 3;
    public const Agent = 4;
    public const Customer = 5;
    public const Backoffice = 6;
    public const Support = 7;
    public const SuperBackoffice = 8;
    public const Financier = 9;
    public const Verificateur = 10;
}
