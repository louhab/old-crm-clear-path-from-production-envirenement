<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAssign extends Model
{
    protected $guarded = [];

    protected $casts = [
        'users' => 'array'
    ];
}
