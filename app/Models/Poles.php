<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Poles extends Model
{
    protected $fillable = [
        'name', 'address', 'location'
    ];
}
