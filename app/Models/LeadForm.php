<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadForm extends Model
{
    protected $fillable = [
        'title', 'user_id', 'lead_form_marketing_campaign_id', 'token', 'active', 'is_students_form', 'lang', 'redirect_to', "student_program_id", "is_multi_program", "is_for_commercial", "commercial_id"
    ];
    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Models\LeadFormMarketingCampaign', 'lead_form_marketing_campaign_id');
    }
}
