<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerScoreEvaluation extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['deleted_at'];

    /**
     * Get the lead ranking.
     */
    public function ranking()
    {
        return $this->belongsTo('App\Models\Ranking');
    }
}
