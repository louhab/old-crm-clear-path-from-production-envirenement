<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentSubStep extends Model
{
    protected $table = 'document_type_program_sub_step';
    protected $guarded = [];
}
