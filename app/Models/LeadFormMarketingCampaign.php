<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadFormMarketingCampaign extends Model
{
    protected $fillable = [
        'campaign_name',
    ];
}
