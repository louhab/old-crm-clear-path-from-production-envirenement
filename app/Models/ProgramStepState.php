<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramStepState extends Model
{
        /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['deleted_at'];

    /**
     * The cases.
     */
    public function cases()
    {
        return $this->belongsToMany('App\Models\CustomerCase');
    }

    /**
     * The step.
     */
    public function step()
    {
        return $this->belongsTo('App\Models\ProgramStep');
    }

    /**
     * The Program.
     */
    public function program()
    {
        return $this->belongsTo('App\Models\Program');
    }
}
