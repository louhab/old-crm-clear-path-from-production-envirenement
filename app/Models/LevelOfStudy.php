<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LevelOfStudy extends Model
{
    public const NiveauBac = 1;
    public const Bac = 3;
    public const BacPlus = 5;
    public const Licence = 6;
    public const Master = 9;

    public function docs()
    {
        return $this->belongsToMany('App\Models\DocumentType', 'document_type_level_of_study', 'level_id');
    }
}
