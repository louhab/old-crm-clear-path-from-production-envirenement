<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerFieldValue extends Model
{
    protected $guarded = [];
}
