<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramOption extends Model
{
    //
    /**
     * Get the payment.
     */
    public function payment()
    {
        return $this->belongsTo('App\Models\ProgramPayment');
    }
}
