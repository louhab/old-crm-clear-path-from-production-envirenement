<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramSubStep extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['deleted_at'];

    /**
     * The programs that belong to the step.
     */
    public function programs()
    {
        return $this->belongsToMany('App\Models\Program');
    }
    /**
     * Get the step phase.
     */
    public function step()
    {
        return $this->belongsTo('App\Models\ProgramStep', 'step_id');
    }

    public function substates()
    {
        return $this->belongsToMany('App\Models\SubStepState');
    }

    public function fields()
    {
        return $this->belongsToMany('App\Models\CustomerField');
    }
    public function docs()
    {
        return $this->belongsToMany('App\Models\DocumentType');
    }
    public function sheets()
    {
        return $this->belongsToMany('App\Models\InformationSheet');
    }
    public function media_sheets()
    {
        return $this->belongsToMany('App\Models\MediaSheet');
    }
}
