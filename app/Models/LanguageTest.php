<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LanguageTest extends Model
{
    protected $guarded = [];
    protected $fillable = ['program_ids'];

    public function getProgramIdsAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setProgramIdsAttribute($value)
    {
        $this->attributes['program_ids'] = json_encode($value);
    }
}
