<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerProgramCourse extends Model
{
    use SoftDeletes;

    protected $table = 'customer_program_course';
    protected $guarded = ['deleted_at'];
}
