<?php

namespace App\Listeners;

use App\Events\LeadProspectionUpdated;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateLeadProcessedDate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LeadProspectionUpdated  $event
     * @return void
     */
    public function handle(LeadProspectionUpdated $event)
    {
        //
        $event->lead->update(array('processed_message' => $event->message, 'processed_at' => Carbon::now()));
    }
}
