<?php

namespace App\Listeners;

use App\Events\CustomerActionsUpdated;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateCustomerProcessedDate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerActionsUpdated  $event
     * @return void
     */
    public function handle(CustomerActionsUpdated $event)
    {
        $event->customer->update(array('processed_message' => $event->message, 'processed_at' => Carbon::now()));
    }
}
