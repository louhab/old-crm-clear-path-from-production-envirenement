<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Symfony\Component\Mime\Email;

class sendingEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.s
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->data['object'])
            ->view('emails.sub-step-notification')
            ->with(['body' => $this->data['message'], 'name' => $this->data['name'], "elements" => $this->data["elements"]]);
    }
}
