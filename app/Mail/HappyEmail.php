<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class HappyEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('ClearPathCanada | Questionnaire Satisfaction')->view('emails.happy')
            ->with([
                'name' => $this->data['name'],
                'phone' => $this->data['phone'],
                'advisor' => $this->data['advisor'],
                "q1" => $this->data['q1'],
                "q2" => $this->data['q2'],
                "q3" => $this->data['q3'],
                "q4" => $this->data['q4'],
                "q5" => $this->data['q5'],
                "note" => $this->data['note'],
            ]);
    }
}
