<?php

/*
|--------------------------------------------------------------------------
| Load The Cached Routes
|--------------------------------------------------------------------------
|
| Here we will decode and unserialize the RouteCollection instance that
| holds all of the route information for an application. This allows
| us to instantaneously load the entire route map into the router.
|
*/

app('router')->setCompiledRoutes(
    array (
  'compiled' => 
  array (
    0 => false,
    1 => 
    array (
      '/_debugbar/open' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'debugbar.openhandler',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/_debugbar/assets/stylesheets' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'debugbar.assets.css',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/_debugbar/assets/javascript' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'debugbar.assets.js',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/livewire/upload-file' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'livewire.upload-file',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/livewire/livewire.js' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::lDQjK0MnQZZHcJZf',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/livewire/livewire.js.map' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::bavtoAMTTTYSOvCc',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/broadcasting/auth' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::t4VMvuSjxvWWoaxz',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'POST' => 1,
            'HEAD' => 2,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/lang' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'select-language',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/lang-change' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::QfVuRHF3PV3IKsZL',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/getintouch' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'getintouch-store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/feedback' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'feedback',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'feedback-store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/affectation-support-lead' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'affectation-support-lead',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/pandadoc' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::zNv8qXpnxfGMcs9A',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/signeasyshow' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-signeasy-show-iframe',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/testClear' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::CiWR37cm5d0jTdlS',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/welcome' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::nMMc13qo8xxJWh9W',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/getClear11' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'clear11-download',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/login' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'login',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::oQrL6EFNEdY0IY2x',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/logout' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'logout',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/login/customer' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customerLogin',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::giecffyCpOWkRjxg',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/changePassword' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::VXDsC0tj0KPhLTnR',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'changePassword',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/soumissions' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'soumissions-index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'home',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/leads/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'lead-create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/leads' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'leads-index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/cases' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'cases-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/tasks' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'tasks-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/calendar' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'calendar',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/calendar/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'calendar.create',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/calendar/update' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'calendar.update',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/profileUser' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'user-profile',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/sav' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'sav',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/dashboard' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'dashboard',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/dashboard/clients' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'dashboard-clients',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/orientation' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'orientation-home',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/sendMail' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'send-mails',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/histocalls' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'histocalls-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/leadslog' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'leads-log',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/updateOrientation' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::RTwvgmDbOy1MA9Uo',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/production' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'production-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/fields' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'field-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/documents' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'documents-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/steps' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'steps-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/clears' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'clears-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/sheets' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'sheet-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/finances' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'finances',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/quotes' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'quotes',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/bills' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'bills',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/users' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'users-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/conseillers' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'conseillers-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/rotation' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'rotation-config',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/rotation/update' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'rotation-edit',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/formulaires' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'forms-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/programs' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'programs-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/import-leads' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'import-leads',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/integrate-csv' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'integrate-leads',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/payments' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'payment-discount',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/messages' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'messages-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/emailnotifications' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'emailnotifications-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/substepnotifications' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'substepnotifications-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/countries' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'countries-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/services' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'services-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/phases' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'phases-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/campaigns' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'campaigns-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/schoollevels' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'schoollevels-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/tags' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'tags-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/immigrants' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'immigrants-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/customers' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customers-index',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/customers/create' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-create',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/students' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'students-list',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/program-course' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::fPjj7Xijr5BQeZUF',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/logs' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'logs',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/analytics' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'analytics',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/analytics/clients' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'analytics-clients',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/agents' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::9AEdZc4fAdXBHYW4',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getPrograms' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::nCAEpGefEJQUW1qA',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getPhases' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::TNTvwAX3sWsK7Jic',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getSteps' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::O1YqBb18S0FjfYsa',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getCampaigns' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::plzsK9K0DU75fonz',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getLeadStatus' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::u0hNxwj87Hz7Q3GO',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/clear1002' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::budmXTSbjdhBzVAp',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/histocalls' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'histocalls',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::LQDIKHIx5UOYZDCJ',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/bills' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::wgOY1cn6mKJVRrUQ',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::hUFxHjC2KGK9TNHW',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/finances' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::CDKD1ztbRO1m8uDD',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/finances-stats' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::GbOYwr1RyyLrVncg',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/quotes' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::mWvzgiQJkeTwWgVe',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::BYGHK8h6QbbdZ6Ot',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/programs' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'programs',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/users' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'users',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::3Cc9rzpcgB5cg5lD',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/conseillers' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'conseillers',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::MjmWvY8IT9bbqIRO',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/messages' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'Messages',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::GqWkcMwYZn9TkrbV',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/emailnotifications' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'EmailNotifications',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::d6Gjjw9NjW1jfu4l',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/substepnotifications' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'SubStepNotifications',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/countries' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'countries',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::pfoHh7Mm6D13FIFb',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/services' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'services',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::JhXWumIpK14DWGn2',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getservices' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::CqAR6mYZqSbcVww6',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/phases' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'phases',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::kXMIKbjThqbP0muD',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/campaigns' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'campaigns',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::rNzY1u29AU6z7mag',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/schoollevels' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'schoollevel',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::L4NfJfz28A2G6RsS',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/tags' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'tag',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::kOs2UMCohTm0ahLD',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/steps' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::Ydd4lkvDJzK4l7HK',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/clears' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::dGu89xbRwkUwfTOK',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/sheets' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ZvYkHY6Cb7a7nHZJ',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/fields' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::hUxbYN8YYSz3IWgS',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::35oH2SuykNYM3kqM',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/documents' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::1kTxXy1Y5RTp5V7P',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::wMcMB7YVPTylEItW',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/leadsforms' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'leadsforms',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::39EsBkMwUcfl72kT',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/customerBackoffice' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ebLIzljg6Nbgn27F',
          ),
          1 => NULL,
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/leads' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'lead-store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'leads',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/leadConseiller' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::Y3MaGXUgWACWovAX',
          ),
          1 => NULL,
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getLeads' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'getLeads',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getCountries' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::w5BuaUUQvwzAfYLu',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/first-meeting-slides' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::If2lDX6L9IRQADge',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/cases' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'cases',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/admissions' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'admissions',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/production' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'production',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/profileUpdate' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'profile-update',
          ),
          1 => NULL,
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/calendar' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::5BVcHPPrqyGStsAf',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/admission-states' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::UA9CnYwl8ESioXuy',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/immigrants' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'immigrants',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::krhlQpLnYmqwwTSJ',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/customers' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customers',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'customer-store',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/discounts' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::Wh5mPeYl50uLvLC0',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getCustomers' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'getCustomers',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getClients' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'getClients',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getPayments' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::k3YVuZvtA88p4upW',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getAmount' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::csz7rJg6ayqhjlit',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/discount' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::GmtluzJaNJ1PFER2',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/getPaymentMethods' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::7vgl7eHJBlRdGrcI',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/task-fees' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'update-fees',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/notifications' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::nIEeN8OoH1XNMCif',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::PIq4uzzb8illyWkP',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/notifications/mark-all-read' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::GLWLiEZqqJhyUHUY',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/subscriptions' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::seovvsTYMPBVycDW',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/subscriptions/delete' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::1YuDIQ9Q6XsReWRk',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/pdf/enableQuote' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'enable-quote',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/myinbox' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-inbox',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/mycase' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-case',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/myquotes' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-myquotes',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/mybills' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-mybills',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/profile' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-profile',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/customer/api/v1/notifications' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::snQaYxz4h30YjgY0',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::LAviWr4xg1FlV1Dz',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/customer/api/v1/notifications/mark-all-read' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::74CjBQAnOwjDqFmR',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/customer/api/v1/subscriptions' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::HWcD1ofhbDMt2WJD',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/customer/api/v1/subscriptions/delete' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::7gcqfTVAY4tz0sDu',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/customer/api/v1/customer' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-profile-update',
          ),
          1 => NULL,
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/getAvailableDateTime' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'getAvailableDateTime',
          ),
          1 => NULL,
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/v1/hr-lead' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::MOi8m1UKh9dEgudr',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      '/api/stripe/webhook' => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::6Iz8ErzLuddqrnH3',
          ),
          1 => NULL,
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
    ),
    2 => 
    array (
      0 => '{^(?|/_debugbar/c(?|lockwork/([^/]++)(*:39)|ache/([^/]++)(?:/([^/]++))?(*:73))|/l(?|ivewire/(?|message/([^/]++)(*:113)|preview\\-file/([^/]++)(*:143))|ead(?|s/([^/]++)(*:168)|/([^/]++)(*:185)))|/getintouch/([^/]++)(*:215)|/s(?|igneasy/redirect/([^/]++)(*:253)|oumission(?|/(?|([^/]++)(*:285)|status/([^/]++)(*:308))|_/([^/]++)(*:327))|heet(?|s/([^/]++)(?|/([^/]++)(?:/([^/]++))?(*:379)|(*:387))|/media/([^/]++)/([^/]++)(*:420))|teps/([^/]++)(*:442))|/m(?|edia(?|s/([^/]++)(*:473)|/([^/]++)(*:490))|ycase/([^/]++)(*:513))|/c(?|ustomer(?|/(?|getMedia/([^/]++)/([^/]++)(*:567)|sheets/(?|media/([^/]++)/([^/]++)(*:608)|([0-9]+)/([^/]++)(?:/([^/]++))?(*:647))|media/([^/]++)(?|(*:673))|api/v1/(?|signeasy/(?|getsigningurl/([^/]++)/([^/]++)(*:735)|download/([^/]++)/([^/]++)(*:769))|customers/(?|media/([^/]++)/([^/]++)(*:814)|([^/]++)(*:830))|notifications/([^/]++)/(?|read(*:869)|dismiss(*:884))))|s/([^/]++)(*:905))|a(?|ses(?|/([^/]++)(*:933)|_/([^/]++)(*:951))|lendar/(?|destroy/([^/]++)(*:986)|getCustomer/([^/]++)(*:1014)))|lear/([^/]++)(*:1038))|/first\\-meeting/([^/]++)(*:1072)|/p(?|df/(?|bill/([^/]++)(*:1105)|quote/([^/]++)(*:1128)|viewQuote/([^/]++)(*:1155)|my(?|bill/([^/]++)(*:1182)|quote/([^/]++)(*:1205)))|ro(?|duction/([^/]++)(*:1237)|gram/([^/]++)(*:1259)))|/orientation/([^/]++)(*:1291)|/document/([^/]++)(*:1318)|/tags/([^/]++)(*:1341)|/api/v1/(?|users/([^/]++)(?|(*:1378)|(*:1387))|histocalls/(?|([^/]++)(*:1419)|lead/([^/]++)(*:1441)|([^/]++)(*:1458))|messages/([^/]++)(?|(*:1488)|(*:1497))|emailnotifications/([^/]++)(?|(*:1537)|(*:1546))|c(?|o(?|untries/([^/]++)(?|(*:1583)|(*:1592))|nseiller(?|/([^/]++)(*:1622)|s/([^/]++)(?|(*:1644)))|mments/(?|([^/]++)(?|(*:1676)|(?:/([^/]++))?(*:1699))|edit/([^/]++)/([^/]++)(?:/([^/]++))?(*:1745)))|a(?|mpaigns/([^/]++)(?|(*:1779)|(*:1788))|se(?|/([^/]++)/([^/]++)(*:1821)|s/(?|convert/([^/]++)(*:1851)|([^/]++)(?|(*:1871)|/([^/]++)(*:1889)))))|ustomer(?|data/([^/]++)(*:1925)|\\-(?|meet/([^/]++)/([^/]++)(*:1961)|orientation/([^/]++)/([^/]++)(*:1999))|/media/([^/]++)(*:2024)|s/(?|([^/]++)(*:2046)|clear(?|100(?|0/([^/]++)(*:2079)|2/([^/]++)(*:2098)|4/([^/]++)(*:2117))|3/([^/]++)(*:2137))|account/(?|([^/]++)(*:2166)|deactivate/([^/]++)(*:2194))|media/([^/]++)/([^/]++)(?|(*:2230)))))|s(?|ervices/([^/]++)(?|(*:2266)|(*:2275))|choollevels/([^/]++)(?|(*:2308)|(*:2317))|avePhone/([^/]++)(*:2344)|oumission/convert/([^/]++)(*:2379)|ubstepnotifications/([^/]++)(*:2416)|heet\\-fields/([^/]++)(?|(*:2449)|(*:2458)|(*:2467)|(*:2476))|igneasy/(?|([^/]++)/([^/]++)(*:2514)|sendrequest/([^/]++)/([^/]++)(*:2552)|download/([^/]++)/([^/]++)(*:2587))|tates/([^/]++)(?:/([^/]++))?(*:2625))|p(?|hases/([^/]++)(?|(*:2656)|(*:2665))|ayments/([^/]++)(?|(*:2694)|/([^/]++)(*:2712))|rogram\\-course/([^/]++)(?|(*:2748)|(*:2757)))|bills/([^/]++)(?|(*:2785))|quotes/([^/]++)(?|(*:2813))|leads(?|forms/(?|([^/]++)(?|(*:2851)|(*:2860))|setactivestate(*:2884))|data/([^/]++)(*:2907)|/(?|switch/([^/]++)(*:2935)|assign/([^/]++)(*:2959)|([^/]++)(*:2976)|convert/([^/]++)(*:3001)|generate/([^/]++)(*:3027)|media/([^/]++)/([^/]++)(*:3059)|([^/]++)(?|(*:3079)|(*:3088)|/([^/]++)(*:3106))))|t(?|a(?|sks/([^/]++)(?|(*:3141)|(*:3150))|g(?|s/([^/]++)(?|(*:3177)|(*:3186))|/leads/([^/]++)(*:3211)))|oggle(?|linkvisibility/([^/]++)/([^/]++)/([^/]++)(*:3271)|sheetvisibility/([^/]++)/([^/]++)(*:3313)|me(?|dia(?|sheetvisibility/([^/]++)/([^/]++)(*:3366)|property/([^/]++)(*:3392))|et/([^/]++)(*:3413))))|f(?|i(?|nances/([^/]++)(*:3448)|elds/([^/]++)(?|(*:3473)|(*:3482)))|orms/([^/]++)(*:3506))|do(?|cument(?|\\-(?|steps/([^/]++)(?|/steps(?|(*:3561)|(*:3570)|(*:3579))|(*:3589)|(*:3598)|(*:3607))|clears/([^/]++)(?|/([^/]++)(*:3644)|(?:/([^/]++))?(?|(*:3670))|(*:3680)))|s/([^/]++)(?|(*:3704)|(*:3713)))|wnloadpdf/([^/]++)(*:3742))|immigrants/([^/]++)(*:3771)|getCallStates/([^/]++)(*:3802)|addrequesteddocument/([^/]++)/([^/]++)/([^/]++)(*:3858)|removerequesteddocument/([^/]++)/([^/]++)/([^/]++)(*:3917)|notifications/([^/]++)/(?|read(*:3956)|dismiss(*:3972))))/?$}sDu',
    ),
    3 => 
    array (
      39 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'debugbar.clockwork',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      73 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'debugbar.cache.delete',
            'tags' => NULL,
          ),
          1 => 
          array (
            0 => 'key',
            1 => 'tags',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      113 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'livewire.message',
          ),
          1 => 
          array (
            0 => 'name',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      143 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'livewire.preview-file',
          ),
          1 => 
          array (
            0 => 'filename',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      168 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'lead-edit',
          ),
          1 => 
          array (
            0 => 'lead',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      185 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'lead-view',
          ),
          1 => 
          array (
            0 => 'lead',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      215 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'getintouch',
          ),
          1 => 
          array (
            0 => 'token',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      253 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-signeasy-redirect-page',
          ),
          1 => 
          array (
            0 => 'customer',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      285 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'soumission-view',
          ),
          1 => 
          array (
            0 => 'customer_case',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      308 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'soumission-update-state',
          ),
          1 => 
          array (
            0 => 'customer_case',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      327 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::wp5sP2LT9yeQXAj5',
          ),
          1 => 
          array (
            0 => 'customer_case',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      379 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'edit-information-sheet',
            'join' => NULL,
          ),
          1 => 
          array (
            0 => 'customer_case',
            1 => 'information_sheet',
            2 => 'join',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      387 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'sheet-view',
          ),
          1 => 
          array (
            0 => 'sheet',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      420 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'edit-media-sheet',
          ),
          1 => 
          array (
            0 => 'customer_case',
            1 => 'media_sheet',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      442 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'step-view',
          ),
          1 => 
          array (
            0 => 'step',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      473 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'get-media',
          ),
          1 => 
          array (
            0 => 'media',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      490 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'view-media',
          ),
          1 => 
          array (
            0 => 'media',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      513 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-mycase',
          ),
          1 => 
          array (
            0 => 'program_step',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      567 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::r3Ykuz1BwUJgLDQJ',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'collection',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      608 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-edit-media-sheet',
          ),
          1 => 
          array (
            0 => 'customer_case',
            1 => 'media_sheet',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      647 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-edit-information-sheet',
            'join' => NULL,
          ),
          1 => 
          array (
            0 => 'customer_case',
            1 => 'information_sheet',
            2 => 'join',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      673 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-remove-media',
          ),
          1 => 
          array (
            0 => 'media',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'customer-view-media',
          ),
          1 => 
          array (
            0 => 'media',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      735 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-signeasy-get-signing-url',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'collectionName',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      769 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-signeasy-download-signed-document',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'collectionName',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      814 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-add-media',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'collectionName',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      830 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-customer-update',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      869 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::H0WYrnx1ViYT9l0s',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PATCH' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      884 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::xb7SXJn5vcDNKmYA',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      905 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-edit',
          ),
          1 => 
          array (
            0 => 'customer',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      933 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'case-edit',
          ),
          1 => 
          array (
            0 => 'customer_case',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      951 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::RRAIsLTJ9aBMo7I0',
          ),
          1 => 
          array (
            0 => 'customer_case',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      986 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'calendar.destroy',
          ),
          1 => 
          array (
            0 => 'calendar',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1014 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'calender.customer',
          ),
          1 => 
          array (
            0 => 'calendrier_id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1038 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'document-view',
          ),
          1 => 
          array (
            0 => 'sheet',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1072 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::f9aWelWmmk0XDwbK',
          ),
          1 => 
          array (
            0 => 'customerCase',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1105 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::jEvMRFTeO7eTMLzK',
          ),
          1 => 
          array (
            0 => 'bill',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1128 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'quote-download',
          ),
          1 => 
          array (
            0 => 'quote',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1155 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'quote-view',
          ),
          1 => 
          array (
            0 => 'quote',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1182 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::EqRDk1ckBe1PZtiU',
          ),
          1 => 
          array (
            0 => 'bill',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1205 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::9l58lPTQAYLwcIzj',
          ),
          1 => 
          array (
            0 => 'quote',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1237 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'production-view',
          ),
          1 => 
          array (
            0 => 'user',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1259 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::rQfEN3pBSqBaAjpE',
          ),
          1 => 
          array (
            0 => 'program',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1291 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'orientation-view',
          ),
          1 => 
          array (
            0 => 'program',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1318 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'doc-view',
          ),
          1 => 
          array (
            0 => 'document',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1341 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'tag-leads',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1378 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::XQZHaiMc9I1Q5iyk',
          ),
          1 => 
          array (
            0 => 'user',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1387 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::uxHdZqpy9BvAUODp',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1419 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::MbgHxo1RasU0wWAa',
          ),
          1 => 
          array (
            0 => 'histocall',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1441 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ZahPyZAoWVtbfsrh',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1458 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::bDvVBHIvZLhV3ELC',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1488 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::IC4ua2a2TzpQPRhW',
          ),
          1 => 
          array (
            0 => 'message',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1497 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::av5VYEiqNHiIDLAN',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1537 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::g44WAaBgGfBpiRGE',
          ),
          1 => 
          array (
            0 => 'emailnotification',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1546 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::qr1aO5vXkmcv4eoM',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1583 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::pFR4AC4zXofNEGii',
          ),
          1 => 
          array (
            0 => 'country',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1592 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::pjd6Qq1sJeu8m73Y',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1622 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::l0MXX7BhvDCS8Q9F',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1644 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::WFvhMbg3K5Xqfz1h',
          ),
          1 => 
          array (
            0 => 'user',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::YnhDNZCBbqUKYxj8',
          ),
          1 => 
          array (
            0 => 'user',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1676 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'comments',
          ),
          1 => 
          array (
            0 => 'customer_case',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1699 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'store-comment',
            'canal' => NULL,
          ),
          1 => 
          array (
            0 => 'customer_case',
            1 => 'canal',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1745 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'update-comment',
            'canal' => NULL,
          ),
          1 => 
          array (
            0 => 'customer_case',
            1 => 'comment',
            2 => 'canal',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1779 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::xUfoKOxKDKPsSoOM',
          ),
          1 => 
          array (
            0 => 'campaign',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1788 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::gAWmSfWxzDnnKKBm',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1821 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::NlCiMP5BRSZIBYRo',
          ),
          1 => 
          array (
            0 => 'customer_case',
            1 => 'status',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1851 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::4lsieHqwMhaVq3dr',
          ),
          1 => 
          array (
            0 => 'customer',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1871 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'case-update',
          ),
          1 => 
          array (
            0 => 'customer_case',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1889 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'step-state-update',
          ),
          1 => 
          array (
            0 => 'customer_case',
            1 => 'program_step',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1925 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::TkJMLQfbPJJVMiWO',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1961 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::5Txht2oo6AiechSB',
          ),
          1 => 
          array (
            0 => 'id',
            1 => 'meet',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      1999 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::3qFOcPYonAWz2dc4',
          ),
          1 => 
          array (
            0 => 'id',
            1 => 'orientationId',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2024 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'remove-media',
          ),
          1 => 
          array (
            0 => 'media',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2046 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-update',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2079 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-update-clear1000',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2098 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-update-clear1002',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2117 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-update-clear1004',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2137 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-update-clear3',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2166 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-account-activation',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2194 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-account-deactivation',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2230 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'customer-delete-media',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'collectionName',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'add-media',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'collectionName',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2266 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::DqDHqiav4Buv4lMD',
          ),
          1 => 
          array (
            0 => 'service',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2275 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::4HBo9PNy4RejsgEc',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2308 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::kr2sCwnZLxWbiTrl',
          ),
          1 => 
          array (
            0 => 'schollevel',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2317 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ety3DgXePlGwo0t2',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2344 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::HBhsnCUhixTa60VJ',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2379 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::Jk6xusZAujWk9zFn',
          ),
          1 => 
          array (
            0 => 'customer',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2416 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ZaJmTuhZgXexF6I9',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2449 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::dfPiEOsYoh47lkI1',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2458 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::bvjLHqDfkPG7gMrc',
          ),
          1 => 
          array (
            0 => 'sheet_id',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2467 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ai6sRcRgjHGS8HJe',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2476 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::twmuP3lREefMhTEc',
          ),
          1 => 
          array (
            0 => 'field_sheet',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2514 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'signeasy-import-document',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'collectionName',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2552 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'signeasy-send-signing-request',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'collectionName',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2587 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'signeasy-download-signed-document',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'collectionName',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2625 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'sub-step-state-update',
            'method' => NULL,
          ),
          1 => 
          array (
            0 => 'case_state',
            1 => 'method',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2656 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::wQ09FxkIHr3NTJkI',
          ),
          1 => 
          array (
            0 => 'phase',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2665 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::4ZrqgfCa5dX2L22q',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2694 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::4Gg6PUn2mmq3Nfn4',
          ),
          1 => 
          array (
            0 => 'case_state',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2712 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::rzrAbOiGVy7XqzeX',
          ),
          1 => 
          array (
            0 => 'case_state',
            1 => 'method',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2748 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::k8FewZYi3vMstPqU',
          ),
          1 => 
          array (
            0 => 'field_name',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2757 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::N8zmluORJ22JqNWr',
          ),
          1 => 
          array (
            0 => 'program',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::w1eYJe62C6hrFWPp',
          ),
          1 => 
          array (
            0 => 'program',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2785 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::QPSddo08rx1LUOZW',
          ),
          1 => 
          array (
            0 => 'bill',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::H76fZCee8ZHrOL7F',
          ),
          1 => 
          array (
            0 => 'bill',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2813 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::9Klq6Kp73YJbMc2E',
          ),
          1 => 
          array (
            0 => 'quote',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::uU1friQZCzwdODx0',
          ),
          1 => 
          array (
            0 => 'quote',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2851 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::1xCjqXx6lzyEFMuF',
          ),
          1 => 
          array (
            0 => 'leadform',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2860 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::c8QlPpb75xcGN63o',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2884 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::qjijdqT5ixgB3Glj',
          ),
          1 => 
          array (
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      2907 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::vstO56UHlaHqefod',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2935 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::jFLa2ZVwpPTX6KIL',
          ),
          1 => 
          array (
            0 => 'lead',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2959 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::LH72xClOpyvR6xQD',
          ),
          1 => 
          array (
            0 => 'lead',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      2976 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::cMm0xC1yuuIxZw5x',
          ),
          1 => 
          array (
            0 => 'lead',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3001 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::7nyWmZNizXNF1W6n',
          ),
          1 => 
          array (
            0 => 'lead',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3027 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'regen-docs',
          ),
          1 => 
          array (
            0 => 'lead',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3059 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'lead-delete-media',
          ),
          1 => 
          array (
            0 => 'lead',
            1 => 'collectionName',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3079 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'lead-convert',
          ),
          1 => 
          array (
            0 => 'lead',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3088 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'lead-update',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3106 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ab7bcebQunTVV8lc',
          ),
          1 => 
          array (
            0 => 'lead',
            1 => 'status',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3141 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'store-task',
          ),
          1 => 
          array (
            0 => 'customer_case',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3150 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::LEcMqeAhFMoeuaDG',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3177 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::B7z2kADbExa8D4ja',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::OneenQalJfNVeRMw',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3186 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'store-tags',
          ),
          1 => 
          array (
            0 => 'customer_case',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3211 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'tags-leads',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3271 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'toggle-link-visibility',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'document_type',
            2 => 'program_step',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3313 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'toggle-sheet-visibility',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'information_sheet',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3366 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'toggle-media-sheet-visibility',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'media_sheet',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3392 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'toggle-media-property',
          ),
          1 => 
          array (
            0 => 'media',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3413 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'toggle-meet',
          ),
          1 => 
          array (
            0 => 'customer',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3448 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::kB2qk1qRPD17321T',
          ),
          1 => 
          array (
            0 => 'payment_id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3473 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::v9mHNel7bACteWKb',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3482 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::ABtin7O2EezJIk19',
          ),
          1 => 
          array (
            0 => 'field',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3506 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::KLJJPC1VkH9BbDft',
          ),
          1 => 
          array (
            0 => 'campaign',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3561 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::OlyGC4P1voTjak5d',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::M0ggQupUlFsnAXEh',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      3570 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::MwZL5tdQUmB4EraH',
          ),
          1 => 
          array (
            0 => 'step_id',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      3579 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::Wwz3t6VRbpB66rUl',
          ),
          1 => 
          array (
            0 => 'document_step',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      3589 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::oOdCsOuT4DhjRWns',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::dDDZnCVD7dSlsLq9',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3598 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::21Treuf65WuybMa4',
          ),
          1 => 
          array (
            0 => 'document_id',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3607 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::M5r8Wgul6dfYL9JK',
          ),
          1 => 
          array (
            0 => 'document_step',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3644 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::kYu3Qh6uTkv2wuNG',
          ),
          1 => 
          array (
            0 => 'id',
            1 => 'column',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3670 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::3gNOof0sPhfSL0JX',
            'column' => NULL,
          ),
          1 => 
          array (
            0 => 'id',
            1 => 'column',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
        1 => 
        array (
          0 => 
          array (
            '_route' => 'generated::HJZdpwLTi0MVlnFM',
            'column' => NULL,
          ),
          1 => 
          array (
            0 => 'id',
            1 => 'column',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3680 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::52kWurGomfAUmx6Z',
          ),
          1 => 
          array (
            0 => 'document_clear',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3704 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::9IiIPEhWZH6P3Z8w',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3713 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::plGEIGPEnya747YO',
          ),
          1 => 
          array (
            0 => 'document',
          ),
          2 => 
          array (
            'DELETE' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3742 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'setasign-test',
          ),
          1 => 
          array (
            0 => 'customer',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3771 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::4iU0Pr9yOoARvczF',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PUT' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3802 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'getCallStates',
          ),
          1 => 
          array (
            0 => 'lead',
          ),
          2 => 
          array (
            'GET' => 0,
            'HEAD' => 1,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3858 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'add-requested-document',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'document_type',
            2 => 'program_step',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3917 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'remove-requested-document',
          ),
          1 => 
          array (
            0 => 'customer',
            1 => 'document_type',
            2 => 'program_step',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => true,
          6 => NULL,
        ),
      ),
      3956 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::gxtFvCz28ymUFkV0',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'PATCH' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
      ),
      3972 => 
      array (
        0 => 
        array (
          0 => 
          array (
            '_route' => 'generated::1gDcrZQRZdkeinTX',
          ),
          1 => 
          array (
            0 => 'id',
          ),
          2 => 
          array (
            'POST' => 0,
          ),
          3 => NULL,
          4 => false,
          5 => false,
          6 => NULL,
        ),
        1 => 
        array (
          0 => NULL,
          1 => NULL,
          2 => NULL,
          3 => NULL,
          4 => false,
          5 => false,
          6 => 0,
        ),
      ),
    ),
    4 => NULL,
  ),
  'attributes' => 
  array (
    'debugbar.openhandler' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => '_debugbar/open',
      'action' => 
      array (
        'domain' => NULL,
        'middleware' => 
        array (
          0 => 'Barryvdh\\Debugbar\\Middleware\\DebugbarEnabled',
        ),
        'uses' => 'Barryvdh\\Debugbar\\Controllers\\OpenHandlerController@handle',
        'as' => 'debugbar.openhandler',
        'controller' => 'Barryvdh\\Debugbar\\Controllers\\OpenHandlerController@handle',
        'namespace' => 'Barryvdh\\Debugbar\\Controllers',
        'prefix' => '_debugbar',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'debugbar.clockwork' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => '_debugbar/clockwork/{id}',
      'action' => 
      array (
        'domain' => NULL,
        'middleware' => 
        array (
          0 => 'Barryvdh\\Debugbar\\Middleware\\DebugbarEnabled',
        ),
        'uses' => 'Barryvdh\\Debugbar\\Controllers\\OpenHandlerController@clockwork',
        'as' => 'debugbar.clockwork',
        'controller' => 'Barryvdh\\Debugbar\\Controllers\\OpenHandlerController@clockwork',
        'namespace' => 'Barryvdh\\Debugbar\\Controllers',
        'prefix' => '_debugbar',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'debugbar.assets.css' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => '_debugbar/assets/stylesheets',
      'action' => 
      array (
        'domain' => NULL,
        'middleware' => 
        array (
          0 => 'Barryvdh\\Debugbar\\Middleware\\DebugbarEnabled',
        ),
        'uses' => 'Barryvdh\\Debugbar\\Controllers\\AssetController@css',
        'as' => 'debugbar.assets.css',
        'controller' => 'Barryvdh\\Debugbar\\Controllers\\AssetController@css',
        'namespace' => 'Barryvdh\\Debugbar\\Controllers',
        'prefix' => '_debugbar',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'debugbar.assets.js' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => '_debugbar/assets/javascript',
      'action' => 
      array (
        'domain' => NULL,
        'middleware' => 
        array (
          0 => 'Barryvdh\\Debugbar\\Middleware\\DebugbarEnabled',
        ),
        'uses' => 'Barryvdh\\Debugbar\\Controllers\\AssetController@js',
        'as' => 'debugbar.assets.js',
        'controller' => 'Barryvdh\\Debugbar\\Controllers\\AssetController@js',
        'namespace' => 'Barryvdh\\Debugbar\\Controllers',
        'prefix' => '_debugbar',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'debugbar.cache.delete' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => '_debugbar/cache/{key}/{tags?}',
      'action' => 
      array (
        'domain' => NULL,
        'middleware' => 
        array (
          0 => 'Barryvdh\\Debugbar\\Middleware\\DebugbarEnabled',
        ),
        'uses' => 'Barryvdh\\Debugbar\\Controllers\\CacheController@delete',
        'as' => 'debugbar.cache.delete',
        'controller' => 'Barryvdh\\Debugbar\\Controllers\\CacheController@delete',
        'namespace' => 'Barryvdh\\Debugbar\\Controllers',
        'prefix' => '_debugbar',
        'where' => 
        array (
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'livewire.message' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'livewire/message/{name}',
      'action' => 
      array (
        'uses' => 'Livewire\\Controllers\\HttpConnectionHandler@__invoke',
        'controller' => 'Livewire\\Controllers\\HttpConnectionHandler',
        'as' => 'livewire.message',
        'middleware' => 
        array (
          0 => 'web',
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'livewire.upload-file' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'livewire/upload-file',
      'action' => 
      array (
        'uses' => 'Livewire\\Controllers\\FileUploadHandler@handle',
        'controller' => 'Livewire\\Controllers\\FileUploadHandler@handle',
        'as' => 'livewire.upload-file',
        'middleware' => 
        array (
          0 => 'web',
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'livewire.preview-file' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'livewire/preview-file/{filename}',
      'action' => 
      array (
        'uses' => 'Livewire\\Controllers\\FilePreviewHandler@handle',
        'controller' => 'Livewire\\Controllers\\FilePreviewHandler@handle',
        'as' => 'livewire.preview-file',
        'middleware' => 
        array (
          0 => 'web',
        ),
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::lDQjK0MnQZZHcJZf' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'livewire/livewire.js',
      'action' => 
      array (
        'uses' => 'Livewire\\Controllers\\LivewireJavaScriptAssets@source',
        'controller' => 'Livewire\\Controllers\\LivewireJavaScriptAssets@source',
        'as' => 'generated::lDQjK0MnQZZHcJZf',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::bavtoAMTTTYSOvCc' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'livewire/livewire.js.map',
      'action' => 
      array (
        'uses' => 'Livewire\\Controllers\\LivewireJavaScriptAssets@maps',
        'controller' => 'Livewire\\Controllers\\LivewireJavaScriptAssets@maps',
        'as' => 'generated::bavtoAMTTTYSOvCc',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::t4VMvuSjxvWWoaxz' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'POST',
        2 => 'HEAD',
      ),
      'uri' => 'broadcasting/auth',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'auth:api',
          1 => 'auth:customer',
        ),
        'uses' => '\\Illuminate\\Broadcasting\\BroadcastController@authenticate',
        'controller' => '\\Illuminate\\Broadcasting\\BroadcastController@authenticate',
        'namespace' => NULL,
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::t4VMvuSjxvWWoaxz',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'select-language' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'lang',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\SelectLanguageController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\SelectLanguageController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'select-language',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::QfVuRHF3PV3IKsZL' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'lang-change',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SelectLanguageController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SelectLanguageController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::QfVuRHF3PV3IKsZL',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'getintouch' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'getintouch/{token}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\GetInTouchController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\GetInTouchController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'getintouch',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'getintouch-store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'getintouch',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\GetInTouchController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\GetInTouchController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'getintouch-store',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'feedback' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'feedback',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\FeedbacksController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\FeedbacksController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'feedback',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'feedback-store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'feedback',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\FeedbacksController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\FeedbacksController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'feedback-store',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'affectation-support-lead' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'affectation-support-lead',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\AffectationSupportLeadController@update',
        'controller' => 'App\\Http\\Controllers\\Web\\AffectationSupportLeadController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'affectation-support-lead',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-signeasy-redirect-page' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'signeasy/redirect/{customer}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\DocumentSignatureController@showPage',
        'controller' => 'App\\Http\\Controllers\\Web\\DocumentSignatureController@showPage',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-signeasy-redirect-page',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::zNv8qXpnxfGMcs9A' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'pandadoc',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\DocumentSignatureController@toggleSigned',
        'controller' => 'App\\Http\\Controllers\\Web\\DocumentSignatureController@toggleSigned',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::zNv8qXpnxfGMcs9A',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-signeasy-show-iframe' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'signeasyshow',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\DocumentSignatureController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\DocumentSignatureController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-signeasy-show-iframe',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'get-media' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'medias/{media}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\DownloadMediaController@show',
        'controller' => 'App\\Http\\Controllers\\Web\\DownloadMediaController@show',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'get-media',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::CiWR37cm5d0jTdlS' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'testClear',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@testClearPDF',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@testClearPDF',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::CiWR37cm5d0jTdlS',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::nMMc13qo8xxJWh9W' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'welcome',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\GetInTouchController@welcome',
        'controller' => 'App\\Http\\Controllers\\Web\\GetInTouchController@welcome',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::nMMc13qo8xxJWh9W',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'clear11-download' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'getClear11',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\MediaController@getClear11',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\MediaController@getClear11',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'clear11-download',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'login' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'login',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Auth\\LoginController@showLoginForm',
        'controller' => 'App\\Http\\Controllers\\Auth\\LoginController@showLoginForm',
        'namespace' => 'App\\Http\\Controllers\\Auth',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'login',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::oQrL6EFNEdY0IY2x' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'login',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Auth\\LoginController@login',
        'controller' => 'App\\Http\\Controllers\\Auth\\LoginController@login',
        'namespace' => 'App\\Http\\Controllers\\Auth',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::oQrL6EFNEdY0IY2x',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'logout' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'logout',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Auth\\LoginController@logout',
        'controller' => 'App\\Http\\Controllers\\Auth\\LoginController@logout',
        'namespace' => 'App\\Http\\Controllers\\Auth',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'logout',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customerLogin' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'login/customer',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Auth\\LoginController@showCustomerLoginForm',
        'controller' => 'App\\Http\\Controllers\\Auth\\LoginController@showCustomerLoginForm',
        'namespace' => 'App\\Http\\Controllers\\Auth',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customerLogin',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::giecffyCpOWkRjxg' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'login/customer',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Auth\\LoginController@customerLogin',
        'controller' => 'App\\Http\\Controllers\\Auth\\LoginController@customerLogin',
        'namespace' => 'App\\Http\\Controllers\\Auth',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::giecffyCpOWkRjxg',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::VXDsC0tj0KPhLTnR' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'changePassword',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Auth\\ResetPasswordController@showChangePasswordForm',
        'controller' => 'App\\Http\\Controllers\\Auth\\ResetPasswordController@showChangePasswordForm',
        'namespace' => 'App\\Http\\Controllers\\Auth',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::VXDsC0tj0KPhLTnR',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'changePassword' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'changePassword',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Auth\\ResetPasswordController@changePassword',
        'controller' => 'App\\Http\\Controllers\\Auth\\ResetPasswordController@changePassword',
        'namespace' => 'App\\Http\\Controllers\\Auth',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'changePassword',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'view-media' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'media/{media}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\PDFController@view',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\PDFController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'view-media',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::r3Ykuz1BwUJgLDQJ' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'customer/getMedia/{customer}/{collection}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\PDFController@getCustomerMedia',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\PDFController@getCustomerMedia',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::r3Ykuz1BwUJgLDQJ',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'soumissions-index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'soumissions',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,backoffice,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\AdmissionController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\AdmissionController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'soumissions-index',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'soumission-view' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'soumission/{customer_case}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,backoffice,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\AdmissionController@view',
        'controller' => 'App\\Http\\Controllers\\Web\\AdmissionController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'soumission-view',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::wp5sP2LT9yeQXAj5' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'soumission_/{customer_case}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,backoffice,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\AdmissionController@view_',
        'controller' => 'App\\Http\\Controllers\\Web\\AdmissionController@view_',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::wp5sP2LT9yeQXAj5',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'soumission-update-state' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'soumission/status/{customer_case}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,backoffice,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\AdmissionController@updateState',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\AdmissionController@updateState',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'soumission-update-state',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'home' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => '/',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\HomeController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\HomeController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'home',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'lead-create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'leads/create',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\LeadController@create',
        'controller' => 'App\\Http\\Controllers\\Web\\LeadController@create',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'lead-create',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'leads-index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'leads',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\LeadController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\LeadController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'leads-index',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'lead-edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'leads/{lead}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\LeadController@edit',
        'controller' => 'App\\Http\\Controllers\\Web\\LeadController@edit',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'lead-edit',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'lead-view' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'lead/{lead}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\LeadController@view',
        'controller' => 'App\\Http\\Controllers\\Web\\LeadController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'lead-view',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'edit-information-sheet' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'sheets/{customer_case}/{information_sheet}/{join?}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,backoffice,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\InformationSheetController@edit',
        'controller' => 'App\\Http\\Controllers\\Web\\InformationSheetController@edit',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'edit-information-sheet',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'edit-media-sheet' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'sheet/media/{customer_case}/{media_sheet}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,backoffice,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\MediaSheetController@edit',
        'controller' => 'App\\Http\\Controllers\\Web\\MediaSheetController@edit',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'edit-media-sheet',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'cases-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'cases',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CaseController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\CaseController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'cases-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'case-edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'cases/{customer_case}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CaseController@edit',
        'controller' => 'App\\Http\\Controllers\\Web\\CaseController@edit',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'case-edit',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::RRAIsLTJ9aBMo7I0' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'cases_/{customer_case}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CaseController@edit_',
        'controller' => 'App\\Http\\Controllers\\Web\\CaseController@edit_',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::RRAIsLTJ9aBMo7I0',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::f9aWelWmmk0XDwbK' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'first-meeting/{customerCase}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\LeadController@firstMeetPresentation',
        'controller' => 'App\\Http\\Controllers\\Web\\LeadController@firstMeetPresentation',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::f9aWelWmmk0XDwbK',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'tasks-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'tasks',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\TasksController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\TasksController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'tasks-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'calendar' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'calendar',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CalendarController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\CalendarController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'calendar',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'calendar.create' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'calendar/create',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CalendarController@create',
        'controller' => 'App\\Http\\Controllers\\Web\\CalendarController@create',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'calendar.create',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'calendar.update' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'calendar/update',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CalendarController@update',
        'controller' => 'App\\Http\\Controllers\\Web\\CalendarController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'calendar.update',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'calendar.destroy' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'calendar/destroy/{calendar}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CalendarController@destroy',
        'controller' => 'App\\Http\\Controllers\\Web\\CalendarController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'calendar.destroy',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'calender.customer' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'calendar/getCustomer/{calendrier_id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CalendarController@getCustomer',
        'controller' => 'App\\Http\\Controllers\\Web\\CalendarController@getCustomer',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'calender.customer',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'user-profile' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'profileUser',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,support,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CaseController@updateUserProfile',
        'controller' => 'App\\Http\\Controllers\\Web\\CaseController@updateUserProfile',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'user-profile',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'sav' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'sav',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,support,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\UserController@sav',
        'controller' => 'App\\Http\\Controllers\\Web\\UserController@sav',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'sav',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'dashboard' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'dashboard',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\DashBoardController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\DashBoardController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'dashboard',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'dashboard-clients' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'dashboard/clients',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\DashBoardController@clientsIndex',
        'controller' => 'App\\Http\\Controllers\\Web\\DashBoardController@clientsIndex',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'dashboard-clients',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::jEvMRFTeO7eTMLzK' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'pdf/bill/{bill}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support,financier',
          4 => 'access:admin,manager,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\PDFController@generateBillPDF',
        'controller' => 'App\\Http\\Controllers\\Web\\PDFController@generateBillPDF',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::jEvMRFTeO7eTMLzK',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'quote-download' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'pdf/quote/{quote}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support,financier',
          4 => 'access:admin,manager,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\PDFController@generateQuotePDF',
        'controller' => 'App\\Http\\Controllers\\Web\\PDFController@generateQuotePDF',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'quote-download',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'orientation-home' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'orientation',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support,financier',
          4 => 'access:admin,manager,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\ProgramCourseController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\ProgramCourseController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'orientation-home',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'quote-view' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'pdf/viewQuote/{quote}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support,financier',
          4 => 'access:admin,manager,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\PDFController@viewQuotePDF',
        'controller' => 'App\\Http\\Controllers\\Web\\PDFController@viewQuotePDF',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'quote-view',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'send-mails' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'sendMail',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support,financier',
          4 => 'access:admin,manager,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\EmailController@send',
        'controller' => 'App\\Http\\Controllers\\Web\\EmailController@send',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'send-mails',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'histocalls-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'histocalls',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support,financier',
          4 => 'access:admin,manager,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\HistoCallController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\HistoCallController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'histocalls-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'leads-log' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'leadslog',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\LeadController@log',
        'controller' => 'App\\Http\\Controllers\\Web\\LeadController@log',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'leads-log',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::RTwvgmDbOy1MA9Uo' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'updateOrientation',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\ProgramCourseController@updateOrientationDB',
        'controller' => 'App\\Http\\Controllers\\Web\\ProgramCourseController@updateOrientationDB',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::RTwvgmDbOy1MA9Uo',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'orientation-view' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'orientation/{program}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\ProgramCourseController@view',
        'controller' => 'App\\Http\\Controllers\\Web\\ProgramCourseController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'orientation-view',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'production-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'production',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\ProductionController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\ProductionController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'production-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'production-view' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'production/{user}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\ProductionController@view',
        'controller' => 'App\\Http\\Controllers\\Web\\ProductionController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'production-view',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'field-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'fields',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CustomerFieldController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\CustomerFieldController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'field-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'doc-view' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'document/{document}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\DocumentTypeController@view',
        'controller' => 'App\\Http\\Controllers\\Web\\DocumentTypeController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'doc-view',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'documents-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'documents',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\DocumentTypeController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\DocumentTypeController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'documents-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'steps-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'steps',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\SubStepController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\SubStepController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'steps-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'step-view' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'steps/{step}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\SubStepController@view',
        'controller' => 'App\\Http\\Controllers\\Web\\SubStepController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'step-view',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'document-view' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'clear/{sheet}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\ClearController@view',
        'controller' => 'App\\Http\\Controllers\\Web\\ClearController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'document-view',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'clears-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'clears',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\ClearController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\ClearController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'clears-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'sheet-view' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'sheets/{sheet}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\SheetController@view',
        'controller' => 'App\\Http\\Controllers\\Web\\SheetController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'sheet-view',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'sheet-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'sheets',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\SheetController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\SheetController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'sheet-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'finances' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'finances',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\FinanceController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\FinanceController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'finances',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'quotes' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'quotes',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\QuoteController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\QuoteController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'quotes',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'bills' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'bills',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\BillController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\BillController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'bills',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'users-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'users',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\UserController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\UserController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'users-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'conseillers-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'conseillers',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\ConseillerController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\ConseillerController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'conseillers-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'rotation-config' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'rotation',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\UserAssignController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\UserAssignController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'rotation-config',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'rotation-edit' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'rotation/update',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\UserAssignController@update',
        'controller' => 'App\\Http\\Controllers\\Web\\UserAssignController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'rotation-edit',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'forms-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'formulaires',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\LeadFormController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\LeadFormController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'forms-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'programs-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'programs',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\ProgramController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\ProgramController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'programs-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::rQfEN3pBSqBaAjpE' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'program/{program}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        0 => 'App\\Http\\Livewire\\Program\\View',
        'uses' => NULL,
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::rQfEN3pBSqBaAjpE',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'import-leads' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'import-leads',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\LeadController@importCSV',
        'controller' => 'App\\Http\\Controllers\\Web\\LeadController@importCSV',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'import-leads',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'integrate-leads' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'integrate-csv',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\LeadController@integrateLeads',
        'controller' => 'App\\Http\\Controllers\\Web\\LeadController@integrateLeads',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'integrate-leads',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'payment-discount' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'payments',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\ProgramPaymentController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\ProgramPaymentController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'payment-discount',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'messages-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'messages',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CustomerMessageController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\CustomerMessageController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'messages-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'emailnotifications-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'emailnotifications',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\EmailNotificationController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\EmailNotificationController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'emailnotifications-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'substepnotifications-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'substepnotifications',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\SubStepStateController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\SubStepStateController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'substepnotifications-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'countries-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'countries',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CountryController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\CountryController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'countries-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'services-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'services',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\ServiceController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\ServiceController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'services-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'phases-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'phases',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\PhaseController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\PhaseController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'phases-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'campaigns-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'campaigns',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CampaignController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\CampaignController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'campaigns-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'schoollevels-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'schoollevels',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\SchoolLevelController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\SchoolLevelController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'schoollevels-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'tags-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'tags',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\TagController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\TagController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'tags-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'tag-leads' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'tags/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\TagController@leads',
        'controller' => 'App\\Http\\Controllers\\Web\\TagController@leads',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'tag-leads',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'immigrants-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'immigrants',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\LeadController@immgIndex',
        'controller' => 'App\\Http\\Controllers\\Web\\LeadController@immgIndex',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'immigrants-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customers-index' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'customers',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CustomerController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\CustomerController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customers-index',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-create' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'customers/create',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CustomerController@create',
        'controller' => 'App\\Http\\Controllers\\Web\\CustomerController@create',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-create',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-edit' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'customers/{customer}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CustomerController@edit',
        'controller' => 'App\\Http\\Controllers\\Web\\CustomerController@edit',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-edit',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'students-list' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'students',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\LeadController@studIndex',
        'controller' => 'App\\Http\\Controllers\\Web\\LeadController@studIndex',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'students-list',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::XQZHaiMc9I1Q5iyk' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/users/{user}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\UserController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\UserController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::XQZHaiMc9I1Q5iyk',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::MbgHxo1RasU0wWAa' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/histocalls/{histocall}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\HistoCallController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\HistoCallController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::MbgHxo1RasU0wWAa',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::IC4ua2a2TzpQPRhW' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/messages/{message}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerMessageController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerMessageController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::IC4ua2a2TzpQPRhW',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::g44WAaBgGfBpiRGE' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/emailnotifications/{emailnotification}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\EmailNotificationController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\EmailNotificationController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::g44WAaBgGfBpiRGE',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::pFR4AC4zXofNEGii' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/countries/{country}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CountryController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CountryController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::pFR4AC4zXofNEGii',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::DqDHqiav4Buv4lMD' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/services/{service}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ServiceController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ServiceController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::DqDHqiav4Buv4lMD',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::wQ09FxkIHr3NTJkI' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/phases/{phase}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\PhaseController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\PhaseController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::wQ09FxkIHr3NTJkI',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::xUfoKOxKDKPsSoOM' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/campaigns/{campaign}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CampaignController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CampaignController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::xUfoKOxKDKPsSoOM',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::QPSddo08rx1LUOZW' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/bills/{bill}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\BillController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\BillController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::QPSddo08rx1LUOZW',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::9Klq6Kp73YJbMc2E' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/quotes/{quote}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\QuoteController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\QuoteController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::9Klq6Kp73YJbMc2E',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::kr2sCwnZLxWbiTrl' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/schoollevels/{schollevel}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SchoolLevelController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SchoolLevelController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::kr2sCwnZLxWbiTrl',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::1xCjqXx6lzyEFMuF' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/leadsforms/{leadform}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadFormController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadFormController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::1xCjqXx6lzyEFMuF',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::NlCiMP5BRSZIBYRo' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/case/{customer_case}/{status}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::NlCiMP5BRSZIBYRo',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::4Gg6PUn2mmq3Nfn4' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/payments/{case_state}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ProgramPaymentController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ProgramPaymentController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::4Gg6PUn2mmq3Nfn4',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::k8FewZYi3vMstPqU' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/program-course/{field_name}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ProgramCourseController@addValue',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ProgramCourseController@addValue',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::k8FewZYi3vMstPqU',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::N8zmluORJ22JqNWr' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/program-course/{program}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ProgramCourseController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ProgramCourseController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::N8zmluORJ22JqNWr',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::fPjj7Xijr5BQeZUF' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/program-course',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ProgramCourseController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ProgramCourseController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::fPjj7Xijr5BQeZUF',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::w1eYJe62C6hrFWPp' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/program-course/{program}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ProgramCourseController@getProgram',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ProgramCourseController@getProgram',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::w1eYJe62C6hrFWPp',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'logs' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/logs',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@log',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@log',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'logs',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'store-task' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/tasks/{customer_case}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,backoffice,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\TasksController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\TasksController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'store-task',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::LEcMqeAhFMoeuaDG' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/tasks/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,backoffice,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\TasksController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\TasksController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::LEcMqeAhFMoeuaDG',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'analytics' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/analytics',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'analytics',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'analytics-clients' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/analytics/clients',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@clientsIndex',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@clientsIndex',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'analytics-clients',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::9AEdZc4fAdXBHYW4' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/agents',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@getAgents',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@getAgents',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::9AEdZc4fAdXBHYW4',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::nCAEpGefEJQUW1qA' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getPrograms',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@getPrograms',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@getPrograms',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::nCAEpGefEJQUW1qA',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::TNTvwAX3sWsK7Jic' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getPhases',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@getPhases',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@getPhases',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::TNTvwAX3sWsK7Jic',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::O1YqBb18S0FjfYsa' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getSteps',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@getSteps',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@getSteps',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::O1YqBb18S0FjfYsa',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::plzsK9K0DU75fonz' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getCampaigns',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@getCampaigns',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@getCampaigns',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::plzsK9K0DU75fonz',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::u0hNxwj87Hz7Q3GO' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getLeadStatus',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@getLeadStatus',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DashBoardController@getLeadStatus',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::u0hNxwj87Hz7Q3GO',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::TkJMLQfbPJJVMiWO' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/customerdata/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@getCustomerData',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@getCustomerData',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::TkJMLQfbPJJVMiWO',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::5Txht2oo6AiechSB' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/customer-meet/{id}/{meet}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@setMeetIsDone',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@setMeetIsDone',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::5Txht2oo6AiechSB',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::3qFOcPYonAWz2dc4' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/customer-orientation/{id}/{orientationId}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@setOrientation',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@setOrientation',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::3qFOcPYonAWz2dc4',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::vstO56UHlaHqefod' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/leadsdata/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@getLeadData',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@getLeadData',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::vstO56UHlaHqefod',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::budmXTSbjdhBzVAp' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/clear1002',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\MediaSheetController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\MediaSheetController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::budmXTSbjdhBzVAp',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ZahPyZAoWVtbfsrh' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/histocalls/lead/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\HistoCallController@show',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\HistoCallController@show',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::ZahPyZAoWVtbfsrh',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'histocalls' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/histocalls',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\HistoCallController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\HistoCallController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'histocalls',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::bDvVBHIvZLhV3ELC' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/histocalls/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\HistoCallController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\HistoCallController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::bDvVBHIvZLhV3ELC',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::LQDIKHIx5UOYZDCJ' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/histocalls',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\HistoCallController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\HistoCallController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::LQDIKHIx5UOYZDCJ',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'remove-media' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/customer/media/{media}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\PDFController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\PDFController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'remove-media',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::HBhsnCUhixTa60VJ' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/savePhone/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@savePhone',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@savePhone',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::HBhsnCUhixTa60VJ',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::jFLa2ZVwpPTX6KIL' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/leads/switch/{lead}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@switch',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@switch',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::jFLa2ZVwpPTX6KIL',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::LH72xClOpyvR6xQD' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/leads/assign/{lead}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:conseiller,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@assign',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@assign',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::LH72xClOpyvR6xQD',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::wgOY1cn6mKJVRrUQ' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/bills',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\BillController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\BillController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::wgOY1cn6mKJVRrUQ',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::H76fZCee8ZHrOL7F' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/bills/{bill}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\BillController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\BillController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::H76fZCee8ZHrOL7F',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::hUFxHjC2KGK9TNHW' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/bills',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\BillController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\BillController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::hUFxHjC2KGK9TNHW',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::CDKD1ztbRO1m8uDD' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/finances',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\FinanceController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\FinanceController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::CDKD1ztbRO1m8uDD',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::GbOYwr1RyyLrVncg' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/finances-stats',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\FinanceController@stats',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\FinanceController@stats',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::GbOYwr1RyyLrVncg',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::kB2qk1qRPD17321T' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/finances/{payment_id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\FinanceController@verify',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\FinanceController@verify',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::kB2qk1qRPD17321T',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::mWvzgiQJkeTwWgVe' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/quotes',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\QuoteController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\QuoteController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::mWvzgiQJkeTwWgVe',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::uU1friQZCzwdODx0' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/quotes/{quote}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\QuoteController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\QuoteController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::uU1friQZCzwdODx0',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::BYGHK8h6QbbdZ6Ot' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/quotes',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,financier',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\QuoteController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\QuoteController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::BYGHK8h6QbbdZ6Ot',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::cMm0xC1yuuIxZw5x' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/leads/{lead}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::cMm0xC1yuuIxZw5x',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::7nyWmZNizXNF1W6n' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/leads/convert/{lead}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@makeClient',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@makeClient',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::7nyWmZNizXNF1W6n',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'regen-docs' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/leads/generate/{lead}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@generateDocs',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@generateDocs',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'regen-docs',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::4lsieHqwMhaVq3dr' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/cases/convert/{customer}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@convert_to_soumission',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@convert_to_soumission',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::4lsieHqwMhaVq3dr',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::Jk6xusZAujWk9zFn' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/soumission/convert/{customer}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\AdmissionController@convert_soumission',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\AdmissionController@convert_soumission',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::Jk6xusZAujWk9zFn',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'programs' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/programs',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ProgramController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ProgramController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'programs',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'users' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/users',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\UserController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\UserController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'users',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::uxHdZqpy9BvAUODp' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/users/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\UserController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\UserController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::uxHdZqpy9BvAUODp',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::3Cc9rzpcgB5cg5lD' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/users',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\UserController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\UserController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::3Cc9rzpcgB5cg5lD',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'conseillers' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/conseillers',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ConseillerController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ConseillerController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'conseillers',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::MjmWvY8IT9bbqIRO' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/conseillers',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ConseillerController@getConseillers',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ConseillerController@getConseillers',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::MjmWvY8IT9bbqIRO',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::l0MXX7BhvDCS8Q9F' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/conseiller/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ConseillerController@attach',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ConseillerController@attach',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::l0MXX7BhvDCS8Q9F',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::WFvhMbg3K5Xqfz1h' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/conseillers/{user}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ConseillerController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ConseillerController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::WFvhMbg3K5Xqfz1h',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::YnhDNZCBbqUKYxj8' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/conseillers/{user}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ConseillerController@dettach',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ConseillerController@dettach',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::YnhDNZCBbqUKYxj8',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'Messages' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/messages',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerMessageController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerMessageController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'Messages',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::av5VYEiqNHiIDLAN' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/messages/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerMessageController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerMessageController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::av5VYEiqNHiIDLAN',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::GqWkcMwYZn9TkrbV' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/messages',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerMessageController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerMessageController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::GqWkcMwYZn9TkrbV',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'EmailNotifications' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/emailnotifications',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\EmailNotificationController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\EmailNotificationController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'EmailNotifications',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::qr1aO5vXkmcv4eoM' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/emailnotifications/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\EmailNotificationController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\EmailNotificationController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::qr1aO5vXkmcv4eoM',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::d6Gjjw9NjW1jfu4l' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/emailnotifications',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\EmailNotificationController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\EmailNotificationController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::d6Gjjw9NjW1jfu4l',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'SubStepNotifications' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/substepnotifications',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SubStepStateController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SubStepStateController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'SubStepNotifications',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ZaJmTuhZgXexF6I9' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/substepnotifications/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SubStepStateController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SubStepStateController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::ZaJmTuhZgXexF6I9',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'countries' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/countries',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CountryController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CountryController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'countries',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::pjd6Qq1sJeu8m73Y' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/countries/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CountryController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CountryController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::pjd6Qq1sJeu8m73Y',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::pfoHh7Mm6D13FIFb' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/countries',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CountryController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CountryController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::pfoHh7Mm6D13FIFb',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'services' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/services',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ServiceController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ServiceController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'services',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::4HBo9PNy4RejsgEc' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/services/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ServiceController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ServiceController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::4HBo9PNy4RejsgEc',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::JhXWumIpK14DWGn2' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/services',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ServiceController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ServiceController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::JhXWumIpK14DWGn2',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::CqAR6mYZqSbcVww6' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getservices',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ServiceController@getServices',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ServiceController@getServices',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::CqAR6mYZqSbcVww6',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'phases' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/phases',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\PhaseController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\PhaseController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'phases',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::4ZrqgfCa5dX2L22q' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/phases/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\PhaseController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\PhaseController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::4ZrqgfCa5dX2L22q',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::kXMIKbjThqbP0muD' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/phases',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\PhaseController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\PhaseController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::kXMIKbjThqbP0muD',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'campaigns' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/campaigns',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CampaignController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CampaignController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'campaigns',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::gAWmSfWxzDnnKKBm' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/campaigns/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CampaignController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CampaignController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::gAWmSfWxzDnnKKBm',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::rNzY1u29AU6z7mag' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/campaigns',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CampaignController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CampaignController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::rNzY1u29AU6z7mag',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'schoollevel' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/schoollevels',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SchoolLevelController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SchoolLevelController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'schoollevel',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ety3DgXePlGwo0t2' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/schoollevels/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SchoolLevelController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SchoolLevelController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::ety3DgXePlGwo0t2',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::L4NfJfz28A2G6RsS' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/schoollevels',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SchoolLevelController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SchoolLevelController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::L4NfJfz28A2G6RsS',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'tag' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/tags',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\TagController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\TagController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'tag',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::B7z2kADbExa8D4ja' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/tags/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\TagController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\TagController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::B7z2kADbExa8D4ja',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::kOs2UMCohTm0ahLD' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/tags',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\TagController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\TagController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::kOs2UMCohTm0ahLD',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::OneenQalJfNVeRMw' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/tags/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\TagController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\TagController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::OneenQalJfNVeRMw',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::Ydd4lkvDJzK4l7HK' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/steps',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SubStepController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SubStepController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::Ydd4lkvDJzK4l7HK',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::OlyGC4P1voTjak5d' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/document-steps/{id}/steps',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SubStepController@view',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SubStepController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::OlyGC4P1voTjak5d',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::M0ggQupUlFsnAXEh' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/document-steps/{id}/steps',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SubStepController@updatePivot',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SubStepController@updatePivot',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::M0ggQupUlFsnAXEh',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::MwZL5tdQUmB4EraH' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/document-steps/{step_id}/steps',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SubStepController@storePivot',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SubStepController@storePivot',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::MwZL5tdQUmB4EraH',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::Wwz3t6VRbpB66rUl' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/document-steps/{document_step}/steps',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SubStepController@destroyPivot',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SubStepController@destroyPivot',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::Wwz3t6VRbpB66rUl',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::dGu89xbRwkUwfTOK' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/clears',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ClearController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ClearController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::dGu89xbRwkUwfTOK',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ZvYkHY6Cb7a7nHZJ' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/sheets',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SheetController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SheetController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::ZvYkHY6Cb7a7nHZJ',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::dfPiEOsYoh47lkI1' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/sheet-fields/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SheetController@view',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SheetController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::dfPiEOsYoh47lkI1',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::bvjLHqDfkPG7gMrc' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/sheet-fields/{sheet_id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SheetController@storePivot',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SheetController@storePivot',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::bvjLHqDfkPG7gMrc',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ai6sRcRgjHGS8HJe' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/sheet-fields/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SheetController@updatePivot',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SheetController@updatePivot',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::ai6sRcRgjHGS8HJe',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::twmuP3lREefMhTEc' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/sheet-fields/{field_sheet}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SheetController@destroyPivot',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SheetController@destroyPivot',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::twmuP3lREefMhTEc',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::oOdCsOuT4DhjRWns' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/document-steps/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@viewSteps',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@viewSteps',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::oOdCsOuT4DhjRWns',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::dDDZnCVD7dSlsLq9' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/document-steps/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@updateStepPivot',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@updateStepPivot',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::dDDZnCVD7dSlsLq9',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::21Treuf65WuybMa4' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/document-steps/{document_id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@storeStepPivot',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@storeStepPivot',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::21Treuf65WuybMa4',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::M5r8Wgul6dfYL9JK' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/document-steps/{document_step}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@destroyStepPivot',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@destroyStepPivot',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::M5r8Wgul6dfYL9JK',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::hUxbYN8YYSz3IWgS' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/fields',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerFieldController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerFieldController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::hUxbYN8YYSz3IWgS',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::v9mHNel7bACteWKb' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/fields/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerFieldController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerFieldController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::v9mHNel7bACteWKb',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::35oH2SuykNYM3kqM' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/fields',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerFieldController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerFieldController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::35oH2SuykNYM3kqM',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ABtin7O2EezJIk19' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/fields/{field}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerFieldController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerFieldController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::ABtin7O2EezJIk19',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::kYu3Qh6uTkv2wuNG' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/document-clears/{id}/{column}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@view',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::kYu3Qh6uTkv2wuNG',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::3gNOof0sPhfSL0JX' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/document-clears/{id}/{column?}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@storePivot',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@storePivot',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::3gNOof0sPhfSL0JX',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::HJZdpwLTi0MVlnFM' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/document-clears/{id}/{column?}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@updatePivot',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@updatePivot',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::HJZdpwLTi0MVlnFM',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::52kWurGomfAUmx6Z' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/document-clears/{document_clear}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@destroyPivot',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@destroyPivot',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::52kWurGomfAUmx6Z',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::1kTxXy1Y5RTp5V7P' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/documents',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::1kTxXy1Y5RTp5V7P',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::9IiIPEhWZH6P3Z8w' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/documents/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::9IiIPEhWZH6P3Z8w',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::wMcMB7YVPTylEItW' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/documents',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::wMcMB7YVPTylEItW',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::plGEIGPEnya747YO' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/documents/{document}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentTypeController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::plGEIGPEnya747YO',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'leadsforms' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/leadsforms',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadFormController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadFormController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'leadsforms',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::c8QlPpb75xcGN63o' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/leadsforms/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadFormController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadFormController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::c8QlPpb75xcGN63o',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::39EsBkMwUcfl72kT' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/leadsforms',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadFormController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadFormController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::39EsBkMwUcfl72kT',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::qjijdqT5ixgB3Glj' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/leadsforms/setactivestate',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadFormController@setActiveState',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadFormController@setActiveState',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::qjijdqT5ixgB3Glj',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ebLIzljg6Nbgn27F' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/customerBackoffice',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@updateCustomerBackoffice',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@updateCustomerBackoffice',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::ebLIzljg6Nbgn27F',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'lead-store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/leads',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'lead-store',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'lead-delete-media' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/leads/media/{lead}/{collectionName}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@deleteMedia',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@deleteMedia',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'lead-delete-media',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'lead-convert' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/leads/{lead}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@convert',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@convert',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'lead-convert',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'lead-update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/leads/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'lead-update',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::ab7bcebQunTVV8lc' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/leads/{lead}/{status}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@updateLeadStatus',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@updateLeadStatus',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::ab7bcebQunTVV8lc',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::Y3MaGXUgWACWovAX' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/leadConseiller',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@updateLeadConseiller',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@updateLeadConseiller',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::Y3MaGXUgWACWovAX',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'getLeads' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getLeads',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@getLeads',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@getLeads',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'getLeads',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'leads' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/leads',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'leads',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'tags-leads' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/tag/leads/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'tags-leads',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::w5BuaUUQvwzAfYLu' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getCountries',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CountryController@getCountries',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CountryController@getCountries',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::w5BuaUUQvwzAfYLu',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::KLJJPC1VkH9BbDft' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/forms/{campaign}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadFormController@getInfluencers',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadFormController@getInfluencers',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::KLJJPC1VkH9BbDft',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::If2lDX6L9IRQADge' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/first-meeting-slides',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\MeetingShowController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\MeetingShowController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::If2lDX6L9IRQADge',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'cases' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/cases',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'cases',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'case-update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/cases/{customer_case}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'case-update',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'step-state-update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/cases/{customer_case}/{program_step}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@updateStepState',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@updateStepState',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'step-state-update',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'admissions' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/admissions',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\AdmissionController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\AdmissionController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'admissions',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'production' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/production',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ProductionController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ProductionController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'production',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'profile-update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/profileUpdate',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,support,super_backoffice',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@updateProfile',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@updateProfile',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'profile-update',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::5BVcHPPrqyGStsAf' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/calendar',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
          3 => 'access:admin,manager,conseiller,agent,backoffice,support',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CalendarController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CalendarController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::5BVcHPPrqyGStsAf',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::UA9CnYwl8ESioXuy' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/admission-states',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\AdmissionController@states',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\AdmissionController@states',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::UA9CnYwl8ESioXuy',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'signeasy-import-document' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/signeasy/{customer}/{collectionName}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentSignatureController@prepareForSigning',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentSignatureController@prepareForSigning',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'signeasy-import-document',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'signeasy-send-signing-request' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/signeasy/sendrequest/{customer}/{collectionName}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentSignatureController@sendForSigning',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentSignatureController@sendForSigning',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'signeasy-send-signing-request',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'signeasy-download-signed-document' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/signeasy/download/{customer}/{collectionName}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentSignatureController@downloadSignedDocument',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentSignatureController@downloadSignedDocument',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'signeasy-download-signed-document',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'setasign-test' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/downloadpdf/{customer}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SetasignPDFController@download',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SetasignPDFController@download',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'setasign-test',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'immigrants' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/immigrants',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@immgIndex',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@immgIndex',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'immigrants',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::4iU0Pr9yOoARvczF' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/immigrants/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::4iU0Pr9yOoARvczF',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::krhlQpLnYmqwwTSJ' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/immigrants',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::krhlQpLnYmqwwTSJ',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customers' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/customers',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customers',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/customers/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-update',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-update-clear1000' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/customers/clear1000/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@updateClear1000',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@updateClear1000',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-update-clear1000',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-update-clear1002' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/customers/clear1002/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@updateClear1002',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@updateClear1002',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-update-clear1002',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-update-clear3' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/customers/clear3/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@updateClear3',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@updateClear3',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-update-clear3',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-update-clear1004' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/customers/clear1004/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@updateClear1004',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@updateClear1004',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-update-clear1004',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-account-activation' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/customers/account/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@activate',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@activate',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-account-activation',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-account-deactivation' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/customers/account/deactivate/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@deactivate',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@deactivate',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-account-deactivation',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-store' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/customers',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-store',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-delete-media' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'api/v1/customers/media/{customer}/{collectionName}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@deleteMedia',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@deleteMedia',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-delete-media',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'add-media' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/customers/media/{customer}/{collectionName}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@addMedia',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@addMedia',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'add-media',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::Wh5mPeYl50uLvLC0' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/discounts',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ProgramPaymentController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ProgramPaymentController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::Wh5mPeYl50uLvLC0',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'getCustomers' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getCustomers',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@getCustomers',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@getCustomers',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'getCustomers',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'getClients' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getClients',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@getClients',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@getClients',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'getClients',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'getCallStates' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/getCallStates/{lead}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@getCallStates',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@getCallStates',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'getCallStates',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::k3YVuZvtA88p4upW' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getPayments',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ProgramPaymentController@getPayments',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ProgramPaymentController@getPayments',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::k3YVuZvtA88p4upW',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::csz7rJg6ayqhjlit' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getAmount',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ProgramPaymentController@getAmount',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ProgramPaymentController@getAmount',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::csz7rJg6ayqhjlit',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::GmtluzJaNJ1PFER2' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/discount',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ProgramPaymentController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ProgramPaymentController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::GmtluzJaNJ1PFER2',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::rzrAbOiGVy7XqzeX' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/payments/{case_state}/{method}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\ProgramPaymentController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\ProgramPaymentController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::rzrAbOiGVy7XqzeX',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'add-requested-document' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/addrequesteddocument/{customer}/{document_type}/{program_step}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@addRequestedDocument',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@addRequestedDocument',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'add-requested-document',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'remove-requested-document' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/removerequesteddocument/{customer}/{document_type}/{program_step}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@removeRequestedDocument',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@removeRequestedDocument',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'remove-requested-document',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'toggle-link-visibility' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/togglelinkvisibility/{customer}/{document_type}/{program_step}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@toggleLinkVisibility',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@toggleLinkVisibility',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'toggle-link-visibility',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'toggle-sheet-visibility' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/togglesheetvisibility/{customer}/{information_sheet}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@toggleSheetVisibility',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@toggleSheetVisibility',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'toggle-sheet-visibility',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'toggle-media-sheet-visibility' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/togglemediasheetvisibility/{customer}/{media_sheet}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@toggleMediaSheetVisibility',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@toggleMediaSheetVisibility',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'toggle-media-sheet-visibility',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'toggle-meet' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/togglemeet/{customer}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@toggleMeet',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@toggleMeet',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'toggle-meet',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::7vgl7eHJBlRdGrcI' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/getPaymentMethods',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SubStepStateController@getPaymentMethods',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SubStepStateController@getPaymentMethods',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::7vgl7eHJBlRdGrcI',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'sub-step-state-update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'api/v1/states/{case_state}/{method?}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\SubStepStateController@updateStepState',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\SubStepStateController@updateStepState',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'sub-step-state-update',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'comments' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/comments/{customer_case}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CaseCommentController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CaseCommentController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'comments',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'store-comment' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/comments/{customer_case}/{canal?}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CaseCommentController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CaseCommentController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'store-comment',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'update-comment' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/comments/edit/{customer_case}/{comment}/{canal?}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CaseCommentController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CaseCommentController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'update-comment',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'update-fees' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/task-fees',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CaseCommentController@updateFees',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CaseCommentController@updateFees',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'update-fees',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'store-tags' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/tags/{customer_case}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@editTags',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\LeadController@editTags',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'store-tags',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'toggle-media-property' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/togglemediaproperty/{media}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\MediaController@toggleProperty',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\MediaController@toggleProperty',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'toggle-media-property',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::nIEeN8OoH1XNMCif' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/notifications',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\NotificationController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\NotificationController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::nIEeN8OoH1XNMCif',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::PIq4uzzb8illyWkP' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/notifications',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\NotificationController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\NotificationController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::PIq4uzzb8illyWkP',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::gxtFvCz28ymUFkV0' => 
    array (
      'methods' => 
      array (
        0 => 'PATCH',
      ),
      'uri' => 'api/v1/notifications/{id}/read',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\NotificationController@markAsRead',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\NotificationController@markAsRead',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::gxtFvCz28ymUFkV0',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::GLWLiEZqqJhyUHUY' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'api/v1/notifications/mark-all-read',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\NotificationController@markAllRead',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\NotificationController@markAllRead',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::GLWLiEZqqJhyUHUY',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::1gDcrZQRZdkeinTX' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/notifications/{id}/dismiss',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\NotificationController@dismiss',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\NotificationController@dismiss',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::1gDcrZQRZdkeinTX',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::seovvsTYMPBVycDW' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/subscriptions',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\PushSubscriptionController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\PushSubscriptionController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::seovvsTYMPBVycDW',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::1YuDIQ9Q6XsReWRk' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/subscriptions/delete',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\PushSubscriptionController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\PushSubscriptionController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::1YuDIQ9Q6XsReWRk',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'enable-quote' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'pdf/enableQuote',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DownloadMediaController@enableQuote',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DownloadMediaController@enableQuote',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'enable-quote',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-inbox' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'myinbox',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CustomerMessageController@customerIndex',
        'controller' => 'App\\Http\\Controllers\\Web\\CustomerMessageController@customerIndex',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-inbox',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-mycase' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'mycase/{program_step}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CaseController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\CaseController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-mycase',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-case' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'mycase',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CaseController@indexCustomer',
        'controller' => 'App\\Http\\Controllers\\Web\\CaseController@indexCustomer',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-case',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-myquotes' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'myquotes',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\QuoteController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\QuoteController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-myquotes',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-mybills' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'mybills',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\BillController@index',
        'controller' => 'App\\Http\\Controllers\\Web\\BillController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-mybills',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::EqRDk1ckBe1PZtiU' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'pdf/mybill/{bill}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\PDFController@generateBillPDF',
        'controller' => 'App\\Http\\Controllers\\Web\\PDFController@generateBillPDF',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::EqRDk1ckBe1PZtiU',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::9l58lPTQAYLwcIzj' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'pdf/myquote/{quote}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\PDFController@generateQuotePDF',
        'controller' => 'App\\Http\\Controllers\\Web\\PDFController@generateQuotePDF',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::9l58lPTQAYLwcIzj',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-edit-media-sheet' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'customer/sheets/media/{customer_case}/{media_sheet}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\MediaSheetController@edit',
        'controller' => 'App\\Http\\Controllers\\Web\\MediaSheetController@edit',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-edit-media-sheet',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-edit-information-sheet' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'customer/sheets/{customer_case}/{information_sheet}/{join?}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\InformationSheetController@edit',
        'controller' => 'App\\Http\\Controllers\\Web\\InformationSheetController@edit',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-edit-information-sheet',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
        'customer_case' => '[0-9]+',
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-profile' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'profile',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\CaseController@updateProfile',
        'controller' => 'App\\Http\\Controllers\\Web\\CaseController@updateProfile',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-profile',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-remove-media' => 
    array (
      'methods' => 
      array (
        0 => 'DELETE',
      ),
      'uri' => 'customer/media/{media}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\PDFController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\PDFController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-remove-media',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-view-media' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'customer/media/{media}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\PDFController@view',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\PDFController@view',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'customer-view-media',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-signeasy-get-signing-url' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'customer/api/v1/signeasy/getsigningurl/{customer}/{collectionName}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentSignatureController@getSigningUrl',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentSignatureController@getSigningUrl',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/customer/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-signeasy-get-signing-url',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-signeasy-download-signed-document' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'customer/api/v1/signeasy/download/{customer}/{collectionName}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\DocumentSignatureController@downloadSignedDocument',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\DocumentSignatureController@downloadSignedDocument',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/customer/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-signeasy-download-signed-document',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-add-media' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'customer/api/v1/customers/media/{customer}/{collectionName}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@addMedia',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@addMedia',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/customer/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-add-media',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::snQaYxz4h30YjgY0' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'customer/api/v1/notifications',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerNotificationController@store',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerNotificationController@store',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/customer/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::snQaYxz4h30YjgY0',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::LAviWr4xg1FlV1Dz' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'customer/api/v1/notifications',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerNotificationController@index',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerNotificationController@index',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/customer/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::LAviWr4xg1FlV1Dz',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::H0WYrnx1ViYT9l0s' => 
    array (
      'methods' => 
      array (
        0 => 'PATCH',
      ),
      'uri' => 'customer/api/v1/notifications/{id}/read',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerNotificationController@markAsRead',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerNotificationController@markAsRead',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/customer/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::H0WYrnx1ViYT9l0s',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::74CjBQAnOwjDqFmR' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'customer/api/v1/notifications/mark-all-read',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerNotificationController@markAllRead',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerNotificationController@markAllRead',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/customer/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::74CjBQAnOwjDqFmR',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::xb7SXJn5vcDNKmYA' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'customer/api/v1/notifications/{id}/dismiss',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerNotificationController@dismiss',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerNotificationController@dismiss',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/customer/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::xb7SXJn5vcDNKmYA',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::HWcD1ofhbDMt2WJD' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'customer/api/v1/subscriptions',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerPushSubscriptionController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerPushSubscriptionController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/customer/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::HWcD1ofhbDMt2WJD',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::7gcqfTVAY4tz0sDu' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'customer/api/v1/subscriptions/delete',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerPushSubscriptionController@destroy',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerPushSubscriptionController@destroy',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/customer/api/v1',
        'where' => 
        array (
        ),
        'as' => 'generated::7gcqfTVAY4tz0sDu',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-customer-update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'customer/api/v1/customers/{id}',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@update',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CustomerController@update',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/customer/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-customer-update',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'customer-profile-update' => 
    array (
      'methods' => 
      array (
        0 => 'PUT',
      ),
      'uri' => 'customer/api/v1/customer',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
          1 => 'web',
          2 => 'auth:customer',
          3 => 'access:customer',
          4 => 'password.reset',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@updateProfile',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\CaseController@updateProfile',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => '/customer/api/v1',
        'where' => 
        array (
        ),
        'as' => 'customer-profile-update',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'getAvailableDateTime' => 
    array (
      'methods' => 
      array (
        0 => 'GET',
        1 => 'HEAD',
      ),
      'uri' => 'getAvailableDateTime',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\GetInTouchController@getAvailableDateTime',
        'controller' => 'App\\Http\\Controllers\\Web\\GetInTouchController@getAvailableDateTime',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'getAvailableDateTime',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::MOi8m1UKh9dEgudr' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/v1/hr-lead',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Api\\v1\\GetInTouchController@storeHRLead',
        'controller' => 'App\\Http\\Controllers\\Api\\v1\\GetInTouchController@storeHRLead',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::MOi8m1UKh9dEgudr',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
    'generated::6Iz8ErzLuddqrnH3' => 
    array (
      'methods' => 
      array (
        0 => 'POST',
      ),
      'uri' => 'api/stripe/webhook',
      'action' => 
      array (
        'middleware' => 
        array (
          0 => 'web',
        ),
        'uses' => 'App\\Http\\Controllers\\Web\\WebhookController@handle',
        'controller' => 'App\\Http\\Controllers\\Web\\WebhookController@handle',
        'namespace' => 'App\\Http\\Controllers',
        'prefix' => NULL,
        'where' => 
        array (
        ),
        'as' => 'generated::6Iz8ErzLuddqrnH3',
      ),
      'fallback' => false,
      'defaults' => 
      array (
      ),
      'wheres' => 
      array (
      ),
      'bindingFields' => 
      array (
      ),
      'lockSeconds' => NULL,
      'waitSeconds' => NULL,
    ),
  ),
)
);
