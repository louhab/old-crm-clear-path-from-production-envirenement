Install dependencies
composer install
npm install


Copy the environement file
cp .env.example .env, (then change mysql connection information the database name, and APP_URL value too.)


Then run the migration php artisan migrate


Seed the data php artisan db:seed


if you are on linux add permission to storage folder chmod -R 777 storage


Finally generate application key php artisan key:generate

php artisan storage:link

Now you are Ready To Go you can run php artisan serve and npm run dev (for watch update js)
