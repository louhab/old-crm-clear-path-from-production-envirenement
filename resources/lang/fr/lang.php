<?php

return [
    'filtre_globale' => 'Filtre Global',
    'dashboard' => 'Tableau de bord',
    'marketing' => 'Marketing',
    'leads' => 'Leads',
    'immigrants' => 'Immigrants',
    'students' => 'Etudiants',
    'forms' => 'Formulaires',
    'customers_management' => 'Processus Client',
    'customers' => 'Clients',
    'files' => 'Fichiers',
    'followed' => 'Suivi',
    'billings' => 'Facturation',
    'call_hostory' => 'Historique des appels',
    'messages' => 'Messages',
    'estimate' => 'Devis',
    'invoices' => 'Factures',
    'user_management' => 'Gestion Utilisateurs',
    'users' => 'Utilisateurs',
    'services' => 'Services',
    'params' => 'Paramétrage Rapide',
    'globaldata' => 'Données Globales',
    'steps' => 'Phases',
    'country' => 'Pays',
    'emailnotification' => 'Notification des emails',
    'substepnotification' => 'Notification sous steps',
    'campaigns' => 'Campagnes marketing',
    'schoollevels' => 'Niveaux scolaires',
    'case' => 'Dossier',
    'mycase' => 'Mon Dossier',
    'myquotes' => 'Mes Devis',
    'mybills' => 'Mes Factures',
    'list' => 'Liste',
    'submissions' => 'Soumissions',
    'tasks' => 'Tâches',
    "tasks-list" => 'List des Tâches',



    'keywords' => 'Mots Clés',
    'Payments_Discount' => 'Remise sur les paiements',
    'Queue_conseillers_supports' => 'Queue conseillers/supports',
    'Statistiques_des_Conseillers' => 'Statistiques des Conseillers',
    'Mes_Statistiques' => 'Mes Statistiques',
    'Nouveaux_leads' => 'Nouveaux leads',
    'Leads_traités' => 'Leads traités',
    'Leads_non_traités' => 'Leads non traités',
    'Nouveaux_leads_recyclés' => 'Nouveaux leads recyclés',
    'Leads_traités_recyclés' => 'Leads traités recyclés',
    'Leads_non_traités_recyclés' => 'Leads non traités recyclés',
    'Rencontres_1' => 'Rencontres 1',
    'select_study_program' => "Choix Programme d'études",
    'No_Show' => 'No Show',
    'RDV_1' => 'RDV 1',
    'No_RDV_1' => 'No RDV 1',
    'Admissibles' => 'Admissibles',
    'no_Admissibles' => 'No Admissibles',
    'Contrats' => 'Contrats',
    'no_Contrats' => 'No Contrats',
    'Appels' => 'Appels',
    'Durée_appels' => 'Durée d\'appels',
    'Injoignables' => 'Injoignables',
    'Pas_Intéressés' => 'Pas Intéressés',
    'Numéro_erronés' => 'Numéro erronés',
    'Statistiques_compagne_de_marketing' => 'Statistiques compagne de marketing',
    //
    'french' => 'Français',
    'english' => 'English',

    //form lead public
    'Sélectionnez_le_programme' => 'Sélectionnez le programme',
    'Pass_au_bureau' => 'Passer au bureau',
    'Conference_video' => 'Vidéo conférence',
    'soumettre' => 'soumettre',
    //Customer fields
    'ranking_id' => 'Ranking',
    'firstname' => 'Prénom',
    'email' => 'Email',
    'budget' => 'Budget',
    'birthday' => 'Date de naissance',
    'adresse_line_1' => 'Adresse Ligne 1',
    'adresse_line_2' => 'Adresse Ligne 2',
    'score' => 'Score',
    'lastname' => 'Nom de famille',
    'sex' => 'Sexe',
    'birthday_country' => 'Pays de naissance',
    'birthday_city' => 'Ville de naissance',
    'adresse_country' => 'Adresse pays de résidence',
    'postal_code' => 'Code postal',
    'adresse_canada' => 'Adresse Canada',
    'phone' => 'Téléphone',
    'father_phone' => 'Téléphone pére',
    'mother_phone' => 'Téléphone mére',
    'campaign_id' => 'Canal',
    'lead_status' => 'Status',
    'citizenship' => 'Citoyenneté',
    'current_marital_status' => 'État matrimonial actuelle',
    'wedding_date' => 'Date de mariage',
    'divorce_date' => 'Date de divorce ',
    'customer_email' => 'Adresse électronique',
    'spoken_languages' => 'Langues parlées',

    //Emploi actuelle
    'work_date_from' => 'Du',
    'work_date_to' => 'Au ',
    'work_description' => 'Activités',
    'work_business_name' => 'Nom de l\'entreprise',
    'work_business_country' => 'Ville et Pays',
    // 'work_business_city' => 'Ville de l\'entreprise',
    'work_business_status' => 'Status',

    'job_fields' => 'Domaines d\'emplois',
    'study_fields' => 'Domaines d\'études',
    // 'program_field' => 'Domaines',
    'imm_activity' => 'Activité',
    'imm_status' => 'status',
    'imm_business_name' => 'Nom Professionnel',
    // Scolarité
    'scholarship_date_from' => 'Date de début des études',
    'scholarship_date_to' => 'Date de fin des études',
    'scholarship_institution_name' => 'Nom De L’Établissement',
    'scholarship_country' => 'Pays De L’Établissement',
    'scholarship_city' => 'Ville De L’Établissement',
    'scholarship_diplomas_title' => 'Titre Du Diplôme',
    'scholarship_domain' => 'Domaine D‘Études',

    //Informations memebre de la famille
    //Époux ou conjoint de faits
    'spouse_name' => 'Nom du conjoint',
    'spouse_birthdate' => 'Date de naissance',
    'spouse_country' => 'Pays',
    'spouse_city' => 'Ville',
    'spouse_address' => 'Adresse',
    'spouse_family_situation' => 'Situation familliale',
    'spouse_current_occupation' => 'Profession actuelle',
    'spouse_email' => 'Adresse email',

    //Mére
    'mother_name' => 'Nom de la mére',
    'mother_birthdate' => 'Date de naissance',
    'mother_country' => 'Pays de naissance',
    'mother_city' => 'Ville de naissance',
    'mother_address' => 'Adresse',
    'mother_family_situation' => 'Situation familliale',
    'mother_current_occupation' => 'Profession actuelle',
    'mother_email' => 'Adresse email',

    //Pére
    'father_name' => 'Nom du pére',
    'father_birthdate' => 'Date de naissance',
    'father_country' => 'Pays de naissance',
    'father_city' => 'Ville de naissance',
    'father_address' => 'Adresse',
    'father_family_situation' => 'Situation familliale',
    'father_current_occupation' => 'Profession actuelle',
    'father_email' => 'Adresse email',
    //Enfants
    'child_name' => 'Nom d\'enfant',
    'child_birthdate' => 'Date de naissance',
    'child_country' => 'Pays de naissance',
    'child_city' => 'Ville de naissance',
    'child_address' => 'Adresse',
    'child_family_situation' => 'Situation familliale',
    'child_current_occupation' => 'Profession actuelle',
    'child_email' => 'Adresse email',
    //Frère/Soeur
    'sibling_name' => 'Nom',
    'sibling_birthdate' => 'Date de naissance',
    'sibling_country' => 'Pays de naissance',
    'sibling_city' => 'Ville de naissance',
    'sibling_address' => 'Adresse',
    'sibling_family_situation' => 'Situation familliale',
    'sibling_current_occupation' => 'Profession actuelle',
    'sibling_email' => 'Adresse email',
    //Pays de résidence et voyages
    //Avez-vous déjà visité le Canada?
    'did_visit_canada' => 'Avez-vous déjà visité le Canada?',
    'did_visit_canada_date_from' => 'Du',
    'did_visit_canada_date_to' => 'Au',

    //Pays de résidence actuelle
    'current_residence_country' => 'Pays de résidence',
    'current_residence_city' => 'Ville de résidence',
    'current_residence_statut' => 'Status',
    'current_residence_date_from' => 'Depuis',
    'current_residence_date_to' => 'a',

    // Pays de résidence antérieure
    'past_residence_country' => 'Ville et Pays',
    'past_residence_city' => 'Rue et numero civique',
    'past_residence_zip' => 'Code Postale',
    // 'past_residence_statut' => 'Status',
    'past_residence_date_from' => 'Depuis',
    'past_residence_date_to' => 'a',

    // Voyage divers
    'various_travel_country' => 'Pays',
    'various_travel_city' => 'Ville',
    'various_travel_reason' => 'But de voyage',
    'various_travel_date_from' => 'Depuis',
    'various_travel_date_to' => 'a',

    // new elements
    'desired_province_for_living' => 'Dans quelle province ou quel territoire est-ce que le candidat a l`intention de résider ?',
    'desired_city_for_living' => 'Dans quelle ville est-ce que le candidat a l`intention de résider ?',
    'has_cert_for_living' => 'Est-ce que le candidat a reçu un certificat de candidat d`une province ou d`un territoire ?',

    //La famille au Canada
    'family_in_canada' => 'La famille au Canada',
    'family_in_canada_relationship' => 'Lien de parenté',
    'family_in_canada_address' => 'Adresse',
    'family_in_canada_phone' => 'Téléphone',

    "eye_colour" => "Couleur des yeux",
    "size" => "Taille (cm) ",
    "previous_spouse_firstname" => "Nom de précédente conjointe ",
    "previous_spouse_lastname" => "Prénom de précédente conjointe ",
    "passport_number" => "Numéro de passeport",
    "passport_expiration_date" => "Date d’expiration ",


    // Questions légales (requis):
    'extra_received_conviction' => "1- reçu une déclaration de culpabilité (crime ou une infraction) au Canada qui n'a pas fait l'objet d'une réhabilitation aux termes de la Loi sur le casier judiciaire ?",
    'extra_committed_criminal_act' => "2- commis un acte criminel, ou fait l'objet d'une arrestation, d'une accusation ou d'une déclaration de culpabilité pour un acte criminel ?",
    'extra_submitted_request' => "3- présenté une demande d'asile au Canada, à un bureau canadien des visas à l'étranger ou auprès du Haut-Commissariat des NU pour les réfugiés (HCR) ?",
    'extra_denied_refugee_status' => "4- refusé le statut de réfugié, un visa d'immigrant ou de résident permanent (y compris CSQ), une demande au titre du Programme des candidats des provinces ou un visa de visiteur ou de résident temporaire ?",
    'extra_been_refused_visa' => "5- vu refuser un visa, un permis, l'entrée dans un pays ou a déjà reçu l'ordre de quitter un pays ?",
    'extra_took_part_in_act_genocide' => "6- pris part à un acte de génocide, un crime de guerre ou un crime contre l’humanité ?",
    'extra_advocated_use_armed_struggle' => "7- utilisé, planifié ou préconisé le recours à la lutte armée ou à la violence pour atteindre des objectifs politiques, religieux ou sociaux ?",
    'extra_had_ties_to_group_that_used_armed_struggle' => "8- eu des liens avec un groupe qui a utilisé, qui utilise, qui a préconisé ou qui préconise le recours à la lutte armée ou à la violence pour atteindre des objectifs politiques, religieux ou sociaux ?",
    'extra_been_member_of_organization' => "9- été membre d'une organisation qui prend ou qui a pris part à une activité s'inscrivant dans un comportement de criminalité ?",
    'extra_is_subject_detention' => "10- fait l'objet d'une détention, d'une incarcération ou d'un emprisonnement ?",
    'extra_suffered_from_serious_illness' => "11- souffert d'une maladie grave ou d'un trouble physique ou mental grave ?",

    //Student fields clear 1001

    //✅ DOSSIER SCOLAIRE -- start -- (sorted in db) 🚀
    'level_study' => "Quel est votre niveau d'étude ?",
    "graduation_year" => "En quelle année avez-vous obtenu votre diplôme ?",
    'branch' => "Quel est votre domaine d'étude (Spécialité, Filière, Branche,...) ?",
    'grade' => "Concernant votre dernier diplôme, quelle note avez-vous obtenue ?",
    'math_grade' => "Combien vous avez eu en  mathématiques ?",

    // New 🧪
    'lang_level_ca' => "Quel est votre niveau de Français ?",
    'lang_level_es' => "Quel est votre niveau d'Espagnol ?",
    'lang_level_en' => "Quel est votre niveau d'Anglais ?",

    // New 🧪
    'language_tests_ca' => "lesquels ?",
    'language_tests_es' => "lesquels ?",
    'language_tests_uk' => "lesquels ?",

    'test_fr' => "Avez-vous déjà passé un Test de Français ?",
    'diploma_fr' => "Avez-vous obtenu un diplôme de Français ?",
    'test_en' => "Avez-vous passé un Test d'Anglais ?",
    'diploma_en' => "Avez-vous obtenu un diplôme d'Anglais ?",
    'is_language_taken' => "Avez-vous un diplôme ou un test de langue ?",



    // DOSSIER SCOLAIRE -- end --

    //✅ ETUDE ENVISAGÉE AU CANADA -- start -- (not sorted in db 🚀) (need to rerun the order seed)
    // Seeder is clean ✅

    "study_session" => "Quelle session d’études vous intéresse ?",
    'language_ca' => "Quelle est votre préférence linguistique pour poursuivre vos études ?",
    'language_es' => "Quelle est votre préférence linguistique pour poursuivre vos études ?",
    'language_uk' => "Quelle est votre préférence linguistique pour poursuivre vos études ?",
    'province' => "Quelle province désirez-vous poursuivre vos études ?",
    'city_ca' => "Dans quelle ville ?",
    'city_es' => "Dans quelle ville ?",
    'city_uk' => "Dans quelle ville ?",
    'est_name' => "Pour quel établissement scolaire s'est fait votre choix ?",
    'training_type' => "Quel type de formation avez-vous opté ?",
    'diplomas_ca' => "Quel diplôme souhaiteriez-vous obtenir ?",
    'diplomas_es' => "Quel diplôme souhaiteriez-vous obtenir ?",
    'diplomas_uk' => "Quel diplôme souhaiteriez-vous obtenir ?",
    'program_first' => "Sur quel programme s'est porté votre 1er choix ?",
    'program_second' => "Sur quel programme s'est porté votre 2ème choix ?",
    // 'annual_budget' => "Quel budget estimez-vous mettre pour couvrir vos frais de scolarité (en CAD) ?",

    // New 🚀
    'annual_budget_ca' => "Quel budget estimez-vous mettre pour couvrir vos frais de scolarité (en CAD) ?",
    'annual_budget_uk' => "Quel budget estimez-vous mettre pour couvrir vos frais de scolarité (en GBP) ?",
    'annual_budget_es' => "Quel budget estimez-vous mettre pour couvrir vos frais de scolarité (en EURO) ?",

    // ETUDE ENVISAGÉE AU CANADA -- end --

    // EVALUATION DU DOSSIER FINANCIER DU GARANT -- start -- (🚀)
    // "relationship_student_guarantor" => "Quel est le lien de parenté entre l'étudiant et le garant ?",
    // Seeder is clean ✅

    'garant_type' => "Quel est votre lien de parenté avec le garant financier ?",

    "garant_last_name" => "Quel est le nom du garant ?",
    "garant_first_name" => "Quel est le prénom du garant ?",
    "guarantor_income_type" => "Quels sont les différents types de revenus de votre garant financier ?",

    "guarantor_work_income" => "En tant que Salarié(e), quel est son revenu mensuel ? ",
    "guarantor_retirement_income" => "En tant que retraité(e), quelle est sa pension moyenne mensuelle ?",
    "guarantor_business_income" => "En tant que patron d'entreprise, quels sont ses revenus mensuels ? ",
    "guarantor_professional_income" => "En tant que profession libérale, quels sont ses gains mensuels ?",
    "guarantor_self_employed_income" => "En tant que travailleur autonome, Quels sont ses gains mensuels ?",
    "guarantor_rental_income" => "Quels sont les revenus générés mensuellement par les biens locatifs ?",
    "guarantor_investment_income" => "Quels sont les revenus que génèrent les investissements et placement ?",


    'goods' => "Est ce que votre garant possède-t-il des biens ?",
    'goods_res' => "Si Oui, combien de biens possède-t-il ?",

    'rental_income' => "Quels sont les revenus générés mensuellement par ces biens ?",
    'bank_amount' => "Quel est son solde bancaire actuel ?",

    // -- this one related to client not guarnt --
    'amount' => "Quel est l'estimation du budget alloué pour financer le projet d'étude ?",

    // EVALUATION DU DOSSIER FINANCIER DU GARANT -- end --

    // SOLUTIONS FINANCIÈRES ET PROGRAMME VOLET DIRECT -- start -- (🚀)
    // Seeder is clean ✅
    'tuition_fees_ca' => "Comptez-vous payer vos frais de scolarité avant la soumission du dossier Visa et permis d'études ?",
    'tuition_fees_uk' => "Comptez-vous payer vos frais de scolarité avant la soumission du dossier Visa  ?",
    'tuition_fees_es' => "Comptez-vous payer vos frais de scolarité avant la soumission du dossier Visa  ?",

    'cpg_account_ca' => "Désirez-vous ouvrir un compte bancaire au Canada ?",
    'cpg_account_uk' => "Désirez-vous ouvrir un compte bancaire ?",
    'cpg_account_es' => "Désirez-vous ouvrir un compte bancaire ?",

    'tef_test_ca' => "Si vous n'avez pas encore fait le test TEF, souhaiteriez-vous le passer ?",
    'tef_test_res_ca' => "Quelle note obtenue dans votre test TEF ?",

    'tef_test_es' => "Si vous n'avez pas encore fait le test PCE ou l'EBAU, souhaiteriez-vous le passer ?",
    'tef_test_res_es' => "Quelle note obtenue dans votre test PCE ou l'EBAU ? ",

    'tef_test_uk' => "Si vous n'avez pas encore fait le test IELTS, souhaiteriez-vous le passer ?",
    'tef_test_res_uk' => "Quelle note obtenue dans votre test IELTS ?",

    // SOLUTIONS FINANCIÈRES ET PROGRAMME VOLET DIRECT -- end --

    // EVALUATION DU DOSSIER CRIMINEL DE L'ÉTUDIANT -- Start -- (🚀)
    // Seeder is clean ✅
    'criminal_record' => 'Avez-vous des antécédents judiciaires ?',
    'criminal_record_rep' => 'Si oui, lesquels ?',
    // EVALUATION DU DOSSIER CRIMINEL DE L'ÉTUDIANT -- End --

    // EVALUATION DU DOSSIER MÉDICAL DE L'ÉTUDIANT -- Start -- (🚀)
    // Seeder is clean ✅
    'health' => 'Avez-vous des maladies chroniques ?',
    'health_rep' => 'Si oui, lesquels ? ',
    // EVALUATION DU DOSSIER MÉDICAL DE L'ÉTUDIANT -- End --

    // INFORMATIONS SUR LES ANCIENNES DEMANDES DE VISAS & PERMIS D’ÉTUDES -- Start -- (🚀)
    // New to here 🚀
    'visa_request' => "Avez-vous déjà déposé des demandes de visas et permis d’études ?",
    'visa_request_ca' => "Avez-vous déjà déposé des demandes de visas et permis d'études ?",
    'visa_request_es' => "Avez-vous déjà déposé des demandes de visa ?",
    'visa_request_uk' => "Avez-vous déjà déposé des demandes de visa ?",

    'visa_request_date' => "Quel est la date de cette demande ?",
    'visa_request_date_placeholder' => "aaaa-mm-jj",
    'visa_request_type' => "Quel type de demande avez-vous fait ?",
    'visa_request_result' => "Quel a été le résultat de cette demande ?",

    // INFORMATIONS SUR LES ANCIENNES DEMANDES DE VISAS & PERMIS D’ÉTUDES -- end --


    'income' => 'Revenu mensuel',
    'business_income' => 'Le revenu d\'entreprise',
    'medical_visit' => 'Visite médicale',

    'anthroprometric' => 'Fiche anthroprometrique',


    // - new fields
    'admission_test_langue' => 'Test de langue',
    'program_third' => 'Programme: troisième choix',
    'admission_fees' => 'Frais de scolarité',
    //Immigrant fields
    'marital_status' => 'Quel est votre état matrimonial?',
    'spouse_cit' => 'Votre époux ou conjoint de fait est-il un citoyen ou un résident permanent du Canada?',
    'spouse_joining' => 'Votre époux ou conjoint de fait vous accompagnera-t-il au Canada?',
    'age' => 'Quel âge avez-vous?',
    'education_level' => 'Quel niveau d’études avez-vous atteint?',
    'diplomas_canadien' => 'Avez-vous obtenu un certificat, grade ou diplôme canadien?',
    'education_type' => 'Choisissez la meilleure réponse pour décrire ce niveau d’éducation.',
    'lang_test_expire' => 'Vos résultats de test linguistique ont-ils moins de deux ans?',
    'lang_test' => 'Quel test linguistique avez-vous passé pour votre première langue officielle?',
    'lang_oral_exp_test_score' => 'Expression orale :',
    'lang_oral_comp_test_score' => 'Compréhension de l\'oral :',
    'lang_writing_comp_test_score' => 'Compréhension de l\'écrit :',
    'lang_writing_exp_test_score' => 'Expression écrite :',
    'other_lang_test' => 'Avez-vous d’autres résultats linguistiques?',
    'lang_speaking' => 'Expression orale :',
    'lang_listening' => 'Compréhension de l\'oral :',
    'lang_reading' => 'Compréhension de l\'écrit :',
    'lang_writing' => 'Expression écrite :',
    'work_experience_canada' => 'Au cours des dix dernières années, combien d\'années d\'expérience de travail qualifié au Canada possédez-vous?',
    'work_experience_abroad' => 'Au cours des dix dernières années, combien d\'années d\'expérience de travail qualifié à l\'étranger possédez-vous?',
    'certificate_of_competence' => 'Possédez-vous un certificat de compétence d\'une province, d\'un territoire ou d’un organisme fédéral canadien?',
    'valid_job' => 'Avez-vous une offre d\'emploi valide appuyée par une étude d\'impact sur le marché du travail (si nécessaire)?',
    'job_kind' => 'Quel est le genre ou le niveau de compétence dans la CNP de l\'offre d\'emploi?',
    'certificate_of_designation' => 'Avez-vous un certificat de désignation d\'une province ou d\'un territoire?',
    'sibling_canada' => 'Est-ce que vous ou votre époux/conjoint de fait (si celui-ci vous accompagne au Canada) avez au moins un frère ou une sœur habitant au Canada qui est un citoyen ou un résident permanent?',
    'spouse_education_level' => 'Quel est le niveau de scolarité le plus élevé pour lequel votre époux ou conjoint de fait a obtenu.',
    'spouse_work_experience_canada' => 'Au cours des dix dernières années, combien d\'années d\'expérience de travail qualifié au Canada votre époux ou conjoint de fait possède-t-il?',
    'spouse_lang_test' => 'Votre époux ou conjoint de fait a-t-il il passé un test linguistique? Si oui, lequel?',
    'spouse_lang_speaking' => 'Expression orale :',
    'spouse_lang_listening' => 'Compréhension de l\'oral :',
    'spouse_lang_reading' => 'Compréhension de l\'écrit :',
    'spouse_lang_writing' => 'Expression écrite :',
    // dev etude clear 1002
    'admission_programme' => 'Programme',
    'admission_lang_study' => 'Langue d\'étude',
    'admission_city' => 'Ville',
    'admission_session' => 'Session',
    'admission_school_level' => 'Niveau d\'étude',
    'admission_est' => 'Établissement',
    'admission_est_name' => 'Nom de l\'Établissement',
    'admission_diplomas' => 'Diplôme',
    'admission_program' => 'Programmes',
    'admission_budget' => 'Budget',
    'admission_langue' => 'Langue',
    'admission_ville' => 'Ville',
    'admission_diplome' => 'Diplôme',
    'admission_type_etablissement' => 'Type de l\'Établissement',
    'admission_prix_total' => 'Prix Total',
    'soumission_list' => 'Liste des soumissions',
    'admission_domaine_etude' => 'Domaine d\'étude',
    'admission_niveau_scolaire' => 'Niveau Scolaire',
    'admission_note' => 'Note',
    'admission_nom_etablissement' => 'Nom d\'etablissement',
    'admission_duree' => 'Durée',
    'admission_frais_admission' => 'Frais d\'admission',
    'admission_prix_annuel' => 'Prix annuel',
    'admission_lien' => 'Lien',
    'admission_date_limit_admission' => 'Date limite d\'admission',
    'admission_score' => 'Score',
    'admission_code_programme' => 'Code de programme',
    'admission_exigence' => 'Exigences',
    'admission_Analyse' => 'Objectifs de formation et débouchés du diplôme',
    'admission_classement_choix' => 'Classement des choix',
    // 'admission_' => '',
    // student docs
    'birth_act' => 'Extrait de naissance',
    'tuition_ctf' => 'Attestation de scolarité',
    'picture' => 'Photographie',
    'diplomas_1' => 'Diplômes et relevés de notes',
    'diplomas_2' => 'Diplômes et relevés de notes',
    'passeport' => 'Copie de la première page de passeport',
    'cv' => 'CV',
    'letter_rcm' => 'Lettre de recommandation',
    //
    'calls-count' => 'Nombre d\'Appel',
    //Soumission
    'make_search' => 'Faire une recherche',
    'search' => 'Rechercher',
    'portal_about_the_lead_backoffice_notesNotes' => 'Notes Backoffice',
    //Agenda
    'agenda' => 'Agenda',
    'add_agenda' => 'Ajouter une planification',
    'lead' => 'Lead',
    'planification' => 'Planification',
    'rencontre' => 'Rencontre',
    'plan_date' => 'Date planification',
    'plan_hour_begin' => 'Heure Début Rencontre',
    'plan_hour_end' => 'Heure Fin Rencontre',
    'description' => 'Description',
    'close' => 'Fermer',
    'edit_agenda' => 'Modifier planification',
    'delete_agenda' => 'Supprimer planification',
    'view_lead' => 'Voir Lead',
    'clients' => 'Clients',

    //
    'call_by_support' => 'appel effectué par support',
    'call_by_conseiller' => 'appel effectué par conseiller',
    'with_payment' => 'avec paiement',
    'with_R1' => 'avec R1',
    'payment_proof' => 'avec Preuve',

    /**
     * Clear 1001 pdf
     */
    'clear1001_title' => 'Rapport d’entretien client',
    "clear1001_date_of_meeting" => "Date de rencontre",
    "clear1001_advisor_name" => "Name of the Clear Path Canada Advisor",
    "clear1001_advisor_phone" => "Numéro de téléphone du conseiller",
    "clear1001_customer_fullname" => "Nom & prénom du client",
    "clear1001_program" => "Programme",
    "clear1001_customer_information_title" => "Informations sur le client",
    "clear1001_customer_dob" => "Date de naissance",
    "clear1001_customer_phone" => "Numéro de téléphone",
    "clear1001_customer_email" => "Adresse mail",
    "clear1001_school_record_title" => "Informations sur le dossier scolaire",
    "clear1001_customer_study_level" => "Niveau d’étude",
    "clear1001_customer_graduation_year" => "Année d'obtention",
    "clear1001_custome_study_field" => "Domaine d’étude :",
    "clear1001_customer_last_mark_diploma" => "Note du dernier diplôme",
    "clear1001_customer_math_score" => "Note des mathématiques",
    "clear1001_customer_bac_note" => "(Pour les bacheliers examen national)",
    "clear1001_customer_french_score" => "Note du français",

    "clear1001_customer_french_test" => "Test de français",

    "clear1001_customer_langue_diploma" => "Diplôme de Langue",
    "clear1001_customer_study_session" => "Session d’études",


    "clear1001_customer_garant_first_name" => "Prenom du garant",
    "clear1001_customer_garant_last_name" => "Nom du garant",
    "clear1001_customer_guarantor_income_type" => "Les types de revenus de garant",




    "clear1001_customer_english_test" => "Test d’anglais",
    "clear1001_customer_english_diploma" => "Diplôme d’anglais",
    "clear1001_planned_studies_info_title" => "Informations sur les études envisagées au Canada",
    "clear1001_customer_language" => "Langue",
    "clear1001_customer_city" => "Ville",
    "clear1001_customer_educational_establishment" => "Etablissement scolaire",
    "clear1001_customer_first_program_choice" => "Programme 1er choix",
    "clear1001_customer_second_program_choice" => "Programme 2ème choix",
    "clear1001_customer_diploma" => "Diplôme",
    "clear1001_customer_tuition_fee" => "Budget des frais de scolarité",
    "clear1001_financial_file_info_title" => "Informations sur le dossier financier",
    "clear1001_customer_guarantor_type" => "Type de garant",
    "clear1001_customer_monthly_income" => "Revenu mensuel",
    "clear1001_customer_current_bank_balance" => "Solde bancaire actuel",
    "clear1001_customer_study_project_budget" => "Budget projet d’étude",
    "clear1001_customer_goods" => "Biens",
    "clear1001_customer_rental_income" => "Revenu locatif",
    "clear1001_customer_business_income" => "Revenu d’entreprise",
    "clear1001_medical_criminal_infos_title" => "Informations médicales et antécédents judiciaires",
    "clear1001_customer_medical_info" => "Avez-vous des maladies chroniques ?",
    "clear1001_customer_criminal_info" => "Avez-vous des antécédents judiciaires ?",
    "clear1001_previous_visa_app_title" => "Informations sur les anciennes demandes des visas & permis d’études",
    "clear1001_customer_previous_visa_app_info" => "Avez-vous déjà fait des demandes de visas et permis d’études ?",

    "clear1001_title_bis" => "Analyse et conseil",

    "clear1001_processing_refusals_title" => "Traitement des refus des anciennes demandes de visa et permis d’études",
    "clear1001_processing_refusals_text" => "Une demande de notes et d’accès à l’information ou de renseignements personnels sera faite afin d’accéder à votre dossier pour soulever les principales raisons derrières le refus des anciennes demandes de visas et permis d’études. L’accès à ces informations nous permettront de capitaliser sur les motifs de refus pour vous conseiller et mener à bien la nouvelle demande.",

    "clear1001_canada_study_programs_title" => "Les programmes d’études au Canada",
    "clear1001_canada_study_programs_text" => "Diplôme d’études professionnelles de 1800 heures et plus : C’est une formation à temps plein qui vous donne la possibilité de choisir une spécialité déterminée par rapport à un métier que vous souhaiterez exercer dans le futur. La durée des cours du DEP est de 1800 heures, c’est l’équivalent de 18 mois d’études qui vous prépare pour intégrer le marché du travail. Il faut noter que c’est un diplôme reconnu au Canada et à l’échelle mondial délivré par le ministère de l’éducation du loisir et du sport du Québec qui coute relativement 20 000$ ou plus.",
    "clear1001_canada_study_programs_subtitle_1" => "Diplôme d’études Collégiales de 2 ans et 3 ans :",
    "clear1001_canada_study_programs_text_1" => "C’est un diplôme avec un parcours d’études établies relativement en 2 ans ou 3 ans dépendamment de la spécialité choisie et qui pourra qui nous mène par la suite vers des études universitaires. Le prix du DEC commence à partir de 30 000 $.",
    "clear1001_canada_study_programs_subtitle_2" => "Diplômes universitaires :",
    "clear1001_canada_study_programs_text_2" => "Le système universitaire canadien est organisé en 4 niveaux :",
    "clear1001_canada_study_programs_subtitle_3" => "Baccalauréat :",
    "clear1001_canada_study_programs_text_3" => "Formation complète qui correspond au 1er cycle universitaire, l’équivalent à la licence et échelonnée sur 3 ans voire même 4 années d’études à temps plein. Le prix du Baccalauréat commence à partir de 40 000 $.",
    "clear1001_canada_study_programs_subtitle_4" => "Maitrise:",
    "clear1001_canada_study_programs_text_4" => "Cursus échelonné sur 2 ans d’études à temps plein. Le prix du Maitrise commence à partir de 25 000 $.",
    "clear1001_canada_study_programs_subtitle_5" => "DESS:",
    "clear1001_canada_study_programs_text_5" => "Formation professionnelle échelonnée à 1 année à temps plein, qui permet un perfectionnement à haut niveau dans un domaine d’études précis. Le prix du DESS commence à partir de 9 000 $.",
    "clear1001_canada_study_programs_subtitle_6" => "Doctorat :",
    "clear1001_canada_study_programs_text_6" => "Formation basée sur la recherche et l’avancement des connaissances dans des domaines d’études. Elle est d’une durée de 4 années d’études à temps plein.",

    "clear1001_guarantor_financial_prerequisites_title" => "Les prérequis financiers du Garant",
    "clear1001_guarantor_financial_prerequisites_subtitle_1" => "Compte bancaire :",
    "clear1001_guarantor_financial_prerequisites_text_1" => "Il faudra appuyer son dossier de candidature par des relevés bancaires personnels et de l’entreprise, le cas échéant, des 6 derniers mois. Ces relevés doivent démontrer une capacité financière solide avec un solde bancaire mensuel d’au moins 300 000 DH pendant les 6 derniers mois.",
    "clear1001_guarantor_financial_prerequisites_subtitle_2" => "Biens immobiliers :",
    "clear1001_guarantor_financial_prerequisites_text_2" => "La disposition des biens immobiliers constitue un plus qu’il faudra en justifier par des certificats de propriété délivré par la conservation foncière ainsi que les contrats locatifs, le cas échéant.",
    "clear1001_guarantor_financial_prerequisites_subtitle_3" => "NB :",
    "clear1001_guarantor_financial_prerequisites_text_3" => "Les patrons d’entreprises sont amenés à présenter les états financiers de leur entreprise. Les travailleurs autonomes doivent justifier leur activité par la carte professionnelle des travailleurs autonomes.",

    "clear1001_financial_solutions_title" => "Les solutions financières",
    "clear1001_financial_solutions_subtitle_1" => "Evaluation des biens immobiliers :",
    "clear1001_financial_solutions_text_1" => "Afin de donner une contre-valeur financière des certificats de propriétés reçus, nous pouvons les évaluer par un professionnel agrémenté à savoir Notaire, Expert-Comptable, évaluateur ou autres.",
    "clear1001_financial_solutions_subtitle_2" => "Paiement des frais de scolarité :",
    "clear1001_financial_solutions_text_2" => "Les personnes désireuses ont la possibilité de payer les frais de scolarité de la 1ère année d’étude. Ces frais s’élèvent à 80 000 MAD et plus.",
    "clear1001_financial_solutions_subtitle_3" => "Blocage des fonds CPG :",
    "clear1001_financial_solutions_text_3" => "Avec la possibilité d’ouverture d’un compte bancaire au Canada pour avoir un Certificat de Placement Garanti (CPG), vous pourrez bloquer des fonds aux alentours de 10 200 $ canadien, l’équivalent de 76 500 MAD. Cette solution vous permet également d’avoir accès à des fonds, une fois au Canada, afin d’y subvenir à vos besoins quotidiens durant votre séjour.",

    "clear1001_direct_component_program_title" => "Le programme volet direct",
    "clear1001_direct_component_program_text" => "Les résidents de certains pays dont le Maroc et le Sénégal sont admissibles à un traitement plus rapide au titre d’un programme volet direct pour les études. La durée est estimée à 20 jours. Pour certains cas, cette durée peut être prolongée à 30 jours. Les prérequis d’admission sont comme suit :",
    "clear1001_direct_component_program_subtitle_1" => "Frais de scolarité :",
    "clear1001_direct_component_program_text_1" => "Paiement des frais de scolarité pour la 1ère année d’étude.",
    "clear1001_direct_component_program_subtitle_2" => "CPG :",
    "clear1001_direct_component_program_text_2" => "Détenir un Certificat de Placement Garanti d’une valeur d’au moins 10 000 $ canadien, l’équivalent de 76 500 MAD.",
    "clear1001_direct_component_program_subtitle_3" => "CAQ :",
    "clear1001_direct_component_program_text_3" => "Un Certificat d’acceptation du Québec, exigible uniquement pour les candidats souhaitant poursuivre leurs études au Québec.",
    "clear1001_direct_component_program_subtitle_4" => "Examen Médical :",
    "clear1001_direct_component_program_text_4" => "Essentiel avant de présenter la demande de visa et permis d’étude.",
    "clear1001_direct_component_program_subtitle_5" => "Certificat de Police :",
    "clear1001_direct_component_program_text_5" => "Fournir une fiche anthropométrique et un casier judiciaire avant de présenter la demande de visa et permis d’étude.",
    "clear1001_direct_component_program_subtitle_6" => "Résultats des tests d’évaluation linguistiques :",
    "clear1001_direct_component_program_text_6" => "Avoir un score de 7 pour TEF et avoir un score de 6 pour IELTS.",

    /**
     * Portal for leads EN
     */
    "portal_personal_infos_title" => "Informations Personnelles",
    "portal_personal_infos_surname" => "Nom",
    "portal_personal_infos_name" => "Prénom",
    "portal_personal_infos_email" => "Email",
    "portal_personal_infos_advisor" => "Conseiller",
    "portal_personal_infos_dob" => "Date de Naissance",
    "portal_personal_infos_phone" => "Téléphone",
    "portal_personal_infos_country" => "Pays",
    "portal_personal_infos_city" => "Ville",
    "portal_personal_infos_work_field" => "Domaines d'emplois",
    "portal_personal_infos_study_field" => "Domaines d'études",
    "portal_personal_infos_field" => "Domaine",
    "portal_personal_infos_programm" => "Programme",
    "portal_personal_infos_edit_button" => "Editer",
    "portal_personal_infos_cancel_button" => "Annuler",
    "portal_personal_infos_upload" => "Uploder",
    "portal_noc" => "CNP",
    "portal_find_your_noc" => "Trouvez votre CNP ici",
    "portal_click_here" => "Cliquez ici",
    "portal_your_noc" => "Votre CNP",
    "portal_skill_level" => "Niveau de compétence",
    "portal_noc_save" => "Sanvegarder",
    "portal_noc_link" => "https://www.canada.ca/fr/immigration-refugies-citoyennete/services/immigrer-canada/entree-express/admissibilite/trouver-classification-nationale-professions.html",
    "portal_histocall_modal_title" => "Ajouter appel client",
    "portal_histocall_modal_phone" => "Téléphone",
    "portal_histocall_modal_choose_status" => "Sélécionner status...",
    "portal_about_the_lead_title" => "A propos du Lead",
    "portal_about_the_lead_reg_date" => "Inscrit le",
    "portal_about_the_lead_assigned_to" => "Affecter à",
    "portal_about_the_lead_status" => "Statut",
    "portal_about_the_lead_comments" => "Notes clients",
    "portal_about_the_lead_write_comments" => "Ecrire un commentaire..",
    "portal_about_the_lead_backoffice_notes" => "Notes Backoffice",
    "portal_about_the_lead_backoffice_write_notes" => "Ecrire une note..",
    "portal_about_the_lead_save_button" => "Enregistrer",
    "portal_about_the_lead_edit_button" => "Modifier",
    "portal_about_the_lead_resend_emails_button" => "Renvoyer Email",
    "portal_doc_regeneration" => "regeneration des documents",
    "portal_doc_regeneration_language" => "Langue",
    "portal_doc_regeneration_currency" => "Devise",
    "portal_doc_regeneration_button" => "Regenerer",
    "portal_payment_method" => "Faire paiement",
    "modal_comment_info_title" => "Commentaire - Information",
    "portal_select_payment_method" => "Sélectionnez méthode de paiement",
    "portal_payment_method_pay" => "Payer",
    "portal_make_appointment" => "Faire un rendez-vous",
    "portal_meet_appointment_title_short" => "RDV",
    "portal_meet_appointment_title" => "Rendez-vous",
    "portal_meet_appointment_date" => "Date du RDV",
    "portal_meet_appointment_time" => "Heure du RDV",
    "portal_meet_appointment_start" => "Début",
    "portal_meet_appointment_end" => "Fin",
    "portal_meeting_title" => "Rencontre",
    "portal_ee_meet_two_content" => "Préparation de la demande d’équivalence et les tests de langues",
    "portal_ee_meet_three_content_one" => "Préparation du profil",
    "portal_ee_meet_three_content_two" => "",
    "portal_doc_type" => "Nom du document",
    "portal_doc_name" => "Nom du document",
    "portal_doc_creation_date" => "Date de création",
    "portal_no_comment" => "Aucun commentaire!",
    "portal_comments" => "Commentaires",
    "portal_clear11" => "PROCÉDURE POUR DEMANDER L'ÉQUIVALENCE DES DIPLÔMES",
    "portal_payment_made_by" => "Paiement effectué par",
    "portal_view_documents" => "Voir les documents",
    "portal_payment_title" => "Paiement",
    "portal_make_payment_btn" => "Effectuer un paiement",
    "portal_in_progress_badge" => "en cours",
    "portal_about_client_title" => "A propos du Client",




    /**
     * Customer Portal
     */
    "customer_portal_my_dossier" => "Mon dossier",
    "customer_portal_docs_to_consolt" => "Documents à consulter",
    "customer_portal_bill_title" => "Reçu",
    "customer_portal_form_to_fill" => "Formulaire a remplir",
    "customer_portal_form_to_upload" => "Documents à téléverser",
    "customer_portal_form_to_upload_selec_key" => "Sélectionnez...",
    "customer_portal_notification_text" => "Afficher toutes les notifications",
    "customer_sheet_title" => "Information sheet",
    "customer_sheet_Add_btn" => "Ajouter",
    "customer_sheet_next_btn" => "Suivant",
    "customer_sheet_previous_btn" => "Précédent",
    "customer_sheet_submit_btn" => "Soumettre",
    "customer_meet_five_title" => "RENCONTRE 5 : SOUMISSION DE LA DEMANDE DU VISA & RP",
    "customer_meet_title" => "Rencontre",
    "customer_fill_btn" => "Faire",
    "customer_view_btn" => "Voir",
    "customer_fill_sheet_btn" => "Voir la fiche",

    "customer_profile_title" => "Information Personnel",
    "customer_profile_name" => "Prénom",
    "customer_profile_surname" => "Nom",
    "customer_profile_password" => "Mot de passe",
    "customer_profile_phone" => "Téléphone",
    "customer_profile_save_btn" => "Enregistrer",


    "lead_form_program" => "Programme",
    "lead_form_program_student_option" => "Etudiant",
    "lead_form_program_immigrant_option" => "Immigrant",

    "clear1001_form_add_garant_btn" => "Ajouter nouveau garant",
    "clear1001_form_add_visa_request_btn" => "Ajouter nouvelle demande",

    // Sidebar && Conseiller portal
    "lead_filter_title" => "Faire une recherche",
    "lead_filter_lead" => "Lead",
    "lead_filter_email" => "Email",
    "lead_filter_phone" => "Telephone",
    "lead_filter_keyword_placeholder" => "Selectionnez mots clés",
    "lead_filter_program" => "Programme",
    "lead_filter_program_placeholder" => "Programmes ...",
    "lead_filter_status" => "Status",
    "lead_filter_status_placeholder" => "Status ...",
    "lead_filter_change" => "Modification",
    "lead_filter_change_placeholder" => "Modification ...",
    "lead_filter_change_true" => "Modifié",
    "lead_filter_change_false" => "Non Modifié",
    "lead_filter_canal_placeholder" => "Canal ...",
    "lead_filter_representing_placeholder" => "Représentant ...",
    "lead_filter_office_placeholder" => "Office ...",
    "lead_filter_call_numbers_placeholder" => "Nombre d'appels ...",
    "lead_filter_support" => "Support",
    "lead_filter_support_placeholder" => "Support ...",
    "lead_filter_search_btn" => "Rechercher",

    "lead_filter_no_calls" => "Pas d'appels",

    "lead_filter_Adviser" => "Conseiller",

    "lead_filter_status_meeting" => "Rencontre",
    "lead_filter_status_untreated" => "Non Traité",
    "lead_filter_status_come_office" => "Venir Au Bureau",
    "lead_filter_status_video_conf" => "Visio Conférence",
    "lead_filter_status_unaffected" => "Non Affecté",

    "lead_list_title" => "Liste des leads",
    "lead_list_add_lead_btn" => "Ajouter lead",

    "cases_list_title" => "Liste des Clients",
    "cases_list_modal_title" => "Modifier Dossier",
    "cases_list_modal_name" => "Prénom",
    "cases_list_modal_surname" => "Nom",
    "cases_list_modal_email" => "Email",
    "cases_list_modal_language_lvl" => "Niveau langue",
    "cases_list_modal_select_language_lvl" => "Sélectionnez un niveau",
    "cases_list_modal_address_line_1" => "Adresse Ligne 1",
    "cases_list_modal_address_line_2" => "Adresse Ligne 2",
    "cases_list_modal_zip_code" => "Code Postal",
    "cases_list_modal_professional_experience" => "Experience professionelle (ans)",
    "cases_list_modal_close_btn" => "Fermer",

    // Calls history
    // filters
    "call_history_make_search" => "Faire une recherche",
    "call_history_surname_placeholder" => "Nom",
    "call_history_users" => "Utilisateurs",
    "call_history_processed" => "Traité",
    "call_history_unprocessed" => "Non Traité",
    "call_history_search_btn" => "Rechercher",

    // main part
    "call_history_title" => "Historiques des appels",
    "call_history_call_duration" => "Durée des appels",

    "call_history_add_call" => "Ajouter appel",
    "call_history_edit_call" => "Modifier appel",

    "call_history_add_lead_call" => "Ajouter appel lead",
    "call_history_add_client_call" => "Ajouter appel client",

    "call_history_edit_lead_call" => "Modifier appel lead",
    "call_history_edit_client_call" => "Modifier appel client",

    "call_history_select_lead" => "Sélectionnez un lead",
    "call_history_select_client" => "Sélectionnez un client",

    "call_history_call_date" => "Date d'appel",
    "call_history_call_duration_bis" => "Durée d'appel",
    "call_history_close" => "Fermer",

    // call form placeholders
    "call_history_duration_placeholder" => "Durée..",

    // adiser profile page
    "adviser_profile_page_subtitle" => "Mon dossier",
    "adviser_profile_info_title" => "Information Personnel",
    "adviser_profile_info_avatar" => "Photo",
    "adviser_profile_info_choose_avatar" => "Sélectionnez une photo",
    "adviser_profile_info_name" => "Nom",
    "adviser_profile_info_password" => "Mot de passe",
    "adviser_profile_info_phone" => "Téléphone",
    "adviser_profile_info_gender" => "Genre",
    "adviser_profile_info_male" => "Male",
    "adviser_profile_info_female" => "Female",
    "adviser_profile_info_save_btn" => "Enregistrer",

    "assign_block_title" => "Affectation des leads",
    "assign_block_language" => "Langue",
    "assign_block_french" => "Français",
    "assign_block_engilsh" => "Anglais",
    "assign_block_country" => "Pays",
    "assign_block_save_btn" => "Enregistrer",

    // logout dropdown
    "logout" => "Déconnexion",
    "notification_text" => "Afficher toutes les notifications",

    // clear1004 - additional fields
    "clear1004_save_btn" => "Enregistrer",
    "clear1004_calculate_your_results" => "Calculer Vos Résultats",

    // Customer Page
    "customer_welcome_message" => "Bienvenue dans Votre Portail",
    "customer_services_message" => "Services CLEARPATH Canada !",
    "customer_current_password" => "Mot de passe courant",
    "customer_password" => "Mot de passe",
    "customer_confirm_password" => "Confirmer mot de passe",
    "customer_change_password" => "Changer mot de passe",
];
