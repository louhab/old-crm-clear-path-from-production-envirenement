<?php

return [
    'error' => 'Une erreur s\'est produite.',
    'success' => 'Opération terminée avec succès.'
];