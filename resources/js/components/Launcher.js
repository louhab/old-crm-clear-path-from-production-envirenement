import cn from "classnames";

import OpenLauncher from "./assets/phone-call-1.svg";

function Launcher({ showChat, updateShowChat }) {
    const toggleCall = () => {
        const selector = document.querySelector("body").className;
        if (selector.endsWith("modal-open")) return;
        if (showChat) updateShowChat(false);
        else updateShowChat(true);
    };
    return (
        <button
            type="button"
            className={cn("rcw-launcher", { "rcw-hide-sm": showChat })}
            onClick={toggleCall}
        >
            {showChat ? (
                <img src={OpenLauncher} className="rcw-close-launcher" />
            ) : (
                <img src={OpenLauncher} className="rcw-open-launcher" />
            )}
        </button>
    );
}

export default Launcher;
