import React from "react";

export default ({ value, max }) => {
    return (
        <progress value={value} max={max}>
            {value}
        </progress>
    );
};
