const close = require('./assets/clear-button.svg');

function Header({ title, subtitle, showChat, updateShowChat, showCloseButton, titleAvatar }) {

    const toggleCall = () => {
        if (showChat)
            updateShowChat(false);
        else
            updateShowChat(true);
    };
    return (
        <div className="rcw-header">
            {showCloseButton &&
                <button className="rcw-close-button" onClick={toggleCall}>
                    {/*<img src={close} className="rcw-close" alt="close" />*/}
                    <i className="fa fa-fw fa-times text-white"></i>
                </button>
            }
            {/*<h4 className="rcw-title">
                {titleAvatar && <img src={titleAvatar} className="avatar" alt="profile" />}
                {title}
            </h4>*/}
            <span>{subtitle}</span>
        </div>
    );
}

export default Header;
