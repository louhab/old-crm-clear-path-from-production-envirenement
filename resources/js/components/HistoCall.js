import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import { useEffect, useState, useCallback } from "react";
import Select from "react-select";
import AsyncSelect from "react-select/async";
import $ from "jquery";

import "./style.scss";
import cn from "classnames";

import Stopwatch from "./Stopwatch";
import Launcher from "./Launcher";
import Header from "./Header";

const STATUS = {
    STARTED: "Started",
    STOPPED: "Stopped",
};

const INITIAL_COUNT = 0;

const INITIAL_CALL_DATA = {
    note: "",
    city: "",
};

function isEmpty(str) {
    return !str || str.length === 0 || /^\s*$/.test(str);
}

// change dom after submiting form
const refreshPage = (lead_state_call_id, states, lead_id) => {
    // change DOM

    if (!lead_state_call_id) return;
    const lead_status = states.find(
        (el) => el.value === String(lead_state_call_id)
    ).label;

    if (lead_status === "Rappel") {
        window.location.href = "/calendar";
    }

    const bagdeElFlag = document.querySelector(`#lead-badge-${lead_id}`);

    if (!!!bagdeElFlag) return;

    // leads status
    var badge = "danger";
    switch (String(lead_state_call_id)) {
        case "1":
            badge = "success";
            break;
        case "3":
            badge = "secondary";
            break;
        case "4":
            badge = "info";
            break;
        case "5":
            badge = "warning";
            break;
    }

    const bagdeEl = $(`#lead-badge-${lead_id}`);
    bagdeEl.text(bagdeEl.text().replace(bagdeEl.text(), lead_status));
    bagdeEl.removeClass(bagdeEl.attr("class").split(" ")[1]);
    bagdeEl.addClass(`badge-${badge}`);
};

export default function HistoCall() {
    const [seconds, setSeconds] = useState(INITIAL_COUNT);
    const [status, setStatus] = useState(STATUS.STOPPED);
    const [isSuccess, setIsSuccess] = useState(false);

    const [showChat, updateShowChat] = useState(false);
    const [states, setStates] = useState([]);
    const [isDisabled, toggleDisabled] = useState(true);
    const [selectValue, setSelctValue] = useState(null);
    const [leadDefaultValue, setLeadDefaultValue] = useState(null);
    const [callData, setCallData] = useState(INITIAL_CALL_DATA);

    const isNumber = (value) => {
        const re = /^[-+ 0-9]+$/;
        if (value === "" || re.test(value)) {
            return true;
        } else {
            return false;
        }
    };

    // onload page
    useEffect(() => {
        if (!isEmpty(localStorage.getItem("status"))) {
            setStatus(localStorage.getItem("status"));
        }

        if (!isEmpty(localStorage.getItem("seconds"))) {
            setSeconds(Number(localStorage.getItem("seconds")));
        }

        if (
            !isEmpty(localStorage.getItem("note")) ||
            !isEmpty(localStorage.getItem("city"))
        ) {
            setCallData({
                note: localStorage.getItem("note"),
                city: localStorage.getItem("city"),
            });
        }

        updateShowChat(JSON.parse(window.localStorage.getItem("showChat")));
        let leadObj = JSON.parse(
            window.localStorage.getItem("leadDefaultValue")
        );
        setLeadDefaultValue(leadObj);
        if (leadObj !== null) {
            axios
                .get("/api/v1/getCallStates/" + leadObj.value)
                .then((response) => {
                    // Use the result as both the value and the label for the dropdown
                    let data = [];
                    for (const [key, value] of Object.entries(response.data)) {
                        data.push({ value: key, label: value });
                    }
                    setStates(data);
                });
            if (leadObj.isClient) toggleDisabled(true);
            else toggleDisabled(false);
        }
    }, []);

    // store stopwatch status and data in local storage
    useEffect(() => {
        window.localStorage.setItem("seconds", seconds);
        window.localStorage.setItem("status", status);
    }, [status, seconds]);

    useEffect(() => {
        window.localStorage.setItem("city", callData.city);
    }, [callData.city]);

    useEffect(() => {
        window.localStorage.setItem("note", callData.note);
    }, [callData.note]);

    useEffect(() => {
        window.localStorage.setItem("showChat", showChat);
        window.localStorage.setItem(
            "leadDefaultValue",
            JSON.stringify(leadDefaultValue)
        );
    }, [showChat, leadDefaultValue]);

    let className = showChat ? "active" : "hidden";

    const handleSelectChange = (event) => {
        setSelctValue(event);
        if (leadDefaultValue !== null) {
            let call = {
                "add-form-duration": seconds,
                "add-form-call_descr": callData.note,
                "add-form-call_city": callData.city,
                "add-form-lead_id": leadDefaultValue.value,
                lead_status: event.value,
            };
            axios
                .post("/api/v1/histocalls", call)
                .then(() => {
                    refreshPage(
                        event.value, // status id
                        states,
                        leadDefaultValue.value // lead id
                    );
                    setIsSuccess(true);
                    setStatus(STATUS.STOPPED);
                    setSeconds(INITIAL_COUNT);
                    setCallData(INITIAL_CALL_DATA);
                    setSelctValue(null);
                    setLeadDefaultValue(null);
                    toggleDisabled(true);
                    // reload page if the current location is [lead] view
                    let url = new URL(window.location.href);
                    if (
                        !!url.pathname
                            .split("/")
                            .filter((el) => el)
                            .find((el) => el === "lead")
                    ) {
                        window.location.reload();
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    };

    const asyncChange = (term) => {
        return axios.post("/api/v1/getClients", { term }).then((response) => {
            return response.data.map((result) => ({
                value: result.id,
                label: isNumber(term) ? result.phone : result.name,
                name: result.name,
                phone: result.phone,
                conseiller: result.conseiller,
                support: result.support,
                city: result.city,
                isClient: result?.is_client || 0,
            }));
        });
    };

    const handleAsyncChange = (event) => {
        if (event != null) {
            if (event.city)
                setCallData((initialData) => ({
                    ...initialData,
                    city: event.city,
                }));
            setLeadDefaultValue({
                value: event.value,
                name: event.name,
                phone: event.phone,
                label: event.label,
                conseiller: event.conseiller,
                support: event.support,
                isClient: event.isClient,
            });
            if (event.isClient == 0) {
                axios
                    .get("/api/v1/getCallStates/" + event.value)
                    .then((response) => {
                        // Use the result as both the value and the label for the dropdown
                        let data = [];
                        for (const [key, value] of Object.entries(
                            response.data
                        )) {
                            data.push({ value: key, label: value });
                        }
                        setStates(data);
                    });
                toggleDisabled(false);
            } else toggleDisabled(true);
        } else {
            setLeadDefaultValue(null);
            setStates([]);
            setSelctValue(null);
            toggleDisabled(true);
        }
    };

    const handleCallDataChange = ({ target: { name, value } }) => {
        setCallData((initialData) => ({
            ...initialData,
            [name]: value,
        }));
    };

    const handleClear = () => {
        setIsSuccess(false);
        setStatus(STATUS.STOPPED);
        setSeconds(INITIAL_COUNT);
        setCallData(INITIAL_CALL_DATA);
        setSelctValue(null);
        setLeadDefaultValue(null);
        toggleDisabled(true);
        window.localStorage.clear(); // clear localstorage
    };

    const handleSubmit = () => {
        setStatus(STATUS.STOPPED);
        if (leadDefaultValue !== null) {
            let call = {
                "add-form-duration": seconds,
                "add-form-call_descr": callData.note,
                "add-form-call_city": callData.city,
                "add-form-lead_id": leadDefaultValue.value,
                lead_status: selectValue?.value,
            };
            axios
                .post("/api/v1/histocalls", call)
                .then(() => {
                    refreshPage(
                        selectValue?.value, // status id
                        states,
                        leadDefaultValue.value // lead id
                    );
                    setIsSuccess(true);
                    setStatus(STATUS.STOPPED);
                    setSeconds(INITIAL_COUNT);
                    setCallData(INITIAL_CALL_DATA);
                    setSelctValue(null);
                    setLeadDefaultValue(null);
                    toggleDisabled(true);
                    // reload page if the current location is [case] view
                    window.localStorage.clear(); // clear local storage after submitimg data
                    let url = new URL(window.location.href);
                    if (
                        url.pathname
                            .split("/")
                            .filter((el) => el)
                            .find((el) => el === "cases") &&
                        url.pathname.split("/").filter((el) => el).length === 2
                    ) {
                        window.location.reload();
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    };

    return (
        <div
            className={cn("rcw-widget-container", {
                "rcw-full-screen": true,
                "rcw-previewer": true,
                "rcw-close-widget-container ": true,
            })}
        >
            <div
                id="rcw-conversation-container"
                className={cn("rcw-conversation-container", className)}
                aria-live="polite"
            >
                <Header
                    title={"title"}
                    subtitle={"Faire Appel"}
                    showChat={showChat}
                    updateShowChat={updateShowChat}
                    showCloseButton={true}
                    titleAvatar={"alt"}
                />
                <Stopwatch
                    seconds={seconds}
                    status={status}
                    setSeconds={setSeconds}
                    setStatus={setStatus}
                    INITIAL_COUNT={INITIAL_COUNT}
                    isSuccess={isSuccess}
                    setIsSuccess={setIsSuccess}
                />

                <div className={"rcw-call"}>
                    <div className={"form-group"}>
                        <AsyncSelect
                            className="select"
                            loadOptions={asyncChange}
                            placeholder="Recherche par Nom, Prénom, Téléphone"
                            value={leadDefaultValue}
                            isClearable="true"
                            onChange={handleAsyncChange}
                        />
                    </div>
                    {leadDefaultValue && !isNumber(leadDefaultValue.label) && (
                        <div className={"form-group"}>
                            <p>
                                <strong>Phone : </strong>
                                {leadDefaultValue.phone}
                            </p>
                        </div>
                    )}
                    {leadDefaultValue && isNumber(leadDefaultValue.label) && (
                        <div className={"form-group"}>
                            <p>
                                <strong>Nom : </strong>
                                {leadDefaultValue.name}
                            </p>
                        </div>
                    )}

                    <div className="form-group mt-4">
                        <label
                            htmlFor="add-form-call_city"
                            className="col-form-label"
                        >
                            Ville :
                        </label>
                        <input
                            type="text"
                            className="form-control form-control-alt"
                            id="add-form-call_city"
                            name="city"
                            placeholder="Ville"
                            value={callData.city}
                            onChange={handleCallDataChange}
                            aria-describedby="add-form-call_city-error"
                            aria-invalid="false"
                        />
                    </div>
                    <div className="form-group mt-4">
                        <label
                            htmlFor="add-form-call_descr"
                            className="col-form-label"
                        >
                            Note :
                        </label>
                        <textarea
                            type="text"
                            className="form-control form-control-alt"
                            id="add-form-call_descr"
                            name="note"
                            placeholder="Notes.."
                            value={callData.note}
                            onChange={handleCallDataChange}
                            aria-describedby="add-form-call_descr-error"
                            aria-invalid="false"
                        ></textarea>
                    </div>
                    {!isDisabled && (
                        <div className={"form-group"}>
                            <Select
                                className="basic-single"
                                isDisabled={isDisabled}
                                isClearable="true"
                                name="status"
                                value={selectValue}
                                onChange={handleSelectChange}
                                menuPlacement="top"
                                options={states}
                            />
                        </div>
                    )}
                    <div className={"form-group"}>
                        <button
                            type="button"
                            className="btn btn-outline-secondary"
                            onClick={handleClear}
                        >
                            Vider
                        </button>
                        {isDisabled && (
                            <button
                                type="button"
                                className="btn btn-alt-primary float-right"
                                onClick={handleSubmit}
                            >
                                Enregistrer
                            </button>
                        )}
                    </div>
                </div>
            </div>
            <Launcher showChat={showChat} updateShowChat={updateShowChat} />
        </div>
    );
}

if (document.getElementById("histo-call")) {
    ReactDOM.render(<HistoCall />, document.getElementById("histo-call"));
}
