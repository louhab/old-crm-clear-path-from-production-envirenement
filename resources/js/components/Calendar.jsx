import React, { useState, useEffect } from "react";
import axios from "axios";
import ReactDOM from "react-dom";

// import "./meeting.css";

function isEmpty(str) {
    return !str || str.length === 0 || /^\s*$/.test(str);
}

export default function MeetingShow() {
    const [activeSlide, SetActiveSlide] = useState(0);
    useEffect(() => {}, []);

    return <div>hello</div>;
}

if (document.getElementById("calendar_component")) {
    ReactDOM.render(
        <MeetingShow />,
        document.getElementById("calendar_component")
    );
}
