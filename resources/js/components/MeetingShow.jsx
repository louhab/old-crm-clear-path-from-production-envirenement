import React, { useState, useRef, useEffect } from "react";
import axios from "axios";
import ReactDOM from "react-dom";
import ProgressBar from "./ProgressBar";

import "./meeting.css";

const STATUS = {
    STARTED: "Started",
    STOPPED: "Stopped",
};

function isEmpty(str) {
    return !str || str.length === 0 || /^\s*$/.test(str);
}

const SLIDES = [
    {
        label: "silde 1",
        content:
            "Accueil, déterminer le profil duclient potentiel et comprendre son besoin",
        hasButton: false,
        // duration: 180,
        duration: 5,
    },
    {
        label: "silde 2",
        content: "Vendre le Canada",
        // duration: 120,
        duration: 6,
        hasButton: false,
    },
    {
        label: "silde 3",
        content:
            "Vendre les services de Clear Path Canada et expliquer notre processus de 6 rencontres",
        // duration: 480,
        duration: 7,
        hasButton: false,
    },
    {
        label: "silde 4",
        content: "Simulation: Remplir le Clear 1001",
        // duration: 1020,
        duration: 8,
        hasButton: true,
    },
    {
        label: "silde 5",
        content: "Simulation: Expliquer le devis",
        // duration: 300,
        duration: 9,
        hasButton: false,
    },
    {
        label: "silde 6",
        content: "Simulation: Expliquer le contrat",
        // duration: 300,
        duration: 10,
        hasButton: false,
    },
    {
        label: "silde 7",
        content: "Simulation: Expliquer le portail client potentiel",
        // duration: 180,
        hasButton: false,
        duration: 11,
    },
];

export default function MeetingShow() {
    const [slides, setSlides] = useState(SLIDES);
    const [activeSlide, SetActiveSlide] = useState(0);
    const [secondsRemaining, setSecondsRemaining] = useState(
        slides[activeSlide]?.duration || 0
    );
    const [status_, setStatus_] = useState(STATUS.STOPPED);
    const [isEnded, setIsEnded] = useState(false);
    const [isStarted, setIsStarted] = useState(false);
    const [progress, setProgress] = useState(0);

    const secondsToDisplay = secondsRemaining % 60;
    const minutesRemaining = (secondsRemaining - secondsToDisplay) / 60;
    const minutesToDisplay = minutesRemaining % 60;
    const hoursToDisplay = (minutesRemaining - minutesToDisplay) / 60;

    useEffect(() => {
        if (!isEmpty(localStorage.getItem("activeSlide"))) {
            SetActiveSlide(JSON.parse(localStorage.getItem("activeSlide")));
        }

        if (!isEmpty(localStorage.getItem("secondsRemaining"))) {
            setSecondsRemaining(
                JSON.parse(localStorage.getItem("secondsRemaining"))
            );
        }

        if (!isEmpty(localStorage.getItem("status_"))) {
            setStatus_(localStorage.getItem("status_"));
        }

        if (!isEmpty(localStorage.getItem("isEnded"))) {
            setIsEnded(JSON.parse(localStorage.getItem("isEnded")));
        }

        if (!isEmpty(localStorage.getItem("isStarted"))) {
            setIsStarted(JSON.parse(localStorage.getItem("isStarted")));
        }

        if (!isEmpty(localStorage.getItem("progress"))) {
            setProgress(JSON.parse(localStorage.getItem("progress")));
        }
    }, []);

    // useEffect(async () => {
    //     const { data } = await axios.get("/api/v1/first-meeting-slides");
    //     if (data) {
    //         console.log(data);
    //         setSlides(data);
    //     }
    // }, []);

    useEffect(() => {
        localStorage.setItem("activeSlide", activeSlide);
        localStorage.setItem("secondsRemaining", secondsRemaining);
        localStorage.setItem("status_", status_);
        localStorage.setItem("isEnded", isEnded);
        localStorage.setItem("isStarted", isStarted);
        localStorage.setItem("progress", progress);
    }, [activeSlide, secondsRemaining, status_, isEnded, isStarted, progress]);

    const handleStartClick = () => {
        if (status_ === STATUS.STARTED) {
            setStatus_(STATUS.STOPPED);
            SetActiveSlide(0);
            setSecondsRemaining(slides[0].duration || 0);
            setIsEnded(false);
            setIsStarted(false);
            setProgress(0);
        } else {
            setIsStarted(true);
            setStatus_(STATUS.STARTED);
            SetActiveSlide(0);
            setSecondsRemaining(slides[0].duration || 0);
            setIsEnded(false);
            setProgress(0);
        }
    };

    const handleNextbtn = () => {
        SetActiveSlide((initialSlide) =>
            initialSlide < slides.length ? initialSlide + 1 : 0
        );
        setSecondsRemaining(() => slides[activeSlide + 1]?.duration || 0);
        console.log("Console", slides[activeSlide]);
        setStatus_(STATUS.STARTED);
    };

    useInterval(
        () => {
            if (secondsRemaining > 0) {
                setSecondsRemaining(secondsRemaining - 1);
            } else if (secondsRemaining === 0 && status_ === STATUS.STOPPED) {
                setIsEnded(progress + 1 < slides.length ? false : true);
                setIsStarted(progress + 1 < slides.length ? true : false);
            } else {
                setStatus_(STATUS.STOPPED);
                setProgress((initialProgress) =>
                    initialProgress < slides.length
                        ? initialProgress + 1
                        : initialProgress
                );
                setIsEnded(progress + 1 < slides.length ? false : true);
                setIsStarted(progress + 1 < slides.length ? true : false);
            }
        },
        status_ === STATUS.STARTED ? 1000 : null
    );

    return (
        <div className="mt__containner">
            <div className="mt__header">
                <img src="https://crm.clearpathcanada.com/img/logo-fr.png" />
                <button onClick={handleStartClick}>
                    {!isStarted ? "Démarrer" : "Quitter"}
                </button>
            </div>
            <div className="mt__progress">
                <ProgressBar
                    value={(progress / slides.length) * 100}
                    max={100}
                />
                <p>
                    {progress}/{slides.length}
                </p>
            </div>
            {isEnded === false ? (
                isStarted === true ? (
                    <div className="mt__main">
                        <div className="mt__timer-wrapper">
                            <div className="mt__timer-main">
                                <span className="mt__timer-numbers">
                                    {twoDigits(hoursToDisplay)}
                                </span>
                                <span className="mt__timer-dots">:</span>
                                <span className="mt__timer-numbers">
                                    {twoDigits(minutesToDisplay)}
                                </span>
                                <span className="mt__timer-dots">:</span>
                                <span className="mt__timer-numbers">
                                    {twoDigits(secondsToDisplay)}
                                </span>
                            </div>
                        </div>
                        <div className="mt__content-wrapper">
                            <div className="mt__content-main">
                                {slides[activeSlide]?.content}
                                {slides[activeSlide]?.hasButton == true ? (
                                    <button>
                                        <a
                                            href={
                                                "/sheets/" +
                                                new URL(
                                                    window.location
                                                ).pathname
                                                    .split("/")
                                                    .filter((el) => el)[1] +
                                                "/1"
                                            }
                                            target="_blank"
                                        >
                                            Faire clear 1001
                                        </a>
                                    </button>
                                ) : (
                                    ""
                                )}
                            </div>
                        </div>
                    </div>
                ) : (
                    <div className="mt__main">
                        <div className="mt__no-content">
                            Presentation CPC de Rencontre 1
                        </div>
                    </div>
                )
            ) : (
                <div className="mt__main">
                    <div className="mt__no-content">Rencontre terminée!</div>
                </div>
            )}
            <div className="mt__footer">
                {isStarted && status_ === STATUS.STOPPED && (
                    <button onClick={handleNextbtn}>Suivant</button>
                )}
            </div>
        </div>
    );
}

function useInterval(callback, delay) {
    const savedCallback = useRef();

    useEffect(() => {
        savedCallback.current = callback;
    }, [callback]);

    useEffect(() => {
        function tick() {
            savedCallback.current();
        }
        if (delay !== null) {
            let id = setInterval(tick, delay);
            return () => clearInterval(id);
        }
    }, [delay]);
}

const twoDigits = (num) => String(num).padStart(2, "0");

if (document.getElementById("meeting-show")) {
    ReactDOM.render(<MeetingShow />, document.getElementById("meeting-show"));
}
