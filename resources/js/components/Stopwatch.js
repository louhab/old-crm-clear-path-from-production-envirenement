import React, { useEffect, useRef } from "react";

const STATUS = {
    STARTED: "Started",
    STOPPED: "Stopped",
};

export default function Stopwatch({
    status,
    seconds,
    setStatus,
    setSeconds,
    INITIAL_COUNT,
    isSuccess,
    setIsSuccess,
}) {
    const secondsToDisplay = seconds % 60;
    const minutesRemaining = (seconds - secondsToDisplay) / 60;
    const minutesToDisplay = minutesRemaining % 60;
    const hoursToDisplay = (minutesRemaining - minutesToDisplay) / 60;

    const handleStart = () => {
        setStatus(STATUS.STARTED);
    };
    const handleStop = () => {
        setStatus(STATUS.STOPPED);
    };
    const handleReset = () => {
        setStatus(STATUS.STOPPED);
        setSeconds(INITIAL_COUNT);
    };
    const hnndleSuccess = () => {
        setIsSuccess(false);
    };
    useInterval(
        () => setSeconds((initialVal) => initialVal + 1),
        status === STATUS.STARTED ? 1000 : null
    );

    return (
        <div style={{ backgroundColor: "#fff", padding: "10px 0" }}>
            {/* alert */}
            {isSuccess && (
                <div
                    className="alert alert-success alert-dismissable"
                    role="alert"
                    style={{
                        width: "75%",
                        margin: "10px auto",
                    }}
                >
                    <button
                        type="button"
                        className="close"
                        data-dismiss="alert"
                        aria-label="Close"
                        onClick={hnndleSuccess}
                    >
                        <span aria-hidden="true">×</span>
                    </button>
                    L'appel a été ajouté avec succès.
                </div>
            )}

            <div className="row justify-content-md-center pt-2">
                <div className="text-center clock-wrapper h1">
                    <span className="hours">{twoDigits(hoursToDisplay)}</span>
                    <span className="dots">:</span>
                    <span className="minutes">
                        {twoDigits(minutesToDisplay)}
                    </span>
                    <span className="dots">:</span>
                    <span className="seconds">
                        {twoDigits(secondsToDisplay)}
                    </span>
                </div>
            </div>
            <div className="row justify-content-md-center pt-2">
                <div className="buttons-wrapper buttons">
                    <button
                        className="btn btn-sm btn-alt-secondary"
                        onClick={handleStart}
                    >
                        <i className="fa fa-fw fa-play"></i>Start
                    </button>
                    <button
                        className="btn btn-sm btn-alt-secondary"
                        onClick={handleStop}
                    >
                        <i className="fa fa-fw fa-pause"></i>Stop
                    </button>
                    <button
                        className="btn btn-sm btn-alt-secondary"
                        onClick={handleReset}
                    >
                        <i className="fa fa-fw fa-trash-restore"></i>Reset
                    </button>
                </div>
            </div>
        </div>
    );
}

function useInterval(callback, delay) {
    const savedCallback = useRef();

    useEffect(() => {
        savedCallback.current = callback;
    }, [callback]);

    useEffect(() => {
        function tick() {
            savedCallback.current();
        }
        if (delay !== null) {
            let id = setInterval(tick, delay);
            return () => clearInterval(id);
        }
    }, [delay]);
}

const twoDigits = (num) => String(num).padStart(2, "0");
