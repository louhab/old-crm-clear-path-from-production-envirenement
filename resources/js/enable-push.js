// Shorthand for $( document ).ready()
$(function() {
    function fetch(limit = 5) {
        console.log('window.Laravel.csrfToken', window.Laravel.csrfToken)
        axios.get(`${process.env.MIX_API_URL}/notifications`, { params: { limit } })
            .then(({ data: { total, notifications } }) => {
                console.log('fetch notifications', notifications)
                console.log('fetch total', total)
                /*total = total*/

                $('#notification-badge').html(total)

                $('#notificaton-dropdown').append(
                    notifications.map(({id, data, created}) => {
                        return `<li data-id="${id}">
                                <a class="text-dark media py-2" href="${id}">
                                    <div class="mr-2 ml-3">
                                        <i class="fa fa-fw fa-bullhorn text-success"></i>
                                    </div>
                                    <div class="media-body pr-2">
                                        <div class="font-w600">${data.title}</div>
                                        <span class="font-w500 text-muted">${created}</span>
                                    </div>
                                </a>
                            </li>`;
                    })
                )

        })
    }

    /**
     * Register the service worker.
     */
    function registerServiceWorker () {
        console.log('registerServiceWorker 1')
        if (!('serviceWorker' in navigator)) {
            console.log('registerServiceWorker 2')
            console.log('Service workers aren\'t supported in this browser.')
            $('#so-settings-check1').attr('disabled', true)
            return
        }
        console.log('registerServiceWorker 3')
        navigator.serviceWorker.register('/sw.js')
            .then(() => initialiseServiceWorker())
    }
    function initialiseServiceWorker () {
        console.log('registerServiceWorker 4')
        if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
            console.log('Notifications aren\'t supported.')
            console.log('registerServiceWorker 5')
            $('#so-settings-check1').prop('checked', false)
            $('#so-settings-check1').attr('disabled', true)
            return
        }
        if (Notification.permission === 'denied') {
            console.log('registerServiceWorker 6')
            $('#so-settings-check1').prop('checked', false)
            $('#so-settings-check1').attr('disabled', true)
            return
        }
        if (!('PushManager' in window)) {
            console.log('registerServiceWorker 7')
            console.log('Push messaging isn\'t supported.')
            $('#so-settings-check1').prop('checked', false)
            $('#so-settings-check1').attr('disabled', true)
            return
        }

        console.log('registerServiceWorker 999')

        navigator.serviceWorker.ready.then(registration => {
            console.log('registerServiceWorker 888')
            registration.pushManager.getSubscription()
                .then(subscription => {
                    console.log('registerServiceWorker 8')
                    $('#so-settings-check1').prop("disabled", false);
                    if (!subscription) {
                        console.log('registerServiceWorker 9')
                        $('#so-settings-check1').prop('checked', false)
                        return
                    }
                    updateSubscription(subscription)
                })
                .catch(e => {
                    console.log('registerServiceWorker 10')
                    console.log('Error during getSubscription()', e)
                })
        })
    }

    /**
     * Subscribe for push notifications.
     */
    function subscribe () {
        navigator.serviceWorker.ready.then(registration => {
            const options = { userVisibleOnly: true }
            const vapidPublicKey = window.Laravel.vapidPublicKey
            if (vapidPublicKey) {
                options.applicationServerKey = urlBase64ToUint8Array(vapidPublicKey)
            }
            registration.pushManager.subscribe(options)
                .then(subscription => {
                    updateSubscription(subscription)
                })
                .catch(e => {
                    if (Notification.permission === 'denied') {
                        $('#so-settings-check1').prop('disabled', true)
                        $('#so-settings-check1').prop('checked', false)
                    } else {
                        console.log('Unable to subscribe to push.', e)
                        $('#so-settings-check1').prop('checked', false)
                    }
                })
        })
    }
    /**
     * Unsubscribe from push notifications.
     */
    function unsubscribe () {
        console.log('unsubscribe')
        navigator.serviceWorker.ready.then(registration => {
            registration.pushManager.getSubscription().then(subscription => {
                if (!subscription) {
                    $('#so-settings-check1').prop('checked', false)
                    return
                }
                subscription.unsubscribe().then(() => {
                    deleteSubscription(subscription)
                }).catch(e => {
                    console.log('Unsubscription error: ', e)
                })
            }).catch(e => {
                console.log('Error thrown while unsubscribing.', e)
            })
        })
    }
    /**
     * Toggle push notifications subscription.
     */
    function togglePush () {
        if ($('#so-settings-check1').is(':checked')) {
            subscribe()
        } else {
            unsubscribe()
        }
    }
    /**
     * Send a request to the server to update user's subscription.
     *
     * @param {PushSubscription} subscription
     */
    function updateSubscription (subscription) {
        console.log('calling updateSubscription')
        const key = subscription.getKey('p256dh')
        const token = subscription.getKey('auth')
        const contentEncoding = (PushManager.supportedContentEncodings || ['aesgcm'])[0]
        const data = {
            endpoint: subscription.endpoint,
            publicKey: key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : null,
            authToken: token ? btoa(String.fromCharCode.apply(null, new Uint8Array(token))) : null,
            contentEncoding
        }
        axios.post(`${process.env.MIX_API_URL}/subscriptions`, data)
            .then(() => { console.log('check1');$('#so-settings-check1').attr('checked', true) })
    }
    /**
     * Send a requst to the server to delete user's subscription.
     *
     * @param {PushSubscription} subscription
     */
    function deleteSubscription (subscription) {
        axios.post( `${process.env.MIX_API_URL}/subscriptions/delete`, { endpoint: subscription.endpoint })
            .then(() => { $('#so-settings-check1').attr('checked', false) })
    }
    /**
     * Send a request to the server for a push notification.
     */
    function sendNotification () {
        axios.post(`${process.env.MIX_API_URL}/notifications`)
            .catch(error => console.log(error))
            .then(() => { })
    }
    /**
     * https://github.com/Minishlink/physbook/blob/02a0d5d7ca0d5d2cc6d308a3a9b81244c63b3f14/app/Resources/public/js/app.js#L177
     *
     * @param  {String} base64String
     * @return {Uint8Array}
     */
    function urlBase64ToUint8Array (base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4)
        const base64 = (base64String + padding)
            .replace(/-/g, '+')
            .replace(/_/g, '/')
        const rawData = window.atob(base64)
        const outputArray = new Uint8Array(rawData.length)
        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i)
        }
        return outputArray
    }
    function initDOMEvents() {

        $('#so-settings-check1').on('click', function () {
            togglePush()
        })

        $('#send-push-btn').on('click', () => {
            console.log('send-push-btn click')
            sendNotification()
        })
    }
    function handlePusherEvents() {
        console.log('window.Laravel', window.Laravel)
        window.Echo.private(`App.User.${window.Laravel.user.id}`)
            .notification(notification => {
                /*total++
                notifications.unshift(notification)*/
            })
            .listen('NotificationRead', ({ notificationId }) => {
                /*total--
                const index = notifications.findIndex(n => n.id === notificationId)
                if (index > -1) {
                    notifications.splice(index, 1)
                }*/
            })
            .listen('NotificationReadAll', () => {
                /*total = 0
                notifications = []*/
                console.log('NotificationReadAll')
            })
    }

    initDOMEvents();

    fetch();

    registerServiceWorker();

    handlePusherEvents();
});
