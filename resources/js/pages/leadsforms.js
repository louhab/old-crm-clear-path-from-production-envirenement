/*
 *  Document   : leadsforms.js
 *  Author     : RR
 *  Description: Custom JS code used in leadsforms page
 */
window.CLEARPATH_LEADFORM_ID = null;
class leadsFormsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm",
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>',
                },
            },
        });

        let columns = [
            { data: "id", name: "id", visible: false }, // Id
            //{"data": "user.name", "name": "name", title: 'Crée par'}, // name
            {
                data: "campaign.campaign_name",
                name: "campaign_name",
                title: "Campagne",
            }, // campaign_name
            { data: "active", name: "active", title: "active" }, // active
            {
                data: "actions",
                className: "text-center",
                title: "action",
                width: "150px",
                mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map((action) =>
                                    action.available
                                        ? `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                        : ""
                                )}
                            </div>`;
                },
            },
        ];

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#leadsforms-dt");
        let blockSelector = $("#leadsforms-dt-block");

        // Init histocalls DataTable
        return tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`,
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[0, "asc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/leadsforms`,
                    data: function (d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function (index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    },
                },
                columns: [
                    { data: "id", name: "id", visible: false }, // Id // name
                    /*{"data": "user.firstname",
                    "render": function (data, type, row) {
                        return row.user.name;
                    },
                    "name": "name", title: 'Crée par'
                },*/ {
                        data: "title",
                        name: "title",
                        title: "Titre",
                    }, // datedate
                    {
                        data: "campaign_name",
                        name: "campaign_name",
                        title: "Campagne",
                    }, // datedate
                    {
                        data: "active",
                        render: function (data, type, row) {
                            return `<div class="custom-control custom-switch custom-control-success mb-1">
                                                <input type="checkbox" class="custom-control-input activestate" id="example-sw-custom-success${
                                                    row.id
                                                }" data-id="${
                                row.id
                            }" name="example-sw-success${row.id}" ${
                                row.active ? 'checked=""' : ""
                            } >
                                                <label class="custom-control-label" for="example-sw-custom-success${
                                                    row.id
                                                }"></label>
                                            </div>`;
                        },
                        name: "active",
                        title: "Active",
                    }, // name
                    {
                        data: "actions",
                        className: "text-center",
                        title: "action",
                        width: "150px",
                        mRender: function (data, type, row) {
                            return `<div class="d-flex justify-content-center action">
                                ${data.map((action) =>
                                    action.available
                                        ? `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                        : ""
                                )}
                            </div>`;
                        },
                    },
                ],
                fnDrawCallback: function () {
                    blockSelector.removeClass("block-mode-loading");
                    $(".submit-search").prop("disabled", false);
                },
                columnDefs: [
                    {
                        /*targets: [2],
                orderable: false,
                searchable: false*/
                    },
                ],
            });
    }

    //DOM Events
    static initDOMEvents(dt) {
        //CHANGE ACTIVE STATE
        $("body").on("click", ".activestate", function (e) {
            e.preventDefault();

            let tableSelector = $("#leadsforms-dt");
            let leadsFormsAddBlockSelector = $("#block-leadsforms-add");

            const $row = $(this).closest("tr");

            const data = tableSelector.DataTable().row($row).data();

            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/leadsforms/setactivestate`,
                data: {
                    id: $(this).attr("data-id"),
                    state: $(this).is(":checked") ? 1 : 0,
                },
                success: () => {
                    Swal.fire(
                        "Modification",
                        "Etat changée avec succés!",
                        "success"
                    ).then(() => {
                        leadsFormsAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                        dt.api().ajax.reload();
                    });
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        leadsFormsAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Modification",
                            "erreur modification état!",
                            "error"
                        ).then(() => {
                            leadsFormsAddBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });
        $("#search-btn").on("click", function () {
            dt.api().ajax.reload();
        });
        $("#clear-search-btn").on("click", function () {
            $("#search-form").trigger("reset");
            $("#campaign-type").val(null).trigger("change");
            dt.api().ajax.reload();
        });
        $("#formsleads-table-add").on("click", function () {
            let leadsFormsAddFormSelector = $("#leadsforms-add-form");

            //hide previous validation errors
            leadsFormsAddFormSelector.find(".invalid-feedback").hide();
            leadsFormsAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            $("#leadsforms-add-form").trigger("reset");
            $("#modal-leadsforms-add").modal("show");
        });

        $(document).on("click", ".leadsforms-table-edit", function () {
            let leadsFormsEditFormSelector = $("#leadsforms-edit-form");
            $("#leadsforms-edit-form").trigger("reset");

            //get form id
            const $row = $(this).closest("tr");
            const data = $("#leadsforms-dt").DataTable().row($row).data();
            window.CLEARPATH_LEADFORM_ID = data.id;

            console.log("this is data >>", data);
            // old values
            $("#edit-form-title").attr("value", data.title);
            $("#edit-form-campaign option:selected").removeAttr("selected");
            $(
                '#edit-form-campaign option[value="' + data.campaign_id + '"]'
            ).attr("selected", "selected");

            // $('#edit-form-campaign').val(data.campaign_id).change();
            // $('#edit-form-campaign').prop('selectedIndex', data.campaign_id).change();
            if (data.redirect_to == 1) {
                $("#edit-form-redirect").prop("checked", true);
            } else {
                // $('#edit-form-redirect').prop('checked', false);
                $("#edit-form-redirect").removeAttr("checked");
            }

            if (data.lang == "en" || data.lang == "fr")
                $("#edit-form-lang-" + data.lang).prop("checked", true);

            if (data.is_students_form == 1) {
                $("#edit-form-is-for-student").prop("checked", true);
            } else if (data.is_students_form == 2)
                $("#edit-form-is-for-immigrant").prop("checked", true);
            else $("#edit-form-is-for-both").prop("checked", true);

            if (data.is_for_commercial !== 0) {
                $("#edit-form-is-for-the-commercial").prop("checked", true);
                $("#edit-form-commercial").show();
                $("#edit-form-commercial option:selected").removeAttr(
                    "selected"
                );
                $(
                    '#edit-form-commercial option[value="' +
                        data.commercial_id +
                        '"]'
                ).attr("selected", "selected");
            } else {
                $("#edit-form-commercial").hide();
            }

            if (data.is != null) {
                $("#edit-form-is-multiple-program").hide();
            } else {
                $("#edit-form-is-multiple-program").show();
                if (data.is_multi_program) {
                    $("#edit-form-is-multiple").prop("checked", true);
                } else {
                    $("#edit-form-is-not-multiple").prop("checked", true);
                }
            }

            // default programs
            // edit-form-student-program-id;
            if (data.is_students_form == 1) {
                $("#edit-form-student-program-id").show();
                if (data.student_program_id == 4)
                    $("#edit-form-canada-student").prop("checked", true);
                else if (data.student_program_id == 31)
                    $("#edit-form-uk-student").prop("checked", true);
                else {
                    $("#edit-form-es-student").prop("checked", true);
                }
            } else {
                $("#edit-form-student-program-id").hide();
            }

            //

            //hide previous validation errors
            leadsFormsEditFormSelector.find(".invalid-feedback").hide();
            leadsFormsEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            // $('#leadsforms-edit-form').trigger('reset');
            $("#modal-leadsforms-edit").modal("show");
        });

        $("#edit-leadsforms-btn").on("click", function () {
            let leadsFormsEditFormSelector = $("#leadsforms-edit-form");
            let leadsFormsEditBlockSelector = $("#block-leadsforms-edit");

            //hide previous validation errors
            leadsFormsEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            leadsFormsEditBlockSelector.addClass("block-mode-loading");

            //let formID =

            $.ajax({
                method: "put",
                url: `${process.env.MIX_API_URL}/leadsforms/${window.CLEARPATH_LEADFORM_ID}`,
                data: leadsFormsEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        "Edit!",
                        "Formulaire Edité avec succés!",
                        "success"
                    ).then(() => {
                        leadsFormsEditBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                        //$('#leadsforms-edit-form').trigger('reset');
                        dt.api().ajax.reload();
                    });
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        leadsFormsEditBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur ajout formulaire!",
                            "error"
                        ).then(() => {
                            leadsFormsEditBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        $("#add-leadsforms-btn").on("click", function () {
            let leadsFormsAddFormSelector = $("#leadsforms-add-form");
            let leadsFormsAddBlockSelector = $("#block-leadsforms-add");

            console.log("data  => ", leadsFormsAddFormSelector.serialize());

            //hide previous validation errors
            leadsFormsAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            leadsFormsAddBlockSelector.addClass("block-mode-loading");
            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/leadsforms`,
                data: leadsFormsAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        "Ajout!",
                        "Formulaire ajouté avec succés!",
                        "success"
                    ).then(() => {
                        leadsFormsAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                        $("#leadsforms-add-form").trigger("reset");
                        dt.api().ajax.reload();
                    });
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        leadsFormsAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur ajout formulaire!",
                            "error"
                        ).then(() => {
                            leadsFormsAddBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        //Copy lead form link
        $("body").on("click", ".leadsforms-table-copy", function () {
            let tableSelector = $("#leadsforms-dt");

            const $row = $(this).closest("tr");

            const data = tableSelector.DataTable().row($row).data();

            leadsFormsListPage.copyToClipboard(
                process.env.MIX_PUBLIC_URL + "/getintouch/" + data.token
            );
        });

        //SEARCH FILTER
        /*$('.submit-search').on('click', function () {
            dt.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            dt.api().ajax.reload();
        });

        //UPDATE FORM
        let histocallEditBlockSelector = $('#block-histocall-edit');
        let histocallEditFormSelector = $('#histocall-edit-form');

        $('body').on('click', '.histocall-table-edit', function() {

            //hide previous validation errors
            histocallEditFormSelector.find('.invalid-feedback').hide();
            histocallEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.histocallId = data.id;

            $('#edit-form-calldt').val(data.call_dt);
            $('#edit-form-duration').val(data.duration);
            $('#edit-form-calldescr').val(data.call_descr);

            if(data.customer_id == null){
                $('#edit-form-lead').val(data.lead_id);
                $('#modal-histocall-lead-edit').modal('show');
            }
            if(data.lead_id == null){
                $('#edit-form-customer').val(data.customer_id);
                $('#modal-histocall-customer-edit').modal('show');
            }


        });

        $('#update-histocall-btn').on('click', function () {

            //hide previous validation errors
            histocallEditFormSelector.find('.invalid-feedback').hide();
            histocallEditFormSelector.find('.form-control').removeClass('is-invalid');

            histocallEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/histocalls/${window.histocallId}`,
                data: histocallEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Etudiant modifié avec succés!',
                        'success'
                    ).then(() => {
                        histocallEditBlockSelector.removeClass('block-mode-loading');
                        dt.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        histocallEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification etudiant!',
                            'error'
                        ).then(() => {
                            histocallEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });
        */
        //DELETE FORM
        $("body").on("click", ".leadsforms-table-delete", function () {
            const $row = $(this).closest("tr");
            const data = $("#leadsforms-dt").DataTable().row($row).data();
            window.CLEARPATH_LEADFORM_ID = data.id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, supprimer!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteLeadForm();
                }
            });
        });

        function deleteLeadForm() {
            $("#leadsforms-dt-block").addClass("block-mode-loading");

            $.ajax({
                method: "delete",
                url: `${process.env.MIX_API_URL}/leadsforms/${window.CLEARPATH_LEADFORM_ID}`,
                success: () => {
                    Swal.fire(
                        "Suppression!",
                        "Lead form supprimé avec succés!",
                        "success"
                    ).then(() => {
                        dt.api().ajax.reload(null, false);
                    });
                },
                error: () => {
                    Swal.fire(
                        "Suppression!",
                        "erreur suppression form!",
                        "error"
                    ).then(() => {
                        $("#leadsforms-dt-block").removeClass(
                            "block-mode-loading"
                        );
                    });
                },
            });
        }

        $('input[name="add-form-is-for"]').on("change", function () {
            let selected = this.value;

            // default == 0
            // == 1
            // == 2

            // TODO add target if its not a multiple programs

            if (selected != "1" && selected != "2") {
                $("#add-form-student-program-id").hide();
                $("#add-form-is-multiple-program").show();
            } else if (selected == "1") {
                $("#add-form-student-program-id").show();
                $("#add-form-is-multiple-program").hide();
            } else {
                $("#add-form-student-program-id").hide();
                $("#add-form-is-multiple-program").hide();
            }
        });

        $('input[name="edit-form-is-for"]').on("change", function () {
            let selected = this.value;

            // default == 0
            // == 1
            // == 2

            if (selected != "1" && selected != "2") {
                $("#edit-form-student-program-id").hide();
                $("#edit-form-is-multiple-program").show();
            } else if (selected == "1") {
                $("#edit-form-student-program-id").show();
                $("#edit-form-is-multiple-program").hide();
            } else {
                $("#edit-form-student-program-id").hide();
                $("#edit-form-is-multiple-program").hide();
            }
        });

        $('input[name="add-form-is-for-commercial"]').on("change", function () {
            let selected = this.value;

            if (selected != "yes") {
                $("#add-form-commercial").hide();
            } else {
                $("#add-form-commercial").show();
            }
        });

        $('input[name="edit-form-is-for-commercial"]').on(
            "change",
            function () {
                let selected = this.value;

                if (selected != "yes") {
                    $("#edit-form-commercial").hide();
                } else {
                    $("#edit-form-commercial").show();
                }
            }
        );
    }

    static copyToClipboard(str) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(str).select();
        document.execCommand("copy");
        $temp.remove();
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents(this.initDataTables());
    }
}

// Initialize when page loads
jQuery(() => {
    leadsFormsListPage.init();
});
