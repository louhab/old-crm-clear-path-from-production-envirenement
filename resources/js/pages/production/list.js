/*
 *  Document   : list.js
 *  Author     : RR
 *  Description: Custom JS code used in production list page
 */
window.CLEARPATH_LEAD_ID = null;

const unserialize = function (serializedString) {
    var str = decodeURI(serializedString);
    var pairs = str.split("&");
    var obj = {},
        p,
        idx,
        val;
    for (var i = 0, n = pairs.length; i < n; i++) {
        p = pairs[i].split("=");
        idx = p[0];

        if (idx.indexOf("[]") == idx.length - 2) {
            var ind = idx.substring(0, idx.length - 2);
            if (obj[ind] === undefined) {
                obj[ind] = [];
            }
            obj[ind].push(p[1]);
        } else {
            obj[idx] = p[1];
        }
    }
    return obj;
};

class leadsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        const lang = $('input[name="lang"]').val();
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm",
        });

        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>',
                },
            },
        });

        $("#add-histocall-customer-btn-bis").on("click", function () {
            histocallCustomerAddFormSelector.find(".invalid-feedback").hide();
            histocallCustomerAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            histocallCustomerAddBlockSelector.addClass("block-mode-loading");
            let seconds =
                parseInt($(".seconds").text().slice(0, 2)) +
                parseInt($(".minutes").text().slice(0, 2)) * 60 +
                parseInt($(".hours").text().slice(0, 2)) * 3600;
            $("#add-form-duration").val(seconds);

            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/histocalls`,
                data: histocallCustomerAddFormSelector.serialize(),
                success: (res) => {
                    var date = new Date(res.data.created_at);
                    let formatted_date =
                        date.getFullYear() +
                        "-" +
                        (date.getMonth() + 1) +
                        "-" +
                        date.getDate() +
                        " " +
                        date.getHours() +
                        ":" +
                        date.getMinutes() +
                        ":" +
                        date.getSeconds();
                    $(
                        '<tr class="calls">' +
                            '<td class="d-none d-sm-table-cell">' +
                            '<em class="font-size-sm text-muted">' +
                            formatted_date +
                            "</em>" +
                            "</td>" +
                            '<td class="font-size-sm">' +
                            '<p class="text-muted mb-0">' +
                            res.data.call_descr +
                            "</p>" +
                            "</td>" +
                            '<td class="d-none d-sm-table-cell">' +
                            '<span class="badge badge-danger">' +
                            res.data.duration +
                            " seconds</span>" +
                            "</td>" +
                            "</tr>"
                    ).insertAfter($(".calls").last());
                    $("#modal-histocall-customer-add").modal("hide");
                    location.reload();
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        histocallCustomerAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur ajout client!",
                            "error"
                        ).then(() => {
                            histocallCustomerAddBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        AjaxGet = function (customerId) {
            let result = $.ajax({
                method: "get",
                url: `${process.env.MIX_API_URL}/leadsdata/${customerId}`,
                async: false,
                success: function (data) {
                    document
                        .getElementById("add-form-call_city")
                        .setAttribute("value", data.city || "");
                    document.getElementById("customer-phone").innerHTML =
                        data.phone;
                    return data;
                },
                error: (xhr) => {
                    console.log("error");
                },
            }).responseText;
            return result;
        };

        AjaxLeadsStatusGet = function (customerId) {
            let result = $.ajax({
                method: "get",
                url: `${process.env.MIX_API_URL}/getCallStates/${customerId}`,
                async: false,
                success: function (data) {
                    return data;
                },
            }).responseText;
            return result;
        };

        AjaxCallCountStatusGet = function (customerId) {
            let result = $.ajax({
                method: "get",
                url: `${process.env.MIX_API_URL}/histocalls/lead/${customerId}`,
                async: false,
                success: function (data) {
                    return data;
                },
            }).responseText;
            return result;
        };

        $("body").on(
            "click",
            "button[id^='histocall-customer-table-add-']",
            function () {
                // Safty check
                const reactHistoCall = $("#rcw-conversation-container")
                    .attr("class")
                    .split(" ")[1];

                if (reactHistoCall === "active") {
                    return;
                }

                let customerId = $(this).data("id");
                window.ajaxdata = AjaxGet(customerId);

                $("<input>")
                    .attr({
                        type: "hidden",
                        name: "add-form-lead_id",
                        value: customerId,
                    })
                    .appendTo("#histocall-add-customer-form");
                // show modal
                histocallCustomerAddFormSelector
                    .find(".invalid-feedback")
                    .hide();
                histocallCustomerAddFormSelector
                    .find(".form-control")
                    .removeClass("is-invalid");

                $("#histocall-add-customer-form").trigger("reset");
                // $("#lead_state_call").select2("destroy");
                $("#lead_state_call").val(null).trigger("change");
                $("#lead_state_call").select2();
                $("#lead_state_call").select2({
                    placeholder:
                        lang === "fr" ? "Lead status ..." : "Lead State ...",
                    width: "100%",
                    allowClear: true,
                    ajax: {
                        url: `${process.env.MIX_API_URL}/getCallStates/${customerId}`,
                        dataType: "json",
                        delay: 250,
                        method: "GET",
                        data: function (term, page) {
                            return {
                                q: term,
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (v, k) {
                                    return {
                                        text: v,
                                        id: k,
                                    };
                                }),
                            };
                        },
                        cache: true,
                    },
                });
                $("#modal-histocall-customer-add").modal("show");
            }
        );

        $("#lead_state_call").on("select2:select", function (e) {
            histocallCustomerAddFormSelector.find(".invalid-feedback").hide();
            histocallCustomerAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            histocallCustomerAddBlockSelector.addClass("block-mode-loading");
            let seconds =
                parseInt($(".seconds").text().slice(0, 2)) +
                parseInt($(".minutes").text().slice(0, 2)) * 60 +
                parseInt($(".hours").text().slice(0, 2)) * 3600;

            $("#add-form-duration").val(seconds);

            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/histocalls`,
                data: histocallCustomerAddFormSelector.serialize(),
                success: (res) => {
                    $("#modal-histocall-customer-add").modal("hide");

                    const userData = JSON.parse(ajaxdata);

                    // Get All leads status list
                    const leadStatus = AjaxLeadsStatusGet(userData.id);

                    const postData = unserialize(
                        histocallCustomerAddFormSelector.serialize()
                    );

                    // leads status
                    var badge = "danger";
                    switch (postData.lead_state_call) {
                        case "1":
                            badge = "success";
                            break;
                        case "3":
                            badge = "secondary";
                            break;
                        case "4":
                            badge = "info";
                            break;
                        case "5":
                            badge = "warning";
                            break;
                    }

                    if (
                        $("#lead_state_call").val() != "Rappel" &&
                        $("#lead_state_call").val() != "No_Chnage"
                    ) {
                        // parce thise string
                        const bagdeEl = $(`#lead-badge-${userData.id}`);

                        bagdeEl.text(
                            bagdeEl
                                .text()
                                .replace(
                                    bagdeEl.text(),
                                    JSON.parse(leadStatus)[
                                        postData.lead_state_call
                                    ]
                                )
                        );
                        bagdeEl.removeClass(
                            bagdeEl.attr("class").split(" ")[1]
                        );
                        bagdeEl.addClass(`badge-${badge}`);
                    }

                    if ($("#lead_state_call").val() == "Rappel") {
                        window.location.href = "/calendar";
                    }
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        histocallCustomerAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur ajout client!",
                            "error"
                        ).then(() => {
                            histocallCustomerAddBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        let columns = [
            {
                data: "id",
                name: "id",
                title: "",
                visible: false,
                className: "select-checkbox",
                mRender: function (data, type, row) {
                    return "";
                },
            },
            {
                data: "actions",
                className: "text-center",
                title: "action",
                width: "150px",
                mRender: function ([action], type, row) {
                    return `
                    <div class="d-flex justify-content-center action">
                        <div class="btn-group" style="margin:5px">
                            <a class="${action.className}" href="${process.env.MIX_PUBLIC_URL}/production/${row.id}" title="${action.title}" class="${action.className}" data-id="${row.id}">
                                <i class="${action.icon}"></i>
                            </a>
                        </div>
                    </div>`;
                },
            },
            {
                data: "name",
                name: "name",
                title: "Nom complet",
            },
            {
                data: "total_amouts",
                name: "total_amouts",
                title: "Total (montant)",
                className: "font-w600 font-size-sm",
            }, // lastname
            // {
            //     data: "iso",
            //     name: "iso",
            //     title: "Devise",
            // },
            {
                data: "total_calls",
                name: "total_calls",
                title: lang === "fr" ? "Total appels" : "Total calls",
                className: "font-w600 font-size-sm",
                mRender: function (data, type, row) {
                    return row.total_calls ? row.total_calls : "0";
                },
            }, // calls
            {
                data: "total_RDV",
                name: "total_RDV",
                title: lang === "fr" ? "Total RDVs" : "Total RDVs",
                className: "font-w600 font-size-sm",
                mRender: function (data, type, row) {
                    return row.total_RDV ? row.total_RDV : "0";
                },
            },
            {
                data: "created_at",
                name: "created_at",
                title: lang === "fr" ? "Crée le" : "Created at",
                mRender: function (data, type, row) {
                    if (data) {
                        var date = new Date(data);
                        return (
                            (date.getDate() > 9
                                ? date.getDate()
                                : "0" + date.getDate()) +
                            "/" +
                            (date.getMonth() > 8
                                ? date.getMonth() + 1
                                : "0" + (date.getMonth() + 1)) +
                            "/" +
                            date.getFullYear()
                        );
                    }
                    return '<span class="badge badge-info">n/d</span>';
                },
            }, // Date de Création
        ];

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#conseillers-dt");
        let blockSelector = $("#conseillers-dt-block");

        // Init leads DataTable
        return tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.${lang}.l10n.json`,
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[0, "desc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/production`,
                    data: function (d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function (index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    },
                },
                columns: columns,
                fnDrawCallback: function () {
                    blockSelector.removeClass("block-mode-loading");
                    $(".dt_action_field").select2({
                        placeholder: "Utilisateurs...",
                        allowClear: true,
                        ajax: {
                            url: `${process.env.MIX_API_URL}/agents`,
                            dataType: "json",
                            delay: 250,
                            method: "POST",
                            data: function (term, page) {
                                return {
                                    q: term,
                                };
                            },
                            processResults: function (data) {
                                let res = [];
                                $.each(data, function (role, group) {
                                    if (role == "Agent") return true;
                                    let groupObj = {
                                        text: role,
                                    };
                                    let children = [];
                                    $.each(group, function (idx, user) {
                                        // if(user.name)
                                        let child = {
                                            text: user.name,
                                            id: user.id,
                                            group: role,
                                        };
                                        children.push(child);
                                    });
                                    groupObj["children"] = children;
                                    res.push(groupObj);
                                });
                                return {
                                    results: res,
                                };
                            },
                            initSelection: function (element, callback) {},
                            cache: true,
                        },
                    });
                    $("#dt_action_btn").on("click", function () {
                        let conseillers = $(".dt_action_field").select2("val");
                        var $dataSelected = $("#conseillers-dt")
                            .DataTable()
                            .rows(".selected")
                            .data();
                        if (
                            conseillers.length > 0 &&
                            $dataSelected.length > 0
                        ) {
                            var resultConseilArray =
                                $(".dt_action_field").select2("val");
                            var cs = 0;
                            Swal.fire({
                                title: "Etes vous sur?",
                                text: "",
                                icon: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#3085d6",
                                cancelButtonColor: "#d33",
                                confirmButtonText: "Oui, Affecter!",
                                cancelButtonText: "Annuler",
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    $("#conseillers-dt-block").addClass(
                                        "block-mode-loading"
                                    );
                                    var leads_ids = $("#conseillers-dt")
                                        .DataTable()
                                        .rows(".selected")
                                        .data()
                                        .pluck("lead_id")
                                        .toArray();
                                    $.ajax({
                                        method: "put",
                                        url: `${process.env.MIX_API_URL}/leadConseiller`,
                                        data: {
                                            leads_ids: leads_ids,
                                            resultConseilArray:
                                                resultConseilArray,
                                        },
                                        success: () => {
                                            Swal.fire(
                                                "Modification!",
                                                "Leads affecté",
                                                "success"
                                            ).then(() => {
                                                $("#conseillers-dt")
                                                    .DataTable()
                                                    .ajax.reload(null, false);
                                                $(
                                                    "#MyTableCheckAllButton"
                                                ).prop("checked", false);
                                            });
                                        },
                                        error: () => {
                                            $(
                                                "#conseillers-dt-block"
                                            ).removeClass("block-mode-loading");
                                        },
                                    });
                                }
                            });
                        } else if ($dataSelected.length == 0) {
                            Swal.fire(
                                "Modification!",
                                "Veuiller Selectinner un lead!",
                                "warning"
                            );
                        } else {
                            Swal.fire(
                                "Modification!",
                                "Veuiller Choisir un conseiller!",
                                "warning"
                            );
                        }
                    });
                    $(".submit-search").prop("disabled", false);
                },
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                        orderable: true,
                        searchable: false,
                        className: "select-checkbox",
                    },
                    {
                        targets: 0,
                        orderable: false,
                    },
                ],
                select: {
                    style: "multi",
                    selector: "td:first-child",
                },
                lengthMenu: [
                    [10, 25, 50, 100, 500, -1],
                    [10, 25, 50, 100, 500, "All"],
                ],
                dom: '<"toolbar">lpfrtip',
            });
    }

    static initDOMEvents(dt) {
        $("#search-btn").on("click", function () {
            const advisors = $('[name="conseiller"]').select2().val();
            const program = $('[name="program"]').select2().val();
            const datePicker = $('[id="daterange"]').val();
            Livewire.emit("filterChanged", advisors, datePicker, program);
            dt.api().ajax.reload();
            $("#MyTableCheckAllButton").prop("checked", false);
        });

        $("#clear-search-btn").on("click", function () {
            $("#search-form").trigger("reset");
            $("#lead").val(null).trigger("change");
            $("#phone").val(null);
            $("#country").val(null).trigger("change");
            dt.api().ajax.reload();
            $("#MyTableCheckAllButton").prop("checked", false);
        });

        $("body").on("change", "#MyTableCheckAllButton", function () {
            var all = $("#conseillers-dt")
                .DataTable()
                .rows({ search: "applied" })
                .count(); // get total count of rows
            var selectedRows = $("#conseillers-dt")
                .DataTable()
                .rows({ selected: true, search: "applied" })
                .count(); // get total count of selected rows

            if (selectedRows < all) {
                $("#MyTableCheckAllButton").prop("checked", true);
                //Added search applied in case user wants the search items will be selected
                $("#conseillers-dt")
                    .DataTable()
                    .rows({ search: "applied" })
                    .deselect();
                $("#conseillers-dt")
                    .DataTable()
                    .rows({ search: "applied" })
                    .select();
            } else {
                $("#conseillers-dt")
                    .DataTable()
                    .rows({ search: "applied" })
                    .deselect();
            }
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents(this.initDataTables());
    }
}

// Initialize when page loads
jQuery(() => {
    leadsListPage.init();
});
