/*
 *  Document   : mycase.js
 *  Author     : RR
 *  Description: Custom JS code used in my case page
 */

class myCasePage {
    /*
     * remove empty wrappers
     *
     */
    // static empty() {
    //     $('#info-table > tbody  > tr').each(function() {
    //         $(this).find('td').each (function() {
    //             let $documentWrapper = $(this).find('.document-wrapper .document-in')
    //             if(!$documentWrapper.length) $(this).closest("tr").remove()
    //         })
    //     })

    //     let $tbody = $("#info-table tbody")

    //     if ($tbody.children().length === 0) {
    //         $tbody.parent().remove()
    //     }
    // }

    /*
     * Init DOM Events
     *
     */
    static initDOMEvents() {
        const lang = $('input[name="lang"]').val();
        //DOM Events
        $(".delete-document").on("click", function(event) {
            event.preventDefault();
            $(`#${$(this).attr("data-formId")}`).submit();
        });

        $(".sign-document").on("click", function(event) {
            event.preventDefault();
            $(`#${$(this).attr("data-formId")}`).submit();
        });

        $(".custom-file-input").on("change", function() {
            let fileName = $(this)
                .val()
                .split("\\")
                .pop();
            $(this)
                .parent()
                .find("label")
                .addClass("selected")
                .html(fileName);
        });
        $("#nav-block li a").on("click", function() {
            if (!$(this).hasClass("active")) {
                // $("#nav-block-content").children().hide();
                $("#" + $("#nav-block li a.active").data("toggle")).hide();
                $("#nav-block li a.active").removeClass("active");
                $("#" + $(this).data("toggle")).show();
                $(this).addClass("active");
                // console.log($active.text);
            }
        });
        // Devis
        $("body").on("click", "#enable-quote", function() {
            let url = $(this).data("url");
            console.log("URL: " + url);
            Swal.fire({
                title: lang === "fr" ? "Etes vous sur ?" : "Are you sure ?",
                text:
                    lang === "fr"
                        ? "L'opération est irréversible!"
                        : "The operation is irreversible",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText:
                    lang === "fr" ? "Oui, accepter!" : "Yes, accept",
                cancelButtonText: lang === "fr" ? "Annuler" : "Cancel"
            }).then(result => {
                if (result.isConfirmed) {
                    $.ajax({
                        method: "get",
                        url: url,
                        success: () => {
                            Swal.fire(
                                lang === "fr" ? "Acceptation!" : "Acceptance!",
                                lang === "fr"
                                    ? "Devis Accepté!"
                                    : "Quote Accepted",
                                "success"
                            ).then(() => {
                                location.reload();
                            });
                        },
                        error: () => {
                            Swal.fire(
                                lang === "fr" ? "Acceptation!" : "Acceptance!",
                                lang === "fr"
                                    ? "erreur acceptation devis!"
                                    : "error occurred",
                                "error"
                            ).then(() => {
                                ///
                            });
                        }
                    });
                }
            });
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        // this.empty()
        this.initDOMEvents();
    }
}

// Initialize when page loads
jQuery(() => {
    myCasePage.init();
});
