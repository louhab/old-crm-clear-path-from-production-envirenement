/*
 *  Document   : services.js
 *  Author     : RR feat NTX
 *  Description: Custom JS code used in services page
 */

class servicesListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "name", "name": "name", title: 'nom'}, // nom
            {"data": "single_price", "name": "single_price", title: 'Single price',mRender: function(data, type, row) {
                var result = '<p>';
                Object.entries(data).forEach(entry => {
                    const [key, value] = entry;
                    result += value + ' ' + key + '<br>';
                    //console.log(key, value);
                });
                result += '</p>';
                // Object.entries(data)
                return result;
            }}, //
            {"data": "couple_price", "name": "couple_price", title: 'Couple price',mRender: function(data, type, row) {
                var result = '<p>';
                Object.entries(data).forEach(entry => {
                    const [key, value] = entry;
                    if (value != null)
                        result += value + ' ' + key + '<br>';
                    else {
                        result = null;
                        return false;
                    }
                    //console.log(key, value);
                });
                if (result == null)
                    return `<span class="badge badge-info">n/d</span>`;
                result += '</p>';
                // console.log(result);
                // Object.entries(data)
                return result;
            }}, //
            {"data": "phase.name", "name": "phase.name", title: 'phase'}, // program
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                        (action.available ?
                                `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                :
                                ''
                        )
                    )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#services-dt');
        let blockSelector = $('#services-dt-block');

        // Init services DataTable
        let servicesTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-s-earch').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/services`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events
        //TODO put dom events here

        //SEARCH FILTER
        $('.submit-search').on('click', function () {
            servicesTable.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            servicesTable.api().ajax.reload();
        });

        //ADD FORM
        let serviceAddBlockSelector = $('#block-service-add');
        let serviceAddFormSelector = $('#service-add-form');

        $('#service-table-add').on('click', function () {

            //hide previous validation errors
            serviceAddFormSelector.find('.invalid-feedback').hide();
            serviceAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#service-add-form').trigger('reset');
            $('#modal-service-add').modal('show');
        });

        $('#add-service-btn').on('click', function () {

            //hide previous validation errors
            serviceAddFormSelector.find('.invalid-feedback').hide();
            serviceAddFormSelector.find('.form-control').removeClass('is-invalid');

            serviceAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/services`,
                data: serviceAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'Service ajouté avec succés!',
                        'success'
                    ).then(() => {
                        serviceAddBlockSelector.removeClass('block-mode-loading');
                        $('#service-add-form').trigger('reset');
                        servicesTable.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        serviceAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'Erreur ajout service!',
                            'error'
                        ).then(() => {
                            serviceAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //UPDATE FORM
        let serviceEditBlockSelector = $('#block-service-edit');
        let serviceEditFormSelector = $('#service-edit-form');

        $('body').on('click', '.service-table-edit', function() {

            //hide previous validation errors
            serviceEditFormSelector.find('.invalid-feedback').hide();
            serviceEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.serviceId = data.id;
            // console.log(data.phase);

            //set service edit form data
            $('#edit-form-phase_id').val(data.phase_id);
            $('#edit-form-name').val(data.name);
            $('#edit-form-single_price-mad').val(data.single_price.MAD);
            $('#edit-form-single_price-eur').val(data.single_price.EUR);
            $('#edit-form-single_price-usd').val(data.single_price.USD);
            if (data.phase.program_id == 2) {
                $('#edit-form-couple_price-mad').closest(".form-row").show();
                $('#edit-form-couple_price-mad').val(data.couple_price.MAD);
                $('#edit-form-couple_price-eur').val(data.couple_price.EUR);
                $('#edit-form-couple_price-usd').val(data.couple_price.USD);
            } else {
                $('#edit-form-couple_price-mad').closest(".form-row").hide();
            }

            $('#modal-service-edit').modal('show');
        });

        $('#update-service-btn').on('click', function () {

            //hide previous validation errors
            serviceEditFormSelector.find('.invalid-feedback').hide();
            serviceEditFormSelector.find('.form-control').removeClass('is-invalid');

            serviceEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/services/${window.serviceId}`,
                data: serviceEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Service modifié avec succés!',
                        'success'
                    ).then(() => {
                        serviceEditBlockSelector.removeClass('block-mode-loading');
                        servicesTable.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        serviceEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification etudiant!',
                            'error'
                        ).then(() => {
                            serviceEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //DELETE FORM
        $('body').on('click', '.service-table-delete', function() {

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.serviceId = data.id;

            Swal.fire({
                title: 'Etes vous sur?',
                text: "L'opération est irréversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteUser();
                }
            })
        });

        function deleteUser() {

            blockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'delete',
                url: `${process.env.MIX_API_URL}/services/${window.serviceId}`,
                success: () => {
                    Swal.fire(
                        'Suppression!',
                        'utilisateur supprimé avec succés!',
                        'success'
                    ).then(() => {
                        servicesTable.api().ajax.reload(null, false);
                    })
                },
                error: () => {
                    Swal.fire(
                        'Suppression!',
                        'erreur suppression utilisateur!',
                        'error'
                    ).then(() => {
                        blockSelector.removeClass('block-mode-loading');
                    })
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { servicesListPage.init(); });
