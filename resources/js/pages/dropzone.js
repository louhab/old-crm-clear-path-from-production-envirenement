import Dropzone from "dropzone";

Dropzone.discover = false;

let myDropzone = new Dropzone(".dropzone", {paramName: "file-input-media"});

// myDropzone.on("addedfile", file => {
//     console.log("A file has been added");
// });
myDropzone.on("processing", file => {
    myDropzone.options.url = $(".dropzone").attr("action");
    // console.log("processing! " + myDropzone.options.url);
});
myDropzone.on("success", (file, response) => {
    Livewire.emit('mediaAdded');
    // console.log("dropzone upload success");
});
