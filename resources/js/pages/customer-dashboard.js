/*
 *  Document   : customer-dashboard.js
 *  Author     : RR feat NTX
 *  Description: Custom JS code used in customer dashboard page
 */

class customerDashboardPage {

    /*
     * Set Up DOM Events Handlers
     *
     */
    static setUpDOMEvents() {
        //
        console.log('setUpDOMEvents');

        $('#enable-push-btn').on('click', function () {
            initSW();
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.setUpDOMEvents();
    }
}

// Initialize when page loads
jQuery(() => { customerDashboardPage.init(); });
