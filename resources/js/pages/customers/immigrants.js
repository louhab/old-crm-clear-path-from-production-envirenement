/*
 *  Document   : immigrants.js
 *  Author     : RR
 *  Description: Custom JS code used in immigrants page
 */

class immigrantsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "firstname", "name": "firstname", title: 'Prénom'}, // firstname
            {"data": "lastname", "name": "lastname", title: 'Nom'}, // lastname
            {"data": "email", "name": "email", title: 'Email'}, // email
            {"data": "country.name_fr_fr", "name": "country", title: 'Pays'}, // country
            {"data": "ranking.ranking_label", "name": "ranking_id", title: 'Ranking', mRender: function (data, type, row) {
                    switch (row.ranking_id) {
                        case 1 : return `<span class="badge badge-success">${data}</span>`;
                        case 2 : return `<span class="badge badge-warning">${data}</span>`;
                        case 3 : return `<span class="badge badge-danger">${data}</span>`;
                    }
                    return 'n/d';
                }
            }, // ranking
            {"data": "birthday", "name": "birthday", title: 'Date de naissance'}, // birthday
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                        (action.available ?
                                `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                :
                                ''
                        )
                    )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#immigrants-dt');
        let blockSelector = $('#immigrants-dt-block');

        // Init immigrants DataTable
        let immigrantsTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-search').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/customers/immigrants`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events
        //TODO put dom events here

        //SEARCH FILTER
        $('.submit-search').on('click', function () {
            immigrantsTable.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            immigrantsTable.api().ajax.reload();
        });

        //ADD FORM
        let immigrantAddBlockSelector = $('#block-immigrant-add');
        let immigrantAddFormSelector = $('#immigrant-add-form');

        $('#immigrant-table-add').on('click', function () {

            //hide previous validation errors
            immigrantAddFormSelector.find('.invalid-feedback').hide();
            immigrantAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#immigrant-add-form').trigger('reset');
            $('#modal-immigrant-add').modal('show');
        });

        $('#add-immigrant-btn').on('click', function () {

            //hide previous validation errors
            immigrantAddFormSelector.find('.invalid-feedback').hide();
            immigrantAddFormSelector.find('.form-control').removeClass('is-invalid');

            immigrantAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/customers/immigrants`,
                data: immigrantAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'Immigrant ajouté avec succés!',
                        'success'
                    ).then(() => {
                        immigrantAddBlockSelector.removeClass('block-mode-loading');
                        $('#immigrant-add-form').trigger('reset');
                        immigrantsTable.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        immigrantAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout immigrant!',
                            'error'
                        ).then(() => {
                            immigrantAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //UPDATE FORM
        let immigrantEditBlockSelector = $('#block-immigrant-edit');
        let immigrantEditFormSelector = $('#immigrant-edit-form');

        $('body').on('click', '.immigrant-table-edit', function() {

            //hide previous validation errors
            immigrantEditFormSelector.find('.invalid-feedback').hide();
            immigrantEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.immigrantId = data.id;

            //set immigrant edit form data
            $('#edit-form-firstname').val(data.firstname);
            $('#edit-form-lastname').val(data.lastname);
            $('#edit-form-email').val(data.email);
            $('#edit-form-birthday').val(data.birthday);
            $('#edit-form-languagelevel').val(data.ranking.id);
            $('#edit-form-country').val(data.country.name_fr_fr);
            $('#edit-form-adresse1').val(data.adresse_line_1);
            $('#edit-form-adresse2').val(data.adresse_line_2);
            $('#edit-form-postalcode').val(data.postal_code);
            $('#edit-form-experience').val(data.professional_experience);

            $('#modal-immigrant-edit').modal('show');
        });

        $('#update-immigrant-btn').on('click', function () {

            //hide previous validation errors
            immigrantEditFormSelector.find('.invalid-feedback').hide();
            immigrantEditFormSelector.find('.form-control').removeClass('is-invalid');

            immigrantEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/customers/immigrants/${window.immigrantId}`,
                data: immigrantEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Immigrant modifié avec succés!',
                        'success'
                    ).then(() => {
                        immigrantEditBlockSelector.removeClass('block-mode-loading');
                        immigrantsTable.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        immigrantEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification immigrant!',
                            'error'
                        ).then(() => {
                            immigrantEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //DELETE FORM
        $('body').on('click', '.immigrant-table-delete', function() {

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.immigrantId = data.id;

            Swal.fire({
                title: 'Etes vous sur?',
                text: "L'opération est irréversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteImmigrant();
                }
            })
        });

        function deleteImmigrant() {

            blockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'delete',
                url: `${process.env.MIX_API_URL}/customers/immigrants/${window.immigrantId}`,
                success: () => {
                    Swal.fire(
                        'Suppression!',
                        'immigrant supprimé avec succés!',
                        'success'
                    ).then(() => {
                        immigrantsTable.api().ajax.reload(null, false);
                    })
                },
                error: () => {
                    Swal.fire(
                        'Suppression!',
                        'erreur suppression immigrant!',
                        'error'
                    ).then(() => {
                        blockSelector.removeClass('block-mode-loading');
                    })
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { immigrantsListPage.init(); });
