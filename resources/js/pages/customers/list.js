/*
 *  Document   : list.js
 *  Author     : RR
 *  Description: Custom JS code used in customers list page
 */

class customersListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info:
                    "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            { data: "id", name: "id", visible: false }, // Id
            {
                data: "firstname",
                name: "firstname",
                title: "Prénom",
                className: "font-w600 font-size-sm"
            }, // firstname
            {
                data: "lastname",
                name: "lastname",
                title: "Nom",
                className: "font-w600 font-size-sm"
            }, // lastname
            {
                data: "email",
                name: "email",
                title: "Email",
                mRender: function(data, type, row) {
                    if (data)
                        return `${
                            data?.match(/^([^@]*)@/)[1]
                        }<em class="text-muted">@${data.split("@")[1]}</em>`;
                    else return `<em class="text-muted">null</em>`;
                }
            }, // email
            {
                data: "program",
                name: "program.name",
                title: "Programme",
                mRender: function(data, type, row) {
                    return `<span class="badge badge-${
                        data.id == 1 ? "info" : "success"
                    }">${data.name}</span>`;
                }
            }, // program
            {
                data: "case",
                name: "case.id",
                title: "Etape dossier",
                mRender: function(data, type, row) {
                    return data && data.step
                        ? `<a href="cases/${data.id}" target="_blank">${data.step.step_label}</a>`
                        : "n/d";
                }
            }, // case step
            {
                data: "country",
                name: "country",
                title: "Pays",
                mRender: function(data, type, row) {
                    return data
                        ? data.name_fr_fr
                        : '<span class="badge badge-danger">n/d</span>';
                }
            }, // country
            {
                data: "cv_id",
                name: "cv_id",
                title: "CV",
                mRender: function(data, type, row) {
                    return data
                        ? `<a class="btn btn-alt-info" href="${process.env.MIX_PUBLIC_URL}/medias/${data}"><i class="fas fa-download"></i></a>`
                        : '<span class="badge badge-danger">n/d</span>';
                }
            }, // cv
            {
                data: "ranking.ranking_label",
                name: "ranking_id",
                title: "Ranking",
                mRender: function(data, type, row) {
                    switch (row.ranking_id) {
                        case 1:
                            return `<span class="badge badge-success">${data}</span>`;
                        case 2:
                            return `<span class="badge badge-warning">${data}</span>`;
                        case 3:
                            return `<span class="badge badge-danger">${data}</span>`;
                    }
                    return '<span class="badge badge-danger">n/d</span>';
                }
            }, // ranking
            {
                data: "birthday",
                name: "birthday",
                title: "Date de naissance",
                mRender: function(data, type, row) {
                    return `<em class="text-muted">${data}</em>`;
                }
            }, // birthday
            {
                data: "actions",
                className: "text-center",
                title: "action",
                width: "150px",
                mRender: function(data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                                    action.available
                                        ? `<div class="btn-group" style="margin:5px">
                                    <a href="${
                                        action.islink
                                            ? `${process.env.MIX_PUBLIC_URL}/customers/${row.id}`
                                            : "javascript:void(0)"
                                    }" title="${action.title}" class="${
                                              action.className
                                          }" data-id="${row.id}"><i class="${
                                              action.icon
                                          }"></i></a>
                                </div>`
                                        : ""
                                )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#customers-dt");
        let blockSelector = $("#customers-dt-block");

        // Init customers DataTable
        return tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[0, "desc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/customers`,
                    data: function(d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function(index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    }
                },
                columns: columns,
                fnDrawCallback: function() {
                    blockSelector.removeClass("block-mode-loading");
                    $(".submit-search").prop("disabled", false);
                },
                columnDefs: [
                    {
                        /*targets: [2],
                orderable: false,
                searchable: false*/
                    }
                ]
            });
    }

    static initDOMEvents(dt) {
        console.log("customersListPage initDOMEvents");
    }

    static deleteLead(bs, dt) {
        bs.addClass("block-mode-loading");

        $.ajax({
            method: "delete",
            url: `${process.env.MIX_API_URL}/customers/${window.customerId}`,
            success: () => {
                Swal.fire(
                    "Suppression!",
                    "utilisateur supprimé avec succés!",
                    "success"
                ).then(() => {
                    dt.api().ajax.reload(null, false);
                });
            },
            error: () => {
                Swal.fire(
                    "Suppression!",
                    "erreur suppression utilisateur!",
                    "error"
                ).then(() => {
                    bs.removeClass("block-mode-loading");
                });
            }
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents(this.initDataTables());
    }
}

// Initialize when page loads
jQuery(() => {
    customersListPage.init();
});
