/*
 *  Document   : edit.js
 *  Author     : RR
 *  Description: Custom JS code used in customer edit page
 */

class customerEditPage {

    static initDOMEvents(dt) {

        $('#delete-cv-href').on('click', function(event) {
            event.preventDefault();
            console.log(document.getElementById('delete-form'))
            $('#delete-form').submit();
        })

        $('.custom-file-input').on('change', function() {
            let fileName = $(this).val().split('\\').pop();
            $('#label-cv-input').addClass("selected").html(fileName);
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents();
    }
}

// Initialize when page loads
jQuery(() => { customerEditPage.init(); });
