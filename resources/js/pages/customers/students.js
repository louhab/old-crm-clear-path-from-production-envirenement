/*
 *  Document   : students.js
 *  Author     : RR
 *  Description: Custom JS code used in students page
 */

class studentsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "firstname", "name": "firstname", title: 'Prénom'}, // firstname
            {"data": "lastname", "name": "lastname", title: 'Nom'}, // lastname
            {"data": "email", "name": "email", title: 'Email'}, // email
            {"data": "country.name_fr_fr", "name": "country", title: 'Pays'}, // country
            {"data": "ranking.ranking_label", "name": "ranking_id", title: 'Ranking', mRender: function (data, type, row) {
                    switch (row.ranking_id) {
                        case 1 : return `<span class="badge badge-success">${data}</span>`;
                        case 2 : return `<span class="badge badge-warning">${data}</span>`;
                        case 3 : return `<span class="badge badge-danger">${data}</span>`;
                    }
                    return 'n/d';
                }
            }, // ranking
            {"data": "birthday", "name": "birthday", title: 'Date de naissance'}, // birthday
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                        (action.available ?
                                `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                :
                                ''
                        )
                    )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#students-dt');
        let blockSelector = $('#students-dt-block');

        // Init students DataTable
        let studentsTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-search').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/customers/students`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events
        //TODO put dom events here

        //SEARCH FILTER
        $('.submit-search').on('click', function () {
            studentsTable.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            studentsTable.api().ajax.reload();
        });

        //ADD FORM
        let studentAddBlockSelector = $('#block-student-add');
        let studentAddFormSelector = $('#student-add-form');

        $('#student-table-add').on('click', function () {

            //hide previous validation errors
            studentAddFormSelector.find('.invalid-feedback').hide();
            studentAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#student-add-form').trigger('reset');
            $('#modal-student-add').modal('show');
        });

        $('#add-student-btn').on('click', function () {

            //hide previous validation errors
            studentAddFormSelector.find('.invalid-feedback').hide();
            studentAddFormSelector.find('.form-control').removeClass('is-invalid');

            studentAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/students`,
                data: studentAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'utilisateur ajouté avec succés!',
                        'success'
                    ).then(() => {
                        studentAddBlockSelector.removeClass('block-mode-loading');
                        $('#student-add-form').trigger('reset');
                        studentsTable.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        studentAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout utilisateur!',
                            'error'
                        ).then(() => {
                            studentAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //UPDATE FORM
        let studentEditBlockSelector = $('#block-student-edit');
        let studentEditFormSelector = $('#student-edit-form');

        $('body').on('click', '.student-table-edit', function() {

            //hide previous validation errors
            studentEditFormSelector.find('.invalid-feedback').hide();
            studentEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.studentId = data.id;

            //set student edit form data
            $('#edit-form-firstname').val(data.firstname);
            $('#edit-form-lastname').val(data.lastname);
            $('#edit-form-email').val(data.email);
            $('#edit-form-birthday').val(data.birthday);
            $('#edit-form-languagelevel').val(data.ranking.id);
            $('#edit-form-country').val(data.country.name_fr_fr);
            $('#edit-form-adresse1').val(data.adresse_line_1);
            $('#edit-form-adresse2').val(data.adresse_line_2);
            $('#edit-form-postalcode').val(data.postal_code);
            $('#edit-form-experience').val(data.professional_experience);

            $('#modal-student-edit').modal('show');
        });

        $('#update-student-btn').on('click', function () {

            //hide previous validation errors
            studentEditFormSelector.find('.invalid-feedback').hide();
            studentEditFormSelector.find('.form-control').removeClass('is-invalid');

            studentEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/students/${window.studentId}`,
                data: studentEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Etudiant modifié avec succés!',
                        'success'
                    ).then(() => {
                        studentEditBlockSelector.removeClass('block-mode-loading');
                        studentsTable.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        studentEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification etudiant!',
                            'error'
                        ).then(() => {
                            studentEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //DELETE FORM
        $('body').on('click', '.student-table-delete', function() {

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.studentId = data.id;

            Swal.fire({
                title: 'Etes vous sur?',
                text: "L'opération est irréversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteUser();
                }
            })
        });

        function deleteUser() {

            blockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'delete',
                url: `${process.env.MIX_API_URL}/students/${window.studentId}`,
                success: () => {
                    Swal.fire(
                        'Suppression!',
                        'utilisateur supprimé avec succés!',
                        'success'
                    ).then(() => {
                        studentsTable.api().ajax.reload(null, false);
                    })
                },
                error: () => {
                    Swal.fire(
                        'Suppression!',
                        'erreur suppression utilisateur!',
                        'error'
                    ).then(() => {
                        blockSelector.removeClass('block-mode-loading');
                    })
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { studentsListPage.init(); });
