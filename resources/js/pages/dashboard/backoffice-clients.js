/*
 *  Document   : dashboard.js
 *  Author     : RR feat NTX
 *  Description: Custom JS code used in dashboard page
 */

class dashboardPage {

    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        })

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        })

        let columns = [
            {"data": "user_id", "name": "user_id", "visible": false}, // user_id
            {"data": "name", "name": "name", title: 'Conseiller', className: 'font-w600 font-size-sm'}, // name
            {"data": "cases", "name": "cases", title: 'Created Cases', className: 'font-w600 font-size-sm'} // count cases
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#agents-dt');
        let blockSelector = $('#agents-dt-block');

        // Init agents DataTable
        return tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading')
            $('.submit-search').prop('disabled', true)
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'desc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/agents`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item)
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val()
                        }
                    })
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading')
                $('.submit-search').prop('disabled', false)
            },
            columnDefs: [{
                targets: [2],
                orderable: false,
                searchable: false
            }],
        })
    }
    /*
     * Set Up DOM Events Handlers
     *
     */
    static initDashboardAnalytics() {
        // analytics
        let searchFilterSelector     = $('.search-filter');
        let callsBlock               = $(".dashboard-analytics-calls").closest(".block.block-rounded");
        let durationsBlock           = $(".dashboard-analytics-durations").closest(".block.block-rounded");
        let messagesBlock            = $(".dashboard-analytics-messages").closest(".block.block-rounded");
        let quotesBlock              = $(".dashboard-analytics-quotes").closest(".block.block-rounded");

        /****************************************/
        // loading class
        callsBlock.addClass("block-mode-loading");
        durationsBlock.addClass("block-mode-loading");
        messagesBlock.addClass("block-mode-loading");
        quotesBlock.addClass("block-mode-loading");

        $('dt[class*="dashboard-analytics-campaign_"]').each(function(){
            $(this).closest(".block.block-rounded").addClass("block-mode-loading");
        });
        // payments
        $('dt[class*="dashboard-analytics-payment_"]').each(function(){
            $(this).closest(".block.block-rounded").addClass("block-mode-loading");
        });
        // steps
        $('dt[class*="dashboard-analytics-step_"]').each(function(){
            $(this).closest(".block.block-rounded").addClass("block-mode-loading");
        });
        /****************************************/
        //console.log(searchFilterSelector.find("form").serialize());
        $.ajax({
            method: 'GET',
            url: `${process.env.MIX_API_URL}/analytics/clients`,
            data : searchFilterSelector.find("form").serialize(),
            success: (data) => {

                // Reload data
                $(".dashboard-analytics-customers").html(data.customers);
                $(".dashboard-analytics-calls").html(data.calls);
                let durations = data.durations;
                //let label = 'Durée des appels <span class="badge badge-sm badge-info">secondes</span>';
                if(durations >= 60) {
                    //label = 'Durée des appels <span class="badge badge-sm badge-info">minutes</span> '+ (durations % 60);
                    durations = Math.trunc( durations/60 ) + ":" + (durations % 60);
                }
                $(".dashboard-analytics-durations").html(durations);

                $(".dashboard-analytics-messages").html(data.messages);
                $("dt.dashboard-analytics-quotes").html(data.ca_encaisse + " MAD");

                window.total_payments = 0;
                $.each( data.step_amount, function( key, group ) {
                    $("dt.dashboard-analytics-payment_" + group.program_payment_id).html(group.amount);
                    $("dd.dashboard-analytics-payment_" + group.program_payment_id).html("Total: " + group.count + " Payments");
                    window.total_payments += group.count;
                    // console.log(key, group);
                });
                $("dd.dashboard-analytics-quotes").html("Total: " + window.total_payments + " Payments");

                $('dt[class*="dashboard-analytics-step_"]').each(function(){
                    $(this).html("0");
                });
                $('dt[class*="dashboard-analytics-campaign_"]').each(function(){
                    $(this).html("0");
                });
                $.each( data.campaigns, function( key, group ) {
                    $(".dashboard-analytics-campaign_" + group.campaign_id).html(group.camps);
                });
                $.each( data.clients, function( key, group ) {
                    $(".dashboard-analytics-step_" + group.program_step_id).html(group.step_count);
                });
                /****************************************/
                // remove class loading
                callsBlock.removeClass('block-mode-loading');
                durationsBlock.removeClass("block-mode-loading");
                messagesBlock.removeClass('block-mode-loading');
                quotesBlock.removeClass('block-mode-loading');
                $('dt[class*="dashboard-analytics-campaign_"]').each(function(){
                    $(this).closest(".block.block-rounded").removeClass("block-mode-loading");
                });
                $('dt[class*="dashboard-analytics-step_"]').each(function(){
                    $(this).closest(".block.block-rounded").removeClass("block-mode-loading");
                });
                $('dt[class*="dashboard-analytics-payment_"]').each(function(){
                    $(this).closest(".block.block-rounded").removeClass("block-mode-loading");
                });
                /****************************************/
            },
            error: () => {
                console.log("Error Analytics backoffice.");
            }
        })
    }
    static setUpDOMEvents(dt) {
        //
        console.log('setUpDOMEvents');

        $('#enable-push-btn').on('click', function () {
            initSW();
        });
        $('#search-btn').on('click', function () {
            dt.api().ajax.reload()
        })
        /*$('.show-campaigns').on('click', function() {
            //..
            $('#modal-campaigns tbody').empty();
            let arr = null;
            if ($(this).find('.dashboard-analytics-campaign_7').length) {
                arr = window.influencers;
            } else if ($(this).find('.dashboard-analytics-campaign_16').length) {
                arr = window.references;
            }
            if (arr != null){
                $.each( arr, function( key, group ) {
                    console.log( key + " -- " + group.length );
                    $('<tr><td>'+key+'</td><td>'+group.length+'</td></tr>').appendTo('#modal-campaigns tbody');
                });
                $('#modal-campaigns').modal('show');
            }
        });*/
        $('.dt_search_agent').on('select2:select', function (e) {
            var data = e.params.data;
            // console.log(e.params.data);
            $('.search-filter input[name="agent-group"]').val(e.params.data.group);
            dashboardPage.initDashboardAnalytics();
        });
        $('.dt_search_agent').on('select2:unselect', function (e) {
            //var data = e.params.data;
            //console.log(e.params.data);
            $('.search-filter input[name="agent-group"]').val("");
            dashboardPage.initDashboardAnalytics();
        });
        $('.dt_search_agent').select2({
            placeholder: 'Utilisateurs...',
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/agents`,
                dataType: 'json',
                delay: 250,
                method: 'POST',
                data: function (term, page) {
                    return {
                        q: term,
                    }
                },
                processResults: function (data) {
                    //console.log(data);
                    let res = [];
                    let minus = -101;
                    $.each(data, function(role, group) {
                        if (role == 'Agent') {
                            return true;
                        }
                        let groupObj = {
                            "text" : role,
                        };
                        let children = [];
                        // console.log(role, group);
                        children.push({
                            text: 'n/d',
                            id: minus,
                            group: role
                        });
                        $.each(group, function(idx, user) {
                            console.log(user.name);
                            children.push({
                                text: user.name,
                                id: user.id,
                                group: role
                            });
                        });
                        groupObj["children"] = children;
                        res.push(groupObj);
                        --minus;
                    });
                    // console.log(res);
                    return {
                        results:  res
                    }
                },
                initSelection: function(element, callback) {
                },
                cache: true
            }
        });
        $('.dt_search_program').select2({
            placeholder: 'Programmes...',
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/getPrograms`,
                dataType: 'json',
                delay: 250,
                method: 'POST',
                data: function (term, page) {
                    return {
                        q: term,
                    }
                },
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    }
                },
                initSelection: function(element, callback) {
                },
                cache: true
            }
        });
        $('.dt_search_date').change(function() {
            dashboardPage.initDashboardAnalytics();
            //console.log("test.");
        });
        $('.dt_search_program').on('select2:select', function (e) {
            //var data = e.params.data;
            //console.log(e.params.data);
            dashboardPage.initDashboardAnalytics();
        });
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            $('input[name="date_start"]').val(start.format('YYYY-MM-DD'));
            $('input[name="date_end"]').val(end.format('YYYY-MM-DD'));
            dashboardPage.initDashboardAnalytics();
        });
        $('.dt_search_program').on('select2:unselect', function (e) {
            //var data = e.params.data;
            //console.log(e.params.data);
            dashboardPage.initDashboardAnalytics();
        });
        // dt_search_state
        $('.dt_search_state').on('select2:unselect', function (e) {
            //var data = e.params.data;
            //console.log(e.params.data);
            dashboardPage.initDashboardAnalytics();
        });
        $('.dt_search_state').on('select2:select', function (e) {
            //var data = e.params.data;
            //console.log(e.params.data);
            dashboardPage.initDashboardAnalytics();
        });
        $('.dt_search_state').select2({
            placeholder: 'Status...',
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/getLeadStatus`,
                dataType: 'json',
                delay: 250,
                method: 'POST',
                data: function (term, page) {
                    return {
                        q: term,
                    }
                },
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.status_label_fr,
                                id: item.id
                            }
                        })
                    }
                },
                initSelection: function(element, callback) {
                },
                cache: true
            }
        });
        $('.dt_search_campaign').on('select2:unselect', function (e) {
            //var data = e.params.data;
            //console.log(e.params.data);
            dashboardPage.initDashboardAnalytics();
        });
        $('.dt_search_campaign').on('select2:select', function (e) {
            //var data = e.params.data;
            //console.log(e.params.data);
            dashboardPage.initDashboardAnalytics();
        });
        $('.dt_search_campaign').select2({
            placeholder: 'Canal...',
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/getCampaigns`,
                dataType: 'json',
                delay: 250,
                method: 'POST',
                data: function (term, page) {
                    return {
                        q: term,
                    }
                },
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.campaign_name,
                                id: item.id
                            }
                        })
                    }
                },
                initSelection: function(element, callback) {
                },
                cache: true
            }
        });
        $('.dt_search_phase').select2({
            placeholder: 'Phase...',
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/getPhases`,
                dataType: 'json',
                delay: 250,
                method: 'POST',
                data: function (term, page) {
                    return {
                        q: term,
                    }
                },
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.phase_label_fr,
                                id: item.id
                            }
                        })
                    }
                },
                initSelection: function(element, callback) {
                },
                cache: true
            }
        });
        $('.dt_search_step').select2({
            placeholder: 'Etapes...',
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/getSteps`,
                dataType: 'json',
                delay: 250,
                method: 'POST',
                data: function (term, page) {
                    return {
                        q: term,
                    }
                },
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.step_label,
                                id: item.id
                            }
                        })
                    }
                },
                initSelection: function(element, callback) {
                },
                cache: true
            }
        });
        $('.dt_search_step').on('select2:select', function (e) {
            //var data = e.params.data;
            console.log(e.params.data.text);
            $("small.clients-step").text(e.params.data.text);
            dashboardPage.initDashboardAnalytics();
        });
        $('.dt_search_step').on('select2:unselect', function (e) {
            //var data = e.params.data;
            //console.log(e.params.data);
            $("small.clients-step").text("");
            dashboardPage.initDashboardAnalytics();
        });

        $(".dropdown-item").on('click', function () {
            // L
            var text = $('#dropdown-time').text();
            $('.dropdown #date').val($(this).html());
            //$('#dropdown-time').text(text.replace($(this).html(), text));
            $('#dropdown-time').contents()[2].textContent=$(this).html();
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.setUpDOMEvents(this.initDataTables());
        this.initDashboardAnalytics();
    }
}

// Initialize when page loads
jQuery(() => { dashboardPage.init(); });
