/*
 *  Document   : campaigns.js
 *  Author     : RR feat NTX
 *  Description: Custom JS code used in campaigns page
 */

class campaignsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "campaign_name", "name": "campaign_name", title: 'nom'}, // nom
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                        (action.available ?
                                `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                :
                                ''
                        )
                    )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#campaigns-dt');
        let blockSelector = $('#campaigns-dt-block');

        // Init campaigns DataTable
        let campaignsTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-s-earch').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/campaigns`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events
        //TODO put dom events here

        //SEARCH FILTER
        $('.submit-search').on('click', function () {
            campaignsTable.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            campaignsTable.api().ajax.reload();
        });

        //ADD FORM
        let campaignAddBlockSelector = $('#block-campaign-add');
        let campaignAddFormSelector = $('#campaign-add-form');

        $('#campaign-table-add').on('click', function () {

            //hide previous validation errors
            campaignAddFormSelector.find('.invalid-feedback').hide();
            campaignAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#campaign-add-form').trigger('reset');
            $('#modal-campaign-add').modal('show');
        });

        $('#add-campaign-btn').on('click', function () {

            //hide previous validation errors
            campaignAddFormSelector.find('.invalid-feedback').hide();
            campaignAddFormSelector.find('.form-control').removeClass('is-invalid');

            campaignAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/campaigns`,
                data: campaignAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'campaign ajouté avec succés!',
                        'success'
                    ).then(() => {
                        campaignAddBlockSelector.removeClass('block-mode-loading');
                        $('#campaign-add-form').trigger('reset');
                        campaignsTable.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        campaignAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout campaign!',
                            'error'
                        ).then(() => {
                            campaignAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //UPDATE FORM
        let campaignEditBlockSelector = $('#block-campaign-edit');
        let campaignEditFormSelector = $('#campaign-edit-form');

        $('body').on('click', '.campaign-table-edit', function() {

            //hide previous validation errors
            campaignEditFormSelector.find('.invalid-feedback').hide();
            campaignEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.campaignId = data.id;

            //set campaign edit form data
            console.log(data);
            $('#edit-form-campaign_name').val(data.campaign_name);

            $('#modal-campaign-edit').modal('show');
        });

        $('#update-campaign-btn').on('click', function () {

            //hide previous validation errors
            campaignEditFormSelector.find('.invalid-feedback').hide();
            campaignEditFormSelector.find('.form-control').removeClass('is-invalid');

            campaignEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/campaigns/${window.campaignId}`,
                data: campaignEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Campaign modifié avec succés!',
                        'success'
                    ).then(() => {
                        campaignEditBlockSelector.removeClass('block-mode-loading');
                        campaignsTable.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        campaignEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification Campaign!',
                            'error'
                        ).then(() => {
                            campaignEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //DELETE FORM
        $('body').on('click', '.campaign-table-delete', function() {

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.campaignId = data.id;

            Swal.fire({
                title: 'Etes vous sur?',
                text: "L'opération est irréversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteCampaign();
                }
            })
        });

        function deleteCampaign() {

            blockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'delete',
                url: `${process.env.MIX_API_URL}/campaigns/${window.campaignId}`,
                success: () => {
                    Swal.fire(
                        'Suppression!',
                        'Campaign supprimé avec succés!',
                        'success'
                    ).then(() => {
                        campaignsTable.api().ajax.reload(null, false);
                    })
                },
                error: () => {
                    Swal.fire(
                        'Suppression!',
                        'erreur suppression campaign!',
                        'error'
                    ).then(() => {
                        blockSelector.removeClass('block-mode-loading');
                    })
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { campaignsListPage.init(); });
