/*
 *  Document   : myinbox.js
 *  Author     : RR
 *  Description: Custom JS code used in bills page
 */

class myinboxPage {
    /*
     * Init DOM Events
     *
     */
    static initDOMEvents() {

        //DOM Events
        //TODO put dom events here

        $('#back-to-inbox-btn').on('click', function () {
            $('#message-block-title').show()
            $('#vendor-pagination-wrapper').show()
            $('#message-refresh').show()
            $('#messages-list').show()
            $('#back-to-inbox-btn').hide()
            $('#message-block-title-0').hide()
        })

        $('.message-subject-link').on('click', function () {
            $('#message-block-title').hide()
            $('#vendor-pagination-wrapper').hide()
            $('#message-refresh').hide()
            $('#messages-list').hide()
            $('#back-to-inbox-btn').show()

            let $subjectTitleH3 = $('#message-block-title-0')
            let $messageContent = $('#message-content')

            $subjectTitleH3.text($(this) . data('object'))
            $messageContent.text($(this) . data('content'))

            $subjectTitleH3.show()
            $messageContent.show()

        })
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents();
    }
}

// Initialize when page loads
jQuery(() => { myinboxPage.init(); });
