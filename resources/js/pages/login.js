/*
 *  Document   : login.js
 *  Author     : ahaloua
 */
window.CLEARPATH_LEAD_ID = null;

class LoginPage {
    static init() {
        $("body").on("change", "#customer-lang", function () {
            const data = `locale=` + $(this).val();
            $.ajax({
                method: "post",
                url: `/lang`,
                data: data,
                success: (res) => {},
                error: (err) => {},
            });
        });
    }
}

// Initialize when page loads
jQuery(() => {
    LoginPage.init();
});
