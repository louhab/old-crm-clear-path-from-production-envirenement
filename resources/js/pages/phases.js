/*
 *  Document   : phases.js
 *  Author     : RR feat NTX
 *  Description: Custom JS code used in phases page
 */

class phasesListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "name", "name": "firstname", title: 'nom'}, // nom
            {"data": "order", "name": "lastname", title: 'order'}, // order
            {"data": "pname", "name": "email", title: 'program'}, // program
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                        (action.available ?
                                `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                :
                                ''
                        )
                    )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#phases-dt');
        let blockSelector = $('#phases-dt-block');

        // Init phases DataTable
        let phasesTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-s-earch').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/phases`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events
        //TODO put dom events here

        //SEARCH FILTER
        $('.submit-search').on('click', function () {
            phasesTable.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            phasesTable.api().ajax.reload();
        });

        //ADD FORM
        let phaseAddBlockSelector = $('#block-phase-add');
        let phaseAddFormSelector = $('#phase-add-form');

        $('#phase-table-add').on('click', function () {

            //hide previous validation errors
            phaseAddFormSelector.find('.invalid-feedback').hide();
            phaseAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#phase-add-form').trigger('reset');
            $('#modal-phase-add').modal('show');
        });

        $('#add-phase-btn').on('click', function () {

            //hide previous validation errors
            phaseAddFormSelector.find('.invalid-feedback').hide();
            phaseAddFormSelector.find('.form-control').removeClass('is-invalid');

            phaseAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/phases`,
                data: phaseAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'Phase ajouté avec succés!',
                        'success'
                    ).then(() => {
                        phaseAddBlockSelector.removeClass('block-mode-loading');
                        $('#phase-add-form').trigger('reset');
                        phasesTable.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        phaseAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'Erreur ajout phase!',
                            'error'
                        ).then(() => {
                            phaseAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //UPDATE FORM
        let phaseEditBlockSelector = $('#block-phase-edit');
        let phaseEditFormSelector = $('#phase-edit-form');

        $('body').on('click', '.phase-table-edit', function() {

            //hide previous validation errors
            phaseEditFormSelector.find('.invalid-feedback').hide();
            phaseEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.phaseId = data.id;

            //set phase edit form data
            $('#edit-form-program_id').val(data.program_id);
            $('#edit-form-name').val(data.name);
            $('#edit-form-order').val(data.order);

            $('#modal-phase-edit').modal('show');
        });

        $('#update-phase-btn').on('click', function () {

            //hide previous validation errors
            phaseEditFormSelector.find('.invalid-feedback').hide();
            phaseEditFormSelector.find('.form-control').removeClass('is-invalid');

            phaseEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/phases/${window.phaseId}`,
                data: phaseEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Etudiant modifié avec succés!',
                        'success'
                    ).then(() => {
                        phaseEditBlockSelector.removeClass('block-mode-loading');
                        phasesTable.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        phaseEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification etudiant!',
                            'error'
                        ).then(() => {
                            phaseEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //DELETE FORM
        $('body').on('click', '.phase-table-delete', function() {

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.phaseId = data.id;

            Swal.fire({
                title: 'Etes vous sur?',
                text: "L'opération est irréversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    deletePhase();
                }
            })
        });

        function deletePhase() {

            blockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'delete',
                url: `${process.env.MIX_API_URL}/phases/${window.phaseId}`,
                success: () => {
                    Swal.fire(
                        'Suppression!',
                        'Phase supprimé avec succés!',
                        'success'
                    ).then(() => {
                        phasesTable.api().ajax.reload(null, false);
                    })
                },
                error: () => {
                    Swal.fire(
                        'Suppression!',
                        'erreur suppression phase!',
                        'error'
                    ).then(() => {
                        blockSelector.removeClass('block-mode-loading');
                    })
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { phasesListPage.init(); });
