/*
 *  Document   : tags.js
 *  Author     : RR feat NTX
 *  Description: Custom JS code used in campaigns page
 */

class tagsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "tag_name", "name": "tag_name", title: 'Mot Clé', mRender: function (data, type, row) {
              return `<a href="/tags/${row.id}">${row.tag_name}</a>`
            }}, // nom
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                        (action.available ?
                                `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                :
                                ''
                        )
                    )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#tags-dt');
        let blockSelector = $('#tags-dt-block');

        // Init tags DataTable
        let tagsTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-s-earch').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/tags`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events
        //TODO put dom events here

        //SEARCH FILTER
        $('.submit-search').on('click', function () {
            tagsTable.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            tagsTable.api().ajax.reload();
        });

        //ADD FORM
        let tagAddBlockSelector = $('#block-tag-add');
        let tagAddFormSelector = $('#tag-add-form');

        $('#tag-table-add').on('click', function () {

            //hide previous validation errors
            tagAddFormSelector.find('.invalid-feedback').hide();
            tagAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#tag-add-form').trigger('reset');
            $('#modal-tag-add').modal('show');
        });

        $('#add-tag-btn').on('click', function () {

            //hide previous validation errors
            tagAddFormSelector.find('.invalid-feedback').hide();
            tagAddFormSelector.find('.form-control').removeClass('is-invalid');

            tagAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/tags`,
                data: tagAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'Mot clé ajouté avec succés!',
                        'success'
                    ).then(() => {
                        tagAddBlockSelector.removeClass('block-mode-loading');
                        $('#tag-add-form').trigger('reset');
                        tagsTable.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        tagAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout tag!',
                            'error'
                        ).then(() => {
                            tagAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //UPDATE FORM
        let tagEditBlockSelector = $('#block-tag-edit');
        let tagEditFormSelector = $('#tag-edit-form');

        $('body').on('click', '.tag-table-edit', function() {

            //hide previous validation errors
            tagEditFormSelector.find('.invalid-feedback').hide();
            tagEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.tagId = data.id;

            //set tag edit form data
            $('#edit-form-tag_name').val(data.tag_name);

            $('#modal-tag-edit').modal('show');
        });

        $('#update-tag-btn').on('click', function () {

            //hide previous validation errors
            tagEditFormSelector.find('.invalid-feedback').hide();
            tagEditFormSelector.find('.form-control').removeClass('is-invalid');

            tagEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/tags/${window.tagId}`,
                data: tagEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Mot clé modifié avec succés!',
                        'success'
                    ).then(() => {
                        tagEditBlockSelector.removeClass('block-mode-loading');
                        tagsTable.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        tagEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification mot clé!',
                            'error'
                        ).then(() => {
                            tagEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //DELETE FORM
        $('body').on('click', '.tag-table-delete', function() {

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.tagId = data.id;

            Swal.fire({
                title: 'Etes vous sur?',
                text: "L'opération est irréversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    deletetag();
                }
            })
        });

        function deletetag() {

            blockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'delete',
                url: `${process.env.MIX_API_URL}/tags/${window.tagId}`,
                success: () => {
                    Swal.fire(
                        'Suppression!',
                        'Mot clé supprimé avec succés!',
                        'success'
                    ).then(() => {
                        tagsTable.api().ajax.reload(null, false);
                    })
                },
                error: () => {
                    Swal.fire(
                        'Suppression!',
                        'erreur suppression mot clé!',
                        'error'
                    ).then(() => {
                        blockSelector.removeClass('block-mode-loading');
                    })
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { tagsListPage.init(); });
