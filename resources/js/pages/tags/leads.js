/*
 *  Document   : list.js
 *  Author     : RR
 *  Description: Custom JS code used in leads list page
 */
window.CLEARPATH_LEAD_ID = null;
//window.fnCallback = false;

class leadsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            //sType: "html",
            sLengthSelect: "form-control form-control-sm",
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>',
                },
            },
        });

        let columns = [
            {
                data: "lead_id",
                name: "lead_id",
                visible:
                    window.userRole.role_id != 1 &&
                    window.userRole.role_id != 10
                        ? false
                        : true,
                className: "select-checkbox",
                mRender: function (data, type, row) {
                    return "";
                },
            }, // lead_id
            {
                data: "lastname",
                name: "lastname",
                title: "Nom",
                className: "font-w600 font-size-sm",
            }, // lastname
            {
                data: "firstname",
                name: "firstname",
                title: "Prénom",
                className: "font-w600 font-size-sm",
            }, // firstname
            {
                data: "email",
                name: "email",
                title: "Email",
                mRender: function (data, type, row) {
                    return `${
                        data.match(/^([^@]*)@/)[1]
                    }<em class="text-muted">@${data.split("@")[1]}</em>`;
                },
            }, // email
            {
                data: "program_id",
                name: "program_id",
                title: "Programme",
                mRender: function (data, type, row) {
                    //return `<span class="badge badge-${data == 1 ? 'info' : 'success'}">${row.pname}</span>`
                    return data
                        ? row.pname
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // program
            {
                data: "phone",
                name: "phone",
                title: "Phone",
                mRender: function (data, type, row) {
                    return data
                        ? data
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // phone
            {
                data: "canal",
                name: "canal",
                title: "Canal",
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // canal
            {
                data: "influencer",
                name: "influencer",
                title: "Référent",
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // influencer
            {
                data: "conseiller",
                name: "conseiller",
                title: "Conseiller",
                visible: window.userRole.role_id == 3 ? false : true,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // influencer
            // {"data": "case_id", "name": "case_id", title: 'Client', mRender: function (data, type, row) {
            //         if(!row.is_agent)
            //             return data ? `<a href="${process.env.MIX_PUBLIC_URL}/cases/${data}" class="btn btn-alt-primary">Dossier</a>` : `<button type="button" class="btn btn-alt-success convert-lead-btn">Convertir</button>`
            //         else
            //             return '-';
            //     }}, // customer
            {
                data: "created",
                name: "created",
                title: "Date de Création",
                mRender: function (data, type, row) {
                    if (data) {
                        var date = new Date(data);
                        return (
                            (date.getMonth() > 8
                                ? date.getMonth() + 1
                                : "0" + (date.getMonth() + 1)) +
                            "/" +
                            (date.getDate() > 9
                                ? date.getDate()
                                : "0" + date.getDate()) +
                            "/" +
                            date.getFullYear()
                        );
                    }
                    return '<span class="badge badge-info">n/d</span>';
                },
            }, // Date de Création
            {
                data: "lead_status_id",
                name: "lead_status_id",
                title: "Status",
                mRender: function (data, type, row) {
                    let badge = "danger";
                    switch (row.lead_status_id) {
                        case 1:
                            // code block
                            badge = "success";
                            break;
                        case 3:
                            // code block
                            badge = "secondary";
                            break;
                        case 4:
                            // code block
                            badge = "info";
                            break;
                        case 5:
                            // code block
                            badge = "warning";
                            break;
                        // default:
                        //   // code block
                    }
                    return data
                        ? `<span class="badge badge-${badge}">${row.status_label}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // canal
            {
                data: "actions",
                className: "text-center",
                title: "action",
                width: "150px",
                mRender: function (data, type, row) {
                    //if (action.islink) {}
                    return `<div class="d-flex justify-content-center action">
                                ${data.map((action) =>
                                    action.available
                                        ? `<div class="btn-group" style="margin:5px">
                                    <a class="${action.className}" href="${
                                              action.islink
                                                  ? `${
                                                        process.env
                                                            .MIX_PUBLIC_URL
                                                    }/${
                                                        action.title ==
                                                        "afficher"
                                                            ? "lead"
                                                            : "leads"
                                                    }/${row.lead_id}`
                                                  : "javascript:void(0)"
                                          }" title="${action.title}" class="${
                                              action.className
                                          }" data-id="${
                                              row.lead_id
                                          }"><i class="${action.icon}"></i></a>
                                </div>`
                                        : ""
                                )}
                            </div>`;
                },
            },
        ];

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#leads-dt");
        let blockSelector = $("#leads-dt-block");

        // Init leads DataTable
        return tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`,
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[0, "desc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/tag/leads/` + tag,
                    data: function (d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function (index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    },
                },
                columns: columns,
                fnDrawCallback: function () {
                    blockSelector.removeClass("block-mode-loading");
                    /* Toolbar Table */
                    //console.log(window.userRole);
                    if (
                        window.userRole.role_id == 1 ||
                        window.userRole.role_id == 10
                    ) {
                        $("div.toolbar").html(
                            '<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-cente mb-2" style="width: 30%;"><select class="form-control dt_action_field" name="agent[]" multiple="multiple"></select><button type="button" class="btn btn-alt-primary ml-2" id="dt_action_btn">Affecter</button></div>'
                        );
                    }
                    $(".dt_action_field").select2({
                        placeholder: "Conseillers...",
                        allowClear: true,
                        ajax: {
                            url: `${process.env.MIX_API_URL}/agents`,
                            dataType: "json",
                            delay: 250,
                            method: "POST",
                            data: function (term, page) {
                                return {
                                    q: term,
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function (item) {
                                        return {
                                            text: item.name,
                                            id: item.id,
                                        };
                                    }),
                                };
                            },
                            initSelection: function (element, callback) {},
                            cache: true,
                        },
                    });
                    $("#dt_action_btn").on("click", function () {
                        let conseillers = $(".dt_action_field").select2("val");
                        var $dataSelected = $("#leads-dt")
                            .DataTable()
                            .rows(".selected")
                            .data();
                        if (
                            conseillers.length > 0 &&
                            $dataSelected.length > 0
                        ) {
                            let i = 0;

                            var resultConseilArray = $dataSelected.reduce(
                                (resultArray, item, index) => {
                                    if (i == conseillers.length) {
                                        i = 0;
                                    }

                                    resultArray[item.lead_id] = conseillers[i];
                                    i = i + 1;

                                    return resultArray;
                                },
                                []
                            );
                            //console.log(Object.keys(result));
                            var cs = 0;
                            // console.log();
                            Swal.fire({
                                title: "Etes vous sur?",
                                text:
                                    "conseiller: " +
                                    $(".dt_action_field").select2("data")[0]
                                        .text +
                                    ", leads: " +
                                    $dataSelected.length,
                                icon: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#3085d6",
                                cancelButtonColor: "#d33",
                                confirmButtonText: "Oui, Affecter!",
                                cancelButtonText: "Annuler",
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    //let bs = $('#leads-dt-block');
                                    $("#leads-dt-block").addClass(
                                        "block-mode-loading"
                                    );
                                    $dataSelected.each(function (data, index) {
                                        // console.log( cs + ' Data ID: '+data.lead_id+' Conseiller: '+resultConseilArray[data.lead_id] );
                                        $.ajax({
                                            method: "put",
                                            url: `${
                                                process.env.MIX_API_URL
                                            }/leadConseiller/${data.lead_id}/${
                                                resultConseilArray[data.lead_id]
                                            }`,
                                            success: () => {
                                                cs += 1;
                                                //console.log(cs + "..." + $dataSelected.length);
                                                if (
                                                    cs == $dataSelected.length
                                                ) {
                                                    Swal.fire(
                                                        "Modification!",
                                                        cs +
                                                            " Leads afecté à " +
                                                            $(
                                                                ".dt_action_field"
                                                            ).select2("data")[0]
                                                                .text,
                                                        "success"
                                                    ).then(() => {
                                                        $("#leads-dt")
                                                            .DataTable()
                                                            .ajax.reload(
                                                                null,
                                                                false
                                                            );
                                                    });
                                                }
                                            },
                                            error: () => {
                                                $(
                                                    "#leads-dt-block"
                                                ).removeClass(
                                                    "block-mode-loading"
                                                );
                                                //window.ce += 1;
                                                // Swal.fire(
                                                //     'Modification!',
                                                //     'erreur Modification lead!',
                                                //     'error'
                                                // );
                                            },
                                        });
                                    });
                                }
                            });
                        } else if ($dataSelected.length == 0) {
                            Swal.fire(
                                "Modification!",
                                "Veuiller Selectinner un lead!",
                                "warning"
                            );
                        } else {
                            Swal.fire(
                                "Modification!",
                                "Veuiller Choisir un conseiller!",
                                "warning"
                            );
                        }
                    });
                    /* Toolbar Table */
                    // if ($('th.conseiller').length > 0){
                    //     $('#leads-dt th').each(function (index) {
                    //         if (index == 8) {
                    //             console.log($(this).text());
                    //             $(this).hide();
                    //             return false;
                    //         }
                    //     });
                    //     $('#leads-dt tbody tr').each(function () {
                    //         //console.log(.length);
                    //         $(this).find("> td").each(function(index){
                    //             if (index == 8) {
                    //                 //console.log($(this).text());
                    //                 $(this).hide();
                    //                 return false;
                    //             }
                    //         });
                    //     });
                    // }
                    $(".submit-search").prop("disabled", false);
                },
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                        orderable: true,
                        searchable: false,
                        className: "select-checkbox",
                    },
                ],
                select: {
                    style: "multi",
                    selector: "td:first-child",
                },
                dom: '<"toolbar">lfrtip',
            });
    }

    static initDOMEvents(dt) {
        // $('#leads-dt').on('click', 'td[class*="select-checkbox"]', function () {
        //     let $row = $(this).closest('tr');

        //     let data =  dt.DataTable().row($row).data();
        //     //console.log($(this).html());
        //     console.log(data);
        // });

        //$('.dt_action_field').;
        var request_pending = false;
        $("body").on("click", ".convert-lead-btn", function () {
            console.log(request_pending);
            if (request_pending) {
                return;
            }
            const $row = $(this).closest("tr");

            const data = dt.DataTable().row($row).data();
            request_pending = true;
            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/leads/${data.lead_id}`, //CHANGE FOR CUST
                success: () => {
                    Swal.fire(
                        "Création",
                        "Lead converti avec succés!",
                        "success"
                    ).then(() => {
                        dt.api().ajax.reload();
                    });
                    request_pending = false;
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                    } else
                        Swal.fire(
                            "Modification",
                            "erreur conversion lead!",
                            "error"
                        );
                    request_pending = false;
                },
            });
        });

        $("#search-btn").on("click", function () {
            dt.api().ajax.reload();
        });

        $("#clear-search-btn").on("click", function () {
            $("#search-form").trigger("reset");
            $("#lead").val(null).trigger("change");
            $("#phone").val(null);
            $("#country").val(null).trigger("change");
            dt.api().ajax.reload();
        });

        // $('.dt_search_lead').select2({
        //     placeholder: 'Séléctionnez un lead',
        //     allowClear: true,
        //     ajax: {
        //         url: `${process.env.MIX_API_URL}/getLeads`,
        //         dataType: 'json',
        //         delay: 250,
        //         method: 'POST',
        //         data: function (term, page) {
        //             return {
        //                 q: term,
        //             }
        //         },
        //         processResults: function (data) {
        //             return {
        //                 results:  $.map(data, function (item) {
        //                     return {
        //                         text: item.name,
        //                         id: item.id
        //                     }
        //                 })
        //             }
        //         },
        //         initSelection: function(element, callback) {
        //         },
        //         cache: true
        //     }
        // })

        $(".dt_search_country").select2({
            placeholder: "Pays",
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/getCountries`,
                dataType: "json",
                delay: 250,
                method: "POST",
                data: function (term, page) {
                    return {
                        q: term,
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                            };
                        }),
                    };
                },
                cache: true,
            },
        });

        //DELETE Quote
        $("body").on("click", ".lead-delete", function () {
            const $row = $(this).closest("tr");
            const data = $("#leads-dt").DataTable().row($row).data();
            //alert(data);
            window.CLEARPATH_LEAD_ID = data.lead_id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, supprimer!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    //let bs = $('#leads-dt-block');
                    leadsListPage.deleteLead(dt);
                }
            });
        });
    }

    static deleteLead(dt) {
        $("#leads-dt-block").addClass("block-mode-loading");

        $.ajax({
            method: "delete",
            url: `${process.env.MIX_API_URL}/leads/${window.CLEARPATH_LEAD_ID}`,
            success: () => {
                Swal.fire(
                    "Suppression!",
                    "Lead supprimé avec succés!",
                    "success"
                ).then(() => {
                    dt.api().ajax.reload(null, false);
                });
            },
            error: () => {
                Swal.fire(
                    "Suppression!",
                    "erreur suppression lead!",
                    "error"
                ).then(() => {
                    $("#leads-dt-block").removeClass("block-mode-loading");
                });
            },
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents(this.initDataTables());
    }
}

// Initialize when page loads
jQuery(() => {
    leadsListPage.init();
});
