/*
 *  Document   : leadsforms.js
 *  Author     : RR
 *  Description: Custom JS code used in leadsforms page
 */
window.CLEARPATH_LEADFORM_ID = null
class leadsFormsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#leadsforms-dt');
        let blockSelector = $('#leadsforms-dt-block');

        // Init histocalls DataTable
        return tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-search').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/discounts`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: [
                {"data": "id", "name": "id", "visible": false}, // Id
                {"data": "name", "name": "name", title: 'Bénéficiaire'}, // name
                {"data": "payment", "name": "payment", title: 'Paiement'}, // datedate
                {"data": "discount", "name": "discount", title: 'Réduction'} // datedate
                /*{
                    data: "actions",
                    className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                        return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                            (action.available ?
                                    `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                    :
                                    ''
                            )
                        )}
                            </div>`;
                    }
                }*/
            ],
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });


    }

    //DOM Events
    static initDOMEvents(dt) {

        //CHANGE ACTIVE STATE
        $('body').on('click', '.activestate', function (e) {

            e.preventDefault();

            let tableSelector = $('#leadsforms-dt');
            let leadsFormsAddBlockSelector = $('#block-payments-add');

            const $row = $(this).closest('tr');

            const data =  tableSelector.DataTable().row($row).data();

            console.log($(this).attr("data-id"));

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/leadsforms/setactivestate`,
                data: {
                    id : $(this).attr("data-id"),
                    state : $(this).is(':checked') ? 1 : 0
                },
                success: () => {
                    Swal.fire(
                        'Modification',
                        'Etat changée avec succés!',
                        'success'
                    ).then(() => {
                        leadsFormsAddBlockSelector.removeClass('block-mode-loading');
                        dt.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        leadsFormsAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification',
                            'erreur modification état!',
                            'error'
                        ).then(() => {
                            leadsFormsAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });

        });
        $('#search-btn').on('click', function () {
            dt.api().ajax.reload()
        });
        $('#clear-search-btn').on('click', function () {
            $('#search-form').trigger("reset");
            $("#campaign-type").val(null).trigger("change");
            dt.api().ajax.reload()
        });
        $('#payments-table-add').on('click', function () {

            let leadsFormsAddFormSelector = $('#payments-add-form');

            //hide previous validation errors
            leadsFormsAddFormSelector.find('.invalid-feedback').hide();
            leadsFormsAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#payments-add-form').trigger('reset');
            $('#modal-payments-add').modal('show');
        });
        $("#add-form-percent").on("change", function() {
            document.getElementById('rangePercent').innerText = $(this).val() + " %";
            let lead = $("#add-form-lead").val();
            let payment = $("#add-form-payment").val();
            // console.log(lead, payment);
            if (lead && payment) {
                let amount = parseInt($("#amount").attr("value"));
                // console.log("Amount: " + amount);
                if ($(this).val() != 0) {
                    let percent = amount * parseInt($(this).val())/100;
                    // console.log("Percent: " + (amount - percent));
                    $("#newAmount").text(amount - percent);
                    $("#newAmount").attr("value", amount - percent);
                } else {
                    $("#newAmount").text(amount);
                    $("#newAmount").attr("value", amount);
                }
            }
            // console.log();
        });
        // add-form-payment
        $('#add-form-payment').select2({}).empty();
        $('#add-form-payment').select2({
            placeholder: 'Payment..',
            ajax: {
                url: "".concat("/api/v1", "/getPayments"),
                dataType: 'json',
                delay: 250,
                method: 'POST',
                data: function data(term, page) {
                    return {
                        q: term,
                    };
                },
                processResults: function processResults(data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name_fr,
                                id: item.id
                            };
                        })
                    };
                },
                cache: false
            }
        });
        $('.claculate').on('select2:select', function (e) {

            var data = e.params.data;
            let paymentsAddFormSelector = $('#payments-add-form');
            let paymentsAddBlockSelector = $('#block-payments-add');

            //hide previous validation errors
            paymentsAddFormSelector.find('.form-control').removeClass('is-invalid');

            paymentsAddBlockSelector.addClass('block-mode-loading');
            // console.log(data.id);
            var customer = $('#add-form-lead').val();
            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/getAmount`,
                data: paymentsAddFormSelector.serialize(),
                success: (data) => {
                    // result
                    // console.log(data);
                    $("#amount").text(data.amount);
                    $("#newAmount").text(data.amount);
                    $("#amount").attr("value", data.amount);
                    let percent = $("#add-form-percent").val();
                    if (percent != 0) {
                        percent = data.amount * parseInt(percent)/100;
                        $("#newAmount").text(data.amount - percent);
                        $("#newAmount").attr("value", data.amount - percent);
                    }
                    paymentsAddBlockSelector.removeClass('block-mode-loading');
                },
                error: () => {
                    paymentsAddBlockSelector.removeClass('block-mode-loading');
                    // console.log("error!");
                }
            });
        });
        $('#add-form-lead').select2({}).empty();
        $('#add-form-lead').select2({
            placeholder: 'Lead..',
            ajax: {
                url: "".concat("/api/v1", "/getCustomers"),
                dataType: 'json',
                delay: 250,
                method: 'POST',
                data: function data(term, page) {
                    return {
                        q: term,
                    };
                },
                processResults: function processResults(data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        })
                    };
                },
                cache: false
            }
        });
        $(document).on("click", ".leadsforms-table-edit",function() {

            let leadsFormsEditFormSelector = $('#leadsforms-edit-form');

            //get form id
            const $row = $(this).closest('tr');
            const data =  $('#leadsforms-dt').DataTable().row($row).data();
            window.CLEARPATH_LEADFORM_ID = data.id;
            // old values
            $("#edit-form-title").attr("value", data.title);
            $('#edit-form-campaign option[value="'+data.lead_form_marketing_campaign_id+'"]').attr('selected','selected');
            //console.log();

            //hide previous validation errors
            leadsFormsEditFormSelector.find('.invalid-feedback').hide();
            leadsFormsEditFormSelector.find('.form-control').removeClass('is-invalid');

            $('#leadsforms-edit-form').trigger('reset');
            $('#modal-leadsforms-edit').modal('show');
        });
        $('#edit-leadsforms-btn').on('click', function () {

            let leadsFormsEditFormSelector = $('#leadsforms-edit-form');
            let leadsFormsEditBlockSelector = $('#block-leadsforms-edit');

            //hide previous validation errors
            leadsFormsEditFormSelector.find('.form-control').removeClass('is-invalid');

            leadsFormsEditBlockSelector.addClass('block-mode-loading');

            //let formID =

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/leadsforms/${window.CLEARPATH_LEADFORM_ID}`,
                data: leadsFormsEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Edit!',
                        'Formulaire Edité avec succés!',
                        'success'
                    ).then(() => {
                        leadsFormsEditBlockSelector.removeClass('block-mode-loading');
                        //$('#leadsforms-edit-form').trigger('reset');
                        dt.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        leadsFormsEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout formulaire!',
                            'error'
                        ).then(() => {
                            leadsFormsEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        $('#add-discount-btn').on('click', function () {

            let paymentsAddFormSelector = $('#payments-add-form');
            let paymentsAddBlockSelector = $('#block-payments-add');

            $("#add-form-amount").val($("#newAmount").attr("value"));

            //hide previous validation errors
            paymentsAddFormSelector.find('.form-control').removeClass('is-invalid');

            paymentsAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/discount`,
                data: paymentsAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'Réduction ajouté avec succés!',
                        'success'
                    ).then(() => {
                        paymentsAddBlockSelector.removeClass('block-mode-loading');
                        $('#payments-add-form').trigger('reset');
                        dt.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        paymentsAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout formulaire!',
                            'error'
                        ).then(() => {
                            paymentsAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //Copy lead form link
        $('body').on('click', '.leadsforms-table-copy', function () {

            let tableSelector = $('#leadsforms-dt');

            const $row = $(this).closest('tr');

            const data =  tableSelector.DataTable().row($row).data();

            leadsFormsListPage.copyToClipboard(process.env.MIX_PUBLIC_URL + '/getintouch/' + data.token)
        });

        //SEARCH FILTER
        /*$('.submit-search').on('click', function () {
            dt.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            dt.api().ajax.reload();
        });

        //UPDATE FORM
        let histocallEditBlockSelector = $('#block-histocall-edit');
        let histocallEditFormSelector = $('#histocall-edit-form');

        $('body').on('click', '.histocall-table-edit', function() {

            //hide previous validation errors
            histocallEditFormSelector.find('.invalid-feedback').hide();
            histocallEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.histocallId = data.id;

            $('#edit-form-calldt').val(data.call_dt);
            $('#edit-form-duration').val(data.duration);
            $('#edit-form-calldescr').val(data.call_descr);

            if(data.customer_id == null){
                $('#edit-form-lead').val(data.lead_id);
                $('#modal-histocall-lead-edit').modal('show');
            }
            if(data.lead_id == null){
                $('#edit-form-customer').val(data.customer_id);
                $('#modal-histocall-customer-edit').modal('show');
            }


        });

        $('#update-histocall-btn').on('click', function () {

            //hide previous validation errors
            histocallEditFormSelector.find('.invalid-feedback').hide();
            histocallEditFormSelector.find('.form-control').removeClass('is-invalid');

            histocallEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/histocalls/${window.histocallId}`,
                data: histocallEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Etudiant modifié avec succés!',
                        'success'
                    ).then(() => {
                        histocallEditBlockSelector.removeClass('block-mode-loading');
                        dt.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        histocallEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification etudiant!',
                            'error'
                        ).then(() => {
                            histocallEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });
        */
        //DELETE FORM
        $('body').on('click', '.leadsforms-table-delete', function() {

            const $row = $(this).closest('tr');
            const data =  $('#leadsforms-dt').DataTable().row($row).data();
            window.CLEARPATH_LEADFORM_ID = data.id;

            Swal.fire({
                title: 'Etes vous sur?',
                text: "L'opération est irréversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteLeadForm();
                }
            })
        });
        function deleteLeadForm() {

            $('#leadsforms-dt-block').addClass('block-mode-loading');

            $.ajax({
                method: 'delete',
                url: `${process.env.MIX_API_URL}/leadsforms/${window.CLEARPATH_LEADFORM_ID}`,
                success: () => {
                    Swal.fire(
                        'Suppression!',
                        'Lead form supprimé avec succés!',
                        'success'
                    ).then(() => {
                        dt.api().ajax.reload(null, false);
                    })
                },
                error: () => {
                    Swal.fire(
                        'Suppression!',
                        'erreur suppression form!',
                        'error'
                    ).then(() => {
                        $('#leadsforms-dt-block').removeClass('block-mode-loading');
                    })
                }
            });
        }
    }

    static copyToClipboard(str) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(str).select();
        document.execCommand("copy");
        $temp.remove();
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents(this.initDataTables())
    }
}

// Initialize when page loads
jQuery(() => { leadsFormsListPage.init(); });
