/*
 *  Document   : leadsforms.js
 *  Author     : RR
 *  Description: Custom JS code used in leadsforms page
 */
window.CLEARPATH_DOCUMENT_STEP_ID = null;
class leadsFormsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info:
                    "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#documents-dt");
        let blockSelector = $("#documents-dt-block");

        // Init histocalls DataTable
        return tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[0, "asc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/document-steps/${window.CLEARPATH_DOCUMENT_ID}`,
                    data: function(d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function(index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    }
                },
                columns: [
                    { data: "id", name: "id", visible: false }, // Id // name
                    {
                        data: "step_name",
                        name: "step_name",
                        title: "Step Name"
                    }, // name_fr
                    {
                        data: "program_name",
                        name: "program_name",
                        title: "Programme"
                    }, // collection_name
                    {
                        data: "order",
                        name: "order",
                        title: "Ordre"
                    }, // group_name
                    {
                        data: "actions",
                        className: "text-center",
                        title: "action",
                        width: "150px",
                        mRender: function(data, type, row) {
                            return `<div class="d-flex justify-content-center action">
                                ${data.map(action => {
                                if (action.islink) {
                                    if (action.title == "afficher") {
                                        return `<div class="btn-group" style="margin:5px"><a class="${action.className}" href="${process.env.MIX_PUBLIC_URL}/document/${row.id}" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></a></div>`;
                                    }
                                } else {
                                    return `<div class="btn-group" style="margin:5px">
                                                        <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                                    </div>`;
                                }
                            })}
                            </div>`;
                        }

                    }
                ],
                fnDrawCallback: function() {
                    blockSelector.removeClass("block-mode-loading");
                    $(".submit-search").prop("disabled", false);
                },
                columnDefs: [
                    {
                        /*targets: [2],
                orderable: false,
                searchable: false*/
                    }
                ]
            });
    }

    //DOM Events
    static initDOMEvents(dt) {
        $("#search-btn").on("click", function() {
            dt.api().ajax.reload();
        });
        $("#clear-search-btn").on("click", function() {
            $("#search-form").trigger("reset");
            $("#campaign-type")
                .val(null)
                .trigger("change");
            dt.api().ajax.reload();
        });
        $("#documents-table-add").on("click", function() {
            let documentAddFormSelector = $("#documents-add-form");

            //hide previous validation errors
            documentAddFormSelector.find(".invalid-feedback").hide();
            documentAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            $("#documents-add-form").trigger("reset");
            $("#modal-documents-add").modal("show");
        });

        $(document).on("click", ".documents-table-edit", function() {
            let documentsEditFormSelector = $("#documents-edit-form");
            documentsEditFormSelector.trigger("reset");

            //get form id
            const $row = $(this).closest("tr");
            const data = $("#documents-dt")
                .DataTable()
                .row($row)
                .data();
            window.CLEARPATH_DOCUMENT_STEP_ID = data.id;
            // old values
            $("#edit-form-step").val(data.program_sub_step_id);
            $("#edit-form-program").val(data.program_id);
            $("#edit-form-order").val(data.order);

            //hide previous validation errors
            documentsEditFormSelector.find(".invalid-feedback").hide();
            documentsEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            // $('#leadsforms-edit-form').trigger('reset');
            $("#modal-document-edit").modal("show");
        });

        $("#edit-document-btn").on("click", function() {
            let documentsEditFormSelector = $("#documents-edit-form");
            // let leadsFormsEditFormSelector = $("#leadsforms-edit-form");
            let documentEditBlockSelector = $("#block-document-edit");

            //hide previous validation errors
            documentsEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            documentEditBlockSelector.addClass("block-mode-loading");

            $.ajax({
                method: "put",
                url: `${process.env.MIX_API_URL}/document-steps/${window.CLEARPATH_DOCUMENT_STEP_ID}`,
                data: documentsEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        "Edit!",
                        "Pivot Edité avec succés!",
                        "success"
                    ).then(() => {
                        documentEditBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                        //$('#leadsforms-edit-form').trigger('reset');
                        dt.api().ajax.reload();
                    });
                },
                error: xhr => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        documentEditBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur edition pivot!",
                            "error"
                        ).then(() => {
                            documentEditBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                }
            });
        });

        $("#add-documents-btn").on("click", function() {
            let documentAddFormSelector = $("#documents-add-form");
            let documentAddBlockSelector = $("#block-documents-add");

            //hide previous validation errors
            documentAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            documentAddBlockSelector.addClass("block-mode-loading");
            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/document-steps/${window.CLEARPATH_DOCUMENT_ID}`,
                data: documentAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        "Ajout!",
                        "clear pivot ajouté avec succés!",
                        "success"
                    ).then(() => {
                        documentAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                        $("#documents-add-form").trigger("reset");
                        dt.api().ajax.reload();
                    });
                },
                error: xhr => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        documentAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur ajout clear pivot!",
                            "error"
                        ).then(() => {
                            documentAddBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                }
            });
        });

        //DELETE Document
        $("body").on("click", ".documents-table-delete", function() {
            const $row = $(this).closest("tr");
            const data = $("#documents-dt")
                .DataTable()
                .row($row)
                .data();
            window.CLEARPATH_DOCUMENT_STEP_ID = data.id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, supprimer!",
                cancelButtonText: "Annuler"
            }).then(result => {
                if (result.isConfirmed) {
                    deleteDocument();
                }
            });
        });
        function deleteDocument() {
            $("#documents-dt-block").addClass("block-mode-loading");

            $.ajax({
                method: "delete",
                url: `${process.env.MIX_API_URL}/document-steps/${window.CLEARPATH_DOCUMENT_STEP_ID}`,
                success: () => {
                    Swal.fire(
                        "Suppression!",
                        "Document supprimé avec succés!",
                        "success"
                    ).then(() => {
                        dt.api().ajax.reload(null, false);
                    });
                },
                error: () => {
                    Swal.fire(
                        "Suppression!",
                        "erreur suppression document!",
                        "error"
                    ).then(() => {
                        $("#documents-dt-block").removeClass(
                            "block-mode-loading"
                        );
                    });
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents(this.initDataTables());
    }
}

// Initialize when page loads
jQuery(() => {
    leadsFormsListPage.init();
});
