/*
 *  Document   : messages.js
 *  Author     : RR
 *  Description: Custom JS code used in Messages page
 */
window.CLEARPATH_MESSAGE_ID = null

class messagesListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "firstname", "name": "firstname", title: 'Prénom'}, // firstname
            {"data": "lastname", "name": "lastname", title: 'Nom'}, // lastname
            {"data": "message", "name": "message", title: 'Message'}, // objet
            {"data": "email", "name": "email", title: 'Email'}, // email
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                        (action.available ?
                                `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                :
                                ''
                        )
                    )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#messages-dt');
        let blockSelector = $('#messages-dt-block');

        // Init messages DataTable
        let messageTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-search').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/messages`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events
        //TODO put dom events here

        //SEARCH FILTER
        $('.submit-search').on('click', function () {
            messageTable.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            messageTable.api().ajax.reload();
        });

        let messageAddBlockSelector = $('#block-message-add');
        let messageAddFormSelector = $('#message-add-form');

        $('#message-table-add').on('click', function () {

            //hide previous validation errors
            messageAddFormSelector.find('.invalid-feedback').hide();
            messageAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#message-add-form').trigger('reset');
            $('#modal-message-add').modal('show');
        });

        $('#add-message-btn').on('click', function () {

            //hide previous validation errors
            messageAddFormSelector.find('.invalid-feedback').hide();
            messageAddFormSelector.find('.form-control').removeClass('is-invalid');

            messageAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/messages`,
                data: messageAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'Message ajouté avec succés!',
                        'success'
                    ).then(() => {
                        messageAddBlockSelector.removeClass('block-mode-loading');
                        $('#message-add-form').trigger('reset');
                        messageTable.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        messageAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout message!',
                            'error'
                        ).then(() => {
                            messageAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        let messageEditBlockSelector = $('#block-message-edit');
        let messageEditFormSelector = $('#message-edit-form');

        $('body').on('click', '.message-table-edit', function() {

            //hide previous validation errors
            messageEditFormSelector.find('.invalid-feedback').hide();
            messageEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.histocallId = data.id;

            $('#edit-form-customer_id').val(data.customer_id);
            $('#edit-form-object').val(data.object);
            $('#edit-form-message').val(data.message);

            $('#modal-message-edit').modal('show');
        });

        $('#update-message-btn').on('click', function () {

            //hide previous validation errors
            messageEditFormSelector.find('.invalid-feedback').hide();
            messageEditFormSelector.find('.form-control').removeClass('is-invalid');

            messageEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/messages/${window.histocallId}`,
                data: messageEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Message modifié avec succés!',
                        'success'
                    ).then(() => {
                        messageEditBlockSelector.removeClass('block-mode-loading');
                        messageTable.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        messageEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification message!',
                            'error'
                        ).then(() => {
                            messageEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });



        //DOM Events
        //TODO put dom events here
        //DELETE FORM
        $('body').on('click', '.message-table-delete', function() {

            const $row = $(this).closest('tr');
            const data =  $('#messages-dt').DataTable().row($row).data();
            window.CLEARPATH_MESSAGE_ID = data.id;

            Swal.fire({
                title: 'Etes vous sur?',
                text: "L'opération est irréversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteMessage();
                }
            })
        });
        function deleteMessage() {

            $('#messages-dt-block').addClass('block-mode-loading');

            $.ajax({
                method: 'delete',
                url: `${process.env.MIX_API_URL}/messages/${window.CLEARPATH_MESSAGE_ID}`,
                success: () => {
                    Swal.fire(
                        'Suppression!',
                        'Message supprimé avec succés!',
                        'success'
                    ).then(() => {
                        messageTable.api().ajax.reload(null, false);
                    })
                },
                error: () => {
                    Swal.fire(
                        'Suppression!',
                        'erreur suppression message!',
                        'error'
                    ).then(() => {
                        $('#messages-dt-block').removeClass('block-mode-loading');
                    })
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { messagesListPage.init(); });
