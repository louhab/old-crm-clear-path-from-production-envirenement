/*
 *  Document   : campaigns.js
 *  Author     : RR feat NTX
 *  Description: Custom JS code used in campaigns page
 */

class schoollevelsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "school_level_name", "name": "school_level_name", title: 'Niveau'}, // nom
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                        (action.available ?
                                `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                :
                                ''
                        )
                    )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#schoollevels-dt');
        let blockSelector = $('#schoollevels-dt-block');

        // Init schoollevels DataTable
        let schoollevelsTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-s-earch').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/schoollevels`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events
        //TODO put dom events here

        //SEARCH FILTER
        $('.submit-search').on('click', function () {
            schoollevelsTable.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            schoollevelsTable.api().ajax.reload();
        });

        //ADD FORM
        let schoollevelAddBlockSelector = $('#block-schoollevel-add');
        let schoollevelAddFormSelector = $('#schoollevel-add-form');

        $('#schoollevel-table-add').on('click', function () {

            //hide previous validation errors
            schoollevelAddFormSelector.find('.invalid-feedback').hide();
            schoollevelAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#schoollevel-add-form').trigger('reset');
            $('#modal-schoollevel-add').modal('show');
        });

        $('#add-schoollevel-btn').on('click', function () {

            //hide previous validation errors
            schoollevelAddFormSelector.find('.invalid-feedback').hide();
            schoollevelAddFormSelector.find('.form-control').removeClass('is-invalid');

            schoollevelAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/schoollevels`,
                data: schoollevelAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'niveau scolaire ajouté avec succés!',
                        'success'
                    ).then(() => {
                        schoollevelAddBlockSelector.removeClass('block-mode-loading');
                        $('#schoollevel-add-form').trigger('reset');
                        schoollevelsTable.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        schoollevelAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout schoollevel!',
                            'error'
                        ).then(() => {
                            schoollevelAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //UPDATE FORM
        let schoollevelEditBlockSelector = $('#block-schoollevel-edit');
        let schoollevelEditFormSelector = $('#schoollevel-edit-form');

        $('body').on('click', '.schoollevel-table-edit', function() {

            //hide previous validation errors
            schoollevelEditFormSelector.find('.invalid-feedback').hide();
            schoollevelEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.schoollevelId = data.id;

            //set schoollevel edit form data
            $('#edit-form-school_level_name').val(data.school_level_name);

            $('#modal-schoollevel-edit').modal('show');
        });

        $('#update-schoollevel-btn').on('click', function () {

            //hide previous validation errors
            schoollevelEditFormSelector.find('.invalid-feedback').hide();
            schoollevelEditFormSelector.find('.form-control').removeClass('is-invalid');

            schoollevelEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/schoollevels/${window.schoollevelId}`,
                data: schoollevelEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Niveau scolaire modifié avec succés!',
                        'success'
                    ).then(() => {
                        schoollevelEditBlockSelector.removeClass('block-mode-loading');
                        schoollevelsTable.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        schoollevelEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification niveau scolaire!',
                            'error'
                        ).then(() => {
                            schoollevelEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //DELETE FORM
        $('body').on('click', '.schoollevel-table-delete', function() {

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.schoollevelId = data.id;

            Swal.fire({
                title: 'Etes vous sur?',
                text: "L'opération est irréversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteschoollevel();
                }
            })
        });

        function deleteschoollevel() {

            blockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'delete',
                url: `${process.env.MIX_API_URL}/schoollevels/${window.schoollevelId}`,
                success: () => {
                    Swal.fire(
                        'Suppression!',
                        'Niveau scolaire supprimé avec succés!',
                        'success'
                    ).then(() => {
                        schoollevelsTable.api().ajax.reload(null, false);
                    })
                },
                error: () => {
                    Swal.fire(
                        'Suppression!',
                        'erreur suppression niveau scolaire!',
                        'error'
                    ).then(() => {
                        blockSelector.removeClass('block-mode-loading');
                    })
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { schoollevelsListPage.init(); });
