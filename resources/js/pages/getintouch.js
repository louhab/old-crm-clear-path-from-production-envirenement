/*
 *  Document   : getintouch.js
 *  Author     : RR
 *  Description: Custom JS code used in getintouch page
 */

class getInTouchPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDOMEvents() {
        var request_pending = true;
        //DOM Events
        $('#add-lead-btn').on('click', function(){
            if(request_pending)
                $('#lead-form').submit();
            request_pending = true;
        });
        /*$('.custom-file-input').on('change', function() {
            let fileName = $(this).val().split('\\').pop();
            $('#label-cv-input').addClass("selected").html(fileName);
        });*/
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents();
    }
}

// Initialize when page loads
jQuery(() => { getInTouchPage.init(); });
