/*
 *  Document   : cases.js
 *  Author     : RR
 *  Description: Custom JS code used in cases page
 */

class casesListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "customer.firstname", "name": "customer.firstname", title: 'Prénom'}, // firstname
            {"data": "customer.lastname", "name": "customer.lastname", title: 'Nom'}, // lastname
            {"data": "customer.email", "name": "customer.email", title: 'Email'}, // email
            {"data": "program.name", "name": "program.name", title: 'Programme'}, // program.name
            {"data": "step.step_label", "name": "step.step_label", title: 'Avancement', mRender: function (data, type, row) {
                    return `<a href="cases/${row.id}" target="_blank">${data}</a>`
                }, "className" : "font-w600 font-size-sm"
            },//step_label
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                        (action.available ?
                                `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                :
                                ''
                        )
                    )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#cases-dt');
        let blockSelector = $('#cases-dt-block');

        // Init cases DataTable
        let casesTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-search').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'desc']],
            ajax: {
                type: "POST",
                url: `${process.env.MIX_API_URL}/cases`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events
        //TODO put dom events here

        $('#delete-cv-href').on('click', function(event) {
            event.preventDefault();
            $('#delete-form').submit();
        })

        $('.custom-file-input').on('change', function() {
            let fileName = $(this).val().split('\\').pop();
            $('#label-cv-input').addClass("selected").html(fileName);
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        console.log('init')
        this.initDataTables()
    }
}

// Initialize when page loads
jQuery(() => { casesListPage.init(); });
