import intlTelInput from 'intl-tel-input';
//import utilsScript from 'intl-tel-input';

//window.intlTelInput = intlTelInput;
//window.utilsScript = utilsScript;

//console.log(node_modules/intl-tel-input/build/js/utils.js);
// var inputs = document.querySelectorAll('input[type="tel"]');
// for (var i = 0, element; element = inputs[i]; i++) {
//     //work with element
//     var errorMsg = document.querySelector("#phone-error");
//     var validMsg = document.querySelector("#valid-msg");
//     var iti = intlTelInput(element, {
//         /* utilsScript: 'build/js/utils.js', */
//         autoPlaceholder: 'polite',
//         preferredCountries: ['ma', 'ca', 'fr', 'us', 'gb'],
//         utilsScript: '/js/utils.js'
//     });
//     var reset = function() {
//         //element.classList.remove("error");
//         errorMsg.innerHTML = "";
//         errorMsg.classList.add("hide");
//         validMsg.classList.add("hide");
//     };
//     // on blur: validate
//     element.addEventListener('blur', function() {
//         reset();
//         if (element.value.trim()) {
    //             if (iti.isValidNumber()) {
        //                 validMsg.classList.remove("hide");
        //             } else {
            //                 element.classList.add("error");
            //                 var errorCode = iti.getValidationError();
            //                 errorMsg.innerHTML = errorMap[errorCode];
            //                 errorMsg.classList.remove("hide");
            //             }
            //         }
            //     });
            //     // on keyup / change flag: reset
            //     element.addEventListener('change', reset);
            //     element.addEventListener('keyup', reset);
            // }


            // here, the index maps to the error code returned from getValidationError - see readme

            // initialise plugin
            // var iti = window.intlTelInput(input, {
                //   utilsScript: "../../build/js/utils.js?1613236686837"
                // });

                // var reset = function() {
                    //   input.classList.remove("error");
//   errorMsg.innerHTML = "";
//   errorMsg.classList.add("hide");
//   validMsg.classList.add("hide");
// };
var setIntlTel =function(inputID){
    var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
    var errorMsg = document.querySelector("#"+inputID+"-error");
    var validMsg = document.querySelector("#valid-msg");

    //var inputID = "phone";
    var input = document.querySelector('#' + inputID);
    var iti = intlTelInput(input, {
        /* utilsScript: 'build/js/utils.js', */
        nationalMode: false,
        formatOnDisplay: false,
        //autoPlaceholder: false,
        preferredCountries: ['ma', 'ca', 'fr'],
        utilsScript: '/js/utils.js'
    });
    var reset = function() {
        input.classList.remove("is-invalid");
        input.setAttribute("aria-invalid", "false");
        errorMsg.innerHTML = "";
        errorMsg.style.display = "none";
        validMsg.style.display = "none";
    };
    var validate = function() {
        reset();
        let value = input.value.trim();
        if (value) {
            // console.log(value.startsWith('+2120'));
            if (value.startsWith('+2120')){
                console.log(value);
                errorMsg.innerHTML = '<input type="hidden" name="' + inputID + '_error" value="Too long">';
            } else if (value.includes("-")){
                errorMsg.innerHTML = '<input type="hidden" name="' + inputID + '_error" value="Invalid number">';
            }
            else if (iti.isValidNumber()) {
                validMsg.style.display = "block";
            } else {
                //input.classList.add("is-invalid");
                var errorCode = iti.getValidationError();
                errorMsg.innerHTML = '<input type="hidden" name="' + inputID + '_error" value="'+errorMap[errorCode]+'">';
                //console.log(errorMap[errorCode]);
                //input.setAttribute("aria-invalid", "true");
                //errorMsg.style.display = 'block';
            }
        }
    };
    // on blur: validate
    input.addEventListener('blur', validate);
    input.closest('form').addEventListener('submit', validate);
    // $('#customer-form')

}

window.setIntlTel = setIntlTel;
// // on keyup / change flag: reset
// input.addEventListener('change', reset);
// input.addEventListener('keyup', reset);
