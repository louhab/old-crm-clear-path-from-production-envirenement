/*
 *  Document   : bills.js
 *  Author     : RR
 *  Description: Custom JS code used in bills page
 */

window.CLEARPATH_BILL_ID = null;
window.CLEARPATH_CUSTOMER_ID = null;
window.CLEARPATH_SERVICES = null;

class billsPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info:
                    "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            { data: "id", name: "id", visible: false }, // Id
            {
                data: "customer",
                name: "customer",
                title: "Client",
                mRender: function(data, type, row) {
                    return `${data.firstname} ${data.lastname}`;
                }
            }, // customer name
            {
                data: "amount_sum",
                name: "amount_sum",
                title: "Prix Total",
                className: "font-w600 font-size-sm"
            }, // total price
            { data: "user.name", name: "user.name", title: "Crée Par" }, // creator (user)

            {
                data: "actions",
                className: "text-center",
                title: "action",
                width: "150px",
                mRender: function(data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                                    action.available
                                        ? `<div class="btn-group" style="margin:5px">
                                                <a href="${
                                                    action.downloadable
                                                        ? `/pdf/bill/${row.id}`
                                                        : "javascript:void(0)"
                                                }" title="${
                                              action.title
                                          }" class="${
                                              action.className
                                          }" data-id="${row.id}"><i class="${
                                              action.icon
                                          }"></i></a>
                                            </div>`
                                        : ""
                                )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#bills-dt");
        let blockSelector = $("#bills-dt-block");

        // Init countries DataTable
        let billsTable = tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[0, "desc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/bills`,
                    data: function(d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function(index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    }
                },
                columns: columns,
                fnDrawCallback: function() {
                    blockSelector.removeClass("block-mode-loading");
                    $(".submit-search").prop("disabled", false);
                },
                columnDefs: [
                    {
                        /*targets: [2],
                orderable: false,
                searchable: false*/
                    }
                ]
            });

        return billsTable;
    }

    /*
     * Init DOM Events
     *
     */
    static initDOMEvents(dt) {
        //DOM Events

        $("#show-services-student").on("click", () => {
            window.CLEARPATH_BILL_ID = null;
            window.CLEARPATH_CUSTOMER_ID = null;
            window.CLEARPATH_SERVICES = null;

            $("#customer-select-wrapper").show();
            $("#services-table").removeClass("edit-customer-bill");
            this.showServicesForStudent();
        });

        $("#show-services-immigrant").on("click", () => {
            window.CLEARPATH_BILL_ID = null;
            window.CLEARPATH_CUSTOMER_ID = null;
            window.CLEARPATH_SERVICES = null;

            $("#customer-select-wrapper").show();
            $("#services-table").removeClass("edit-customer-bill");
            this.showServicesForImmigrant();
        });

        $("#pt-select").on("change", () => {
            //load services
            this.loadServices(
                2,
                window.CLEARPATH_SERVICES ? window.CLEARPATH_SERVICES : []
            );
        });

        $("#save-op").on("click", function() {
            if (
                (window.CLEARPATH_BILL_ID && window.CLEARPATH_CUSTOMER_ID) ||
                ($("#customer-select").val() &&
                    !window.CLEARPATH_BILL_ID &&
                    !window.CLEARPATH_CUSTOMER_ID)
            ) {
                if ($("#services-table tr.is-selected").length) {
                    let billData = {
                        cid: $("#services-table").hasClass("edit-customer-bill")
                            ? window.CLEARPATH_CUSTOMER_ID
                            : $("#customer-select").val(),
                        services: []
                    };

                    if ($("#pt-select-wrapper").is(":visible"))
                        billData.ap = $("#pt-select").val();

                    $("#services-table tr.is-selected").each(function(idx) {
                        billData.services.push([
                            $(this).attr("data-serviceId"),
                            $(this).attr("data-initial-billed-unit-price"),
                            $(this).attr("data-qte")
                        ]);
                    });
                    // console.log(billData);
                    $.ajax({
                        method: $("#services-table").hasClass(
                            "edit-customer-bill"
                        )
                            ? "put"
                            : "post",
                        url: `${process.env.MIX_API_URL}/bills${
                            $("#services-table").hasClass("edit-customer-bill")
                                ? `/${window.CLEARPATH_BILL_ID}`
                                : ""
                        }`,
                        data: billData,
                        success: () => {
                            Swal.fire(
                                "Facture",
                                "Facture crée avec succés.",
                                "success"
                            ).then(() => {
                                $("#modal-services").modal("hide");

                                dt.api().ajax.reload();
                            });
                        },
                        error: xhr => {}
                    });
                } else {
                    Swal.fire(
                        "Facture!",
                        "Veuillez Sélectionnez au moins un service.",
                        "warning"
                    );
                }
            } else {
                Swal.fire(
                    "Facture!",
                    "Veuillez Sélectionnez un client.",
                    "warning"
                );
            }
        });

        $("#cid").on("change", () => {
            dt.api().ajax.reload();
        });

        $("body").on("click", ".service-select-checkbox", function(e) {
            if ($(this).is(":checked")) {
                $(this)
                    .closest("tr")
                    .addClass("is-selected");
            } else {
                $(this)
                    .closest("tr")
                    .removeClass("is-selected");
            }
        });

        $("body").on("change", ".service-qte", function() {
            let priceTD = $(this)
                .closest(".td-qte")
                .prev(".td-price");

            if ($(this).val() > 0) {
                let price = (
                    parseFloat(priceTD.data("unit-price")) * $(this).val()
                ).toFixed(2);
                priceTD.html(price);
                $(this)
                    .closest("tr")
                    .attr("data-billed-unit-price", price);
                $(this)
                    .closest("tr")
                    .attr("data-qte", $(this).val());
            } else {
            }
        });

        $("body").on("click", ".bill-table-edit", e => {
            $("#customer-select-wrapper").hide();

            $("#services-table").addClass("edit-customer-bill");

            const $row = $(e.target).closest("tr");
            const data = dt
                .DataTable()
                .row($row)
                .data();

            window.CLEARPATH_BILL_ID = data.id;
            window.CLEARPATH_CUSTOMER_ID = data.customer_id;
            window.CLEARPATH_SERVICES = data.services;

            console.log("window.CLEARPATH_SERVICES", window.CLEARPATH_SERVICES);

            console.log("data.customer.program_id", data.customer.program_id);

            if (data.customer.program_id == 1) {
                $("#pt-select-wrapper").hide();
                this.showServicesForStudent(window.CLEARPATH_SERVICES);
            } else {
                $("#pt-select").val(`${data.applied_price}`);
                $("#pt-select-wrapper").show();
                this.showServicesForImmigrant(window.CLEARPATH_SERVICES);
            }
        });

        $("#cid").select2({
            placeholder: "Séléctionnez un client",
            allowClear: true,

            ajax: {
                url: `${process.env.MIX_API_URL}/getCustomers`,

                dataType: "json",

                delay: 250,

                method: "POST",

                data: function(term, page) {
                    return {
                        q: term
                        //pid: pid
                    };
                },

                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.name,

                                id: item.id
                            };
                        })
                    };
                },

                cache: true
            }
        });
        //DELETE Bill
        $("body").on("click", ".bill-table-delete", function() {
            const $row = $(this).closest("tr");
            const data = $("#bills-dt")
                .DataTable()
                .row($row)
                .data();
            window.CLEARPATH_BILL_ID = data.id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, supprimer!",
                cancelButtonText: "Annuler"
            }).then(result => {
                if (result.isConfirmed) {
                    deleteBill();
                }
            });
        });
        function deleteBill() {
            $("#bills-dt-block").addClass("block-mode-loading");

            $.ajax({
                method: "delete",
                url: `${process.env.MIX_API_URL}/bills/${window.CLEARPATH_BILL_ID}`,
                success: () => {
                    Swal.fire(
                        "Suppression!",
                        "Bill supprimé avec succés!",
                        "success"
                    ).then(() => {
                        dt.api().ajax.reload(null, false);
                    });
                },
                error: () => {
                    Swal.fire(
                        "Suppression!",
                        "erreur suppression bill!",
                        "error"
                    ).then(() => {
                        $("#bills-dt-block").removeClass("block-mode-loading");
                    });
                }
            });
        }
    }

    static loadServices(pid, services = []) {
        console.log("pid", pid);
        let data = { pid: pid };

        let pt = $("#pt-select").val();

        $("#block-services").addClass("block-mode-loading");
        $.ajax({
            method: "post",
            url: `${process.env.MIX_API_URL}/getservices`,
            data: data,
            success: data => {
                $("#services-table-body").html(
                    data.data.map(function(service) {
                        return `<tr class="${
                            services.length == 0 ||
                            (services.length > 0 &&
                                services.some(el => el.id === service.id))
                                ? "is-selected"
                                : ""
                        }" data-serviceId="${service.id}" data-initial-billed-unit-price="${pid == 2 && pt == "2" ? service.couple_price : service.single_price}" data-billed-unit-price="${pid == 2 && pt == "2" ? service.couple_price : service.single_price}" data-qte="1">
                                        <td>Phase 1</td>
                                        <td class="font-size-sm">
                                            ${
                                                !service.details.length
                                                    ? service.name
                                                    : service.details
                                                          .map(function(
                                                              detail
                                                          ) {
                                                              return `${detail.name} <br>`;
                                                          })
                                                          .join("")
                                            }
                                        </td>
                                        <td class="td-price" data-unit-price="${
                                            pid == 2 && pt == "2"
                                                ? service.couple_price
                                                : service.single_price
                                        }">
                                            ${
                                                pid == 2 && pt == "2"
                                                    ? services.length > 0 &&
                                                      services.some(
                                                          el =>
                                                              el.id ===
                                                              service.id
                                                      )
                                                        ? services.find(
                                                              o =>
                                                                  o.id ===
                                                                  service.id
                                                          ).pivot.qte *
                                                          service.couple_price
                                                        : service.couple_price
                                                    : services.length > 0 &&
                                                      services.some(
                                                          el =>
                                                              el.id ===
                                                              service.id
                                                      )
                                                    ? services.find(
                                                          o =>
                                                              o.id ===
                                                              service.id
                                                      ).pivot.qte *
                                                      service.single_price
                                                    : service.single_price
                                            }
                                        </td>
                                        <td class="text-center td-qte">
                                            ${
                                                service.can_edit_qte
                                                    ? `<input type="number" class="form-control form-control-sm service-qte" id="example-text-input-sm${
                                                          service.id
                                                      }" name="example-text-input-sm${
                                                          service.id
                                                      }" min="1" value="${
                                                          services.length > 0 &&
                                                          services.some(
                                                              el =>
                                                                  el.id ===
                                                                  service.id
                                                          )
                                                              ? services.find(
                                                                    o =>
                                                                        o.id ===
                                                                        service.id
                                                                ).pivot.qte
                                                              : 1
                                                      }" placeholder="Quantité">`
                                                    : "1"
                                            }
                                        </td>
                                        <td class="text-center">
                                            <div class="custom-control custom-switch custom-control-success mb-1">
                                                <input type="checkbox" class="custom-control-input service-select-checkbox" id="example-sw-custom${
                                                    service.id
                                                }" name="example-sw-custom${service.id}" ${services.length == 0 || (services.length > 0 && services.some(el => el.id === service.id)) ? 'checked=""' : ""}>
                                                <label class="custom-control-label" for="example-sw-custom${
                                                    service.id
                                                }"></label>
                                            </div>
                                        </td>
                                    </tr>`;
                    })
                );
                $("#block-services").removeClass("block-mode-loading");
            },
            error: xhr => {
                //console.log(xhr.responseJSON);
                $("#block-services").removeClass("block-mode-loading");
            }
        });
    }

    static loadCustomers(pid) {
        $("#customer-select")
            .select2({})
            .empty();
        $("#customer-select").select2({
            placeholder: "Séléctionnez un client",

            ajax: {
                url: `${process.env.MIX_API_URL}/getCustomers`,

                dataType: "json",

                delay: 250,

                method: "POST",

                data: function(term, page) {
                    return {
                        q: term,
                        pid: pid
                    };
                },

                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.name,

                                id: item.id
                            };
                        })
                    };
                },

                cache: false
            }
        });
    }

    static showServicesForStudent(services = []) {
        $("#customer-select").show();
        $("#pt-select-wrapper").hide();
        $("#modal-services").modal("show");
        $("#block-services").addClass("block-mode-loading");

        //load customers
        if (!$("#services-table").hasClass("edit-customer-bill"))
            this.loadCustomers(4);

        //load services
        this.loadServices(1, services);
    }

    static showServicesForImmigrant(services = []) {
        $("#customer-select").show();
        $("#pt-select-wrapper").show();
        $("#modal-services").modal("show");
        $("#block-services").addClass("block-mode-loading");

        //load customers
        if (!$("#services-table").hasClass("edit-customer-bill"))
            this.loadCustomers(2);

        //load services
        this.loadServices(2, services);
    }

    /*
     * Init functionality
     *
     */
    static init() {
        let dt = this.initDataTables();
        this.initDOMEvents(dt);
    }
}

// Initialize when page loads
jQuery(() => {
    billsPage.init();
});
