/*
 *  Document   : customer-quotes.js
 *  Author     : RR
 *  Description: Custom JS code used in customer quotes page
 */

class customerQuotesPage {
    /*
     * Init DOM Events
     *
     */
    static initDOMEvents() {

        //DOM Events
        //TODO put dom events here

        console.log('customerQuotesPage initDOMEvents')
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents();
    }
}

// Initialize when page loads
jQuery(() => { customerQuotesPage.init(); });
