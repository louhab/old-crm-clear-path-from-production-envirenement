/*
 *  Document   : customer-bills.js
 *  Author     : RR
 *  Description: Custom JS code used in customer bills page
 */

class customerBillsPage {
    /*
     * Init DOM Events
     *
     */
    static initDOMEvents() {

        //DOM Events
        //TODO put dom events here

        console.log('customerBillsPage initDOMEvents')
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents();
    }
}

// Initialize when page loads
jQuery(() => { customerBillsPage.init(); });
