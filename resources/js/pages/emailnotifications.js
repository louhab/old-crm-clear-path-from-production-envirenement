/*
 *  Document   : emailnotifications.js
 *  Author     : RR
 *  Description: Custom JS code used in Email Notifications page
 */
window.CLEARPATH_MESSAGE_ID = null

class emailnotificationsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "object", "name": "object", title: 'Object'}, // email
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                        (action.available ?
                                `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                :
                                ''
                        )
                    )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#emailnotifications-dt');
        let blockSelector = $('#emailnotifications-dt-block');

        // Init emailnotifications DataTable
        let emailnotificationTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-search').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/emailnotifications`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events
        //TODO put dom events here

        //SEARCH FILTER
        $('.submit-search').on('click', function () {
            emailnotificationTable.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            emailnotificationTable.api().ajax.reload();
        });

        let emailnotificationAddBlockSelector = $('#block-emailnotification-add');
        let emailnotificationAddFormSelector = $('#emailnotification-add-form');

        $('#emailnotification-table-add').on('click', function () {

            //hide previous validation errors
            emailnotificationAddFormSelector.find('.invalid-feedback').hide();
            emailnotificationAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#emailnotification-add-form').trigger('reset');
            $('#modal-emailnotification-add').modal('show');
        });

        $('#add-emailnotification-btn').on('click', function () {

            //hide previous validation errors
            emailnotificationAddFormSelector.find('.invalid-feedback').hide();
            emailnotificationAddFormSelector.find('.form-control').removeClass('is-invalid');

            emailnotificationAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/emailnotifications`,
                data: emailnotificationAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'Email Notification ajouté avec succés!',
                        'success'
                    ).then(() => {
                        emailnotificationAddBlockSelector.removeClass('block-mode-loading');
                        $('#emailnotification-add-form').trigger('reset');
                        emailnotificationTable.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        emailnotificationAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout emailnotification!',
                            'error'
                        ).then(() => {
                            emailnotificationAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        let emailnotificationEditBlockSelector = $('#block-emailnotification-edit');
        let emailnotificationEditFormSelector = $('#emailnotification-edit-form');

        $('body').on('click', '.emailnotification-table-edit', function() {

            //hide previous validation errors
            emailnotificationEditFormSelector.find('.invalid-feedback').hide();
            emailnotificationEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.histocallId = data.id;

            $('#edit-form-object').val(data.object);
            $('#edit-form-message').val(data.message);

            $('#modal-emailnotification-edit').modal('show');
        });

        $('#update-emailnotification-btn').on('click', function () {

            //hide previous validation errors
            emailnotificationEditFormSelector.find('.invalid-feedback').hide();
            emailnotificationEditFormSelector.find('.form-control').removeClass('is-invalid');

            emailnotificationEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/emailnotifications/${window.histocallId}`,
                data: emailnotificationEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Email Notification modifié avec succés!',
                        'success'
                    ).then(() => {
                        emailnotificationEditBlockSelector.removeClass('block-mode-loading');
                        emailnotificationTable.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        emailnotificationEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification email notification!',
                            'error'
                        ).then(() => {
                            emailnotificationEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });



        //DOM Events
        //TODO put dom events here
        //DELETE FORM
        $('body').on('click', '.emailnotification-table-delete', function() {

            const $row = $(this).closest('tr');
            const data =  $('#emailnotifications-dt').DataTable().row($row).data();
            window.CLEARPATH_MESSAGE_ID = data.id;

            Swal.fire({
                title: 'Etes vous sur?',
                text: "L'opération est irréversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteEmailNotification();
                }
            })
        });
        function deleteEmailNotification() {

            $('#emailnotifications-dt-block').addClass('block-mode-loading');

            $.ajax({
                method: 'delete',
                url: `${process.env.MIX_API_URL}/emailnotifications/${window.CLEARPATH_MESSAGE_ID}`,
                success: () => {
                    Swal.fire(
                        'Suppression!',
                        'Email Notification supprimé avec succés!',
                        'success'
                    ).then(() => {
                        emailnotificationTable.api().ajax.reload(null, false);
                    })
                },
                error: () => {
                    Swal.fire(
                        'Suppression!',
                        'erreur suppression email notification!',
                        'error'
                    ).then(() => {
                        $('#emailnotifications-dt-block').removeClass('block-mode-loading');
                    })
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { emailnotificationsListPage.init(); });
