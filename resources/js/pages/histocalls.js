/*
 *  Document   : histocalls.js
 *  Author     : RR
 *  Description: Custom JS code used in histocalls page
 */

class histocallsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        var lang = $('input[name="lang"]').val();
        if (lang == null) {
            // console.log("lalalal");
            lang = "fr";
        }
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm",
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>',
                },
            },
        });

        let columns = [
            { data: "id", name: "id", visible: false }, // Id
            {
                data: "customer.firstname",
                name: "firstname",
                title: lang === "fr" ? "Prénom" : "Name",
            }, // firstname
            {
                data: "customer.lastname",
                name: "lastname",
                title: lang === "fr" ? "Nom" : "Surename",
            }, // lastname
            { data: "customer.email", name: "email", title: "Email" }, // email
            { data: "call_dt", name: "call_dt", title: "Date" }, // date
            {
                data: "duration",
                name: "duration",
                title: lang === "fr" ? "Durée" : "Duration",
            }, // duration
            {
                data: "actions",
                className: "text-center",
                title: "action",
                width: "150px",
                mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map((action) =>
                                    action.available
                                        ? `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                        : ""
                                )}
                            </div>`;
                },
            },
        ];

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#histocalls-dt");
        let blockSelector = $("#histocalls-dt-block");

        // Init histocalls DataTable
        let histocallsTable = tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.${lang}.l10n.json`,
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[3, "desc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/histocalls`,
                    data: function (d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function (index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    },
                },
                columns: [
                    { data: "id", name: "id", visible: false }, // Id
                    {
                        data: "customer.firstname",
                        render: function (data, type, row) {
                            if (row.customer_id == null) {
                                return row.lfirstname + " " + row.llastname;
                            }
                            if (row.lead_id == null) {
                                return row.cfirstname + " " + row.clastname;
                            }
                        },
                        name: "cfirstname",
                        title: lang === "fr" ? "Prénom" : "Name",
                    }, // firstname
                    {
                        data: "username",
                        name: "username",
                        title: lang === "fr" ? "Fait Par" : "Made by",
                        visible: window.userRole.role_id == 3 ? false : true,
                    }, // date
                    { data: "call_dt", name: "call_dt", title: "Date" }, // date
                    { data: "call_descr", name: "call_descr", title: "Note" }, // description
                    {
                        data: "duration",
                        name: "duration",
                        title: lang === "fr" ? "Durée" : "Duration",
                        mRender: function (data, type, row) {
                            var minutes = Math.floor(row.duration / 60);
                            var seconds = row.duration - minutes * 60;
                            /*var hours = Math.floor(time / 3600);
                        time = time - hours * 3600;*/
                            function str_pad_left(string, pad, length) {
                                return (
                                    new Array(length + 1).join(pad) + string
                                ).slice(-length);
                            }

                            var finalTime =
                                minutes + ":" + str_pad_left(seconds, "0", 2);
                            return finalTime;
                        },
                    }, //
                    // {"data": "duration", "name": "duration", title: 'Durée'}, // duration
                    {
                        data: "lead_state_id",
                        name: "lead_state_id",
                        title: "Status actuelle",
                        mRender: function (data, type, row) {
                            let badge = "danger";
                            switch (row.lead_state_id) {
                                case 1:
                                    // code block
                                    badge = "success";
                                    break;
                                case 3:
                                    // code block
                                    badge = "secondary";
                                    break;
                                case 4:
                                    // code block
                                    badge = "info";
                                    break;
                                case 5:
                                    // code block
                                    badge = "warning";
                                    break;
                                // default:
                                //   // code block
                            }
                            return row.lead_state_id
                                ? `<span class="badge badge-${badge}">${row.status_label}</span>`
                                : '<span class="badge badge-info">n/d</span>';
                        },
                    }, // lead state
                    {
                        data: "state_before",
                        name: "state_before",
                        title: "Status avant",
                        mRender: function (data, type, row) {
                            let badge = "danger";
                            switch (row.state_before) {
                                case 1:
                                    // code block
                                    badge = "success";
                                    break;
                                case 3:
                                    // code block
                                    badge = "secondary";
                                    break;
                                case 4:
                                    // code block
                                    badge = "info";
                                    break;
                                case 5:
                                    // code block
                                    badge = "warning";
                                    break;
                                // default:
                                //   // code block
                            }
                            return row.state_before
                                ? `<span class="badge badge-${badge}">${row.status_label_before}</span>`
                                : '<span class="badge badge-info">n/d</span>';
                        },
                    }, // lead state
                    {
                        data: "actions",
                        className: "text-center",
                        title: "action",
                        width: "150px",
                        mRender: function (data, type, row) {
                            return `<div class="d-flex justify-content-center action">
                                ${data.map((action) =>
                                    action.available
                                        ? `<div class="btn-group" style="margin:5px">
                                                <a href="${
                                                    action.islink
                                                        ? `${
                                                              process.env
                                                                  .MIX_PUBLIC_URL
                                                          }/${
                                                              action.title ==
                                                              "afficher"
                                                                  ? "lead"
                                                                  : "leads"
                                                          }/${row.lead_id}`
                                                        : "javascript:void(0)"
                                                }" title="${
                                              action.title
                                          }" class="${
                                              action.className
                                          }" data-id="${row.id}"><i class="${
                                              action.icon
                                          }"></i></a>
                                            </div>`
                                        : ""
                                )}
                            </div>`;
                        },
                    },
                ],
                fnDrawCallback: function () {
                    var api = this.api();
                    var coltosum = [5];
                    for (c in coltosum) {
                        // Total over all pages
                        //console.log(api.column( coltosum[c] ).data())
                        // total = api.column( coltosum[c] ).data().reduce( function (a, b) {return a + b;}, 0 );
                        // Total over this page
                        pageTotal = api
                            .column(coltosum[c], { page: "current" })
                            .data()
                            .reduce(function (a, b) {
                                return a + b;
                            }, 0);
                        // Update footer
                        var minutes = Math.floor(pageTotal / 60);
                        var seconds = pageTotal - minutes * 60;
                        /*var hours = Math.floor(time / 3600);
                    time = time - hours * 3600;*/
                        function str_pad_left(string, pad, length) {
                            return (
                                new Array(length + 1).join(pad) + string
                            ).slice(-length);
                        }

                        var finalTime =
                            minutes + ":" + str_pad_left(seconds, "0", 2);
                        $(api.column(coltosum[c]).footer()).html(finalTime);
                    }
                    blockSelector.removeClass("block-mode-loading");
                    $(".submit-search").prop("disabled", false);
                },
                columnDefs: [
                    {
                        /*targets: [2],
                orderable: false,
                searchable: false*/
                    },
                ],
            });

        //DOM Events
        //TODO put dom events here

        //SEARCH FILTER
        $(".submit-search").on("click", function () {
            histocallsTable.api().ajax.reload();
        });

        $(".clear-search").on("click", function () {
            searchFilterSelector.find(".dt_search_field").val("");
            histocallsTable.api().ajax.reload();
        });

        //ADD FORM
        let histocallLeadAddBlockSelector = $("#block-histocall-lead-add");
        let histocallLeadAddFormSelector = $("#histocall-add-lead-form");

        let histocallCustomerAddBlockSelector = $(
            "#block-histocall-customer-add"
        );
        let histocallCustomerAddFormSelector = $(
            "#histocall-add-customer-form"
        );

        $("#histocall-lead-table-add").on("click", function () {
            //hide previous validation errors
            histocallLeadAddFormSelector.find(".invalid-feedback").hide();
            histocallLeadAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            $("#histocall-add-lead-form").trigger("reset");
            $("#modal-histocall-lead-add").modal("show");
        });
        $("#histocall-customer-table-add").on("click", function () {
            //hide previous validation errors
            histocallCustomerAddFormSelector.find(".invalid-feedback").hide();
            histocallCustomerAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            $("#histocall-add-customer-form").trigger("reset");
            $("#modal-histocall-customer-add").modal("show");
        });

        $("#add-histocall-lead-btn").on("click", function () {
            //hide previous validation errors
            histocallLeadAddFormSelector.find(".invalid-feedback").hide();
            histocallLeadAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            histocallLeadAddBlockSelector.addClass("block-mode-loading");

            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/histocalls`,
                data: histocallLeadAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        "Ajout!",
                        "lead ajouté avec succés!",
                        "success"
                    ).then(() => {
                        histocallLeadAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                        $("#histocall-add-form").trigger("reset");
                        histocallsTable.api().ajax.reload();
                    });
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        histocallLeadAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire("Ajout!", "erreur ajout lead!", "error").then(
                            () => {
                                histocallLeadAddBlockSelector.removeClass(
                                    "block-mode-loading"
                                );
                            }
                        );
                },
            });
        });

        $("#add-histocall-customer-btn").on("click", function () {
            //hide previous validation errors
            histocallCustomerAddFormSelector.find(".invalid-feedback").hide();
            histocallCustomerAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            histocallCustomerAddBlockSelector.addClass("block-mode-loading");

            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/histocalls`,
                data: histocallCustomerAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        "Ajout!",
                        "client ajouté avec succés!",
                        "success"
                    ).then(() => {
                        histocallCustomerAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                        $("#histocall-add-form").trigger("reset");
                        histocallsTable.api().ajax.reload();
                    });
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        histocallCustomerAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur ajout client!",
                            "error"
                        ).then(() => {
                            histocallCustomerAddBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        //UPDATE FORM
        let histocallLeadEditBlockSelector = $("#block-lead-histocall-edit");
        let histocallLeadEditFormSelector = $("#histocall-edit-lead-form");

        let histocallCustomerEditBlockSelector = $(
            "#block-customer-histocall-edit"
        );
        let histocallCustomerEditFormSelector = $(
            "#histocall-edit-customer-form"
        );

        $("body").on("click", ".histocall-table-edit", function () {
            //hide previous validation errors
            histocallLeadEditBlockSelector.find(".invalid-feedback").hide();
            histocallLeadEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            histocallCustomerEditBlockSelector.find(".invalid-feedback").hide();
            histocallCustomerEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            const $row = $(this).closest("tr");
            const data = tableSelector.DataTable().row($row).data();
            window.histocallId = data.id;

            if (data.customer_id == null) {
                $("#edit-form-lead_id").val(data.lead_id);
                $("#edit-form-lead-call_dt").val(data.call_dt.split(" ")[0]);
                $("#edit-form-lead-duration").val(data.duration);
                $("#edit-form-lead-call_descr").val(data.call_descr);
                $("#modal-histocall-lead-edit").modal("show");
            }
            if (data.lead_id == null) {
                $("#edit-form-customer_id").val(data.customer_id);
                $("#edit-form-customer-call_dt").val(
                    data.call_dt.split(" ")[0]
                );
                $("#edit-form-customer-duration").val(data.duration);
                $("#edit-form-customer-call_descr").val(data.call_descr);
                $("#modal-histocall-customer-edit").modal("show");
            }
        });

        $("#update-lead-histocall-btn").on("click", function () {
            //hide previous validation errors
            histocallLeadEditFormSelector.find(".invalid-feedback").hide();
            histocallLeadEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            histocallLeadEditBlockSelector.addClass("block-mode-loading");

            $.ajax({
                method: "put",
                url: `${process.env.MIX_API_URL}/histocalls/${window.histocallId}`,
                data: histocallLeadEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        "Modification!",
                        "Lead modifié avec succés!",
                        "success"
                    ).then(() => {
                        histocallLeadEditBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                        histocallsTable.api().ajax.reload(null, false);
                    });
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        histocallLeadEditBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Modification!",
                            "erreur modification lead!",
                            "error"
                        ).then(() => {
                            histocallLeadEditBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        $("#update-customer-histocall-btn").on("click", function () {
            //hide previous validation errors
            histocallCustomerEditFormSelector.find(".invalid-feedback").hide();
            histocallCustomerEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            histocallCustomerEditBlockSelector.addClass("block-mode-loading");

            $.ajax({
                method: "put",
                url: `${process.env.MIX_API_URL}/histocalls/${window.histocallId}`,
                data: histocallCustomerEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        "Modification!",
                        "Client modifié avec succés!",
                        "success"
                    ).then(() => {
                        histocallCustomerEditBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                        histocallsTable.api().ajax.reload(null, false);
                    });
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        histocallCustomerEditBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Modification!",
                            "erreur modification client!",
                            "error"
                        ).then(() => {
                            histocallCustomerEditBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        //DELETE FORM
        $("body").on("click", ".histocall-table-delete", function () {
            const $row = $(this).closest("tr");
            const data = tableSelector.DataTable().row($row).data();
            window.histocallId = data.id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, supprimer!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteUser();
                }
            });
        });

        function deleteUser() {
            blockSelector.addClass("block-mode-loading");

            $.ajax({
                method: "delete",
                url: `${process.env.MIX_API_URL}/histocalls/${window.histocallId}`,
                success: () => {
                    Swal.fire(
                        "Suppression!",
                        "utilisateur supprimé avec succés!",
                        "success"
                    ).then(() => {
                        histocallsTable.api().ajax.reload(null, false);
                    });
                },
                error: () => {
                    Swal.fire(
                        "Suppression!",
                        "erreur suppression utilisateur!",
                        "error"
                    ).then(() => {
                        blockSelector.removeClass("block-mode-loading");
                    });
                },
            });
        }

        $(".dt_search_agent").on("select2:select", function (e) {
            var data = e.params.data;
            // console.log(e.params.data);
            $('.search-filter input[name="agent-group"]').val(
                e.params.data.group
            );
            histocallsTable.api().ajax.reload(null, false);
        });
        $(".dt_search_agent").on("select2:unselect", function (e) {
            //var data = e.params.data;
            //console.log(e.params.data);
            $('.search-filter input[name="agent-group"]').val("");
            histocallsTable.api().ajax.reload(null, false);
        });
        $(".dt_search_agent").select2({
            placeholder: "Utilisateurs...",
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/agents`,
                dataType: "json",
                delay: 250,
                method: "POST",
                data: function (term, page) {
                    return {
                        q: term,
                    };
                },
                processResults: function (data) {
                    let res = [];
                    // console.log(window.dashboard_filter);
                    $.each(data, function (role, group) {
                        let groupObj = {
                            text: role,
                        };
                        let children = [];
                        // console.log(role, group);
                        $.each(group, function (idx, user) {
                            // if(user.name)
                            let child = {
                                text: user.name,
                                id: user.id,
                                group: role,
                            };
                            /*if ("agent" in window.dashboard_filter && window.dashboard_filter.agent == user.id ) {
                                // console.log(window.dashboard_filter);
                                child["selected"] = true;
                            }*/
                            children.push(child);
                        });
                        groupObj["children"] = children;
                        res.push(groupObj);
                    });
                    //console.log(res);
                    return {
                        results: res,
                    };
                },
                initSelection: function (element, callback) {},
                cache: true,
            },
        });
        // sum duration
        $(document).ready(function () {
            /*let finalTime = histocallsTable.api().column( 5 ).data().sum();
            $('#durations-sum').html(
                lang === "fr"
                    ? "Durée des appels: " + finalTime
                    : "Calls duration: " + finalTime
            );*/
            //histocallsTable.api().column( 5 ).data().sum());
        });
        $("#histocalls-dt").on("xhr.dt", function (e, settings, json, xhr) {
            var minutes = Math.floor(json.total / 60);
            var seconds = json.total - minutes * 60;
            /*var hours = Math.floor(time / 3600);
                time = time - hours * 3600;*/
            function str_pad_left(string, pad, length) {
                return (new Array(length + 1).join(pad) + string).slice(
                    -length
                );
            }

            var finalTime = minutes + ":" + str_pad_left(seconds, "0", 2);
            $("#durations-sum").html(
                lang === "fr"
                    ? "Durée des appels: " + finalTime
                    : "Calls duration: " + finalTime
            );
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => {
    histocallsListPage.init();
});
