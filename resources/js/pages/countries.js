/*
 *  Document   : countries.js
 *  Author     : RR
 *  Description: Custom JS code used in countries page
 */

class countriesListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "name_fr_fr", "name": "name_fr_fr", title: 'Nom FR'}, // name_fr_fr
            {"data": "name_en_gb", "name": "name_en_gb", title: 'Nom EN'}, // name_en_gb
            {"data": "code", "name": "code", title: 'Code'}, // code
            {"data": "alpha2", "name": "alpha2", title: 'Code Alpha 2'}, // alpha2
            {"data": "alpha3", "name": "alpha3", title: 'Code Alpha 3'}, // alpha3
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                        (action.available ?
                                `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                :
                                ''
                        )
                    )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#countries-dt');
        let blockSelector = $('#countries-dt-block');

        // Init countries DataTable
        let countriesTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-search').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/countries`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events
        //TODO put dom events here

        //SEARCH FILTER
        $('.submit-search').on('click', function () {
            countriesTable.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            countriesTable.api().ajax.reload();
        });

        //ADD FORM
        let countryAddBlockSelector = $('#block-country-add');
        let countryAddFormSelector = $('#country-add-form');

        $('#country-table-add').on('click', function () {

            //hide previous validation errors
            countryAddFormSelector.find('.invalid-feedback').hide();
            countryAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#country-add-form').trigger('reset');
            $('#modal-country-add').modal('show');
        });

        $('#add-country-btn').on('click', function () {

            //hide previous validation errors
            countryAddFormSelector.find('.invalid-feedback').hide();
            countryAddFormSelector.find('.form-control').removeClass('is-invalid');

            countryAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/countries`,
                data: countryAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'Pays ajouté avec succés!',
                        'success'
                    ).then(() => {
                        countryAddBlockSelector.removeClass('block-mode-loading');
                        $('#country-add-form').trigger('reset');
                        countriesTable.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        countryAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout pays!',
                            'error'
                        ).then(() => {
                            countryAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //UPDATE FORM
        let countryEditBlockSelector = $('#block-country-edit');
        let countryEditFormSelector = $('#country-edit-form');

        $('body').on('click', '.country-table-edit', function() {

            //hide previous validation errors
            countryEditFormSelector.find('.invalid-feedback').hide();
            countryEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.countryId = data.id;

            //set country edit form data
            $('#edit-form-code').val(data.code);
            $('#edit-form-alpha2').val(data.alpha2);
            $('#edit-form-alpha3').val(data.alpha3);
            $('#edit-form-name_fr_fr').val(data.name_fr_fr);
            $('#edit-form-name_en_gb').val(data.name_en_gb);

            $('#modal-country-edit').modal('show');
        });

        $('#update-country-btn').on('click', function () {

            //hide previous validation errors
            countryEditFormSelector.find('.invalid-feedback').hide();
            countryEditFormSelector.find('.form-control').removeClass('is-invalid');

            countryEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/countries/${window.countryId}`,
                data: countryEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Pays modifié avec succés!',
                        'success'
                    ).then(() => {
                        countryEditBlockSelector.removeClass('block-mode-loading');
                        countriesTable.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        countryEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification pays!',
                            'error'
                        ).then(() => {
                            countryEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //DELETE FORM
        $('body').on('click', '.country-table-delete', function() {

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.countryId = data.id;

            Swal.fire({
                title: 'Etes vous sur?',
                text: "L'opération est irréversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteCountry();
                }
            })
        });

        function deleteCountry() {

            blockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'delete',
                url: `${process.env.MIX_API_URL}/countries/${window.countryId}`,
                success: () => {
                    Swal.fire(
                        'Suppression!',
                        'Pays supprimé avec succés!',
                        'success'
                    ).then(() => {
                        countriesTable.api().ajax.reload(null, false);
                    })
                },
                error: () => {
                    Swal.fire(
                        'Suppression!',
                        'Erreur suppression pays!',
                        'error'
                    ).then(() => {
                        blockSelector.removeClass('block-mode-loading');
                    })
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { countriesListPage.init(); });
