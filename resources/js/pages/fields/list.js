/*
 *  Document   : leadsforms.js
 *  Author     : RR
 *  Description: Custom JS code used in leadsforms page
 */
window.CLEARPATH_FIELD_ID = null;
class leadsFormsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info:
                    "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        /*let columns = [
            { data: "id", name: "id", visible: false }, // Id
            //{"data": "user.name", "name": "name", title: 'Crée par'}, // name
            {
                data: "campaign.campaign_name",
                name: "campaign_name",
                title: "Campagne"
            }, // campaign_name
            { data: "active", name: "active", title: "active" }, // active
            {
                data: "actions",
                className: "text-center",
                title: "action",
                width: "150px",
                mRender: function(data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action => {
                                    if (action.islink) {
                                        if (action.title == "afficher") {
                                            return `<div class="btn-group" style="margin:5px"><a class="${action.className}" href="${process.env.MIX_PUBLIC_URL}/document/${row.id}" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></a></div>`;
                                        }
                                    } else {
                                        action.available
                                            ? `<div class="btn-group" style="margin:5px">
                                                        <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                                    </div>`
                                            : "";
                                    }
                                })}
                            </div>`;
                }
            }
        ];*/

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#fields-dt");
        let blockSelector = $("#fields-dt-block");

        // Init histocalls DataTable
        return tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[0, "asc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/fields`,
                    data: function(d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function(index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    }
                },
                columns: [
                    { data: "id", name: "id", visible: false }, // Id // name
                    /*{"data": "user.firstname",
                    "render": function (data, type, row) {
                        return row.user.name;
                    },
                    "name": "name", title: 'Crée par'
                },*/ {
                        data: "field_name",
                        name: "field_name",
                        title: "Titre"
                    }, //
                    {
                        data: "group_name",
                        name: "group_name",
                        title: "Groupe",
                        mRender: function(data, type, row) {
                            return data
                                ? `${row.group_name}`
                                : `<span class="badge badge-info">n/d</span>`;
                        }
                    }, // group_name
                    {
                        data: "template",
                        name: "template",
                        title: "Template",
                        mRender: function(data, type, row) {
                            return data
                                ? `${row.template}`
                                : `<span class="badge badge-info">n/d</span>`;
                        }
                    }, //
                    {
                        data: "select_model",
                        name: "select_model",
                        title: "Model",
                        mRender: function(data, type, row) {
                            return data
                                ? `${row.select_model.replace("\\App\\Models\\", "")}`
                                : `<span class="badge badge-info">n/d</span>`;
                        }
                    }, //
                    {
                        data: "select_model_label",
                        name: "select_model_label",
                        title: "Model Label",
                        mRender: function(data, type, row) {
                            return data
                                ? `${row.select_model_label}`
                                : `<span class="badge badge-info">n/d</span>`;
                        }
                    }, //
                    {
                        data: "select_model_id",
                        name: "select_model_id",
                        title: "Model ID",
                        mRender: function(data, type, row) {
                            return data
                                ? `${row.select_model_id}`
                                : `<span class="badge badge-info">n/d</span>`;
                        }
                    }, //
                    {
                        data: "actions",
                        className: "text-center",
                        title: "action",
                        width: "150px",
                        mRender: function(data, type, row) {
                            return `<div class="d-flex justify-content-center action">
                                ${data.map(action => {
                                if (action.islink) {
                                    if (action.title == "afficher") {
                                        return `<div class="btn-group" style="margin:5px"><a class="${action.className}" href="${process.env.MIX_PUBLIC_URL}/document/${row.id}" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></a></div>`;
                                    }
                                } else {
                                    return `<div class="btn-group" style="margin:5px">
                                                        <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                                    </div>`;
                                }
                            })}
                            </div>`;
                        }

                    }
                ],
                fnDrawCallback: function() {
                    blockSelector.removeClass("block-mode-loading");
                    $(".submit-search").prop("disabled", false);
                },
                columnDefs: [
                    {
                        /*targets: [2],
                orderable: false,
                searchable: false*/
                    }
                ]
            });
    }

    //DOM Events
    static initDOMEvents(dt) {

        $("#search-btn").on("click", function() {
            dt.api().ajax.reload();
        });
        $("#clear-search-btn").on("click", function() {
            $("#search-form").trigger("reset");
            $("#campaign-type")
                .val(null)
                .trigger("change");
            dt.api().ajax.reload();
        });
        $("#fields-table-add").on("click", function() {
            let documentAddFormSelector = $("#fields-add-form");

            //hide previous validation errors
            documentAddFormSelector.find(".invalid-feedback").hide();
            documentAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            $("#fields-add-form").trigger("reset");
            $("#modal-fields-add").modal("show");
        });

        $(document).on("click", ".fields-table-edit", function() {
            let documentsEditFormSelector = $("#fields-edit-form");
            documentsEditFormSelector.trigger("reset");

            //get form id
            const $row = $(this).closest("tr");
            const data = $("#fields-dt")
                .DataTable()
                .row($row)
                .data();
            window.CLEARPATH_FIELD_ID = data.id;
            // old values
            $("#edit-form-title-fr").attr("value", data.field_name);
            $("#edit-form-group").val(data.customer_field_group_id);
            $("#edit-form-template").val(data.template);
            $("#edit-form-model").attr("value", data.select_model);
            $("#edit-form-model-label").attr("value", data.select_model_label);
            $("#edit-form-model-id").attr("value", data.select_model_id);
            //hide previous validation errors
            documentsEditFormSelector.find(".invalid-feedback").hide();
            documentsEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            // $('#leadsforms-edit-form').trigger('reset');
            $("#modal-field-edit").modal("show");
        });
        $("#edit-field-btn").on("click", function() {
            let documentsEditFormSelector = $("#fields-edit-form");
            // let leadsFormsEditFormSelector = $("#leadsforms-edit-form");
            let documentEditBlockSelector = $("#block-field-edit");

            //hide previous validation errors
            documentsEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            documentEditBlockSelector.addClass("block-mode-loading");

            $.ajax({
                method: "put",
                url: `${process.env.MIX_API_URL}/fields/${window.CLEARPATH_FIELD_ID}`,
                data: documentsEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        "Edit!",
                        "Field Edité avec succés!",
                        "success"
                    ).then(() => {
                        documentEditBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                        //$('#leadsforms-edit-form').trigger('reset');
                        dt.api().ajax.reload();
                    });
                },
                error: xhr => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        documentEditBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur edition field!",
                            "error"
                        ).then(() => {
                            documentEditBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                }
            });
        });

        $("#add-fields-btn").on("click", function() {
            let documentAddFormSelector = $("#fields-add-form");
            let documentAddBlockSelector = $("#block-fields-add");

            //hide previous validation errors
            documentAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            documentAddBlockSelector.addClass("block-mode-loading");
            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/fields`,
                data: documentAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        "Ajout!",
                        "Field ajouté avec succés!",
                        "success"
                    ).then(() => {
                        documentAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                        $("#fields-add-form").trigger("reset");
                        dt.api().ajax.reload();
                    });
                },
                error: xhr => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        documentAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur ajout field!",
                            "error"
                        ).then(() => {
                            documentAddBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                }
            });
        });

        //DELETE Document
        $("body").on("click", ".fields-table-delete", function() {
            const $row = $(this).closest("tr");
            const data = $("#fields-dt")
                .DataTable()
                .row($row)
                .data();
            window.CLEARPATH_FIELD_ID = data.id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, supprimer!",
                cancelButtonText: "Annuler"
            }).then(result => {
                if (result.isConfirmed) {
                    deleteDocument();
                }
            });
        });
        function deleteDocument() {
            $("#fields-dt-block").addClass("block-mode-loading");

            $.ajax({
                method: "delete",
                url: `${process.env.MIX_API_URL}/fields/${window.CLEARPATH_FIELD_ID}`,
                success: () => {
                    Swal.fire(
                        "Suppression!",
                        "Field supprimé avec succés!",
                        "success"
                    ).then(() => {
                        dt.api().ajax.reload(null, false);
                    });
                },
                error: () => {
                    Swal.fire(
                        "Suppression!",
                        "erreur suppression field!",
                        "error"
                    ).then(() => {
                        $("#fields-dt-block").removeClass(
                            "block-mode-loading"
                        );
                    });
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents(this.initDataTables());
    }
}

// Initialize when page loads
jQuery(() => {
    leadsFormsListPage.init();
});
