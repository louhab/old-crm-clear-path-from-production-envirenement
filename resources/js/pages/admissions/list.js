/*
 *  Document   : list.js
 *  Author     : RR
 *  Description: Custom JS code used in admissions list page
 */
window.CLEARPATH_LEAD_ID = null;

class admissionsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // const lang = $('input[name="lang"]').val();
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            //sType: "html",
            sLengthSelect: "form-control form-control-sm",
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>',
                },
            },
        });

        let columns = [
            {
                data: "client_id",
                name: "client_id",
                title: "<input type='checkbox' id='MyTableCheckAllButton'>",
                visible:
                    window.userRole.role_id > 2 && window.userRole.role_id != 10
                        ? false
                        : true,
                className: "select-checkbox",
                mRender: function (data, type, row) {
                    return "";
                },
            }, // client_id
            {
                data: "actions",
                className: "text-center",
                title: "action",
                width: "150px",
                mRender: function (data, type, row) {
                    //if (action.islink) {}
                    return `<div class="d-flex justify-content-center action">
                                ${data.map((action) =>
                                    action.available
                                        ? `<div class="btn-group" style="margin:5px">
                                    <a class="${action.className}" href="${
                                              action.islink
                                                  ? `${
                                                        process.env
                                                            .MIX_PUBLIC_URL
                                                    }/${
                                                        action.title ==
                                                        "afficher"
                                                            ? "soumission"
                                                            : "soumissions"
                                                    }/${row.case_id}`
                                                  : "javascript:void(0)"
                                          }" title="${action.title}" class="${
                                              action.className
                                          }" data-id="${
                                              row.client_id
                                          }"><i class="${action.icon}"></i></a>
                                </div>`
                                        : ""
                                )}
                            </div>`;
                },
            },
            {
                data: "admission_state_id",
                name: "admission_state_id",
                title: "Status",
                mRender: function (data, type, row) {
                    let badge = "info";
                    switch (row.admission_order) {
                        case 1:
                            badge = "info";
                            break;
                        case 2:
                            badge = "warning";
                            break;
                        case 3:
                            badge = "success";
                            break;
                        case 4:
                            badge = "done";
                            break;
                        case 'Visa&PE refusé':
                            badge = "danger";
                            break;
                    }
                    return data
                        ? `<span class="badge badge-${badge}">${row.admission_state}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // status
            {
                data: "lastname",
                name: "lastname",
                title: "Nom",
                className: "font-w600 font-size-sm",
            }, // lastname
            {
                data: "firstname",
                name: "firstname",
                title: "Prénom",
                className: "font-w600 font-size-sm",
            }, // firstname
            /* {"data": "email", "name": "email", title: 'Email'}, */ // email
            { data: "phone", name: "phone", title: "Téléphone" }, // phone
            {
                data: "pid",
                name: "pid",
                title: "Programme",
                mRender: function (data, type, row) {
                    //return `<span class="badge badge-${data == 1 ? 'info' : 'success'}">${row.pname}</span>`
                    return data
                        ? row.pname
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // program
            {
                name: "city",
                title: "Ville",
                mRender: function (data, type, row) {
                    if (row?.country == "144") {
                        return row?.city2;
                    } else {
                        return row?.city || `<em class="text-muted">null</em>`;
                    }
                },
            }, // Ville
            {
                data: "backoffice",
                name: "backoffice",
                title: "Backoffice",
                visible: window.userRole.role_id == 6 ? false : true,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // backoffice
            {
                data: "conseiller",
                name: "conseiller",
                title: "Conseiller",
                visible: true,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // conseiller
            // total fees
            {
                data: "total_fees",
                name: "total_fees",
                title: "Frais total",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 2 ||
                    window.userRole.role_id == 3 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    if (data && Number(data))
                        return `<span class="badge badge-dark">${
                            data + ` ` + row.currency
                        } </span>`;
                    return `-`;
                },
            },
            {
                data: "currency",
                name: "currency",
                title: "null",
                visible: false,
                mRender: function (data, type, row) {
                    return "";
                },
            },
            {
                data: "admission_date",
                name: "admission_date",
                title: "Date de Création",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 10
                        ? true
                        : true,
                mRender: function (data, type, row) {
                    if (data) {
                        var date = new Date(data);
                        return (
                            (date.getMonth() > 8
                                ? date.getMonth() + 1
                                : "0" + (date.getMonth() + 1)) +
                            "/" +
                            (date.getDate() > 9
                                ? date.getDate()
                                : "0" + date.getDate()) +
                            "/" +
                            date.getFullYear()
                        );
                    }
                    return '<span class="badge badge-info">n/d</span>';
                },
            }, // Date de Creation
            /*{"data": "soumission_processed", "name": "soumission_processed", title: 'Date de Traitement Soumission', "visible": (window.userRole.role_id == 1 ? true : true), mRender: function (data, type, row) {
                    if(data) {
                        var date = new Date(data);
                        return ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear();
                    }
                    return '<span class="badge badge-info">n/d</span>';
                }}, // Date de Traitement
            {"data": "soumission_processed_message", "name": "soumission_processed_message", title: 'Message de Traitement Soumission',"visible": (window.userRole.role_id == 1 ? true : true),
                mRender: function (data, type, row) {
                    let class_comment = "";
                    if (row.client_comment_id!=null) {
                        class_comment = "recent-comment-client";
                    }
                    return data
                        ? `<p class="${class_comment} m-0"><span class="text-muted">${data}</span><a data-id="${row.id}" data-comment="${row.client_comment_id}" data-process="case-edit" href="javascript:void(0)" class="comment-info">Afficher plus</a></p>`
                        : '<span class="badge badge-info">n/d</span>';
                }},
            {"data": "processed", "name": "processed", title: 'Date de Traitement Client', "visible": (window.userRole.role_id == 1 ? true : true), mRender: function (data, type, row) {
                    if(data) {
                        var date = new Date(data);
                        return ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear();
                    }
                    return '<span class="badge badge-info">n/d</span>';
                }}, // Date de Traitement
            {"data": "processed_message", "name": "processed_message", title: 'Message de Traitement Client',"visible": (window.userRole.role_id == 1 ? true : true),
                mRender: function (data, type, row) {
                    let class_comment = "";
                    if (row.last_comment!=null) {
                        class_comment = "recent-comment-client";
                    }
                    return data
                        ? `<p class="${class_comment} m-0"><span class="text-muted">${data}</span><a data-id="${row.id}" data-comment="${row.last_comment}" data-process="case-edit" href="javascript:void(0)" class="comment-info">Afficher plus</a></p>`
                        : '<span class="badge badge-info">n/d</span>';
                }},*/
            {
                data: "soumission_processed",
                name: "soumission_processed",
                title: "Date backoffice",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 2 ||
                    window.userRole.role_id == 8 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    if (data) {
                        var date = new Date(data);
                        return (
                            (date.getMonth() > 8
                                ? date.getMonth() + 1
                                : "0" + (date.getMonth() + 1)) +
                            "/" +
                            (date.getDate() > 9
                                ? date.getDate()
                                : "0" + date.getDate()) +
                            "/" +
                            date.getFullYear()
                        );
                    }
                    return '<span class="badge badge-info">n/d</span>';
                },
            }, // Date de Traitement Soumission
            {
                data: "processed_message",
                name: "processed_message",
                title: "message backoffice",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 2 ||
                    window.userRole.role_id == 8 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    // console.log("Soumission : " + data);
                    let class_comment = "";
                    let tt = data;

                    // const then = new Date(row.bo_update_time);
                    // const then = new Date(data.soumission_processed);
                    // const now = new Date();

                    // const msBetweenDates = Math.abs(
                    //     then.getTime() - now.getTime()
                    // );

                    // 👇️ convert ms to hours                  min  sec   ms
                    // const hoursBetweenDates = msBetweenDates / (60 * 60 * 1000);

                    // if (hoursBetweenDates <= 24) {
                    //     class_comment = "recent-comment";
                    // }

                    if (data?.match(/soumission status modifié/g)) {
                        // console.log(data.match(/note soumission-admission-/g));
                        tt = "Status modifié";
                        return data
                            ? `<span class="text-muted">${tt}</span>`
                            : '<span class="badge badge-info">n/d</span>';
                    }
                    if (data?.match(/note soumission-admission-/g)) {
                        // console.log(data.match(/note soumission-admission-/g));
                        tt = data.replace(
                            "note soumission-admission-",
                            "Admission "
                        );
                        tt = tt.replace(" ajouté", "");
                    } else if (data?.match(/admission note ajouté/g)) {
                        tt = tt.replace("admission note ajouté", "");
                        if (tt != "") {
                            tt = tt.replace(")", "");
                            tt = tt.replace("(", "");
                            if (tt.match(/admission-/g))
                                tt = tt.replace("admission-", "Admission ID:");
                            if (tt.match(/course-/g))
                                tt = tt.replace("course-", "Admission ID:");
                        } else tt = "admission note ajouté";
                    } else if (data?.match(/note soumission-/g)) {
                        tt = tt.replace("note soumission-", "");
                        tt = tt.replace(" ajouté", "");
                    }
                    return data
                        ? `<p class="${class_comment} m-0"><span class="text-muted">${tt}</span><br><a data-id="${row.id}" data-comment="${row.last_comment}" data-process="soumission-view" href="javascript:void(0)" class="comment-info"><small>Afficher plus</small> <i class="fa fa-fw fa-plus ml-1" style="font-size: 15px;"></i></a></p>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            },
            {
                data: "processed",
                name: "processed",
                title: "date conseiller",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 2 ||
                    window.userRole.role_id == 6 ||
                    window.userRole.role_id == 8 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    if (data) {
                        var date = new Date(data);
                        return (
                            (date.getMonth() > 8
                                ? date.getMonth() + 1
                                : "0" + (date.getMonth() + 1)) +
                            "/" +
                            (date.getDate() > 9
                                ? date.getDate()
                                : "0" + date.getDate()) +
                            "/" +
                            date.getFullYear()
                        );
                    }
                    return '<span class="badge badge-info">n/d</span>';
                },
            }, // Date de Traitement client
            {
                data: "processed_message_client",
                name: "processed_message_client",
                title: "message conseiller",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 2 ||
                    window.userRole.role_id == 6 ||
                    window.userRole.role_id == 8 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                // className: row.client_comment_id=="null"? "": "recent-comment",
                mRender: function (data, type, row) {
                    // console.log("Client : " + data);

                    // nadi for admin (date consseiller admission page) @ahaloua
                    const then = new Date(row.co_update_time);
                    const now = new Date();

                    const msBetweenDates = Math.abs(
                        then.getTime() - now.getTime()
                    );

                    // 👇️ convert ms to hours                  min  sec   ms
                    const hoursBetweenDates = msBetweenDates / (60 * 60 * 1000);

                    let class_comment = "";
                    let tt = data;
                    if (hoursBetweenDates <= 24) {
                        // const then = new Date(row.processed);
                        class_comment = "recent-comment-client";
                    }

                    if (data?.match(/soumission status modifié/g)) {
                        // console.log(data.match(/note soumission-admission-/g));
                        tt = "Status modifié";
                        return data
                            ? `<span class="badge badge-info">n/d</span>`
                            : '<span class="badge badge-info">n/d</span>';
                    }
                    if (data?.match(/note soumission-admission-/g)) {
                        // console.log(data.match(/note soumission-admission-/g));
                        tt = data.replace(
                            "note soumission-admission-",
                            "Admission "
                        );
                        tt = tt.replace(" ajouté", "");
                    } else if (data?.match(/admission note ajouté/g)) {
                        tt = tt.replace("admission note ajouté", "");
                        if (tt != "") {
                            tt = tt.replace(")", "");
                            tt = tt.replace("(", "");
                            if (tt.match(/admission-/g))
                                tt = tt.replace("admission-", "Admission ID:");
                            if (tt.match(/course-/g))
                                tt = tt.replace("course-", "Admission ID:");
                        } else tt = "admission note ajouté";
                    } else if (data?.match(/note soumission-/g)) {
                        tt = tt.replace("note soumission-", "");
                        tt = tt.replace(" ajouté", "");
                    }
                    return data
                        ? `<p class="${class_comment} m-0"><span class="text-muted">${tt}</span><br><a data-id="${row.id}" data-comment="${row.client_comment_id}" data-process="case-edit" href="javascript:void(0)" class="comment-info"><small>Afficher plus</small> <i class="fa fa-fw fa-plus ml-1" style="font-size: 15px;"></i></a></p>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            },

            /*{"data": "calls", "name": "calls", title: 'Nombre d\'Appel', className: 'font-w600 font-size-sm', mRender: function (data, type, row) {
                    return row.calls ? row.calls : '<span class="badge badge-info">n/d</span>';
                }}, */ // calls
        ];

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#admissions-dt");
        let blockSelector = $("#admissions-dt-block");

        // Init leads DataTable
        return tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`,
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[14, "desc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/admissions`,
                    data: function (d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function (index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    },
                },
                columns: columns,
                fnDrawCallback: function () {
                    blockSelector.removeClass("block-mode-loading");
                    /* Toolbar Table */
                    //console.log(window.userRole);
                    if (
                        window.userRole.role_id == 1 ||
                        window.userRole.role_id == 2 ||
                        window.userRole.role_id == 10
                    ) {
                        $("div.toolbar").html(
                            '<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-cente mb-2" style="width: 30%;"><select class="form-control dt_action_field" name="agent[]" multiple="multiple"></select><button type="button" class="btn btn-alt-primary ml-2" id="dt_action_btn">Affecter</button></div>'
                        );
                    }
                    $(".dt_action_field").select2({
                        placeholder: "Utilisateurs...",
                        allowClear: true,
                        ajax: {
                            url: `${process.env.MIX_API_URL}/agents`,
                            dataType: "json",
                            delay: 250,
                            method: "POST",
                            data: function (term, page) {
                                return {
                                    q: term,
                                };
                            },
                            processResults: function (data) {
                                let res = [];
                                // console.log(role, data);
                                $.each(data, function (role, group) {
                                    let groupObj = {
                                        text: role,
                                    };
                                    let children = [];
                                    // console.log(role, group);
                                    $.each(group, function (idx, user) {
                                        // if(user.name)
                                        let child = {
                                            text: user.name,
                                            id: user.id,
                                            group: role,
                                        };
                                        children.push(child);
                                    });
                                    groupObj["children"] = children;
                                    res.push(groupObj);
                                });
                                //console.log(res);
                                return {
                                    results: res,
                                };
                            },
                            initSelection: function (element, callback) {},
                            cache: true,
                        },
                    });
                    $("#dt_action_btn").on("click", function () {
                        let backoffices = $(".dt_action_field").select2("val");
                        var $dataSelected = $("#admissions-dt")
                            .DataTable()
                            .rows(".selected")
                            .data();
                        if (
                            backoffices.length > 0 &&
                            $dataSelected.length > 0
                        ) {
                            var resultBackofficeArray =
                                $(".dt_action_field").select2("val");
                            var cs = 0;
                            // console.log();
                            Swal.fire({
                                title: "Etes vous sur?",
                                text: "",
                                icon: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#3085d6",
                                cancelButtonColor: "#d33",
                                confirmButtonText: "Oui, Affecter!",
                                cancelButtonText: "Annuler",
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    $("#admissions-dt-block").addClass(
                                        "block-mode-loading"
                                    );
                                    var custmers_ids = $("#admissions-dt")
                                        .DataTable()
                                        .rows(".selected")
                                        .data()
                                        .pluck("client_id")
                                        .toArray();
                                    $.ajax({
                                        method: "put",
                                        url: `${process.env.MIX_API_URL}/customerBackoffice`,
                                        data: {
                                            custmers_ids: custmers_ids,
                                            resultBackofficeArray:
                                                resultBackofficeArray,
                                        },
                                        success: () => {
                                            Swal.fire(
                                                "Modification!",
                                                "Clients affecté",
                                                "success"
                                            ).then(() => {
                                                $("#admissions-dt")
                                                    .DataTable()
                                                    .ajax.reload(null, false);
                                                $(
                                                    "#MyTableCheckAllButton"
                                                ).prop("checked", false);
                                            });
                                        },
                                        error: () => {
                                            $(
                                                "#admissions-dt-block"
                                            ).removeClass("block-mode-loading");
                                        },
                                    });
                                }
                            });
                        } else if ($dataSelected.length == 0) {
                            Swal.fire(
                                "Modification!",
                                "Veuiller Selectinner un client!",
                                "warning"
                            );
                        } else {
                            Swal.fire(
                                "Modification!",
                                "Veuiller Choisir un backoffice!",
                                "warning"
                            );
                        }
                    });
                    $(".submit-search").prop("disabled", false);
                },
                rowCallback: function (row, data) {
                    const co_update_time = new Date(data.co_update_time);
                    const bo_update_time = new Date(data.bo_update_time);
                    const now = new Date();

                    const co_msBetweenDates = Math.abs(
                        co_update_time.getTime() - now.getTime()
                    );

                    const bo_msBetweenDates = Math.abs(
                        bo_update_time.getTime() - now.getTime()
                    );

                    const co_hoursBetweenDates =
                        co_msBetweenDates / (60 * 60 * 1000);
                    const bo_hoursBetweenDates =
                        bo_msBetweenDates / (60 * 60 * 1000);

                    if ($(row).find(".recent-comment-client").length > 0) {
                        if (
                            window.userRole.role_id == 6 &&
                            co_hoursBetweenDates <= 24
                        ) {
                            $(row).find("td:eq(8)").css("color", "white");
                            $(row)
                                .find("td:eq(8)")
                                .css("background-color", "#FD971F");
                        }

                        $(row).find("td:eq(13)").css("color", "white");
                        $(row)
                            .find("td:eq(13)")
                            .css("background-color", "#FD971F");
                    }

                    // fixing old bug
                    if (bo_hoursBetweenDates <= 24) {
                        $(row).find("td:eq(11)").css("color", "white");
                        $(row)
                            .find("td:eq(11)")
                            .css("background-color", "#ff6363");
                    }

                    // not working
                    if ($(row).find(".recent-comment").length > 0) {
                        $(row).find("td:eq(11)").css("color", "white");
                        $(row)
                            .find("td:eq(11)")
                            .css("background-color", "#ff6363");
                    }
                },
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                        orderable: true,
                        searchable: false,
                        className: "select-checkbox",
                    },
                    {
                        targets: 0,
                        orderable: false,
                    },
                ],
                select: {
                    style: "multi",
                    selector: "td:first-child",
                },
                lengthMenu: [
                    [10, 25, 50, 100, 500, -1],
                    [10, 25, 50, 100, 500, "All"],
                ],
                dom: '<"toolbar">lfrtip',
            });
    }

    static initDOMEvents(dt) {
        $("body").on("click", ".soumission-convert", function () {
            const $row = $(this).closest("tr");
            const data = $("#admissions-dt").DataTable().row($row).data();
            // console.log(data);
            window.CLEARPATH_CUSTOMER_ID = data.client_id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, convertir!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    // console.log(window.CLEARPATH_CASE_ID);
                    $("#admissions-dt-block").addClass("block-mode-loading");

                    $.ajax({
                        method: "put",
                        url: `${process.env.MIX_API_URL}/soumission/convert/${window.CLEARPATH_CUSTOMER_ID}`,
                        success: () => {
                            Swal.fire(
                                "Modification!",
                                "Client converti avec succés!",
                                "success"
                            ).then(() => {
                                dt.api().ajax.reload(null, false);
                                $("#MyTableCheckAllButton").prop(
                                    "checked",
                                    false
                                );
                            });
                        },
                        error: () => {
                            Swal.fire(
                                "Modification!",
                                "erreur modification lead!",
                                "error"
                            ).then(() => {
                                $("#admissions-dt-block").removeClass(
                                    "block-mode-loading"
                                );
                            });
                        },
                    });
                }
            });
        });
        $("body").on("click", ".comment-info", function () {
            /*let customerId = $(this).data("id");
            console.log(customerId);*/
            Livewire.emit(
                "commentInfo",
                $(this).data("id"),
                $(this).data("process")
            );
            $("#modal-comment-info").modal("show");
        });
        jQuery("#modal-comment-info").on("hidden.bs.modal", function (e) {
            // do something...
            Livewire.emit("clearCommentInfo", $(this).data("id"));
        });
        /*var request_pending = false;
        $('body').on('click', '.convert-lead-btn', function () {
            console.log(request_pending)
            if (request_pending) {
                return;
            }
            const $row = $(this).closest('tr')

            const data =  dt.DataTable().row($row).data()
            request_pending = true;
            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/leads/${data.lead_id}`, //CHANGE FOR CUST
                success: () => {
                    Swal.fire(
                        'Création',
                        'Lead converti avec succés!',
                        'success'
                    ).then(() => {
                        dt.api().ajax.reload()
                    });
                    request_pending = false;
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                    }
                    else
                        Swal.fire(
                            'Modification',
                            'erreur conversion lead!',
                            'error'
                        );
                    request_pending = false;
                }
            })

        })*/
        $("body").on("change", "#MyTableCheckAllButton", function () {
            var all = $("#admissions-dt")
                .DataTable()
                .rows({ search: "applied" })
                .count(); // get total count of rows
            var selectedRows = $("#admissions-dt")
                .DataTable()
                .rows({ selected: true, search: "applied" })
                .count(); // get total count of selected rows

            if (selectedRows < all) {
                $("#MyTableCheckAllButton").prop("checked", true);
                //Added search applied in case user wants the search items will be selected
                $("#admissions-dt")
                    .DataTable()
                    .rows({ search: "applied" })
                    .deselect();
                $("#admissions-dt")
                    .DataTable()
                    .rows({ search: "applied" })
                    .select();
            } else {
                $("#admissions-dt")
                    .DataTable()
                    .rows({ search: "applied" })
                    .deselect();
            }
        });

        /*$('body').on('change', '[name="leads-dt_length"]', function () {
            $('#MyTableCheckAllButton').prop('checked', false);
        });*/

        $("#search-btn").on("click", function () {
            dt.api().ajax.reload();
        });

        $("#clear-search-btn").on("click", function () {
            $("#search-form").trigger("reset");
            $("#lead").val(null).trigger("change");
            $("#phone").val(null);
            $("#country").val(null).trigger("change");
            dt.api().ajax.reload();
        });

        $(".dt_search_lead").select2({
            placeholder: "Séléctionnez un lead",
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/getLeads`,
                dataType: "json",
                delay: 250,
                method: "POST",
                data: function (term, page) {
                    return {
                        q: term,
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                            };
                        }),
                    };
                },
                initSelection: function (element, callback) {},
                cache: true,
            },
        });

        $(".dt_search_country").select2({
            placeholder: "Pays",
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/getCountries`,
                dataType: "json",
                delay: 250,
                method: "POST",
                data: function (term, page) {
                    return {
                        q: term,
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                            };
                        }),
                    };
                },
                cache: true,
            },
        });

        //DELETE Quote
        $("body").on("click", ".lead-delete", function () {
            const $row = $(this).closest("tr");
            const data = $("#leads-dt").DataTable().row($row).data();
            //alert(data);
            window.CLEARPATH_LEAD_ID = data.lead_id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, supprimer!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    //let bs = $('#leads-dt-block');
                    leadsListPage.deleteLead(dt);
                }
            });
        });
    }

    static deleteLead(dt) {
        $("#leads-dt-block").addClass("block-mode-loading");

        $.ajax({
            method: "delete",
            url: `${process.env.MIX_API_URL}/leads/${window.CLEARPATH_LEAD_ID}`,
            success: () => {
                Swal.fire(
                    "Suppression!",
                    "Lead supprimé avec succés!",
                    "success"
                ).then(() => {
                    dt.api().ajax.reload(null, false);
                });
            },
            error: () => {
                Swal.fire(
                    "Suppression!",
                    "erreur suppression lead!",
                    "error"
                ).then(() => {
                    $("#leads-dt-block").removeClass("block-mode-loading");
                });
            },
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents(this.initDataTables());
    }
}

// Initialize when page loads
jQuery(() => {
    admissionsListPage.init();
});
