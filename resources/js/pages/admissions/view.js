function _serialize(json) {
    return Object.keys(json)
        .map(function (key) {
            return (
                encodeURIComponent(key) + "=" + encodeURIComponent(json[key])
            );
        })
        .join("&");
}

function refreshPage() {
    var page_y = document.getElementsByTagName("body")[0].scrollTop;
    window.location.href =
        window.location.href.split("?")[0] + "?page_y=" + page_y;
}
window.onload = function () {
    // setTimeout(refreshPage, 35000);
    if (window.location.href.indexOf("page_y") != -1) {
        var match = window.location.href.split("?")[1].split("&")[0].split("=");
        document.getElementsByTagName("body")[0].scrollTop = match[1];
    }
};

// update state
$("body").on("click", ".case-state-update", function () {
    let parameters = $(this).data("case");
    console.log(parameters);
    return;
    $.ajax({
        method: "put",
        url: `${process.env.MIX_API_URL}/states/${parameters}`,
        success: (data) => {
            Swal.fire(
                "Modification!",
                "Status modiffié avec succés!",
                "success"
            ).then(() => {
                document.location.reload(true);
                // location.reload();
            });
            ///$('.method-modal-btn').closest('.block').removeClass('block-mode-loading');
        },
        error: () => {
            Swal.fire("Modification!", "erreur modification status!", "error");
        },
    });
});

$("#modal-message-btn").on("click", function () {
    //hide previous validation errors
    $("#modal-message-add").modal("show");
});

$("#add-message-btn").on("click", function () {
    $.ajax({
        method: "post",
        url: `${process.env.MIX_API_URL}/messages`,
        data: $("#add-message-form").serialize(),
        success: (data) => {},
    });
});

$("body").on("click", "button[id^='admission-add-']", function () {
    let commentID = $(this).data("comment-id");
    let initVal = $(this).data("init-value");
    let currency = $(this).data("currency");

    $("<input>")
        .attr({
            type: "hidden",
            name: "add-form-fees-comment-id",
            value: commentID,
        })
        .appendTo(`#form-add-admision-fees`);

    $("#form-add-admision-fees input[type=number]").val(initVal);
    $("#form-add-admision-fees #task-title").append(
        document.createTextNode(` (en ${currency}) :`)
    );

    $("#modal-set-admission-fees").trigger("reset");
    $("#modal-set-admission-fees").modal("show");
});

$("body").on("click", "button[id^='add-tache-']", function () {
    let customer = $(this).data("customer");
    const dataObj = {
        canal: $(this).data("canal"),
        note: $(this).data("note"),
        view: $(this).data("view"),
    };

    const serialized = _serialize(dataObj);
    $.ajax({
        method: "post",
        url: `${process.env.MIX_API_URL}/tasks/${customer}`,
        data: serialized,
        success: () => {
            Swal.fire("Ajout!", "tache ajoutée avec succés!", "success").then(
                () => {
                    document.location.reload(true);
                    // window.location.reload();
                }
            );
        },
        error: (xhr) => {
            Swal.fire("Ajout!", "erreur ajout tache!", "error").then(() => {
                formAddAdmissionFeesSelector.removeClass("block-mode-loading");
            });
        },
    });
});

// close-

$("body").on("click", "button[id^='update-tache-']", function () {
    const taskID = $(this).data("task");
    const note = $(this).data("note");

    const dataObj = {
        canal: $(this).data("canal"),
        view: $(this).data("view"),
        customer: $(this).data("customer"),
        note: $(this).data("note"),
    };

    const serialized = _serialize(dataObj);
    $.ajax({
        method: "put",
        url: `${process.env.MIX_API_URL}/tasks/${taskID}`,
        data: serialized,
        success: () => {
            Swal.fire(
                "Modif!",
                `tache definie comme ${note} succés!`,
                "success"
            ).then(() => {
                document.location.reload(true);
            });
        },
        error: (xhr) => {
            Swal.fire("Modif!", "erreur modification de tache!", "error").then(
                () => {
                    formAddAdmissionFeesSelector.removeClass(
                        "block-mode-loading"
                    );
                }
            );
        },
    });
});

$("#modal-set-admission-fees").on("hidden.bs.modal", function () {
    $(this).find("form").trigger("reset");
    $("#form-add-admision-fees input[type=hidden]").not(".ignore").remove();
});

$("#form-add-admision-fees-btn").on("click", function () {
    let formAddAdmissionFeesSelector = $("#form-add-admision-fees");

    formAddAdmissionFeesSelector.find(".invalid-feedback").hide();
    formAddAdmissionFeesSelector
        .find(".form-control")
        .removeClass("is-invalid");

    formAddAdmissionFeesSelector.addClass("block-mode-loading");
    $.ajax({
        method: "post",
        url: `${process.env.MIX_API_URL}/task-fees`,
        data: formAddAdmissionFeesSelector.serialize(),
        success: () => {
            Swal.fire(
                "Ajout!",
                "frais admission ajouté avec succés!",
                "success"
            ).then(() => {
                $("#modal-set-admission-fees").trigger("reset");
                $("#modal-set-admission-fees").modal("hide");
                $("#form-add-admision-fees input[type=hidden]")
                    .not(".ignore")
                    .remove();
                window.location.reload();
            });
        },
        error: (xhr) => {
            if (xhr.statusCode().status === 422) {
                $.each(xhr.responseJSON.errors, function (key, value) {
                    $(`#${key}`).addClass("is-invalid");
                    $(`#${key}-error`)
                        .html(
                            value
                                .toString()
                                .replace(
                                    key,
                                    $(
                                        "label[for='" +
                                            $(`#${key}`).attr("id") +
                                            "']"
                                    ).html()
                                )
                        )
                        .show();
                });
                formAddAdmissionFeesSelector.removeClass("block-mode-loading");
            } else
                Swal.fire(
                    "Ajout!",
                    "erreur ajout frais admission!",
                    "error"
                ).then(() => {
                    formAddAdmissionFeesSelector.removeClass(
                        "block-mode-loading"
                    );
                });
        },
    });
});
