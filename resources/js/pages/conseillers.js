/*
 *  Document   : users.js
 *  Author     : RR
 *  Description: Custom JS code used in users page
 */

class conseillersListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm",
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>',
                },
            },
        });

        let columns = [
            { data: "user_id", name: "user_id", visible: false }, // Id
            { data: "name", name: "name", title: "Nom" }, // name
            {
                data: "queue",
                name: "queue",
                title: "Nombre de leads affecté",
                mRender: function (data, type, row) {
                    return `${row.role_id == 7 ? row.queue : "-"}`;
                },
            }, // queue val
            { data: "email", name: "email", title: "Email" }, // email
            { data: "phone", name: "phone", title: "Téléphone" }, // phone
            {
                data: "actions",
                className: "text-center",
                title: "action",
                width: "150px",
                mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map((action) =>
                                    action.available
                                        ? `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${
                                                    action.title
                                                }" class="${action.className} ${
                                              action.action == "edit" &&
                                              row.role_id != 7
                                                  ? "d-none"
                                                  : ""
                                          }" data-id="${
                                              row.user_id
                                          }"><i class="${
                                              action.icon
                                          }"></i></button>
                                            </div>`
                                        : ""
                                )}
                            </div>`;
                },
            },
        ];

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#users-dt");
        let blockSelector = $("#users-dt-block");

        // Init users DataTable
        let usersTable = tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`,
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[0, "asc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/conseillers`,
                    data: function (d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function (index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    },
                },
                columns: columns,
                fnDrawCallback: function () {
                    // $('.submit-search').prop('disabled', false);
                    $("div.toolbar").html(
                        '<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-cente mb-2" style="width: 30%;"><select class="form-control dt_action_field" name="agent[]" multiple="multiple"></select><button type="button" class="btn btn-alt-primary ml-2" id="dt_action_btn">Ajouter</button></div>'
                    );
                    $(".dt_action_field").select2({
                        placeholder: "Conseillers...",
                        allowClear: true,
                        ajax: {
                            url: `${process.env.MIX_API_URL}/conseillers`,
                            dataType: "json",
                            delay: 250,
                            method: "POST",
                            data: function (term, page) {
                                return {
                                    q: term,
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function (item) {
                                        return {
                                            text: item.name,
                                            id: item.id,
                                        };
                                    }),
                                };
                            },
                            initSelection: function (element, callback) {},
                            cache: true,
                        },
                    });
                    $("#dt_action_btn").on("click", function () {
                        // console.log(conseillers);
                        let conseillers = $(".dt_action_field").select2("val");
                        let count_success = 0;
                        blockSelector.addClass("block-mode-loading");
                        $.each(conseillers, function (key, conseiller) {
                            console.log(conseiller);
                            $.ajax({
                                method: "get",
                                url: `${process.env.MIX_API_URL}/conseiller/${conseiller}`,
                                async: false,
                                success: () => {
                                    count_success += 1;
                                },
                            });
                        });
                        console.log(conseillers.length, count_success);
                        if (count_success == conseillers.length) {
                            usersTable.api().ajax.reload();
                        } else {
                            Swal.fire(
                                "Ajout!",
                                "erreur ajout conseiller!",
                                "error"
                            );
                        }
                    });
                    blockSelector.removeClass("block-mode-loading");
                },
                columnDefs: [
                    {
                        /*targets: [2],
                orderable: false,
                searchable: false*/
                        className: "select-checkbox",
                    },
                ],
                select: {
                    style: "multi",
                    selector: "td:first-child",
                },
                dom: '<"toolbar">lfrtip',
            });

        //DOM Events

        //SEARCH FILTER
        $(".submit-search").on("click", function () {
            usersTable.api().ajax.reload();
        });

        $(".clear-search").on("click", function () {
            searchFilterSelector.find(".dt_search_field").val("");
            usersTable.api().ajax.reload();
        });

        //UPDATE FORM
        let userEditBlockSelector = $("#block-user-edit");
        let userEditFormSelector = $("#user-edit-form");

        $("body").on("click", ".user-table-edit", function () {
            //hide previous validation errors
            userEditFormSelector.find(".invalid-feedback").hide();
            userEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            const $row = $(this).closest("tr");
            const data = tableSelector.DataTable().row($row).data();
            window.queueId = data.queue_id;

            //set user edit form data
            $("#edit-form-queue").val(data.queue);
            $("#modal-user-edit").modal("show");
        });

        $("#update-user-btn").on("click", function () {
            //hide previous validation errors
            userEditFormSelector.find(".invalid-feedback").hide();
            userEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            userEditBlockSelector.addClass("block-mode-loading");

            $.ajax({
                method: "put",
                url: `${process.env.MIX_API_URL}/conseillers/${window.queueId}`,
                data: userEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        "Modification!",
                        "Nombre de leads affectés à ce support modifié avec succès!",
                        "success"
                    ).then(() => {
                        userEditBlockSelector.removeClass("block-mode-loading");
                        usersTable.api().ajax.reload(null, false);
                    });
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        userEditBlockSelector.removeClass("block-mode-loading");
                    } else
                        Swal.fire(
                            "Modification!",
                            "erreur modification nombre de leads affectés à ce support!",
                            "error"
                        ).then(() => {
                            userEditBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        //ADD FORM
        let userAddFormSelector = $("#user-add-form");

        //DELETE FORM
        $("body").on("click", ".user-table-delete", function () {
            const $row = $(this).closest("tr");
            const data = tableSelector.DataTable().row($row).data();
            window.conseillerId = data.user_id;
            //alert(window.conseillerId);

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, supprimer!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteUser();
                }
            });
        });

        function deleteUser() {
            blockSelector.addClass("block-mode-loading");

            $.ajax({
                method: "delete",
                url: `${process.env.MIX_API_URL}/conseillers/${window.conseillerId}`,
                success: () => {
                    Swal.fire(
                        "Modification!",
                        "conseiller modifié avec succés!",
                        "success"
                    ).then(() => {
                        usersTable.api().ajax.reload(null, false);
                    });
                },
                error: () => {
                    Swal.fire(
                        "Suppression!",
                        "erreur suppression utilisateur!",
                        "error"
                    ).then(() => {
                        blockSelector.removeClass("block-mode-loading");
                    });
                },
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => {
    conseillersListPage.init();
});
