/*
 *  Document   : users.js
 *  Author     : RR
 *  Description: Custom JS code used in users page
 */

class usersListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "name", "name": "name", title: 'Nom'}, // name
            {"data": "email", "name": "email", title: 'Email'}, // email
            {"data": "phone", "name": "phone", title: 'Téléphone'}, // phone
            {"data": "role.role_name", "name": "role_id", title: 'Role'}, // role
            {"data": "status", "name": "status", title: 'Status', mRender: function (data, type, row) {
                let badge = "danger";
                let label = "Disabled";
                switch(row.status) {
                    case 1:
                      // code block
                      badge = "success";
                      label = "Enabled";
                      break;
                  }
                return `<span class="badge badge-${badge}">${label}</span>`;
            }}, // status
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                                     (action.available ?
                                            `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                            :
                                            ''
                                     )
                                )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#users-dt');
        let blockSelector = $('#users-dt-block');

        // Init users DataTable
        let usersTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-search').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/users`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events

        //SEARCH FILTER
        $('.submit-search').on('click', function () {
            usersTable.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            usersTable.api().ajax.reload();
        });

        //ADD FORM
        let userAddBlockSelector = $('#block-user-add');
        let userAddFormSelector = $('#user-add-form');

        $('#user-table-add').on('click', function () {

            //hide previous validation errors
            userAddFormSelector.find('.invalid-feedback').hide();
            userAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#user-add-form').trigger('reset');
            $('#modal-user-add').modal('show');
        });

        $('#add-user-btn').on('click', function () {

            //hide previous validation errors
            userAddFormSelector.find('.invalid-feedback').hide();
            userAddFormSelector.find('.form-control').removeClass('is-invalid');

            userAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/users`,
                data: userAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'utilisateur ajouté avec succés!',
                        'success'
                    ).then(() => {
                        userAddBlockSelector.removeClass('block-mode-loading');
                        $('#user-add-form').trigger('reset');
                        usersTable.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        userAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout utilisateur!',
                            'error'
                        ).then(() => {
                            userAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //UPDATE FORM
        let userEditBlockSelector = $('#block-user-edit');
        let userEditFormSelector = $('#user-edit-form');

        $('body').on('click', '.user-table-edit', function() {

            //hide previous validation errors
            userEditFormSelector.find('.invalid-feedback').hide();
            userEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.userId = data.id;

            //set user edit form data
            $('#edit-form-name').val(data.name);
            $('#edit-form-email').val(data.email);
            $('#edit-form-role').val(data.role.id);
            // console.log(data.country.name_fr_fr);
            // $("#edit-form-country").select2('data', { id:data.country_id, text: data.country.name_fr_fr});
            $("#edit-form-country").val(data.country_id).trigger("change");
            $("#edit-form-lang").val(data.lang).trigger("change");
            // $('#edit-form-gender').val(data.role.id);
            //if (data.gender == "male")
            $('.edit-form-gender[value="'+data.gender+'"]').prop('checked', true);
            //console.log($('#edit-form-status option[value="'+data.status+'"]').html());
            $('#edit-form-status option[value="'+data.status+'"]').prop("selected", true);
            $('#phone').val(data.phone);
            $('#edit-form-password').val('');
            $('#modal-user-edit').modal('show');
        });

        $('#update-user-btn').on('click', function () {

            //hide previous validation errors
            userEditFormSelector.find('.invalid-feedback').hide();
            userEditFormSelector.find('.form-control').removeClass('is-invalid');

            userEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/users/${window.userId}`,
                data: userEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'utilisateur modifié avec succés!',
                        'success'
                    ).then(() => {
                        userEditBlockSelector.removeClass('block-mode-loading');
                        usersTable.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        userEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification utilisateur!',
                            'error'
                        ).then(() => {
                            userEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        //DELETE FORM
        $('body').on('click', '.user-table-delete', function() {

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.userId = data.id;

            Swal.fire({
                title: 'Etes vous sur?',
                text: "L'opération est irréversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteUser();
                }
            })
        });

        function deleteUser() {

            blockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'delete',
                url: `${process.env.MIX_API_URL}/users/${window.userId}`,
                success: () => {
                    Swal.fire(
                        'Suppression!',
                        'utilisateur supprimé avec succés!',
                        'success'
                    ).then(() => {
                        usersTable.api().ajax.reload(null, false);
                    })
                },
                error: () => {
                    Swal.fire(
                        'Suppression!',
                        'erreur suppression utilisateur!',
                        'error'
                    ).then(() => {
                        blockSelector.removeClass('block-mode-loading');
                    })
                }
            });
        }
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { usersListPage.init(); });
