/*
 *  Document   : substepnotifications.js
 *  Author     : RR
 *  Description: Custom JS code used in Messages page
 */
window.CLEARPATH_MESSAGE_ID = null

class substepnotificationsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        let columns = [
            {"data": "id", "name": "id", "visible": false}, // Id
            {"data": "step_label", "name": "step_label", title: 'Step'}, // firstname
            {"data": "name_fr", "name": "name_fr", title: 'name_fr'}, // firstname
            {"data": "name_en", "name": "name_en", title: 'name_en'}, // lastname
            {"data": "object", "name": "object", title: 'object'}, // objet
            {
                data: "actions",
                className: "text-center", title: "action", width: "150px", mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                                ${data.map(action =>
                        (action.available ?
                                `<div class="btn-group" style="margin:5px">
                                                <button href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.id}"><i class="${action.icon}"></i></button>
                                            </div>`
                                :
                                ''
                        )
                    )}
                            </div>`;
                }
            }
        ];

        let searchFilterSelector  = $('.search-filter');
        let tableSelector = $('#substepnotifications-dt');
        let blockSelector = $('#substepnotifications-dt-block');

        // Init substepnotifications DataTable
        let substepnotificationTable = tableSelector.on('preXhr.dt', () => {
            blockSelector.addClass('block-mode-loading');
            $('.submit-search').prop('disabled', true);
        }).dataTable({
            language: {
                url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`
            },
            searching: false,
            responsive: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            order: [[0, 'asc']],
            ajax: {
                type: "GET",
                url: `${process.env.MIX_API_URL}/substepnotifications`,
                data: function (d) {
                    searchFilterSelector.find('.dt_search_field').each(function (index, item) {
                        let itemInput = $(item);
                        if (itemInput.val() !== '') {
                            d[itemInput.attr('name')] = itemInput.val();
                        }
                    });
                }
            },
            columns: columns,
            fnDrawCallback : function() {
                blockSelector.removeClass('block-mode-loading');
                $('.submit-search').prop('disabled', false);
            },
            columnDefs: [{
                /*targets: [2],
                orderable: false,
                searchable: false*/
            }],
        });

        //DOM Events
        //TODO put dom events here

        //SEARCH FILTER
        $('.submit-search').on('click', function () {
            substepnotificationTable.api().ajax.reload();
        });

        $('.clear-search').on('click', function () {
            searchFilterSelector.find('.dt_search_field').val('');
            substepnotificationTable.api().ajax.reload();
        });

        let substepnotificationEditBlockSelector = $('#block-substepnotification-edit');
        let substepnotificationEditFormSelector = $('#substepnotification-edit-form');

        $('body').on('click', '.substepnotification-table-edit', function() {

            //hide previous validation errors
            substepnotificationEditFormSelector.find('.invalid-feedback').hide();
            substepnotificationEditFormSelector.find('.form-control').removeClass('is-invalid');

            const $row = $(this).closest('tr');
            const data =  tableSelector.DataTable().row($row).data();
            window.histocallId = data.id;

            $('#edit-form-name_fr').val(data.name_fr);
            $('#edit-form-name_en').val(data.name_en);
            $('#edit-form-email_notification_id').val(data.email_notification_id);

            $('#modal-substepnotification-edit').modal('show');
        });

        $('#update-substepnotification-btn').on('click', function () {

            //hide previous validation errors
            substepnotificationEditFormSelector.find('.invalid-feedback').hide();
            substepnotificationEditFormSelector.find('.form-control').removeClass('is-invalid');

            substepnotificationEditBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/substepnotifications/${window.histocallId}`,
                data: substepnotificationEditFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Modification!',
                        'Sous step state modifié avec succés!',
                        'success'
                    ).then(() => {
                        substepnotificationEditBlockSelector.removeClass('block-mode-loading');
                        substepnotificationTable.api().ajax.reload(null, false);
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        substepnotificationEditBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Modification!',
                            'erreur modification sub step state!',
                            'error'
                        ).then(() => {
                            substepnotificationEditBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { substepnotificationsListPage.init(); });
