/*
 *  Document   : sheet.js
 *  Author     : OL
 *  Description: Custom JS code used in case sheet page
 */

class casesSheetPage {
    /*
     * Init DOM events
     *
     */
    static lang_test(tcf, tef, ielts, celpip) {
        var fol_test = $('select[name="lang_test"] option:selected').val();

        let lang_speaking_old = $(
            'select[name="lang_oral_exp_test_score"]'
        ).data("old");
        let lang_listening_old = $(
            'select[name="lang_oral_comp_test_score"]'
        ).data("old");
        let lang_reading_old = $(
            'select[name="lang_writing_comp_test_score"]'
        ).data("old");
        let lang_writing_old = $(
            'select[name="lang_writing_exp_test_score"]'
        ).data("old");
        //console.log("Old : " + fol_test);
        // console.log(.html());
        // if(q3_age_old && q3_age[i][0] == q3_age_old) {
        //     q3_options += '<option value="' + q3_age[i][0] + '" selected="selected">' + q3_age[i][1] + '</option>';
        // }
        $('select[name="lang_oral_exp_test_score"]').empty(); // #q5i-b-speaking
        $('select[name="lang_oral_comp_test_score"]').empty(); // #q5i-b-listening
        $('select[name="lang_writing_comp_test_score"]').empty(); // #q5i-b-reading
        $('select[name="lang_writing_exp_test_score"]').empty(); // #q5i-b-writing

        // console.log("line 37", this.select_placeholder);
        // console.log(this);
        var q5i_b_speaking =
            '<option value="badvalue" label="' +
            `${this.select_placeholder}` +
            '">' +
            `${this.select_placeholder}` +
            "</option>";
        var q5i_b_listening =
            '<option value="badvalue" label="' +
            `${this.select_placeholder}` +
            '">' +
            `${this.select_placeholder}` +
            "</option>";
        var q5i_b_reading =
            '<option value="badvalue" label="' +
            `${this.select_placeholder}` +
            '">' +
            `${this.select_placeholder}` +
            "</option>";
        var q5i_b_writing =
            '<option value="badvalue" label="' +
            `${this.select_placeholder}` +
            '">' +
            `${this.select_placeholder}` +
            "</option>";

        if (fol_test) {
            if (fol_test == "A") {
                for (i = 0; i < celpip.length; i++) {
                    if (
                        lang_speaking_old &&
                        celpip[i][1] == lang_speaking_old
                    ) {
                        q5i_b_speaking +=
                            '<option value="' +
                            celpip[i][0] +
                            '" selected="selected">' +
                            celpip[i][1] +
                            "</option>";
                    } else {
                        q5i_b_speaking +=
                            '<option value="' +
                            celpip[i][0] +
                            '"">' +
                            celpip[i][1] +
                            "</option>";
                    }
                    if (
                        lang_listening_old &&
                        celpip[i][2] == lang_listening_old
                    ) {
                        q5i_b_listening +=
                            '<option value="' +
                            celpip[i][0] +
                            '" selected="selected">' +
                            celpip[i][2] +
                            "</option>";
                    } else {
                        q5i_b_listening +=
                            '<option value="' +
                            celpip[i][0] +
                            '"">' +
                            celpip[i][2] +
                            "</option>";
                    }
                    if (lang_reading_old && celpip[i][3] == lang_reading_old) {
                        q5i_b_reading +=
                            '<option value="' +
                            celpip[i][0] +
                            '" selected="selected">' +
                            celpip[i][3] +
                            "</option>";
                    } else {
                        q5i_b_reading +=
                            '<option value="' +
                            celpip[i][0] +
                            '"">' +
                            celpip[i][3] +
                            "</option>";
                    }
                    if (lang_writing_old && celpip[i][4] == lang_writing_old) {
                        q5i_b_writing +=
                            '<option value="' +
                            celpip[i][0] +
                            '" selected="selected">' +
                            celpip[i][4] +
                            "</option>";
                    } else {
                        q5i_b_writing +=
                            '<option value="' +
                            celpip[i][0] +
                            '"">' +
                            celpip[i][4] +
                            "</option>";
                    }
                }
            } else if (fol_test == "B") {
                for (i = 0; i < ielts.length; i++) {
                    if (lang_speaking_old && ielts[i][1] == lang_speaking_old) {
                        q5i_b_speaking +=
                            '<option value="' +
                            ielts[i][0] +
                            '" selected="selected">' +
                            ielts[i][1] +
                            "</option>";
                    } else {
                        q5i_b_speaking +=
                            '<option value="' +
                            ielts[i][0] +
                            '"">' +
                            ielts[i][1] +
                            "</option>";
                    }
                    if (
                        lang_listening_old &&
                        ielts[i][2] == lang_listening_old
                    ) {
                        q5i_b_listening +=
                            '<option value="' +
                            ielts[i][0] +
                            '" selected="selected">' +
                            ielts[i][2] +
                            "</option>";
                    } else {
                        q5i_b_listening +=
                            '<option value="' +
                            ielts[i][0] +
                            '"">' +
                            ielts[i][2] +
                            "</option>";
                    }
                    if (lang_reading_old && ielts[i][3] == lang_reading_old) {
                        q5i_b_reading +=
                            '<option value="' +
                            ielts[i][0] +
                            '" selected="selected">' +
                            ielts[i][3] +
                            "</option>";
                    } else {
                        q5i_b_reading +=
                            '<option value="' +
                            ielts[i][0] +
                            '"">' +
                            ielts[i][3] +
                            "</option>";
                    }
                    if (lang_writing_old && ielts[i][4] == lang_writing_old) {
                        q5i_b_writing +=
                            '<option value="' +
                            ielts[i][0] +
                            '" selected="selected">' +
                            ielts[i][4] +
                            "</option>";
                    } else {
                        q5i_b_writing +=
                            '<option value="' +
                            ielts[i][0] +
                            '"">' +
                            ielts[i][4] +
                            "</option>";
                    }
                }
            } else if (fol_test == "C") {
                // console.log("test" + lang_speaking_old);
                for (i = 0; i < tef.length; i++) {
                    if (lang_speaking_old && tef[i][1] == lang_speaking_old) {
                        q5i_b_speaking +=
                            '<option value="' +
                            tef[i][0] +
                            '" selected="selected">' +
                            tef[i][1] +
                            "</option>";
                    } else {
                        q5i_b_speaking +=
                            '<option value="' +
                            tef[i][0] +
                            '"">' +
                            tef[i][1] +
                            "</option>";
                    }
                    if (lang_listening_old && tef[i][2] == lang_listening_old) {
                        q5i_b_listening +=
                            '<option value="' +
                            tef[i][0] +
                            '" selected="selected">' +
                            tef[i][2] +
                            "</option>";
                    } else {
                        q5i_b_listening +=
                            '<option value="' +
                            tef[i][0] +
                            '"">' +
                            tef[i][2] +
                            "</option>";
                    }
                    if (lang_reading_old && tef[i][3] == lang_reading_old) {
                        q5i_b_reading +=
                            '<option value="' +
                            tef[i][0] +
                            '" selected="selected">' +
                            tef[i][3] +
                            "</option>";
                    } else {
                        q5i_b_reading +=
                            '<option value="' +
                            tef[i][0] +
                            '"">' +
                            tef[i][3] +
                            "</option>";
                    }
                    if (lang_writing_old && tef[i][4] == lang_writing_old) {
                        q5i_b_writing +=
                            '<option value="' +
                            tef[i][0] +
                            '" selected="selected">' +
                            tef[i][4] +
                            "</option>";
                    } else {
                        q5i_b_writing +=
                            '<option value="' +
                            tef[i][0] +
                            '"">' +
                            tef[i][4] +
                            "</option>";
                    }
                }
            } else if (fol_test == "D") {
                for (i = 0; i < tcf.length; i++) {
                    if (lang_speaking_old && tcf[i][1] == lang_speaking_old) {
                        q5i_b_speaking +=
                            '<option value="' +
                            tcf[i][0] +
                            '" selected="selected">' +
                            tcf[i][1] +
                            "</option>";
                    } else {
                        q5i_b_speaking +=
                            '<option value="' +
                            tcf[i][0] +
                            '"">' +
                            tcf[i][1] +
                            "</option>";
                    }
                    if (lang_listening_old && tcf[i][2] == lang_listening_old) {
                        q5i_b_listening +=
                            '<option value="' +
                            tcf[i][0] +
                            '" selected="selected">' +
                            tcf[i][2] +
                            "</option>";
                    } else {
                        q5i_b_listening +=
                            '<option value="' +
                            tcf[i][0] +
                            '"">' +
                            tcf[i][2] +
                            "</option>";
                    }
                    if (lang_reading_old && tcf[i][3] == lang_reading_old) {
                        q5i_b_reading +=
                            '<option value="' +
                            tcf[i][0] +
                            '" selected="selected">' +
                            tcf[i][3] +
                            "</option>";
                    } else {
                        q5i_b_reading +=
                            '<option value="' +
                            tcf[i][0] +
                            '"">' +
                            tcf[i][3] +
                            "</option>";
                    }
                    if (lang_writing_old && tcf[i][4] == lang_writing_old) {
                        q5i_b_writing +=
                            '<option value="' +
                            tcf[i][0] +
                            '" selected="selected">' +
                            tcf[i][4] +
                            "</option>";
                    } else {
                        q5i_b_writing +=
                            '<option value="' +
                            tcf[i][0] +
                            '"">' +
                            tcf[i][4] +
                            "</option>";
                    }
                }
            }

            $('select[name="lang_oral_exp_test_score"]').append(q5i_b_speaking);
            $('select[name="lang_oral_comp_test_score"]').append(
                q5i_b_listening
            );
            $('select[name="lang_writing_comp_test_score"]').append(
                q5i_b_reading
            );
            $('select[name="lang_writing_exp_test_score"]').append(
                q5i_b_writing
            );

            //document.getElementById("q5i-b-fol").style.display = "block";
            $('select[name="lang_oral_exp_test_score"]').closest(".row").show();
            $('select[name="lang_oral_comp_test_score"]')
                .closest(".row")
                .show();
            $('select[name="lang_writing_comp_test_score"]')
                .closest(".row")
                .show();
            $('select[name="lang_writing_exp_test_score"]')
                .closest(".row")
                .show();
            // populate_q5ii(fol_test);
            window.fol_lang = null;
            // $("#q5ii")
            let other_lang_old = $('select[name="other_lang_test"]').data(
                "old"
            );
            let index_old = $('select[name="other_lang_test"] option').index(
                $(
                    'select[name="other_lang_test"] option[value="' +
                        other_lang_old +
                        '"]'
                )
            );
            // console.log(index_old);
            $('select[name="other_lang_test"]').empty();

            // console.log("line 369", this.select_placeholder);
            // @@
            const clear__lang = $('[name="lang"]').val();
            // console.log(clear__lang, " :: clear__lang (1)");
            var q5ii_options =
                '<option value="badvalue" label="' +
                `${this.select_placeholder}` +
                '">' +
                `${this.select_placeholder}` +
                "</option>";
            if (fol_test == "A" || fol_test == "B") {
                if (clear__lang === "fr") {
                    q5ii_options +=
                        '<option value="A">TEF Canada</option><option value="B">TCF Canada</option><option value="C">sans objet</option>';
                } else {
                    q5ii_options +=
                        '<option value="A">TEF Canada</option><option value="B">TCF Canada</option><option value="C">Not applicable</option>';
                }
                window.fol_lang = "eng";
            } else if (fol_test == "C" || fol_test == "D") {
                if (clear__lang === "fr") {
                    q5ii_options +=
                        '<option value="A">CELPIP-G</option><option value="B">IELTS</option><option value="C">sans objet</option>';
                } else {
                    q5ii_options +=
                        '<option value="A">CELPIP-G</option><option value="B">IELTS</option><option value="C">Not applicable</option>';
                }
                window.fol_lang = "fra";
            }
            //document.getElementById("q5ii").innerHTML = q5ii_options;
            $('select[name="other_lang_test"]').append(q5ii_options);
            if (index_old > 0) {
                $('select[name="other_lang_test"]')
                    .prop("selectedIndex", index_old)
                    .change();
            }

            // function display_q5ii() {
            //     if (display_q5ii_s == true && display_q5ii_l == true && display_q5ii_r == true && display_q5ii_w == true) {
            //         $('select[name="other_lang_test"]').closest('.row').show();
            //         //document.getElementById("q5ii-sol").style.display = "block";
            //     } else {
            //         $('select[name="other_lang_test"]').closest('.row').hide();
            //         //document.getElementById("q5ii-sol").style.display = "none";
            //     }
            // }
            // window.fol_lang;
            // if (fol_test == "A" || fol_test == "B") {
            //     window.fol_lang = "eng";
            // } else if (fol_test == "C" || fol_test == "D") {
            //     window.fol_lang = "fra";
            // }
            //document.getElementById("q5ii").innerHTML = q5ii_options;
        }
    }
    static spouse_lang_test(tcf, tef, ielts, celpip) {
        var s_fol_test = $(
            'select[name="spouse_lang_test"] option:selected'
        ).val();

        let lang_speaking_old = $('select[name="spouse_lang_speaking"]').data(
            "old"
        );
        let lang_listening_old = $('select[name="spouse_lang_listening"]').data(
            "old"
        );
        let lang_reading_old = $('select[name="spouse_lang_reading"]').data(
            "old"
        );
        let lang_writing_old = $('select[name="spouse_lang_writing"]').data(
            "old"
        );
        // $("#q12ii-fol-speaking")
        $('select[name="spouse_lang_speaking"]').empty();
        //$("#q12ii-fol-listening")
        $('select[name="spouse_lang_listening"]').empty();
        //$("#q12ii-fol-reading")
        $('select[name="spouse_lang_reading"]').empty();
        //$("#q12ii-fol-writing")
        $('select[name="spouse_lang_writing"]').empty();

        // console.log("line 438", this.select_placeholder);

        var q12i_speaking =
            '<option value="badvalue" label="' +
            `${this.select_placeholder}` +
            '">' +
            `${this.select_placeholder}` +
            "</option>";
        var q12i_listening =
            '<option value="badvalue" label="' +
            `${this.select_placeholder}` +
            '">' +
            `${this.select_placeholder}` +
            "</option>";
        var q12i_reading =
            '<option value="badvalue" label="' +
            `${this.select_placeholder}` +
            '">' +
            `${this.select_placeholder}` +
            "</option>";
        var q12i_writing =
            '<option value="badvalue" label="' +
            `${this.select_placeholder}` +
            '">' +
            `${this.select_placeholder}` +
            "</option>";

        if (s_fol_test) {
            // $("#submit").addClass("disabled");
            //$('#update-customer-btn').hide();
            //document.getElementById("q12ii-s-fol").style.display = "block";
            $('select[name="spouse_lang_speaking"]').closest(".row").show();
            $('select[name="spouse_lang_listening"]').closest(".row").show();
            $('select[name="spouse_lang_reading"]').closest(".row").show();
            $('select[name="spouse_lang_writing"]').closest(".row").show();
            if (s_fol_test == "A") {
                for (i = 0; i < celpip.length; i++) {
                    if (
                        lang_speaking_old &&
                        celpip[i][1] == lang_speaking_old
                    ) {
                        q12i_speaking +=
                            '<option value="' +
                            celpip[i][0] +
                            '" selected="selected">' +
                            celpip[i][1] +
                            "</option>";
                    } else {
                        q12i_speaking +=
                            '<option value="' +
                            celpip[i][0] +
                            '"">' +
                            celpip[i][1] +
                            "</option>";
                    }
                    if (
                        lang_listening_old &&
                        celpip[i][2] == lang_listening_old
                    ) {
                        q12i_listening +=
                            '<option value="' +
                            celpip[i][0] +
                            '" selected="selected">' +
                            celpip[i][2] +
                            "</option>";
                    } else {
                        q12i_listening +=
                            '<option value="' +
                            celpip[i][0] +
                            '"">' +
                            celpip[i][2] +
                            "</option>";
                    }
                    if (lang_reading_old && celpip[i][3] == lang_reading_old) {
                        q12i_reading +=
                            '<option value="' +
                            celpip[i][0] +
                            '" selected="selected">' +
                            celpip[i][3] +
                            "</option>";
                    } else {
                        q12i_reading +=
                            '<option value="' +
                            celpip[i][0] +
                            '"">' +
                            celpip[i][3] +
                            "</option>";
                    }
                    if (lang_writing_old && celpip[i][4] == lang_writing_old) {
                        q12i_writing +=
                            '<option value="' +
                            celpip[i][0] +
                            '" selected="selected">' +
                            celpip[i][4] +
                            "</option>";
                    } else {
                        q12i_writing +=
                            '<option value="' +
                            celpip[i][0] +
                            '"">' +
                            celpip[i][4] +
                            "</option>";
                    }
                }
            } else if (s_fol_test == "B") {
                for (i = 0; i < ielts.length; i++) {
                    if (lang_speaking_old && ielts[i][1] == lang_speaking_old) {
                        q12i_speaking +=
                            '<option value="' +
                            ielts[i][0] +
                            '" selected="selected">' +
                            ielts[i][1] +
                            "</option>";
                    } else {
                        q12i_speaking +=
                            '<option value="' +
                            ielts[i][0] +
                            '"">' +
                            ielts[i][1] +
                            "</option>";
                    }
                    if (
                        lang_listening_old &&
                        ielts[i][2] == lang_listening_old
                    ) {
                        q12i_listening +=
                            '<option value="' +
                            ielts[i][0] +
                            '" selected="selected">' +
                            ielts[i][2] +
                            "</option>";
                    } else {
                        q12i_listening +=
                            '<option value="' +
                            ielts[i][0] +
                            '"">' +
                            ielts[i][2] +
                            "</option>";
                    }
                    if (lang_reading_old && ielts[i][3] == lang_reading_old) {
                        q12i_reading +=
                            '<option value="' +
                            ielts[i][0] +
                            '" selected="selected">' +
                            ielts[i][3] +
                            "</option>";
                    } else {
                        q12i_reading +=
                            '<option value="' +
                            ielts[i][0] +
                            '"">' +
                            ielts[i][3] +
                            "</option>";
                    }
                    if (lang_writing_old && ielts[i][4] == lang_writing_old) {
                        q12i_writing +=
                            '<option value="' +
                            ielts[i][0] +
                            '" selected="selected">' +
                            ielts[i][4] +
                            "</option>";
                    } else {
                        q12i_writing +=
                            '<option value="' +
                            ielts[i][0] +
                            '"">' +
                            ielts[i][4] +
                            "</option>";
                    }
                }
            } else if (s_fol_test == "C") {
                for (i = 0; i < tef.length; i++) {
                    if (lang_speaking_old && tef[i][1] == lang_speaking_old) {
                        q12i_speaking +=
                            '<option value="' +
                            tef[i][0] +
                            '" selected="selected">' +
                            tef[i][1] +
                            "</option>";
                    } else {
                        q12i_speaking +=
                            '<option value="' +
                            tef[i][0] +
                            '"">' +
                            tef[i][1] +
                            "</option>";
                    }
                    if (lang_listening_old && tef[i][2] == lang_listening_old) {
                        q12i_listening +=
                            '<option value="' +
                            tef[i][0] +
                            '" selected="selected">' +
                            tef[i][2] +
                            "</option>";
                    } else {
                        q12i_listening +=
                            '<option value="' +
                            tef[i][0] +
                            '"">' +
                            tef[i][2] +
                            "</option>";
                    }
                    if (lang_reading_old && tef[i][3] == lang_reading_old) {
                        q12i_reading +=
                            '<option value="' +
                            tef[i][0] +
                            '" selected="selected">' +
                            tef[i][3] +
                            "</option>";
                    } else {
                        q12i_reading +=
                            '<option value="' +
                            tef[i][0] +
                            '"">' +
                            tef[i][3] +
                            "</option>";
                    }
                    if (lang_writing_old && tef[i][4] == lang_writing_old) {
                        q12i_writing +=
                            '<option value="' +
                            tef[i][0] +
                            '" selected="selected">' +
                            tef[i][4] +
                            "</option>";
                    } else {
                        q12i_writing +=
                            '<option value="' +
                            tef[i][0] +
                            '"">' +
                            tef[i][4] +
                            "</option>";
                    }
                }
            } else if (s_fol_test == "D") {
                for (i = 0; i < tcf.length; i++) {
                    if (lang_speaking_old && tcf[i][1] == lang_speaking_old) {
                        q12i_speaking +=
                            '<option value="' +
                            tcf[i][0] +
                            '" selected="selected">' +
                            tcf[i][1] +
                            "</option>";
                    } else {
                        q12i_speaking +=
                            '<option value="' +
                            tcf[i][0] +
                            '"">' +
                            tcf[i][1] +
                            "</option>";
                    }
                    if (lang_listening_old && tcf[i][2] == lang_listening_old) {
                        q12i_listening +=
                            '<option value="' +
                            tcf[i][0] +
                            '" selected="selected">' +
                            tcf[i][2] +
                            "</option>";
                    } else {
                        q12i_listening +=
                            '<option value="' +
                            tcf[i][0] +
                            '"">' +
                            tcf[i][2] +
                            "</option>";
                    }
                    if (lang_reading_old && tcf[i][3] == lang_reading_old) {
                        q12i_reading +=
                            '<option value="' +
                            tcf[i][0] +
                            '" selected="selected">' +
                            tcf[i][3] +
                            "</option>";
                    } else {
                        q12i_reading +=
                            '<option value="' +
                            tcf[i][0] +
                            '"">' +
                            tcf[i][3] +
                            "</option>";
                    }
                    if (lang_writing_old && tcf[i][4] == lang_writing_old) {
                        q12i_writing +=
                            '<option value="' +
                            tcf[i][0] +
                            '" selected="selected">' +
                            tcf[i][4] +
                            "</option>";
                    } else {
                        q12i_writing +=
                            '<option value="' +
                            tcf[i][0] +
                            '"">' +
                            tcf[i][4] +
                            "</option>";
                    }
                }
            } else if (s_fol_test == "E") {
                $('select[name="spouse_lang_speaking"]').closest(".row").hide();
                $('select[name="spouse_lang_listening"]')
                    .closest(".row")
                    .hide();
                $('select[name="spouse_lang_reading"]').closest(".row").hide();
                $('select[name="spouse_lang_writing"]').closest(".row").hide();
                //document.getElementById("q12ii-s-fol").style.display = "none";
                //$("#submit").removeClass("disabled");
                //$('#update-customer-btn').show();
                calc_score();
                //$('#results').show();
            }
            //$("#q12ii-fol-speaking")
            $('select[name="spouse_lang_speaking"]').append(q12i_speaking);
            // $("#q12ii-fol-listening")
            $('select[name="spouse_lang_listening"]').append(q12i_listening);
            // $("#q12ii-fol-reading")
            $('select[name="spouse_lang_reading"]').append(q12i_reading);
            // $("#q12ii-fol-writing")
            $('select[name="spouse_lang_writing"]').append(q12i_writing);
        }
    }
    static other_lang_test(tcf, tef, ielts, celpip) {
        var sol_test = $(
            'select[name="other_lang_test"] option:selected'
        ).val();
        // console.log(sol_test);
        let lang_speaking_old = $('select[name="lang_speaking"]').data("old");
        let lang_listening_old = $('select[name="lang_listening"]').data("old");
        let lang_reading_old = $('select[name="lang_reading"]').data("old");
        let lang_writing_old = $('select[name="lang_writing"]').data("old");

        $('select[name="lang_speaking"]').empty();
        $('select[name="lang_listening"]').empty();
        $('select[name="lang_reading"]').empty();
        $('select[name="lang_writing"]').empty();

        // console.log("line 786", this.select_placeholder, this);

        var q5ii_sol_speaking =
            '<option value="badvalue" label="' +
            `${this.select_placeholder}` +
            '">' +
            `${this.select_placeholder}` +
            "</option>";
        var q5ii_sol_listening =
            '<option value="badvalue" label="' +
            `${this.select_placeholder}` +
            '">' +
            `${this.select_placeholder}` +
            "</option>";
        var q5ii_sol_reading =
            '<option value="badvalue" label="' +
            `${this.select_placeholder}` +
            '">' +
            `${this.select_placeholder}` +
            "</option>";
        var q5ii_sol_writing =
            '<option value="badvalue" label="' +
            `${this.select_placeholder}` +
            '">' +
            `${this.select_placeholder}` +
            "</option>";

        if (sol_test != "C") {
            // document.getElementById("q5ii-b-sol").style.display = "block";
            $('select[name="lang_speaking"]').closest(".row").show();
            $('select[name="lang_listening"]').closest(".row").show();
            $('select[name="lang_reading"]').closest(".row").show();
            $('select[name="lang_writing"]').closest(".row").show();

            if (window.fol_lang == "eng") {
                if (sol_test == "A") {
                    for (i = 0; i < tef.length; i++) {
                        if (
                            lang_speaking_old &&
                            tef[i][1] == lang_speaking_old
                        ) {
                            q5ii_sol_speaking +=
                                '<option value="' +
                                tef[i][0] +
                                '" selected="selected">' +
                                tef[i][1] +
                                "</option>";
                        } else {
                            q5ii_sol_speaking +=
                                '<option value="' +
                                tef[i][0] +
                                '"">' +
                                tef[i][1] +
                                "</option>";
                        }
                        if (
                            lang_listening_old &&
                            tef[i][2] == lang_listening_old
                        ) {
                            q5ii_sol_listening +=
                                '<option value="' +
                                tef[i][0] +
                                '" selected="selected">' +
                                tef[i][2] +
                                "</option>";
                        } else {
                            q5ii_sol_listening +=
                                '<option value="' +
                                tef[i][0] +
                                '"">' +
                                tef[i][2] +
                                "</option>";
                        }
                        if (lang_reading_old && tef[i][3] == lang_reading_old) {
                            q5ii_sol_reading +=
                                '<option value="' +
                                tef[i][0] +
                                '" selected="selected">' +
                                tef[i][3] +
                                "</option>";
                        } else {
                            q5ii_sol_reading +=
                                '<option value="' +
                                tef[i][0] +
                                '"">' +
                                tef[i][3] +
                                "</option>";
                        }
                        if (lang_writing_old && tef[i][4] == lang_writing_old) {
                            q5ii_sol_writing +=
                                '<option value="' +
                                tef[i][0] +
                                '" selected="selected">' +
                                tef[i][4] +
                                "</option>";
                        } else {
                            q5ii_sol_writing +=
                                '<option value="' +
                                tef[i][0] +
                                '"">' +
                                tef[i][4] +
                                "</option>";
                        }
                    }
                } else if (sol_test == "B") {
                    for (i = 0; i < tcf.length; i++) {
                        if (
                            lang_speaking_old &&
                            tcf[i][1] == lang_speaking_old
                        ) {
                            q5ii_sol_speaking +=
                                '<option value="' +
                                tcf[i][0] +
                                '" selected="selected">' +
                                tcf[i][1] +
                                "</option>";
                        } else {
                            q5ii_sol_speaking +=
                                '<option value="' +
                                tcf[i][0] +
                                '"">' +
                                tcf[i][1] +
                                "</option>";
                        }
                        if (
                            lang_listening_old &&
                            tcf[i][2] == lang_listening_old
                        ) {
                            q5ii_sol_listening +=
                                '<option value="' +
                                tcf[i][0] +
                                '" selected="selected">' +
                                tcf[i][2] +
                                "</option>";
                        } else {
                            q5ii_sol_listening +=
                                '<option value="' +
                                tcf[i][0] +
                                '"">' +
                                tcf[i][2] +
                                "</option>";
                        }
                        if (lang_reading_old && tcf[i][3] == lang_reading_old) {
                            q5ii_sol_reading +=
                                '<option value="' +
                                tcf[i][0] +
                                '" selected="selected">' +
                                tcf[i][3] +
                                "</option>";
                        } else {
                            q5ii_sol_reading +=
                                '<option value="' +
                                tcf[i][0] +
                                '"">' +
                                tcf[i][3] +
                                "</option>";
                        }
                        if (lang_writing_old && tcf[i][4] == lang_writing_old) {
                            q5ii_sol_writing +=
                                '<option value="' +
                                tcf[i][0] +
                                '" selected="selected">' +
                                tcf[i][4] +
                                "</option>";
                        } else {
                            q5ii_sol_writing +=
                                '<option value="' +
                                tcf[i][0] +
                                '"">' +
                                tcf[i][4] +
                                "</option>";
                        }
                    }
                }
            } else if (window.fol_lang == "fra") {
                if (sol_test == "A") {
                    for (i = 0; i < celpip.length; i++) {
                        if (
                            lang_speaking_old &&
                            celpip[i][1] == lang_speaking_old
                        ) {
                            q5ii_sol_speaking +=
                                '<option value="' +
                                celpip[i][0] +
                                '" selected="selected">' +
                                celpip[i][1] +
                                "</option>";
                        } else {
                            q5ii_sol_speaking +=
                                '<option value="' +
                                celpip[i][0] +
                                '"">' +
                                celpip[i][1] +
                                "</option>";
                        }
                        if (
                            lang_listening_old &&
                            celpip[i][2] == lang_listening_old
                        ) {
                            q5ii_sol_listening +=
                                '<option value="' +
                                celpip[i][0] +
                                '" selected="selected">' +
                                celpip[i][2] +
                                "</option>";
                        } else {
                            q5ii_sol_listening +=
                                '<option value="' +
                                celpip[i][0] +
                                '"">' +
                                celpip[i][2] +
                                "</option>";
                        }
                        if (
                            lang_reading_old &&
                            celpip[i][3] == lang_reading_old
                        ) {
                            q5ii_sol_reading +=
                                '<option value="' +
                                celpip[i][0] +
                                '" selected="selected">' +
                                celpip[i][3] +
                                "</option>";
                        } else {
                            q5ii_sol_reading +=
                                '<option value="' +
                                celpip[i][0] +
                                '"">' +
                                celpip[i][3] +
                                "</option>";
                        }
                        if (
                            lang_writing_old &&
                            celpip[i][4] == lang_writing_old
                        ) {
                            q5ii_sol_writing +=
                                '<option value="' +
                                celpip[i][0] +
                                '" selected="selected">' +
                                celpip[i][4] +
                                "</option>";
                        } else {
                            q5ii_sol_writing +=
                                '<option value="' +
                                celpip[i][0] +
                                '"">' +
                                celpip[i][4] +
                                "</option>";
                        }
                    }
                } else if (sol_test == "B") {
                    for (i = 0; i < ielts.length; i++) {
                        if (
                            lang_speaking_old &&
                            ielts[i][1] == lang_speaking_old
                        ) {
                            q5ii_sol_speaking +=
                                '<option value="' +
                                ielts[i][0] +
                                '" selected="selected">' +
                                ielts[i][1] +
                                "</option>";
                        } else {
                            q5ii_sol_speaking +=
                                '<option value="' +
                                ielts[i][0] +
                                '"">' +
                                ielts[i][1] +
                                "</option>";
                        }
                        if (
                            lang_listening_old &&
                            ielts[i][2] == lang_listening_old
                        ) {
                            q5ii_sol_listening +=
                                '<option value="' +
                                ielts[i][0] +
                                '" selected="selected">' +
                                ielts[i][2] +
                                "</option>";
                        } else {
                            q5ii_sol_listening +=
                                '<option value="' +
                                ielts[i][0] +
                                '"">' +
                                ielts[i][2] +
                                "</option>";
                        }
                        if (
                            lang_reading_old &&
                            ielts[i][3] == lang_reading_old
                        ) {
                            q5ii_sol_reading +=
                                '<option value="' +
                                ielts[i][0] +
                                '" selected="selected">' +
                                ielts[i][3] +
                                "</option>";
                        } else {
                            q5ii_sol_reading +=
                                '<option value="' +
                                ielts[i][0] +
                                '"">' +
                                ielts[i][3] +
                                "</option>";
                        }
                        if (
                            lang_writing_old &&
                            ielts[i][4] == lang_writing_old
                        ) {
                            q5ii_sol_writing +=
                                '<option value="' +
                                ielts[i][0] +
                                '" selected="selected">' +
                                ielts[i][4] +
                                "</option>";
                        } else {
                            q5ii_sol_writing +=
                                '<option value="' +
                                ielts[i][0] +
                                '"">' +
                                ielts[i][4] +
                                "</option>";
                        }
                    }
                }
            }

            $('select[name="lang_speaking"]').append(q5ii_sol_speaking);
            $('select[name="lang_listening"]').append(q5ii_sol_listening);
            $('select[name="lang_reading"]').append(q5ii_sol_reading);
            $('select[name="lang_writing"]').append(q5ii_sol_writing);
        } else {
            // document.getElementById("q5ii-b-sol").style.display = "none";
            //$('select[name="other_lang_test"]').closest('.row').hide();
            $('select[name="lang_speaking"]').closest(".row").hide();
            $('select[name="lang_listening"]').closest(".row").hide();
            $('select[name="lang_reading"]').closest(".row").hide();
            $('select[name="lang_writing"]').closest(".row").hide();
            // document.getElementById("q6-work-xp").style.display = "block";
            $('select[name="work_experience_canada"]').closest(".row").show();
        }
    }
    static initDOMEvents() {
        // console.log("QWERTZ", this.select_placeholder);
        window.factors = null;
        $(document).ready(function () {
            // $('#update-customer-btn').closest('form').trigger("reset");
            $("#results").append(
                '<div id="core_factors" style="display:none;"></div>'
            );
            //console.log('testin');
            $("#results").append(
                '<div id="spouse_factors" style="display:none;"></div>'
            );
            $("#results").append(
                '<div id="skill_factors" style="display:none;"></div>'
            );
            $("#results").append(
                '<div id="totals" style="display:none;"></div>'
            );
            // $(".wb-frmvld").trigger("wb-init.wb-frmvld");
            //$('#update-customer-btn').hide();
            let last_select_visible = null;
            $("#sheet-form select").each(function () {
                let val = $(this).find("option:selected").val();
                //console.log(val);
                // if($(this).attr('name') == "lang_speaking") {
                //     let selected = $('select[name="lang_speaking"] option:selected');
                //     let selectedIndx = $('select[name="lang_speaking"] option').index(selected);
                //     $('select[name="lang_speaking"]').prop('selectedIndex', selectedIndx).change();
                //     // console.log($(this).attr('name'), $(this).prop('selectedIndex'), );
                // } else
                if (val) {
                    last_select_visible = $(this).attr("name");
                    let oldd = $(
                        'select[name="' + last_select_visible + '"]'
                    ).data("old");
                    // console.log(last_select_visible, oldd);
                    let selected = $(
                        'select[name="' +
                            last_select_visible +
                            '"] option[value="' +
                            oldd +
                            '"]'
                    );
                    let selectedIndx = $(
                        'select[name="' + last_select_visible + '"] option'
                    ).index(selected);
                    $('select[name="' + last_select_visible + '"]')
                        .prop("selectedIndex", selectedIndx)
                        .change();
                    $(this).closest(".row").show();
                }
            });
            //console.log(last_select_visible);
            if (last_select_visible != "lang_test_expire") {
                //
                $('select[name="' + last_select_visible + '"]')
                    .closest(".row")
                    .next()
                    .show();
            }
            // spouse_lang_test
            casesSheetPage.lang_test(tcf, tef, ielts, celpip);
            casesSheetPage.other_lang_test(tcf, tef, ielts, celpip);
            casesSheetPage.spouse_lang_test(tcf, tef, ielts, celpip);
            $("#calc-score").on("click", function () {
                calc_score();
            });
            //let ttt = last_select_visible.find('select option:selected').val();
            //$('.no-js').();
            var q5i_an = $(
                'select[name="lang_test_expire"] option:selected'
            ).val();
            if (q5i_an && q5i_an == "B") {
                document.getElementById("neg-results").style.display = "block";
                document.getElementById("neg-results").innerHTML =
                    "<div class='alert alert-danger'><p class='mrgn-lft-lg'>D'après vos réponses, vous <strong>ne semblez pas </strong> être admissible au système Entrée express à ce stade-ci.</p><p class='mrgn-lft-lg'>Pour soumettre un profil d’Entrée express, vous devez prouver vos compétences linguistiques en passant un test de compétences linguistiques approuvé.</p></div>";
            }
        });
        $('select[name="marital_status"]').closest(".row").show();
        var with_spouse = false;
        var show_spouse = false;

        //Code, Option, With Spouse Table value, Without Spouse Table Value
        let yo_keyword =
            $('[name="lang"]').val() === "fr" ? "ans" : "years old";
        let or_less_keyword =
            $('[name="lang"]').val() === "fr"
                ? "ans ou moins"
                : "years old or less";
        let or_more_keyword =
            $('[name="lang"]').val() === "fr"
                ? "ans ou plus"
                : "years old or more";
        var q3_age = [
            ["A", `17 ${or_less_keyword}`, "0", "0"],
            ["B", `18 ${yo_keyword}`, "90", "99"],
            ["C", `19 ${yo_keyword}`, "95", "105"],
            ["D", `20 ${yo_keyword}`, "100", "110"],
            ["E", `21 ${yo_keyword}`, "100", "110"],
            ["F", `22 ${yo_keyword}`, "100", "110"],
            ["G", `23 ${yo_keyword}`, "100", "110"],
            ["H", `24 ${yo_keyword}`, "100", "110"],
            ["I", `25 ${yo_keyword}`, "100", "110"],
            ["J", `26 ${yo_keyword}`, "100", "110"],
            ["K", `27 ${yo_keyword}`, "100", "110"],
            ["L", `28 ${yo_keyword}`, "100", "110"],
            ["M", `29 ${yo_keyword}`, "100", "110"],
            ["N", `30 ${yo_keyword}`, "95", "105"],
            ["O", `31 ${yo_keyword}`, "90", "99"],
            ["P", `32 ${yo_keyword}`, "85", "94"],
            ["Q", `33 ${yo_keyword}`, "80", "88"],
            ["R", `34 ${yo_keyword}`, "75", "83"],
            ["S", `35 ${yo_keyword}`, "70", "77"],
            ["T", `36 ${yo_keyword}`, "65", "72"],
            ["U", `37 ${yo_keyword}`, "60", "66"],
            ["V", `38 ${yo_keyword}`, "55", "61"],
            ["W", `39 ${yo_keyword}`, "50", "55"],
            ["X", `40 ${yo_keyword}`, "45", "50"],
            ["Y", `41 ${yo_keyword}`, "35", "39"],
            ["Z", `42 ${yo_keyword}`, "25", "28"],
            ["AA", `43 ${yo_keyword}`, "15", "17"],
            ["AB", `44 ${yo_keyword}`, "5", "6"],
            ["AC", `45 ${or_more_keyword}`, "0", "0"],
        ];

        var q3_options;
        let q3_age_old = $('select[name="age"] option:selected').val();
        // console.log("Old Age: " + q3_age_old);
        // console.log(.html());
        q3_options =
            "<option value>" +
            $('select[name="age"] option:first-child').text() +
            "</option>";
        $('select[name="age"]').empty();
        for (i = 0; i < q3_age.length; i++) {
            if (q3_age_old && q3_age[i][0] == q3_age_old) {
                q3_options +=
                    '<option value="' +
                    q3_age[i][0] +
                    '" selected="selected">' +
                    q3_age[i][1] +
                    "</option>";
            } else {
                q3_options +=
                    '<option value="' +
                    q3_age[i][0] +
                    '">' +
                    q3_age[i][1] +
                    "</option>";
            }
        }
        //$("#q3")
        $('select[name="age"]').append(q3_options);

        //Code, Option, With Spouse Table, Without Spouse Table
        const clear__lang = $('[name="lang"]').val();
        let q4_answer_en = {
            A: {
                en: "None, secondary studies not completed",
                fr: "Aucun, études secondaires non complétées (école secondaire)",
            },
            B: {
                en: "High school diploma",
                fr: "Diplôme d'études secondaires",
            },
            C: {
                en: "One year program of a university/college",
                fr: "Programme d'un an d'une université, d'un collège, d'une école technique ou d'une école de métier, ou d'un autre établissement",
            },
            D: {
                en: "Two-year program of a university/college",
                fr: "Programme de deux ans d'une université, d'un collège, d'une école technique ou d'une école de métier, ou d'un autre établissement",
            },
            E: {
                en: "Baccalaureate (university or college program)",
                fr: "Baccalauréat (programme d'une université, d'un collège, d'une école technique ou d'une école de métier, ou d'un autre établissement)",
            },
            F: {
                en: "At least two certificates, degrees or diplomas",
                fr: "Au moins deux certificats, grades ou diplômes. Au moins l'un de ces diplômes a été obtenu à la suite de la réussite d'un programme d'études postsecondaires d'au moins trois ans",
            },
            G: {
                en: "Masters or professional diploma required for",
                fr: "Maîtrise ou diplôme professionnel nécessaire pour pratiquer une profession réglementée (voir Aide)",
            },
            H: {
                en: "University degree at doctoral level (Ph. D)",
                fr: "Diplôme universitaire au niveau du doctorat (Ph. D.)",
            },
        };
        var q4_education = [
            ["A", q4_answer_en.A[clear__lang], "0", "0"],
            ["B", q4_answer_en.B[clear__lang], "28", "30"],
            ["C", q4_answer_en.C[clear__lang], "84", "90"],
            ["D", q4_answer_en.D[clear__lang], "91", "98"],
            ["E", q4_answer_en.E[clear__lang], "112", "120"],
            ["F", q4_answer_en.F[clear__lang], "119", "128"],
            ["G", q4_answer_en.G[clear__lang], "126", "135"],
            ["H", q4_answer_en.H[clear__lang], "140", "150"],
        ];

        var q4_options;
        let q4_options_old = $(
            'select[name="education_level"] option:selected'
        ).val();
        q4_options =
            "<option value>" +
            $('select[name="education_level"] option:first-child').text() +
            "</option>";
        $('select[name="education_level"]').empty();
        for (i = 0; i < q4_education.length; i++) {
            if (q4_options_old && q4_education[i][0] == q4_options_old) {
                q4_options +=
                    '<option value="' +
                    q4_education[i][0] +
                    '" selected="selected">' +
                    q4_education[i][1] +
                    "</option>";
            } else {
                q4_options +=
                    '<option value="' +
                    q4_education[i][0] +
                    '">' +
                    q4_education[i][1] +
                    "</option>";
            }
        }
        //$("#q4")
        $('select[name="education_level"]').append(q4_options);

        //Code, Speaking, Listening, Reading, Writing, CLB Level, With Spouse Table (FOL), Without Spouse Table (FOL), With Spouse Table (SOL), Without Spouse Table (SOL), Spouse points
        var celpip = [
            [
                "H",
                "10 - 12",
                "10 - 12",
                "10 - 12",
                "10 - 12",
                "10",
                "32",
                "34",
                "6",
                "6",
                "5",
            ],
            ["G", "9", "9", "9", "9", "9", "29", "31", "6", "6", "5"],
            ["F", "8", "8", "8", "8", "8", "22", "23", "3", "3", "3"],
            ["E", "7", "7", "7", "7", "7", "16", "17", "3", "3", "3"],
            ["D", "6", "6", "6", "6", "6", "8", "9", "1", "1", "1"],
            ["C", "5", "5", "5", "5", "5", "6", "6", "1", "1", "1"],
            ["B", "4", "4", "4", "4", "4", "6", "6", "0", "0", "0"],
            [
                "A",
                "M, 0 - 3",
                "M, 0 - 3",
                "M, 0 - 3",
                "M, 0 - 3",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
            ],
        ];

        //Code, Speaking, Listening, Reading, Writing, CLB Level, With Spouse Table (FOL), Without Spouse Table (FOL), With Spouse Table (SOL), Without Spouse Table (SOL), Spouse points
        var ielts = [
            [
                "H",
                "7.5 – 9.0",
                "8.5 – 9.0",
                "8.0 – 9.0",
                "7.5 – 9.0",
                "10",
                "32",
                "34",
                "6",
                "6",
                "5",
            ],
            [
                "G",
                "7.0",
                "8.0",
                "7.0 - 7.5",
                "7.0",
                "9",
                "29",
                "31",
                "6",
                "6",
                "5",
            ],
            ["F", "6.5", "7.5", "6.5", "6.5", "8", "22", "23", "3", "3", "3"],
            [
                "E",
                "6.0",
                "6 .0- 7.0",
                "6.0",
                "6.0",
                "7",
                "16",
                "17",
                "3",
                "3",
                "3",
            ],
            [
                "D",
                "5.5",
                "5.5",
                "5.0 - 5.5",
                "5.5",
                "6",
                "8",
                "9",
                "1",
                "1",
                "1",
            ],
            [
                "C",
                "5.0",
                "5.0",
                "4.0 - 4.5",
                "5.0",
                "5",
                "6",
                "6",
                "1",
                "1",
                "1",
            ],
            [
                "B",
                "4.0 - 4.5",
                "4.5",
                "3.5",
                "4.0 - 4.5",
                "4",
                "6",
                "6",
                "0",
                "0",
                "0",
            ],
            [
                "A",
                "0 - 3.5",
                "0- 4.0",
                "0 - 3.0",
                "0 - 3.5",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
            ],
        ];

        //Code, Speaking, Listening, Reading, Writing, CLB Level, With Spouse Table (FOL), Without Spouse Table (FOL), With Spouse Table (SOL), Without Spouse Table (SOL), Spouse points
        var tef = [
            [
                "H",
                "393-450",
                "316-360",
                "263-300",
                "393-450",
                "10",
                "32",
                "34",
                "6",
                "6",
                "5",
            ],
            [
                "G",
                "371-392",
                "298-315",
                "248-262",
                "371-392",
                "9",
                "29",
                "31",
                "6",
                "6",
                "5",
            ],
            [
                "F",
                "349-370",
                "280-297",
                "233-247",
                "349-370",
                "8",
                "22",
                "23",
                "3",
                "3",
                "3",
            ],
            [
                "E",
                "310-348",
                "249-279",
                "207-232",
                "310-348",
                "7",
                "16",
                "17",
                "3",
                "3",
                "3",
            ],
            [
                "D",
                "271-309",
                "217-248",
                "181-206",
                "271-309",
                "6",
                "8",
                "9",
                "1",
                "1",
                "1",
            ],
            [
                "C",
                "226-270",
                "181-216",
                "151-180",
                "226-270",
                "5",
                "6",
                "6",
                "1",
                "1",
                "1",
            ],
            [
                "B",
                "181-225",
                "145-180",
                "121-150",
                "181-225",
                "4",
                "6",
                "6",
                "0",
                "0",
                "0",
            ],
            [
                "A",
                "0 - 180",
                "0 - 144",
                "0 - 120",
                "0 - 180",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
            ],
        ];

        //Code, Speaking, Listening, Reading, Writing, CLB Level, With Spouse Table (FOL), Without Spouse Table (FOL), With Spouse Table (SOL), Without Spouse Table (SOL), Spouse points
        var tcf = [
            [
                "H",
                "16-20",
                "549-699",
                "549-699",
                "16-20",
                "10",
                "32",
                "34",
                "6",
                "6",
                "5",
            ],
            [
                "G",
                "14-15",
                "523-548",
                "524-548",
                "14-15",
                "9",
                "29",
                "31",
                "6",
                "6",
                "5",
            ],
            [
                "F",
                "12-13",
                "503-522",
                "499-523",
                "12-13",
                "8",
                "22",
                "23",
                "3",
                "3",
                "3",
            ],
            [
                "E",
                "10-11",
                "458-502",
                "453-498",
                "10-11",
                "7",
                "16",
                "17",
                "3",
                "3",
                "3",
            ],
            [
                "D",
                "7-9",
                "398-457",
                "406-452",
                "7-9",
                "6",
                "8",
                "9",
                "1",
                "1",
                "1",
            ],
            ["C", "6", "369-397", "375-405", "6", "5", "6", "6", "1", "1", "1"],
            [
                "B",
                "4-5",
                "331-368",
                "342-374",
                "4-5",
                "4",
                "0",
                "0",
                "0",
                "0",
                "0",
            ],
            ["A", "0-3", "0-330", "0-341", "0-3", "0", "0", "0", "0", "0", "0"],
        ];

        $('select[name="marital_status"]').change(function () {
            var marital_status = $(
                'select[name="marital_status"] option:selected'
            ).val();
            // console.log("marital status: " + marital_status);
            with_spouse = false;
            if (marital_status) {
                if (marital_status == "B" || marital_status == "E") {
                    $('select[name="spouse_cit"]').closest(".row").show();
                    var q3_activated = $(
                        'select[name="age"] option:selected'
                    ).val();
                    //console.log("q3 activated: " + q3_activated);
                    if (q3_activated) {
                        $('select[name="age"]').closest(".row").show();
                    } else {
                        $('select[name="age"]').closest(".row").hide();
                    }
                } else {
                    $('select[name="age"]').closest(".row").show();
                    $('select[name="spouse_cit"]').prop("selectedIndex", 0);
                    $('select[name="spouse_joining"]').prop("selectedIndex", 0);
                    $('select[name="spouse_cit"]').closest(".row").hide();
                    $('select[name="spouse_joining"]').closest(".row").hide();
                }
            } else {
                $('select[name="spouse_cit"]').closest(".row").hide();
                $('select[name="age"]').closest(".row").hide();
            }
        });

        $('select[name="spouse_cit"]').change(function () {
            var spouse_cit = $(
                'select[name="spouse_cit"] option:selected'
            ).val();
            with_spouse = false;
            if (spouse_cit) {
                if (spouse_cit == "A") {
                    $('select[name="spouse_joining"]').closest(".row").show();
                    var q3_activated = $(
                        'select[name="age"] option:selected'
                    ).val();
                    if (q3_activated) {
                        $('select[name="age"]').closest(".row").show();
                    } else {
                        $('select[name="age"]').closest(".row").hide();
                    }
                } else {
                    $('select[name="age"]').closest(".row").show();
                    $('select[name="spouse_joining"]').closest(".row").hide();
                }
            } else {
                $('select[name="spouse_joining"]').closest(".row").hide();
                $('select[name="age"]').closest(".row").hide();
            }
        });

        $('select[name="spouse_joining"]').change(function () {
            var spouse_joining = $(
                'select[name="spouse_joining"] option:selected'
            ).val();
            if (spouse_joining) {
                if (spouse_joining == "B") {
                    with_spouse = true;
                    if (show_spouse == true) {
                        // document.getElementById("spouse_questions").style.display = "block";
                        // document.getElementById("q10-s-education").style.display = "block";
                        $('select[name="spouse_education_level"]')
                            .closest(".row")
                            .show();
                    }
                } else {
                    with_spouse = false;
                    if (show_spouse == true) {
                        // document.getElementById("spouse_questions").style.display = "none";
                        // document.getElementById("q10-s-education").style.display = "none";
                        // $('#q10').prop('selectedIndex', 0);
                        $('select[name="spouse_education_level"]')
                            .closest(".row")
                            .hide();
                        $('select[name="spouse_education_level"]').prop(
                            "selectedIndex",
                            0
                        );
                        // $('#q11').prop('selectedIndex', 0);
                        $('select[name="spouse_work_experience_canada"]').prop(
                            "selectedIndex",
                            0
                        );
                        // $('#q12i').prop('selectedIndex', 0);
                        $('select[name="spouse_lang_test"]').prop(
                            "selectedIndex",
                            0
                        );
                        // $('#q12ii-fol-speaking').prop('selectedIndex', 0);
                        // $('#q12ii-fol-listening').prop('selectedIndex', 0);
                        // $('#q12ii-fol-reading').prop('selectedIndex', 0);
                        // $('#q12ii-fol-writing').prop('selectedIndex', 0);
                        $('select[name="spouse_lang_speaking"]').prop(
                            "selectedIndex",
                            0
                        );
                        $('select[name="spouse_lang_listening"]').prop(
                            "selectedIndex",
                            0
                        );
                        $('select[name="spouse_lang_reading"]').prop(
                            "selectedIndex",
                            0
                        );
                        $('select[name="spouse_lang_writing"]').prop(
                            "selectedIndex",
                            0
                        );
                    }
                }
                $('select[name="age"]').closest(".row").show();
            } else {
                $('select[name="age"]').closest(".row").hide();
            }
        });

        $('select[name="age"]').change(function () {
            var app_age = $('select[name="age"] option:selected').val();
            if (app_age) {
                $('select[name="education_level"]').closest(".row").show();
            } else {
                $('select[name="education_level"]').closest(".row").hide();
            }
        });

        $('select[name="education_level"]').change(function () {
            $('select[name="diplomas_canadien"]').closest(".row").show();
        });
        //#q4b
        $('select[name="diplomas_canadien"]').change(function () {
            var q4b_an = $(
                'select[name="diplomas_canadien"] option:selected'
            ).val();
            if (q4b_an) {
                if (q4b_an == "B") {
                    $('select[name="education_type"]').closest(".row").show();
                    $('select[name="lang_test_expire"]').closest(".row").hide();
                    //document.getElementById("q4c-education").style.display = "block";
                    //document.getElementById("q5-ol").style.display = "none";
                    //document.getElementById("q5i-fol").style.display = "none";
                } else {
                    $('select[name="lang_test_expire"]').closest(".row").show();
                    $('select[name="education_type"]').closest(".row").hide();
                    //document.getElementById("q5-ol").style.display = "block";
                    //document.getElementById("q5i-fol").style.display = "block";
                    //document.getElementById("q4c-education").style.display = "none";
                    $('select[name="education_type"]').prop("selectedIndex", 0); //#q4c
                }
            } else {
                $('select[name="education_type"]').closest(".row").hide();
                $('select[name="lang_test_expire"]').closest(".row").hide();
                //document.getElementById("q4c-education").style.display = "none";
                //document.getElementById("q5-ol").style.display = "none";
                //document.getElementById("q5i-fol").style.display = "none";
            }
        });
        //#q4c
        $('select[name="education_type"]').change(function () {
            $('select[name="lang_test_expire"]').closest(".row").show();
            // document.getElementById("q5-ol").style.display = "block";
            // document.getElementById("q5i-fol").style.display = "block";
        });
        // #q5i
        $('select[name="lang_test_expire"]').change(function () {
            var q5i_an = $(
                'select[name="lang_test_expire"] option:selected'
            ).val();
            if (q5i_an) {
                if (q5i_an == "A") {
                    //$('select[name="lang_test"]').closest('.row').show();
                    //$('select[name="lang_test_expire"]').closest('.row').nextAll().show();
                    //var check_ = false;
                    $('select[name="lang_test_expire"]')
                        .closest(".row")
                        .nextAll()
                        .each(function () {
                            let curr = $(this)
                                .find("select option:selected")
                                .val();
                            //console.log("current val: " + curr.html());
                            if (curr && curr != "") {
                                $(this).show();
                                //check_ = true;
                            }
                        });
                    $('select[name="lang_test"]').closest(".row").show();
                    //document.getElementById("q5i-a-fol").style.display = "block";
                    document.getElementById("neg-results").innerHTML = "";
                    document.getElementById("neg-results").style.display =
                        "none";
                } else {
                    // $('select[name="lang_test"]').closest('.row').hide();
                    // $('select[name="lang_oral_exp_test_score"]').closest('.row').hide();
                    // $('select[name="lang_oral_comp_test_score"]').closest('.row').hide();
                    // $('select[name="lang_writing_comp_test_score"]').closest('.row').hide();
                    // $('select[name="lang_writing_exp_test_score"]').closest('.row').hide();
                    // document.getElementById("q5i-a-fol").style.display = "none";
                    // document.getElementById("q5i-b-fol").style.display = "none";
                    //$('select[name="lang_test"]').prop('selectedIndex', 0);
                    //$('#update-customer-btn').hide();
                    document.getElementById("neg-results").style.display =
                        "block";
                    document.getElementById("neg-results").innerHTML =
                        "<div class='alert alert-danger'><p class='mrgn-lft-lg'>D'après vos réponses, vous <strong>ne semblez pas </strong> être admissible au système Entrée express à ce stade-ci.</p><p class='mrgn-lft-lg'>Pour soumettre un profil d’Entrée express, vous devez prouver vos compétences linguistiques en passant un test de compétences linguistiques approuvé.</p></div>";
                    $('select[name="lang_test_expire"]')
                        .closest(".row")
                        .nextAll()
                        .each(function () {
                            let curr = $(this).find("select");
                            if (curr.length != 0) {
                                $(this).hide();
                            }
                        });
                }
            } else {
                $('select[name="lang_test"]').closest(".row").hide();
                $('select[name="lang_oral_exp_test_score"]')
                    .closest(".row")
                    .hide();
                $('select[name="lang_oral_comp_test_score"]')
                    .closest(".row")
                    .hide();
                $('select[name="lang_writing_comp_test_score"]')
                    .closest(".row")
                    .hide();
                $('select[name="lang_writing_exp_test_score"]')
                    .closest(".row")
                    .hide();
                // document.getElementById("q5i-a-fol").style.display = "none";
                // document.getElementById("q5i-b-fol").style.display = "none";
                document.getElementById("neg-results").innerHTML = "";
                document.getElementById("neg-results").style.display = "none";
            }
        });
        // lang_test, q5i-a
        const select_placeholder = this.select_placeholder;
        $('select[name="lang_test"]').change(function () {
            var fol_test = $('select[name="lang_test"] option:selected').val();

            let lang_speaking_old = $(
                'select[name="lang_oral_exp_test_score"]'
            ).data("old");
            let lang_listening_old = $(
                'select[name="lang_oral_comp_test_score"]'
            ).data("old");
            let lang_reading_old = $(
                'select[name="lang_writing_comp_test_score"]'
            ).data("old");
            let lang_writing_old = $(
                'select[name="lang_writing_exp_test_score"]'
            ).data("old");

            $('select[name="lang_oral_exp_test_score"]').empty(); // #q5i-b-speaking
            $('select[name="lang_oral_comp_test_score"]').empty(); // #q5i-b-listening
            $('select[name="lang_writing_comp_test_score"]').empty(); // #q5i-b-reading
            $('select[name="lang_writing_exp_test_score"]').empty(); // #q5i-b-writing

            // console.log("line 2029", select_placeholder);

            var q5i_b_speaking =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";
            var q5i_b_listening =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";
            var q5i_b_reading =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";
            var q5i_b_writing =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";

            if (fol_test) {
                if (fol_test == "A") {
                    for (i = 0; i < celpip.length; i++) {
                        if (
                            lang_speaking_old &&
                            celpip[i][1] == lang_speaking_old
                        ) {
                            q5i_b_speaking +=
                                '<option value="' +
                                celpip[i][0] +
                                '" selected="selected">' +
                                celpip[i][1] +
                                "</option>";
                        } else {
                            q5i_b_speaking +=
                                '<option value="' +
                                celpip[i][0] +
                                '"">' +
                                celpip[i][1] +
                                "</option>";
                        }
                        if (
                            lang_listening_old &&
                            celpip[i][1] == lang_listening_old
                        ) {
                            q5i_b_listening +=
                                '<option value="' +
                                celpip[i][0] +
                                '" selected="selected">' +
                                celpip[i][2] +
                                "</option>";
                        } else {
                            q5i_b_listening +=
                                '<option value="' +
                                celpip[i][0] +
                                '"">' +
                                celpip[i][2] +
                                "</option>";
                        }
                        if (
                            lang_reading_old &&
                            celpip[i][1] == lang_reading_old
                        ) {
                            q5i_b_reading +=
                                '<option value="' +
                                celpip[i][0] +
                                '" selected="selected">' +
                                celpip[i][3] +
                                "</option>";
                        } else {
                            q5i_b_reading +=
                                '<option value="' +
                                celpip[i][0] +
                                '"">' +
                                celpip[i][3] +
                                "</option>";
                        }
                        if (
                            lang_writing_old &&
                            celpip[i][1] == lang_writing_old
                        ) {
                            q5i_b_writing +=
                                '<option value="' +
                                celpip[i][0] +
                                '" selected="selected">' +
                                celpip[i][4] +
                                "</option>";
                        } else {
                            q5i_b_writing +=
                                '<option value="' +
                                celpip[i][0] +
                                '"">' +
                                celpip[i][4] +
                                "</option>";
                        }
                    }
                } else if (fol_test == "B") {
                    for (i = 0; i < ielts.length; i++) {
                        if (
                            lang_speaking_old &&
                            ielts[i][1] == lang_speaking_old
                        ) {
                            q5i_b_speaking +=
                                '<option value="' +
                                ielts[i][0] +
                                '" selected="selected">' +
                                ielts[i][1] +
                                "</option>";
                        } else {
                            q5i_b_speaking +=
                                '<option value="' +
                                ielts[i][0] +
                                '"">' +
                                ielts[i][1] +
                                "</option>";
                        }
                        if (
                            lang_listening_old &&
                            ielts[i][1] == lang_listening_old
                        ) {
                            q5i_b_listening +=
                                '<option value="' +
                                ielts[i][0] +
                                '" selected="selected">' +
                                ielts[i][2] +
                                "</option>";
                        } else {
                            q5i_b_listening +=
                                '<option value="' +
                                ielts[i][0] +
                                '"">' +
                                ielts[i][2] +
                                "</option>";
                        }
                        if (
                            lang_reading_old &&
                            ielts[i][1] == lang_reading_old
                        ) {
                            q5i_b_reading +=
                                '<option value="' +
                                ielts[i][0] +
                                '" selected="selected">' +
                                ielts[i][3] +
                                "</option>";
                        } else {
                            q5i_b_reading +=
                                '<option value="' +
                                ielts[i][0] +
                                '"">' +
                                ielts[i][3] +
                                "</option>";
                        }
                        if (
                            lang_writing_old &&
                            ielts[i][1] == lang_writing_old
                        ) {
                            q5i_b_writing +=
                                '<option value="' +
                                ielts[i][0] +
                                '" selected="selected">' +
                                ielts[i][4] +
                                "</option>";
                        } else {
                            q5i_b_writing +=
                                '<option value="' +
                                ielts[i][0] +
                                '"">' +
                                ielts[i][4] +
                                "</option>";
                        }
                    }
                } else if (fol_test == "C") {
                    for (i = 0; i < tef.length; i++) {
                        if (
                            lang_speaking_old &&
                            tef[i][1] == lang_speaking_old
                        ) {
                            q5i_b_speaking +=
                                '<option value="' +
                                tef[i][0] +
                                '" selected="selected">' +
                                tef[i][1] +
                                "</option>";
                        } else {
                            q5i_b_speaking +=
                                '<option value="' +
                                tef[i][0] +
                                '"">' +
                                tef[i][1] +
                                "</option>";
                        }
                        if (
                            lang_listening_old &&
                            tef[i][1] == lang_listening_old
                        ) {
                            q5i_b_listening +=
                                '<option value="' +
                                tef[i][0] +
                                '" selected="selected">' +
                                tef[i][2] +
                                "</option>";
                        } else {
                            q5i_b_listening +=
                                '<option value="' +
                                tef[i][0] +
                                '"">' +
                                tef[i][2] +
                                "</option>";
                        }
                        if (lang_reading_old && tef[i][1] == lang_reading_old) {
                            q5i_b_reading +=
                                '<option value="' +
                                tef[i][0] +
                                '" selected="selected">' +
                                tef[i][3] +
                                "</option>";
                        } else {
                            q5i_b_reading +=
                                '<option value="' +
                                tef[i][0] +
                                '"">' +
                                tef[i][3] +
                                "</option>";
                        }
                        if (lang_writing_old && tef[i][1] == lang_writing_old) {
                            q5i_b_writing +=
                                '<option value="' +
                                tef[i][0] +
                                '" selected="selected">' +
                                tef[i][4] +
                                "</option>";
                        } else {
                            q5i_b_writing +=
                                '<option value="' +
                                tef[i][0] +
                                '"">' +
                                tef[i][4] +
                                "</option>";
                        }
                    }
                } else if (fol_test == "D") {
                    for (i = 0; i < tcf.length; i++) {
                        if (
                            lang_speaking_old &&
                            tcf[i][1] == lang_speaking_old
                        ) {
                            q5i_b_speaking +=
                                '<option value="' +
                                tcf[i][0] +
                                '" selected="selected">' +
                                tcf[i][1] +
                                "</option>";
                        } else {
                            q5i_b_speaking +=
                                '<option value="' +
                                tcf[i][0] +
                                '"">' +
                                tcf[i][1] +
                                "</option>";
                        }
                        if (
                            lang_listening_old &&
                            tcf[i][1] == lang_listening_old
                        ) {
                            q5i_b_listening +=
                                '<option value="' +
                                tcf[i][0] +
                                '" selected="selected">' +
                                tcf[i][2] +
                                "</option>";
                        } else {
                            q5i_b_listening +=
                                '<option value="' +
                                tcf[i][0] +
                                '"">' +
                                tcf[i][2] +
                                "</option>";
                        }
                        if (lang_reading_old && tcf[i][1] == lang_reading_old) {
                            q5i_b_reading +=
                                '<option value="' +
                                tcf[i][0] +
                                '" selected="selected">' +
                                tcf[i][3] +
                                "</option>";
                        } else {
                            q5i_b_reading +=
                                '<option value="' +
                                tcf[i][0] +
                                '"">' +
                                tcf[i][3] +
                                "</option>";
                        }
                        if (lang_writing_old && tcf[i][1] == lang_writing_old) {
                            q5i_b_writing +=
                                '<option value="' +
                                tcf[i][0] +
                                '" selected="selected">' +
                                tcf[i][4] +
                                "</option>";
                        } else {
                            q5i_b_writing +=
                                '<option value="' +
                                tcf[i][0] +
                                '"">' +
                                tcf[i][4] +
                                "</option>";
                        }
                    }
                }
                // if (fol_test == "A") {
                //     for (i = 0; i < celpip.length; i++) {
                //         q5i_b_speaking += '<option value="' + celpip[i][0] + '"">' + celpip[i][1] + '</option>';
                //         q5i_b_listening += '<option value="' + celpip[i][0] + '"">' + celpip[i][2] + '</option>';
                //         q5i_b_reading += '<option value="' + celpip[i][0] + '"">' + celpip[i][3] + '</option>';
                //         q5i_b_writing += '<option value="' + celpip[i][0] + '"">' + celpip[i][4] + '</option>';
                //     }
                // } else if (fol_test == "B") {
                //     for (i = 0; i < ielts.length; i++) {
                //         q5i_b_speaking += '<option value="' + ielts[i][0] + '"">' + ielts[i][1] + '</option>';
                //         q5i_b_listening += '<option value="' + ielts[i][0] + '"">' + ielts[i][2] + '</option>';
                //         q5i_b_reading += '<option value="' + ielts[i][0] + '"">' + ielts[i][3] + '</option>';
                //         q5i_b_writing += '<option value="' + ielts[i][0] + '"">' + ielts[i][4] + '</option>';
                //     }
                // } else if (fol_test == "C") {
                //     for (i = 0; i < tef.length; i++) {
                //         q5i_b_speaking += '<option value="' + tef[i][0] + '"">' + tef[i][1] + '</option>';
                //         q5i_b_listening += '<option value="' + tef[i][0] + '"">' + tef[i][2] + '</option>';
                //         q5i_b_reading += '<option value="' + tef[i][0] + '"">' + tef[i][3] + '</option>';
                //         q5i_b_writing += '<option value="' + tef[i][0] + '"">' + tef[i][4] + '</option>';
                //     }
                // } else if (fol_test == "D") {
                //     for (i = 0; i < tcf.length; i++) {
                //         q5i_b_speaking += '<option value="' + tcf[i][0] + '"">' + tcf[i][1] + '</option>';
                //         q5i_b_listening += '<option value="' + tcf[i][0] + '"">' + tcf[i][2] + '</option>';
                //         q5i_b_reading += '<option value="' + tcf[i][0] + '"">' + tcf[i][3] + '</option>';
                //         q5i_b_writing += '<option value="' + tcf[i][0] + '"">' + tcf[i][4] + '</option>';
                //     }
                // }

                $('select[name="lang_oral_exp_test_score"]').append(
                    q5i_b_speaking
                );
                $('select[name="lang_oral_comp_test_score"]').append(
                    q5i_b_listening
                );
                $('select[name="lang_writing_comp_test_score"]').append(
                    q5i_b_reading
                );
                $('select[name="lang_writing_exp_test_score"]').append(
                    q5i_b_writing
                );

                //document.getElementById("q5i-b-fol").style.display = "block";
                $('select[name="lang_oral_exp_test_score"]')
                    .closest(".row")
                    .show();
                $('select[name="lang_oral_comp_test_score"]')
                    .closest(".row")
                    .show();
                $('select[name="lang_writing_comp_test_score"]')
                    .closest(".row")
                    .show();
                $('select[name="lang_writing_exp_test_score"]')
                    .closest(".row")
                    .show();
                populate_q5ii(fol_test);
            } else {
                //document.getElementById("q5i-b-fol").style.display = "none";
                $('select[name="lang_oral_exp_test_score"]')
                    .closest(".row")
                    .hide();
                $('select[name="lang_oral_comp_test_score"]')
                    .closest(".row")
                    .hide();
                $('select[name="lang_writing_comp_test_score"]')
                    .closest(".row")
                    .hide();
                $('select[name="lang_writing_exp_test_score"]')
                    .closest(".row")
                    .hide();
            }
        });

        var display_q5ii_s = false;
        var display_q5ii_l = false;
        var display_q5ii_r = false;
        var display_q5ii_w = false;
        // $("#q5i-b-speaking")
        $('select[name="lang_oral_exp_test_score"]').change(function () {
            var speaking_option = $(
                'select[name="lang_oral_exp_test_score"] option:selected'
            ).val();
            if (speaking_option != "badvalue") {
                display_q5ii_s = true;
            } else {
                display_q5ii_s = false;
            }
            display_q5ii();
        });
        // $("#q5i-b-listening")
        $('select[name="lang_oral_comp_test_score"]').change(function () {
            var listening_option = $(
                'select[name="lang_oral_comp_test_score"] option:selected'
            ).val();
            if (listening_option != "badvalue") {
                display_q5ii_l = true;
            } else {
                display_q5ii_l = false;
            }
            display_q5ii();
        });
        // $("#q5i-b-reading")
        $('select[name="lang_writing_comp_test_score"]').change(function () {
            var reading_option = $(
                'select[name="lang_writing_comp_test_score"] option:selected'
            ).val();
            if (reading_option != "badvalue") {
                display_q5ii_r = true;
            } else {
                display_q5ii_r = false;
            }
            display_q5ii();
        });
        $('select[name="lang_writing_exp_test_score"]').change(function () {
            var writing_option = $(
                'select[name="lang_writing_exp_test_score"] option:selected'
            ).val();
            if (writing_option != "badvalue") {
                display_q5ii_w = true;
            } else {
                display_q5ii_w = false;
            }
            display_q5ii();
        });

        window.fol_lang = null;
        function populate_q5ii(taken_test) {
            // console.log("line 2472", select_placeholder);
            // $("#q5ii")
            $('select[name="other_lang_test"]').empty();
            var q5ii_options =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";
            if (taken_test == "A" || taken_test == "B") {
                if (clear__lang === "fr") {
                    q5ii_options +=
                        '<option value="A">TEF Canada</option><option value="B">TCF Canada</option><option value="C">sans objet</option>';
                } else {
                    q5ii_options +=
                        '<option value="A">TEF Canada</option><option value="B">TCF Canada</option><option value="C">Not applicable</option>';
                }

                window.fol_lang = "eng";
            } else if (taken_test == "C" || taken_test == "D") {
                if (clear__lang === "fr") {
                    q5ii_options +=
                        '<option value="A">CELPIP-G</option><option value="B">IELTS</option><option value="C">sans objet</option>';
                } else {
                    q5ii_options +=
                        '<option value="A">CELPIP-G</option><option value="B">IELTS</option><option value="C">Not applicable</option>';
                }

                window.fol_lang = "fra";
            }
            //document.getElementById("q5ii").innerHTML = q5ii_options;
            $('select[name="other_lang_test"]').append(q5ii_options);
        }

        function display_q5ii() {
            if (
                display_q5ii_s == true &&
                display_q5ii_l == true &&
                display_q5ii_r == true &&
                display_q5ii_w == true
            ) {
                $('select[name="other_lang_test"]').closest(".row").show();
                //document.getElementById("q5ii-sol").style.display = "block";
            } else {
                $('select[name="other_lang_test"]').closest(".row").hide();
                //document.getElementById("q5ii-sol").style.display = "none";
            }
        }
        // $("#q5ii")
        $('select[name="other_lang_test"]').change(function () {
            var sol_test = $(
                'select[name="other_lang_test"] option:selected'
            ).val();
            // console.log(sol_test);
            let lang_speaking_old = $('select[name="lang_speaking"]').data(
                "old"
            );
            let lang_listening_old = $('select[name="lang_listening"]').data(
                "old"
            );
            let lang_reading_old = $('select[name="lang_reading"]').data("old");
            let lang_writing_old = $('select[name="lang_writing"]').data("old");

            $('select[name="lang_speaking"]').empty();
            $('select[name="lang_listening"]').empty();
            $('select[name="lang_reading"]').empty();
            $('select[name="lang_writing"]').empty();

            // console.log("line 2532", select_placeholder);

            var q5ii_sol_speaking =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";
            var q5ii_sol_listening =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";
            var q5ii_sol_reading =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";
            var q5ii_sol_writing =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";

            if (sol_test) {
                if (sol_test != "C") {
                    // document.getElementById("q5ii-b-sol").style.display = "block";
                    $('select[name="lang_speaking"]').closest(".row").show();
                    $('select[name="lang_listening"]').closest(".row").show();
                    $('select[name="lang_reading"]').closest(".row").show();
                    $('select[name="lang_writing"]').closest(".row").show();

                    if (window.fol_lang == "eng") {
                        if (sol_test == "A") {
                            for (i = 0; i < tef.length; i++) {
                                if (
                                    lang_speaking_old &&
                                    tef[i][1] == lang_speaking_old
                                ) {
                                    q5ii_sol_speaking +=
                                        '<option value="' +
                                        tef[i][0] +
                                        '" selected="selected">' +
                                        tef[i][1] +
                                        "</option>";
                                } else {
                                    q5ii_sol_speaking +=
                                        '<option value="' +
                                        tef[i][0] +
                                        '"">' +
                                        tef[i][1] +
                                        "</option>";
                                }
                                if (
                                    lang_listening_old &&
                                    tef[i][2] == lang_listening_old
                                ) {
                                    q5ii_sol_listening +=
                                        '<option value="' +
                                        tef[i][0] +
                                        '" selected="selected">' +
                                        tef[i][2] +
                                        "</option>";
                                } else {
                                    q5ii_sol_listening +=
                                        '<option value="' +
                                        tef[i][0] +
                                        '"">' +
                                        tef[i][2] +
                                        "</option>";
                                }
                                if (
                                    lang_reading_old &&
                                    tef[i][3] == lang_reading_old
                                ) {
                                    q5ii_sol_reading +=
                                        '<option value="' +
                                        tef[i][0] +
                                        '" selected="selected">' +
                                        tef[i][3] +
                                        "</option>";
                                } else {
                                    q5ii_sol_reading +=
                                        '<option value="' +
                                        tef[i][0] +
                                        '"">' +
                                        tef[i][3] +
                                        "</option>";
                                }
                                if (
                                    lang_writing_old &&
                                    tef[i][4] == lang_writing_old
                                ) {
                                    q5ii_sol_writing +=
                                        '<option value="' +
                                        tef[i][0] +
                                        '" selected="selected">' +
                                        tef[i][4] +
                                        "</option>";
                                } else {
                                    q5ii_sol_writing +=
                                        '<option value="' +
                                        tef[i][0] +
                                        '"">' +
                                        tef[i][4] +
                                        "</option>";
                                }
                            }
                        } else if (sol_test == "B") {
                            for (i = 0; i < tcf.length; i++) {
                                if (
                                    lang_speaking_old &&
                                    tcf[i][1] == lang_speaking_old
                                ) {
                                    q5ii_sol_speaking +=
                                        '<option value="' +
                                        tcf[i][0] +
                                        '" selected="selected">' +
                                        tcf[i][1] +
                                        "</option>";
                                } else {
                                    q5ii_sol_speaking +=
                                        '<option value="' +
                                        tcf[i][0] +
                                        '"">' +
                                        tcf[i][1] +
                                        "</option>";
                                }
                                if (
                                    lang_listening_old &&
                                    tcf[i][2] == lang_listening_old
                                ) {
                                    q5ii_sol_listening +=
                                        '<option value="' +
                                        tcf[i][0] +
                                        '" selected="selected">' +
                                        tcf[i][2] +
                                        "</option>";
                                } else {
                                    q5ii_sol_listening +=
                                        '<option value="' +
                                        tcf[i][0] +
                                        '"">' +
                                        tcf[i][2] +
                                        "</option>";
                                }
                                if (
                                    lang_reading_old &&
                                    tcf[i][3] == lang_reading_old
                                ) {
                                    q5ii_sol_reading +=
                                        '<option value="' +
                                        tcf[i][0] +
                                        '" selected="selected">' +
                                        tcf[i][3] +
                                        "</option>";
                                } else {
                                    q5ii_sol_reading +=
                                        '<option value="' +
                                        tcf[i][0] +
                                        '"">' +
                                        tcf[i][3] +
                                        "</option>";
                                }
                                if (
                                    lang_writing_old &&
                                    tcf[i][4] == lang_writing_old
                                ) {
                                    q5ii_sol_writing +=
                                        '<option value="' +
                                        tcf[i][0] +
                                        '" selected="selected">' +
                                        tcf[i][4] +
                                        "</option>";
                                } else {
                                    q5ii_sol_writing +=
                                        '<option value="' +
                                        tcf[i][0] +
                                        '"">' +
                                        tcf[i][4] +
                                        "</option>";
                                }
                            }
                        }
                    } else if (window.fol_lang == "fra") {
                        if (sol_test == "A") {
                            for (i = 0; i < celpip.length; i++) {
                                if (
                                    lang_speaking_old &&
                                    celpip[i][1] == lang_speaking_old
                                ) {
                                    q5ii_sol_speaking +=
                                        '<option value="' +
                                        celpip[i][0] +
                                        '" selected="selected">' +
                                        celpip[i][1] +
                                        "</option>";
                                } else {
                                    q5ii_sol_speaking +=
                                        '<option value="' +
                                        celpip[i][0] +
                                        '"">' +
                                        celpip[i][1] +
                                        "</option>";
                                }
                                if (
                                    lang_listening_old &&
                                    celpip[i][2] == lang_listening_old
                                ) {
                                    q5ii_sol_listening +=
                                        '<option value="' +
                                        celpip[i][0] +
                                        '" selected="selected">' +
                                        celpip[i][2] +
                                        "</option>";
                                } else {
                                    q5ii_sol_listening +=
                                        '<option value="' +
                                        celpip[i][0] +
                                        '"">' +
                                        celpip[i][2] +
                                        "</option>";
                                }
                                if (
                                    lang_reading_old &&
                                    celpip[i][3] == lang_reading_old
                                ) {
                                    q5ii_sol_reading +=
                                        '<option value="' +
                                        celpip[i][0] +
                                        '" selected="selected">' +
                                        celpip[i][3] +
                                        "</option>";
                                } else {
                                    q5ii_sol_reading +=
                                        '<option value="' +
                                        celpip[i][0] +
                                        '"">' +
                                        celpip[i][3] +
                                        "</option>";
                                }
                                if (
                                    lang_writing_old &&
                                    celpip[i][4] == lang_writing_old
                                ) {
                                    q5ii_sol_writing +=
                                        '<option value="' +
                                        celpip[i][0] +
                                        '" selected="selected">' +
                                        celpip[i][4] +
                                        "</option>";
                                } else {
                                    q5ii_sol_writing +=
                                        '<option value="' +
                                        celpip[i][0] +
                                        '"">' +
                                        celpip[i][4] +
                                        "</option>";
                                }
                            }
                        } else if (sol_test == "B") {
                            for (i = 0; i < ielts.length; i++) {
                                if (
                                    lang_speaking_old &&
                                    ielts[i][1] == lang_speaking_old
                                ) {
                                    q5ii_sol_speaking +=
                                        '<option value="' +
                                        ielts[i][0] +
                                        '" selected="selected">' +
                                        ielts[i][1] +
                                        "</option>";
                                } else {
                                    q5ii_sol_speaking +=
                                        '<option value="' +
                                        ielts[i][0] +
                                        '"">' +
                                        ielts[i][1] +
                                        "</option>";
                                }
                                if (
                                    lang_listening_old &&
                                    ielts[i][2] == lang_listening_old
                                ) {
                                    q5ii_sol_listening +=
                                        '<option value="' +
                                        ielts[i][0] +
                                        '" selected="selected">' +
                                        ielts[i][2] +
                                        "</option>";
                                } else {
                                    q5ii_sol_listening +=
                                        '<option value="' +
                                        ielts[i][0] +
                                        '"">' +
                                        ielts[i][2] +
                                        "</option>";
                                }
                                if (
                                    lang_reading_old &&
                                    ielts[i][3] == lang_reading_old
                                ) {
                                    q5ii_sol_reading +=
                                        '<option value="' +
                                        ielts[i][0] +
                                        '" selected="selected">' +
                                        ielts[i][3] +
                                        "</option>";
                                } else {
                                    q5ii_sol_reading +=
                                        '<option value="' +
                                        ielts[i][0] +
                                        '"">' +
                                        ielts[i][3] +
                                        "</option>";
                                }
                                if (
                                    lang_writing_old &&
                                    ielts[i][4] == lang_writing_old
                                ) {
                                    q5ii_sol_writing +=
                                        '<option value="' +
                                        ielts[i][0] +
                                        '" selected="selected">' +
                                        ielts[i][4] +
                                        "</option>";
                                } else {
                                    q5ii_sol_writing +=
                                        '<option value="' +
                                        ielts[i][0] +
                                        '"">' +
                                        ielts[i][4] +
                                        "</option>";
                                }
                            }
                        }
                    }
                    // if (fol_lang == "eng") {
                    //     if (sol_test == "A") {
                    //         for (i = 0; i < tef.length; i++) {
                    //             q5ii_sol_speaking += '<option value="' + tef[i][0] + '"">' + tef[i][1] + '</option>';
                    //             q5ii_sol_listening += '<option value="' + tef[i][0] + '"">' + tef[i][2] + '</option>';
                    //             q5ii_sol_reading += '<option value="' + tef[i][0] + '"">' + tef[i][3] + '</option>';
                    //             q5ii_sol_writing += '<option value="' + tef[i][0] + '"">' + tef[i][4] + '</option>';
                    //         }
                    //     } else if (sol_test == "B") {
                    //         for (i = 0; i < tcf.length; i++) {
                    //             q5ii_sol_speaking += '<option value="' + tcf[i][0] + '"">' + tcf[i][1] + '</option>';
                    //             q5ii_sol_listening += '<option value="' + tcf[i][0] + '"">' + tcf[i][2] + '</option>';
                    //             q5ii_sol_reading += '<option value="' + tcf[i][0] + '"">' + tcf[i][3] + '</option>';
                    //             q5ii_sol_writing += '<option value="' + tcf[i][0] + '"">' + tcf[i][4] + '</option>';
                    //         }
                    //     }
                    // } else if (fol_lang == "fra") {
                    //     if (sol_test == "A") {
                    //         for (i = 0; i < celpip.length; i++) {
                    //             q5ii_sol_speaking += '<option value="' + celpip[i][0] + '"">' + celpip[i][1] + '</option>';
                    //             q5ii_sol_listening += '<option value="' + celpip[i][0] + '"">' + celpip[i][2] + '</option>';
                    //             q5ii_sol_reading += '<option value="' + celpip[i][0] + '"">' + celpip[i][3] + '</option>';
                    //             q5ii_sol_writing += '<option value="' + celpip[i][0] + '"">' + celpip[i][4] + '</option>';
                    //         }
                    //     } else if (sol_test == "B") {
                    //         for (i = 0; i < ielts.length; i++) {
                    //             q5ii_sol_speaking += '<option value="' + ielts[i][0] + '"">' + ielts[i][1] + '</option>';
                    //             q5ii_sol_listening += '<option value="' + ielts[i][0] + '"">' + ielts[i][2] + '</option>';
                    //             q5ii_sol_reading += '<option value="' + ielts[i][0] + '"">' + ielts[i][3] + '</option>';
                    //             q5ii_sol_writing += '<option value="' + ielts[i][0] + '"">' + ielts[i][4] + '</option>';
                    //         }
                    //     }
                    // }

                    $('select[name="lang_speaking"]').append(q5ii_sol_speaking);
                    $('select[name="lang_listening"]').append(
                        q5ii_sol_listening
                    );
                    $('select[name="lang_reading"]').append(q5ii_sol_reading);
                    $('select[name="lang_writing"]').append(q5ii_sol_writing);
                } else {
                    // document.getElementById("q5ii-b-sol").style.display = "none";
                    //$('select[name="other_lang_test"]').closest('.row').hide();
                    $('select[name="lang_speaking"]').closest(".row").hide();
                    $('select[name="lang_listening"]').closest(".row").hide();
                    $('select[name="lang_reading"]').closest(".row").hide();
                    $('select[name="lang_writing"]').closest(".row").hide();
                    // document.getElementById("q6-work-xp").style.display = "block";
                    $('select[name="work_experience_canada"]')
                        .closest(".row")
                        .show();
                }
            } else {
                //$('select[name="other_lang_test"]').closest('.row').hide();
                // document.getElementById("q5ii-b-sol").style.display = "none";
                $('select[name="lang_speaking"]').closest(".row").hide();
                $('select[name="lang_listening"]').closest(".row").hide();
                $('select[name="lang_reading"]').closest(".row").hide();
                $('select[name="lang_writing"]').closest(".row").hide();
            }
        });

        var display_q6_s = false;
        var display_q6_l = false;
        var display_q6_r = false;
        var display_q6_w = false;

        // $("#q5ii-sol-speaking")
        $('select[name="lang_speaking"]').change(function () {
            var sol_speaking_option = $(
                'select[name="lang_speaking"] option:selected'
            ).val();
            // console.log($('select[name="lang_speaking"] option:selected').text() +" :tt: " +display_q6_s);
            if (sol_speaking_option != "badvalue") {
                display_q6_s = true;
            } else {
                display_q6_s = false;
            }
            display_q6();
        });
        // $("#q5ii-sol-listening")
        $('select[name="lang_listening"]').change(function () {
            var sol_listening_option = $(
                'select[name="lang_listening"] option:selected'
            ).val();
            if (sol_listening_option != "badvalue") {
                display_q6_l = true;
            } else {
                display_q6_l = false;
            }
            display_q6();
        });
        // $("#q5ii-sol-reading")
        $('select[name="lang_reading"]').change(function () {
            var sol_reading_option = $(
                'select[name="lang_reading"] option:selected'
            ).val();
            if (sol_reading_option != "badvalue") {
                display_q6_r = true;
            } else {
                display_q6_r = false;
            }
            display_q6();
        });
        //$("#q5ii-sol-writing")
        $('select[name="lang_writing"]').change(function () {
            var sol_writing_option = $(
                'select[name="lang_writing"] option:selected'
            ).val();
            if (sol_writing_option != "badvalue") {
                display_q6_w = true;
            } else {
                display_q6_w = false;
            }
            display_q6();
        });

        function display_q6() {
            if (
                display_q6_s == true &&
                display_q6_l == true &&
                display_q6_r == true &&
                display_q6_w == true
            ) {
                // console.log("test: ");
                $('select[name="work_experience_canada"]')
                    .closest(".row")
                    .show();
                // document.getElementById("q6-work-xp").style.display = "block";
            } else {
                $('select[name="work_experience_canada"]')
                    .closest(".row")
                    .hide();
                // document.getElementById("q6-work-xp").style.display = "none";
                // $('#q6-work-xp')
                $('select[name="work_experience_canada"]').prop(
                    "selectedIndex",
                    0
                );
            }
        }

        // $("#q6i")
        $('select[name="work_experience_canada"]').change(function () {
            var x = $(
                'select[name="work_experience_canada"] option:selected'
            ).val();
            if (x) {
                // work_experience_abroad
                $('select[name="work_experience_abroad"]')
                    .closest(".row")
                    .show();
                //document.getElementById("q6ii-foreign").style.display = "block";
            } else {
                $('select[name="work_experience_abroad"]')
                    .closest(".row")
                    .hide();
                //document.getElementById("q6ii-foreign").style.display = "none";
                $('select[name="work_experience_abroad"]').prop(
                    "selectedIndex",
                    0
                );
            }
        });

        // $("#q6ii")
        $('select[name="work_experience_abroad"]').change(function () {
            var x = $(
                'select[name="work_experience_abroad"] option:selected'
            ).val();
            if (x) {
                // certificate_of_competence
                $('select[name="certificate_of_competence"]')
                    .closest(".row")
                    .show();
                //document.getElementById("q7-certificate").style.display = "block";
            } else {
                $('select[name="certificate_of_competence"]')
                    .closest(".row")
                    .hide();
                //document.getElementById("q7-certificate").style.display = "none";
                $('select[name="certificate_of_competence"]').prop(
                    "selectedIndex",
                    0
                );
            }
        });

        // $("#q7")
        $('select[name="certificate_of_competence"]').change(function () {
            var x = $(
                'select[name="certificate_of_competence"] option:selected'
            ).val();
            if (x) {
                $('select[name="valid_job"]').closest(".row").show();
                //document.getElementById("q8-offer").style.display = "block";
            } else {
                $('select[name="valid_job"]').closest(".row").hide();
                //document.getElementById("q8-offer").style.display = "none";
                $('select[name="valid_job"]').prop("selectedIndex", 0);
            }
        });

        // q8
        $('select[name="valid_job"]').change(function () {
            var x = $('select[name="valid_job"] option:selected').val();
            if (x) {
                if (x == "B") {
                    $('select[name="job_kind"]').closest(".row").show();
                    $('select[name="certificate_of_designation"]')
                        .closest(".row")
                        .hide();
                    // document.getElementById("q8-noc").style.display = "block";
                    // document.getElementById("q9-nomination").style.display = "none";
                } else {
                    $('select[name="job_kind"]').closest(".row").hide();
                    $('select[name="certificate_of_designation"]')
                        .closest(".row")
                        .show();
                    // document.getElementById("q8-noc").style.display = "none";
                    // document.getElementById("q9-nomination").style.display = "block";
                    $('select[name="job_kind"]').prop("selectedIndex", 0);
                }
            } else {
                $('select[name="job_kind"]').closest(".row").hide();
                $('select[name="certificate_of_designation"]')
                    .closest(".row")
                    .hide();
                // document.getElementById("q8-noc").style.display = "none";
                // document.getElementById("q9-nomination").style.display = "none";
            }
        });

        // $("#q8a")
        $('select[name="job_kind"]').change(function () {
            var x = $('select[name="job_kind"] option:selected').val();
            if (x) {
                $('select[name="certificate_of_designation"]')
                    .closest(".row")
                    .show();
                //document.getElementById("q9-nomination").style.display = "block";
            } else {
                $('select[name="certificate_of_designation"]')
                    .closest(".row")
                    .hide();
                //document.getElementById("q9-nomination").style.display = "none";
            }
        });

        // $("#q9")
        $('select[name="certificate_of_designation"]').change(function () {
            $('select[name="sibling_canada"]').closest(".row").show();
            //document.getElementById("q10-sibling").style.display = "block";
        });

        // $("#q10i")
        /* ------- */
        $('select[name="sibling_canada"]').change(function () {
            var x = $('select[name="sibling_canada"] option:selected').val();
            //console.log(x);
            if (x) {
                show_spouse = true;
                //document.getElementById("q10-s-education").style.display = "block";
                $('select[name="spouse_education_level"]')
                    .closest(".row")
                    .show();
                //Travelling with spouse
                if (with_spouse == true) {
                    //document.getElementById("q10-s-education").style.display = "block";
                    $('select[name="spouse_education_level"]')
                        .closest(".row")
                        .show();
                } //travelling witout spouse
                else {
                    $('select[name="spouse_education_level"]')
                        .closest(".row")
                        .hide();
                    //document.getElementById("q10-s-education").style.display = "none";
                    //document.getElementById("results").style.display = "block";
                    //$("#submit").removeClass("disabled");
                    //$('#update-customer-btn').show();
                    calc_score();
                    //$('#results').show();
                }
            } else {
                // document.getElementById("results").style.display = "none";
                // $("#submit").addClass("disabled");
                //$('#update-customer-btn').hide();
                //$('#results').hide();
            }
        });

        // $("#q10")
        $('select[name="spouse_education_level"]').change(function () {
            var x = $(
                'select[name="spouse_education_level"] option:selected'
            ).val();
            if (x) {
                $('select[name="spouse_work_experience_canada"]')
                    .closest(".row")
                    .show();
                // document.getElementById("q11-s-work-xp").style.display = "block";
            } else {
                $('select[name="spouse_work_experience_canada"]')
                    .closest(".row")
                    .hide();
                // document.getElementById("q11-s-work-xp").style.display = "none";
            }
        });
        // spouse_work_experience_canada
        //$("#q11")
        $('select[name="spouse_work_experience_canada"]').change(function () {
            var x = $(
                'select[name="spouse_work_experience_canada"] option:selected'
            ).val();
            if (x) {
                $('select[name="spouse_lang_test"]').closest(".row").show();
                // document.getElementById("q12-s-fol").style.display = "block";
            } else {
                $('select[name="spouse_lang_test"]').closest(".row").hide();
                //document.getElementById("q12-s-fol").style.display = "none";
            }
        });
        // spouse_lang_test
        // $("#q12i")
        $('select[name="spouse_lang_test"]').change(function () {
            var s_fol_test = $(
                'select[name="spouse_lang_test"] option:selected'
            ).val();

            let lang_speaking_old = $(
                'select[name="spouse_lang_speaking"]'
            ).data("old");
            let lang_listening_old = $(
                'select[name="spouse_lang_listening"]'
            ).data("old");
            let lang_reading_old = $('select[name="spouse_lang_reading"]').data(
                "old"
            );
            let lang_writing_old = $('select[name="spouse_lang_writing"]').data(
                "old"
            );
            // $("#q12ii-fol-speaking")
            $('select[name="spouse_lang_speaking"]').empty();
            //$("#q12ii-fol-listening")
            $('select[name="spouse_lang_listening"]').empty();
            //$("#q12ii-fol-reading")
            $('select[name="spouse_lang_reading"]').empty();
            //$("#q12ii-fol-writing")
            $('select[name="spouse_lang_writing"]').empty();

            // console.log("line 3267", select_placeholder);

            var q12i_speaking =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";
            var q12i_listening =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";
            var q12i_reading =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";
            var q12i_writing =
                '<option value="badvalue" label="' +
                `${select_placeholder}` +
                '">' +
                `${select_placeholder}` +
                "</option>";

            if (s_fol_test) {
                // $("#submit").addClass("disabled");
                //$('#update-customer-btn').hide();
                //document.getElementById("q12ii-s-fol").style.display = "block";
                $('select[name="spouse_lang_speaking"]').closest(".row").show();
                $('select[name="spouse_lang_listening"]')
                    .closest(".row")
                    .show();
                $('select[name="spouse_lang_reading"]').closest(".row").show();
                $('select[name="spouse_lang_writing"]').closest(".row").show();
                // if (s_fol_test == "A") {
                //     for (i = 0; i < celpip.length; i++) {
                //         q12i_speaking += '<option value="' + celpip[i][0] + '"">' + celpip[i][1] + '</option>';
                //         q12i_listening += '<option value="' + celpip[i][0] + '"">' + celpip[i][2] + '</option>';
                //         q12i_reading += '<option value="' + celpip[i][0] + '"">' + celpip[i][3] + '</option>';
                //         q12i_writing += '<option value="' + celpip[i][0] + '"">' + celpip[i][4] + '</option>';
                //     }
                // } else if (s_fol_test == "B") {
                //     for (i = 0; i < ielts.length; i++) {
                //         q12i_speaking += '<option value="' + ielts[i][0] + '"">' + ielts[i][1] + '</option>';
                //         q12i_listening += '<option value="' + ielts[i][0] + '"">' + ielts[i][2] + '</option>';
                //         q12i_reading += '<option value="' + ielts[i][0] + '"">' + ielts[i][3] + '</option>';
                //         q12i_writing += '<option value="' + ielts[i][0] + '"">' + ielts[i][4] + '</option>';
                //     }
                // } else if (s_fol_test == "C") {
                //     for (i = 0; i < tef.length; i++) {
                //         q12i_speaking += '<option value="' + tef[i][0] + '"">' + tef[i][1] + '</option>';
                //         q12i_listening += '<option value="' + tef[i][0] + '"">' + tef[i][2] + '</option>';
                //         q12i_reading += '<option value="' + tef[i][0] + '"">' + tef[i][3] + '</option>';
                //         q12i_writing += '<option value="' + tef[i][0] + '"">' + tef[i][4] + '</option>';
                //     }
                // } else if (s_fol_test == "D") {
                //     for (i = 0; i < tcf.length; i++) {
                //         q12i_speaking += '<option value="' + tcf[i][0] + '"">' + tcf[i][1] + '</option>';
                //         q12i_listening += '<option value="' + tcf[i][0] + '"">' + tcf[i][2] + '</option>';
                //         q12i_reading += '<option value="' + tcf[i][0] + '"">' + tcf[i][3] + '</option>';
                //         q12i_writing += '<option value="' + tcf[i][0] + '"">' + tcf[i][4] + '</option>';
                //     }
                // }
                if (s_fol_test == "A") {
                    for (i = 0; i < celpip.length; i++) {
                        if (
                            lang_speaking_old &&
                            celpip[i][1] == lang_speaking_old
                        ) {
                            q12i_speaking +=
                                '<option value="' +
                                celpip[i][0] +
                                '" selected="selected">' +
                                celpip[i][1] +
                                "</option>";
                        } else {
                            q12i_speaking +=
                                '<option value="' +
                                celpip[i][0] +
                                '"">' +
                                celpip[i][1] +
                                "</option>";
                        }
                        if (
                            lang_listening_old &&
                            celpip[i][1] == lang_listening_old
                        ) {
                            q12i_listening +=
                                '<option value="' +
                                celpip[i][0] +
                                '" selected="selected">' +
                                celpip[i][2] +
                                "</option>";
                        } else {
                            q12i_listening +=
                                '<option value="' +
                                celpip[i][0] +
                                '"">' +
                                celpip[i][2] +
                                "</option>";
                        }
                        if (
                            lang_reading_old &&
                            celpip[i][1] == lang_reading_old
                        ) {
                            q12i_reading +=
                                '<option value="' +
                                celpip[i][0] +
                                '" selected="selected">' +
                                celpip[i][3] +
                                "</option>";
                        } else {
                            q12i_reading +=
                                '<option value="' +
                                celpip[i][0] +
                                '"">' +
                                celpip[i][3] +
                                "</option>";
                        }
                        if (
                            lang_writing_old &&
                            celpip[i][1] == lang_writing_old
                        ) {
                            q12i_writing +=
                                '<option value="' +
                                celpip[i][0] +
                                '" selected="selected">' +
                                celpip[i][4] +
                                "</option>";
                        } else {
                            q12i_writing +=
                                '<option value="' +
                                celpip[i][0] +
                                '"">' +
                                celpip[i][4] +
                                "</option>";
                        }
                    }
                } else if (s_fol_test == "B") {
                    for (i = 0; i < ielts.length; i++) {
                        if (
                            lang_speaking_old &&
                            ielts[i][1] == lang_speaking_old
                        ) {
                            q12i_speaking +=
                                '<option value="' +
                                ielts[i][0] +
                                '" selected="selected">' +
                                ielts[i][1] +
                                "</option>";
                        } else {
                            q12i_speaking +=
                                '<option value="' +
                                ielts[i][0] +
                                '"">' +
                                ielts[i][1] +
                                "</option>";
                        }
                        if (
                            lang_listening_old &&
                            ielts[i][1] == lang_listening_old
                        ) {
                            q12i_listening +=
                                '<option value="' +
                                ielts[i][0] +
                                '" selected="selected">' +
                                ielts[i][2] +
                                "</option>";
                        } else {
                            q12i_listening +=
                                '<option value="' +
                                ielts[i][0] +
                                '"">' +
                                ielts[i][2] +
                                "</option>";
                        }
                        if (
                            lang_reading_old &&
                            ielts[i][1] == lang_reading_old
                        ) {
                            q12i_reading +=
                                '<option value="' +
                                ielts[i][0] +
                                '" selected="selected">' +
                                ielts[i][3] +
                                "</option>";
                        } else {
                            q12i_reading +=
                                '<option value="' +
                                ielts[i][0] +
                                '"">' +
                                ielts[i][3] +
                                "</option>";
                        }
                        if (
                            lang_writing_old &&
                            ielts[i][1] == lang_writing_old
                        ) {
                            q12i_writing +=
                                '<option value="' +
                                ielts[i][0] +
                                '" selected="selected">' +
                                ielts[i][4] +
                                "</option>";
                        } else {
                            q12i_writing +=
                                '<option value="' +
                                ielts[i][0] +
                                '"">' +
                                ielts[i][4] +
                                "</option>";
                        }
                    }
                } else if (s_fol_test == "C") {
                    for (i = 0; i < tef.length; i++) {
                        if (
                            lang_speaking_old &&
                            tef[i][1] == lang_speaking_old
                        ) {
                            q12i_speaking +=
                                '<option value="' +
                                tef[i][0] +
                                '" selected="selected">' +
                                tef[i][1] +
                                "</option>";
                        } else {
                            q12i_speaking +=
                                '<option value="' +
                                tef[i][0] +
                                '"">' +
                                tef[i][1] +
                                "</option>";
                        }
                        if (
                            lang_listening_old &&
                            tef[i][1] == lang_listening_old
                        ) {
                            q12i_listening +=
                                '<option value="' +
                                tef[i][0] +
                                '" selected="selected">' +
                                tef[i][2] +
                                "</option>";
                        } else {
                            q12i_listening +=
                                '<option value="' +
                                tef[i][0] +
                                '"">' +
                                tef[i][2] +
                                "</option>";
                        }
                        if (lang_reading_old && tef[i][1] == lang_reading_old) {
                            q12i_reading +=
                                '<option value="' +
                                tef[i][0] +
                                '" selected="selected">' +
                                tef[i][3] +
                                "</option>";
                        } else {
                            q12i_reading +=
                                '<option value="' +
                                tef[i][0] +
                                '"">' +
                                tef[i][3] +
                                "</option>";
                        }
                        if (lang_writing_old && tef[i][1] == lang_writing_old) {
                            q12i_writing +=
                                '<option value="' +
                                tef[i][0] +
                                '" selected="selected">' +
                                tef[i][4] +
                                "</option>";
                        } else {
                            q12i_writing +=
                                '<option value="' +
                                tef[i][0] +
                                '"">' +
                                tef[i][4] +
                                "</option>";
                        }
                    }
                } else if (s_fol_test == "D") {
                    for (i = 0; i < tcf.length; i++) {
                        if (
                            lang_speaking_old &&
                            tcf[i][1] == lang_speaking_old
                        ) {
                            q12i_speaking +=
                                '<option value="' +
                                tcf[i][0] +
                                '" selected="selected">' +
                                tcf[i][1] +
                                "</option>";
                        } else {
                            q12i_speaking +=
                                '<option value="' +
                                tcf[i][0] +
                                '"">' +
                                tcf[i][1] +
                                "</option>";
                        }
                        if (
                            lang_listening_old &&
                            tcf[i][1] == lang_listening_old
                        ) {
                            q12i_listening +=
                                '<option value="' +
                                tcf[i][0] +
                                '" selected="selected">' +
                                tcf[i][2] +
                                "</option>";
                        } else {
                            q12i_listening +=
                                '<option value="' +
                                tcf[i][0] +
                                '"">' +
                                tcf[i][2] +
                                "</option>";
                        }
                        if (lang_reading_old && tcf[i][1] == lang_reading_old) {
                            q12i_reading +=
                                '<option value="' +
                                tcf[i][0] +
                                '" selected="selected">' +
                                tcf[i][3] +
                                "</option>";
                        } else {
                            q12i_reading +=
                                '<option value="' +
                                tcf[i][0] +
                                '"">' +
                                tcf[i][3] +
                                "</option>";
                        }
                        if (lang_writing_old && tcf[i][1] == lang_writing_old) {
                            q12i_writing +=
                                '<option value="' +
                                tcf[i][0] +
                                '" selected="selected">' +
                                tcf[i][4] +
                                "</option>";
                        } else {
                            q12i_writing +=
                                '<option value="' +
                                tcf[i][0] +
                                '"">' +
                                tcf[i][4] +
                                "</option>";
                        }
                    }
                } else if (s_fol_test == "E") {
                    $('select[name="spouse_lang_speaking"]')
                        .closest(".row")
                        .hide();
                    $('select[name="spouse_lang_listening"]')
                        .closest(".row")
                        .hide();
                    $('select[name="spouse_lang_reading"]')
                        .closest(".row")
                        .hide();
                    $('select[name="spouse_lang_writing"]')
                        .closest(".row")
                        .hide();
                    //document.getElementById("q12ii-s-fol").style.display = "none";
                    //$("#submit").removeClass("disabled");
                    //$('#update-customer-btn').show();
                    calc_score();
                    //$('#results').show();
                }
                //$("#q12ii-fol-speaking")
                $('select[name="spouse_lang_speaking"]').append(q12i_speaking);
                // $("#q12ii-fol-listening")
                $('select[name="spouse_lang_listening"]').append(
                    q12i_listening
                );
                // $("#q12ii-fol-reading")
                $('select[name="spouse_lang_reading"]').append(q12i_reading);
                // $("#q12ii-fol-writing")
                $('select[name="spouse_lang_writing"]').append(q12i_writing);
            } else {
                //$('#update-customer-btn').hide();
                //$('#results').hide();
                // $("#submit").addClass("disabled");
            }

            var btn_sub_s = false;
            var btn_sub_l = false;
            var btn_sub_r = false;
            var btn_sub_w = false;

            //$("#q12ii-fol-speaking")
            $('select[name="spouse_lang_speaking"]').change(function () {
                var spouse_speaking = $(
                    'select[name="spouse_lang_speaking"] option:selected'
                ).val();
                if (spouse_speaking != "badvalue") {
                    btn_sub_s = true;
                } else {
                    btn_sub_s = false;
                }
                btn_sub();
            });
            // $("#q12ii-fol-listening")
            $('select[name="spouse_lang_listening"]').change(function () {
                var spouse_listening = $(
                    'select[name="spouse_lang_listening"] option:selected'
                ).val();
                if (spouse_listening != "badvalue") {
                    btn_sub_l = true;
                } else {
                    btn_sub_l = false;
                }
                btn_sub();
            });
            // $("#q12ii-fol-reading")
            $('select[name="spouse_lang_reading"]').change(function () {
                var spouse_reading = $(
                    'select[name="spouse_lang_reading"] option:selected'
                ).val();
                if (spouse_reading != "badvalue") {
                    btn_sub_r = true;
                } else {
                    btn_sub_r = false;
                }
                btn_sub();
            });
            // $("#q12ii-fol-writing")
            $('select[name="spouse_lang_writing"]').change(function () {
                var spouse_writing = $(
                    'select[name="spouse_lang_writing"] option:selected'
                ).val();
                if (spouse_writing != "badvalue") {
                    btn_sub_w = true;
                } else {
                    btn_sub_w = false;
                }
                btn_sub();
            });

            function btn_sub() {
                if (
                    btn_sub_s == true &&
                    btn_sub_l == true &&
                    btn_sub_r == true &&
                    btn_sub_w == true
                ) {
                    // $("#submit").removeClass("disabled");
                    //$('#update-customer-btn').show();
                    calc_score();
                    //$('#results').show();
                } else {
                    //$("#submit").addClass("disabled");
                    //$('#update-customer-btn').hide();
                    //$('#results').hide();
                }
            }
        });

        //Submit/Calculations
        const clear_lang = this.clear_lang;
        $("#update-customer-btn")
            .closest("form")
            .submit(function (event) {
                event.preventDefault();
                var q5i_an = $(
                    'select[name="lang_test_expire"] option:selected'
                ).val();
                if (q5i_an && q5i_an == "B") {
                    $('select[name="lang_test"]').prop("selectedIndex", 0);
                    $('select[name="lang_test_expire"]')
                        .closest(".row")
                        .nextAll()
                        .each(function () {
                            $(this).find("select").prop("selectedIndex", 0);
                        });
                }
                let url = $("#update-customer-btn")
                    .closest("form")
                    .attr("action")
                    .split("/");
                let CLEARPATH_CUSTOMER_ID = url[url.length - 1];
                let data = document.getElementById("results").innerHTML;
                $("#update-customer-btn")
                    .find("i")
                    .replaceWith(
                        '<i class="fa fa-3x fa-sun fa-spin mr-1"></i>'
                    );
                // <i class="fa fa-fw fa-check mr-1"></i>
                //console.log(CLEARPATH_CUSTOMER_ID);
                // $('select[name="lang_speaking"] option').val(function(){
                //     return $(this).text();
                // });
                // $('select[name="lang_listening"] option').val(function(){
                //     return $(this).text();
                // });
                // $('select[name="lang_reading"] option').val(function(){
                //     return $(this).text();
                // });
                // $('select[name="lang_writing"] option').val(function(){
                //     return $(this).text();
                // });
                // $('select[name="spouse_lang_speaking"] option').val(function(){
                //     return $(this).text();
                // });
                // $('select[name="spouse_lang_listening"] option').val(function(){
                //     return $(this).text();
                // });
                // $('select[name="spouse_lang_reading"] option').val(function(){
                //     return $(this).text();
                // });
                // $('select[name="spouse_lang_writing"] option').val(function(){
                //     return $(this).text();
                // });
                // $('select[name="lang_oral_exp_test_score"] option').val(function(){
                //     return $(this).text();
                // });
                // $('select[name="lang_oral_comp_test_score"] option').val(function(){
                //     return $(this).text();
                // });
                // $('select[name="lang_writing_comp_test_score"] option').val(function(){
                //     return $(this).text();
                // });
                // $('select[name="lang_writing_exp_test_score"] option').val(function(){
                //     return $(this).text();
                // });
                $('select[class*="language_test_score"] option').val(
                    function () {
                        //if ($(this).find)
                        return $(this).text();
                    }
                );
                $.ajax({
                    method: "put",
                    url: `${process.env.MIX_API_URL}/customers/clear1004/${CLEARPATH_CUSTOMER_ID}`,
                    data:
                        $("#update-customer-btn").closest("form").serialize() +
                        "&scoring=" +
                        (window.factors ? JSON.stringify(window.factors) : 0),
                    success: (data) => {
                        Swal.fire(
                            clear_lang === "fr" ? "Mis À Jour!" : "Update!",
                            clear_lang === "fr"
                                ? "Modifié avec succés!"
                                : "Modified successfully!",
                            "success"
                        ).then(() => {
                            let url = `/lead/${data.lead_id}`;
                            $(location).attr("href", url);
                        });
                    },
                    error: () => {
                        Swal.fire(
                            clear_lang === "fr" ? "Mis À Jour!" : "Update!",
                            clear_lang === "fr"
                                ? "erreur modifcation!"
                                : "an error occurred",
                            "error"
                        ).then(() => {
                            //
                        });
                    },
                });
            });
        function calc_score() {
            var lead_lang = $('[name="lang"]').val();
            var q3 = 0;
            var q4 = 0;
            var q5i = 0;
            var q5ii = 0;
            var q6i = 0;
            var q6ii = 0;
            var q7 = 0;
            var q8 = 0;
            var q9 = 0;
            var q10i = 0;
            var q10 = 0;
            var q11 = 0;
            var q12i = 0;
            var q12ii = 0;
            var core_factors = 0;
            var spouse_factors = 0;
            var skill_factors = 0;
            var bonus_factors = 0;

            //"answer" changes with each question calculation results.
            //"z" is used as a placeholder for questions 5i, 5ii and 12; "z" becomes the array that corresponds with the FOL/SOL test.
            var answer, z;
            //document.getElementById("test_variables").innerHTML = "w/ spouse = " + with_spouse

            //Core/Human capital factors

            //Question 3
            answer = $('select[name="age"] option:selected').val();
            for (i = 0; i < q3_age.length; i++) {
                if (answer == q3_age[i][0]) {
                    if (with_spouse == true) {
                        q3 = q3_age[i][2];
                    } else {
                        q3 = q3_age[i][3];
                    }
                }
            }
            // console.log("Age: " + q3);
            core_factors += parseInt(q3);
            //document.getElementById("test_q3").innerHTML = "Question 3 = " + answer + " || q3 = " + q3;

            //Question 4
            //answer = $("#q4 option:selected").val();
            answer = $('select[name="education_level"] option:selected').val();
            for (i = 0; i < q4_education.length; i++) {
                if (answer == q4_education[i][0]) {
                    if (with_spouse == true) {
                        q4 = q4_education[i][2];
                    } else {
                        q4 = q4_education[i][3];
                    }
                }
            }
            core_factors += parseInt(q4);
            //document.getElementById("test_q4").innerHTML = "Question 4 = " + answer + " || q4 = " + q4;

            //canadian study experience
            var q4c_answer;
            var temp_q4c_answer = 0;
            // education_type
            q4c_answer = $(
                'select[name="education_type"] option:selected'
            ).val();
            //q4c_answer = $("#q4c option:selected").val();
            if (q4c_answer == "B") {
                temp_q4c_answer = 15;
            } else if (q4c_answer == "C") {
                temp_q4c_answer = 30;
            }
            //		core_factors += parseInt(temp_q4c_answer);

            //Question 5i
            // lang_test
            // answer = $("#q5i-a option:selected").val();
            answer = $('select[name="lang_test"] option:selected').val();
            // console.log(answer);
            if (answer == "A") {
                z = celpip;
                other_answer = "";
            } else if (answer == "B") {
                z = ielts;
            } else if (answer == "C") {
                z = tef;
            } else if (answer == "D") {
                z = tcf;
            }

            var q5_s, q5_l, q5_r, q5_w;
            q5_s = $(
                'select[name="lang_oral_exp_test_score"] option:selected'
            ).val();
            q5_l = $(
                'select[name="lang_oral_comp_test_score"] option:selected'
            ).val();
            q5_r = $(
                'select[name="lang_writing_comp_test_score"] option:selected'
            ).val();
            q5_w = $(
                'select[name="lang_writing_exp_test_score"] option:selected'
            ).val();
            // q5_s = $("#q5i-b-speaking option:selected").val();
            // q5_l = $("#q5i-b-listening option:selected").val();
            // q5_r = $("#q5i-b-reading option:selected").val();
            // q5_w = $("#q5i-b-writing option:selected").val();

            var clb_s, clb_l, clb_r, clb_w;

            for (i = 0; i < z.length; i++) {
                if (q5_s == z[i][0]) {
                    if (with_spouse == true) {
                        q5i += parseInt(z[i][6]);
                    } else {
                        q5i += parseInt(z[i][7]);
                    }
                    clb_s = parseInt(z[i][5]);
                }
                if (q5_l == z[i][0]) {
                    if (with_spouse == true) {
                        q5i += parseInt(z[i][6]);
                    } else {
                        q5i += parseInt(z[i][7]);
                    }
                    clb_l = parseInt(z[i][5]);
                }
                if (q5_r == z[i][0]) {
                    if (with_spouse == true) {
                        q5i += parseInt(z[i][6]);
                    } else {
                        q5i += parseInt(z[i][7]);
                    }
                    clb_r = parseInt(z[i][5]);
                }
                if (q5_w == z[i][0]) {
                    if (with_spouse == true) {
                        q5i += parseInt(z[i][6]);
                    } else {
                        q5i += parseInt(z[i][7]);
                    }
                    clb_w = parseInt(z[i][5]);
                }
            }

            core_factors += parseInt(q5i);
            // document.getElementById("test_q5i").innerHTML = "Question 5i = " + answer + " || q5i = " + q5i;
            // document.getElementById("test_q5i").innerHTML += "<br />CLBs = " + clb_s + " || CLBl = " + clb_l + " || CLBr = " + clb_r + " || CLBw = " + clb_w;

            //Question 5ii
            // $("#q5ii option:selected").val();
            answer = $('select[name="other_lang_test"] option:selected').val();
            if ((window.fol_lang = "en")) {
                if (answer == "A") {
                    z = celpip;
                } else if (answer == "B") {
                    z = ielts;
                }
            } else {
                if (answer == "A") {
                    z = tef;
                }
                if (answer == "B") {
                    z = tcf;
                }
            }
            q5_s = $('select[name="lang_speaking"] option:selected').val();
            q5_l = $('select[name="lang_listening"] option:selected').val();
            q5_r = $('select[name="lang_reading"] option:selected').val();
            q5_w = $('select[name="lang_writing"] option:selected').val();

            // q5_s = $("#q5ii-sol-speaking option:selected").val();
            // q5_l = $("#q5ii-sol-listening option:selected").val();
            // q5_r = $("#q5ii-sol-reading option:selected").val();
            // q5_w = $("#q5ii-sol-writing option:selected").val();

            for (i = 0; i < z.length; i++) {
                if (q5_s == z[i][0]) {
                    if (with_spouse == true) {
                        q5ii += parseInt(z[i][8]);
                    } else {
                        q5ii += parseInt(z[i][9]);
                    }
                }
                if (q5_l == z[i][0]) {
                    if (with_spouse == true) {
                        q5ii += parseInt(z[i][8]);
                    } else {
                        q5ii += parseInt(z[i][9]);
                    }
                }
                if (q5_r == z[i][0]) {
                    if (with_spouse == true) {
                        q5ii += parseInt(z[i][8]);
                    } else {
                        q5ii += parseInt(z[i][9]);
                    }
                }
                if (q5_w == z[i][0]) {
                    if (with_spouse == true) {
                        q5ii += parseInt(z[i][8]);
                    } else {
                        q5ii += parseInt(z[i][9]);
                    }
                }
            }

            if (with_spouse == true) {
                if (q5ii > 22) {
                    q5ii = 22;
                }
            }
            // console.log(q5i);
            core_factors += parseInt(q5ii);
            // document.getElementById("test_q5ii").innerHTML = "Question 5ii = " + answer + " || q5ii = " + q5ii;

            //Question 6i
            // $("#q6i option:selected")
            answer = $(
                'select[name="work_experience_canada"] option:selected'
            ).val();
            if (with_spouse == true) {
                if (answer == "A") {
                    q6i = 0;
                } else if (answer == "B") {
                    q6i = 35;
                } else if (answer == "C") {
                    q6i = 46;
                } else if (answer == "D") {
                    q6i = 56;
                } else if (answer == "E") {
                    q6i = 63;
                } else if (answer == "F") {
                    q6i = 70;
                }
            } else {
                if (answer == "A") {
                    q6i = 0;
                } else if (answer == "B") {
                    q6i = 40;
                } else if (answer == "C") {
                    q6i = 53;
                } else if (answer == "D") {
                    q6i = 64;
                } else if (answer == "E") {
                    q6i = 72;
                } else if (answer == "F") {
                    q6i = 80;
                }
            }
            core_factors += parseInt(q6i);
            //document.getElementById("test_q6i").innerHTML = "Question 6i = " + answer + " || q6i = " + q6i;
            //document.getElementById("test_q6i").innerHTML = "Question 6i = " + answer;

            //Question 6ii
            // work_experience_abroad
            //answer = $("#q6ii option:selected").val();
            answer = $(
                'select[name="work_experience_abroad"] option:selected'
            ).val();
            if (with_spouse == true) {
                if (answer == "A") {
                    q6ii = 0;
                } else if (answer == "B") {
                    q6ii = 35;
                } else if (answer == "C") {
                    q6ii = 46;
                } else if (answer == "D") {
                    q6ii = 56;
                }
            } else {
                if (answer == "A") {
                    q6ii = 0;
                } else if (answer == "B") {
                    q6ii = 40;
                } else if (answer == "C") {
                    q6ii = 52;
                } else if (answer == "D") {
                    q6ii = 64;
                }
            }
            //document.getElementById("test_q6ii").innerHTML = "Question 6ii = " + answer + " || q6ii = " + q6ii;

            //Question 9
            //answer = $("#q9 option:selected").val();
            answer = $(
                'select[name="certificate_of_designation"] option:selected'
            ).val();
            if (answer == "A") {
                q9 = 0;
            } else {
                q9 = 600;
            }

            /************ START - CORE/HUMAN CAPITAL FACTORS ************/
            if (lead_lang === "fr") {
                $("#core_factors").show();
                document.getElementById("core_factors").innerHTML =
                    '<h3><strong>Facteurs de base/du capital humain</strong></h3><ul class="list-unstyled"><li>Age = ' +
                    q3 +
                    "</li><li>Niveau de scolarité = " +
                    q4 +
                    "</li><li>Langues officielles = " +
                    (q5i + q5ii) +
                    '<ul class="lst-no-blt"><li><em>Première langue officielle</em> = ' +
                    q5i +
                    "</li><li><em>Deuxième langue officielle </em> = " +
                    q5ii +
                    "</li></ul></li><li>Expérience de travail au Canada = " +
                    q6i +
                    "</li></ul><p><strong>Sous-total - Facteurs de base/du capital humain</strong> = " +
                    core_factors +
                    "</p>";

                var factors = [
                    {
                        title: "Facteurs de base/du capital humain",
                        factors: {
                            keys: [
                                "Age",
                                "Niveau de scolarité",
                                "Langues officielles",
                                "Première langue officielle",
                                "Deuxième langue officielle",
                                "Expérience de travail au Canada",
                            ],
                            values: [q3, q4, q5i + q5ii, q5i, q5ii, q6i],
                        },
                        total: core_factors,
                    },
                ];
            } else {
                $("#core_factors").show();
                document.getElementById("core_factors").innerHTML =
                    '<h3><strong>Basic/human capital factors</strong></h3><ul class="list-unstyled"><li>Age = ' +
                    q3 +
                    "</li><li>Education level = " +
                    q4 +
                    "</li><li>Official languages = " +
                    (q5i + q5ii) +
                    '<ul class="lst-no-blt"><li><em>First official language</em> = ' +
                    q5i +
                    "</li><li><em>Second official language </em> = " +
                    q5ii +
                    "</li></ul></li><li>Canadian work experience = " +
                    q6i +
                    "</li></ul><p><strong>Subtotal - Basic/human capital factors</strong> = " +
                    core_factors +
                    "</p>";

                var factors = [
                    {
                        title: "Basic/human capital factors",
                        factors: {
                            keys: [
                                "Age",
                                "Education level",
                                "Langues officielles",
                                "First official language",
                                "Second official language",
                                "Canadian work experience",
                            ],
                            values: [q3, q4, q5i + q5ii, q5i, q5ii, q6i],
                        },
                        total: core_factors,
                    },
                ];
            }

            /************ END - CORE/HUMAN CAPITAL FACTORS ************/

            /************ START - SPOUSE FACTORS ************/
            //Question 10
            // spouse_education_level
            // $("#q10 option:selected").val();
            answer = $(
                'select[name="spouse_education_level"] option:selected'
            ).val();
            if (answer == "A") {
                q10 = 0;
            } else if (answer == "B") {
                q10 = 2;
            } else if (answer == "C") {
                q10 = 6;
            } else if (answer == "D") {
                q10 = 7;
            } else if (answer == "E") {
                q10 = 8;
            } else if (answer == "F") {
                q10 = 9;
            } else if (answer == "G") {
                q10 = 10;
            } else if (answer == "H") {
                q10 = 10;
            }
            //document.getElementById("test_q10").innerHTML = "Question 10 = " + answer;

            //Question 11
            // $('select[name="spouse_work_experience_canada"]')
            // $("#q11 option:selected").val();
            answer = $(
                'select[name="spouse_work_experience_canada"] option:selected'
            ).val();
            if (answer == "A") {
                q11 = 0;
            } else if (answer == "B") {
                q11 = 5;
            } else if (answer == "C") {
                q11 = 7;
            } else if (answer == "D") {
                q11 = 8;
            } else if (answer == "E") {
                q11 = 9;
            } else if (answer == "F") {
                q11 = 10;
            }
            //document.getElementById("test_q11").innerHTML = "Question 11 = " + answer;

            //Question 12i
            // $("#q12i option:selected").val();
            answer = $('select[name="spouse_lang_test"] option:selected').val();
            if (answer == "A") {
                z = celpip;
                other_answer = "";
            } else if (answer == "B") {
                z = ielts;
            } else if (answer == "C") {
                z = tef;
            } else if (answer == "D") {
                z = tcf;
            }

            var q12_s, q12_l, q12_r, q12_w;
            //$("#q12ii-fol-speaking option:selected").val();
            q12_s = $(
                'select[name="spouse_lang_speaking"] option:selected'
            ).val();
            // $("#q12ii-fol-listening option:selected").val();
            q12_l = $(
                'select[name="spouse_lang_listening"] option:selected'
            ).val();
            // $("#q12ii-fol-reading option:selected").val();
            q12_r = $(
                'select[name="spouse_lang_reading"] option:selected'
            ).val();
            // $("#q12ii-fol-writing option:selected").val();
            q12_w = $(
                'select[name="spouse_lang_writing"] option:selected'
            ).val();

            var s_clb_s, s_clb_l, s_clb_r, s_clb_w;

            for (i = 0; i < z.length; i++) {
                if (q12_s == z[i][0]) {
                    q12i += parseInt(z[i][10]);
                    s_clb_s = parseInt(z[i][5]);
                }
                if (q12_l == z[i][0]) {
                    q12i += parseInt(z[i][10]);
                    s_clb_l = parseInt(z[i][5]);
                }
                if (q12_r == z[i][0]) {
                    q12i += parseInt(z[i][10]);
                    s_clb_r = parseInt(z[i][5]);
                }
                if (q12_w == z[i][0]) {
                    q12i += parseInt(z[i][10]);
                    s_clb_w = parseInt(z[i][5]);
                }
            }
            //document.getElementById("test_q12i").innerHTML = "Question 12i = " + answer + " || q12i = " + q12i;
            //document.getElementById("test_q12ii").innerHTML = "Question 12ii = CLBs = " + s_clb_s + " || CLBl = " + s_clb_l + " || CLBr = " + s_clb_r + " || CLBw = " + s_clb_w;

            let spouse = {};
            if (lead_lang === "fr") {
                spouse_factors = q10 + q11 + q12i;
                document.getElementById("spouse_factors").innerHTML =
                    '<h3>Facteurs de l&rsquo;époux ou du conjoint de fait</h3><ul class="list-unstyled"><li>Niveau de scolarité = ' +
                    q10 +
                    "</li><li>Première langue officielle = " +
                    q12i +
                    "</li><li>Expérience de travail au Canada = " +
                    q11 +
                    "</li></ul><p><strong>Sous-total - Facteurs de l&rsquo;époux</strong> = " +
                    spouse_factors +
                    "</p>";
                $("#spouse_factors").show();

                spouse = {
                    title: "Facteurs de l’époux ou du conjoint de fait",
                    factors: {
                        keys: [
                            "Niveau de scolarité",
                            "Première langue officielle",
                            "Expérience de travail au Canada",
                        ],
                        values: [q10, q12i, q11],
                    },
                    total: spouse_factors,
                };
            } else {
                spouse_factors = q10 + q11 + q12i;
                document.getElementById("spouse_factors").innerHTML =
                    '<h3>Spouse or common-law partner factors</h3><ul class="list-unstyled"><li>Education level = ' +
                    q10 +
                    "</li><li>First official language = " +
                    q12i +
                    "</li><li>Work experience in Canada = " +
                    q11 +
                    "</li></ul><p><strong>Subtotal  - Spouse or common-law partner factors</strong> = " +
                    spouse_factors +
                    "</p>";
                $("#spouse_factors").show();

                spouse = {
                    title: "Spouse or common-law partner factors",
                    factors: {
                        keys: [
                            "Education level",
                            "First official language",
                            "Work experience in Canada",
                        ],
                        values: [q10, q12i, q11],
                    },
                    total: spouse_factors,
                };
            }

            factors.push(spouse);
            // console.log(spouse);
            //console.log(JSON.stringify(factors));

            /************ END - SPOUSE FACTORS ************/

            /************ START - SKILL FACTORS ************/

            /************ START - Education A (With good OL proficiency and a post-secondary degree) ************/
            // $("#q4 option:selected").val();
            var q4_answer = $(
                'select[name="education_level"] option:selected'
            ).val();
            var educationA = 0;

            // calculates number of points awarded based on CLB score
            // If education level option A or B, no points are awarded.
            if (q4_answer == "A" || q4_answer == "B") {
                educationA = 0;
            } /*
                else if (clb_s <= 6 || clb_l < 6 || clb_r < 6 || clb_w < 6){
                    educationA = 0;
                }*/
            // If education level is C-E, and CLB levels are between seven and nine, 13 points are awarded. If CLB levels are all higher than nine, 25 points, otherwise, no points.
            else if (q4_answer == "C" || q4_answer == "D" || q4_answer == "E") {
                if (clb_s < 7 || clb_l < 7 || clb_r < 7 || clb_w < 7) {
                    educationA = 0;
                } else if (
                    clb_s >= 9 &&
                    clb_l >= 9 &&
                    clb_r >= 9 &&
                    clb_w >= 9
                ) {
                    educationA = 25;
                } else {
                    educationA = 13;
                }
            } // If education level is F-H, and CLB levels are between seven and nine, 25 points are awarded. If CLB levels are all higher than nine, 50 points, otherwise, no points.
            else if (q4_answer == "F" || q4_answer == "G" || q4_answer == "H") {
                if (clb_s < 7 || clb_l < 7 || clb_r < 7 || clb_w < 7) {
                    educationA = 0;
                } else if (
                    clb_s >= 9 &&
                    clb_l >= 9 &&
                    clb_r >= 9 &&
                    clb_w >= 9
                ) {
                    educationA = 50;
                } else {
                    educationA = 25;
                }
            }

            /************ END - Education A ************/
            /************ START - Education B (With Canadian Work Experience and a post-secondary degree) ************/
            // $("#q4 option:selected").val();
            var q4_answer = $(
                'select[name="education_level"] option:selected'
            ).val();
            // $("#q6i option:selected").val();
            var q6i_answer = $(
                'select[name="work_experience_canada"] option:selected'
            ).val();
            var educationB = 0;

            // calculates number of points awarded based on CLB score
            // If education level option A or B, no points are awarded.
            if (q4_answer == "A" || q4_answer == "B") {
                educationB = 0;
            } else if (
                q4_answer == "C" ||
                q4_answer == "D" ||
                q4_answer == "E"
            ) {
                if (q6i_answer == "A") {
                    educationB = 0;
                } else if (q6i_answer == "B") {
                    educationB = 13;
                } else {
                    educationB = 25;
                }
            } else if (
                q4_answer == "F" ||
                q4_answer == "G" ||
                q4_answer == "H"
            ) {
                if (q6i_answer == "A") {
                    educationB = 0;
                } else if (q6i_answer == "B") {
                    educationB = 25;
                } else {
                    educationB = 50;
                }
            }

            /************ END - Education B ************/
            var educationTot = 0;
            if (educationA + educationB > 50) {
                educationTot = 50;
            } else {
                educationTot = educationA + educationB;
            }

            if (educationTot > 50) {
                educationTot = 50;
            }

            // $("#q6i option:selected").val()
            var q6i_answer = $(
                'select[name="work_experience_canada"] option:selected'
            ).val();
            // $("#q6ii option:selected").val();
            var q6ii_answer = $(
                'select[name="work_experience_abroad"] option:selected'
            ).val();
            var foreign_xp_1 = 0;
            var foreign_xp_2 = 0;

            if (q6ii_answer == "A") {
                foreign_xp_1 = 0;
                foreign_xp_2 = 0;
            } else if (q6ii_answer == "B" || q6ii_answer == "C") {
                //Calcualtes OL
                if (clb_s < 7 || clb_l < 7 || clb_r < 7 || clb_w < 7) {
                    foreign_xp_1 = 0;
                } else if (
                    clb_s >= 9 &&
                    clb_l >= 9 &&
                    clb_r >= 9 &&
                    clb_w >= 9
                ) {
                    foreign_xp_1 = 25;
                } else {
                    foreign_xp_1 = 13;
                }

                //Calculates Work XP
                if (q6i_answer == "A") {
                    foreign_xp_2 = 0;
                } else if (q6i_answer == "B") {
                    foreign_xp_2 = 13;
                } else if (
                    q6i_answer == "C" ||
                    q6i_answer == "D" ||
                    q6i_answer == "E" ||
                    q6i_answer == "F"
                ) {
                    foreign_xp_2 = 25;
                }
            } else if (q6ii_answer == "D") {
                //Calculates OL
                if (clb_s < 7 || clb_l < 7 || clb_r < 7 || clb_w < 7) {
                    foreign_xp_1 = 0;
                } else if (
                    clb_s >= 9 &&
                    clb_l >= 9 &&
                    clb_r >= 9 &&
                    clb_w >= 9
                ) {
                    foreign_xp_1 = 50;
                } else {
                    foreign_xp_1 = 25;
                }

                //Calculates Work XP
                if (q6i_answer == "A") {
                    foreign_xp_2 = 0;
                } else if (q6i_answer == "B") {
                    foreign_xp_2 = 25;
                } else if (
                    q6i_answer == "C" ||
                    q6i_answer == "D" ||
                    q6i_answer == "E" ||
                    q6i_answer == "F"
                ) {
                    foreign_xp_2 = 50;
                }
            }

            var foreign_xp_tot = 0;

            if (foreign_xp_1 + foreign_xp_2 > 50) {
                foreign_xp_tot = 50;
            } else {
                foreign_xp_tot = foreign_xp_1 + foreign_xp_2;
            }
            if (foreign_xp_tot > 50) {
                foreign_xp_tot = 50;
            }

            //Certificate of qualifications
            // With good OL proficiency adn a post-secondary degree
            // $("#q7 option:selected").val();
            var q7_answer = $(
                'select[name="certificate_of_competence"] option:selected'
            ).val();
            var certificate = 0;

            if (q7_answer == "B") {
                if (clb_s < 5 || clb_l < 5 || clb_r < 5 || clb_w < 5) {
                    certificate = 0;
                } else if (
                    clb_s >= 7 &&
                    clb_l >= 7 &&
                    clb_r >= 7 &&
                    clb_w >= 7
                ) {
                    certificate = 50;
                } else {
                    certificate = 25;
                }
            } else {
                certificate = 0;
            }
            //document.getElementById("test_q7").innerHTML = "Question 7 = " + q7_answer;

            skill_factors = educationTot + foreign_xp_tot + certificate;

            if (skill_factors > 100) {
                skill_factors = 100;
            }

            if (lead_lang === "fr") {
                document.getElementById("skill_factors").innerHTML =
                    '<h3>Facteurs liés au transfert des compétences</h3><h4>Études (jusqu’à un maximum de 50 points)</h4><ul class="list-unstyled"><li>A) Maîtrise de une langue officielle et niveau de scolarité = ' +
                    educationA +
                    "</li><li>B) Expérience de travail au Canada et niveau de scolarité = " +
                    educationB +
                    "</li></ul><p><em>sous-total</em> = " +
                    educationTot +
                    '</p><h4>Expérience de travail à l&rsquo;étranger (jusqu’à un maximum de 50 points)</h4><ul class="list-unstyled"><li>A) Maîtrise de une langue officielle et de l&rsquo;expérience de travail à l&rsquo;étranger = ' +
                    foreign_xp_1 +
                    "</li><li>B) Expérience de travail au Canada et à l&rsquo;étranger = " +
                    foreign_xp_2 +
                    "</li></ul><p><em>sous-total</em> = " +
                    foreign_xp_tot +
                    "</p><p>Certificat de compétence (pour les personnes exerçant un métier spécialisé) = " +
                    certificate +
                    "</p><p><strong>Sous-total facteurs liés au transfert des compétences</strong> = " +
                    skill_factors +
                    "</p>";
                $("#skill_factors").show();

                let groups = [
                    {
                        title: "Études (jusqu’à un maximum de 50 points)",
                        factors: {
                            keys: [
                                "A) Maîtrise de une langue officielle et niveau de scolarité",
                                "B) Expérience de travail au Canada et niveau de scolarité",
                            ],
                            values: [educationA, educationB],
                        },
                        total: educationTot,
                    },
                    {
                        title: "Expérience de travail à l’étranger (jusqu’à un maximum de 50 points)",
                        factors: {
                            keys: [
                                "A) Maîtrise de une langue officielle et de l’expérience de travail à l’étranger",
                                "B) Expérience de travail au Canada et à l’étranger",
                            ],
                            values: [foreign_xp_1, foreign_xp_2],
                        },
                        total: foreign_xp_tot,
                    },
                ];
                let skill = {
                    title: "Facteurs liés au transfert des compétences",
                    factors: {
                        groups: groups,
                        keys: [
                            "Certificat de compétence (pour les personnes exerçant un métier spécialisé)",
                        ],
                        values: [certificate],
                    },
                    total: skill_factors,
                };
                factors.push(skill);
            } else {
                document.getElementById("skill_factors").innerHTML =
                    '<h3>Skills Transfer Factors</h3><h4>Education (up to a maximum of 50 points)</h4><ul class="list-unstyled"><li>A) Proficiency in an official language and level of education = ' +
                    educationA +
                    "</li><li>B) Canadian work experience and level of education = " +
                    educationB +
                    "</li></ul><p><em>Subtotal</em> = " +
                    educationTot +
                    '</p><h4>Foreign work experience (up to a maximum of 50 points)</h4><ul class="list-unstyled"><li>A) Proficiency in an official language and work experience abroad = ' +
                    foreign_xp_1 +
                    "</li><li>B) Work experience in Canada and abroad = " +
                    foreign_xp_2 +
                    "</li></ul><p><em>Subtotal</em> = " +
                    foreign_xp_tot +
                    "</p><p>Competency certificate (for people working in a skilled trade) = " +
                    certificate +
                    "</p><p><strong>Subtotal - Skills Transfer Factors</strong> = " +
                    skill_factors +
                    "</p>";
                $("#skill_factors").show();

                let groups = [
                    {
                        title: "Education (up to a maximum of 50 points)",
                        factors: {
                            keys: [
                                "A) Proficiency in an official language and level of education",
                                "B) Canadian work experience and level of education",
                            ],
                            values: [educationA, educationB],
                        },
                        total: educationTot,
                    },
                    {
                        title: "Foreign work experience (up to a maximum of 50 points)",
                        factors: {
                            keys: [
                                "A) Proficiency in an official language and work experience abroad",
                                "B) Work experience in Canada and abroad",
                            ],
                            values: [foreign_xp_1, foreign_xp_2],
                        },
                        total: foreign_xp_tot,
                    },
                ];
                let skill = {
                    title: "Skills Transfer Factors",
                    factors: {
                        groups: groups,
                        keys: [
                            "Competency certificate (for people working in a skilled trade)",
                        ],
                        values: [certificate],
                    },
                    total: skill_factors,
                };
                factors.push(skill);
            }

            /************ END - SKILL FACTORS ************/

            var subtotal, grandtotal;

            //Bonus Points

            // $("#q4c option:selected").val();
            var q4c_answer = $(
                'select[name="education_type"] option:selected'
            ).val();

            // $("#q8 option:selected").val();
            var q8_answer = $('select[name="valid_job"] option:selected').val();
            // $("#q8a option:selected").val();
            var q8a_answer = $('select[name="job_kind"] option:selected').val();
            // $("#q9 option:selected").val();
            var q9_answer = $(
                'select[name="certificate_of_designation"] option:selected'
            ).val();

            // $("#q5i-a option:selected").val();
            var q5ii_answer = $(
                'select[name="lang_test"] option:selected'
            ).val();
            // $("#q5i-b-speaking option:selected").val();
            var q5ii_speaking = $(
                'select[name="lang_oral_exp_test_score"] option:selected'
            ).val();
            // $("#q5i-b-listening option:selected").val();
            var q5ii_listening = $(
                'select[name="lang_oral_comp_test_score"] option:selected'
            ).val();
            // $("#q5i-b-reading option:selected").val();
            var q5ii_reading = $(
                'select[name="lang_writing_comp_test_score"] option:selected'
            ).val();
            // $("#q5i-b-writing option:selected").val();
            var q5ii_writing = $(
                'select[name="lang_writing_exp_test_score"] option:selected'
            ).val();

            // $("#q5ii option:selected").val();
            var q5iii_answer = $(
                'select[name="other_lang_test"] option:selected'
            ).val();
            var q5iii_speaking = $(
                'select[name="lang_speaking"] option:selected'
            ).val();
            var q5iii_listening = $(
                'select[name="lang_listening"] option:selected'
            ).val();
            var q5iii_reading = $(
                'select[name="lang_reading"] option:selected'
            ).val();
            var q5iii_writing = $(
                'select[name="lang_writing"] option:selected'
            ).val();

            // $("#q10i option:selected").val();
            var q10i_answer = $(
                'select[name="sibling_canada"] option:selected'
            ).val();

            //canadian study experience
            var study_bonus = 0;

            if (q4c_answer == "B") {
                study_bonus = 15;
            } else if (q4c_answer == "C") {
                study_bonus = 30;
            }

            bonus_factors += study_bonus;

            //job offer
            var job_bonus = 0;

            if (q8_answer == "B" || q8_answer == "C") {
                if (q8a_answer == "A") {
                    job_bonus = 200;
                } else if (q8a_answer == "B") {
                    job_bonus = 50;
                }
            }

            bonus_factors += job_bonus;

            //Question 9 - provincial nomination
            var prov_nom_bonus = 0;

            if (q9_answer == "B") {
                prov_nom_bonus = 600;
            }

            bonus_factors += prov_nom_bonus;

            //French skills
            var french_bonus = 0;

            //Check if you answered french as official language
            //if you took a french test (TEF = C or TCF = D) then continue, else no bonus score for first official language and end if statement.
            //if you didn't take an English test OR if you scored higher than CLB 4
            // = 25 points
            //if you took an Engilsh test AND you scored above CLB5
            // = 50 points

            //if you took an English test and you took a French second test
            //if you scored higher than 7 and if you scored higher than CLB 5
            // = 50
            //if you scored higher than 7
            // = 25

            //if TEF or TCF
            if (q5ii_answer == "C" || q5ii_answer == "D") {
                //If you scored higher than 7 (E to H)
                if (
                    (q5ii_speaking == "E" ||
                        q5ii_speaking == "F" ||
                        q5ii_speaking == "G" ||
                        q5ii_speaking == "H") &&
                    (q5ii_listening == "E" ||
                        q5ii_listening == "F" ||
                        q5ii_listening == "G" ||
                        q5ii_listening == "H") &&
                    (q5ii_reading == "E" ||
                        q5ii_reading == "F" ||
                        q5ii_reading == "G" ||
                        q5ii_reading == "H") &&
                    (q5ii_writing == "E" ||
                        q5ii_writing == "F" ||
                        q5ii_writing == "G" ||
                        q5ii_writing == "H")
                ) {
                    french_bonus = 25;
                    if (q5iii_answer == "A" || q5iii_answer == "B") {
                        if (
                            q5iii_speaking != "A" &&
                            q5iii_speaking != "B" &&
                            q5iii_listening != "A" &&
                            q5iii_listening != "B" &&
                            q5iii_reading != "A" &&
                            q5iii_reading != "B" &&
                            q5iii_writing != "A" &&
                            q5iii_writing != "B"
                        ) {
                            french_bonus = 50;
                        }
                    }
                }
            } else if (q5ii_answer == "A" || q5ii_answer == "B") {
                if (q5iii_answer == "A" || q5iii_answer == "B") {
                    if (
                        (q5iii_speaking == "E" ||
                            q5iii_speaking == "F" ||
                            q5iii_speaking == "G" ||
                            q5iii_speaking == "H") &&
                        (q5iii_listening == "E" ||
                            q5iii_listening == "F" ||
                            q5iii_listening == "G" ||
                            q5iii_listening == "H") &&
                        (q5iii_reading == "E" ||
                            q5iii_reading == "F" ||
                            q5iii_reading == "G" ||
                            q5iii_reading == "H") &&
                        (q5iii_writing == "E" ||
                            q5iii_writing == "F" ||
                            q5iii_writing == "G" ||
                            q5iii_writing == "H")
                    ) {
                        french_bonus = 25;

                        if (
                            (q5ii_speaking == "C" ||
                                q5ii_speaking == "D" ||
                                q5ii_speaking == "E" ||
                                q5ii_speaking == "F" ||
                                q5ii_speaking == "G" ||
                                q5ii_speaking == "H") &&
                            (q5ii_listening == "C" ||
                                q5ii_listening == "D" ||
                                q5ii_listening == "E" ||
                                q5ii_listening == "F" ||
                                q5ii_listening == "G" ||
                                q5ii_listening == "H") &&
                            (q5ii_reading == "C" ||
                                q5ii_reading == "D" ||
                                q5ii_reading == "E" ||
                                q5ii_reading == "F" ||
                                q5ii_reading == "G" ||
                                q5ii_reading == "H") &&
                            (q5ii_writing == "C" ||
                                q5ii_writing == "D" ||
                                q5ii_writing == "E" ||
                                q5ii_writing == "F" ||
                                q5ii_writing == "G" ||
                                q5ii_writing == "H")
                        ) {
                            french_bonus = 50;
                        }
                    }
                }
            }

            bonus_factors += french_bonus;

            //Question 10 - Brother or sister in Canada
            var sibling_bonus = 0;

            if (q10i_answer == "B") {
                sibling_bonus = 15;
            }

            bonus_factors += sibling_bonus;
            if (bonus_factors > 600) {
                bonus_factors = 600;
            }

            subtotal = core_factors + spouse_factors + skill_factors;
            grandtotal = subtotal + bonus_factors;

            //document.getElementById("test_q8").innerHTML = "Question 8 = " + q8_answer;
            //document.getElementById("test_q9").innerHTML = "Question 9 = " + q9_answer;

            if (lead_lang === "fr") {
                document.getElementById("totals").innerHTML =
                    "<h3>Points supplémentaires (jusqu’à un maximum de 600 points)</h3><p>Désignation provinciale = " +
                    prov_nom_bonus +
                    "</p><p>Offre d’emploi = " +
                    job_bonus +
                    "</p><p>Études au Canada = " +
                    study_bonus +
                    "</p><p>Frère/sœur au Canada = " +
                    sibling_bonus +
                    "</p><p>Compétences linguistiques en français = " +
                    french_bonus +
                    "</p><p><strong>Sous-total des points supplémentaires</strong> = " +
                    bonus_factors +
                    "</p><hr /><p><strong>Formule du système de classement somme finale</strong>&nbsp;= " +
                    grandtotal +
                    "</p>";
                $("#totals").show();
                //$('#results').addClass('alert alert-success');

                let total = {
                    title: "Points supplémentaires (jusqu’à un maximum de 600 points)",
                    factors: {
                        keys: [
                            "Désignation provinciale",
                            "Offre d’emploi",
                            "Études au Canada",
                            "Frère/sœur au Canada",
                            "Compétences linguistiques en français",
                        ],
                        values: [
                            prov_nom_bonus,
                            job_bonus,
                            study_bonus,
                            sibling_bonus,
                            french_bonus,
                        ],
                    },
                    total: bonus_factors,
                };
                factors.push(total);
            } else {
                document.getElementById("totals").innerHTML =
                    "<h3>Additional points (up to a maximum of 600 points)</h3><p>Provincial designation = " +
                    prov_nom_bonus +
                    "</p><p>Job offer = " +
                    job_bonus +
                    "</p><p>Study in Canada = " +
                    study_bonus +
                    "</p><p>Brother/sister in Canada = " +
                    sibling_bonus +
                    "</p><p>French language skills = " +
                    french_bonus +
                    "</p><p><strong>Subtotal - Subtotal - Additional Points (to a maximum of 600 points)</strong> = " +
                    bonus_factors +
                    "</p><hr /><p><strong>Ranking system formula final sum</strong>&nbsp;= " +
                    grandtotal +
                    "</p>";
                $("#totals").show();
                //$('#results').addClass('alert alert-success');

                let total = {
                    title: "Additional points (up to a maximum of 600 points)",
                    factors: {
                        keys: [
                            "Provincial designation",
                            "Job offer",
                            "Study in Canada",
                            "Brother/sister in Canada",
                            "French language skills",
                        ],
                        values: [
                            prov_nom_bonus,
                            job_bonus,
                            study_bonus,
                            sibling_bonus,
                            french_bonus,
                        ],
                    },
                    total: bonus_factors,
                };
                factors.push(total);
            }
            window.factors = factors;

            //console.log(JSON.stringify(factors));
            //document.getElementById("results").style.display = "block";
            // $('select[class*="language_test_score"] option').val(function(){
            //     //if ($(this).find)
            //     return $(this).text();
            // });
        }
    }

    static template(count, lang) {
        // console.log(count, lang);
        return (html = `<div class="col-md-6">
                <div class="form-group">
                    <label for="dyn-garant_type_${count}">
                        ${lang === "en" ? "Garant type" : "Type de garant"}
                    </label>
                    <span class="text-danger">*</span>
                    <select class="form-control form-control-altfalse" aria-describedby="dyn-garant_type_${count}-error" aria-invalid="" name="dyn-garant_type_${count}">
                        <option value="">
                            ${
                                lang === "en"
                                    ? "Garant type.."
                                    : "Type de garant.."
                            }
                        </option>
                        <option value="1">
                            ${lang === "en" ? "Father" : "Père"}
                        </option>
                        <option value="2">
                            ${lang === "en" ? "Mother" : "Mère"}
                        </option>
                        <option value="3">
                            ${lang === "en" ? "Grandfather" : "Grand Père"}
                        </option>
                        <option value="4">
                            ${lang === "en" ? "Brother" : "Frère"}
                        </option>
                        <option value="5">
                            ${lang === "en" ? "Sister" : "Soeur"}
                        </option>
                        <option value="6">
                            ${lang === "en" ? "Uncle" : "Oncle"}
                        </option>
                        <option value="7">
                            ${lang === "en" ? "Tent" : "Tente"}
                        </option>
                    </select>
                    <div id="dyn-garant_type_${count}-error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="dyn-income_${count}">
                        ${lang === "en" ? "Mensual income" : "Revenu mensuel"}
                    </label>
                    <span class="text-danger">*</span>
                    <select class="form-control form-control-altfalse" aria-describedby="dyn-income_${count}-error" aria-invalid="" name="dyn-income_${count}">
                        <option value="">${
                            lang === "en"
                                ? "Mensual income.."
                                : "Revenu mensuel.."
                        }</option>
                        <option value="1">&lt;= 10000 DH</option>
                        <option value="2">15000 DH</option>
                        <option value="3">20000 DH</option>
                        <option value="4">&gt; 20000 DH</option>
                    </select>
                    <div id="dyn-income_${count}-error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="dyn-bank_amount_${count}">
                        ${
                            lang === "en"
                                ? "Current bank amount"
                                : "Solde bancaire actuel"
                        }
                    </label>
                        <span class="text-danger">*</span>
                        <input type="text" class="form-control form-control-alt" id="dyn-bank_amount_${count}" name="dyn-bank_amount_${count}" value="" placeholder="${
            lang === "en" ? "Current bank amount.." : "Solde bancaire actuel.."
        }" aria-describedby="dyn-bank_amount_${count}-error" aria-invalid="false">
                    <div id="dyn-bank_amount_${count}-error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="dyn-amount_${count}">
                        ${
                            lang === "en"
                                ? "Amount available to finance the study project"
                                : "Montant disponible pour financer le projet d'étude"
                        }
                    </label>
                    <span class="text-danger">*</span>
                    <select class="form-control form-control-altfalse" aria-describedby="dyn-amount_${count}-error" aria-invalid="" name="dyn-amount_${count}">
                        <option value="">
                            ${
                                lang === "en"
                                    ? "Amount available to finance the study project.."
                                    : "Montant disponible pour financer le projet d'étude.."
                            }
                        </option>
                        <option value="1">25000 DH</option>
                        <option value="2">300000 DH</option>
                        <option value="3">35000 DH</option>
                        <option value="4">&gt;= 400000 DH</option>
                    </select>
                    <div id="dyn-amount_${count}-error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="dyn-goods_${count}">
                        ${lang === "en" ? "Goods" : "Biens"}
                    </label>
                    <span class="text-danger">*</span>
                    <select class="form-control form-control-altfalse" aria-describedby="dyn-goods_${count}-error" aria-invalid="" name="dyn-goods_${count}">
                        <option value="">
                            ${lang === "en" ? "Goods.." : "Biens.."}
                        </option>
                        <option value="9">${
                            lang === "en" ? "Yes" : "Oui"
                        }</option>
                        <option value="10">${
                            lang === "en" ? "No" : "Non"
                        }</option>
                    </select>
                    <div id="dyn-goods_${count}-error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="dyn-rental_income_${count}">
                        ${lang === "en" ? "Rental income" : "Le revenu locatif"}
                    </label>
                    <span class="text-danger">*</span>
                    <select class="form-control form-control-altfalse" aria-describedby="dyn-rental_income_${count}-error" aria-invalid="" name="dyn-rental_income_${count}">
                        <option value="">
                            ${
                                lang === "en"
                                    ? "Rental income.."
                                    : "Le revenu locatif.."
                            }
                        </option>
                        <option value="11">
                            ${lang === "en" ? "Yes" : "Oui"}
                        </option>
                        <option value="12">
                            ${lang === "en" ? "No" : "Non"}
                        </option>
                    </select>
                    <div id="dyn-rental_income_${count}-error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="dyn-business_income_${count}">
                        ${
                            lang === "en"
                                ? "Business income"
                                : "Le revenu d'entreprise"
                        }
                    </label>
                    <span class="text-danger">*</span>
                    <select class="form-control form-control-altfalse" aria-describedby="dyn-business_income_${count}-error" aria-invalid="" name="dyn-business_income_${count}">
                        <option value="">
                            ${
                                lang === "en"
                                    ? "Business income.."
                                    : "Le revenu d'entreprise.."
                            }
                        </option>
                        <option value="13">
                            ${lang === "en" ? "Yes" : "Oui"}
                        </option>
                        <option value="14">
                            ${lang === "en" ? "No" : "Non"}
                        </option>
                    </select>
                    <div id="dyn-business_income_${count}-error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-6 center"></div>
            <div class="garant-btn col-md-12"></div>`);
    }
    static templateVisa(count, lang) {
        return `<div class="col-md-6 visa-request-field">
                <div class="form-group">
                    <label for="dyn-visa_request_${count}">
                        ${
                            lang === "en"
                                ? "Have you ever applied for visas and study permits?"
                                : "Avez-vous déjà fait des demandes de visas et permis d’études ?"
                        }
                    </label>
                    <span class="text-danger">*</span>
                    <select class="form-control form-control-altfalse" aria-describedby="dyn-visa_request_${count}-error" aria-invalid="" name="dyn-visa_request_${count}">
                        <option selected="selected" value="">
                            ${
                                lang === "en"
                                    ? "Have you ever applied for visas and study permits? .."
                                    : "Avez-vous déjà fait des demandes de visas et permis d’études? .."
                            }
                        </option>
                        <option value="82">
                            ${lang === "en" ? "Yes" : "Oui"}
                        </option>
                        <option value="83">
                            ${lang === "en" ? "No" : "Non"}
                        </option>
                    </select>
                    <div id="dyn-visa_request_${count}-error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-6 visa-result-field-${count}" style="display: none;">
                <div class="form-group">
                    <label for="dyn-visa_request_date_${count}">
                        ${lang === "en" ? "Request Date" : "Date de la demande"}
                    </label>
                    <input type="text" class="form-control form-control-alt" id="dyn-visa_request_date_${count}" name="dyn-visa_request_date_${count}" value="" aria-describedby="dyn-visa_request_date_${count}-error" aria-invalid="false" placeholder="${
            lang === "en" ? "yyyy-mm-dd" : "aaaa-mm-jj"
        }">
                    <div id="dyn-visa_request_date_${count}-error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-6 visa-result-field-${count}" style="display: none;">
                <div class="form-group">
                    <label for="dyn-visa_request_type_${count}">
                        ${lang === "en" ? "Request Type" : "Type de la demande"}
                    </label>
                    <span class="text-danger">*</span>
                    <select class="form-control  form-control-altfalse" aria-describedby="dyn-visa_request_type_${count}-error" aria-invalid="" name="dyn-visa_request_type_${count}">
                        <option selected="selected" value="">
                            ${
                                lang === "en"
                                    ? "Request Type.."
                                    : "Type de la demande.."
                            }
                        </option>
                        ${
                            lang === "en"
                                ? `<option value="15">Agrifood Pilot Program</option><option value="6">Child sponsorship</option><option value="22">Citizenship application</option><option value="1">Express Entry</option><option value="11">Family support</option><option value="18">Health workers</option><option value="13">Immigration to Atlantic Canada</option><option value="19">Immigration to Rural and Northern Communities</option><option value="20">Investor</option><option value="7">Parent sponsorship</option><option value="23">Passport application</option><option value="16">Pilot project: Economic Mobility</option><option value="9">Post-Graduation Work Permit</option><option value="2">Province</option><option value="10">Provincial Nominee Program</option><option value="3">Quebec</option><option value="12">Quebec Arrima</option><option value="17">Refugees</option><option value="14">Self-employed workers</option><option value="5">Spouse sponsorship</option><option value="4">Student</option><option value="21">Tourist Visa</option><option value="8">Work Permit EIMT</option>`
                                : `<option value="11">Aide familiaux</option><option value="23">Demande de Passeport</option><option value="22">Demande de citoyenneté</option><option value="1">Entrée express</option><option value="4">Etudiant</option><option value="13">Immigration au Canada atlantique</option><option value="19">Immigration dans les communautés rurales et de Nord</option><option value="20">Investisseur</option><option value="18">Les travailleurs de la santé</option><option value="6">Parrainage d'un enfant</option><option value="5">Parrainage d'un époux</option><option value="7">Parrainage des parents</option><option value="8">Permis de travail EIMT</option><option value="9">Permis de travail post-diplôme</option><option value="10">Programme des candidats des provinces</option><option value="15">Programme pilote sur agroalimentaire</option><option value="16">Projet pilote: la mobilité économique</option><option value="2">Province</option><option value="3">Quebec</option><option value="12">Québec Arrima</option><option value="17">Réfugiés</option><option value="14">Travilleurs autonomes</option><option value="21">Visa touriste</option>`
                        }
                    </select>
                    <div id="dyn-visa_request_type_${count}-error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-6 visa-result-field-${count}" style="display: none;">
                <div class="form-group">
                    <label for="dyn-visa_request_result_${count}">
                        ${
                            lang === "en"
                                ? "Request result"
                                : "Résultat de la demande"
                        }
                    </label>
                    <span class="text-danger">*</span>
                    <select class="form-control  form-control-altfalse" aria-describedby="dyn-visa_request_result_${count}-error" aria-invalid="" name="dyn-visa_request_result_${count}">
                        <option value="">
                            ${
                                lang === "en"
                                    ? "Request result.."
                                    : "Résultat de la demande.."
                            }
                        </option>
                        <option value="84">
                            ${lang === "en" ? "Admitted" : "Admis"}
                        </option>
                        <option value="85">
                            ${lang === "en" ? "Rejected" : "Refus"}
                        </option>
                    </select>
                    <div id="dyn-visa_request_result_${count}-error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-6 center"></div>
            <div class="request-visa-btn col-md-12"></div>`;
    }
    static initFields(field_name) {
        // var sData;
        $.ajax({
            method: "get",
            url: `${process.env.MIX_API_URL}/clear1002`,
            data: $("form#customer-form").serialize(),
            success: (data) => {
                // console.log(data["count"]);
                $('form#customer-form input[name="combs"]').attr(
                    "value",
                    data["count"]
                );
                //sData = data;
                $('select[name*="[]"]').each(function () {
                    // console.log($(this).attr('name'));
                    let attr = $(this).attr("name");
                    attr = attr.replace("[]", "");
                    //let res = jQuery.inArray(attr, Object.keys(data['selected']));
                    let selectedOptions = [];
                    if (
                        jQuery.inArray(attr, Object.keys(data["selected"])) !==
                        -1
                    ) {
                        selectedOptions = data["selected"][attr];
                        // console.log(data['selected'][attr]);
                    }
                    //attr = attr.replace("admission_", "");
                    //$currentSelect = $(this);
                    $(this).empty();
                    //console.log(data[attr]);
                    $.each(
                        data[attr.replace("admission_", "")],
                        function (key, value) {
                            //$('').select2('data', {id: key, text: value});
                            let selected = "";
                            if (jQuery.inArray(key, selectedOptions) !== -1) {
                                //console.log('selected: ' + key);
                                selected = ' selected="selected"';
                            }
                            $(
                                '<option value="' +
                                    key +
                                    '"' +
                                    selected +
                                    ">" +
                                    value +
                                    "</option>"
                            ).appendTo($('select[name="' + attr + '[]"]'));
                            //console.log("key: " + key + ", value: " + value);
                        }
                    );
                    // $options = $('select[name="' + attr + '[]"]').find('option');
                    // if ($options.length === 1) {
                    //     $('select[name="' + attr + '[]"] option:first-child').attr("selected","selected");
                    // }
                    //return false;
                });
            },
            async: false,
        });
        //return sData;
    }
    /*
     * Init functionality
     *
     */
    static init() {
        let sheet = $('[name="sheet"]').val();
        this.clear_lang = $('[name="lang"]').val();
        this.select_placeholder =
            this.clear_lang === "fr" ? "Selectionner..." : "Select...";

        $.datepicker.regional["fr"] = {
            closeText: "Fermer",
            prevText: "Précédent",
            nextText: "Suivant",
            currentText: "Aujourd'hui",
            monthNames: [
                "Janvier",
                "Février",
                "Mars",
                "Avril",
                "Mai",
                "Juin",
                "Juillet",
                "Août",
                "Septembre",
                "Octobre",
                "Novembre",
                "Décembre",
            ],
            monthNamesShort: [
                "Janv.",
                "Févr.",
                "Mars",
                "Avril",
                "Mai",
                "Juin",
                "Juil.",
                "Août",
                "Sept.",
                "Oct.",
                "Nov.",
                "Déc.",
            ],
            dayNames: [
                "Dimanche",
                "Lundi",
                "Mardi",
                "Mercredi",
                "Jeudi",
                "Vendredi",
                "Samedi",
            ],
            dayNamesShort: [
                "Dim.",
                "Lun.",
                "Mar.",
                "Mer.",
                "Jeu.",
                "Ven.",
                "Sam.",
            ],
            dayNamesMin: ["D", "L", "M", "M", "J", "V", "S"],
            weekHeader: "Sem.",
            dateFormat: "yy-mm-dd",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
        };

        this.clear_lang === "fr"
            ? $.datepicker.setDefaults($.datepicker.regional.fr)
            : "";
        $(function () {
            $("#visa_request_date").datepicker({
                dateFormat: "yy-mm-dd",
            });
        });

        if (sheet == 7) {
            this.initDOMEvents();
        } else if (sheet == 4) {
            // let $langue = $('select[name="admission_lang_study"]');
            // let $city = $('select[name="admission_city"]');
            // $langue.change(function(){
            //     //console.log($(this).find(':selected').val());
            //     casesSheetPage.initFields();
            // });
            $(document).ready(function () {
                $('select[name*="[]"]').select2();
            });
            $('select[name*="[]"]').on("change", function (e) {
                // Do
                casesSheetPage.initFields($(this).attr("name"));
                // let selected = $(this).find('option:selected').length;
                // console.log(selected);
                //console.log($(this).attr('name'));
            });
        } else {
            var count = 1;
            var cc = $('select[name^="dyn-"]:last');
            if (cc.length > 0) {
                count = parseInt(cc.attr("name").split("_").pop()) + 1;
                //console.log('cc - *' + count + '*');
            }
            $("#add-garant").on("click", function (event) {
                //event.preventDefault();
                // console.log("clicked");
                const lang = $(this).data("lang");
                var temp = document.getElementsByTagName("template")[0];
                temp.innerHTML = casesSheetPage.template(count, lang);
                var clon = $(temp.content.cloneNode(true));
                $("<hr>").appendTo($(this).parent());
                //clon = clon.attr("innerHTML").replaceText("%%count%%", count);
                //console.log("co: " + clon);
                clon.appendTo($(this).closest(".row"));
                $(this).appendTo($(".garant-btn:last"));
                count += 1;
            });
            $("#add-visa-request").on("click", function (event) {
                // event.preventDefault();
                const lang = $(this).data("lang");
                var temp = document.getElementsByTagName("template")[0];
                temp.innerHTML = casesSheetPage.templateVisa(count, lang);
                var clon = $(temp.content.cloneNode(true));
                $("<hr>").appendTo($(this).parent());
                //clon = clon.attr("innerHTML").replaceText("%%count%%", count);
                //console.log("co: " + clon);
                clon.appendTo($(this).closest(".row"));
                $(this).appendTo($(".request-visa-btn:last"));

                $("#dyn-visa_request_date_" + count).datepicker({
                    dateFormat: "yy-mm-dd",
                });

                count += 1;
            });
            // when visa request is changed
            $('select[name="visa_request"]').change(function () {
                // console.log("testing");
                if ($(this).val() == 82) {
                    $(".visa-result-field").each(function () {
                        $(this).show();
                    });
                } else {
                    $(".visa-result-field").each(function () {
                        $(this).hide();
                    });
                }
            });
            // dyn-visa_request_1
            $(document).on("change", ".visa-request-field select", function () {
                // console.log($(this).val());
                let count = $(this).attr("name");
                count = count.replace("dyn-visa_request_", "");
                if ($(this).val() == 82) {
                    $(".visa-result-field-" + count).each(function () {
                        $(this).show();
                    });
                } else {
                    $(".visa-result-field-" + count).each(function () {
                        $(this).hide();
                    });
                }
            });
            //$('select[name="test_fr"]').parent().hide();
            //$('select[name="test_en"]').parent().hide();
            /*var diploma_fr = $('select[name="diploma_fr"] option:selected').val();
            if(!diploma_fr || diploma_fr == "1"){
                $('select[name="test_fr"]').prop('selectedIndex', 0);
                $('select[name="test_fr"]').parent().hide();
                $('select[name="remark-test_fr"]').hide();
            }
            var diploma_en = $('select[name="diploma_en"] option:selected').val();
            if(!diploma_en || diploma_en == "1"){
                //$('select[name="test_en"]').empty();
                $('select[name="test_en"]').prop('selectedIndex', 0);
                $('select[name="test_en"]').parent().hide();
                $('select[name="remark-test_en"]').hide();
            }*/
            /*$('select[name="diploma_fr"]').change(function() {
                var diploma = $('select[name="diploma_fr"] option:selected').val();
                if (diploma) {
                    if (diploma != "1") {
                        $('select[name="test_fr"]').parent().show();
                        $('select[name="remark-test_fr"]').show();
                    } else {
                        //$('select[name="test_fr"]').empty();
                        $('select[name="test_fr"]').prop('selectedIndex', 0);
                        $('select[name="test_fr"]').parent().hide();
                        $('select[name="remark-test_fr"]').hide();
                    }
                }
            });*/
            /*$('select[name="diploma_en"]').change(function() {
                var diploma = $('select[name="diploma_en"] option:selected').val();
                if (diploma) {
                    if (diploma != "1") {
                        $('select[name="test_en"]').parent().show();
                        $('select[name="remark-test_en"]').show();
                    } else {
                        //$('select[name="test_en"]').empty();
                        $('select[name="test_en"]').prop('selectedIndex', 0);
                        $('select[name="test_en"]').parent().hide();
                        $('select[name="remark-test_en"]').hide();
                    }
                }
            });*/
        }
    }
}

// Initialize when page loads
jQuery(() => {
    casesSheetPage.init();
});
