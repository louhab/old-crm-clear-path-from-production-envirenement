/*
 *  Document   : sheet.js
 *  Author     : OL
 *  Description: Custom JS code used in case sheet page
 */

class casesMediaSheetPage {
    /*
     * Init DOM events
     *
     */
    static initDOMEvents() {
        $('.delete-document').on('click', function(event) {
            event.preventDefault()
            $(`#${$(this).attr('data-formId')}`).submit()
        });
    }

   
    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents();
    }
}

// Initialize when page loads
jQuery(() => { casesMediaSheetPage.init() })
