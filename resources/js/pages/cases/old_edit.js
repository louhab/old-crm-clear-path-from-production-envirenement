/*
 *  Document   : edit.js
 *  Author     : RR
 *  Description: Custom JS code used in case edit page
 */

class casesEditPage {
    static loadComments(lid = 0) {

        $('#comments-block').addClass('block-mode-loading')

        $.ajax({
            method: 'get',
            url: `${process.env.MIX_API_URL}/comments/${$('#main-comment-container').attr('data-comment-id')}`,
            data: lid ? {lid: lid} : {},
            success: (data) => {
                let $commentWrapper = $('#comments-wrapper')
                let $loadCommentsBtn = $('#load-old-comments')
                let $loadCommentsBtnWrapper = $('#load-old-comments-wrapper')

                if(lid) $commentWrapper.append(data)
                else $commentWrapper.html(data)

                $loadCommentsBtn.attr('data-lid', $commentWrapper.children().last().attr('data-lid'))

                if(!$.trim( data ).length) $loadCommentsBtnWrapper.hide()
                else $loadCommentsBtnWrapper.show()

            },
            error: () => {
                throw 'comments list error'
            }
        }).done(function () {
            $('#comments-block').removeClass('block-mode-loading')
        })
    }
    /*
     * Init DOM events
     *
     */
    static initDOMEvents() {

        $('.delete-document').on('click', function(event) {
            event.preventDefault()
            $(`#${$(this).attr('data-formId')}`).submit()
        })

        $('.sign-document, .requested-doc').on('click', function(event) {
            event.preventDefault()
            $(`#${$(this).attr('data-formId')}`).submit()
        })

        $('.custom-file-input').on('change', function() {
            let fileName = $(this).val().split('\\').pop()
            $(this).parent().find('label').addClass("selected").html(fileName)
        })

        $('.toggle-property').on('click', function(event) {
            event.preventDefault()
            let $form = $(`#${$(this).attr('data-formId')}`)
            let input = jQuery(`<input name="property" value="${$(this).attr('data-property')}">`)
            $form.append(input)
            $form.submit()
        })

        $('.toggle-link-visibility, .toggle-sheet-visibility').on('click', function(event) {
            event.preventDefault()
            $(`#${$(this).attr('data-formId')}`).submit()
        })

        $('#refresh-comment-btn').on('click', (event) => {
            event.preventDefault()
            this.loadComments()
        })

        $('body').on('click', '#load-old-comments', () => {
            this.loadComments($('#load-old-comments') . attr('data-lid'))
        })
        $('body').on('click', '.case-state-update', function() {
            // {{ route('sub-step-state-update', ['case_state' => $case_state->id]) }}
            let stateBtn = $(this);
            let iconStatus = $(this).closest("tr").find("a").first();
            let customer_case = $(this).data("case");
            //let step_state = $(this).data();
            
            //console.log(iconStatus.html());
            $.ajax({
                method: 'put',
                url: `${process.env.MIX_API_URL}/states/${customer_case}`,
                success: (data) => {
                    Swal.fire(
                        'Modification!',
                        'Status modiffié avec succés!',
                        'success'
                    ).then(() => {
                        //$('#substep-7').load('cases/ #substep-7');
                        //$("#div1").load("demo_test.txt #p1");
                        //alert(data);
                        //
                        //<i class="fas fa-toggle-off"></i>
                        //console.log(data);
                        // if(data.status){
                        //     stateBtn.html('<i class="fas fa-toggle-on"></i>');
                        //     iconStatus.html('<i class="fa fa-check text-success mr-4"></i>'+data.name);
                        //     //console.log(iconStatus);
                        // } else {
                        //     stateBtn.html('<i class="fas fa-toggle-off"></i>');
                        //     iconStatus.html('<i class="fa fa-sync fa-spin text-warning mr-4"></i>'+data.name)
                        //     //console.log(iconStatus.html());
                        // }
                        location.reload();
                    })
                },
                error: () => {
                    Swal.fire(
                        'Modification!',
                        'erreur modification lead!',
                        'error'
                    )
                }
            })
        });

        let histocallCustomerAddBlockSelector = $('#block-histocall-customer-add');
        let histocallCustomerAddFormSelector = $('#histocall-add-customer-form');
        // HistoCalls Modal
        $('#histocall-customer-table-add').on('click', function () {

            //hide previous validation errors
            histocallCustomerAddFormSelector.find('.invalid-feedback').hide();
            histocallCustomerAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#histocall-add-customer-form').trigger('reset');
            $('#modal-histocall-customer-add').modal('show');
        });
        
        $('#add-histocall-customer-btn').on('click', function () {

            //hide previous validation errors
            histocallCustomerAddFormSelector.find('.invalid-feedback').hide();
            histocallCustomerAddFormSelector.find('.form-control').removeClass('is-invalid');

            histocallCustomerAddBlockSelector.addClass('block-mode-loading');
            let seconds = parseInt($('#sec').text()) + parseInt($('#min').text()) * 60 + parseInt($('#hour').text()) * 3600;
            //console.log(seconds);
            $('#add-form-duration').val(seconds);

            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/histocalls`,
                data: histocallCustomerAddFormSelector.serialize(),
                success: (res) => {
                    // console.log(res.data);
                    console.log($('.calls').last().html());
                    //var newElement = ;
                    // element.parentNode.insertBefore(newElement, element.nextSibling);
                    // Swal.fire(
                    //     'Ajout!',
                    //     'client ajouté avec succés!',
                    //     'success'
                    //     ).then(() => {
                    //         histocallCustomerAddBlockSelector.removeClass('block-mode-loading');
                    //         $('#histocall-add-form').trigger('reset');
                    //         //histocallsTable.api().ajax.reload();
                    //     });
                    var length = $('.calls').length;
                    // if (length == 0) {

                    // }
                    $('<tr class="calls"><td>Call '+(length+1)+':</td><td>'+res.data.call_descr+'</td><td>'+res.data.duration+'</td></tr>').insertAfter($('.calls').last());
                    $('#modal-histocall-customer-add').modal('hide');
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        histocallCustomerAddBlockSelector.removeClass('block-mode-loading');
                    }
                    else
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout client!',
                            'error'
                        ).then(() => {
                            histocallCustomerAddBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });

        (function($) {

            var Stopwatch = function(elem, options) {
              // code from above...
              var timer       = createTimer(),
                    startButton = createButton("start", "play", start),
                    //createElementFromHTML('<button type="button" class="btn btn-alt-secondary mr-1 mb-3"><i class="fa fa-fw fa-play mr-1"></i> Play</button>'),//
                    stopButton  = createButton("stop", "pause", stop),
                    resetButton = createButton("reset", "trash-restore", reset),
                    offset,
                    clock,
                    interval;

                // default options
                options = options || {};
                options.delay = options.delay || 1;

                // append elements     
                elem.appendChild(timer);
                elem.appendChild(startButton);
                elem.appendChild(stopButton);
                elem.appendChild(resetButton);

                // initialize
                reset();

                // private functions
                function createElementFromHTML(htmlString) {
                    var div = document.createElement('div');
                    div.innerHTML = htmlString.trim();
                  
                    // Change this to div.childNodes to support multiple top-level nodes
                    return div.firstChild; 
                }
                function createTimer() {
                    //return createElementFromHTML('<input type="text" class="js-masked-time form-control js-masked-enabled" id="example-masked-time" name="example-masked-time" placeholder="00:00">');
                    return createElementFromHTML('<div class="form-group"></div>');
                }

                function createButton(action, icon, handler) {
                    var b = createElementFromHTML('<button type="button" class="btn btn-alt-secondary mr-1 mb-3"><i class="fa fa-fw fa-'+icon+' mr-1"></i> '+action+'</button>');
                    //var a = document.createElement("a");
                    // a.href = "#" + action;
                    // a.innerHTML = action;
                    b.addEventListener("click", function(event) {
                        handler();
                        event.preventDefault();
                    });
                    return b;
                }

                function start() {
                    if (!interval) {
                    offset   = Date.now();
                    interval = setInterval(update, options.delay);
                    }
                }

                function stop() {
                    if (interval) {
                    clearInterval(interval);
                    interval = null;
                    }
                }
                function minutes(){
                    return Math.round((clock/1000)/60);
                }

                function reset() {
                    clock = 0;
                    render();
                }

                function update() {
                    clock += delta();
                    render();
                }

                function render() {
                    var seconds = clock/1000;
                    if (Math.round(seconds) >= 60) {
                        clock = 0;
                    }
                    timer.innerHTML = '<input type="text" class="js-masked-time form-control js-masked-enabled" id="example-masked-time" name="add-form-duration" value="'+minutes()+':'+seconds+'">';
                    // clock/1000;
                }

                function delta() {
                    var now = Date.now(),
                        d   = now - offset;

                    offset = now;
                    return d;
                }

                // public API
                this.start  = start;
                this.stop   = stop;
                this.reset  = reset;
            };
          
            $.fn.stopwatch = function(options) {
              return this.each(function(idx, elem) {
                new Stopwatch(elem, options);
              });
            };
          })(jQuery);

        

        $(".stopwatch").stopwatch({delay:10});
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents()
        this.loadComments()
    }
}

// Initialize when page loads
jQuery(() => { casesEditPage.init() })
