/*
 *  Document   : cases.js
 *  Author     : RR
 *  Description: Custom JS code used in cases page
 */

class casesListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        const lang = $('input[name="lang"]').val();
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm",
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>',
                },
            },
        });

        let columns = [
            {
                data: "lead_id",
                name: "lead_id",
                title: "<input type='checkbox' id='MyTableCheckAllButton'>",
                visible:
                    window.userRole.role_id > 1 && window.userRole.role_id != 10
                        ? false
                        : true,
                className: "select-checkbox",
                mRender: function (data, type, row) {
                    return "";
                },
            }, // lead_id
            {
                data: "actions",
                className: "text-center",
                title: "action",
                width: "150px",
                mRender: function (data, type, row) {
                    return `<div class="d-flex justify-content-center action">
                            ${data.map((action) => {
                                if (action.available) {
                                    if (action.edit) {
                                        return `<div class="btn-group" style="margin:5px">  <a title="${action.title}" class="${action.className}" data-id="${row.id}"  href="cases/${row.id}"> <i class="${action.icon}"></i></a></div>`;
                                    } else {
                                        if (action.action === "call") {
                                            return `<div class="btn-group" style="margin:5px"><button title="${action.title}" class="${action.className}" id="histocall-customer-table-add-${row.id}" data-id="${row.id}"> <i class="${action.icon}"></i></button></div>`;
                                        } else {
                                            return `<div class="btn-group" style="margin:5px"><button title="${action.title}" class="${action.className}" data-id="${row.id}"> <i class="${action.icon}"></i></button></div>`;
                                        }
                                    }
                                } else {
                                    return "";
                                }
                            })}
                        </div>`;
                },
            },
            {
                data: "admission_state_id",
                name: "admission_state_id",
                title: "Status",
                mRender: function (data, type, row) {
                    let badge = "info";
                    switch (row.admission_state_id) {
                        case 1:
                            badge = "info";
                            break;
                        case 2:
                            badge = "warning";
                            break;
                        case 3:
                            badge = "success";
                            break;

                        case 11:
                        case 16:
                        case 26:
                        case 21:
                        case 31:
                        case 4:
                            badge = "danger";
                            break;
                    }
                    return data
                        ? `<span class="badge badge-${badge}">${row.admission_state}</span>`
                        : '<span class="badge badge-secondary">n/d</span>';
                },
            }, // status
            { data: "id", name: "id", visible: false }, // Id
            {
                data: "firstname",
                name: "firstname",
                title: lang === "fr" ? "Prénom" : "Name",
            }, // firstname
            {
                data: "lastname",
                name: "lastname",
                title: lang === "fr" ? "Nom" : "Surename",
            }, // lastname // email
            /*{"data": "email", "name": "email", title: 'Email'},*/ {
                data: "phone",
                name: "phone",
                title: "Phone",
                visible:
                    window.userRole.role_id == 3 ||
                    window.userRole.role_id == 4 ||
                    window.userRole.role_id == 7
                        ? false
                        : true,
                mRender: function (data, type, row) {
                    return data
                        ? data
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // phone
            {
                name: "city",
                title: "Ville",
                mRender: function (data, type, row) {
                    if (row?.country == "144" && !row?.city) {
                        return row?.city2;
                    } else {
                        return row?.city || `<em class="text-muted">null</em>`;
                    }
                },
            }, // Ville
            {
                name: "sub_step_state_id",
                title: "Rencontre",
                visible: [1, 2, 3, 4].includes(window.userRole.role_id),
                mRender: function (data, type, row) {
                    switch (row.sub_step_state_id) {
                        case 3:
                            return "R1";
                            break;
                        case 5:
                            return "R2";
                            break;
                        case 10:
                            return "R3";
                            break;
                        case 20:
                            return "R4";
                            break;
                        case 29:
                            return "R5";
                            break;
                        default:
                            return '<span class="badge badge-info">n/d</span>';
                    }
                },
            },
            {
                name: "meet_date",
                title: " Date Rencontre",
                visible: [1, 2, 3, 4].includes(window.userRole.role_id),
                mRender: function (data, type, row) {
                    if (row.sub_step_state_id && row.meet_date) {
                        const meet_date = new Date(row.meet_date);
                        return (
                            (meet_date.getDate() > 9
                                ? meet_date.getDate()
                                : "0" + meet_date.getDate()) +
                            "/" +
                            (meet_date.getMonth() > 8
                                ? meet_date.getMonth() + 1
                                : "0" + (meet_date.getMonth() + 1)) +
                            "/" +
                            meet_date.getFullYear()
                        );
                    }
                    return '<span class="badge badge-info">n/d</span>';
                },
            },
            {
                data: "pname",
                name: "pname",
                title: lang === "fr" ? "Programme" : "Program",
            }, // program.name
            /*{
                data: "last_comment",
                name: "last_comment",
                title: lang === "fr" ? "cc" : "cc",
            },
            {
                data: "client_comment_id",
                name: "client_comment_id",
                title: lang === "fr" ? "vv" : "vv",
            },*/
            {
                data: "canal",
                name: "canal",
                title: "Canal",
                visible:
                    window.userRole.role_id == 2 ||
                    window.userRole.role_id == 3 ||
                    window.userRole.role_id == 6
                        ? false
                        : true,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : "-";
                },
            }, // canal
            {
                data: "influencer",
                name: "influencer",
                title: "Source",
                visible:
                    window.userRole.role_id == 2 ||
                    window.userRole.role_id == 3 ||
                    window.userRole.role_id == 6
                        ? false
                        : true,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : "-";
                },
            }, // influencer
            {
                data: "reference",
                name: "reference",
                title: "Référence",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : "-";
                },
            }, // reference
            {
                data: "conseiller",
                name: "conseiller",
                title: lang === "fr" ? "Conseiller" : "Advisor",
                visible: window.userRole.role_id == 3 ? false : true,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // conseiller
            {
                data: "backoffice",
                name: "backoffice",
                title: "Backoffice",
                visible:
                    window.userRole.role_id === 1 ||
                    window.userRole.role_id === 2 ||
                    window.userRole.role_id == 3 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // backoffice // Date de Création
            // total fees
            {
                data: "total_fees",
                name: "total_fees",
                title: "Frais total",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 2 ||
                    window.userRole.role_id == 3 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    if (data && Number(data))
                        return `<span class="badge badge-dark">${
                            data + ` ` + row.currency
                        } </span>`;
                    return `-`;
                },
            },
            /*{
                data: "currency",
                name: "currency",
                title: "null",
                visible: false,
                mRender: function (data, type, row) {
                    return "";
                },
            },*/
            /*{"data": "created_at", "name": "created", title: 'Crée le', mRender: function (data, type, row) {
                    if(data) {
                        var date = new Date(data);
                        return ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + date.getFullYear();
                    }
                    return '<span class="badge badge-info">n/d</span>';
                }},*/ {
                data: "processed",
                name: "processed",
                title: lang === "fr" ? "date backoffice" : "backoffice date",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 2 ||
                    window.userRole.role_id == 3 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    if (data) {
                        var date = new Date(data);
                        return (
                            (date.getMonth() > 8
                                ? date.getMonth() + 1
                                : "0" + (date.getMonth() + 1)) +
                            "/" +
                            (date.getDate() > 9
                                ? date.getDate()
                                : "0" + date.getDate()) +
                            "/" +
                            date.getFullYear()
                        );
                    }
                    return '<span class="badge badge-info">n/d</span>';
                },
            }, // Date de Traitement
            {
                data: "processed_message",
                name: "processed_message",
                title:
                    lang === "fr"
                        ? // ? "Message de Traitement Soumission"
                          "message backoffice"
                        : "backoffice message",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 2 ||
                    window.userRole.role_id == 3 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    // @edited
                    // console.log("processed_message", data);

                    // console.log("row from cases page for bo_update", row);

                    let class_comment = "";
                    let tt = data;

                    // const then = new Date(row.bo_update_time);
                    // const now = new Date();

                    // const msBetweenDates = Math.abs(
                    //     then.getTime() - now.getTime()
                    // );

                    // 👇️ convert ms to hours                  min  sec   ms
                    // const hoursBetweenDates = msBetweenDates / (60 * 60 * 1000);

                    // if (hoursBetweenDates <= 24) {
                    //     class_comment = "recent-comment";
                    // }

                    if (data?.match(/soumission status modifié/g)) {
                        // console.log(data.match(/note soumission-admission-/g));
                        tt = "Status modifié";
                        return data
                            ? `<span class="text-muted">${tt}</span>`
                            : '<span class="badge badge-info">n/d</span>';
                    }
                    if (data?.match(/note soumission-admission-/g)) {
                        // console.log(data.match(/note soumission-admission-/g));
                        tt = data.replace(
                            "note soumission-admission-",
                            "Commentaire admission "
                        );
                        tt = tt.replace(" ajouté", "");
                    } else if (data?.match(/admission note ajouté/g)) {
                        tt = tt.replace("admission note ajouté", "");
                        if (tt != "") {
                            tt = tt.replace(")", "");
                            tt = tt.replace("(", "");
                            if (tt.match(/admission-/g))
                                tt = tt.replace("admission-", "Admission ID:");
                            if (tt.match(/course-/g))
                                tt = tt.replace("course-", "Admission ID:");
                        } else tt = "admission note ajouté";
                    } else if (data?.match(/note soumission-/g)) {
                        tt = tt.replace("note soumission-", "Commentaire ");
                        tt = tt.replace(" ajouté", "");
                    }
                    return data
                        ? `<p class="${class_comment} m-0"><span class="text-muted">${tt}</span><br><a data-id="${row.id}" data-comment="${row.last_comment}" data-process="soumission-view" href="javascript:void(0)" class="comment-info"><small>Afficher plus</small> <i class="fa fa-fw fa-plus ml-1" style="font-size: 15px;"></i></a></p>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            },
            {
                data: "processed_message_client",
                name: "processed_message_client",
                title: lang === "fr" ? "message conseiller" : "advisor message",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 2 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                // className: row.client_comment_id=="null"? "": "recent-comment",
                mRender: function (data, type, row) {
                    // @edited
                    // console.log("last comment", row.client_comment_id);

                    let class_comment = "";
                    let tt = data;

                    // processed
                    // const then = new Date(row.processed);
                    // const now = new Date();

                    // const msBetweenDates = Math.abs(
                    //     then.getTime() - now.getTime()
                    // );

                    // 👇️ convert ms to hours                  min  sec   ms
                    // const hoursBetweenDates = msBetweenDates / (60 * 60 * 1000);
                    // if (hoursBetweenDates <= 24) {
                    //     class_comment = "recent-comment-client";
                    // }

                    if (data?.match(/soumission status modifié/g)) {
                        // console.log(data.match(/note soumission-admission-/g));
                        tt = "Status modifié";
                        return data
                            ? `<span class="badge badge-info">n/d</span>`
                            : '<span class="badge badge-info">n/d</span>';
                    }
                    if (data?.match(/note soumission-admission-/g)) {
                        // console.log(data.match(/note soumission-admission-/g));
                        tt = data.replace(
                            "note soumission-admission-",
                            "Admission "
                        );
                        tt = tt.replace(" ajouté", "");
                    } else if (data?.match(/admission note ajouté/g)) {
                        tt = tt.replace("admission note ajouté", "");
                        if (tt != "") {
                            tt = tt.replace(")", "");
                            tt = tt.replace("(", "");
                            if (tt.match(/admission-/g))
                                tt = tt.replace("admission-", "Admission ID:");
                            if (tt.match(/course-/g))
                                tt = tt.replace("course-", "Admission ID:");
                        } else tt = "admission note ajouté";
                    } else if (data?.match(/note soumission-/g)) {
                        tt = tt.replace("note soumission-", "");
                        tt = tt.replace(" ajouté", "");
                    }
                    return data
                        ? `<p class="${class_comment} m-0"><span class="text-muted">${tt}</span><br><a data-id="${row.id}" data-comment="${row.client_comment_id}" data-process="case-edit" href="javascript:void(0)" class="comment-info"><small>Afficher plus</small> <i class="fa fa-fw fa-plus ml-1" style="font-size: 15px;"></i></a></p>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            },
            {
                data: "calls",
                name: "calls",
                title: lang === "fr" ? "Total appels" : "Total calls",
                className: "font-w600 font-size-sm",
                mRender: function (data, type, row) {
                    return row.calls ? row.calls : "0";
                },
            }, // calls
        ];

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#cases-dt");
        let blockSelector = $("#cases-dt-block");

        // Init cases DataTable
        return tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.${lang}.l10n.json`,
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[12, "desc"]],
                ajax: {
                    type: "POST",
                    url: `${process.env.MIX_API_URL}/cases`,
                    data: function (d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function (index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    },
                },
                columns: columns,
                fnDrawCallback: function () {
                    blockSelector.removeClass("block-mode-loading");
                    /* Toolbar Table */
                    //console.log(window.userRole);
                    if (
                        window.userRole.role_id == 1 ||
                        window.userRole.role_id == 2 ||
                        window.userRole.role_id == 10
                    ) {
                        $("div.toolbar").html(
                            '<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-cente mb-2" style="width: 30%;"><select class="form-control dt_action_field" name="agent[]" multiple="multiple"></select><button type="button" class="btn btn-alt-primary ml-2" id="dt_action_btn">Affecter</button></div>'
                        );
                    }
                    $(".dt_action_field").select2({
                        placeholder: "Utilisateurs...",
                        allowClear: true,
                        ajax: {
                            url: `${process.env.MIX_API_URL}/agents`,
                            dataType: "json",
                            delay: 250,
                            method: "POST",
                            data: function (term, page) {
                                return {
                                    q: term,
                                };
                            },
                            processResults: function (data) {
                                let res = [];
                                // console.log(data);
                                $.each(data, function (role, group) {
                                    if (role == "Agent") return true;
                                    let groupObj = {
                                        text: role,
                                    };
                                    let children = [];
                                    // console.log(role, group);
                                    $.each(group, function (idx, user) {
                                        // if(user.name)
                                        let child = {
                                            text: user.name,
                                            id: user.id,
                                            group: role,
                                        };
                                        children.push(child);
                                    });
                                    groupObj["children"] = children;
                                    res.push(groupObj);
                                });
                                //console.log(res);
                                return {
                                    results: res,
                                };
                            },
                            initSelection: function (element, callback) {},
                            cache: true,
                        },
                    });
                    $("#dt_action_btn").on("click", function () {
                        let conseillers = $(".dt_action_field").select2("val");
                        var $dataSelected = $("#cases-dt")
                            .DataTable()
                            .rows(".selected")
                            .data();
                        if (
                            conseillers.length > 0 &&
                            $dataSelected.length > 0
                        ) {
                            var resultConseilArray =
                                $(".dt_action_field").select2("val");

                            /*let i = 0;
                        var resultConseilArray = $dataSelected.reduce((resultArray, item, index) => {

                            if(i == conseillers.length) {
                                i = 0;
                            }

                            resultArray[item.lead_id] = conseillers[i];
                            i = i + 1;

                            return resultArray;
                        }, []);*/
                            //console.log(Object.keys(result));
                            var cs = 0;
                            // console.log();
                            Swal.fire({
                                title: "Etes vous sur?",
                                text: "",
                                icon: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#3085d6",
                                cancelButtonColor: "#d33",
                                confirmButtonText: "Oui, Affecter!",
                                cancelButtonText: "Annuler",
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    //let bs = $('#leads-dt-block');
                                    $("#cases-dt-block").addClass(
                                        "block-mode-loading"
                                    );
                                    var leads_ids = $("#cases-dt")
                                        .DataTable()
                                        .rows(".selected")
                                        .data()
                                        .pluck("lead_id")
                                        .toArray();
                                    $.ajax({
                                        method: "put",
                                        url: `${process.env.MIX_API_URL}/leadConseiller`,
                                        data: {
                                            leads_ids: leads_ids,
                                            resultConseilArray:
                                                resultConseilArray,
                                        },
                                        success: () => {
                                            Swal.fire(
                                                "Modification!",
                                                "Leads affecté",
                                                "success"
                                            ).then(() => {
                                                $("#cases-dt")
                                                    .DataTable()
                                                    .ajax.reload(null, false);
                                                $(
                                                    "#MyTableCheckAllButton"
                                                ).prop("checked", false);
                                            });
                                            //console.log(cs + "..." + $dataSelected.length);
                                            /*if (cs == $dataSelected.length){
                                            Swal.fire(
                                                'Modification!',
                                                cs + ' Leads afecté à ' + $(".dt_action_field").select2('data')[0].text,
                                                'success'
                                            ).then(() => {
                                                $('#leads-dt').DataTable().ajax.reload( null, false );
                                            });
                                        }*/
                                        },
                                        error: () => {
                                            $("#cases-dt-block").removeClass(
                                                "block-mode-loading"
                                            );
                                            //window.ce += 1;
                                            // Swal.fire(
                                            //     'Modification!',
                                            //     'erreur Modification lead!',
                                            //     'error'
                                            // );
                                        },
                                    });
                                    /*$dataSelected.each( function ( data, index ) {
                                    // console.log( cs + ' Data ID: '+data.lead_id+' Conseiller: '+resultConseilArray[data.lead_id] );
                                    $.ajax({
                                        method: 'put',
                                        url: `${process.env.MIX_API_URL}/leadConseiller/${data.lead_id}/${resultConseilArray[data.lead_id]}`,
                                        success: () => {
                                            cs += 1;
                                            //console.log(cs + "..." + $dataSelected.length);
                                            if (cs == $dataSelected.length){
                                                Swal.fire(
                                                    'Modification!',
                                                    cs + ' Leads afecté à ' + $(".dt_action_field").select2('data')[0].text,
                                                    'success'
                                                ).then(() => {
                                                    $('#leads-dt').DataTable().ajax.reload( null, false );
                                                });
                                            }
                                        },
                                        error: () => {
                                            $('#leads-dt-block').removeClass('block-mode-loading')
                                            //window.ce += 1;
                                            // Swal.fire(
                                            //     'Modification!',
                                            //     'erreur Modification lead!',
                                            //     'error'
                                            // );
                                        }
                                    });
                                });*/
                                }
                            });
                        } else if ($dataSelected.length == 0) {
                            Swal.fire(
                                "Modification!",
                                "Veuiller Selectinner un lead!",
                                "warning"
                            );
                        } else {
                            Swal.fire(
                                "Modification!",
                                "Veuiller Choisir un conseiller!",
                                "warning"
                            );
                        }
                    });
                    $(".submit-search").prop("disabled", false);
                },
                rowCallback: function (row, data) {
                    const bo_update_time = new Date(data.bo_update_time);
                    const now = new Date();

                    const bo_msBetweenDates = Math.abs(
                        bo_update_time.getTime() - now.getTime()
                    );

                    const bo_hoursBetweenDates =
                        bo_msBetweenDates / (60 * 60 * 1000);

                    if (
                        window.userRole.role_id == 3 &&
                        bo_hoursBetweenDates <= 24
                    ) {
                        $(row).find("td:eq(7)").css("color", "white");
                        $(row)
                            .find("td:eq(7)")
                            .css("background-color", "#FD971F");
                    }

                    if (bo_hoursBetweenDates <= 24) {
                        $(row).find("td:eq(11)").css("color", "white");
                        $(row)
                            .find("td:eq(11)")
                            .css("background-color", "#FD971F");
                    }

                    // old

                    if ($(row).find(".recent-comment").length > 0) {
                        // console.log("inactive part");
                        let indx = 11;
                        if ($(row).children().length == 13) indx = 9;
                        $(row)
                            .find("td:eq(" + indx + ")")
                            .css("color", "white");
                        $(row)
                            .find("td:eq(" + indx + ")")
                            .css("background-color", "#FD971F");
                    }
                },
                columnDefs: [
                    { className: "comment-soumission", targets: 12 },
                    { className: "comment-client", targets: 13 },
                    {
                        targets: [],
                        visible: false,
                        orderable: true,
                        searchable: false,
                        className: "select-checkbox",
                    },
                    {
                        targets: 0,
                        orderable: false,
                    },
                ],
                select: {
                    style: "multi",
                    selector: "td:first-child",
                },
                lengthMenu: [
                    [10, 25, 50, 100, 500, -1],
                    [10, 25, 50, 100, 500, "All"],
                ],
                dom: '<"toolbar">lpfrtip',
            });
    }

    static initDOMEvents(dt) {
        //DELETE Quote
        $("body").on("click", ".comment-info", function () {
            /*let customerId = $(this).data("id");
            console.log(customerId);*/
            Livewire.emit(
                "commentInfo",
                $(this).data("id"),
                $(this).data("process")
            );
            $("#modal-comment-info").modal("show");
        });
        /*var table = $('#cases-dt').DataTable({
            "rowCallback": function( row, data, index ) {
                var allData = this.api().column(2).data().toArray();
                $('td:eq(2)', row).css('background-color', 'Red');
                /!*if (allData.indexOf(data[2]) != allData.lastIndexOf(data[2])) {
                }*!/
            }
        });*/
        /*Livewire.on('setCellColor', obj => {
            // console.log(obj);
            var table = $('#cases-dt').DataTable({
                "rowCallback": function( row, data, index ) {
                    var allData = this.api().column(2).data().toArray();
                    if (allData.indexOf(data[2]) != allData.lastIndexOf(data[2])) {
                        $('td:eq(2)', row).css('background-color', 'Red');
                    }
                }
            });
        });*/
        jQuery("#modal-comment-info").on("hidden.bs.modal", function (e) {
            // do something...
            Livewire.emit("clearCommentInfo", $(this).data("id"));
        });
        $("body").on("click", ".case-table-delete", function () {
            const $row = $(this).closest("tr");
            const data = $("#cases-dt").DataTable().row($row).data();
            //alert(data);
            window.CLEARPATH_LEAD_ID = data.id;

            /*swal({
                title: 'Select Status',
                input: 'select',
                inputOptions: {
                    '': 'n/a',
                    '5': 'Contrat'
                },
                inputPlaceholder: 'Select country',
            }).then(function (result) {
                swal({
                    type: 'success',
                    html: 'You selected: ' + result
                })
            });*/
            Swal.fire({
                title: "Etes vous sur?",
                text: "Convert client to lead",
                icon: "warning",
                input: "select",
                inputOptions: {
                    14: "Désistement",
                    13: "Repport",
                    5: "Contrat",
                    4: "RDV 1",
                    3: "Injoignable",
                    2: "Pas Intéressé",
                    1: "Rencontre 1",
                    nd: "n/d",
                },
                inputValue: "nd",
                inputPlaceholder: "Select status",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, convertir!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    //let bs = $('#leads-dt-block');
                    casesListPage.deleteCase(dt, result);
                }
            });
        });

        //DOM Events
        //TODO put dom events here

        // CONVERT LEAD TO CLIENT
        // lead-convert
        $("body").on("click", ".client-convert", function () {
            const $row = $(this).closest("tr");
            const data = $("#cases-dt").DataTable().row($row).data();
            // console.log(data);
            window.CLEARPATH_CUSTOMER_ID = data.customer_id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, convertir!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#leads-dt-block").addClass("block-mode-loading");

                    $.ajax({
                        method: "put",
                        url: `${process.env.MIX_API_URL}/cases/convert/${window.CLEARPATH_CUSTOMER_ID}`,
                        success: () => {
                            Swal.fire(
                                "Modification!",
                                "Client converti avec succés!",
                                "success"
                            ).then(() => {
                                dt.api().ajax.reload(null, false);
                                $("#MyTableCheckAllButton").prop(
                                    "checked",
                                    false
                                );
                            });
                        },
                        error: () => {
                            Swal.fire(
                                "Modification!",
                                "erreur modification lead!",
                                "error"
                            ).then(() => {
                                $("#leads-dt-block").removeClass(
                                    "block-mode-loading"
                                );
                            });
                        },
                    });
                }
            });
        });

        //SEARCH FILTER
        $(".submit-search").on("click", function () {
            dt.api().ajax.reload();
            $("#MyTableCheckAllButton").prop("checked", false);
        });
        $(".clear-search").on("click", function () {
            $(".search-filter").find(".dt_search_field").val("");
            dt.api().ajax.reload();
            $("#MyTableCheckAllButton").prop("checked", false);
        });
        $("body").on("change", "#MyTableCheckAllButton", function () {
            var all = $("#cases-dt")
                .DataTable()
                .rows({ search: "applied" })
                .count(); // get total count of rows
            var selectedRows = $("#cases-dt")
                .DataTable()
                .rows({ selected: true, search: "applied" })
                .count(); // get total count of selected rows

            if (selectedRows < all) {
                $("#MyTableCheckAllButton").prop("checked", true);
                //Added search applied in case user wants the search items will be selected
                $("#cases-dt")
                    .DataTable()
                    .rows({ search: "applied" })
                    .deselect();
                $("#cases-dt").DataTable().rows({ search: "applied" }).select();
            } else {
                $("#cases-dt")
                    .DataTable()
                    .rows({ search: "applied" })
                    .deselect();
            }
        });

        // Submit event listinner
        $("#add-histocall-customer-btn-bis").on("click", function () {
            // Add
            // request should be like this
            // add-form-lead_id=1&add-form-duration=4&add-form-call_descr=test&lead_status=1

            //hide previous validation errors
            histocallCustomerAddFormSelector.find(".invalid-feedback").hide();
            histocallCustomerAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            histocallCustomerAddBlockSelector.addClass("block-mode-loading");
            let seconds =
                parseInt($(".seconds").text().slice(0, 2)) +
                parseInt($(".minutes").text().slice(0, 2)) * 60 +
                parseInt($(".hours").text().slice(0, 2)) * 3600;
            $("#add-form-duration").val(seconds);

            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/histocalls`,
                data: histocallCustomerAddFormSelector.serialize(),
                success: (res) => {
                    //console.log($('.calls').last().html());
                    var date = new Date(res.data.created_at);
                    let formatted_date =
                        date.getFullYear() +
                        "-" +
                        (date.getMonth() + 1) +
                        "-" +
                        date.getDate() +
                        " " +
                        date.getHours() +
                        ":" +
                        date.getMinutes() +
                        ":" +
                        date.getSeconds();
                    //console.log(formatted_date)
                    $(
                        '<tr class="calls">' +
                            '<td class="d-none d-sm-table-cell">' +
                            '<em class="font-size-sm text-muted">' +
                            formatted_date +
                            "</em>" +
                            "</td>" +
                            '<td class="font-size-sm">' +
                            '<p class="text-muted mb-0">' +
                            res.data.call_descr +
                            "</p>" +
                            "</td>" +
                            '<td class="d-none d-sm-table-cell">' +
                            '<span class="badge badge-danger">' +
                            res.data.duration +
                            " seconds</span>" +
                            "</td>" +
                            "</tr>"
                    ).insertAfter($(".calls").last());
                    //$('<tr class="calls"><td>Call '+(length+1)+':</td><td>'++'</td><td>'+res.data.duration+'</td></tr>').insertAfter($('.calls').last());
                    $("#modal-histocall-customer-add").modal("hide");
                    //resetChrono();
                    // console.log($('#_lead_status').val());
                    // if ($("#_lead_status").val() == "Rappel") {
                    //     window.location.href = "/calendar";
                    // }
                    // location.reload();
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        histocallCustomerAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur ajout client!",
                            "error"
                        ).then(() => {
                            histocallCustomerAddBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        // for the new timer
        AjaxGet = function (customerId) {
            var result = $.ajax({
                method: "get",
                url: `${process.env.MIX_API_URL}/customerdata/${customerId}`,
                async: false,
                success: function (data) {
                    document.getElementById("customer-phone").innerHTML =
                        data.phone;
                    return data;
                },
                error: (xhr) => {
                    console.log("error");
                },
            }).responseText;
            return result;
        };

        $("body").on(
            "click",
            "button[id^='histocall-customer-table-add-']",
            function () {
                const reactHistoCall = $("#rcw-conversation-container")
                    .attr("class")
                    .split(" ")[1];

                if (reactHistoCall === "active") {
                    return;
                }
                let customerId = $(this).data("id");
                window.ajaxdata = AjaxGet(customerId);
                // add-form-lead_id=1&add-form-duration=4&add-form-call_descr=test&lead_status=1
                $("<input>")
                    .attr({
                        type: "hidden",
                        name: "add-form-lead_id",
                        value: customerId,
                    })
                    .appendTo("#histocall-add-customer-form");
                // show modal
                histocallCustomerAddFormSelector
                    .find(".invalid-feedback")
                    .hide();
                histocallCustomerAddFormSelector
                    .find(".form-control")
                    .removeClass("is-invalid");

                $("#histocall-add-customer-form").trigger("reset");
                $("#modal-histocall-customer-add").modal("show");

                // AJOUTER APPEL CLIENT
            }
        );
    }

    static deleteCase(dt, status) {
        $("#leads-dt-block").addClass("block-mode-loading");
        // console.log();
        $.ajax({
            method: "delete",
            url: `${process.env.MIX_API_URL}/case/${window.CLEARPATH_LEAD_ID}/${status.value}`,
            success: () => {
                Swal.fire(
                    "Conversion!",
                    "Dossier converti avec succés!",
                    "success"
                ).then(() => {
                    dt.api().ajax.reload(null, false);
                });
            },
            error: () => {
                Swal.fire(
                    "Conversion!",
                    "erreur conversion dossier!",
                    "error"
                ).then(() => {
                    $("#leads-dt-block").removeClass("block-mode-loading");
                });
            },
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents(this.initDataTables());
    }
}

// Initialize when page loads
jQuery(() => {
    casesListPage.init();
});
