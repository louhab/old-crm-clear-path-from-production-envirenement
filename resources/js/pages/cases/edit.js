/*
 *  Document   : edit.js
 *  Author     : RR
 *  Description: Custom JS code used in case edit page
 */

function _serialize(json) {
    return Object.keys(json)
        .map(function (key) {
            return (
                encodeURIComponent(key) + "=" + encodeURIComponent(json[key])
            );
        })
        .join("&");
}

class casesEditPage {
    static loadComments(lid = 0) {
        $("#comments-block").addClass("block-mode-loading");
        $.ajax({
            method: "get",
            url: `${process.env.MIX_API_URL}/comments/${$(
                "#main-comment-container"
            ).attr("data-comment-id")}`,
            data: lid ? { lid: lid } : {},
            success: (data) => {
                let $commentWrapper = $("#comments-wrapper");
                let $loadCommentsBtn = $("#load-old-comments");
                let $loadCommentsBtnWrapper = $("#load-old-comments-wrapper");

                if (lid) $commentWrapper.append(data);
                else $commentWrapper.html(data);

                $loadCommentsBtn.attr(
                    "data-lid",
                    $commentWrapper.children().last().attr("data-lid")
                );

                if (!$.trim(data).length) $loadCommentsBtnWrapper.hide();
                else $loadCommentsBtnWrapper.show();
            },
            error: () => {
                throw "comments list error";
            },
        }).done(function () {
            $("#comments-block").removeClass("block-mode-loading");
        });
    }
    /*
     * Init DOM events
     *
     */
    static initDOMEvents() {
        window.onload = function () {
            if (window.location.href.indexOf("page_y") != -1) {
                var match = window.location.href
                    .split("?")[1]
                    .split("&")[0]
                    .split("=");
                document.getElementsByTagName("body")[0].scrollTop = match[1];
            }
        };

        $("body").on("click", "button[id^='add-tache-']", function () {
            let customer = $(this).data("customer");
            const dataObj = {
                canal: $(this).data("canal"),
                note: $(this).data("note"),
                view: $(this).data("view"),
            };

            const serialized = _serialize(dataObj);
            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/tasks/${customer}`,
                data: serialized,
                success: () => {
                    Swal.fire(
                        "Ajout!",
                        "tache ajoutée avec succés!",
                        "success"
                    ).then(() => {
                        document.location.reload(true);
                    });
                },
                error: (xhr) => {
                    Swal.fire("Ajout!", "erreur ajout tache!", "error").then(
                        () => {
                            formAddAdmissionFeesSelector.removeClass(
                                "block-mode-loading"
                            );
                        }
                    );
                },
            });
        });

        $("body").on("click", "button[id^='update-tache-']", function () {
            const taskID = $(this).data("task");
            const note = $(this).data("note");

            const dataObj = {
                canal: $(this).data("canal"),
                view: $(this).data("view"),
                customer: $(this).data("customer"),
                note: $(this).data("note"),
            };

            const serialized = _serialize(dataObj);
            $.ajax({
                method: "put",
                url: `${process.env.MIX_API_URL}/tasks/${taskID}`,
                data: serialized,
                success: () => {
                    Swal.fire(
                        "Modif!",
                        `tache definie comme ${note} succés!`,
                        "success"
                    ).then(() => {
                        document.location.reload(true);
                    });
                },
                error: (xhr) => {
                    Swal.fire(
                        "Modif!",
                        "erreur modification de tache!",
                        "error"
                    ).then(() => {
                        formAddAdmissionFeesSelector.removeClass(
                            "block-mode-loading"
                        );
                    });
                },
            });
        });

        $("body").on("click", ".method-modal-btn", function () {
            $("#modal-payment-add .case-state-update").attr(
                "data-case",
                $(this).data("case")
            );
            for (let i = 1; i < 6; i++) {
                $(`#paiement-${i}-proof`).css("display", "none");
            }

            $(`#paiement-${$(this).data("paiement")}-proof`).css(
                "display",
                "block"
            );

            $("#modal-payment-add").modal("show");
        });
        //Reférence
        $('select[name="campaign_id"]').change(function () {
            let campaign = $(this).val();
            if (campaign) {
                $.ajax({
                    method: "get",
                    url: `${process.env.MIX_API_URL}/forms/${campaign}`,
                    success: (data) => {
                        // sort Canals
                        let arr = [];
                        for (const [id, value] of Object.entries(data)) {
                            arr.push({
                                id,
                                value,
                            });
                        }
                        arr = arr.sort((a, b) =>
                            String(a.value).toLowerCase() >
                            String(b.value).toLowerCase()
                                ? 1
                                : String(b.value).toLowerCase() >
                                  String(a.value).toLowerCase()
                                ? -1
                                : 0
                        );
                        if (arr.length > 0) {
                            $('select[name="form"]').empty();
                            var influencers =
                                '<option value="" label="Selectionner...">Selectionner...</option>';
                            for (const { id, value } of arr) {
                                influencers +=
                                    '<option value="' +
                                    id +
                                    '">' +
                                    value +
                                    "</option>";
                            }
                            // end sort algo
                            $('select[name="form"]').append(influencers);
                            $('#lead-form label[for="form"]').text(
                                $(
                                    '#lead-form select[name="campaign_id"] option:selected'
                                ).text()
                            );
                            $('#lead-form select[name="form"]')
                                .parent()
                                .parent()
                                .show();
                            $('#lead-form input[name="reference"]')
                                .parent()
                                .parent()
                                .hide();
                        } else {
                            $('#lead-form select[name="form"]')
                                .parent()
                                .parent()
                                .hide();
                            $('#lead-form input[name="reference"]')
                                .parent()
                                .parent()
                                .hide();
                            //$('select[name="influencer"]').empty();
                        }
                        if (campaign == 23) {
                            $('#lead-form select[name="form"]')
                                .parent()
                                .parent()
                                .hide();
                            $('#lead-form input[name="reference"]')
                                .parent()
                                .parent()
                                .show();
                        }
                    },
                    error: () => {
                        console.log("error!");
                    },
                });
            } else {
                $('select[name="form"]').parent().parent().hide();
                $('select[name="form"]').empty();
            }
        });
        // jQuery('#modal-payment-add').on('hidden.bs.modal', function (e) {
        //     // do something...
        //     //$("#modal-payment-add .case-state-update").attr('data-case', "");
        // });
        $("body").on("click", ".case-state-update", function () {
            $(".method-modal-btn")
                .closest(".block")
                .addClass("block-mode-loading");
            //console.log($(this).closest('.block').attr('class'));

            //let stateBtn = $(this);
            //let iconStatus = $(this).closest("tr").find("a").first();
            let customer_case = $(this).data("case");
            let method = $(this).data("method");
            let parameters = customer_case;
            if (method) {
                parameters += "/" + method;
                $.get(
                    `${process.env.MIX_API_URL}/payments/${parameters}`,
                    function (data, status) {
                        console.log("Data: " + data + "\nStatus: " + status);
                    }
                );
            }
            // console.log("parameters: " + $(this).html());
            //let step_state = $(this).data();

            // console.log("this params ..", parameters);
            // return;

            //console.log(iconStatus.html());
            $.ajax({
                method: "put",
                url: `${process.env.MIX_API_URL}/states/${parameters}`,
                success: (data) => {
                    if (data.name == "Message whatsapp") {
                        if (typeof data.phone !== "undefined") {
                            let message;
                            let lang = document.getElementById("locale").value;
                            let detection_lang_user = document.getElementById(
                                "detection_lang_user"
                            ).value;
                            //= 'Bonjour+'+data.firstname +"+"+ data.lastname+'%0D%0A%0D%0AJ%E2%80%99esp%C3%A8re+que+vous+allez+bien+%3F+%0D%0A%0D%0AJe+suis+'+data.conseiller+'+votre+conseiller+de+Clear+Path+Canada.%0D%0A%0D%0AJe+vous+remercie+pour+votre+int%C3%A9r%C3%AAt.+Pour+plus+de+renseignements%2C+nous+serons+ravis+de+vous+accueillir+dans+nos+locaux%2C+afin+de+vous+pr%C3%A9senter+notre+programme.%0D%0A%0D%0ACordialement%2C%0D%0A'+data.conseiller+'+%0D%0AConseiller+Clear+%0D%0APath+Canada+%0D%0ATel+%3A+%2B'+data.conseiller_phone+'%0D%0AAdresse+%3A+42%2C+rue+Bachir+Laalej%2C+Casablanca+-+Maroc%0D%0ALocalisation+%3A+https%3A%2F%2Fmaps.google.com%2F%3Fq%3D33.571827%2C-7.627207';
                            //let url = "https://wa.me/"+data.phone+"?text=Bonjour \n<br /> tt: "+ data.firstname +" "+ data.lastname+" ‏cv bien ?";
                            // console.log("testing");
                            // console.log(data);
                            console.log(data);
                            if (data.gender == "male") {
                                if (
                                    lang == "fr" ||
                                    detection_lang_user == "en"
                                ) {
                                    message =
                                        "Hello," +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "The Clear Path Canada team thanks you for your interest. I am " +
                                        data.conseiller +
                                        ", and I will be your advisor. Since you filled our form, I will call you to advise you about how you can go to Canada.";
                                } else {
                                    message =
                                        "Bonjour " +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "Toute l’équipe Clear Path Canada vous remercie de votre intérêt. Je suis " +
                                        data.conseiller +
                                        " votre conseiller, pour plus de renseignements je serais ravi de vous accueillir dans nos locaux, afin de vous accompagner pour réaliser votre rêve.";
                                }
                            }
                            if (data.gender == "female") {
                                if (
                                    lang == "fr" ||
                                    detection_lang_user == "en"
                                ) {
                                    message =
                                        "Hello," +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "The Clear Path Canada team thanks you for your interest. I am " +
                                        data.conseiller +
                                        ", and I will be your advisor. Since you filled our form, I will call you to advise you about how you can go to Canada.";
                                } else {
                                    message =
                                        "Bonjour " +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "Toute l’équipe Clear Path Canada vous remercie de votre intérêt. Je suis " +
                                        data.conseiller +
                                        " votre conseillère, pour plus de renseignements je serais ravie de vous accueillir dans nos locaux, afin de vous accompagner pour réaliser votre rêve.";
                                }
                            }
                            if (data.user_role == 7) {
                                if (
                                    lang == "fr" ||
                                    detection_lang_user == "en"
                                ) {
                                    message =
                                        "Hello," +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "The Clear Path Canada team thanks you for your interest. I am " +
                                        data.conseiller +
                                        ", and I will be your advisor. Since you filled our form, I will call you to advise you about how you can go to Canada.";
                                } else {
                                    message =
                                        "Bonjour " +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        ", Toute l’équipe Clear Path Canada vous remercie de votre intérêt. " +
                                        "Pour plus de renseignements, nous serons ravis de vous accueillir dans nos locaux, afin de vous accompagner pour réaliser votre rêve.";
                                }
                                /*message = "Bonjour, \n" +
                                        "Toute l’équipe de Clearpath Canada vous remercie de votre intérêt.\n" +
                                        "Pour plus de renseignements, nous serons ravis de vous accueillir dans\n" +
                                        "nos locaux, afin de vous orienter vers la démarche adéquate à votre profil.";*/
                            }
                            if (data.user_role == 4) {
                                if (
                                    lang == "fr" ||
                                    detection_lang_user == "en"
                                ) {
                                    message =
                                        "Hello," +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "The Clear Path Canada team thanks you for your interest. I am " +
                                        data.conseiller +
                                        ", and I will be your advisor. Since you filled our form, I will call you to advise you about how you can go to Canada.";
                                } else {
                                    message =
                                        "Bonjour " +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "Toute l’équipe Clear Path Canada vous remercie de votre intérêt. Je suis " +
                                        data.conseiller +
                                        "." +
                                        `%0a` +
                                        "Pour plus de renseignements, je serais ravie de vous accueillir dans nos locaux, afin de rencontrer nos conseillers spécialisés qui vont vous orienter vers la démarche adéquate à votre profil\n";
                                }
                                //Si English
                                // message = "Hello " + data.firstname + " " + data.lastname + ",\n" +
                                //     "All Clear Path Canada team thanks you for your interest. I'm " + data.conseiller + ".\n" +
                                //     "For more information, it will be a pleasure to receive you at our office, in order to meet our specialized advisers who will direct and help you to achieve your goals in the most effective way.\n";
                            }

                            window.open(
                                "https://wa.me/" +
                                    data.phone +
                                    "?text=" +
                                    message,
                                "_blank"
                            );
                            //location.reload();
                            if (
                                $(this).parent().find("i.fa-check-circle")
                                    .length == 0
                            ) {
                                $(this)
                                    .parent()
                                    .prepend(
                                        '<button type="button" class="btn btn-sm text-success"><i class="far fa-2x fa-check-circle"></i></button>'
                                    );
                            }
                            $("#lead-form").hide();
                            $("#lead-info").show();
                        } else {
                            Swal.fire(
                                "Whatsapp!",
                                "Numéro de telephone manquant!",
                                "error"
                            );
                        }
                    } else {
                        Swal.fire(
                            "Modification!",
                            "Status modiffié avec succés!",
                            "success"
                        ).then(() => {
                            location.reload();
                        });
                    }
                    $(".method-modal-btn")
                        .closest(".block")
                        .removeClass("block-mode-loading");
                },
                error: () => {
                    Swal.fire(
                        "Modification!",
                        "erreur modification status!",
                        "error"
                    );
                },
            });
        });
        // payment methods
        $("#method-select").select2({}).empty();
        $("#method-select").select2({
            placeholder: "Espèce",
            ajax: {
                url: `${process.env.MIX_API_URL}/getPaymentMethods`,
                dataType: "json",
                delay: 250,
                method: "POST",
                data: function (term, page) {
                    return {
                        q: term,
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                            };
                        }),
                    };
                },
                cache: false,
            },
        });
        $("#method-select").on("select2:select", function (e) {
            //c o n s o l e  . l o g ( $ ( t h i s ) . v a l ( ) ) ;
            $(".case-state-update[data-method]").attr(
                "data-method",
                $(this).val()
            );
        });

        // set meet is done
        // $(".meet-is-done").on("click", function () {
        //     let meet = $(this)
        //         .closest(".block")
        //         .find(".block-title")
        //         .data("prefix")
        //         .replace("Rencontre ", "r");

        //     let customer = $(this)
        //         .closest(".block")
        //         .find(".block-title")
        //         .data("customer");
        //     Swal.fire({
        //         title: "Etes vous sur?",
        //         text: "L'opération est irréversible!",
        //         icon: "warning",
        //         showCancelButton: true,
        //         confirmButtonColor: "#3085d6",
        //         cancelButtonColor: "#d33",
        //         confirmButtonText: "Oui!",
        //         cancelButtonText: "Annuler",
        //     }).then((result) => {
        //         if (result.isConfirmed) {
        //             $.ajax({
        //                 method: "get",
        //                 url: `${process.env.MIX_API_URL}/customer-meet/${customer}/${meet}`,
        //                 success: () => {
        //                     Swal.fire(
        //                         "Modification!",
        //                         "Rencontre modifié avec succés!",
        //                         "success"
        //                     ).then(() => {
        //                         location.reload();
        //                     });
        //                 },
        //                 error: (xhr) => {
        //                     if (xhr.statusCode().status === 422) {
        //                         $.each(
        //                             xhr.responseJSON.errors,
        //                             function (key, value) {}
        //                         );
        //                     } else
        //                         Swal.fire(
        //                             "Modification!",
        //                             "erreur modification Remcontre!",
        //                             "error"
        //                         ).then(() => {});
        //                 },
        //             });
        //         }
        //     });
        // });

        $(".save-orientation-btn").on("click", function () {
            const id = $(this).data("customer");
            const orientationId = $('input[name="orientation"]:checked').val();
            $.ajax({
                method: "get",
                url: `${process.env.MIX_API_URL}/customer-orientation/${id}/${orientationId}`,
                success: () => {
                    Swal.fire(
                        "Ajout!",
                        "Choix d'orientaion modifieé avec succés!",
                        "success"
                    ).then(() => {
                        location.reload();
                    });
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(
                            xhr.responseJSON.errors,
                            function (key, value) {}
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur modif orientation!",
                            "error"
                        ).then(() => {});
                },
            });
        });

        //ADD FORM
        // let calendarAddBlockSelector = $('#block-calendar-add');
        let calendarAddFormSelector = $("#calendar-add-form");

        $(".calendar-table-add").on("click", function () {
            let meetName = $(this)
                .closest(".block")
                .find(".block-title")
                .data("prefix");
            $("#add-form-title")
                .prev()
                .find("span")
                .text(meetName.replace("Rencontre ", "R"));
            $("#add-form-title").val($("#add-form-title").prev().text());
            //console.log();
            //hide previous validation errors
            calendarAddFormSelector.find(".invalid-feedback").hide();
            calendarAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            $("#calendar-add-form").trigger("reset");
            $("#modal-calendar-add").modal("show");
        });
        $("#add-form-time").change(function () {
            var moment = require("moment"); // require
            let time_end = moment
                .utc($(this).val(), "hh:mm")
                .add(45, "minutes")
                .format("HH:mm");
            $("#add-form-time_end").val(time_end);
        });
        $("#add-calendar-btn").on("click", function () {
            // testing
            // $( "#calendar-add-form" ).submit();
            $("#block-calendar-add").addClass("block-mode-loading");
            $.ajax({
                method: "post",
                url: `/calendar/create`,
                data: $("#calendar-add-form").serialize(),
                success: () => {
                    Swal.fire(
                        "Ajout!",
                        "Calendar ajouté avec succés!",
                        "success"
                    ).then(() => {
                        $("#block-calendar-add").removeClass(
                            "block-mode-loading"
                        );
                        location.reload();
                    });
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        $("#block-calendar-add").removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur ajout calendar!",
                            "error"
                        ).then(() => {
                            $("#block-calendar-add").removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        /*$('.toggle-property').on('click', function(event) {
            event.preventDefault()
            let $form = $(`#${$(this).attr('data-formId')}`)
            let input = jQuery(`<input name="property" value="${$(this).attr('data-property')}">`)
            $form.append(input)
            $form.submit()
        })*/

        $(".toggle-media-sheet-visibility").on("click", function (event) {
            event.preventDefault();
            $(`#${$(this).attr("data-formId")}`).submit();
        });
        $(".toggle-link-visibility, .toggle-sheet-visibility").on(
            "click",
            function (event) {
                event.preventDefault();
                $(`#${$(this).attr("data-formId")}`).submit();
            }
        );

        $("body").on("click", "#toggle-lead-form", (event) => {
            const clickedBtn = $(event.target);
            $("#lead-form").toggle();
            $("#lead-info").toggle();
            if (clickedBtn.text() == "Editer") {
                clickedBtn.replaceWith(
                    '<button type="button" id="toggle-lead-form" class="btn btn-sm btn-outline-secondary">Annuler</button>'
                );
            } else {
                clickedBtn.replaceWith(
                    '<button type="button" id="toggle-lead-form" class="btn btn-sm btn-alt-primary">Editer</button>'
                );
                // clickedBtn.text('Editer');
            }
            //console.log(clickedBtn.html());
            //$(this).closest('.block').removeClass('block-mode-loading');
        });

        // add block tous les appels
        let histocallCustomerAddBlockSelector = $(
            "#block-histocall-customer-add"
        );
        let histocallCustomerAddFormSelector = $(
            "#histocall-add-customer-form"
        );

        $("#histocall-customer-table-add").on("click", function () {
            const reactHistoCall = $("#rcw-conversation-container")
                .attr("class")
                .split(" ")[1];

            if (reactHistoCall === "active") {
                return;
            }

            //hide previous validation errors
            histocallCustomerAddFormSelector.find(".invalid-feedback").hide();
            histocallCustomerAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            $("#histocall-add-customer-form").trigger("reset");
            $("#modal-histocall-customer-add").modal("show");
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents();
        // this.loadComments()
    }
}

// Initialize when page loads
jQuery(() => {
    casesEditPage.init();
});
