// var lang = $('input[name="lang"]').val();

// /*
//  *  Document   : calendar.js
//  *  Author     : RR
//  *  Description: Custom JS code used in countries page
//  */

// class calendarPage {
//     golbal_calendar = null;
//     events = [];
//     static initFetch(__data = null) {
//         function getData(__data = null) {
//             return $.ajax({
//                 method: "GET",
//                 data: __data,
//                 url: `/api/v1/calendar`,
//                 success: (data) => {
//                     calendar__ = data.calendar;
//                     events = [];
//                     for (let i = 0; i < Object.keys(calendar__).length; i++) {
//                         let title = calendar__[i].title;
//                         title = title.replace("Rencontre 2", "R2");
//                         title = title.replace("Rencontre 1", "R1");
//                         title = title.replace("Rencontre 3", "R3");
//                         events.push({
//                             start: new Date(
//                                 calendar__[i].year,
//                                 calendar__[i].month - 1,
//                                 calendar__[i].day,
//                                 calendar__[i].hour,
//                                 calendar__[i].minute
//                             ),
//                             end: new Date(
//                                 calendar__[i].year,
//                                 calendar__[i].month - 1,
//                                 calendar__[i].day,
//                                 calendar__[i].hourEnd,
//                                 calendar__[i].minuteEnd
//                             ),
//                             title: title,
//                             RTitle: calendar__[i].title,
//                             description: calendar__[i].description,
//                             customer_id: calendar__[i].customer_id,
//                             step: calendar__[i].step,
//                             isClient: calendar__[i].is_client,
//                             lead_id: calendar__[i].lead_id,
//                             customer_name: calendar__[i].customer_name,
//                             time_start: calendar__[i].time_start,
//                             time_end: calendar__[i].time_end,
//                             id: calendar__[i].id,
//                         });
//                     }
//                 },
//                 error: (e) => {
//                     console.log(e);
//                 },
//             });
//         }

//         async function __fetch(__data = null) {
//             try {
//                 await getData(__data);
//             } catch (err) {
//                 console.log(err);
//             }
//         }

//         __fetch();
//     }
//     /*
//      *
//      */
//     static initDataTables() {
//         $("#delete-calendar-btn").click(function () {
//             $(this)
//                 .closest("form")
//                 .attr(
//                     "action",
//                     APP_URL + "/calendar/destroy/" + $("#edit-form-id").val()
//                 );
//         });

//         //ADD FORM
//         let calendarAddBlockSelector = $("#block-calendar-add");
//         let calendarAddFormSelector = $("#calendar-add-form");
//         let calendarEditFormSelector = $("#calendar-edit-form");

//         $("#calendar-table-add").on("click", function () {
//             //hide previous validation errors
//             calendarAddFormSelector.find(".invalid-feedback").hide();
//             calendarAddFormSelector
//                 .find(".form-control")
//                 .removeClass("is-invalid");

//             $("#calendar-add-form").trigger("reset");
//             $("#modal-calendar-add").modal("show");
//         });

//         $("#add-form-time").change(function () {
//             var moment = require("moment"); // require
//             let time_end = moment
//                 .utc($(this).val(), "hh:mm")
//                 .add(45, "minutes")
//                 .format("HH:mm");
//             $("#add-form-time_end").val(time_end);
//         });

//         $("#add-form-lead").select2({}).empty();

//         $("#add-form-lead").select2({
//             placeholder: "Lead..",
//             ajax: {
//                 url: `${process.env.MIX_API_URL}/getCustomers`,
//                 dataType: "json",
//                 delay: 250,
//                 method: "POST",
//                 data: function (term, page) {
//                     return {
//                         q: term,
//                         cal: "true",
//                     };
//                 },
//                 processResults: function (data) {
//                     return {
//                         results: $.map(data, function (item) {
//                             return {
//                                 isClient: item.is_client,
//                                 text: item.name,
//                                 id: item.id,
//                             };
//                         }),
//                     };
//                 },
//                 cache: false,
//             },
//         });

//         $("#add-form-lead").on("select2:select", function (e) {
//             var data = e.params.data;
//             if (data.isClient) {
//                 $("#meet-step-label").text("Rencontre 2");
//             } else {
//                 $("#meet-step-label").text("Rencontre 1");
//             }
//         });

//         $("#edit-form-lead").select2({
//             placeholder: "Lead..",
//             ajax: {
//                 url: `${process.env.MIX_API_URL}/getCustomers`,
//                 dataType: "json",
//                 delay: 250,
//                 method: "POST",
//                 data: function (term, page) {
//                     return {
//                         q: term,
//                         cal: "true",
//                     };
//                 },
//                 processResults: function (data) {
//                     return {
//                         results: $.map(data, function (item) {
//                             return {
//                                 isClient: item.is_client,
//                                 text: item.name,
//                                 id: item.id,
//                             };
//                         }),
//                     };
//                 },
//                 cache: false,
//             },
//         });

//         calendarAddFormSelector.submit(function () {
//             let title = $("#add-form-title").val();

//             $("#add-form-title").val(
//                 $("#meet-step-label").text() + " : " + title
//             );
//         });
//         calendarEditFormSelector.submit(function () {
//             // your code here
//             let title = $("#edit-form-title").val();
//             $("#edit-form-title").val(
//                 $("#meet-step-label-edit").text() + " : " + title
//             );
//         });
//     }

//     /*
//      * Init functionality
//      *
//      */
//     static renderCalendar() {
//         this.golbal_calendar = new FullCalendar.Calendar($(".js-calendar")[0], {
//             headerToolbar: {
//                 left: "prev,next today",
//                 center: "title",
//                 right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth",
//             },
//             initialView: "listMonth",
//             eventTimeFormat: {
//                 // like '14:30'
//                 hour: "2-digit",
//                 minute: "2-digit",
//                 meridiem: false,
//             },
//             // timeFormat: "H(:mm)",
//             locale: lang,
//             buttonIcons: false, // show the prev/next text
//             weekNumbers: true,
//             navLinks: true, // can click day/week names to navigate views
//             editable: true,
//             dayMaxEvents: true, // allow "more" link when too many events
//             events: this.events,
//             eventClick: function (event, jsEvent, view) {
//                 $("#edit-form-id").val(event.event.id);
//                 let title = event.event.title;
//                 if (event.event.extendedProps.isClient) {
//                     $("#meet-step-label-edit").text("Rencontre 2");
//                     title = event.event.title.replace("Rencontre 2", "");
//                 } else {
//                     $("#meet-step-label-edit").text("Rencontre 1");
//                     title = event.event.title.replace("Rencontre 1", "");
//                 }
//                 title = title.replace(":", " ");
//                 title = $.trim(title);

//                 $("#edit-form-title").val(event.event.extendedProps.RTitle);
//                 $("#edit-form-date").val(event.event.startStr);
//                 $("#edit-form-description").text(
//                     event.event.extendedProps.description
//                 );
//                 $("#edit-form-time").val(event.event.extendedProps.time_start);
//                 $("#edit-form-time_end").val(
//                     event.event.extendedProps.time_end
//                 );
//                 $("#voir-calendar-btn").attr(
//                     "href",
//                     `${process.env.MIX_PUBLIC_URL}` +
//                         "/lead/" +
//                         event.event.extendedProps.lead_id
//                 );
//                 $("#modal-calendar-edit").modal();
//                 var $newOption = $("<option selected='selected'></option>")
//                     .val(event.event.extendedProps.customer_id)
//                     .text(event.event.extendedProps.customer_name);
//                 $("#edit-form-lead").append($newOption).trigger("change");
//             },
//         }).render();
//     }

//     /*
//      * Init functionality
//      *
//      */
//     static init() {
//         this.initDataTables();
//         this.initFetch();

//         new Promise((resolve) => {
//             if (this.events?.length) {
//                 resolve(this.events);
//             }

//             const observer = new MutationObserver((mutations) => {
//                 if (this.events?.length) {
//                     resolve(this.events);
//                     // observer.disconnect();
//                 }
//             });

//             observer.observe(document.body, {
//                 childList: true,
//                 subtree: true,
//             });
//         }).then(() => {
//             console.log("testing rerendring ..");
//             console.log(this.golbal_calendar);
//             if (this.golbal_calendar) this.golbal_calendar.removeAllEvents();
//             this.renderCalendar();
//         });
//     }
// }

// // Initialize when page loads
// jQuery(() => {
//     calendarPage.init();
// });

const lang = $('input[name="lang"]').val();

const __main__ = new FullCalendar.Calendar($(".js-calendar")[0], {
    views: {
        dayGridMonth: { buttonText: 'Mois' },
        listMonth: { buttonText: 'List' },
        listWeek: { buttonText: 'Planning' },
    },
    headerToolbar: {
        left: "prev,next today",
        center: "title",
        right: "dayGridMonth,listMonth,listWeek",
    },
    initialView: "listWeek",
    timeFormat: "H(:mm)",
    locale: lang,
    buttonIcons: false, // show the prev/next text
    weekNumbers: true,
    navLinks: true, // can click day/week names to navigate views
    editable: true,
    dayMaxEvents: true, // allow "more" link when too many events
    events: events,
    firstDay: new Date().getDay(),
    eventClick: function (event, jsEvent, view) {
        $("#edit-form-id").val(event.event.id);
        // console.log(event.event.extendedProps);
        let title = event.event.title;
        if (event.event.extendedProps.isClient) {
            $("#meet-step-label-edit").text("Rencontre 2");
            title = event.event.title.replace("RDV", "");
        } else {
            $("#meet-step-label-edit").text("Rencontre 1");
            title = event.event.title.replace("RDV", "");
        }
        title = title.replace(":", " ");
        // title = $.trim(title);
        title = title.trim();

        // $('#edit-form-title').val(title);
        console.log(event.event.extendedProps);
        if (
            event.event.extendedProps.eventType &&
            event.event.extendedProps.eventType !== "rencontre" &&
            event.event.extendedProps.eventType !== "rappel"
        ) {
            $("#edit-form-type").val(event.event.extendedProps.eventType);
            $("#edit-form-title-bis").val(event.event.extendedProps.RTitle);
            $("#edit-form-lead").parent().hide();
            $("#edit-form-title-bis").parent().show();
            $("#edit-form-title").parent().parent().hide();
            $("#edit-form-title").val("");
        } else {
            $("#edit-form-title-bis").val("");
            $("#edit-form-lead").parent().show();
            $("#edit-form-title-bis").parent().hide();
            $("#edit-form-title").parent().parent().show();
            $("#edit-form-type").val(event.event.extendedProps.eventType);
            $("#edit-form-title").val(event.event.extendedProps.RTitle);
        }
        // formating date
        const date = new Date(event.event.startStr);
        const day = date.getDate().toString().padStart(2, "0");
        const month = (date.getMonth() + 1).toString().padStart(2, "0");
        const year = date.getFullYear().toString();
        const formattedDate = year + "-" + month + "-" + day;

        $("#edit-form-date").val(formattedDate);
        $("#edit-form-description").text(event.event.extendedProps.description);
        $("#edit-form-time").val(event.event.extendedProps.time_start);
        $("#edit-form-time_end").val(event.event.extendedProps.time_end);
        $("#voir-calendar-btn").attr(
            "href",
            `${process.env.MIX_PUBLIC_URL}` +
                "/lead/" +
                event.event.extendedProps.lead_id
        );
        //$('#edit-form-lead').select2("val", event.event.extendedProps.customer);
        $("#modal-calendar-edit").modal();
        //$("#edit-form-lead").select2('data', { id: event.event.extendedProps.customer_id, text: event.event.extendedProps.customer_name});
        // console.log(event.event.extendedProps);
        var $newOption = $("<option selected='selected'></option>")
            .val(event.event.extendedProps.customer_id)
            .text(event.event.extendedProps.customer_name);
        $("#edit-form-lead").append($newOption).trigger("change");
    },
    /*eventRender: function(event, element) {
        element.find('.fc-title').append("<br/>" + event.description);
    }*/
});

__main__.render();

/*calendar.on('dateClick', function(info) {
    console.log(info);
});*/
$("#delete-calendar-btn").click(function () {
    $(this)
        .closest("form")
        .attr(
            "action",
            APP_URL + "/calendar/destroy/" + $("#edit-form-id").val()
        );
});

// SEARCH FORM
let calendarSearchFormSelector = $("#search-form");

//ADD FORM
let calendarAddBlockSelector = $("#block-calendar-add");
let calendarAddFormSelector = $("#calendar-add-form");
let calendarEditFormSelector = $("#calendar-edit-form");

$("#calendar-table-add").on("click", function () {
    //hide previous validation errors
    calendarAddFormSelector.find(".invalid-feedback").hide();
    calendarAddFormSelector.find(".form-control").removeClass("is-invalid");

    $("#calendar-add-form").trigger("reset");
    $("#modal-calendar-add").modal("show");
});

$(".submit-search").on("click", () => {
    // console.log("test on click");
    // console.log(events);
    let searchFilterSelector = $(".search-filter");
    // console.log("okey google ::> ", calendarSearchFormSelector.serialize());

    const data__ = calendarSearchFormSelector.serialize();

    // console.log($(".search-filter").select2("val"), "l-----");

    // if (data__.includes("advisors")) {
    const __events__ = [];
    $.ajax({
        method: "GET",
        data: calendarSearchFormSelector.serialize(),
        url: `/api/v1/calendar`,
        success: async (data) => {
            // calendar__ = data.calendar;
            // console.log("the calendar ", calendar__);
            // console.log(data.calendar, "-----(1)");
            // console.log(calendarSearchFormSelector.serialize(), "-----(2)");
            // events = [];
            // __main__.der;
            __main__.getEventSources().forEach(function (item) {
                item.remove();
            });
            for (let i = 0; i < Object.keys(data.calendar).length; i++) {
                let title = data.calendar[i].title;
                title = title.replace("Rencontre 2", "R2");
                title = title.replace("Rencontre 1", "R1");
                title = title.replace("Rencontre 3", "R3");
                __events__.push({
                    start: new Date(
                        data.calendar[i].year,
                        data.calendar[i].month - 1,
                        data.calendar[i].day,
                        data.calendar[i].hour,
                        data.calendar[i].minute
                    ),
                    end: new Date(
                        data.calendar[i].year,
                        data.calendar[i].month - 1,
                        data.calendar[i].day,
                        data.calendar[i].hourEnd,
                        data.calendar[i].minuteEnd
                    ),
                    title: title,
                    RTitle: data.calendar[i].title,
                    description: data.calendar[i].description,
                    customer_id: data.calendar[i].customer_id,
                    step: data.calendar[i].step,
                    isClient: data.calendar[i].is_client,
                    lead_id: data.calendar[i].lead_id,
                    customer_name: data.calendar[i].customer_name,
                    time_start: data.calendar[i].time_start,
                    time_end: data.calendar[i].time_end,
                    id: data.calendar[i].id,
                });
            }
            __main__.addEventSource(__events__);
        },
        error: (e) => {
            console.log(e);
        },
    });
    // console.log("2 :: >", events);
    // }
});

$("#edit-form-date").on("change", function () {
    console.log(this.value);
});

// Changes :
$("#remove-events").on("click", function () {
    __main__.getEventSources().forEach(function (item) {
        item.remove();
    });
    // __main__.addEventSource([]);
    // console.log(events);
    // events = [];
    // console.log(__main__.events());
    // __main__.destroy();
    // __main__("removeEvents");
    // const __test__ = $("#calendar")[0];
    // console.log(__test__);
    // __test__.fullCalendar("removeEvents");
    // console.log("this is main ::", __main__);
    setTimeout(() => {
        // __main__.r();

        // __main__.events = events;
        __main__.render();
        console.log("noting");
    }, 2000);
});

$("#add-form-title-bis").parent().hide();

// Add modal
$("#add-form-type").on("change", function () {
    if (this.value != "rencontre" && this.value != "rappel") {
        $("#add-form-title").parent().parent().hide();
        $("#add-form-lead").parent().hide();
        $("#add-form-title-bis").parent().show();
    } else {
        $("#add-form-title").parent().parent().show();
        $("#add-form-lead").parent().show();
        $("#add-form-title-bis").parent().hide();
    }
});

// Edit Modal
$("#edit-form-type").on("change", function () {
    if (this.value != "rencontre" && this.value != "rappel") {
        $("#edit-form-title").parent().parent().hide();
        $("#edit-form-lead").parent().hide();
        $("#edit-form-title-bis").parent().show();
    } else {
        $("#edit-form-title").parent().parent().show();
        $("#edit-form-lead").parent().show();
        $("#edit-form-title-bis").parent().hide();
    }
});

$("#add-form-time").change(function () {
    var moment = require("moment"); // require
    let time_end = moment
        .utc($(this).val(), "hh:mm")
        .add(45, "minutes")
        .format("HH:mm");
    $("#add-form-time_end").val(time_end);
});

$("#add-form-lead").select2({}).empty();

$("#add-form-lead").select2({
    placeholder: "Lead..",
    ajax: {
        url: `${process.env.MIX_API_URL}/getCustomers`,
        dataType: "json",
        delay: 250,
        method: "POST",
        data: function (term, page) {
            return {
                q: term,
                cal: "true",
            };
        },
        processResults: function (data) {
            // console.log(data);
            return {
                results: $.map(data, function (item) {
                    return {
                        isClient: item.is_client,
                        text: item.name,
                        id: item.id,
                    };
                }),
            };
        },
        cache: false,
    },
});

$("#add-form-lead").on("select2:select", function (e) {
    console.log("teeeesting ....", $("#add-form-type").val());
    var data = e.params.data;
    // console.log(data);

    if ($("#add-form-type").val() == "rappel") {
        $("#meet-step-label").text("Rappel");
        $("#add-form-title-hidden").val("Rappel");
    } else {
        if (data.isClient) {
            $("#meet-step-label").text("Rencontre 2");
            $("#add-form-title-hidden").val("R2");
        } else {
            $("#meet-step-label").text("Rencontre 1");
            $("#add-form-title-hidden").val("R1");
        }
    }
});

$("#edit-form-lead").select2({}).empty();

$("#edit-form-lead").select2({
    placeholder: "Lead..",
    ajax: {
        url: `${process.env.MIX_API_URL}/getCustomers`,
        dataType: "json",
        delay: 250,
        method: "POST",
        data: function (term, page) {
            return {
                q: term,
                cal: "true",
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        isClient: item.is_client,
                        text: item.name,
                        id: item.id,
                    };
                }),
            };
        },
        cache: false,
    },
});

calendarAddFormSelector.submit(function () {
    // your code here
    $("#add-form-title").val($("#meet-step-label").text());
});
