/*
 *  Document   : list.js
 *  Author     : ahaloua
 */
window.CLEARPATH_LEAD_ID = null;

const countDown = (issueTime, context) => {
    function timer() {
        // diff = time in secs
        let diff = Math.ceil((Date.now() - new Date(issueTime)) / 1000);

        let secondsToDisplay = diff % 60 | 0;
        let minutesRemaining = ((diff - secondsToDisplay) / 60) | 0;
        let minutesToDisplay = minutesRemaining % 60;
        let hoursToDisplay = (minutesRemaining - minutesToDisplay) / 60;

        hoursToDisplay =
            hoursToDisplay < 10 ? "0" + hoursToDisplay : hoursToDisplay;
        minutesToDisplay =
            minutesToDisplay < 10 ? "0" + minutesToDisplay : minutesToDisplay;
        // secondsToDisplay =
        //     secondsToDisplay < 10 ? "0" + secondsToDisplay : secondsToDisplay;

        context.textContent = hoursToDisplay + ":" + minutesToDisplay;
    }
    // we don't want to wait a full second before the timer starts
    timer();
    setInterval(timer, 1000);
};

const markTaskAsDone = function (taskID) {
    $.ajax({
        method: "put",
        url: `${process.env.MIX_API_URL}/tasks/${taskID}`,
        success: () => {
            Swal.fire("Done!", "Rendre la tâche terminée!", "success").then(
                () => {
                    window.location.reload();
                }
            );
        },
        error: () => {
            Swal.fire("Suppression!", "erreur suppression task!", "error").then(
                () => {}
            );
        },
    });
};

const deleteTask = function (taskID) {
    $.ajax({
        method: "delete",
        url: `${process.env.MIX_API_URL}/tasks/${taskID}`,
        success: () => {
            Swal.fire(
                "Suppression!",
                "Email Notification supprimé avec succés!",
                "success"
            ).then(() => {
                window.location.reload();
            });
        },
        error: () => {
            Swal.fire("Suppression!", "erreur suppression task!", "error").then(
                () => {}
            );
        },
    });
};

class TasksPage {
    static init() {
        // Load the correct tab with # url
        const anchor = window.location.hash;
        $(`a[href="${anchor}"]`).tab("show");

        $("[id^=task-]").each(function () {
            countDown($(this).val(), $(this).prev("p").get(0));
        });

        $("body").on("click", "button[id^='end-comment-case-']", function () {
            const comment_cases_id = $(this).data("id");
            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    markTaskAsDone(comment_cases_id);
                }
            });
        });

        $("body").on("click", "button[id^='delcomment-case-']", function () {
            const comment_cases_id = $(this).data("id");
            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    deleteTask(comment_cases_id);
                }
            });
        });
    }
}

// Initialize when page loads
jQuery(() => {
    TasksPage.init();
});
