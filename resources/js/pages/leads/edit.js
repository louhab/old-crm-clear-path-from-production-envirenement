/*
 *  Document   : edit.js
 *  Author     : RR
 *  Description: Custom JS code used in lead edit page
 */

class leadEditPage {

    static initDOMEvents(dt) {

        $('#delete-cv-href').on('click', function(event) {
            event.preventDefault();
            $('#delete-form').submit();
        })

        $('.custom-file-input').on('change', function() {
            let fileName = $(this).val().split('\\').pop();
            $('#label-cv-input').addClass("selected").html(fileName);
        });
        var camp_ = $('select[name="campaign"] option:selected').val();
        if(camp_) {
            $.ajax({
                method: 'get',
                url: `${process.env.MIX_API_URL}/forms/${camp_}`,
                success: (data) => {
                    //
                    let keys = Object.keys(data);
                    if(keys.length > 0){
                        //console.log("keys: " + keys);
                        $('select[name="influencer"]').parent().show();
                        $('select[name="influencer"]').empty();
                        var influencers = '<option value="" label="Selectionner...">Selectionner...</option>';
                        let old = $('select[name="influencer"]').attr("value");
                        for (i = 0; i < keys.length; i++) {
                            let key = keys[i];
                            ////console.log("key: " + key + ", old: "+old);
                            if(old && old == key)
                                influencers += '<option value="' + key + '" selected="selected">' + data[key] + '</option>';
                            else
                                influencers += '<option value="' + key + '">' + data[key] + '</option>';
                        }
                        $('select[name="influencer"]').append(influencers);
                    } else {
                        $('select[name="influencer"]').parent().hide();
                        $('select[name="influencer"]').empty();
                    }
                },
                error: () => {
                    console.log("error!");
                }
            });
        }

        $('select[name="campaign"]').change(function(){
            let campaign = $(this).val();
            //console.log("Campaign: " + campaign);
            if(campaign){
                $.ajax({
                    method: 'get',
                    url: `${process.env.MIX_API_URL}/forms/${campaign}`,
                    success: (data) => {
                        //
                        let keys = Object.keys(data);
                        if(keys.length > 0){
                            //console.log("keys: " + keys);
                            $('select[name="influencer"]').parent().show();
                            $('select[name="influencer"]').empty();
                            var influencers = '<option value="" label="Selectionner...">Selectionner...</option>';
                            for (i = 0; i < keys.length; i++) {
                                let key = keys[i];
                                influencers += '<option value="' + key + '"">' + data[key] + '</option>';
                            }
                            $('select[name="influencer"]').append(influencers);
                        } else {
                            $('select[name="influencer"]').parent().hide();
                            $('select[name="influencer"]').empty();
                        }
                    },
                    error: () => {
                        console.log("error!");
                    }
                });
            } else {
                $('select[name="influencer"]').parent().hide();
                $('select[name="influencer"]').empty();
            }
        });
        
    }
        static createCase(LEAD_ID){
            $.ajax({
                method: 'post',
                url: `${process.env.MIX_API_URL}/leads/${LEAD_ID}`, //CHANGE FOR CUST
                success: (data) => {
                    Swal.fire(
                        'Création',
                        'Lead converti avec succés!',
                        'success'
                    ).then(() => {
                        $( "select[name='lead_status']" ).attr("data-customer", data.data.id);
                        //console.log(data.data);
                    });
                    //console.log(data.data.id);
                    //window.CASE_ID = data.data.id;
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {
                    }
                    else {
                        Swal.fire(
                            'Modification',
                            'erreur conversion lead!',
                            'error'
                        );
                    }
                    
                }
            })
            //return window.CASE_ID;
        }

    static deleteCustomer(CUSTOMER_ID) {

        //$('#leads-dt-block').addClass('block-mode-loading')

        $.ajax({
            method: 'delete',
            url: `${process.env.MIX_API_URL}/customer/${CUSTOMER_ID}`,
            success: () => {
                Swal.fire(
                    'Suppression!',
                    'Dossier supprimé avec succés!',
                    'success'
                ).then(() => {
                    //dt.api().ajax.reload(null, false);
                    $( "select[name='lead_status']" ).attr("data-customer", "");
                })
            },
            error: () => {
                Swal.fire(
                    'Suppression!',
                    'erreur suppression dossier!',
                    'error'
                ).then(() => {
                    //$('#leads-dt-block').removeClass('block-mode-loading');
                })
            }
        })
    }
    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents();
    }
}

// Initialize when page loads
jQuery(() => { leadEditPage.init(); });
