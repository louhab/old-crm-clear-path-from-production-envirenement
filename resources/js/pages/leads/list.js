/*
 *  Document   : list.js
 *  Author     : RR
 *  Description: Custom JS code used in leads list page
 */
window.CLEARPATH_LEAD_ID = null;
//window.fnCallback = false;

const unserialize = function (serializedString) {
    var str = decodeURI(serializedString);
    var pairs = str.split("&");
    var obj = {},
        p,
        idx,
        val;
    for (var i = 0, n = pairs.length; i < n; i++) {
        p = pairs[i].split("=");
        idx = p[0];

        if (idx.indexOf("[]") == idx.length - 2) {
            // Eh um vetor
            var ind = idx.substring(0, idx.length - 2);
            if (obj[ind] === undefined) {
                obj[ind] = [];
            }
            obj[ind].push(p[1]);
        } else {
            obj[idx] = p[1];
        }
    }
    return obj;
};

class leadsListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        const lang = $('input[name="lang"]').val();
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            //sType: "html",
            sLengthSelect: "form-control form-control-sm",
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>',
                },
            },
        });

        $("#add-histocall-customer-btn-bis").on("click", function () {
            // Add
            // request should be like this
            // add-form-lead_id=1&add-form-duration=4&add-form-call_descr=test&lead_status=1

            //hide previous validation errors
            histocallCustomerAddFormSelector.find(".invalid-feedback").hide();
            histocallCustomerAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            histocallCustomerAddBlockSelector.addClass("block-mode-loading");
            let seconds =
                parseInt($(".seconds").text().slice(0, 2)) +
                parseInt($(".minutes").text().slice(0, 2)) * 60 +
                parseInt($(".hours").text().slice(0, 2)) * 3600;
            $("#add-form-duration").val(seconds);

            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/histocalls`,
                data: histocallCustomerAddFormSelector.serialize(),
                success: (res) => {
                    var date = new Date(res.data.created_at);
                    let formatted_date =
                        date.getFullYear() +
                        "-" +
                        (date.getMonth() + 1) +
                        "-" +
                        date.getDate() +
                        " " +
                        date.getHours() +
                        ":" +
                        date.getMinutes() +
                        ":" +
                        date.getSeconds();
                    $(
                        '<tr class="calls">' +
                            '<td class="d-none d-sm-table-cell">' +
                            '<em class="font-size-sm text-muted">' +
                            formatted_date +
                            "</em>" +
                            "</td>" +
                            '<td class="font-size-sm">' +
                            '<p class="text-muted mb-0">' +
                            res.data.call_descr +
                            "</p>" +
                            "</td>" +
                            '<td class="d-none d-sm-table-cell">' +
                            '<span class="badge badge-danger">' +
                            res.data.duration +
                            " seconds</span>" +
                            "</td>" +
                            "</tr>"
                    ).insertAfter($(".calls").last());
                    //$('<tr class="calls"><td>Call '+(length+1)+':</td><td>'++'</td><td>'+res.data.duration+'</td></tr>').insertAfter($('.calls').last());
                    $("#modal-histocall-customer-add").modal("hide");
                    //resetChrono();
                    // if ($("#_lead_status").val() == "Rappel") {
                    //     window.location.href = "/calendar";
                    // }
                    location.reload();
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        histocallCustomerAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur ajout client!",
                            "error"
                        ).then(() => {
                            histocallCustomerAddBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        AjaxGet = function (customerId) {
            let result = $.ajax({
                method: "get",
                url: `${process.env.MIX_API_URL}/leadsdata/${customerId}`,
                async: false,
                success: function (data) {
                    document
                        .getElementById("add-form-call_city")
                        .setAttribute("value", data.city || "");
                    document.getElementById("customer-phone").innerHTML =
                        data.phone;
                    return data;
                },
                error: (xhr) => {
                    console.log("error");
                },
            }).responseText;
            return result;
        };

        AjaxLeadsStatusGet = function (customerId) {
            let result = $.ajax({
                method: "get",
                url: `${process.env.MIX_API_URL}/getCallStates/${customerId}`,
                async: false,
                success: function (data) {
                    return data;
                },
            }).responseText;
            return result;
        };

        AjaxCallCountStatusGet = function (customerId) {
            let result = $.ajax({
                method: "get",
                url: `${process.env.MIX_API_URL}/histocalls/lead/${customerId}`,
                async: false,
                success: function (data) {
                    return data;
                },
            }).responseText;
            return result;
        };

        $("body").on(
            "click",
            "button[id^='histocall-customer-table-add-']",
            function () {
                // Safty check
                const reactHistoCall = $("#rcw-conversation-container")
                    .attr("class")
                    .split(" ")[1];

                if (reactHistoCall === "active") {
                    return;
                }

                let customerId = $(this).data("id");
                window.ajaxdata = AjaxGet(customerId);

                $("<input>")
                    .attr({
                        type: "hidden",
                        name: "add-form-lead_id",
                        value: customerId,
                    })
                    .appendTo("#histocall-add-customer-form");
                // show modal
                histocallCustomerAddFormSelector
                    .find(".invalid-feedback")
                    .hide();
                histocallCustomerAddFormSelector
                    .find(".form-control")
                    .removeClass("is-invalid");

                $("#histocall-add-customer-form").trigger("reset");
                // $("#lead_state_call").select2("destroy");
                $("#lead_state_call").val(null).trigger("change");
                $("#lead_state_call").select2();
                $("#lead_state_call").select2({
                    placeholder:
                        lang === "fr" ? "Lead status ..." : "Lead State ...",
                    width: "100%",
                    allowClear: true,
                    ajax: {
                        url: `${process.env.MIX_API_URL}/getCallStates/${customerId}`,
                        dataType: "json",
                        delay: 250,
                        method: "GET",
                        data: function (term, page) {
                            return {
                                q: term,
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (v, k) {
                                    return {
                                        text: v,
                                        id: k,
                                    };
                                }),
                            };
                        },
                        cache: true,
                    },
                });
                $("#modal-histocall-customer-add").modal("show");

                // AJOUTER APPEL CLIENT
            }
        );

        $("#lead_state_call").on("select2:select", function (e) {
            // var data = e.params.data;
            //hide previous validation errors
            histocallCustomerAddFormSelector.find(".invalid-feedback").hide();
            histocallCustomerAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            histocallCustomerAddBlockSelector.addClass("block-mode-loading");
            let seconds =
                parseInt($(".seconds").text().slice(0, 2)) +
                parseInt($(".minutes").text().slice(0, 2)) * 60 +
                parseInt($(".hours").text().slice(0, 2)) * 3600;

            $("#add-form-duration").val(seconds);

            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/histocalls`,
                data: histocallCustomerAddFormSelector.serialize(),
                success: (res) => {
                    $("#modal-histocall-customer-add").modal("hide");

                    const userData = JSON.parse(ajaxdata);
                    // const callsCount = AjaxCallCountStatusGet(userData.id);
                    // Ajax modifier

                    // Get All leads status list
                    const leadStatus = AjaxLeadsStatusGet(userData.id);

                    const postData = unserialize(
                        histocallCustomerAddFormSelector.serialize()
                    );

                    // leads status
                    var badge = "danger";
                    switch (postData.lead_state_call) {
                        case "1":
                            badge = "success";
                            break;
                        case "3":
                            badge = "secondary";
                            break;
                        case "4":
                            badge = "info";
                            break;
                        case "5":
                            badge = "warning";
                            break;
                    }

                    if (
                        $("#lead_state_call").val() != "Rappel" &&
                        $("#lead_state_call").val() != "No_Chnage"
                    ) {
                        // parce this string
                        const bagdeEl = $(`#lead-badge-${userData.id}`);

                        bagdeEl.text(
                            bagdeEl
                                .text()
                                .replace(
                                    bagdeEl.text(),
                                    JSON.parse(leadStatus)[
                                        postData.lead_state_call
                                    ]
                                )
                        );
                        bagdeEl.removeClass(
                            bagdeEl.attr("class").split(" ")[1]
                        );
                        bagdeEl.addClass(`badge-${badge}`);
                    }

                    if ($("#lead_state_call").val() == "Rappel") {
                        window.location.href = "/calendar";
                    }
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        histocallCustomerAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            `erreur ajout appel! ${xhr.statusCode().status}`,
                            "error"
                        ).then(() => {
                            histocallCustomerAddBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        let columns = [
            {
                data: "lead_id",
                name: "lead_id",
                title: "<input type='checkbox' id='MyTableCheckAllButton'>",
                visible: window.userRole.role_id !== 1 ? false : true,
                className: "select-checkbox",
                mRender: function (data, type, row) {
                    return "";
                },
            }, // lead_id
            {
                data: "actions",
                className: "text-center",
                title: "action",
                width: "150px",
                mRender: function (data, type, row) {
                    let isContract = false;
                    let canCall = false;

                    if (
                        (window.userRole.role_id === 3 &&
                            (row.lead_status_id === 5 ||
                                row.lead_status_id === 11)) ||
                        (window.userRole.role_id === 7 &&
                            row.lead_status_id === 1)
                    ) {
                        data.forEach((el) => {
                            if (el.action === "assignment") isContract = true;
                        });
                    }
                    let newData = data.filter((el) => el && el?.available);
                    newData = newData.filter((el) =>
                        el.action === "assignment" ? !isContract : el
                    );
                    newData = newData.filter((el) =>
                        el.action === "call" ? !isContract : el
                    );

                    return `<div class="d-flex justify-content-center action">
                    ${newData.map((action) => {
                        if (action.available) {
                            if (action.islink) {
                                if (action.title === "afficher") {
                                    return `<div class="btn-group" style="margin:5px"><a class="${action.className}" href="${process.env.MIX_PUBLIC_URL}/lead/${row.lead_id}" title="${action.title}" class="${action.className}" data-id="${row.lead_id}"><i class="${action.icon}"></i></a></div>`;
                                } else {
                                    return `<div class="btn-group" style="margin:5px"><a class="${action.className}" href="${process.env.MIX_PUBLIC_URL}/leads/${row.lead_id}" title="${action.title}" class="${action.className}" data-id="${row.lead_id}"><i class="${action.icon}"></i></a></div>`;
                                }
                            } else {
                                // if not link
                                if (action.action === "call") {
                                    return `<div class="btn-group" style="margin:5px"><button title="${action.title}" class="${action.className}" id="histocall-customer-table-add-${row.lead_id}" data-id="${row.lead_id}"> <i class="${action.icon}"></i></button></div>`;
                                } else {
                                    return `<div class="btn-group" style="margin:5px"><a class="${action.className}" href="javascript:void(0)" title="${action.title}" class="${action.className}" data-id="${row.lead_id}"><i class="${action.icon}"></i></a></div>`;
                                }
                            }
                        } else {
                            return "";
                        }
                    })}</div>`;
                },
            },
            {
                data: "lead_status_id",
                name: "lead_status_id",
                title: "Status",
                mRender: function (data, type, row) {
                    let badge = "danger";
                    switch (row.lead_status_id) {
                        case 1:
                            // code block
                            badge = "success";
                            break;
                        case 3:
                            // code block
                            badge = "secondary";
                            break;
                        case 4:
                            // code block
                            badge = "info";
                            break;
                        case 5:
                            // code block
                            badge = "warning";
                            break;
                        // default:
                        //   // code block
                    }
                    return data
                        ? `<span id="lead-badge-${row.lead_id}" class="badge badge-${badge}">${row.status_label}</span>`
                        : `<span id="lead-badge-${row.lead_id}" class="badge badge-info">n/d</span>`;
                },
            },
            {
                data: "lastname",
                name: "lastname",
                title: lang === "fr" ? "Nom" : "Surname",
                className: "font-w600 font-size-sm",
            }, // lastname
            {
                data: "firstname",
                name: "firstname",
                title: lang === "fr" ? "Prénom" : "Name",
                className: "font-w600 font-size-sm",
            }, // firstname
            {
                data: "email",
                name: "email",
                title: "Email",
                mRender: function (data, type, row) {
                    if (data)
                        return `${
                            data?.match(/^([^@]*)@/)[1]
                        }<em class="text-muted">@${data.split("@")[1]}</em>`;
                    else return `<em class="text-muted">null</em>`;
                },
            }, // email
            {
                name: "city",
                title: "Ville",
                visible: [1, 7].includes(window.userRole.role_id),
                mRender: function (data, type, row) {
                    if (row?.country == "144" && !row?.city) {
                        return row?.city2;
                    } else {
                        return row?.city || `<em class="text-muted">null</em>`;
                    }
                },
            }, // Ville
            {
                name: "sub_step_state_id",
                title: "Rencontre",
                visible: [1, 2, 3, 4].includes(window.userRole.role_id),
                mRender: function (data, type, row) {
                    // console.log(row);
                    switch (row.sub_step_state_id) {
                        case 3:
                            return "R1";
                            break;
                        case 5:
                            return "R2";
                            break;
                        case 10:
                            return "R3";
                            break;
                        case 20:
                            return "R4";
                            break;
                        case 29:
                            return "R5";
                            break;
                        default:
                            return '<span class="badge badge-info">n/d</span>';
                    }
                },
            },
            {
                name: "meet_date",
                title: "Date Rencontre",
                visible: [1, 2, 3, 4].includes(window.userRole.role_id),
                mRender: function (data, type, row) {
                    if (row.sub_step_state_id && row.meet_date) {
                        const meet_date = new Date(row.meet_date);
                        return (
                            (meet_date.getDate() > 9
                                ? meet_date.getDate()
                                : "0" + meet_date.getDate()) +
                            "/" +
                            (meet_date.getMonth() > 8
                                ? meet_date.getMonth() + 1
                                : "0" + (meet_date.getMonth() + 1)) +
                            "/" +
                            meet_date.getFullYear()
                        );
                    }
                    return '<span class="badge badge-info">n/d</span>';
                },
            },
            {
                data: "program_id",
                name: "program_id",
                title: lang === "fr" ? "Programme" : "Program",
                mRender: function (data, type, row) {
                    //return `<span class="badge badge-${data == 1 ? 'info' : 'success'}">${row.pname}</span>`
                    return data
                        ? row.pname
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // program
            {
                data: "phone",
                name: "phone",
                title: "Phone",
                visible:
                    window.userRole.role_id == 3 ||
                    window.userRole.role_id == 4 ||
                    window.userRole.role_id == 7
                        ? false
                        : true,
                mRender: function (data, type, row) {
                    return data
                        ? data
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // phone
            {
                data: "canal",
                name: "canal",
                title: "Canal",
                visible:
                    window.userRole.role_id == 2 || window.userRole.role_id == 3
                        ? false
                        : true,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : "-";
                },
            }, // canal
            {
                data: "influencer",
                name: "influencer",
                title: "Source",
                visible:
                    window.userRole.role_id == 2 || window.userRole.role_id == 3
                        ? false
                        : true,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : "-";
                },
            }, // influencer
            {
                data: "reference",
                name: "reference",
                title: "Référence",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : "-";
                },
            }, // reference
            {
                data: "conseiller",
                name: "conseiller",
                title: "Conseiller",
                visible: window.userRole.role_id == 3 ? false : true,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // conseiller
            {
                data: "support",
                name: "support",
                title: "Support",
                visible:
                    window.userRole.role_id == 2 || window.userRole.role_id == 3
                        ? false
                        : true,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            },
            {
                data: "to_office",
                name: "to_office",
                title: lang === "fr" ? "Type R1" : "Type 1",
                mRender: function (data, type, row) {
                    //return `<span class="badge badge-${data == 1 ? 'info' : 'success'}">${row.pname}</span>`
                    return data
                        ? data
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // to_office // recycled
            /*{
                data: "recycled",
                name: "recycled",
                title: "Recyclage",
                visible: window.userRole.role_id > 2 ? false : true,
                mRender: function(data, type, row) {
                    //return `<span class="badge badge-${data == 1 ? 'info' : 'success'}">${row.pname}</span>`
                    return data
                        ? '<span class="badge badge-warning">Recyclée</span>'
                        : '<span class="badge badge-info">Non Recyclée</span>';
                }
            }, */ {
                data: "status_changed_at",
                name: "status_changed_at",
                title: "Status changer le",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    if (data) {
                        var date = new Date(data);
                        return (
                            (date.getDate() > 9
                                ? date.getDate()
                                : "0" + date.getDate()) +
                            "/" +
                            (date.getMonth() > 8
                                ? date.getMonth() + 1
                                : "0" + (date.getMonth() + 1)) +
                            "/" +
                            date.getFullYear()
                        );
                    }
                    return '<span class="badge badge-info">n/d</span>';
                },
            }, // status_changed_at
            {
                data: "created",
                name: "created",
                title: lang === "fr" ? "Crée le" : "Created at",
                mRender: function (data, type, row) {
                    if (data) {
                        var date = new Date(data);
                        return (
                            (date.getDate() > 9
                                ? date.getDate()
                                : "0" + date.getDate()) +
                            "/" +
                            (date.getMonth() > 8
                                ? date.getMonth() + 1
                                : "0" + (date.getMonth() + 1)) +
                            "/" +
                            date.getFullYear()
                        );
                    }
                    return '<span class="badge badge-info">n/d</span>';
                },
            }, // Date de Création
            {
                data: "processed",
                name: "processed",
                title: lang === "fr" ? "Traiter le" : "Treated at",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    if (data) {
                        var date = new Date(data);
                        return (
                            (date.getDate() > 9
                                ? date.getDate()
                                : "0" + date.getDate()) +
                            "/" +
                            (date.getMonth() > 8
                                ? date.getMonth() + 1
                                : "0" + (date.getMonth() + 1)) +
                            "/" +
                            date.getFullYear()
                        );
                    }
                    return '<span class="badge badge-info">n/d</span>';
                },
            }, // Date de Traitement
            {
                data: "processed_message",
                name: "processed_message",
                title:
                    lang === "fr" ? "Message de Traitement" : "Process Message",
                visible:
                    window.userRole.role_id == 1 ||
                    window.userRole.role_id == 10
                        ? true
                        : false,
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : "-";
                },
            },
            // canal
            {
                data: "calls",
                name: "calls",
                title: lang === "fr" ? "Total appels" : "Total calls",
                className: "font-w600 font-size-sm",
                mRender: function (data, type, row) {
                    return row.calls ? row.calls : "0";
                },
            }, // calls
        ];

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#leads-dt");
        let blockSelector = $("#leads-dt-block");
        let searchSelector = $("#block-leads-search");

        // Init leads DataTable
        return tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                searchSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.${lang}.l10n.json`,
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[0, "desc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/leads`,
                    data: function (d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function (index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    },
                },
                columns: columns,
                fnDrawCallback: function () {
                    blockSelector.removeClass("block-mode-loading");
                    searchSelector.removeClass("block-mode-loading");
                    /* Toolbar Table */
                    //console.log(window.userRole);
                    if (window.userRole.role_id == 1) {
                        $("div.toolbar").html(
                            '<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-cente mb-2" style="width: 30%;"><select class="form-control dt_action_field" name="agent[]" multiple="multiple"></select><button type="button" class="btn btn-alt-primary ml-2" id="dt_action_btn">Affecter</button></div>'
                        );
                    }
                    $(".dt_action_field").select2({
                        placeholder: "Utilisateurs...",
                        allowClear: true,
                        ajax: {
                            url: `${process.env.MIX_API_URL}/agents`,
                            dataType: "json",
                            delay: 250,
                            method: "POST",
                            data: function (term, page) {
                                return {
                                    q: term,
                                };
                            },
                            processResults: function (data) {
                                let res = [];
                                // console.log(data);
                                $.each(data, function (role, group) {
                                    if (role == "Agent") return true;
                                    let groupObj = {
                                        text: role,
                                    };
                                    let children = [];
                                    // console.log(role, group);
                                    $.each(group, function (idx, user) {
                                        // if(user.name)
                                        let child = {
                                            text: user.name,
                                            id: user.id,
                                            group: role,
                                        };
                                        children.push(child);
                                    });
                                    groupObj["children"] = children;
                                    res.push(groupObj);
                                });
                                //console.log(res);
                                return {
                                    results: res,
                                };
                            },
                            initSelection: function (element, callback) {},
                            cache: true,
                        },
                    });
                    $("#dt_action_btn").on("click", function () {
                        let conseillers = $(".dt_action_field").select2("val");
                        var $dataSelected = $("#leads-dt")
                            .DataTable()
                            .rows(".selected")
                            .data();
                        if (
                            conseillers.length > 0 &&
                            $dataSelected.length > 0
                        ) {
                            var resultConseilArray =
                                $(".dt_action_field").select2("val");

                            /*let i = 0;
                        var resultConseilArray = $dataSelected.reduce((resultArray, item, index) => {

                            if(i == conseillers.length) {
                                i = 0;
                            }

                            resultArray[item.lead_id] = conseillers[i];
                            i = i + 1;

                            return resultArray;
                        }, []);*/
                            //console.log(Object.keys(result));
                            var cs = 0;
                            // console.log();
                            Swal.fire({
                                title: "Etes vous sur?",
                                text: "",
                                icon: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#3085d6",
                                cancelButtonColor: "#d33",
                                confirmButtonText: "Oui, Affecter!",
                                cancelButtonText: "Annuler",
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    //let bs = $('#leads-dt-block');
                                    $("#leads-dt-block").addClass(
                                        "block-mode-loading"
                                    );
                                    var leads_ids = $("#leads-dt")
                                        .DataTable()
                                        .rows(".selected")
                                        .data()
                                        .pluck("lead_id")
                                        .toArray();
                                    $.ajax({
                                        method: "put",
                                        url: `${process.env.MIX_API_URL}/leadConseiller`,
                                        data: {
                                            leads_ids: leads_ids,
                                            resultConseilArray:
                                                resultConseilArray,
                                        },
                                        success: () => {
                                            Swal.fire(
                                                "Modification!",
                                                "Leads affecté",
                                                "success"
                                            ).then(() => {
                                                $("#leads-dt")
                                                    .DataTable()
                                                    .ajax.reload(null, false);
                                                $(
                                                    "#MyTableCheckAllButton"
                                                ).prop("checked", false);
                                            });
                                            //console.log(cs + "..." + $dataSelected.length);
                                            /*if (cs == $dataSelected.length){
                                            Swal.fire(
                                                'Modification!',
                                                cs + ' Leads afecté à ' + $(".dt_action_field").select2('data')[0].text,
                                                'success'
                                            ).then(() => {
                                                $('#leads-dt').DataTable().ajax.reload( null, false );
                                            });
                                        }*/
                                        },
                                        error: () => {
                                            $("#leads-dt-block").removeClass(
                                                "block-mode-loading"
                                            );
                                            //window.ce += 1;
                                            // Swal.fire(
                                            //     'Modification!',
                                            //     'erreur Modification lead!',
                                            //     'error'
                                            // );
                                        },
                                    });
                                    /*$dataSelected.each( function ( data, index ) {
                                    // console.log( cs + ' Data ID: '+data.lead_id+' Conseiller: '+resultConseilArray[data.lead_id] );
                                    $.ajax({
                                        method: 'put',
                                        url: `${process.env.MIX_API_URL}/leadConseiller/${data.lead_id}/${resultConseilArray[data.lead_id]}`,
                                        success: () => {
                                            cs += 1;
                                            //console.log(cs + "..." + $dataSelected.length);
                                            if (cs == $dataSelected.length){
                                                Swal.fire(
                                                    'Modification!',
                                                    cs + ' Leads afecté à ' + $(".dt_action_field").select2('data')[0].text,
                                                    'success'
                                                ).then(() => {
                                                    $('#leads-dt').DataTable().ajax.reload( null, false );
                                                });
                                            }
                                        },
                                        error: () => {
                                            $('#leads-dt-block').removeClass('block-mode-loading')
                                            //window.ce += 1;
                                            // Swal.fire(
                                            //     'Modification!',
                                            //     'erreur Modification lead!',
                                            //     'error'
                                            // );
                                        }
                                    });
                                });*/
                                }
                            });
                        } else if ($dataSelected.length == 0) {
                            Swal.fire(
                                "Modification!",
                                "Veuiller Selectinner un lead!",
                                "warning"
                            );
                        } else {
                            Swal.fire(
                                "Modification!",
                                "Veuiller Choisir un conseiller!",
                                "warning"
                            );
                        }
                    });
                    /* Toolbar Table */
                    // if ($('th.conseiller').length > 0){
                    //     $('#leads-dt th').each(function (index) {
                    //         if (index == 8) {
                    //             console.log($(this).text());
                    //             $(this).hide();
                    //             return false;
                    //         }
                    //     });
                    //     $('#leads-dt tbody tr').each(function () {
                    //         //console.log(.length);
                    //         $(this).find("> td").each(function(index){
                    //             if (index == 8) {
                    //                 //console.log($(this).text());
                    //                 $(this).hide();
                    //                 return false;
                    //             }
                    //         });
                    //     });
                    // }
                    $(".submit-search").prop("disabled", false);
                },
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                        orderable: true,
                        searchable: false,
                        className: "select-checkbox",
                    },
                    {
                        targets: 0,
                        orderable: false,
                    },
                ],
                select: {
                    style: "multi",
                    selector: "td:first-child",
                },
                lengthMenu: [
                    [10, 25, 50, 100, 500, -1],
                    [10, 25, 50, 100, 500, "All"],
                ],
                dom: '<"toolbar">lpfrtip',
            });
    }

    static initDOMEvents(dt) {
        // $('#leads-dt').on('click', 'td[class*="select-checkbox"]', function () {
        //     let $row = $(this).closest('tr');

        //     let data =  dt.DataTable().row($row).data();
        //     //console.log($(this).html());
        //     console.log(data);
        // });

        //$('.dt_action_field').;
        var request_pending = false;
        $("body").on("click", ".convert-lead-btn", function () {
            // console.log(request_pending);
            if (request_pending) {
                return;
            }
            const $row = $(this).closest("tr");

            const data = dt.DataTable().row($row).data();
            request_pending = true;
            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/leads/${data.lead_id}`, //CHANGE FOR CUST
                success: () => {
                    Swal.fire(
                        "Création",
                        "Lead converti avec succés!",
                        "success"
                    ).then(() => {
                        dt.api().ajax.reload();
                        $("#MyTableCheckAllButton").prop("checked", false);
                    });
                    request_pending = false;
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                    } else
                        Swal.fire(
                            "Modification",
                            "erreur conversion lead!",
                            "error"
                        );
                    request_pending = false;
                },
            });
        });

        $("#search-btn").on("click", function () {
            dt.api().ajax.reload();
            $("#MyTableCheckAllButton").prop("checked", false);
        });

        $("#clear-search-btn").on("click", function () {
            $("#search-form").trigger("reset");
            $("#lead").val(null).trigger("change");
            $("#phone").val(null);
            $("#country").val(null).trigger("change");
            dt.api().ajax.reload();
            $("#MyTableCheckAllButton").prop("checked", false);
        });

        // $('.dt_search_lead').select2({
        //     placeholder: 'Séléctionnez un lead',
        //     allowClear: true,
        //     ajax: {
        //         url: `${process.env.MIX_API_URL}/getLeads`,
        //         dataType: 'json',
        //         delay: 250,
        //         method: 'POST',
        //         data: function (term, page) {
        //             return {
        //                 q: term,
        //             }
        //         },
        //         processResults: function (data) {
        //             return {
        //                 results:  $.map(data, function (item) {
        //                     return {
        //                         text: item.name,
        //                         id: item.id
        //                     }
        //                 })
        //             }
        //         },
        //         initSelection: function(element, callback) {
        //         },
        //         cache: true
        //     }
        // })

        $(".dt_search_country").select2({
            placeholder: "Pays",
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/getCountries`,
                dataType: "json",
                delay: 250,
                method: "POST",
                data: function (term, page) {
                    return {
                        q: term,
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                            };
                        }),
                    };
                },
                cache: true,
            },
        });

        $("body").on("change", "#MyTableCheckAllButton", function () {
            var all = $("#leads-dt")
                .DataTable()
                .rows({ search: "applied" })
                .count(); // get total count of rows
            var selectedRows = $("#leads-dt")
                .DataTable()
                .rows({ selected: true, search: "applied" })
                .count(); // get total count of selected rows

            if (selectedRows < all) {
                $("#MyTableCheckAllButton").prop("checked", true);
                //Added search applied in case user wants the search items will be selected
                $("#leads-dt")
                    .DataTable()
                    .rows({ search: "applied" })
                    .deselect();
                $("#leads-dt").DataTable().rows({ search: "applied" }).select();
            } else {
                $("#leads-dt")
                    .DataTable()
                    .rows({ search: "applied" })
                    .deselect();
            }
        });

        $("body").on("change", '[name="leads-dt_length"]', function () {
            $("#MyTableCheckAllButton").prop("checked", false);
        });

        // CONVERT LEAD TO CLIENT
        // lead-convert
        $("body").on("click", ".lead-convert", function () {
            const $row = $(this).closest("tr");
            const data = $("#leads-dt").DataTable().row($row).data();
            //alert(data);
            window.CLEARPATH_LEAD_ID = data.lead_id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, convertir!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#leads-dt-block").addClass("block-mode-loading");

                    $.ajax({
                        method: "put",
                        url: `${process.env.MIX_API_URL}/leads/convert/${window.CLEARPATH_LEAD_ID}`,
                        success: () => {
                            Swal.fire(
                                "Modification!",
                                "Lead converti avec succés!",
                                "success"
                            ).then(() => {
                                dt.api().ajax.reload(null, false);
                                $("#MyTableCheckAllButton").prop(
                                    "checked",
                                    false
                                );
                            });
                        },
                        error: () => {
                            Swal.fire(
                                "Modification!",
                                "erreur modification lead!",
                                "error"
                            ).then(() => {
                                $("#leads-dt-block").removeClass(
                                    "block-mode-loading"
                                );
                            });
                        },
                    });
                }
            });
        });
        //DELETE Quote
        $("body").on("click", ".lead-delete", function () {
            const $row = $(this).closest("tr");
            const data = $("#leads-dt").DataTable().row($row).data();
            //alert(data);
            window.CLEARPATH_LEAD_ID = data.lead_id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, supprimer!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    //let bs = $('#leads-dt-block');
                    leadsListPage.deleteLead(dt);
                }
            });
        });

        $("body").on("click", ".lead-assignment", function () {
            const $row = $(this).closest("tr");
            const data = $("#leads-dt").DataTable().row($row).data();

            window.CLEARPATH_LEAD_ID = data.lead_id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "Re-affectez le lead!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, Affectez!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#leads-dt-block").addClass("block-mode-loading");
                    $.ajax({
                        method: "put",
                        url: `${process.env.MIX_API_URL}/leads/assign/${window.CLEARPATH_LEAD_ID}`,
                        success: () => {
                            Swal.fire(
                                "Modification!",
                                "Lead affecter avec succés!",
                                "success"
                            ).then(() => {
                                dt.api().ajax.reload(null, false);
                                $("#MyTableCheckAllButton").prop(
                                    "checked",
                                    false
                                );
                            });
                        },
                        error: () => {
                            Swal.fire(
                                "Modification!",
                                "erreur modification lead!",
                                "error"
                            ).then(() => {
                                $("#leads-dt-block").removeClass(
                                    "block-mode-loading"
                                );
                            });
                        },
                    });
                }
            });
        });

        $("body").on("click", ".lead-copy", function () {
            const $row = $(this).closest("tr");
            const data = $("#leads-dt").DataTable().row($row).data();

            window.CLEARPATH_LEAD_ID = data.lead_id;
            navigator.clipboard.writeText(
                `${process.env.MIX_PUBLIC_URL}/lead/${data.lead_id}?ref=true`
            );

            Swal.fire("URL copiée!", "", "success");
        });
    }

    static deleteLead(dt) {
        $("#leads-dt-block").addClass("block-mode-loading");

        $.ajax({
            method: "delete",
            url: `${process.env.MIX_API_URL}/leads/${window.CLEARPATH_LEAD_ID}`,
            success: () => {
                Swal.fire(
                    "Suppression!",
                    "Lead supprimé avec succés!",
                    "success"
                ).then(() => {
                    dt.api().ajax.reload(null, false);
                    $("#MyTableCheckAllButton").prop("checked", false);
                });
            },
            error: () => {
                Swal.fire(
                    "Suppression!",
                    "erreur suppression lead!",
                    "error"
                ).then(() => {
                    $("#leads-dt-block").removeClass("block-mode-loading");
                });
            },
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents(this.initDataTables());
    }
}

// Initialize when page loads
jQuery(() => {
    leadsListPage.init();
});
