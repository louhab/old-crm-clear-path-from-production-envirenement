/*
 *  Document   : list.js
 *  Author     : RR
 *  Description: Custom JS code used in leads list page
 */
window.CLEARPATH_LEAD_ID = null;
//window.fnCallback = false;

class leadsLogPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            //sType: "html",
            sLengthSelect: "form-control form-control-sm",
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>',
                },
            },
        });

        let columns = [
            {
                data: "lead_id",
                name: "lead_id",
                title: "<input type='checkbox' id='MyTableCheckAllButton'>",
                visible:
                    window.userRole.role_id > 2 && window.userRole.role_id != 10
                        ? false
                        : true,
                className: "select-checkbox",
                mRender: function (data, type, row) {
                    return "";
                },
            }, // lead_id
            {
                data: "lastname",
                name: "lastname",
                title: "Nom",
                className: "font-w600 font-size-sm",
            }, // lastname
            {
                data: "firstname",
                name: "firstname",
                title: "Prénom",
                className: "font-w600 font-size-sm",
            }, // firstname
            {
                data: "email",
                name: "email",
                title: "Email",
                mRender: function (data, type, row) {
                    return `${
                        data.match(/^([^@]*)@/)[1]
                    }<em class="text-muted">@${data.split("@")[1]}</em>`;
                },
            }, // email
            /*{"data": "program_id", "name": "program_id", title: 'Programme', mRender: function (data, type, row) {
                    //return `<span class="badge badge-${data == 1 ? 'info' : 'success'}">${row.pname}</span>`
                    return data ? row.pname : '<span class="badge badge-info">n/d</span>'
                }}, // program*/
            /*{"data": "phone", "name": "phone", title: 'Phone',"visible": (window.userRole.role_id == 3 || window.userRole.role_id == 4 || window.userRole.role_id == 7 ? false : true), mRender: function (data, type, row) {
                    return data ? data : '<span class="badge badge-info">n/d</span>'
                }}, // phone*/
            /*{"data": "canal", "name": "canal", title: 'Canal', mRender: function (data, type, row) {
                    return data ? `<span class="text-muted">${data}</span>` : '<span class="badge badge-info">n/d</span>'
                }}, // canal*/
            /*{"data": "conseiller", "name": "conseiller", title: 'Conseiller', "visible": (window.userRole.role_id == 3 ? false : true), mRender: function (data, type, row) {
                    return data ? `<span class="text-muted">${data}</span>` : '<span class="badge badge-info">n/d</span>'
                }}, // conseiller*/
            {
                data: "causer_name",
                name: "causer_name",
                title: "Causer",
                mRender: function (data, type, row) {
                    return data
                        ? `<span class="text-muted">${data}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            },
            {
                data: "created",
                name: "created",
                title: "Date de Création",
                mRender: function (data, type, row) {
                    if (data) {
                        var date = new Date(data);
                        return (
                            (date.getDate() > 9
                                ? date.getDate()
                                : "0" + date.getDate()) +
                            "/" +
                            (date.getMonth() > 8
                                ? date.getMonth() + 1
                                : "0" + (date.getMonth() + 1)) +
                            "/" +
                            date.getFullYear()
                        );
                    }
                    return '<span class="badge badge-info">n/d</span>';
                },
            }, // Date de Création
            /*{"data": "processed", "name": "processed", title: 'Date de Traitement', "visible": (window.userRole.role_id == 1 ? true : false), mRender: function (data, type, row) {
                    if(data) {
                        var date = new Date(data);
                        return ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + date.getFullYear();
                    }
                    return '<span class="badge badge-info">n/d</span>';
                }}, // Date de Traitement
            {"data": "processed_message", "name": "processed_message", title: 'Message de Traitement',"visible": (window.userRole.role_id == 1 ? true : false), mRender: function (data, type, row) {
                    return data ? `<span class="text-muted">${data}</span>` : '<span class="badge badge-info">n/d</span>'
                }},*/
            {
                data: "lead_status_id",
                name: "lead_status_id",
                title: "Status",
                mRender: function (data, type, row) {
                    let badge = "danger";
                    switch (row.lead_status_id) {
                        case 1:
                            // code block
                            badge = "success";
                            break;
                        case 3:
                            // code block
                            badge = "secondary";
                            break;
                        case 4:
                            // code block
                            badge = "info";
                            break;
                        case 5:
                            // code block
                            badge = "warning";
                            break;
                        // default:
                        //   // code block
                    }
                    return data
                        ? `<span class="badge badge-${badge}">${row.status_label}</span>`
                        : '<span class="badge badge-info">n/d</span>';
                },
            }, // canal
            {
                data: "actions",
                className: "text-center",
                title: "action",
                width: "150px",
                mRender: function (data, type, row) {
                    //if (action.islink) {}
                    return `<div class="d-flex justify-content-center action">
                                ${data.map((action) =>
                                    action.available
                                        ? `<div class="btn-group" style="margin:5px">
                                    <a class="${action.className}" href="${
                                              action.islink
                                                  ? `${
                                                        process.env
                                                            .MIX_PUBLIC_URL
                                                    }/${
                                                        action.title ==
                                                        "afficher"
                                                            ? "lead"
                                                            : "leads"
                                                    }/${row.lead_id}`
                                                  : "javascript:void(0)"
                                          }" title="${action.title}" class="${
                                              action.className
                                          }" data-id="${
                                              row.lead_id
                                          }"><i class="${action.icon}"></i></a>
                                </div>`
                                        : ""
                                )}
                            </div>`;
                },
            },
        ];

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#leads-dt");
        let blockSelector = $("#leads-dt-block");

        // Init leads DataTable
        return tableSelector
            .on("preXhr.dt", () => {
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`,
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[1, "desc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/logs`,
                    data: function (d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function (index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    },
                },
                columns: columns,
                fnDrawCallback: function () {
                    blockSelector.removeClass("block-mode-loading");
                    $(".submit-search").prop("disabled", false);
                },
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                        orderable: true,
                        searchable: false,
                        className: "select-checkbox",
                    },
                    {
                        targets: 0,
                        orderable: false,
                    },
                ],
                /*select: {
                style:    'multi',
                selector: 'td:first-child'
            },*/
                lengthMenu: [
                    [10, 25, 50, 100, 500, -1],
                    [10, 25, 50, 100, 500, "All"],
                ],
            });
    }

    static initDOMEvents(dt) {
        // $('#leads-dt').on('click', 'td[class*="select-checkbox"]', function () {
        //     let $row = $(this).closest('tr');

        //     let data =  dt.DataTable().row($row).data();
        //     //console.log($(this).html());
        //     console.log(data);
        // });

        //$('.dt_action_field').;
        var request_pending = false;
        $("body").on("click", ".convert-lead-btn", function () {
            console.log(request_pending);
            if (request_pending) {
                return;
            }
            const $row = $(this).closest("tr");

            const data = dt.DataTable().row($row).data();
            request_pending = true;
            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/leads/${data.lead_id}`, //CHANGE FOR CUST
                success: () => {
                    Swal.fire(
                        "Création",
                        "Lead converti avec succés!",
                        "success"
                    ).then(() => {
                        dt.api().ajax.reload();
                        $("#MyTableCheckAllButton").prop("checked", false);
                    });
                    request_pending = false;
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                    } else
                        Swal.fire(
                            "Modification",
                            "erreur conversion lead!",
                            "error"
                        );
                    request_pending = false;
                },
            });
        });

        $("#search-btn").on("click", function () {
            dt.api().ajax.reload();
            $("#MyTableCheckAllButton").prop("checked", false);
        });

        $("#clear-search-btn").on("click", function () {
            $("#search-form").trigger("reset");
            $("#lead").val(null).trigger("change");
            $("#phone").val(null);
            $("#country").val(null).trigger("change");
            dt.api().ajax.reload();
            $("#MyTableCheckAllButton").prop("checked", false);
        });

        // $('.dt_search_lead').select2({
        //     placeholder: 'Séléctionnez un lead',
        //     allowClear: true,
        //     ajax: {
        //         url: `${process.env.MIX_API_URL}/getLeads`,
        //         dataType: 'json',
        //         delay: 250,
        //         method: 'POST',
        //         data: function (term, page) {
        //             return {
        //                 q: term,
        //             }
        //         },
        //         processResults: function (data) {
        //             return {
        //                 results:  $.map(data, function (item) {
        //                     return {
        //                         text: item.name,
        //                         id: item.id
        //                     }
        //                 })
        //             }
        //         },
        //         initSelection: function(element, callback) {
        //         },
        //         cache: true
        //     }
        // })

        $(".dt_search_country").select2({
            placeholder: "Pays",
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/getCountries`,
                dataType: "json",
                delay: 250,
                method: "POST",
                data: function (term, page) {
                    return {
                        q: term,
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                            };
                        }),
                    };
                },
                cache: true,
            },
        });

        $("body").on("change", "#MyTableCheckAllButton", function () {
            var all = $("#leads-dt")
                .DataTable()
                .rows({ search: "applied" })
                .count(); // get total count of rows
            var selectedRows = $("#leads-dt")
                .DataTable()
                .rows({ selected: true, search: "applied" })
                .count(); // get total count of selected rows

            if (selectedRows < all) {
                $("#MyTableCheckAllButton").prop("checked", true);
                //Added search applied in case user wants the search items will be selected
                $("#leads-dt")
                    .DataTable()
                    .rows({ search: "applied" })
                    .deselect();
                $("#leads-dt").DataTable().rows({ search: "applied" }).select();
            } else {
                $("#leads-dt")
                    .DataTable()
                    .rows({ search: "applied" })
                    .deselect();
            }
        });

        $("body").on("change", '[name="leads-dt_length"]', function () {
            $("#MyTableCheckAllButton").prop("checked", false);
        });

        // CONVERT LEAD TO CLIENT
        // lead-convert
        $("body").on("click", ".lead-convert", function () {
            const $row = $(this).closest("tr");
            const data = $("#leads-dt").DataTable().row($row).data();
            //alert(data);
            window.CLEARPATH_LEAD_ID = data.lead_id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, convertir!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    console.log(window.CLEARPATH_LEAD_ID);
                    $("#leads-dt-block").addClass("block-mode-loading");

                    $.ajax({
                        method: "put",
                        url: `${process.env.MIX_API_URL}/leads/convert/${window.CLEARPATH_LEAD_ID}`,
                        success: () => {
                            Swal.fire(
                                "Modification!",
                                "Lead converti avec succés!",
                                "success"
                            ).then(() => {
                                dt.api().ajax.reload(null, false);
                                $("#MyTableCheckAllButton").prop(
                                    "checked",
                                    false
                                );
                            });
                        },
                        error: () => {
                            Swal.fire(
                                "Modification!",
                                "erreur modification lead!",
                                "error"
                            ).then(() => {
                                $("#leads-dt-block").removeClass(
                                    "block-mode-loading"
                                );
                            });
                        },
                    });
                }
            });
        });
        //DELETE Quote
        $("body").on("click", ".lead-delete", function () {
            const $row = $(this).closest("tr");
            const data = $("#leads-dt").DataTable().row($row).data();
            //alert(data);
            window.CLEARPATH_LEAD_ID = data.lead_id;

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, supprimer!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    //let bs = $('#leads-dt-block');
                    leadsListPage.deleteLead(dt);
                }
            });
        });
    }

    static deleteLead(dt) {
        $("#leads-dt-block").addClass("block-mode-loading");

        $.ajax({
            method: "delete",
            url: `${process.env.MIX_API_URL}/leads/${window.CLEARPATH_LEAD_ID}`,
            success: () => {
                Swal.fire(
                    "Suppression!",
                    "Lead supprimé avec succés!",
                    "success"
                ).then(() => {
                    dt.api().ajax.reload(null, false);
                    $("#MyTableCheckAllButton").prop("checked", false);
                });
            },
            error: () => {
                Swal.fire(
                    "Suppression!",
                    "erreur suppression lead!",
                    "error"
                ).then(() => {
                    $("#leads-dt-block").removeClass("block-mode-loading");
                });
            },
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents(this.initDataTables());
    }
}

// Initialize when page loads
jQuery(() => {
    leadsLogPage.init();
});
