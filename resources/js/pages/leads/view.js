/*
 *  Document   : view.js
 *  Author     : OL
 *  Description: Custom JS code used in lead view page
 */

class leadViewPage {
    static loadComments(lid = 0) {
        // @added late by ahaloua
        $("#delete-cv-href").on("click", function (event) {
            event.preventDefault();
            $("#delete-form").submit();
        });

        $(".custom-file-input").on("change", function () {
            let fileName = $(this).val().split("\\").pop();
            $("#label-paiement-input").addClass("selected").html(fileName);
        });

        $("#comments-block").addClass("block-mode-loading");
        $.ajax({
            method: "get",
            url: `${process.env.MIX_API_URL}/comments/${$(
                "#main-comment-container"
            ).attr("data-comment-id")}`,
            data: lid ? { lid: lid } : {},
            success: (data) => {
                let $commentWrapper = $("#comments-wrapper");
                let $loadCommentsBtn = $("#load-old-comments");
                let $loadCommentsBtnWrapper = $("#load-old-comments-wrapper");

                if (lid) $commentWrapper.append(data);
                else $commentWrapper.html(data);

                $loadCommentsBtn.attr(
                    "data-lid",
                    $commentWrapper.children().last().attr("data-lid")
                );

                if (!$.trim(data).length) $loadCommentsBtnWrapper.hide();
                else $loadCommentsBtnWrapper.show();
            },
            error: () => {
                throw "comments list error";
                // $("#comments-block").removeClass("block-mode-loading");
            },
        }).done(function () {
            $("#comments-block").removeClass("block-mode-loading");
        });
    }
    static initDOMEvents() {
        $(".delete-document").on("click", function (event) {
            event.preventDefault();
            $(`#${$(this).attr("data-formId")}`).submit();
        });
        $("#delete-cv-href").on("click", function (event) {
            event.preventDefault();
            $("#delete-form").submit();
        });
        $("#delete-cin-href").on("click", function (event) {
            event.preventDefault();
            $("#delete-cin-form").submit();
        });
        window.loadPreview = function (input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(id).attr("src", e.target.result).width(250).height(150);
                };

                reader.readAsDataURL(input.files[0]);
                $(id).parent().show();
            }
        };
        // $('#file-input-cin-recto').on('change',function(e){
        //     console.log("testing");
        //     var file = e.target.files[0];
        //     var reader = new FileReader();

        //     reader.onload =function(c) {

        //         $('#image-cin-recto').attr('src',c.target.result);
        //     }

        // });

        $(".sign-document, .requested-doc").on("click", function (event) {
            event.preventDefault();
            $(`#${$(this).attr("data-formId")}`).submit();
        });

        let histocallCustomerAddBlockSelector = $(
            "#block-histocall-customer-add"
        );
        let histocallCustomerAddFormSelector = $(
            "#histocall-add-customer-form"
        );

        $("#histocall-customer-table-add").on("click", function () {
            //hide previous validation errors
            histocallCustomerAddFormSelector.find(".invalid-feedback").hide();
            histocallCustomerAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            $("#histocall-add-customer-form").trigger("reset");
            $("#modal-histocall-customer-add").modal("show");
        });

        $("#_lead_status").on("change", function () {
            //hide previous validation errors
            histocallCustomerAddFormSelector.find(".invalid-feedback").hide();
            histocallCustomerAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            histocallCustomerAddBlockSelector.addClass("block-mode-loading");
            let seconds =
                parseInt($("#histocall-add-customer-form .seconds").text()) +
                parseInt($("#histocall-add-customer-form .minutes").text()) *
                    60 +
                parseInt($("#histocall-add-customer-form .hours").text()) *
                    3600;
            console.log(seconds);
            $("#add-form-duration").val(seconds);

            $.ajax({
                method: "post",
                url: `${process.env.MIX_API_URL}/histocalls`,
                data: histocallCustomerAddFormSelector.serialize(),
                success: (res) => {
                    //console.log($('.calls').last().html());
                    var date = new Date(res.data.created_at);
                    let formatted_date =
                        date.getFullYear() +
                        "-" +
                        (date.getMonth() + 1) +
                        "-" +
                        date.getDate() +
                        " " +
                        date.getHours() +
                        ":" +
                        date.getMinutes() +
                        ":" +
                        date.getSeconds();
                    //console.log(formatted_date)
                    $(
                        '<tr class="calls">' +
                            '<td class="d-none d-sm-table-cell">' +
                            '<em class="font-size-sm text-muted">' +
                            formatted_date +
                            "</em>" +
                            "</td>" +
                            '<td class="font-size-sm">' +
                            '<p class="text-muted mb-0">' +
                            res.data.call_descr +
                            "</p>" +
                            "</td>" +
                            '<td class="d-none d-sm-table-cell">' +
                            '<span class="badge badge-danger">' +
                            res.data.duration +
                            " seconds</span>" +
                            "</td>" +
                            "</tr>"
                    ).insertAfter($(".calls").last());
                    //$('<tr class="calls"><td>Call '+(length+1)+':</td><td>'++'</td><td>'+res.data.duration+'</td></tr>').insertAfter($('.calls').last());
                    $("#modal-histocall-customer-add").modal("hide");
                    //resetChrono();
                    console.log($("#_lead_status").val());
                    if ($("#_lead_status").val() == "Rappel") {
                        window.location.href = "/calendar";
                    }
                    location.reload();
                },
                error: (xhr) => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function (key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                    $(`#${key}`).attr("id") +
                                                    "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        histocallCustomerAddBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur ajout client!",
                            "error"
                        ).then(() => {
                            histocallCustomerAddBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                },
            });
        });

        $("#add-form-time").change(function () {
            var moment = require("moment"); // require
            let time_end = moment
                .utc($(this).val(), "hh:mm")
                .add(45, "minutes")
                .format("HH:mm");
            $("#add-form-time_end").val(time_end);
        });
        /*$('#add-calendar-btn').on('click', function () {
            $( "#calendar-add-form" ).submit();
        });*/

        $(".toggle-property").on("click", function (event) {
            event.preventDefault();
            let $form = $(`#${$(this).attr("data-formId")}`);
            let input = jQuery(
                `<input name="property" value="${$(this).attr(
                    "data-property"
                )}">`
            );
            $form.append(input);
            $form.submit();
        });

        //ADD FORM
        // let calendarAddBlockSelector = $('#block-calendar-add');
        let calendarAddFormSelector = $("#calendar-add-form");

        $("#calendar-table-add").on("click", function () {
            //hide previous validation errors
            calendarAddFormSelector.find(".invalid-feedback").hide();
            calendarAddFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            $("#calendar-add-form").trigger("reset");
            $("#modal-calendar-add").modal("show");
        });

        $("body").on("click", ".method-modal-btn", function () {
            $("#modal-payment-add").modal("show");
        });
        $("body").on("click", ".delete-payment", function () {
            //
            let case_state = $(this).data("case");

            $.ajax({
                method: "put",
                url: `${process.env.MIX_API_URL}/payments/${case_state}`,
                success: () => {
                    console.log("paiement deleted!");
                },
                error: () => {
                    console.log("paiement delete error!");
                },
            });
        });
        $("body").on("click", ".case-state-update", function () {
            // console.log("hmmm 222");
            $(".method-modal-btn")
                .closest(".block")
                .addClass("block-mode-loading");
            //console.log($(this).closest('.block').attr('class'));

            //let stateBtn = $(this);
            //let iconStatus = $(this).closest("tr").find("a").first();
            let customer_case = $(this).data("case");
            let method = $(this).data("method");
            let parameters = customer_case;
            // console.log(
            //     "huracan :: ",
            //     `${process.env.MIX_API_URL}/payments/${parameters}`
            // );
            // return;

            if (method) {
                parameters += "/" + method;
                $.get(
                    `${process.env.MIX_API_URL}/payments/${parameters}`,
                    function (data, status) {
                        console.log("Data: " + data + "\nStatus: " + status);
                    }
                );
            }

            //console.log('parameters: ' + parameters);
            //let step_state = $(this).data();
            // console.log(parameters);
            //console.log(iconStatus.html());
            // console.log(
            //     `${process.env.MIX_API_URL}/states/${parameters}`,
            //     "qwertz"
            // );
            // return;
            $.ajax({
                method: "put",
                url: `${process.env.MIX_API_URL}/states/${parameters}`,
                success: (data) => {
                    // if (typeof data.phone === "undefined") {
                    //     console.log(data.phone);
                    // }
                    console.log(data);
                    if (data.name == "Message whatsapp") {
                        if (typeof data.phone !== "undefined") {
                            let message;
                            //= 'Bonjour+'+data.firstname +"+"+ data.lastname+'%0D%0A%0D%0AJ%E2%80%99esp%C3%A8re+que+vous+allez+bien+%3F+%0D%0A%0D%0AJe+suis+'+data.conseiller+'+votre+conseiller+de+Clear+Path+Canada.%0D%0A%0D%0AJe+vous+remercie+pour+votre+int%C3%A9r%C3%AAt.+Pour+plus+de+renseignements%2C+nous+serons+ravis+de+vous+accueillir+dans+nos+locaux%2C+afin+de+vous+pr%C3%A9senter+notre+programme.%0D%0A%0D%0ACordialement%2C%0D%0A'+data.conseiller+'+%0D%0AConseiller+Clear+%0D%0APath+Canada+%0D%0ATel+%3A+%2B'+data.conseiller_phone+'%0D%0AAdresse+%3A+42%2C+rue+Bachir+Laalej%2C+Casablanca+-+Maroc%0D%0ALocalisation+%3A+https%3A%2F%2Fmaps.google.com%2F%3Fq%3D33.571827%2C-7.627207';
                            //let url = "https://wa.me/"+data.phone+"?text=Bonjour \n<br /> tt: "+ data.firstname +" "+ data.lastname+" ‏cv bien ?";
                            // console.log("testing");
                            // console.log(data);
                            let lang = document.getElementById("locale").value;
                            let detection_lang_user = document.getElementById(
                                "detection_lang_user"
                            ).value;

                            if (data.gender == "male") {
                                if (data.lead_lang === "en") {
                                    message =
                                        "Hello, " +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "the Clear Path Canada team thanks you for your interest. I am " +
                                        data.conseiller +
                                        ", and I will be your advisor. For more information, it will be a pleasure to do a videoconference or receive you at our office, where I will advise and help you to achieve your goals in the most effective way.";
                                } else {
                                    message =
                                        "Bonjour " +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "Toute l’équipe Clear Path Canada vous remercie de votre intérêt. Je suis " +
                                        data.conseiller +
                                        " votre conseiller, pour plus de renseignements je serais ravi de vous accueillir dans nos locaux, afin de vous accompagner pour réaliser votre rêve.";
                                }
                            }
                            if (data.gender == "female") {
                                if (data.lead_lang === "en") {
                                    message =
                                        "Hello, " +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "The Clear Path Canada team thanks you for your interest. I am " +
                                        data.conseiller +
                                        ", and I will be your advisor.  Since you filled our form, I will call you to advise you about how you can go to Canada.";
                                } else {
                                    message =
                                        "Bonjour " +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "Toute l’équipe Clear Path Canada vous remercie de votre intérêt. Je suis " +
                                        data.conseiller +
                                        " votre conseillère, pour plus de renseignements je serais ravie de vous accueillir dans nos locaux, afin de vous accompagner pour réaliser votre rêve.";
                                }
                            }
                            if (data.user_role == 7) {
                                if (data.lead_lang === "en") {
                                    message =
                                        "Hello, " +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "The Clear Path Canada team thanks you for your interest. I am " +
                                        data.conseiller +
                                        ", and I will be your advisor.  Since you filled our form, I will call you to advise you about how you can go to Canada.";
                                } else {
                                    message =
                                        "Bonjour " +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        ", Toute l’équipe Clear Path Canada vous remercie de votre intérêt. " +
                                        "Pour plus de renseignements, nous serons ravis de vous accueillir dans nos locaux, afin de vous accompagner pour réaliser votre rêve.";
                                }
                                /*message = "Bonjour, \n" +
                                    "Toute l’équipe de Clearpath Canada vous remercie de votre intérêt.\n" +
                                    "Pour plus de renseignements, nous serons ravis de vous accueillir dans\n" +
                                    "nos locaux, afin de vous orienter vers la démarche adéquate à votre profil.";*/
                            }
                            if (data.user_role == 4) {
                                if (data.lead_lang === "en") {
                                    message =
                                        "Hello, " +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "The Clear Path Canada team thanks you for your interest. I am " +
                                        data.conseiller +
                                        ", and I will be your advisor.  Since you filled our form, I will call you to advise you about how you can go to Canada.";
                                } else {
                                    message =
                                        "Bonjour " +
                                        data.firstname +
                                        " " +
                                        data.lastname +
                                        "," +
                                        `%0a` +
                                        "Toute l’équipe Clear Path Canada vous remercie de votre intérêt. Je suis " +
                                        data.conseiller +
                                        "." +
                                        `%0a` +
                                        "Pour plus de renseignements, je serais ravie de vous accueillir dans nos locaux, afin de rencontrer nos conseillers spécialisés qui vont vous orienter vers la démarche adéquate à votre profil\n";
                                }
                                //Si English
                                // message = "Hello " + data.firstname + " " + data.lastname + ",\n" +
                                //     "All Clear Path Canada team thanks you for your interest. I'm " + data.conseiller + ".\n" +
                                //     "For more information, it will be a pleasure to receive you at our office, in order to meet our specialized advisers who will direct and help you to achieve your goals in the most effective way.\n";
                            }
                            let phone = data.phone;
                            phone = phone.replace("-", "");
                            phone = phone.replace("+2120", "+212");

                            window.open(
                                "https://wa.me/" + phone + "?text=" + message,
                                "_blank"
                            );
                            //location.reload();
                            if (
                                $(this).parent().find("i.fa-check-circle")
                                    .length == 0
                            ) {
                                $(this)
                                    .parent()
                                    .prepend(
                                        '<div class="alert alert-success">\n' +
                                        '                        <i class="far fa-check-circle"></i>\n' +
                                        '                         Message envoyé\n' +
                                        '                    </div>'
                                    );
                            }
                            $("#lead-form").hide();
                            $("#lead-info").show();
                        } else {
                            Swal.fire(
                                "Whatsapp!",
                                "Numéro de telephone manquant!",
                                "error"
                            );
                        }
                    } else {
                        Swal.fire(
                            "Modification!",
                            "Status modifié avec succès!",
                            "success"
                        ).then(() => {
                            location.reload();
                        });
                    }
                    $(".method-modal-btn")
                        .closest(".block")
                        .removeClass("block-mode-loading");
                },
                error: () => {
                    Swal.fire(
                        "Modification!",
                        "erreur modification status!",
                        "error"
                    );
                },
            });
        });
        $("#refresh-comment-btn").on("click", (event) => {
            event.preventDefault();
            this.loadComments();
        });

        $("[name='country_id']").on("change", function () {
            if ($("[name='country_id']").val() == 144) {
                $("#city2").show();
                $("#city").hide();
            } else {
                $("#city2").hide();
                $("#city").show();
            }
        });

        // faire r2
        $("body").on("click", "#faire-r2", function () {
            const lead_id = $(this).attr("data-lead-id");
            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, Changer vers R2!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        method: "put",
                        url: `/api/v1/leads/switch/${lead_id}`,
                        success: (res) => {
                            Swal.fire(
                                "Changement!",
                                "Lead form changé avec succés!",
                                "success"
                            ).then(() => {
                                window.location.replace(
                                    `${process.env.MIX_PUBLIC_URL}/cases/${res?.id}`
                                );
                            });
                        },
                        error: () => {
                            Swal.fire(
                                "Changement!",
                                "erreur changement form!",
                                "error"
                            ).then(() => {});
                        },
                    });
                }
            });
        });

        $("body").on("click", "#load-old-comments", () => {
            this.loadComments($("#load-old-comments").attr("data-lid"));
        });
        $("body").on("click", "#toggle-lead-form", (event) => {
            const clickedBtn = $(event.target);
            $("#lead-form").toggle();
            $("#lead-info").toggle();
            if (clickedBtn.text() == "Editer") {
                clickedBtn.replaceWith(
                    '<button type="button" id="toggle-lead-form" class="btn btn-sm btn-outline-secondary">Annuler</button>'
                );
            } else {
                clickedBtn.replaceWith(
                    '<button type="button" id="toggle-lead-form" class="btn btn-sm btn-alt-primary">Editer</button>'
                );
                //clickedBtn.text('Editer');
            }
            //console.log(clickedBtn.html());
            //$(this).closest('.block').removeClass('block-mode-loading');
        });
        // payment methods
        $("#method-select").select2({}).empty();
        $("#method-select").select2({
            placeholder: "Espèce",
            ajax: {
                url: `${process.env.MIX_API_URL}/getPaymentMethods`,
                dataType: "json",
                delay: 250,
                method: "POST",
                data: function (term, page) {
                    return {
                        q: term,
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                            };
                        }),
                    };
                },
                cache: false,
            },
        });
        $("#method-select").on("select2:select", function (e) {
            // console.log("hmmm");
            // return;
            //console.log($(this).val());
            $(".case-state-update[data-method]").attr(
                "data-method",
                $(this).val()
            );
        });
        //Reférence
        $('select[name="campaign_id"]').change(function () {
            let campaign = $(this).val();
            if (campaign) {
                $.ajax({
                    method: "get",
                    url: `${process.env.MIX_API_URL}/forms/${campaign}`,
                    success: (data) => {
                        let arr = [];
                        for (const [id, value] of Object.entries(data)) {
                            arr.push({
                                id,
                                value,
                            });
                        }
                        arr = arr.sort((a, b) =>
                            String(a.value).toLowerCase() >
                            String(b.value).toLowerCase()
                                ? 1
                                : String(b.value).toLowerCase() >
                                  String(a.value).toLowerCase()
                                ? -1
                                : 0
                        );
                        if (arr.length > 0) {
                            $('select[name="form"]').empty();
                            var influencers =
                                '<option value="" label="Selectionner...">Selectionner...</option>';
                            for (const { id, value } of arr) {
                                influencers +=
                                    '<option value="' +
                                    id +
                                    '">' +
                                    value +
                                    "</option>";
                            }
                            $('#lead-form select[name="form"]').append(
                                influencers
                            );
                            $('#lead-form label[for="form"]').text(
                                $(
                                    '#lead-form select[name="campaign_id"] option:selected'
                                ).text()
                            );
                            // console.log($('#lead-form select[name="campaign"] option:selected').text());
                            $('#lead-form select[name="form"]')
                                .parent()
                                .parent()
                                .show();
                            $('#lead-form input[name="reference"]')
                                .parent()
                                .parent()
                                .hide();
                        } else {
                            $('#lead-form select[name="form"]')
                                .parent()
                                .parent()
                                .hide();
                            $('#lead-form input[name="reference"]')
                                .parent()
                                .parent()
                                .hide();
                            //$('select[name="influencer"]').empty();
                        }
                        if (campaign == 23) {
                            $('#lead-form select[name="form"]')
                                .parent()
                                .parent()
                                .hide();
                            $('#lead-form input[name="reference"]')
                                .parent()
                                .parent()
                                .show();
                        }
                    },
                    error: () => {
                        console.log("error!");
                    },
                });
            } else {
                $('select[name="form"]').parent().parent().hide();
                $('select[name="form"]').empty();
            }
        });
    }

    static init() {
        this.initDOMEvents();
        this.loadComments();
    }
}

// Initialize when page loads
jQuery(() => {
    leadViewPage.init();
});
