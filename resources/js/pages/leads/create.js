/*
 *  Document   : create.js
 *  Author     : RR
 *  Description: Custom JS code used in lead edit page
 */

class leadCreatePage {
    static initDOMEvents(dt) {
        $("#delete-cv-href").on("click", function (event) {
            event.preventDefault();
            $("#delete-form").submit();
        });

        $(".custom-file-input").on("change", function () {
            let fileName = $(this).val().split("\\").pop();
            $("#label-cv-input").addClass("selected").html(fileName);
        });

        let campaign_ = $('select[name="campaign"] option:selected').val();

        console.log("this is campainge default value", campaign_);
        if ($('select[name="influencer"]').hasClass("is-invalid"))
            console.log(1);
        else console.log(2);

        if (
            $('select[name="influencer"]').hasClass("is-invalid") ||
            campaign_
        ) {
            $.ajax({
                method: "get",
                url: `${process.env.MIX_API_URL}/forms/${campaign_}`,
                success: (data) => {
                    //
                    let keys = Object.keys(data);
                    if (keys.length > 0) {
                        $('select[name="influencer"]').empty();
                        //let selected = "--";
                        var influencers =
                            '<option value="" label="Selectionner...">Selectionner...</option>';
                        for (i = 0; i < keys.length; i++) {
                            let key = keys[i];
                            if (
                                key ==
                                $('select[name="influencer"]').data("old")
                            ) {
                                //selected = data[key];
                                influencers +=
                                    '<option value="' +
                                    key +
                                    '" selected>' +
                                    data[key] +
                                    "</option>";
                            } else {
                                influencers +=
                                    '<option value="' +
                                    key +
                                    '">' +
                                    data[key] +
                                    "</option>";
                            }
                        }
                        influencers +=
                            '<option value="other">Créer Nouveau Referent..</option>';
                        $('select[name="influencer"]').append(influencers);
                        $('label[for="influencer"]').html(
                            $('select[name="campaign"] option:selected').text()
                        );
                        $('select[name="influencer"]').val(
                            $('select[name="influencer"]').data("old")
                        );
                        $('select[name="influencer"]').parent().show();
                    } else {
                        $('select[name="influencer"]').parent().hide();
                    }
                },
            });
        }

        $('select[name="influencer"]').parent().hide();

        $('select[name="influencer"]').change(function () {
            if ($(this).val() == "other") {
                $('input[name="reference"]').parent().parent().show();
            } else {
                $('input[name="reference"]').parent().parent().hide();
            }
        });

        $('select[name="campaign"]').change(function () {
            let campaign = $(this).val();
            if (campaign) {
                $.ajax({
                    method: "get",
                    url: `${process.env.MIX_API_URL}/forms/${campaign}`,
                    success: (data) => {
                        let keys = Object.keys(data);
                        if (keys.length > 0) {
                            $('select[name="influencer"]').empty();
                            var influencers =
                                '<option value="" label="Selectionner...">Selectionner...</option>';
                            for (i = 0; i < keys.length; i++) {
                                let key = keys[i];
                                influencers +=
                                    '<option value="' +
                                    key +
                                    '">' +
                                    data[key] +
                                    "</option>";
                            }
                            influencers +=
                                '<option value="other">Créer Nouveau Referent..</option>';
                            $('select[name="influencer"]').append(influencers);
                            $('label[for="influencer"]').html(
                                $(
                                    'select[name="campaign"] option:selected'
                                ).text()
                            );
                            $('select[name="influencer"]').parent().show();
                        } else {
                            $('select[name="influencer"]').parent().hide();
                        }
                    },
                    error: () => {
                        console.log("error!");
                    },
                });
            } else {
                $('select[name="influencer"]').parent().hide();
                $('select[name="influencer"]').empty();
            }
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDOMEvents();
    }
}

// Initialize when page loads
jQuery(() => {
    leadCreatePage.init();
});
