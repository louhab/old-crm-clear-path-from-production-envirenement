/*
 *  Document   : finances.js
 *  Author     : RR
 *  Description: Custom JS code used in finances page
 */

class financesListPage {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {
        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput: "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm",
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>',
                },
            },
        });

        let searchFilterSelector = $(".search-filter");
        let tableSelector = $("#finances-dt");
        let blockSelector = $("#finances-dt-block");

        // Init finances DataTable
        let financesTable = tableSelector
            .on("preXhr.dt", () => {
                // console.log(
                //     "baladi  ",
                //     searchFilterSelector.find(".dt_search_field").serialize()
                // );

                // console.log(
                //     searchFilterSelector.find(".dt_search_field").serialize(),
                //     " relax"
                // );

                // reftach from here
                $.ajax({
                    method: "GET",
                    url: `${process.env.MIX_API_URL}/finances-stats`,
                    data: searchFilterSelector
                        .find(".dt_search_field")
                        .serialize(),
                    success: (data) => {
                        data.forEach((el, i) => {
                            $(`dt.finances-stats-total-${el.iso}`).html(
                                el.total_amount_payment
                            );
                        });
                    },
                    error: () => {},
                });
                // end refetch stats
                blockSelector.addClass("block-mode-loading");
                $(".submit-search").prop("disabled", true);
            })
            .dataTable({
                language: {
                    url: `${process.env.MIX_PUBLIC_URL}/json/jquery.dataTables.fr.l10n.json`,
                },
                searching: false,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[0, "desc"]],
                ajax: {
                    type: "GET",
                    url: `${process.env.MIX_API_URL}/finances`,
                    data: function (d) {
                        searchFilterSelector
                            .find(".dt_search_field")
                            .each(function (index, item) {
                                let itemInput = $(item);
                                if (itemInput.val() !== "") {
                                    d[itemInput.attr("name")] = itemInput.val();
                                }
                            });
                    },
                },
                columns: [
                    { data: "id", name: "id", visible: false }, // Id
                    {
                        data: "firstname",
                        name: "firstname",
                        render: function (data, type, row) {
                            return row.firstname + " " + row.lastname;
                        },
                        title: "Nom et Prénom",
                    }, // firstname
                    {
                        data: "lead_status_id",
                        name: "lead_status_id",
                        title: "Status",
                        mRender: function (data, type, row) {
                            let badge = "danger";
                            if (!row.is_client) {
                                switch (row.lead_status_id) {
                                    case 1:
                                        // code block
                                        badge = "success";
                                        break;
                                    case 3:
                                        // code block
                                        badge = "secondary";
                                        break;
                                    case 4:
                                        // code block
                                        badge = "info";
                                        break;
                                    case 5:
                                        // code block
                                        badge = "warning";
                                        break;
                                    // default:
                                    //   // code block
                                }
                                return row.lead_status_id
                                    ? `<span class="badge badge-${badge}">${row.status_label}</span>`
                                    : '<span class="badge badge-info">n/d</span>';
                            } else {
                                badge = "info";
                                return row.admission_label
                                    ? `<span class="badge badge-${badge}">${row.admission_label}</span>`
                                    : '<span class="badge badge-info">n/d</span>';
                            }
                        },
                    }, // lead state
                    {
                        data: "name_fr",
                        name: "name_fr",
                        title: "Paiement du programme",
                    },
                    {
                        data: "is_verified",
                        name: "is_verified",
                        title: "Verification status",
                        mRender: function (data, type, row) {
                            let badge = "danger";
                            let newData = data;

                            if (data === 1) {
                                newData = "Oui";
                                badge = "success";
                            } else {
                                newData = "Non";
                            }
                            return `<span class="badge badge-${badge}">${newData}</span>`;
                        },
                    },
                    {
                        data: "adviser",
                        name: "adviser",
                        title: "conseiller",
                        mRender: function (data, type, row) {
                            if (data) return data;
                            return '<span class="badge badge-info">n/d</span>';
                        },
                    },
                    {
                        data: "method_label_fr",
                        name: "method_label_fr",
                        title: "Methode de paiement",
                    },
                    { data: "amount", name: "amount", title: "Montant" },
                    { data: "devise", name: "devise", title: "Devise" },
                    {
                        data: "discount_percent",
                        name: "discount_percent",
                        title: "Réduction",
                    },
                    {
                        data: "performed_at",
                        name: "performed_at",
                        title: "Date de paiement",
                    },
                    {
                        title: "Action",
                        mRender: function (data, type, row) {
                            // if (row.is_verified ==)
                            return `
                            <a class="btn btn-primary" href="/lead/${
                                row.lead_id
                            }">
                                <i class="fas fa-eye"></i>
                            </a>
                            ${
                                !row.is_verified
                                    ? `<a class="btn btn-success payment-validation">
                                    <i class="fas fa-check"></i>
                                </a>`
                                    : ""
                            }
                            `;
                        },
                        className: "text-center",
                    },
                ],
                fnDrawCallback: function () {
                    blockSelector.removeClass("block-mode-loading");
                    $(".submit-search").prop("disabled", false);
                },
            });

        // payment options (deprecated)

        //DOM Events
        //TODO put dom events here

        $("body").on("click", ".payment-validation", function () {
            const $row = $(this).closest("tr");
            const data = $("#finances-dt").DataTable().row($row).data();

            window.PAYMENT_ID = data.id;
            // console.log("this is data", window.PAYMENT_ID);

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, Verifier!",
                cancelButtonText: "Annuler",
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#finances-dt-block").addClass("block-mode-loading");

                    $.ajax({
                        method: "put",
                        url: `${process.env.MIX_API_URL}/finances/${window.PAYMENT_ID}`,
                        success: () => {
                            Swal.fire(
                                "Modification!",
                                "Paiement Verfier avec succés!",
                                "success"
                            ).then(() => {
                                financesTable.api().ajax.reload(null, false);
                                $("#MyTableCheckAllButton").prop(
                                    "checked",
                                    false
                                );
                            });
                        },
                        error: () => {
                            Swal.fire(
                                "Modification!",
                                "erreur modification du paiement!",
                                "error"
                            ).then(() => {
                                $("#finances-dt-block").removeClass(
                                    "block-mode-loading"
                                );
                            });
                        },
                    });
                }
            });
        });

        //SEARCH FILTER
        $(".submit-search").on("click", function () {
            financesTable.api().ajax.reload();
            // Refetch stats
            // $.ajax({
            //     method: "GET",
            //     url: `${process.env.MIX_API_URL}/finances-stats`,
            //     data: searchFilterSelector.find(".dt_search_field").serialize(),
            //     success: (data) => {
            //         data.forEach((el, i) => {
            //             $(`dt.finances-stats-total-${el.iso}`).html(
            //                 el.total_amount_payment
            //             );
            //         });
            //     },
            //     error: () => {},
            // });
        });

        $(".clear-search").on("click", function () {
            searchFilterSelector.find(".dt_search_field").val("");
            financesTable.api().ajax.reload();
        });

        $(".dt_search_agent").on("select2:select", function (e) {
            console.log("jkdbsajkfbsadjfbjasd", e.params.data);
            var data = e.params.data;
            // console.log(e.params.data);
            $('.search-filter input[name="agent-group"]').val(
                e.params.data.group
            );
            financesTable.api().ajax.reload(null, false);
        });
        $(".dt_search_agent").on("select2:unselect", function (e) {
            //var data = e.params.data;
            //console.log(e.params.data);
            $('.search-filter input[name="agent-group"]').val("");
            financesTable.api().ajax.reload(null, false);
        });
        $(".dt_search_agent").select2({
            placeholder: "Utilisateurs...",
            allowClear: true,
            ajax: {
                url: `${process.env.MIX_API_URL}/agents`,
                dataType: "json",
                delay: 250,
                method: "POST",
                data: function (term, page) {
                    return {
                        q: term,
                    };
                },
                processResults: function (data) {
                    let res = [];
                    // console.log(window.dashboard_filter);
                    $.each(data, function (role, group) {
                        let groupObj = {
                            text: role,
                        };
                        let children = [];
                        // console.log(role, group);
                        $.each(group, function (idx, user) {
                            // if(user.name)
                            let child = {
                                text: user.name,
                                id: user.id,
                                group: role,
                            };
                            /*if ("agent" in window.dashboard_filter && window.dashboard_filter.agent == user.id ) {
                                // console.log(window.dashboard_filter);
                                child["selected"] = true;
                            }*/
                            children.push(child);
                        });
                        groupObj["children"] = children;
                        res.push(groupObj);
                    });
                    //console.log(res);
                    return {
                        results: res,
                    };
                },
                initSelection: function (element, callback) {},
                cache: true,
            },
        });
        // sum duration
        $(document).ready(function () {
            // $('#durations-sum').html(financesTable.api().column( 5 ).data().sum());
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => {
    financesListPage.init();
});
