<div wire:ignore.self class="modal fade bd-example-modal-lg" id="programModal">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Détail programme</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="program-form">
                    @if ($action == 'view')
                        @foreach ($columns as $value => $key)
                            {{-- @elseif(in_array($key, ['domaine_etude', 'test_langue','langue', 'ville', 'session', 'diplome', 'type_etablissement']))
                                @livewire('input.program-course', ['label' => $value, 'key' => $key], key($key))
                            @else --}}
                            <table class="table table-borderless table-vcenter">
                                <thead>
                                    <tr>
                                        <th>{{ $value }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="font-w600 font-size-sm">
                                            @if (array_key_exists($key, $program_modal))
                                                @if (empty($program_modal[$key]))
                                                    <a href="javascript:void(0)" class="table-infos"
                                                        data-key="{{ $key }}">--</a>
                                                @elseif($key == 'date_limit_admission')
                                                    <a href="javascript:void(0)" class="table-infos"
                                                        data-key="{{ $key }}">{{ $program_modal[$key]['day'] }}
                                                        / {{ $program_modal[$key]['month'] }}</a>
                                                @elseif(is_array($program_modal[$key]))
                                                    <a href="javascript:void(0)" class="table-infos"
                                                        data-key="{{ $key }}">{{ implode(' - ', $program_modal[$key]) }}</a>
                                                @else
                                                    <a href="javascript:void(0)" class="table-infos"
                                                        data-key="{{ $key }}">{{ $program_modal[$key] }}</a>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        @endforeach
                    @else
                        @foreach ($columns as $value => $key)
                            @if ($key == 'date_limit_admission')
                                <div class="form-group form-row">
                                    <label class="d-block">{{ $value }} : </label>
                                    <div class="col-12"></div>
                                    <div class="col-4">
                                        <select class="custom-select"
                                            wire:model.defer="program_modal.{{ $key }}.day"
                                            name="{{ $key }}-day">
                                            <option value>Day</option>
                                            @foreach (range(1, 31) as $day)
                                                <option value="{{ $day }}">{{ $day }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <select class="custom-select"
                                            wire:model.defer="program_modal.{{ $key }}.month"
                                            name="{{ $key }}-month">
                                            <option value>Month</option>
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                    </div>
                                    <div class="col-4"></div>
                                </div>
                            @elseif(in_array($key, $filter_keys))
                                {{-- @if ($key === 'exigence')
                                    @php dd($key) @endphp
                                @endif --}}
                                @livewire('input.program-course', ['label' => $value, 'key' => $key], key($key))
                            @else
                                <div class="form-group">
                                    <label for="{{ $key }}">{{ $value }}</label>
                                    <input type="text" class="form-control form-control-alt"
                                        id="{{ $key }}" name="{{ $key }}"
                                        wire:model.defer="program_modal.{{ $key }}"
                                        placeholder="{{ $value }}.."
                                        aria-describedby="{{ $key }}-error" aria-invalid="false">
                                    <div id="{{ $key }}-error" class="invalid-feedback"></div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                </form>
            </div>
            <div class="modal-footer">
                @if ($action == 'add')
                    <button type="button" class="btn btn-primary" id="btn-program-add">Ajouter programme</button>
                @endif
                @if ($action == 'update')
                    <button type="button" class="btn btn-primary" id="btn-program-update"
                        data-pid="{{ $program_modal['id'] }}">Modifier programme</button>
                @endif
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    {{-- @push('custom_styles')
        <style>
            /*li.select2-selection__choice {
                max-width: 100%;
                overflow: hidden;
                white-space: normal;
            }
            ul.select2-selection__rendered {
                padding-right: 12px !important;
            }*/
            li.select2-selection__choice {
                max-width: 100%;
                overflow: hidden;
                /*Altered two below to make word wrap work */
                word-wrap: normal !important;
                white-space: normal;
            }
            ul.select2-selection__rendered {
                padding-right: 12px !important;
            }
        </style>
    @endpush --}}
</div>
