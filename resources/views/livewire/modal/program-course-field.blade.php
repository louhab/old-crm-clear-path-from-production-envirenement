<div class="modal" id="modal-programCourseField-add" tabindex="-1" role="dialog" aria-labelledby="modal-programCourseField-add" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-rounded block-themed block-transparent mb-0" id="block-programCourseField-add">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">{{ !is_null($title) ? $title : "" }} </h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    <div class="row">
<!--                        <div class="col-md-6 pr-0">
                            <div class="block block-rounded block-bordered">
                                <div class="block-content">
                                    <form action="" method="POST" onsubmit="return false;" id="form-programCourseField-add">
                                        <div class="form-group">
                                            <label for="add-form-title">{{ !is_null($title) ? $title : "" }}</label>
                                            <span class="text-danger">*</span>
                                            <input type="text" class="form-control form-control-alt" id="add-form-title" wire:model.defer="value" name="add-form-title" aria-describedby="add-form-title-error" aria-invalid="false" placeholder="{{ !is_null($title) ? $title : "" }}..">
                                            <div id="add-form-title-error" class="invalid-feedback"></div>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-sm btn-alt-primary btn-block" wire:click="addValue"><i class="fa fa-plus mr-1"></i>Ajouter valeur</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>-->
                        <div class="col-md-12 overflow-auto" style="height: 200px;">
                            <div class="block">
                                <div class="block-content">
                                    <div class="list-group">
                                        @if(!is_null($field))
                                            @foreach($field["values"] as $val)
                                                <a href="javascript:void(0)" class="list-group-item list-group-item-action">
                                                    <div class="media-body" style="margin-right: 25%;">
                                                        <div class="font-size-sm">{{ $val }}</div>
                                                    </div>
                                                    <div class="btn-group-vertical" style="margin:5px;position:absolute;right: 20px;top: 5px;">
                                                        <button title="Supprimer valeur" class="btn btn-sm btn-light" wire:click="deleteValue('{{ addslashes($val) }}')">
                                                            <i class="fas fa-times text-danger mr-1"></i> Supprimer
                                                        </button>
                                                    </div>
                                                </a>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full text-right border-top">
                    <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
</div>
