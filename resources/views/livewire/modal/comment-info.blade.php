<div>
    @if(!is_null($canal) && !is_null($comment))
        <button class="list-group-item list-group-item-action d-flex justify-content-between program-item align-items-center" href="javascript:void(0)">
            <div class="media-body" style="margin-right: 25%;">
                <div class="font-w600">
                    {{ \App\Models\User::find($comment->user_id)->name }} - {{ $comment->comment }}
                </div>
                <div class="font-size-sm">Canal: {{ implode(" ", explode("-", $canal)) }}</div>
                <div class="font-size-sm">Client: {{ $customer_case->customer->firstname }} - {{ $customer_case->customer->lastname }}</div>
            </div>
        </button>
    @else
        No Comments!
    @endif
</div>
