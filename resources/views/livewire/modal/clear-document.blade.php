<div class="modal" id="modal-block-large" tabindex="-1" aria-labelledby="modal-block-large" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="block block-rounded block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">{{ $tag }} @if(!is_null($garant))<span class="badge badge-info">Garant {{ $garant + 1 }}</span>@endif</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    <div class="col-12">
                        <p>{{ $title }}</p>
                    </div>
                    <div class="col-12">
                        <form action="{{ $dropzoneAction }}" method="post" enctype="multipart/form-data" id="my-dropzone" class="dropzone">
                            @csrf
                            <input type="hidden" name="garant" value="{{ $garant }}">
                        </form>
                    </div>
                </div>
                <div class="block-content block-content-full text-right border-top">
                    <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    @push('custom_scripts')
        <script>
            $('.toggle-clear-modal').on('click', function (e) {
                let document = $(this).closest("tr").data('document');
                let garant = $(this).closest("table").data('garant');
                // console.log("test toggle", document);
                Livewire.emit('modalShown', document, garant);
                // $('#modal-block-large').modal('show');
            });
            Livewire.on('showClearModal', obj => {
                $('#modal-block-large').modal('toggle');
            });
        </script>
    @endpush
</div>
