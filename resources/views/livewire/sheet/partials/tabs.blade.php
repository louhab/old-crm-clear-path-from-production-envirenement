<?php
function safeCheck($to_look_for, $to_look_in)
{
    if (array_key_exists('key', $to_look_in[$to_look_for]) && array_key_exists('value', $to_look_in[$to_look_for])) {
        return $to_look_in[$to_look_for]['value'];
    } else {
        if (count($to_look_in[$to_look_for]['values'])) {
            return $to_look_in[$to_look_for]['values'][0];
        }
        return '';
    }
    return '';
}
?>


<div id="orientation" class="block block-rounded mb-0">
    <div class="block-header-default">
        <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link{{ $activeTab == 1 ? ' active' : '' }}" href="javascript:void(0)"
                    wire:click="toggleTab(1)">
                    Tous les programmes
                    <span class="nav-main-link-badge badge badge-pill badge-primary">{{ count($programs) }}</span>
                </a>
            </li>
            @if (!is_null($customer_case))
                <li class="nav-item">
                    <a class="nav-link{{ $activeTab == 2 ? ' active' : '' }}" href="javascript:void(0)"
                        wire:click="toggleTab(2)">
                        Programmes séléctionner
                        <span class="nav-main-link-badge badge badge-pill badge-primary">{{ count($choices) }}</span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
    <div class="block-content tab-content overflow-auto" style="height: 645px;margin-bottom: 50px;">
        <div class="tab-pane fade fade-left{{ $activeTab == 1 ? ' active show' : '' }}" id="btabs-programs"
            role="tabpanel">
            <div class="list-group push">
                @foreach ($pagination_programs as $pagination_program)
                    @php

                        // $program = \App\Models\OrientationSchool::find($pid)->toArray();
                        // $program = \App\Models\ProgramCourse::find($pid)->toArray();
                        $pid = $pagination_program->id;
                        $program = $pagination_program->toArray();
                        $is_closed = null;
                        if (isset($program['properties']['date_limit_admission']) && $program['properties']['date_limit_admission']['value'] != '') {
                            // if (isset($program["properties"]["date_limit_admission"]["value"]["day"]))
                            $date = $program['properties']['date_limit_admission']['value'];
                            if (isset($date['day']) && intval($date['day']) > 0 && isset($date['month']) && intval($date['month']) > 0) {
                                if (intval(date('m')) == $date['month']) {
                                    if ($date['day'] < intval(date('d'))) {
                                        $is_closed = true;
                                    } else {
                                        $is_closed = false;
                                    }
                                    // dd($program["properties"]["date_limit_admission"],date("m"), date("d"));
                                } elseif ($date['month'] < intval(date('m'))) {
                                    $is_closed = true;
                                } elseif ($date['month'] > intval(date('m'))) {
                                    $is_closed = false;
                                }
                            }
                        }
                        foreach (['diplome', 'type_etablissement', 'langue', 'ville'] as $kk) {
                            $program['properties'][$kk]['value'] = array_key_exists('value', $program['properties'][$kk]) ? $program['properties'][$kk]['value'] : '--';
                            if (array_key_exists('values', $program['properties'][$kk]) && is_array($program['properties'][$kk]['values']) && count($program['properties'][$kk]['values'])) {
                                $program['properties'][$kk]['value'] = '(' . implode('-', $program['properties'][$kk]['values']) . ')';
                            }
                        }
                    @endphp
                    <a class="list-group-item list-group-item-action d-flex justify-content-between program-item align-items-center{{ in_array($pid, $choices) ? ' active' : '' }}"
                        href="javascript:void(0)" id="{{ $pid }}" style="min-height: 137px;">
                        <div class="media-body" style="margin-right: 40%;">
                            <div class="font-w600">
                                @if (!is_null($is_closed))
                                    <span
                                        class="badge badge-{{ $is_closed ? 'warning' : 'success' }}">{{ $is_closed ? 'Fermé' : 'Ouvert' }}</span>
                                @endif
                                <small>{{ $program['properties']['diplome']['value'] ?? 'Diplôme' }} -
                                    {{ $program['name'] }}</small>
                            </div>
                            <div class="font-size-sm">{{ safeCheck('type_etablissement', $program['properties']) }} -
                                {{ safeCheck('nom_etablissement', $program['properties']) }}
                            </div>
                            <div class="font-size-sm">
                                {{ safeCheck('langue', $program['properties']) }} -
                                {{ safeCheck('ville', $program['properties']) }}</div>
                            <div class="font-size-sm"><strong>
                                    ${{ safeCheck('prix_total', $program['properties']) }} CAD -
                                    {{ safeCheck('frais_admission', $program['properties']) }}CAD</strong></div>
                        </div>
                        <div class="btn-group-vertical" id="btn-group-{{ $pid }}"
                            style="margin:5px;position:absolute;right: 20px;top: 5px;">
                            <button title="Details programme" class="btn btn-sm btn-light program-details-btn"
                                data-action="view">
                                <i class="fas fa-eye"></i> Détail programme
                            </button>
                            <button title="Voire programme" class="btn btn-sm btn-light"
                                onclick="window.open('{{ route('orientation-view', ['program' => $pid]) }}', '_blank').focus();">
                                <i class="fas fa-external-link-alt pr-2"></i> Voire programme
                            </button>
                            @admin
                                <button title="Voir programme" class="btn btn-sm btn-light program-details-btn"
                                    data-action="update">
                                    <i class="fas fa-edit text-info"></i> Modifier programme
                                </button>
                                <button title="Voir programme" class="btn btn-sm btn-light delete-program"
                                    data-pid="{{ $pid }}">
                                    <i class="fas fa-times text-danger"></i> Supprimer programme
                                </button>
                            @endadmin
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
        @if (!is_null($customer_case))
            <div class="tab-pane fade fade-left{{ $activeTab == 2 ? ' active show' : '' }}" id="btabs-programs"
                role="tabpanel">
                <div class="list-group push">
                    @foreach ($choices as $pid)
                        @php
                            // $program = \App\Models\OrientationSchool::find($pid)->toArray();
                            $program = \App\Models\ProgramCourse::find($pid)->toArray();
                            foreach (['diplome', 'type_etablissement', 'langue', 'ville'] as $kk) {
                                $program['properties'][$kk]['value'] = array_key_exists('value', $program['properties'][$kk]) ? $program['properties'][$kk]['value'] : '--';
                                if (array_key_exists('values', $program['properties'][$kk]) && is_array($program['properties'][$kk]['values']) && count($program['properties'][$kk]['values'])) {
                                    $program['properties'][$kk]['value'] = '(' . implode('-', $program['properties'][$kk]['values']) . ')';
                                }
                            }
                        @endphp
                        <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
                            href="javascript:void(0)">
                            <div class="media-body" style="margin-right: 25%;">
                                <div class="font-w600">{{ $program['properties']['diplome']['value'] ?? 'Diplôme' }} -
                                    {{ $program['name'] }}</div>

                                <div class="font-size-sm">
                                    {{ safeCheck('nom_etablissement', $program['properties']) }}
                                </div>
                                <div class="font-size-sm">{{ safeCheck('langue', $program['properties']) }} -
                                    {{ safeCheck('ville', $program['properties']) }}</div>
                                <div class="font-size-sm"><strong>
                                        ${{ safeCheck('prix_total', $program['properties']) }} CAD -
                                        {{ safeCheck('frais_admission', $program['properties']) }} CAD</strong></div>
                            </div>
                            <div class="btn-group-vertical" id="btn-group-selection-{{ $pid }}"
                                style="margin:5px;position:absolute;right: 20px;top: 5px;">
                                <button title="Déselectionner programme" class="btn btn-sm btn-warning"
                                    wire:click="selectProgram({{ $pid }})">
                                    <i class="fas fa-times-circle"></i>
                                </button>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
    <div class="content-full" style="margin: 20px;">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <nav class="flex-sm-00-auto" aria-label="breadcrumb">
                @if (!is_null($customer_case))
                    <button type="button" class="btn btn-alt-primary btn-lg" id="clear-btn"
                        wire:click="savePDF()">Sauvegarder</button>
                @endif
                <div class="row d-flex justify-content-center">
                    <div class="col-10">
                        {{ $pagination_programs->links('pagination::bootstrap-4') }}
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <!-- Modal -->
    @livewire('modal.program-course', ['columns' => $columns])
    @livewire('modal.program-course-field')
    @push('custom_scripts')
        <script>
            $(document).on('show.bs.modal', '.modal', function() {
                const zIndex = 1040 + 10 * $('.modal:visible').length;
                $(this).css('z-index', zIndex);
                setTimeout(() => $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass(
                    'modal-stack'));
                if ($(this).attr("id") == "programModal") {
                    // $('.tags').select2();
                }
            });
            // js-example-basic-multiple
            /*$(document).on('hide.bs.modal', '.modal', function() {
                if ($(this).attr("id") == "modal-programCourseField-add") {
                    // console.log($(this).attr("id"));
                    $('#programModal').modal('show');
                    // $('#programModal select.js-example-basic-multiple').select2();
                }
            });*/
            $('body').on('click', '.delete-program-course-field', function() {
                let field = $(this).data("field");
                let title = $(this).data("title");
                // $('#programModal').modal('hide');
                Livewire.emit("programCourseField", field, title);
            });
            Livewire.on('showProgramCourseField', obj => {
                $('#modal-programCourseField-add').modal('show');
            });
            Livewire.on('closeProgramCourseField', obj => {
                // console.log(obj);
                if (obj[0] == "valueDeleted") {
                    $("#tags_" + obj[1] + " option[value='" + obj[2] + "']").remove();
                }
                $('#modal-programCourseField-add').modal('hide');
                // $('#programModal select.js-example-basic-multiple').select2();
            });
            $('body').on('click', '#clear-btn', function() {
                $('#orientation').addClass('block-mode-loading');
            });
            // show modal
            Livewire.on('programDetails', obj => {
                // console.log("test show modal");
                $('#programModal').modal('show');
            });
            Livewire.on('closeModal', obj => {
                $('#programModal').modal('hide');
            });
            // click outside details, closeModal
            /*var ignoreClickOnMeElement = document.getElementById('program-btns');

            document.addEventListener('click', function(event) {
                var isClickInsideElement = ignoreClickOnMeElement.contains(event.target);
                if (!isClickInsideElement) {
                    //Do something click is outside specified element
                }
            });*/
            // $('#programModal select.js-example-basic-multiple').select2();
            /*$('#programModal select').each(function () {
                /!* Get options *!/
                var selectList = $(this).find('option');
                /!* Order by innerText (case insensitive) *!/
                selectList.sort(
                    (a, b) => a.innerText.localeCompare(b.innerText)
                );
                /!* Re-do select HTML *!/
                $(this).html(selectList);
                $(this).select2();
            });*/
            // wire:click="selectProgram('')"
            $('body').on('click', '.program-item', function(event) {
                var ignoreClickOnMeElement = document.getElementById('btn-group-' + $(this).attr('id'));
                var isClickInsideElement = ignoreClickOnMeElement.contains(event.target);
                if (!isClickInsideElement) {
                    // console.log($(this).attr('id'));
                    Livewire.emit('programSelected', $(this).attr('id'));
                }
            });
            $('body').on('click', '.delete-program', function() {
                let pid = $(this).data("pid");
                Swal.fire({
                    title: 'Etes vous sur?',
                    text: "L'opération est irréversible!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Oui, supprimer!',
                    cancelButtonText: 'Annuler'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emit("confirmDelete", pid);
                    }
                })
            });
            // tags_budget
        </script>
        <script>
            /*$('.tags').select2({
                                                                                                                                                                                                tags: true,
                                                                                                                                                                                                tokenSeparators: [','],
                                                                                                                                                                                                allowClear: true,
                                                                                                                                                                                                placeholder: "Add your tags here",
                                                                                                                                                                                                /!* the next 2 lines make sure the user can click away after typing and not lose the new tag *!/
                                                                                                                                                                                                selectOnClose: false,
                                                                                                                                                                                                closeOnSelect: true,
                                                                                                                                                                                                createTag: function (params) {
                                                                                                                                                                                                    var term = $.trim(params.term);

                                                                                                                                                                                                    if (term === '') {
                                                                                                                                                                                                        return null;
                                                                                                                                                                                                    }

                                                                                                                                                                                                    return {
                                                                                                                                                                                                        id: term,
                                                                                                                                                                                                        text: term,
                                                                                                                                                                                                        newTag: true // add additional parameters
                                                                                                                                                                                                    }
                                                                                                                                                                                                }
                                                                                                                                                                                                /!*createTag: function (params) {
                                                                                                                                                                                                    // console.log(params.term);
                                                                                                                                                                                                    return {
                                                                                                                                                                                                        id: params.term,
                                                                                                                                                                                                        text: params.term
                                                                                                                                                                                                    }
                                                                                                                                                                                                },
                                                                                                                                                                                                insertTag: function (data, tag) {
                                                                                                                                                                                                    // Insert the tag at the end of the results
                                                                                                                                                                                                    console.log(tag);
                                                                                                                                                                                                    data.push(tag);
                                                                                                                                                                                                }*!/
                                                                                                                                                                                            }).on("select2:select", function(e) {
                                                                                                                                                                                                // console.log(e.target.id);
                                                                                                                                                                                                // Livewire.emit('tagAdded', e.target.id.replace("tags_", ""), e.params.data.text);
                                                                                                                                                                                                let field_name = e.target.id.replace("tags_", "");
                                                                                                                                                                                                $.ajax({
                                                                                                                                                                                                    method: 'post',
                                                                                                                                                                                                    url: `/api/v1/program-course/${field_name}`,
                                                                                                                                                                                                    data: {values: $(this).val()},
                                                                                                                                                                                                    success: () => {
                                                                                                                                                                                                        console.log("value added");
                                                                                                                                                                                                    },
                                                                                                                                                                                                    error: (xhr) => {
                                                                                                                                                                                                        Swal.fire(
                                                                                                                                                                                                            'Ajout!',
                                                                                                                                                                                                            'Erreur ajout valeurs!',
                                                                                                                                                                                                            'error'
                                                                                                                                                                                                        );
                                                                                                                                                                                                    }
                                                                                                                                                                                                });
                                                                                                                                                                                            }).on("select2:unselect", function(e) {
                                                                                                                                                                                                var data = e.params.data;
                                                                                                                                                                                                let field_name = e.target.id.replace("tags_", "");

                                                                                                                                                                                                if ('newTag' in data) {
                                                                                                                                                                                                    console.log(data);
                                                                                                                                                                                                    var dataOp = {
                                                                                                                                                                                                        id: data.id,
                                                                                                                                                                                                        text: data.text
                                                                                                                                                                                                    };

                                                                                                                                                                                                    var newOption = new Option(dataOp.text, dataOp.id, false, false);
                                                                                                                                                                                                    $("#tags_" + field_name).append(newOption).trigger('change');
                                                                                                                                                                                                }
                                                                                                                                                                                            });*/
        </script>
        <script>
            let userAddBlockSelector = $('#programModal .modal-dialog');
            let userAddFormSelector = $('#program-form');
            // add Program
            $(document).on('click', '#btn-program-add', function() {
                // $('#btn-program-add').click(function (e) {
                $.ajax({
                    method: "post",
                    url: `/api/v1/program-course`,
                    data: $("#program-form").serialize(),
                    success: (res) => {
                        // console.log(res);
                        $('#programModal').modal('hide');
                        document.getElementById("program-form").reset();
                        $('.tags').val(null).trigger('change');
                        // $("#modal-histocall-customer-add").modal("hide");
                    },
                    error: (xhr) => {
                        if (xhr.statusCode().status === 422) {

                            $.each(xhr.responseJSON.errors, function(key, value) {
                                $(`#${key}`).addClass('is-invalid');
                                $(`#${key}-error`).html(value.toString().replace(key, $(
                                        "label[for='" + $(`#${key}`).attr('id') + "']")
                                    .html())).show();
                            });
                            userAddBlockSelector.removeClass('block-mode-loading');
                        } else
                            Swal.fire(
                                'Ajout!',
                                'erreur ajout program!',
                                'error'
                            ).then(() => {
                                userAddBlockSelector.removeClass('block-mode-loading');
                            })
                    },
                });
            });
            // update program
            $(document).on('click', '#btn-program-update', function() {
                // console.log("clicking here")
                let program = $(this).data("pid");
                $.ajax({
                    method: "put",
                    url: `/api/v1/program-course/${program}`,
                    data: $("#program-form").serialize(),
                    success: (res) => {
                        // console.log(res);
                        $('#programModal').modal('hide');
                        document.getElementById("program-form").reset();
                        $('.tags').val(null).trigger('change');
                        // $("#modal-histocall-customer-add").modal("hide");
                    },
                    error: (xhr) => {
                        if (xhr.statusCode().status === 422) {
                            Swal.fire(
                                'Modif!',
                                'erreur : invalid given link or program',
                                'error'
                            )
                        } else Swal.fire(
                            'Modif!',
                            'erreur modif program!',
                            'error'
                        )
                    },
                });
            });
        </script>
        <script>
            $('body').on('click', '.program-details-btn', function() {
                let action = $(this).data("action");
                let program = $(this).closest('a.list-group-item').attr("id");
                if (action == "view" || action == "update") {
                    Livewire.emit("loadProgramModal", program);
                }
                Livewire.emit('toggleModalAction', action);
                $('#programModal').modal('show');
                /*$.ajax({
                    method: "get",
                    url: `/api/v1/program-course/${program}`,
                    success: (data) => {
                        // console.log(data);
                        document.getElementById("program-form").reset();
                        $('.tags').val(null).trigger('change');
                        $('[name="programme"]').val(data.name);
                        $.each(data.properties, function( index, data_field ) {
                            // console.log(data_field);
                            // console.log($('[name="' + index + '[]"]').length);
                            // let dom = $('input[name="' + index + '"]');
                            $('[name="' + index + '"]').val(data_field.value);
                            /!*if (action == "view") {
                                // $('[name="' + index + '"]').attr("placeholder", data_field.value);
                                // $('input[name="' + index + '"]').prop("disabled", true);
                                // $('#programModal [name="' + index + '"]').attr('disabled','disabled');
                                $('.table-infos').each(function () {
                                    if ($(this).data("key") == index) {
                                        console.log(data_field.value);
                                        $(this).text(data_field.value);
                                    }
                                });
                            }*!/
                            /!*else {
                                // $('input[name="' + index + '"]').prop("disabled", false);
                                $('#programModal [name="' + index + '"]').removeAttr('disabled');
                            }*!/
                            if(index == "date_limit_admission") {
                                $('[name="date_limit_admission-day"]').val(data_field.value.day);
                                $('[name="date_limit_admission-month"]').val(data_field.value.month);
                            } else if($('[name="' + index + '[]"]').length) {
                                /!*$('[name="' + index + '[]"]').on("click", function () {
                                    $('[name="' + index + '[]"]').prop("disabled", true);
                                });*!/
                                $('[name="' + index + '[]"]').val(data_field.value);
                                $('[name="' + index + '[]"]').trigger('change');
                                if (action == "view") {
                                    $('[name="' + index + '[]"]').prop("disabled", true);
                                }
                                else {
                                    $('[name="' + index + '[]"]').prop("disabled", false);
                                }
                            }
                        });
                        // $('#programModal input').prop('disabled', true);
                        $('#programModal').modal('show');
                    },
                    error: (xhr) => {
                        console.log("Error");
                    },
                });*/
            });
        </script>
    @endpush


</div>
