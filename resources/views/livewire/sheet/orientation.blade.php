<div class="row">

    <div class="col-md-4">
        <div class="block block-rounded">
            <div class="block-header block-header-default"><h3 class="block-title">Filtre</h3></div>
            <div class="block-content p-3">
                    <form id="program-filter">
                    @foreach($groupedResultsArray as $field_name => $values)
                        @php
                            /*if ($field_name == 'admission_test_langue'){
                                // dd($field_name, $values);
                                $values = ["Pas de test", "TCF", "TEF ", "TFI", "IELTS", "TOEFL", "TOEIC", "DELF", "DALF", "GMAT"];
                            }*/
                            // dd($field_name, $values);
                            $attributes = [
                                'class' => 'search-field form-control form-control-alt' . (isset($errors) && $errors->has($field_name) ? ' is-invalid' : (isset($errors) && $errors->has($field_name) ? 'true' : 'false')),
                                'aria-describedby' => $field_name . '-error',
                                'aria-invalid' => ''
                            ];

                            //$attributes['data-placeholder'] = __('lang.' . $field_name) . '..';
                            $attributes['multiple'] = "multiple";
                            $attributes['name'] = $field_name . "[]";
                        @endphp
                        <div class="form-group">
                            @if($field_name =="admission_prix_total")
                                <label for="{{ $field_name }}">{{ __('lang.' . $field_name)}}</label> <span class="budget">0</span>
                                <br>

                                @php sort($values); @endphp
                                <input type="range" name="{{$field_name}}" class="form-control-range" id="formControlRange" value="0" min="0" max="{{ end($values) }}">
                                <div id="{{ $field_name }}-error" class="invalid-feedback">{{ isset($errors) && $errors->has($field_name) ? $errors->first($field_name) : '' }}</div>
                            @else
                                <label for="{{ $field_name }}">{{ __('lang.' . $field_name)}}</label>
                                <br>
                                {{ Form::select($field_name, $values, old($field_name), $attributes) }}
                                <div id="{{ $field_name }}-error" class="invalid-feedback">{{ isset($errors) && $errors->has($field_name) ? $errors->first($field_name) : '' }}</div>
                            @endif
                        </div>
                    @endforeach
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="content-full" style="margin-bottom: 10px;">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Les programmes
                </h1>
                @admin
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <button type="button" class="btn btn-alt-primary add-program-btn"><i class="fa fa-fw fa-plus mr-1"></i> Ajouter programme</button>
                </nav>
                @endadmin
            </div>
        </div>
        @livewire('sheet.partials.tabs', ['customer_case' => $customer_case])
    </div>
    @push('custom_scripts')
        <script>
            /*Livewire.on('filterUpdatedList', (obj, data) => {
                console.log(data);

                $('select[name*="[]"]').select2({
                    sortResults: data => data.sort((a, b) => a.text.localeCompare(b.text)),
                });

            });*/
            function serialize() {
                var fields = {admission_prix_total: $('#formControlRange').val()};
                // var fields = {};
                $("#program-filter .search-field").each(function() {
                    let name = $(this).attr('name');
                    name = name.replace('[]', '');
                    let dataArray = [];
                    let values = $(this).select2('data');
                    $.each(values, function( index, data ) {
                        dataArray.push(data.text);
                    });
                    fields[name] = dataArray;
                    // return false
                });
                return fields;
            }

            $(document).ready(function() {
                $('.add-program-btn').click(function () {
                    // Livewire.emit('resetForm');
                    document.getElementById("program-form").reset();
                    $('.tags').val(null).trigger('change');
                    Livewire.emit('toggleModalAction', "add");
                    // $('#program-form').trigger('reset');
                    $('#programModal').modal('show');
                });
                $('#program-filter select[name*="[]"]').each(function () {
                    /* Get options */
                    var selectList = $(this).find('option');
                    /* Order by innerText (case insensitive) */
                    selectList.sort(
                        (a, b) => a.innerText.localeCompare(b.innerText)
                    );
                    /* Re-do select HTML */
                    $(this).html(selectList);
                    $(this).select2();
                });
                $('#program-filter select[name*="[]"]').on('select2:select', function (e) {
                    // Do something
                    // console.log($(this).select2('data'));
                    // var select = $(this).attr('name');
                    //var data = e.params.data;
                    var data = serialize();
                    Livewire.emit('filterUpdated', 'select', data);
                });
                $('#program-filter select[name*="[]"]').on('select2:unselect', function (e) {
                    // Do something
                    // var select = $(this).attr('name');
                    // var data = e.params.data;
                    var data = serialize();
                    Livewire.emit('filterUpdated', 'unselect', data);
                    // $(this).select2('data')
                });
            });

            $(document).ready(function() {
                $('#formControlRange').on('change',function(e){
                    console.log("changing ...")
                    // console.log($("#program-filter").serialize());
                    // var controlrange = $(this).attr('name');
                    // var targetElement = event.target || event.srcElement;
                    $(this).closest('.form-group').find('.budget').text($(this).val())
                    var data = serialize();
                    Livewire.emit('filterUpdated', 'unrange', data);
                });
            });
        </script>
    @endpush
</div>
