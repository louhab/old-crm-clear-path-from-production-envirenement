<div wire:ignore.self class="js-wizard-simple block">
    <!-- Wizard Progress Bar -->
    <div class="progress rounded-0" data-wizard="progress" style="height: 8px;">
        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 30%;"
            aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
    <!-- END Wizard Progress Bar -->

    <!-- Step Tabs -->
    <ul class="nav nav-tabs nav-tabs-alt nav-justified" role="tablist">
        @foreach ($groupNames as $groupId => $groupName)
            <li class="nav-item" wire:key="nav-item-{{ $groupId }}">
                @if (isset($this->filled[$groupId]))
                    <i class="far fa-2x fa-check-circle text-success"></i>
                @else
                    <i class="far fa-2x fa-times-circle text-danger"></i>
                @endif
                <a class="nav-link" href="#wizard-progress2-step{{ $groupId }}" data-toggle="tab"
                    style="height: 66px;">{{ $groupId }}.{{ $groupName }}</a>
            </li>
        @endforeach
    </ul>
    <!-- END Step Tabs -->

    <!-- Form -->
    <form action="" onsubmit="return false" method="POST" id="customer-form" enctype="multipart/form-data">
        <!-- Steps Content -->
        <div class="block-content block-content-full tab-content px-md-5" style="min-height: 314px;">
            {{-- @php dd($groupNames); @endphp --}}
            @foreach ($groupNames as $groupId => $groupName)
                @livewire('input.group.multiple', ['customer_case' => $customer_case, 'fields' => $groupedResultsArray[$groupId], 'groupId' => $groupId, 'grouping' => $grouping, 'join' => $join], key('group-' . $groupId))
            @endforeach
        </div>
        <!-- END Steps Content -->

        <!-- Steps Navigation -->
        <div class="block-content block-content-sm block-content-full bg-body-light rounded-bottom">
            <div class="row">
                <div class="col-6">
                    <button type="button" class="btn btn-alt-primary" data-wizard="prev">
                        <i class="fa fa-angle-left mr-1"></i> {{ __('lang.customer_sheet_previous_btn') }}
                    </button>
                </div>
                <div class="col-6 text-right">
                    @if ($sheet_id != 9)
                        <button type="button" class="btn btn-alt-primary" data-wizard="next">
                            {{ __('lang.customer_sheet_next_btn') }} <i class="fa fa-angle-right ml-1"></i>
                        </button>
                    @endif
                    <button type="submit" class="btn btn-primary d-none" data-wizard="finish" wire:click="submit">
                        <i class="fa fa-check mr-1"></i> {{ __('lang.customer_sheet_submit_btn') }}
                    </button>
                </div>
            </div>
        </div>
        <!-- END Steps Navigation -->
    </form>
    <!-- END Form -->
    @push('custom_scripts')
        <script>
            Livewire.on('tabActive', obj => {
                // console.log(obj);
                $('.js-wizard-simple').bootstrapWizard('show', obj["tabId"] - 1);
            });
            Livewire.on('makeFlash', obj => {
                window.scrollTo(0, 0);
            });
            /*$('body').on('click', '.add-block', function () {
                let pp = $(this).closest('.tab-pane').attr('id');
                pp = pp.replace('wizard-progress2-step', '');
                console.log("block-template-" + pp);
                var temp = document.getElementById("block-template-" + pp);
                console.log(temp);
                var clon = $(temp.content.cloneNode(true));
                clon.appendTo($('#row-'+pp));
            });*/
        </script>
    @endpush
</div>
