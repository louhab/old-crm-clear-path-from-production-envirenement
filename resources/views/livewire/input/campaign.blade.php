<div class="col-3 p-1">
    <label for="campaign" class="sr-only">Canal</label>
    {{Form::select('campaign', $campaigns, null, ['class' => 'form-control form-control-alt dt_search_field', "data-control" => "select2", 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '','style' => 'height: 42px;border: 1px solid #aaa;', 'wire:model' => "campaign_id", 'wire:change' => "change"])}}
</div>
