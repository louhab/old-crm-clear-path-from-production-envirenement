
<div class="col-3 p-1">
    <label for="source" class="sr-only">Source</label>
    {{Form::select('source', \App\Models\LeadForm::whereIn("lead_form_marketing_campaign_id", $campaigns)->pluck('title', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', "data-control" => "select2", 'multiple' => 'multiple', 'aria-describedby' => 'source-error', 'aria-invalid' => '','style' => 'height: 42px;border: 1px solid #aaa;'])}}
</div>
