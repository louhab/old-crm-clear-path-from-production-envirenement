<div class="col-3 p-1">
    <select name="calls-count" id="calls-count"  class="form-control form-control-alt dt_search_field"data-control="select2" multiple="multiple" aria-describedby="tags-error" aria-invalid=""   style="height: 42px;border: 1px solid #aaa;">
        <option value >{{ __('lang.calls-count') }}</option>
        <option value="0">{{ __('lang.lead_filter_no_calls') }}</option>
        @foreach($counts as $group)
            <option value="{{ $group->calls }}">{{ $group->calls }}</option>
        @endforeach
    </select>
</div>
