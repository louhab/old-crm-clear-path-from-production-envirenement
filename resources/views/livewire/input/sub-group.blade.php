<?php
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
?>
<div wire:ignore.self>

    @foreach ($groupedResultsArray as $group => $fields_)
        @if ($group)
            @foreach ($fields_ as $field)
                @if ($loop->first)
                    <span
                        class="badge badge-danger">{{ $lang === 'fr' ? App\Models\CustomerFieldSubGroup::find($group)->name_fr : App\Models\CustomerFieldSubGroup::find($group)->name_en }}</span>
                    <div class="row">
                @endif
                <div class="col-md-6">
                    @livewire('input.group.partials.field', ['customer_case' => $customer_case, 'field_name' => $field['field_name'], 'field_template' => $field['template'], 'field_group_id' => $field['customer_field_group_id'], 'join' => $join], key($groupName . '.' . $field['id']))
                </div>
                @if ($loop->last)
</div>
@if (in_array($group, [4, 5, 7, 8]))
    @if (count($orders[$group]) > 0)
        @foreach ($orders[$group] as $idx)
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn-block-option float-right"
                        wire:click="deleteBlock('{{ $group }}', '{{ $idx }}')">
                        <i class="si si-close text-danger"></i>
                    </button>
                </div>
            </div>
            @livewire('input.group.block', ['customer_case' => $customer_case, 'fields' => $fields_, 'order' => $idx, 'groupId' => $group, 'grouping' => $grouping, 'join' => $join], key('block.' . $group . '.' . $idx))
        @endforeach
    @endif
    <div class="row">
        <div class="col-12">
            <button class="btn btn-alt-secondary float-right"
                wire:click="addBlock('{{ $group }}')">{{ __('lang.customer_sheet_Add_btn') }}</button>
        </div>
    </div>
@endif
@endif
@endforeach
@endif
@endforeach
</div>
