<?php
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
?>
<div class="col-3 p-1">
    {{ Form::select($name, $lang === 'fr' ? ['Non', 'Oui'] : ['No', 'Yes'], null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
    @push('custom_scripts')
        <script>
            $("[name='{{ $name }}']").select2({
                placeholder: "{{ __('lang.' . $name) }} ...",
                width: "100%",
                allowClear: true,
            }).select2("val", null);
            $("[name='{{ $name }}']").val(null).trigger("change");
        </script>
    @endpush
</div>
