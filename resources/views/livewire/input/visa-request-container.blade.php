<div class="row">
    @php
        $result_fields = ['visa_request_date', 'visa_request_type', 'visa_request_result'];
        $show_result = true;
    @endphp
    @foreach ($fields as $field)
        @php
            $field_name = $field['field_name'];
            if ($field['field_name'] == 'visa_request' && $clear_fields->visa_request != 82) {
                // 82 => oui, 83 => non
                $show_result = false;
            }
        @endphp
        @if (in_array($field['field_name'], Illuminate\Support\Facades\Schema::getColumnListing('customer_visa_requests')))
            <div
                @if (in_array($field['field_name'], $result_fields)) class="col-6 visa-result-field-0" style="display: {{ $show_result ? 'block' : 'none' }};" @elseif($field['field_name'] == 'visa_request') class="col-6 visa-request-field" @else class="col-6" @endif>
                @if ($field['template'] === 'text')
                    @livewire('input.form.text', ['field' => $field, 'selected' => $clear_fields->$field_name, 'index' => null, 'container' => 'VisaRequest'], key($field['field_name'] . 'VisaRequest'))
                @elseif ($field['template'] === 'select')
                    @livewire('input.form.select', ['field' => $field, 'selected' => $clear_fields->$field_name, 'is_multiple' => false, 'index' => null, 'container' => 'VisaRequest', 'program_id' => $program_id], key($field['field_name'] . 'VisaRequest'))
                @else
                    @livewire('input.form.date', ['field' => $field, 'selected' => $clear_fields->$field_name, 'index' => null, 'container' => 'VisaRequest'], key($field['field_name'] . 'VisaRequest'))
                @endif
            </div>
        @endif
    @endforeach
    @if (count($visa_requests))
        @foreach ($visa_requests as $index => $visa_request)
            @php
                // $result_fields = ["visa_request_date", "visa_request_type", "visa_request_result"];
                $show_result = true;
            @endphp
            @foreach ($fields as $field)
                @php
                    $field_name = $field['field_name'];
                    if ($field['field_name'] == 'visa_request' && $visa_request->visa_request != 82) {
                        // 82 => oui, 83 => non
                        $show_result = false;
                    }
                @endphp
                @if ($loop->first)
                    <div class="col-12">
                        <hr>
                    </div>
                @endif
                @if (in_array($field['field_name'], Illuminate\Support\Facades\Schema::getColumnListing('customer_visa_requests')))
                    <div
                        @if (in_array($field['field_name'], $result_fields)) class="col-6 visa-result-field-{{ $visa_request->id }}" style="display: {{ $show_result ? 'block' : 'none' }};" @elseif($field['field_name'] == 'visa_request') class="col-6 visa-request-field" @else class="col-6" @endif>
                        @if ($field['template'] === 'text')
                            @livewire('input.form.text', ['field' => $field, 'selected' => $visa_request->$field_name, 'index' => $index, 'container' => 'VisaRequest'], key($field['field_name'] . $index))
                        @elseif ($field['template'] === 'select')
                            @livewire('input.form.select', ['field' => $field, 'selected' => $visa_request->$field_name, 'is_multiple' => false, 'index' => $index, 'container' => 'VisaRequest'], key($field['field_name'] . $index))
                        @else
                            @livewire('input.form.date', ['field' => $field, 'selected' => $visa_request->$field_name, 'index' => $index, 'container' => 'VisaRequest'], key($field['field_name'] . $index))
                        @endif
                    </div>
                @endif
            @endforeach
            {{-- @if (!$loop->last)
                <div class="col-12">
                    <hr>
                </div>
            @endif --}}
        @endforeach
    @endif
    <div class="col-12">
        <button type="button" class="float-right m-3 btn btn-secondary" data-lang="{{ session()->get('locale') }}"
            wire:click="addRequest">
            {{ __('lang.clear1001_form_add_visa_request_btn') }}
        </button>
    </div>
    @push('custom_scripts')
        <script>
            Livewire.on('toggleVisaRequestVisibility', (id, value) => {
                if (value == 82) {
                    $(".visa-result-field-" + id).each(function() {
                        $(this).show();
                    });
                } else {
                    $(".visa-result-field-" + id).each(function() {
                        $(this).hide();
                    });
                }
            });
        </script>
    @endpush
</div>
