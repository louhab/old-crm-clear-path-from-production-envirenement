<div class="col-3 p-1">
    <label for="processed" class="sr-only">{{ __("lang.lead_filter_change") }}</label>
    <select name="processed" id="processed" class="form-control form-control-alt dt_search_field"data-control="select2" multiple="multiple" aria-describedby="tags-error" aria-invalid=""  style="height: 42px;border: 1px solid #aaa;">
        <option value="processed" @if($selected == "processed") selected @endif>{{ __("lang.lead_filter_change_true") }}</option>
        <option value="unprocessed" @if($selected == "unprocessed") selected @endif>{{ __("lang.lead_filter_change_false") }}</option>
    </select>
</div>
