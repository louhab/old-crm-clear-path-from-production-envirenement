<div class="col-md-12">
    @if(count($garants))
        @foreach($garants as $index => $garant)
            <div class="col-md-12">
                <hr>
            </div>
            <div class="col-md-6">
                <ul style='list-style:none;padding:0;'>
                    @foreach($fields as $field)
                        @if(in_array($field["field_name"], Illuminate\Support\Facades\Schema::getColumnListing('customer_garants')))
                            @php
                                $field_name = $field["field_name"];
                            @endphp
                            <li>
                                @if ($field["template"] === "date")
                                    @include('form.elements.date-text' , ['customer_field' => $field, 'field_name' => $field["field_name"], 'customer_garant' => $garant])
                                @elseif ($field["template"] === "text")
                                    @livewire('input.form.text', ['field' => $field, 'selected' => $garant->$field_name, 'index' => $index], key($field["field_name"] . $index))
                                @elseif ($field["template"] === "select")
                                    @livewire('input.form.select', ['field' => $field, 'selected' => $garant->$field_name, 'is_multiple' => false,'index' => $index], key($field["field_name"] . $index))
                                @else
                                    @include('form.elements.' . $field["template"], ['customer_field' => $field, 'field_name' => $field["field_name"], 'customer_garant' => $garant])
                                @endif
                            </li>
                        @elseif(in_array($field["field_name"], ["guarantor_income_type"]))
                            @php
                                $selected = \App\Models\CustomerFieldValue::where('customer_id', $customer_case->customer_id)
                                    ->where('field_name', $field["field_name"])
                                    ->where('customer_garant_id', $garant->id)
                                    ->pluck('model_id')
                                    ->toArray();
                            @endphp
                            @livewire('input.form.select', ['field' => $field, 'selected' => $selected, 'is_multiple' => true, 'index' => $index], key($field["field_name"] . $index))
                        @endif
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6 center"></div>
        @endforeach
    @endif
    <div class="col-md-12">
        <button type="button" class="float-right m-3 btn btn-secondary" wire:click="addGarant">
            {{ __("lang.clear1001_form_add_garant_btn") }}
        </button>
    </div>
    @push('custom_scripts')
        <script>
            Livewire.on('dehydrateGarantIncomeType', obj => {
                console.log("dehydrate");
                $(".js-select2").select2();
            });
        </script>
    @endpush
</div>
