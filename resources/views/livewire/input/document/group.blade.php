<div>
    <div class="row">
        @if($count > 0)
            @foreach(range(0, $count-1) as $index)
                <div class="col-12 pb-2">
                    @livewire('input.document.condition', ["order" => $index, "documentID" => $documentID], key($index))
                </div>
            @endforeach
        @endif
    </div>
    <div class="form-group p-3">
        <button class="btn btn-sm btn-secondary float-right m-2" wire:click="increase">Add Condition</button>
    </div>
    @push('custom_scripts')
        <script>
            /*Livewire.on("conditionSelect2Values", (order, values) => {
                console.log(values);
            });*/
            Livewire.on("conditionSelect2", (order) => {
                // console.log(order);
                $('#edit-form-condition_' + order).select2({
                    placeholder: "Customer Field ...",
                    width:"100%",
                    allowClear: true
                });
                $('#edit-form-values_' + order).select2({
                    placeholder: "Valeurs ...",
                    width:"100%",
                    allowClear: true
                });
                $('#edit-form-condition_' + order).on('select2:select', function (e) {
                    // Do something
                    // console.log("condition changed");
                    var data = $('#edit-form-condition_' + order).val();
                    Livewire.emit("fieldChanged", order, data);
                    // console.log(data);
                });
            });
        </script>
    @endpush
</div>
