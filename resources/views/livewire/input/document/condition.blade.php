<div style="border: 2px solid red;padding: 5px;">
    <div class="form-group p-2">
        <label for="edit-form-condition_type_{{ $order }}">Type</label>
        <select class="form-control form-control-alt" wire:model="conditionType" id="edit-form-condition_type_{{ $order }}" name="edit-form-condition_type_{{ $order }}" aria-describedby="edit-form-condition_type_{{ $order }}-error" aria-invalid="false">
            <option value="">Sélectionnez type</option>
                <option value="question">Question</option>
                <option value="payment">Payment</option>
        </select>
        <div id="edit-form-condition_type_{{ $order }}-error" class="invalid-feedback"></div>
    </div>
    @if($conditionType == "question")
    <div class="form-group p-2">
        <label for="edit-form-condition_{{ $order }}">Question</label>
        <select class="form-control form-control-alt" id="edit-form-condition_{{ $order }}" name="edit-form-condition_{{ $order }}" aria-describedby="edit-form-condition_{{ $order }}-error" aria-invalid="false">
            <option value="">Sélectionnez question</option>
            @foreach(\App\Models\CustomerField::where('template', 'select')->get() as $customer_field)
                <option value="{{ $customer_field->field_name }}" @if($customer_field->field_name == $field_name) selected @endif>{{ $customer_field->field_name }}</option>
            @endforeach
        </select>
        <div id="edit-form-condition_{{ $order }}-error" class="invalid-feedback"></div>
    </div>
    <div class="form-group p-2">
        <label for="edit-form-values_{{ $order }}">Valeurs</label>
        <select class="form-control form-control-alt" id="edit-form-values_{{ $order }}" name="edit-form-values_{{ $order }}[]" aria-describedby="edit-form-values_{{ $order }}-error" aria-invalid="false" multiple>
            <option value="">Sélectionnez les valeurs</option>
            @foreach($values as $value)
                <option value="{{ $value }}" @if(in_array($value, $selectedValues)) selected @endif>{{ $value }}</option>
            @endforeach
        </select>
        <div id="edit-form-values_{{ $order }}-error" class="invalid-feedback"></div>
    </div>
    @elseif($conditionType == "meet")
        <div class="form-group p-2">
            <label for="edit-form-meet_values_{{ $order }}">Rencontres</label>
            <select class="form-control form-control-alt" id="edit-form-meet_values_{{ $order }}" name="edit-form-meet_values_{{ $order }}[]" aria-describedby="edit-form-meet_values_{{ $order }}-error" aria-invalid="false">
                <option value="">Sélectionnez les rencontres</option>
                @foreach(range(1, 5) as $i)
                    <option value="meet_{{ $i }}">Rencontre {{ $i }}</option>
                @endforeach
            </select>
            <div id="edit-form-meet_values_{{ $order }}-error" class="invalid-feedback"></div>
        </div>
    @elseif($conditionType == "payment")
        <div class="form-group p-2">
            <label for="edit-form-payment_condition_{{ $order }}">Payments</label>
            <select class="form-control form-control-alt" id="edit-form-payment_condition_{{ $order }}" name="edit-form-payment_condition_{{ $order }}[]" aria-describedby="edit-form-payment_condition_{{ $order }}-error" aria-invalid="false">
                <option value="">Sélectionnez les payments</option>
                @foreach(\App\Models\ProgramPayment::all() as $payment)
                    <option value="{{ $payment->id }}" @if(in_array($payment->id, $selectedValues)) selected @endif>{{ $payment->name_fr }}</option>
                @endforeach
            </select>
            <div id="edit-form-payment_condition_{{ $order }}-error" class="invalid-feedback"></div>
        </div>
    @endif
    @push("custom_scripts")
        <script>
            /*let order = {!! $order !!};
            $('#edit-form-condition_' + order).select2({
                placeholder: "Customer Field ...",
                width:"100%",
                allowClear: true
            });
            $('#edit-form-values_' + order).select2({
                placeholder: "Valeurs ...",
                width:"100%",
                allowClear: true
            });*/
            /*Livewire.on("conditionSelect2", obj => {
                console.log("testing", order);
                $('#edit-form-condition_' + order).select2({
                    placeholder: "Customer Field ...",
                    width:"100%",
                    allowClear: true
                });
                $('#edit-form-values_' + order).select2({
                    placeholder: "Valeurs ...",
                    width:"100%",
                    allowClear: true
                });
            });*/
        </script>
    @endpush
</div>
