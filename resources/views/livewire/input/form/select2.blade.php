<?php
$hidden_by_def = ['language_tests'];
?>
<div class="form-group" id="{{ $field_name }}" @if (in_array($field_name, $hidden_by_def) &&
        old($field_name, $selected) === null &&
        !$errors->has($customer_field->field_name)) style="visibility: hidden" @endif>
    <label for="{{ $field_name }}">{{ __('lang.' . $field_name) }}</label>
    @if ($customer_field->pivot->required)
        <span class="text-danger">*</span>
    @endif
    @php
        if (!is_array($values)) {
            $values = $values->toArray();
        }
        asort($values);
        $attributes['class'] = 'form-control form-control-alt' . (isset($errors) && $errors->has($field_name) ? ' is-invalid' : (isset($errors) && $errors->has($field_name) ? 'true' : 'false'));
    @endphp
    {{ Form::select($field_name, $values, old($field_name, $selected), $attributes) }}
    <div id="{{ $field_name }}-error" class="invalid-feedback">
        {{ isset($errors) && $errors->has($field_name) ? $errors->first($field_name) : '' }}</div>
    @push('custom_scripts')
        <script>
            $("[name='{{ $field_name }}[]']").select2();
        </script>
    @endpush
</div>
