<div class="custom-file" lang="fr">
    <input type="file" wire:model="document" class="custom-file-input js-custom-file-input-enabled" id="{{ $collection }}">
    <label class="custom-file-label">{{ $name }} ..</label>
</div>
