<?php
if ($program_id) {
    switch ($program_id) {
        case 31:
            $label = $field['field_name'] . '_uk';
            break;
        case 32:
            $label = $field['field_name'] . '_es';
            break;
        default:
            $label = $field['field_name'];
    }
} else {
    $label = $field['field_name'];
}
?>

<div class="form-group" @if (in_array($field['field_name'], $hidden_by_def) && old($field['field_name'], $selected) === null) style="display: none" @endif>
    <label for="{{ $field['field_name'] }}">{{ __('lang.' . $label) }}</label>
    {{-- {{ Form::select($field["field_name"], $values, old($field["field_name"], $selected), $attributes) }} --}}
    <select class="form-control form-control-alt {{ $is_multiple === true ? 'js-select2' : '' }}"
        wire:model.lazy="selected" {{ $is_multiple === true ? 'multiple' : '' }}>
        @if ($is_multiple === false)
            <option value="">{{ __('lang.' . $label) . '..' }}</option>
        @endif
        @foreach ($values as $id => $value)
            <option value="{{ $id }}"
                {{ ($is_multiple === true && in_array($id, $selected)) || ($is_multiple === false && $id == $selected) ? 'selected' : '' }}>
                {{ $value }}</option>
        @endforeach
    </select>
    @if ($is_multiple === true)
        @push('custom_scripts')
            <script>
                $(".js-select2").select2();
                $('.js-select2').on('change', function(e) {
                    // Do something
                    @this.set('selected', $(this).val());
                });
                /*document.addEventListener("DOMContentLoaded", () => {
                    Livewire.hook('element.updated', (el, component) => {
                        console.log("updated..!");
                        $(".js-select2").select2();
                        $('.js-select2').on('change', function (e) {
                            // Do something
                        @this.set('selected', $(this).val());
                        });
                    });
                });*/
            </script>
        @endpush
    @endif
</div>
