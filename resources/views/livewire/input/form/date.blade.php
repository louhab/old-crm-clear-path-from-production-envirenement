<div class="form-group">
    <label for="{{ $field["field_name"] }}">{{ __('lang.' . $field["field_name"])}}</label>
    <input type="date" class="form-control form-control-alt" wire:model="selected">
    <div id="{{ $field["field_name"] }}-error" class="invalid-feedback"></div>
    {{--@push('custom_scripts')
        <script>
            /*$(function () {
                $(".visa_request_date").datepicker({
                    dateFormat: "yy-mm-dd",
                });
            });*/
        </script>
    @endpush--}}
</div>
