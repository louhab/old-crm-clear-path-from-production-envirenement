<div class="form-group" @if(in_array($field["field_name"], $hidden_by_def) && old($field["field_name"], $selected) === null) style="display: none" @endif>
    <label for="{{ $field["field_name"] }}">{{ __('lang.' . $field["field_name"])}}</label>
    <input type="text" class="form-control form-control-alt" wire:model="selected" placeholder="{{ __('lang.' . $field["field_name"]) }}..">
</div>
