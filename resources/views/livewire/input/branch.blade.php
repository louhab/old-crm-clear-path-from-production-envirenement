
<div class="col-md-6" @if(!$visible) style="display: none;" @endif>
    <div class="form-group">
        <label for="other_branch">Ajouter Domaine d'étude</label>
        <input type="text" class="form-control form-control-alt" name="other_branch" value="{{ $value }}">
        <div id="other_branch-error" class="invalid-feedback">{{ isset($errors) && $errors->has('other_branch') ? $errors->first('other_branch') : '' }}</div>
    </div>
</div>
