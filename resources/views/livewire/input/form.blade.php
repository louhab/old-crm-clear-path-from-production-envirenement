
<div class="col-3 p-1">
    <label for="influencer" class="sr-only">Représentant</label>
    {{Form::select('influencer', \App\Models\LeadForm::whereIn("lead_form_marketing_campaign_id", $campaigns)->pluck('title', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', "data-control" => "select2", 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '','style' => 'height: 42px;border: 1px solid #aaa;'])}}
</div>
