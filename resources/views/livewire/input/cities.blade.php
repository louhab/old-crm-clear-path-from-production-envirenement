{{ Form::select('city2', $cities, null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
@push('custom_scripts')
    <script>
        const value = "{{ $value }}";

        $("[name='city2']").select2({
            placeholder: "Ville ...",
            width: "-webkit-fill-available",
            allowClear: true,
        })

        $("[name='city2']").val(value).trigger("change");
    </script>
@endpush
