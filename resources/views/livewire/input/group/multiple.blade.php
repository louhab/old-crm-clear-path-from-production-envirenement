<div wire:ignore.self class="tab-pane" id="wizard-progress2-step{{ $groupId }}" role="tabpanel">
    @if (in_array($groupId, [4, 5]))
        @livewire('input.sub-group', ['customer_case' => $customer_case, 'fields' => $fields, 'groupId' => $groupId, 'join' => $join], key($groupName))
    @else
        @foreach ($fields as $field)
            @if ($loop->first)
                <span class="badge badge-danger">{{ $groupName }}</span>
                <div class="row">
            @endif
            <div class="col-md-6">
                @livewire('input.group.partials.field', ['customer_case' => $customer_case, 'field_name' => $field['field_name'], 'field_template' => $field['template'], 'field_group_id' => $field['customer_field_group_id'], 'join' => $join], key($groupName . '.' . $field['id']))
            </div>
            @if ($loop->last)
</div>
@if (in_array($groupId, [1]))
    <span class="badge badge-danger">Anciennes demandes des visas & permis d'études</span>
    @livewire('input.visa-request-container', ['customer_case' => $customer_case, 'fields' => $extraFields])
@endif
@if (in_array($groupId, [2, 3, 6]))
    @if (count($orders) > 0)
        @foreach ($orders as $idx)
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn-block-option float-right"
                        wire:click="deleteBlock({{ $idx }})">
                        <i class="si si-close text-danger"></i>
                    </button>
                </div>
            </div>
            @livewire('input.group.element', ['customer_case' => $customer_case, 'fields' => $fields, 'order' => $idx, 'groupId' => $groupId, 'grouping' => $grouping, 'join' => $join], key($groupId . '.' . $idx))
        @endforeach
    @endif
    <div class="row">
        <div class="col-12">
            <button class="btn btn-alt-secondary float-right"
                wire:click="addBlock">{{ __('lang.customer_sheet_Add_btn') }}</button>
        </div>
    </div>
@endif
@endif
@endforeach
@endif
</div>
