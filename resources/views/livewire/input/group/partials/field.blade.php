@if($field_template == "text")
    <div class="form-group">
        <label for="{{ $field_name }}">{{ __('lang.' . $field_name)}}</label>
        <span class="text-danger">*</span>
        <input type="text" class="form-control form-control-alt" wire:model="value">
        @error($field_name) <span class="error">{{ $message }}</span> @enderror
    </div>
@elseif($field_template == "date")
    <div class="form-group">
        <label for="{{ $field_name }}">{{ __('lang.' . $field_name)}}</label>
        <span class="text-danger">*</span>
        <input type="date" class="form-control form-control-alt" wire:model="value">
        @error($field_name) <span class="error">{{ $message }}</span> @enderror
    </div>
@elseif($field_template == "tel")
    <div class="form-group">
        <label for="{{ $field_name }}">{{ __('lang.' . $field_name) }}</label>
        <span class="text-danger">*</span>
        <br />
        <input type="tel" class="form-control form-control-alt" wire:model="value">
        <span id="valid-msg" class="text-success" style="display:none;">✓ Valid</span>
        @error($field_name) <span class="error">{{ $message }}</span> @enderror
    </div>
@elseif($field_template == "email")
    <div class="form-group">
        <label for="{{ $field_name }}">{{ __('lang.' . $field_name)}}</label>
        <span class="text-danger">*</span>
        <input type="email" class="form-control form-control-alt" wire:model="value">
        @error($field_name) <span class="error">{{ $message }}</span> @enderror
    </div>
@elseif($field_template == "textarea")
    <div class="form-group">
        <label for="{{ $field_name }}">{{ __('lang.' . $field_name)}}</label>
        <span class="text-danger">*</span>
        <textarea class="form-control form-control-alt" wire:model="value"></textarea>
        @error($field_name) <span class="error">{{ $message }}</span> @enderror
    </div>
@elseif($field_template == "number")
    <div class="form-group">
        <label for="{{ $field_name }}">{{ __('lang.' . $field_name)}}</label>
        <span class="text-danger">*</span>
        <input type="number" class="form-control form-control-alt" wire:model="value">
        @error($field_name) <span class="error">{{ $message }}</span> @enderror
    </div>
@endif
