<div>
    @if($visible)
    <div class="content status" style="padding-top: 0;padding-bottom: 0;">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6" style="text-align: center">
                <p>
                    <a class="badge badge-danger" href="javascript:void(0)">{{ $message }}</a>
                </p>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
    @endif
</div>
