<div wire:ignore.self class="row">
    @foreach($fields as $field)
        <div class="col-md-6">
            @livewire('input.group.partials.field', ['customer_case' => $customer_case, 'field_name' => $field["field_name"], "field_template" => $field["template"], "field_group_id" => $field["customer_field_group_id"], 'order' => $order, 'groupData' => $groupData, 'join' => $join], key('element.'.$field["id"]))
        </div>
    @endforeach
</div>
