<div wire:ignore.self>
    @foreach($fields as $field)
        @if($loop->first)
            <div class="row">
                @endif
                <div class="col-md-6">
                    @livewire('input.group.partials.field', ['customer_case' => $customer_case, 'field_name' => $field["field_name"], "field_template" => $field["template"], "field_group_id" => $field["customer_field_group_id"], 'order' => $order, 'groupData' => $groupData, 'join' => $join], key('block.'.$field["id"]))
                </div>
                @if($loop->last)
            </div>
        @endif
    @endforeach
</div>
