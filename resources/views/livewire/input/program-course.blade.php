<div class="form-group form-row">
    <div class="col-12">
        <label>{{ $label }} : </label>
        @admin
            <!--        <small class="float-right text-danger-light">
                <a class="text-warning delete-program-course-field" href="javascript:void(0)" data-field="{{ $key }}" data-title="{{ $label }}">
                    Supprimer des valeurs
                </a>
            </small>-->
        @endadmin
        <select id="tags_{{ $key }}" class="tags" name="{{ $key }}[]" multiple="multiple"
            style="width: 100%">
            @foreach ($selectValues as $val)
                <option value="{{ $val }}">{{ $val }}</option>
            @endforeach
        </select>
    </div>
    @push('custom_scripts')
        <script>
            Livewire.on('selectInit', obj => {
                $('#tags_{!! $key !!}').select2({
                    tags: true,
                    tokenSeparators: [';'],
                    allowClear: true,
                    placeholder: "Ajouter des valeurs",
                    /* the next 2 lines make sure the user can click away after typing and not lose the new tag */
                    selectOnClose: false,
                    closeOnSelect: true,
                    createTag: function(params) {
                        var term = $.trim(params.term);

                        if (term === '') {
                            return null;
                        }

                        return {
                            id: term,
                            text: term,
                            newTag: true // add additional parameters
                        }
                    },
                    /*language: {
                        noResults: function (params) {
                            return "Lien existe deja sur la base veuillez chercher par nom du programe";
                        }
                    },*/
                    /*createTag: function (params) {
                        // console.log(params.term);
                        return {
                            id: params.term,
                            text: params.term
                        }
                    },
                    insertTag: function (data, tag) {
                        // Insert the tag at the end of the results
                        console.log(tag);
                        data.push(tag);
                    }*/
                }).on("select2:select", function(e) {
                    // console.log(e.target.id);
                    // Livewire.emit('tagAdded', e.target.id.replace("tags_", ""), e.params.data.text);
                    let field_name = e.target.id.replace("tags_", "");
                    $.ajax({
                        method: 'post',
                        url: `/api/v1/program-course/${field_name}`,
                        data: {
                            values: $(this).val()
                        },
                        success: () => {
                            // console.log("value added");
                        },
                        error: (xhr) => {
                            Swal.fire(
                                'Ajout!',
                                'Erreur ajout valeurs!',
                                'error'
                            );
                        }
                    });
                }).on("select2:unselect", function(e) {
                    var data = e.params.data;
                    let field_name = e.target.id.replace("tags_", "");

                    if ('newTag' in data) {
                        console.log(data);
                        var dataOp = {
                            id: data.id,
                            text: data.text
                        };

                        var newOption = new Option(dataOp.text, dataOp.id, false, false);
                        $("#tags_" + field_name).append(newOption).trigger('change');
                    }
                });
                if ('{!! $key !!}' in obj) {
                    console.log(obj);
                    $('#tags_{!! $key !!}').val(obj.{!! $key !!});
                    $('#tags_{!! $key !!}').trigger('change');
                }
            });
        </script>
    @endpush
</div>
