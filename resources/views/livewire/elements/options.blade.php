<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
<div class="block-content">
    <table class="js-table-checkable table table-hover table-vcenter bg-light">
        <tbody>
    <!-- If you put a checkbox in thead section, it will automatically toggle all tbody section checkboxes -->
    @php $currency = \App\Models\Currency::find($customer_case->customer->currency_id)->iso; @endphp
    @if(is_null($payment) || (is_null($payment["pivot"]["performed_at"])))

            @foreach($options as $option)
                <tr>
                    <td class="text-center">
                        <div class="custom-control custom-checkbox d-inline-block">
                            <input type="checkbox" class="custom-control-input option-check" value="{{ $option->id }}" @if(in_array($option->id, $this->selected_options)) checked="true" @endif>
                            <label class="custom-control-label"></label>
                        </div>
                    </td>
                    <td class="font-size-sm">
                        <p class="mb-0">
                            {{ $lang === "fr" ? $option->name : $option->name_en }}
                        </p>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="font-size-sm text-muted">{{ $option->getTranslation("single_price", $currency) }} {{ $currency }}</em>
                    </td>
                </tr>
            @endforeach

    @else

            @foreach($options as $option)
                <tr>
                    <td class="text-center">
                        <div class="custom-control custom-checkbox d-inline-block">
                            <p class="mb-0">
                                @if(in_array($option->id, $this->selected_options))
                                    <i class="fa fa-check-circle text-success"></i>
                                @else
                                    <i class="fa fa-times-circle text-danger"></i>
                                @endif
                            </p>
                        </div>
                    </td>
                    <td class="font-size-sm">
                        <p class="mb-0">
                            {{ $lang === "fr" ? $option->name : $option->name_en }}
                        </p>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="font-size-sm text-muted">{{ $option->getTranslation("single_price", $currency) }} {{ $currency }}</em>
                    </td>
                </tr>
            @endforeach

    @endif
    {{--@push('custom_scripts')
        <!-- Page JS Helpers (Table Tools helpers) -->
        <script>jQuery(function(){One.helpers(['table-tools-checkable', 'table-tools-sections']);});</script>
    @endpush--}}
        </tbody>
    </table>
</div>
