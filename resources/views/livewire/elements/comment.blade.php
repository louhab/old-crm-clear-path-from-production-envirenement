<div class="row" wire:key="{{ $canal }}" id="{{ str_replace(' ', '_', $canal) }}">
    <div class="col-sm-12">
        @if($comments->count())
        <?php
            $canal_list = [
                "soumission-admission-1",
                "soumission-admission-2",
                "soumission-admission-3",
                "soumission-admission-4",
                "soumission-admission-5",
                "soumission-Prise en charge",
                "soumission-Facture scolaire",
                "soumission-CAQ",
                "soumission-CPG",
                "soumission-Imm5645",
                "soumission-Imm5476",
            ];
            // get tasks
            $first = "";
            $second = "";
            if (in_array($canal, $canal_list)) {
                $tasks = $customer_case->comments()->where(function ($query) use ($canal) {
                    $query->where('canal', '=', $canal);
                })
                    ->join('users', 'users.id', '=', 'case_comments.user_id')
                    ->join('roles', 'roles.id', '=', 'users.role_id')
                    ->where(function ($query) {
                        $query->where('role_name', '=', "Admin")
                                ->orWhere('role_name', '=', "Conseiller")
                                ->orWhere('role_name', '=', "Manager");
                })->get(['case_comments.id', 'is_done as status', 'case_comments.comment', 'case_comments.created_at'])->toArray();

                $tmp = array_reverse($tasks);
                $first = array_pop($tmp);

                if ($first) {
                    if(!$first["status"]) {
                        $currentTime = \Carbon\Carbon::now();
                        $issueTime = Carbon\Carbon::parse($first["created_at"])->addHours(24);
                        if($currentTime->lte($issueTime)) {
                            // $first["url"] = "/tasks#pills-todo-today#:~:text=" . $first["comment"];
                            $first["url"] = "/tasks#pills-todo-today";
                        } else {
                            $first["url"] = "/tasks#pills-overdue-task";
                        }
                    }

                    // second task
                    $second = array_pop($tmp);
                    if ($second && !$second["status"]) {
                        $currentTime = \Carbon\Carbon::now();
                        $issueTime = \Carbon\Carbon::parse($second["created_at"])->addHours(12);

                        if($currentTime->lte($issueTime)) {
                            $second["url"] = "/tasks#pills-follow-up-today";
                        } else {
                            $second["url"] = "/tasks#pills-overdue-follow-up";
                        }
                    }
                }
            }
        ?>
            <div class="block block-rounded">
                <div class="block-content">
                    <div class="overflow-auto" style="max-height: 200px;">
                        @foreach($comments as $comment)
                            <table class="table table-striped table-borderless font-size-sm" wire:key="{{ $comment->id }}" data-lid="{{ $comment->id }}">
                                <tbody class="comment-show">
                                    <tr>
                                        <td>
                                            <strong>{{ $comment->author ? $comment->author->name: '-' }}</strong> {{ $comment->created_at }}
                                            @if(auth()->user()->isAdmin() || auth()->user()->isVerificateur() || $comment->user_id == auth()->id())
                                                <small class="font-size-sm text-muted float-right mr-2">
                                                    <a href="javascript:void(0)" class="delete-admission-comment mr-2">
                                                        <i class="fa fa-1x fa-times text-danger"></i>
                                                    </a>
                                                    <a href="javascript:void(0)" class="edit-comment">
                                                        <i class="si si-pencil text-info"></i>
                                                    </a>
                                                    @if($first && !$first["status"] && $comment->id === $first["id"])
                                                        <a href="{{ $first["url"] }}" class="edit-comment">
                                                            <i class="si si-check"></i>
                                                        </a>
                                                    @elseif($second && !$second["status"] && $comment->id === $second["id"])
                                                        <a href="{{ $second["url"] }}" class="edit-comment">
                                                            <i class="si si-check"></i>
                                                        </a>
                                                    @endif
                                                </small>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p style="white-space: pre-wrap;margin: 0;overflow-wrap: break-word;word-break: break-word;  hyphens: auto;">{{ $comment->comment ? $comment->comment: '-' }} </p>
                                        </td>
                                    </tr>
                                </tbody>
                                @if(auth()->user()->isAdmin() || auth()->user()->isVerificateur() || $comment->user_id == auth()->id())
                                    <tbody class="edit-comment-form" style="display: none;">
                                        <tr>
                                            <td>
                                                <form action="{{ route('update-comment', [$customer_case, $comment, 'canal' => $canal]) }}" method="POST">
                                                    @csrf
                                                    <div class="form-group">
                                                        <textarea name="comment" class="form-control form-control-alt">{{ $comment->comment ? $comment->comment: '-' }}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-sm btn-outline-danger">{{ __("lang.portal_about_the_lead_edit_button") }}</button>
                                                        <button type="button" class="btn btn-sm btn-outline-secondary edit-comment-reset">Annuler</button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                @endif
                            </table>
                        @endforeach
                    </div>
                </div>
            </div>
        @else
            <div class="block block-rounded">
                <div class="block-header block-header-rtl">
                    <h3 class="block-title"></h3>
                    <div class="block-options">
                        <div class="block-options-item text-warning">{{ __("lang.portal_no_comment") }}</div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="col-sm-12">
        <div class="block block-rounded add-comment-block">
            @if(!str_starts_with($canal, "soumission-"))
            <div class="block-header">
                <h3 class="block-title">{{ __("lang.portal_comments") }} {{ str_starts_with($canal, "soumission-")? ' '.str_replace("soumission-", "", $canal):'' }}</h3>
            </div>
            @endif
            <div class="block-content">
                <div class="row items-push">
                    <div class="col-md-12">
                        {{--{{ route('store-comment', [$customer_case, 'canal' => $canal]) }}--}}
                        <form action="" method="POST" onsubmit="return false;">
                            @csrf
                            <div class="form-group">
                                <textarea rows="2" name="comment" class="form-control form-control-alt" placeholder="{{ __('lang.portal_about_the_lead_write_comments') }}"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="view" value="{{ $currentRouteName }}">
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button type="button" class="btn btn-outline-danger btn-block add-comment-btn" data-canal="{{ $canal }}">{{ __("lang.portal_about_the_lead_save_button") }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('custom_scripts')
        <script>
            $('body').on('click', '.delete-admission-comment', function (event) {
                event.stopPropagation();
                event.stopImmediatePropagation();

                let cid = $(this).closest('.table').data('lid');
                // console.log(cid);
                Swal.fire({
                    title: "Etes vous sur?",
                    text: "Delete comment",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Oui, supprimer!",
                    cancelButtonText: "Annuler"
                }).then(result => {
                    if (result.isConfirmed) {
                        Livewire.emit('deleteAdmissionComment', cid);
                        //console.log(cid);
                    }
                });
            });
            $('body').on('click', '.edit-comment-reset', function () {
                $wrap = $(this).closest('.table');

                $wrap.find('.comment-show').show();
                $wrap.find('.edit-comment-form').hide();
            });
            $('body').on('click', '.edit-comment', function () {
                $wrap = $(this).closest('.table');

                $wrap.find('.comment-show').hide();
                $wrap.find('.edit-comment-form').show();
            });
        </script>
        <script>
            $('.add-comment-btn').on('click', function () {
                // api/v1/comments/7722/soumission-WES
                let canal = {!! json_encode($canal) !!};
                let customerCase = {!! json_encode($customer_case->id) !!};
                if (canal == $(this).data("canal")) {
                    //hide previous validation errors
                    /*userAddFormSelector.find('.invalid-feedback').hide();
                    userAddFormSelector.find('.form-control').removeClass('is-invalid');*/
                    let commentAddFormSelector = $(this).closest('form');
                    let commentAddBlockSelector = $(this).closest('.add-comment-block');

                    commentAddBlockSelector.addClass('block-mode-loading');
                    // console.log($(this).data("canal"));
                    $.ajax({
                        method: 'post',
                        url: `/api/v1/comments/${customerCase}/${canal}`,
                        data: commentAddFormSelector.serialize(),
                        success: () => {
                            Swal.fire(
                                'Ajout!',
                                'commentaire ajouté avec succés!',
                                'success'
                            ).then(() => {
                                Livewire.emit('updateAdmissionComment', canal);
                                commentAddBlockSelector.removeClass('block-mode-loading');
                                commentAddFormSelector.trigger('reset');
                            })
                        },
                        error: (xhr) => {

                            if(xhr.statusCode().status === 422) {

                                $.each(xhr.responseJSON.errors, function(key,value) {
                                    $(`#${canal.replace(/ /g,"_")} [name="${key}"]`).addClass('is-invalid');
                                    // $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                                });
                                commentAddBlockSelector.removeClass('block-mode-loading');
                            }
                            else
                                Swal.fire(
                                    'Ajout!',
                                    'erreur ajout comment!',
                                    'error'
                                ).then(() => {
                                    commentAddBlockSelector.removeClass('block-mode-loading');
                                })
                        }
                    });
                }
            });
        </script>
    @endpush
</div>
