<div>
    <!-- Horizontal Navigation - Hover Normal -->
    <div class="bg-white p-3 push">
        <!-- Toggle Navigation -->
        <div class="d-lg-none">
            <!-- Class Toggle, functionality initialized in Helpers.coreToggleClass() -->
            <button type="button" class="btn btn-block btn-light d-flex justify-content-between align-items-center" data-toggle="class-toggle" data-target="#horizontal-navigation-hover-normal" data-class="d-none">
                Menu - Hover Normal
                <i class="fa fa-bars"></i>
            </button>
        </div>
        <!-- END Toggle Navigation -->

        <!-- Navigation -->
        <div id="horizontal-navigation-hover-normal" class="d-none d-lg-block mt-2 mt-lg-0">
            <ul class="nav-main nav-main-horizontal nav-main-hover">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="be_ui_navigation_horizontal.html">
                        <i class="nav-main-link-icon fa fa-home"></i>
                        <span class="nav-main-link-name">Home</span>
                        <span class="nav-main-link-badge badge badge-pill badge-primary">5</span>
                    </a>
                </li>
                <li class="nav-main-heading">Personal</li>
                <li class="nav-main-item">
                    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                        <i class="nav-main-link-icon far fa-user"></i>
                        <span class="nav-main-link-name">Profile</span>
                    </a>
                    <ul class="nav-main-submenu">
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="javascript:void(0)">
                                <span class="nav-main-link-name">Edit</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="javascript:void(0)">
                                <span class="nav-main-link-name">Settings</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="javascript:void(0)">
                                <span class="nav-main-link-name">Log out</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- END Navigation -->
    </div>
    <!-- END Horizontal Navigation - Hover Normal -->
</div>
