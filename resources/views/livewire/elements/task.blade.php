<div class="block">
    @management
        <div class="block-header">
            <h3 class="block-title"><span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-info-light text-info">{{ str_replace(["soumission-", "course"] , ["", "admission"] , $canal) }}</span></h3>
            <div class="block-options">
                {{-- note yet done --}}
                @if ($task->is_done == 0 && $task->is_relaunched == 0 && $task->is_exists == 0)
                    {{-- first time (no relance btn) --}}
                    <button type="button" class="btn btn-sm btn-alt-success" id="add-{{  "tache-" . $customer_case->id }}" data-customer="{{ $customer_case->id }}" data-canal="{{ $canal }}" data-view="{{ $currentRouteName }}">A faire</button>
                @elseif ($task->is_done == 1 && $task->is_relaunched == 0 && $task->is_exists == 1)
                    {{-- done (relance) --}}
                    <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-warning-light text-warning"><b>{{ $task->note }}</b></span>
                    @if ($task->note == "Rejet fiche")
                        <button type="button" class="btn btn-sm btn-alt-success"  data-task="{{ $task->id }} " id="update-{{ "tache-" . $customer_case->id . "-" . $canal }}" data-customer="{{ $customer_case->id }}" data-canal="{{ $canal }}" data-view="{{ $currentRouteName }}" data-note="Relance">A faire</button>
                    @else
                        <button type="button" class="btn btn-sm btn-alt-danger" data-task="{{ $task->id }} " id="update-{{ "tache-" . $customer_case->id . "-" . $canal }}" data-customer="{{ $customer_case->id }}" data-canal="{{ $canal }}" data-view="{{ $currentRouteName }}" data-note="Relance">Relance</button>
                    @endif
                @else
                {{-- created but not done yet (refresh) --}}
                    @if (Carbon\Carbon::now()->lte(Carbon\Carbon::parse($task->created_at)->addHours(24)))
                        <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-info-light text-info"><b>à faire en cours</b></span>
                    @elseif (Carbon\Carbon::now()->gt(Carbon\Carbon::parse($task->created_at)->addHours(24)))
                        <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-warning-light text-warning"><b>Tache en retard</b></span>
                    @endif
                    <button type="button" class="btn btn-sm btn-alt-danger" data-task="{{ $task->id }} " id="update-{{ "tache-" . $customer_case->id . "-" . $canal }}" data-customer="{{ $customer_case->id }}" data-canal="{{ $canal }}" data-view="{{ $currentRouteName }}" data-note="Refresh">Refresh</button>
                @endif
            </div>
        </div>
    @endmanagement

    @backoffice
        {{-- note yet done --}}
        @if ($task->is_done == 0 && $task->is_relaunched == 0 && $task->is_exists == 0)
            {{-- first time (no relance btn) --}}
            {{-- nothing to show for the BO --}}
        @elseif ($task->is_done == 1 && $task->is_relaunched == 0 && $task->is_exists == 1)
            {{-- done (relance) --}}
            <div class="block-header">
                <h3 class="block-title"><span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-info-light text-info">{{ str_replace(["soumission-", "course"] , ["", "admission"] , $canal) }}</span></h3>
                <div class="block-options">
                    <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-success-light text-success"><b>Terminée</b></span>
                    {{-- <button type="button" class="btn btn-sm btn-alt-success" id="add-{{ "tache-" . $customer_case->id }}" data-customer="{{ $customer_case->id }}" data-canal="{{ $canal }}" data-view="{{ $currentRouteName }}">Relance</button> --}}
                </div>
            </div>
        @else
        <div class="block-header">
            <h3 class="block-title"><span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-info-light text-info">{{ str_replace(["soumission-", "course"] , ["", "admission"] , $canal) }}</span></h3>
            <div class="block-options">
            {{-- created but not done yet (refresh) --}}
                <button type="button" class="btn btn-sm btn-alt-success" data-task="{{ $task->id}} " id="update-{{ "tache-" . $customer_case->id . "-" . $canal }}" data-customer="{{ $customer_case->id }}" data-canal="{{ $canal }}" data-view="{{ $currentRouteName }}" data-note="C'est fait">C'est fait</button>
                <button type="button" class="btn btn-sm btn-alt-danger" data-task="{{ $task->id}} " id="update-{{ "tache-" . $customer_case->id . "-" . $canal }}" data-customer="{{ $customer_case->id }}" data-canal="{{ $canal }}" data-view="{{ $currentRouteName }}" data-note="Rejet fiche">Rejet fiche</button>
                <button type="button" class="btn btn-sm btn-alt-danger" data-task="{{ $task->id}} " id="update-{{ "tache-" . $customer_case->id . "-" . $canal }}" data-customer="{{ $customer_case->id }}" data-canal="{{ $canal }}" data-view="{{ $currentRouteName }}" data-note="Rejet document">Rejet document</button>
            </div>
        </div>
        @endif
    @endbackoffice
</div>
