<?php if ($media_name) {
    if ($customer_case->program_id === 4) {
        switch ($customer_case->customer->lead->currency_id) {
            case 2:
                $doc_name = 'CA-Devis-' . 'ECA' . '-EURO.pdf';
                break;
            default:
                $doc_name = 'CA-Devis-' . 'ECA' . '-MAD.pdf';
                break;
        }
    } elseif ($customer_case->program_id === 13) {
        switch ($customer_case->customer->lead->currency_id) {
            case 2:
                $doc_name = 'CA-Devis-' . 'QA' . '-MAD.pdf';
                break;
            default:
                $doc_name = 'CA-Devis-' . 'QA' . '-MAD.pdf';
                break;
        }
    } elseif ($customer_case->program_id === 22) {
        switch ($customer_case->customer->lead->currency_id) {
            case 2:
                $doc_name = 'CA-Devis-' . 'VT' . '-MAD.pdf';
                break;
            default:
                $doc_name = 'CA-Devis-' . 'VT' . '-MAD.pdf';
                break;
        }
    } else {
        switch ($customer_case->customer->lead->currency_id) {
            case 2:
                $doc_name = 'CA-Devis-' . 'EE' . '-EURO.pdf';
                break;
            default:
                $doc_name = 'CA-Devis-' . 'EE' . '-MAD.pdf';
                break;
        }
    }
} ?>
<div class="document-wrapper">
    @if (!isCustomer())
        @if ($media_name)
            <a class="btn btn-sm btn-light document-in" href="{{ asset('/pdf/' . $doc_name) }}" target="_blank">
                <i class="fa fa-fw fa-download text-success"></i>
            </a>
        @else
            <a class="btn btn-sm btn-light document-in"
                href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                <i class="fa fa-fw fa-download text-success"></i>
            </a>
            <a wire:click="deleteMedia('{{ $collectionName }}')" class="btn btn-sm btn-light document-in"
                href="javascript:void(0)">
                <i class="fa fa-fw fa-times text-danger"></i>
            </a>
            @if (
                (!$media->getCustomProperty('customer_owner') &&
                    (Illuminate\Support\Facades\Auth::user()->isAdmin() ||
                        Illuminate\Support\Facades\Auth::user()->isVerificateur() ||
                        Illuminate\Support\Facades\Auth::user()->isManager())) ||
                    Illuminate\Support\Facades\Auth::user()->isBackoffice())
                @if ($media->getCustomProperty('visible_for_customer'))
                    <a wire:click="toggleProperty({{ $media->id }})" data-property="visible_for_customer"
                        class="btn btn-sm btn-light document-in" href="javascript:void(0)">
                        <i class="fa fa-fw fa-eye text-success"></i>
                    </a>
                @else
                    <a wire:click="toggleProperty({{ $media->id }})" data-property="visible_for_customer"
                        class="btn btn-sm btn-light document-in" href="javascript:void(0)">
                        <i class="fa fa-fw fa-eye-slash text-danger"></i>
                    </a>
                @endif
            @endif
            @if ($signable)
                @if (!$media->getCustomProperty('is_signed') && strlen($media->getCustomProperty('pending_file_id')) > 1)
                    <p class="document-in">En attente de la signature client</p>
                @else
                    <a data-formId="form-{{ $formId . '1' }}" class="btn btn-light document-in sign-document"
                        href="javascript:void(0)">
                        <i class="fa fa-fw fa-pencil-alt text-primary"></i>
                        {{ getSigningLabel($media) }}
                    </a>
                @endif
            @endif
        @endif
    @else
        @if ($media_name)
            <a class="btn btn-sm btn-light document-in" href="{{ asset('/pdf/' . $doc_name) }}" target="_blank">
                <i class="fa fa-fw fa-download text-success"></i>
            </a>
        @else
            @if ($media->getCustomProperty('customer_owner'))
                <a class="btn btn-sm btn-light document-in"
                    href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                    <i class="fa fa-fw fa-download text-success"></i>
                </a>
                <a wire:click="deleteMedia('{{ $collectionName }}')" class="btn btn-sm btn-light document-in"
                    href="javascript:void(0)">
                    <i class="fa fa-fw fa-times text-danger"></i>
                </a>
            @else
                @if ($media->getCustomProperty('visible_for_customer'))
                    <a class="btn btn-sm btn-light document-in"
                        href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                        <i class="fa fa-fw fa-download text-success"></i>
                    </a>
                @endif
                @php $cc = App\Models\DocumentType::where("collection_name", $collectionName)->first(); @endphp
                @if ($cc && $cc->customer_owner)
                    <a wire:click="deleteMedia('{{ $collectionName }}')" class="btn btn-sm btn-light document-in"
                        href="javascript:void(0)">
                        <i class="fa fa-fw fa-times text-danger"></i>
                    </a>
                @endif
                @if ($signable)
                    @if ($media->getCustomProperty('is_signed') || strlen($media->getCustomProperty('pending_file_id')) > 1)
                        <a data-formId="form-{{ $formId . '1' }}" class="btn btn-light document-in sign-document"
                            href="javascript:void(0)" target="_blank">
                            <i class="fa fa-fw fa-pencil-alt text-primary"></i>
                            {{ getSigningLabel($media) }}
                        </a>
                    @endif
                @endif
            @endif
        @endif
    @endif
    @if ($signable)
        @if (!isCustomer())
            @if (!$media->getCustomProperty('is_signed') && $media->getCustomProperty('pending_file_id') > 0)
                <!--            <p>En attente de la signature client</p>-->
            @else
                <form id="form-{{ $formId . '1' }}"
                    action="{{ route(getSigningRouteName($media), ['customer' => $customer_case->customer, 'collectionName' => $collectionName]) }}"
                    method="POST" style="display: none;">
                    @csrf
                </form>
            @endif
        @else
            @if ($media->getCustomProperty('is_signed') || strlen($media->getCustomProperty('pending_file_id')) > 0)
                <form id="form-{{ $formId . '1' }}"
                    action="{{ route(getSigningRouteName($media), ['customer' => $customer_case->customer, 'collectionName' => $collectionName]) }}"
                    method="POST" style="display: none;">
                    @csrf
                </form>
            @endif
        @endif
    @endif
</div>
