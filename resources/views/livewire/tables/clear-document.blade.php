<table class="table table-borderless js-table-sections table-vcenter js-dataTable-full">
    <thead>
    <tr>
        {{--<th class="text-center" style="width: 100px;">ID</th>--}}
        <th style="width: 30px;"></th>
        @if($mediaSheet->id == 2)
            <th class="d-none d-md-table-cell" style="color: #d52b26;">Garant {{ $garant + 1 }}</th>
        @else
            <th class="d-none d-md-table-cell"></th>
        @endif
    </tr>
    </thead>
    @foreach($tags as $tag)
    <tbody class="show table-active">
    <tr>
        <td class="text-center">
            <i class="fa fa-angle-right text-muted toggle-medias" style="font-size: 17px;"></i>
        </td>
        <td class="d-none d-md-table-cell font-size-sm">
            <strong>{{ $tag }}</strong>
        </td>
    </tr>
    </tbody>
    <tbody>
        <tr>
            <td colspan="2">
                <table class="table table-vcenter js-dataTable-full" data-garant="{{ $garant }}">
                    <thead>
                    <tr class="bg-danger-light">
                        <th class="d-none d-md-table-cell" style="width: 840px;">Nom</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                        @php
                            $clear_fields = \App\Models\CustomerClear1000Fields::where('customer_id', $customerCase->customer->id)->first();
                            $visa_requests = \App\Models\CustomerVisaRequest::where('customer_id', $customerCase->customer_id)->orderBy('id', 'ASC')->get();
                            // dd($visa_requests);
                            // dd($docs);
                            $filtered = array_filter($docs, function ($item) use ($tag) {
                                $res_tags = array_map(function ($item) {
                                    return $item["tag_name"];
                                }, $item["tagged"]);
                                return in_array($tag, $res_tags);
                            });
                        @endphp
                        @foreach($filtered as $doc)
                            @php
                                $collection = $doc["collection_name"];
                                // $media = $customerCase->customer->getFirstMedia($collection);
                                $media_ = $customerCase->customer->getMedia($collection);
                                if (!is_null($garant)) {
                                    $medias = $customerCase->customer->getMedia($collection, ['garant' => "$garant"]);
                                    // dd($medias->count());
                                    if ($medias->count()) {
                                        $media_ = $medias;
                                    }

                                    // dd($media_);
                                    // else $media = null;
                                }
                                /*if ($tag == "Mère")
                                    dd($doc);*/
                                /*$res_tags = array_map(function ($item) {
                                    return $item["tag_name"];
                                }, $doc["tagged"]);*/
                                // dd($doc);
                            @endphp
                            {{--@if(in_array($tag, $res_tags))
                                @continue
                            @endif--}}
                            @if($doc["id"] == 158 && $clear_fields->visa_request != 82)
                                @continue
                            @endif
                            @if($doc["id"] == 159 && count($visa_requests) == 0)
                                @continue
                            @endif
                            @if($doc["id"] == 160 && $clear_fields->visa_request_result != 85)
                                @continue
                            @endif
                            @if($doc["id"] == 161 && $clear_fields->health != 4)
                                @continue
                            @endif
                            <tbody style="border-bottom: #a87d7d52 solid;">
                                @if(!count($media_))
                                <tr data-document="{{ $doc["id"] }}">
                                    <td class="d-none d-md-table-cell font-size-sm">
                                        <a href="javascript:void(0);">{{ $doc["name_fr"] }}</a>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-sm btn-outline-danger toggle-clear-modal">
                                            <label for="file-zD19jYEoTNRQ0VAsoRID9OyFSA9iwe"><svg width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path></svg></label>
                                            Upload
                                        </button>
                                    </td>
                                </tr>
                                @else
                                    @foreach ($media_ as $media)
                                    <tr data-document="{{ $doc["id"] }}">
                                    <td class="d-none d-md-table-cell font-size-sm">
                                        <a href="javascript:void(0);">{{ $doc["name_fr"] }}@if($customerCase->customer->getMedia($doc["collection_name"])->count() > 1) - {{ $loop->index + 1 }} @endif</a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-sm btn-outline-secondary text-success sheet-media-download" wire:click="downloadMedia({{ $media["id"] }})" href="javascript:void(0)">
                                        {{-- <a class="btn btn-sm btn-outline-secondary text-success sheet-media-download" wire:click="downloadMedia({{ $media->id }})" href="javascript:void(0)"> --}}
                                            <i class="fas fa-fw fa-download"></i>
                                        </a>
                                        <a class="btn btn-sm btn-outline-secondary text-info" href="{{ route((isCustomer() ? 'customer-':'') . 'view-media', ['media' => $media]) }}" target="_blank">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        @if(\Auth::guard('customer')->user() || \Auth::user()->isAdmin() || \Auth::user()->isVerificateur() || \Auth::user()->isManager() || \Auth::user()->isBackoffice() || \Auth::user()->isConseiller())
                                            {{-- <a class="btn btn-sm btn-outline-secondary text-danger media-delete" wire:click="removeMedia({{ $media->id }})" href="javascript:void(0);"> --}}
                                            <a class="btn btn-sm btn-outline-secondary text-danger media-delete" wire:click="removeMedia({{ $media["id"] }})" href="javascript:void(0);">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        @endforeach
                </table>
            </td>
        </tr>
    </tbody>
    @endforeach
    <tbody class="show table-active">
    <tr>
        <td class="text-center">
            <i class="fa fa-angle-right text-muted toggle-medias" style="font-size: 17px;"></i>
        </td>
        <td class="d-none d-md-table-cell font-size-sm">
            <strong>Documents paratagés</strong>
        </td>
    </tr>
    </tbody>
    <tbody>
    <tr>
        <td colspan="2">
            <table class="table table-vcenter js-dataTable-full" data-garant="{{ $garant }}">
                <thead>
                <tr class="bg-danger-light">
                    <th class="d-none d-md-table-cell" style="width: 840px;">Nom</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                @php
                    $clear_fields = \App\Models\CustomerClear1000Fields::where('customer_id', $customerCase->customer->id)->first();
                    $visa_requests = \App\Models\CustomerVisaRequest::where('customer_id', $customerCase->customer_id)->orderBy('id', 'ASC')->get();
                    // dd($visa_requests);
                @endphp
                @foreach($docsWithoutTags as $doc)
                    @php
                        $collection = $doc["collection_name"];
                        // if ($collection == "YvA9tG40vchtCR7kFVkHOQiEOwRIVH") {
                        //             $media = $customerCase->customer->getMedia($collection);
                        //             dd($media->count());
                        // }
                        $media_ = $customerCase->customer->getMedia($collection);
                        if (!is_null($garant)) {
                            $medias = $customerCase->customer->getMedia($collection, ['garant' => "$garant"]);
                            // dd($medias->count());
                            if ($medias->count()) {
                                $media_ = $medias;
                            }
                        }
                    @endphp
                    @if($doc["id"] == 158 && $clear_fields->visa_request != 82)
                        @continue
                    @endif
                    @if($doc["id"] == 159 && count($visa_requests) == 0)
                        @continue
                    @endif
                    @if($doc["id"] == 160 && $clear_fields->visa_request_result != 85)
                        @continue
                    @endif
                    @if($doc["id"] == 161 && $clear_fields->health != 4)
                        @continue
                    @endif
                        <tbody style="border-bottom: #a87d7d52 solid;">
                            @if(!count($media_))
                            {{-- @if(!isset($media_) && !count($media_)) --}}
                            <tr data-document="{{ $doc["id"] }}">
                                <td class="d-none d-md-table-cell font-size-sm">
                                    <a href="javascript:void(0);">{{ $doc["name_fr"] }}</a>
                                </td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-sm btn-outline-danger toggle-clear-modal">
                                        <label for="file-zD19jYEoTNRQ0VAsoRID9OyFSA9iwe"><svg width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path></svg></label>
                                        Upload
                                    </button>
                                </td>
                            </tr>
                            @else
                            @foreach ($media_ as $media)
                            <tr data-document="{{ $doc["id"] }}">
                                <td class="d-none d-md-table-cell font-size-sm">
                                    <a href="javascript:void(0);">{{ $doc["name_fr"] }}@if($customerCase->customer->getMedia($doc["collection_name"])->count() > 1) - {{ $loop->index + 1 }} @endif</a>
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-outline-secondary text-success sheet-media-download" wire:click="downloadMedia({{ $media["id"] }})" href="javascript:void(0)">
                                        <i class="fas fa-fw fa-download"></i>
                                    </a>
                                    <a class="btn btn-sm btn-outline-secondary text-info" href="{{ route((isCustomer() ? 'customer-':'') . 'view-media', ['media' => $media]) }}" target="_blank">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    @if(\Auth::guard('customer')->user() || \Auth::user()->isAdmin() || \Auth::user()->isVerificateur() || \Auth::user()->isManager() || \Auth::user()->isBackoffice() || \Auth::user()->isConseiller())
                                        <a class="btn btn-sm btn-outline-secondary text-danger media-delete" wire:click="removeMedia({{ $media["id"] }})" href="javascript:void(0);">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                                @endforeach
                            @endif
                        </tbody>
                @endforeach
            </table>
        </td>
    </tr>
    </tbody>
</table>
