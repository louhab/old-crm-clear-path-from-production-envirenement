@php
    // @if (!count($media_) || $doc['id'] === 68 || $doc['id'] === 260 || $doc['id'] === 259)
    $multiDocsIds = [262, 263, 266, 270];
    $clear13OldIds = [16, 17, 43, 46, 47, 49, 50, 51, 52, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 258, 259];
    $flag = false;
    
    /**
     *  Certificat de police (multi) => 259
     *  Études (diplômes ou grades universitaires) (multi) => bbmfHbxQZwwxe00PVQkbsTVTwL4Ji2 => 262
     *  Relevé d’emploi (multi) => wdzhmKsMSgH4OUFnsAwIBqIbXLKuTv
     *  Preuve de ressources financières suffisantes (single) => JO7h57sdX9iA7zTFI8QKxwiifTjQgm
     *  Photographie (single) => ebL7BSFLL6HuD4FH8msKUFAShe4SaE
     *  Passeports/titres de voyage (multi) => SjXrEhuFyBtBLXPvD7LsTWFOYcpmwN => 266
     *  Preuve d’examen médical préalable (single) => fkdJghrVHK9yO0FhpkxwTGIqShuF6j
     *  Certificat de mariage (single) => uPSq75B1Eirk5vCnJXYOlO1rdoMfBB
     *  Certificat de divorce (single) => YvfFStaDQMEvFdBXZgvS01LviGw1AX
     *  Preuve du lien de parenté (multi) => 4XK9TZ172q6LKTuluNhRuEzxi4AVgF => 270
     *  Preuve statut membre famille (multi) => H5RtSQqe2pW25KWu3EpVEAsIJdOG8j => 271
     */
    
@endphp
<table class="table table-borderless js-table-sections table-vcenter js-dataTable-full">
    <thead>
        <tr>
            <th style="width: 30px;"></th>
            @if ($mediaSheet->id == 2)
                <th class="d-none d-md-table-cell" style="color: #d52b26;">Garant {{ $garant + 1 }}</th>
            @else
                <th class="d-none d-md-table-cell"></th>
            @endif
        </tr>
    </thead>
    @foreach ($groupedDocuments as $groupId => $documents)
        <tbody class="show table-active">
            <tr>
                <td class="text-center">
                    <i class="fa fa-angle-right text-muted toggle-medias" style="font-size: 17px;"></i>
                </td>
                <td class="d-none d-md-table-cell font-size-sm">
                    @php $group = \App\Models\DocumentTypeGroup::find($groupId); @endphp
                    <strong>{{ count($groupedDocuments) == 1 ? 'Documents' : $group->name_fr }} </strong>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <td colspan="2">
                    <table class="table table-vcenter js-dataTable-full" data-garant="{{ $garant }}">
                        <thead>
                            <tr class="bg-danger-light">
                                <th class="d-none d-md-table-cell" style="width: 840px;">Nom</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        @php
                            $clear_fields = \App\Models\CustomerClear1000Fields::where('customer_id', $customerCase->customer->id)->first();
                            $visa_requests = \App\Models\CustomerVisaRequest::where('customer_id', $customerCase->customer_id)
                                ->orderBy('id', 'ASC')
                                ->get();
                        @endphp
                        @foreach ($documents as $doc)
                            @php
                                $collection = $doc['collection_name'];
                                $media_ = $customerCase->customer->getMedia($collection);
                                if ($garant != 0 || !is_null($garant)) {
                                    $medias = $customerCase->customer->getMedia($collection, ['garant' => "$garant"]);
                                    if ($medias->count()) {
                                        $media_ = $medias;
                                    } else {
                                        $media_ = [];
                                    }
                                }
                                if (!$flag && count($media_) && $mediaSheet->id == 9) {
                                    $flag = true;
                                }
                            @endphp
                            @if ($doc['id'] == 158 && $clear_fields->visa_request != 82)
                                @continue
                            @endif
                            @if ($doc['id'] == 159 && count($visa_requests) == 0)
                                @continue
                            @endif
                            @if ($doc['id'] == 160 && $clear_fields->visa_request_result != 85)
                                @continue
                            @endif
                            @if ($doc['id'] == 161 && $clear_fields->health != 4)
                                @continue
                            @endif
                            @if ((!in_array($doc['id'], $clear13OldIds) && $mediaSheet->id == 9) || $mediaSheet->id != 9)
                                <tbody style="border-bottom: #a87d7d52 solid;">
                                    @if (!count($media_) || in_array($doc['id'], $multiDocsIds))
                                        <tr data-document="{{ $doc['id'] }}">
                                            <td class="d-none d-md-table-cell font-size-sm">
                                                <a href="javascript:void(0);">{{ $doc['name_fr'] }}</a>
                                            </td>
                                            <td class="text-center">
                                                <button type="button"
                                                    class="btn btn-sm btn-outline-danger toggle-clear-modal">
                                                    <label for="file-zD19jYEoTNRQ0VAsoRID9OyFSA9iwe"><svg width="20"
                                                            height="17" viewBox="0 0 20 17">
                                                            <path
                                                                d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z">
                                                            </path>
                                                        </svg></label>
                                                    Upload
                                                </button>
                                            </td>
                                        </tr>
                                    @endif
                                    @if (count($media_))
                                        @foreach ($media_ as $media)
                                            <tr data-document="{{ $doc['id'] }}">
                                                <td class="d-none d-md-table-cell font-size-sm">
                                                    <a href="javascript:void(0);">{{ $doc['name_fr'] }}@if ($customerCase->customer->getMedia($doc['collection_name'])->count() > 1)
                                                            - {{ $loop->index + 1 }}
                                                        @endif
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    <a class="btn btn-sm btn-outline-secondary text-success sheet-media-download"
                                                        wire:click="downloadMedia({{ $media['id'] }})"
                                                        href="javascript:void(0)">
                                                        {{-- <a class="btn btn-sm btn-outline-secondary text-success sheet-media-download" wire:click="downloadMedia({{ $media->id }})" href="javascript:void(0)"> --}}
                                                        <i class="fas fa-fw fa-download"></i>
                                                    </a>
                                                    <a class="btn btn-sm btn-outline-secondary text-info"
                                                        href="{{ route((isCustomer() ? 'customer-' : '') . 'view-media', ['media' => $media]) }}"
                                                        target="_blank">
                                                        <i class="fas fa-eye"></i>
                                                    </a>
                                                    @if (
                                                        \Auth::guard('customer')->user() ||
                                                            \Auth::user()->isAdmin() ||
                                                            \Auth::user()->isVerificateur() ||
                                                            \Auth::user()->isManager() ||
                                                            \Auth::user()->isBackoffice() ||
                                                            \Auth::user()->isConseiller())
                                                        {{-- <a class="btn btn-sm btn-outline-secondary text-danger media-delete" wire:click="removeMedia({{ $media->id }})" href="javascript:void(0);"> --}}
                                                        <a class="btn btn-sm btn-outline-secondary text-danger media-delete"
                                                            wire:click="removeMedia({{ $media['id'] }})"
                                                            href="javascript:void(0);">
                                                            <i class="fas fa-trash"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            @endif
                        @endforeach
                    </table>
                </td>
            </tr>
        </tbody>
    @endforeach
</table>
@if ($flag && $mediaSheet->id == 9)
    <table class="table table-borderless js-table-sections table-vcenter js-dataTable-full">
        <thead>
            <tr>
                <th style="width: 30px;"></th>
                @if ($mediaSheet->id == 2)
                    <th class="d-none d-md-table-cell" style="color: #d52b26;">Garant {{ $garant + 1 }}</th>
                @else
                    <th class="d-none d-md-table-cell"></th>
                @endif
            </tr>
        </thead>
        @foreach ($groupedDocuments as $groupId => $documents)
            <tbody class="show table-active">
                <tr>
                    <td class="text-center">
                        <i class="fa fa-angle-right text-muted toggle-medias" style="font-size: 17px;"></i>
                    </td>
                    <td class="d-none d-md-table-cell font-size-sm">
                        @php $group = \App\Models\DocumentTypeGroup::find($groupId); @endphp
                        <strong>Documents</strong>
                    </td>
                </tr>
            </tbody>
            <tbody>
                <tr>
                    <td colspan="2">
                        <table class="table table-vcenter js-dataTable-full" data-garant="{{ $garant }}">
                            <thead>
                                <tr class="bg-danger-light">
                                    <th class="d-none d-md-table-cell" style="width: 840px;">Nom</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            @php
                                $clear_fields = \App\Models\CustomerClear1000Fields::where('customer_id', $customerCase->customer->id)->first();
                                $visa_requests = \App\Models\CustomerVisaRequest::where('customer_id', $customerCase->customer_id)
                                    ->orderBy('id', 'ASC')
                                    ->get();
                            @endphp
                            @foreach ($documents as $doc)
                                @php
                                    $collection = $doc['collection_name'];
                                    $media_ = $customerCase->customer->getMedia($collection);
                                    if ($garant != 0 || !is_null($garant)) {
                                        $medias = $customerCase->customer->getMedia($collection, ['garant' => "$garant"]);
                                        if ($medias->count()) {
                                            $media_ = $medias;
                                        } else {
                                            $media_ = [];
                                        }
                                    }
                                    
                                    if (!$flag && count($media_) && $mediaSheet->id == 9) {
                                        $flag = true;
                                    }
                                @endphp
                                @if ($doc['id'] == 158 && $clear_fields->visa_request != 82)
                                    @continue
                                @endif
                                @if ($doc['id'] == 159 && count($visa_requests) == 0)
                                    @continue
                                @endif
                                @if ($doc['id'] == 160 && $clear_fields->visa_request_result != 85)
                                    @continue
                                @endif
                                @if ($doc['id'] == 161 && $clear_fields->health != 4)
                                    @continue
                                @endif
                                <tbody style="border-bottom: #a87d7d52 solid;">
                                    @if (count($media_))
                                        @foreach ($media_ as $media)
                                            <tr data-document="{{ $doc['id'] }}">
                                                <td class="d-none d-md-table-cell font-size-sm">
                                                    <a href="javascript:void(0);">{{ $doc['name_fr'] }}@if ($customerCase->customer->getMedia($doc['collection_name'])->count() > 1)
                                                            - {{ $loop->index + 1 }}
                                                        @endif
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    <a class="btn btn-sm btn-outline-secondary text-success sheet-media-download"
                                                        wire:click="downloadMedia({{ $media['id'] }})"
                                                        href="javascript:void(0)">
                                                        {{-- <a class="btn btn-sm btn-outline-secondary text-success sheet-media-download" wire:click="downloadMedia({{ $media->id }})" href="javascript:void(0)"> --}}
                                                        <i class="fas fa-fw fa-download"></i>
                                                    </a>
                                                    <a class="btn btn-sm btn-outline-secondary text-info"
                                                        href="{{ route((isCustomer() ? 'customer-' : '') . 'view-media', ['media' => $media]) }}"
                                                        target="_blank">
                                                        <i class="fas fa-eye"></i>
                                                    </a>
                                                    @if (
                                                        \Auth::guard('customer')->user() ||
                                                            \Auth::user()->isAdmin() ||
                                                            \Auth::user()->isVerificateur() ||
                                                            \Auth::user()->isManager() ||
                                                            \Auth::user()->isBackoffice() ||
                                                            \Auth::user()->isConseiller())
                                                        <a class="btn btn-sm btn-outline-secondary text-danger media-delete"
                                                            wire:click="removeMedia({{ $media['id'] }})"
                                                            href="javascript:void(0);">
                                                            <i class="fas fa-trash"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            @endforeach
                        </table>
                    </td>
                </tr>
            </tbody>
        @endforeach
    </table>
@endif
