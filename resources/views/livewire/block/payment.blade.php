<?php
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
?>
<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title">
            @php $currency = \App\Models\Currency::find($customer_case->customer->currency_id)->iso; @endphp
            <i class="fa fa-2x fa-dollar-sign me-1"></i> {{ __('lang.portal_payment_title') }} {{ $payment }} :

            @if (count($pms))
                <span class="alert alert-success py-1 mb-0 mr-1">
                    <i class="fa fa-gift mr-1" style="font-size: 12px;"></i> <strong>Réduction</strong>
                    {{ $pms[0]->discount }}%
                </span>
                <span class="alert alert-danger py-1 mb-0">
                    {{ $pms[0]->amount . ' ' . $currency }}
                </span>
            @endif
        </h3>
        @if (auth()->user()->isAdmin() ||
                auth()->user()->isVerificateur() ||
                auth()->user()->isManager() ||
                $hasContrat)
            <div class="block-options">
                @if ($case_state->status && $paymentMethod)
                    <div class="block-options-item">
                        <span class="badge badge-success"> {{ __('lang.portal_payment_made_by') }}
                            {{ $lang === 'fr' ? $paymentMethod->method_label_fr : $paymentMethod->method_label_en }}</span>
                    </div>
                    <button type="button" class="btn btn-sm btn-outline-danger" wire:click="delete">
                        {{ __('lang.portal_personal_infos_cancel_button') }}</button>
                @else
                    <button type="button" class="btn btn-sm btn-alt-primary method-modal-btn"
                        data-case="{{ $case_state->id }}" data-paiement="{{ $payment }}">
                        {{ __('lang.portal_make_payment_btn') }}
                    </button>
                @endif
            </div>
        @else
            <div class="block-options">
                <div class="block-options-item">
                    <i class="fa fa-fw fa-lock text-danger"></i>
                </div>
            </div>
        @endif
    </div>
    @if ($case_state->status)
        <div class="block-content p-3">
            @if ($paymentMethod)
                <table class="table table-sm table-vcenter">
                    <thead>
                        <tr class="calls">
                            <th class="d-none d-sm-table-cell" style="width: 30%;">{{ __('lang.portal_doc_type') }}
                            </th>
                            <th>{{ __('lang.portal_doc_creation_date') }}</th>
                            <th class="d-none d-sm-table-cell" style="width: 30%;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!empty($recu['media']))
                            <tr>
                                <td class="font-size-sm">
                                    <p class="text-muted mb-0">
                                        {{ $lang === 'fr' ? $recu['doc']->name_fr : $recu['doc']->name_en }}</p>
                                </td>
                                <td class="font-size-sm">
                                    <p class="text-muted mb-0">{{ $recu['media']->created_at }}</p>
                                </td>
                                <td class="font-size-sm"> @include('cases.steps.partials.document', [
                                    'document' => $recu['doc']->collection_name_fr,
                                    'collectionName' => $recu['collection'],
                                    'media' => $recu['media'],
                                ])</td>
                            </tr>
                        @endif
                        @if (!empty($recu['media']) && $payment !== 6)
                            <tr>
                                <td class="font-size-sm">
                                    <p class="text-muted mb-0">
                                        {{ $lang === 'fr' ? $bill['doc']->name_fr : $bill['doc']->name_en }}</p>
                                </td>
                                <td class="font-size-sm">
                                    <p class="text-muted mb-0">{{ $bill['doc']->created_at }}</p>
                                </td>
                                <td class="font-size-sm"> @include('cases.steps.partials.document', [
                                    'document' => $bill['doc']->collection_name_fr,
                                    'collectionName' => $bill['collection'],
                                    'media' => $bill['media'],
                                    'customer_case' => $customer_case,
                                    'media_name' => $bill['doc']->name_fr,
                                ])</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            @else
                <div class="text-warning">Payment Method Missing</div>
            @endif
        </div>
    @endif

    <div class="block-content p-3">
        @livewire('button.paiement', ['customer_case' => $customer_case, 'collection' => 'Paiement ' . $payment])
    </div>
    @livewire('elements.options', ['customer_case' => $customer_case, 'options' => $options, 'payment' => $payment], key($payment))
</div>
