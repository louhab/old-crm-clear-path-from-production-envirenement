<div class="row row-deck">
    @if (count($payments_stats))
        @foreach ($payments_stats as $key => $value)
        <div class="col-sm-4 col-xl-2">
            <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="10">
                <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                    <dl class="mb-0">
                        <dt class="font-size-h2 font-w700 dashboard-analytics-status_10">{{ $value->total_amount_payment}}</dt>
                        <dd class="text-muted mb-0">Total en {{ $value->iso}} </dd>
                    </dl>
                </div>
            </div>
        </div>
        @endforeach
    @else
        <div class="col-sm-4 col-xl-2">
            <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="10">
                <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                    <dl class="mb-0">
                        <dt class="font-size-h2 font-w700 dashboard-analytics-status_10">0</dt>
                        <dd class="text-muted mb-0">Total en MAD</dd>
                    </dl>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-xl-2">
            <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="10">
                <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                    <dl class="mb-0">
                        <dt class="font-size-h2 font-w700 dashboard-analytics-status_10">0</dt>
                        <dd class="text-muted mb-0">Total en EUR</dd>
                    </dl>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-xl-2">
            <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="10">
                <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                    <dl class="mb-0">
                        <dt class="font-size-h2 font-w700 dashboard-analytics-status_10">0</dt>
                        <dd class="text-muted mb-0">Total en USD</dd>
                    </dl>
                </div>
            </div>
        </div>
    @endif
    <div class="col-sm-4 col-xl-2">
        <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="10">
            <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                <dl class="mb-0">
                    <dt class="font-size-h2 font-w700 dashboard-analytics-status_10">{{ $calls_stats}}</dt>
                    <dd class="text-muted mb-0">Total des Appels</dd>
                </dl>
            </div>
        </div>
    </div>

    <div class="col-sm-4 col-xl-2">
        <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status"   data-value="10">
            <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                <dl class="mb-0">
                    <dt class="font-size-h2 font-w700 dashboard-analytics-status_10">{{ $total_meets }}</dt>
                    <dd class="text-muted mb-0">Total des Rencontres {{ "(total)" }} </dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="col-sm-4 col-xl-2">
        <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status"   data-value="10">
            <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                <dl class="mb-0">
                    <dt class="font-size-h2 font-w700 dashboard-analytics-status_10">{{ $office_meets }}</dt>
                    <dd class="text-muted mb-0">Total des Rencontres Bureau</dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="col-sm-4 col-xl-2">
        <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status"   data-value="10">
            <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                <dl class="mb-0">
                    <dt class="font-size-h2 font-w700 dashboard-analytics-status_10">{{ $visio_meets }}</dt>
                    <dd class="text-muted mb-0">Total des Rencontres Visio</dd>
                </dl>
            </div>
        </div>
    </div>
</div>
