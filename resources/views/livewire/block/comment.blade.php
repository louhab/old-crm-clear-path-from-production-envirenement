@if($hidden)
    <div class="block block-rounded js-ecom-div-cart d-xl-block" id="comments-block">
        <div class="block-header block-header-default">
            <h3 class="block-title">
                {{ $canal === null ? __("lang.portal_about_the_lead_comments") : __("lang.portal_about_the_lead_backoffice_notesNotes") }}
            </h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" id="refresh-comment-btn">
                    <i class="si si-refresh"></i>
                </button>
            </div>
        </div>
        <div class="block-content" style="padding: 10px;">
            <div id="main-comment-container" data-comment-id="{{ $customer_case->id }}" class="block-content overflow-auto" style="height: 100%;max-height: 40em;">
                @if($currentRouteName == "soumission-view" || $canal!="soumission")
            <div class="row items-push">
                <div class="col-md-12">
                    <form action="{{ route('store-comment', [$customer_case, 'canal' => ($canal!=null? 'soumission': null)]) }}" method="POST">
                        @csrf
                        <input type="hidden" name="view" value="{{ $currentRouteName }}">
                        <div class="form-group">
                            <textarea id="comment" name="comment" class="form-control form-control-alt" placeholder="{{ $canal == null ? __("lang.portal_about_the_lead_write_comments") : __("lang.portal_about_the_lead_backoffice_write_notes") }}"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-danger btn-block">{{ __("lang.portal_about_the_lead_save_button") }}</button>
                        </div>
                    </form>
                </div>
            </div>
                @endif

            <div id="comments-wrapper">
                @foreach($comments as $comment)
                    <div class="media push comment-wrapper" data-lid="{{ $comment->id }}" style="border:1px solid #e8e8e8; padding: 5px;margin-bottom: 6px;">
                        <div class="media-body comment-show">
                            <p style="white-space: pre-wrap;margin: 0;"><strong>{{ $comment->author ? $comment->author->name: '-' }}</strong> - <u>{{ $comment->created_at }}</u></p>
                            <p style="white-space: pre-wrap;margin: 0;display: inline-block;word-break: break-word;">{{ $comment->comment ? $comment->comment: '-' }} </p>
                            @if(auth()->user()->isAdmin() || auth()->user()->isVerificateur() || $comment->user_id == auth()->id())
                                <small class="font-size-sm text-muted float-right">
                                    <a href="javascript:void(0)" class="delete-comment" data-id>
                                        <i class="fa fa-1x fa-times text-danger"></i>supprimer
                                    </a>
                                    <a href="javascript:void(0)" class="edit-comment">
                                        <i class="si si-pencil text-info"></i>modifier
                                    </a>
                                </small>
                            @endif
                        </div>
                        @if(auth()->user()->isAdmin() || auth()->user()->isVerificateur() || $comment->user_id == auth()->id())
                            <div class="media-body edit-comment-form" style="display: none;">
                                <form action="{{ route('update-comment', [$customer_case, $comment, 'canal' => ($canal!=null? 'soumission': null)]) }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <textarea id="comment" name="comment" class="form-control form-control-alt" placeholder="{{ $canal == null ? __("lang.portal_about_the_lead_write_comments") : __("lang.portal_about_the_lead_backoffice_write_notes") }}">{{ $comment->comment ? $comment->comment: '-' }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-sm btn-outline-danger">{{ __("lang.portal_about_the_lead_edit_button") }}</button>
                                        <button type="button" class="btn btn-sm btn-outline-secondary edit-comment-reset">Annuler</button>
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>

            <div class="media-body" @if(count($comments) == 0 || count($comments) == $total) style="display: none;" @endif>
                <button wire:click="loadMore" type="button" class="btn btn-outline-danger btn-block">Charger anciens messages</button>
            </div>
        </div>
        </div>
    </div>
    @push('custom_scripts')
        <script>
            $('body').on('click', '.delete-comment', function () {
                event.stopPropagation();
                event.stopImmediatePropagation();
                let cid = $(this).closest('.comment-wrapper').data('lid');
                Swal.fire({
                    title: "Etes vous sur?",
                    text: "Delete comment",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Oui, supprimer!",
                    cancelButtonText: "Annuler"
                }).then(result => {
                    if (result.isConfirmed) {
                        Livewire.emit('deleteComment', cid);
                    }
                });
            });
            $('body').on('click', '.edit-comment-reset', function () {
                $wrap = $(this).closest('.comment-wrapper');

                $wrap.find('.comment-show').show();
                $wrap.find('.edit-comment-form').hide();
            });
            $('body').on('click', '.edit-comment', function () {
                $wrap = $(this).closest('.comment-wrapper');

                $wrap.find('.comment-show').hide();
                $wrap.find('.edit-comment-form').show();
            });
        </script>
    @endpush
@endif
