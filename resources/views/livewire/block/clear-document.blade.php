<div class="block block-rounded" style="margin: 7px 50px;">
    <div class="block-content">
        <div class="table-responsive">
            {{-- @livewire('wizard.media', ['customerCase' => $customerCase, 'mediaSheet' => $mediaSheet]) --}}
            @if ($mediaSheet->id == 2)
                @livewire('tables.clear-document-v2', ['customerCase' => $customerCase, 'mediaSheet' => $mediaSheet, 'garant' => 0])
                @if (count($garants))
                    @foreach ($garants as $garant)
                        @livewire('tables.clear-document-v2', ['customerCase' => $customerCase, 'mediaSheet' => $mediaSheet, 'garant' => $loop->index + 1], key($loop->index + 1))
                    @endforeach
                @endif
            @else
                @livewire('tables.clear-document-v2', ['customerCase' => $customerCase, 'mediaSheet' => $mediaSheet])
            @endif
        </div>
        @if (!isCustomer() && (auth()->user()->id == 7 || auth()->user()->id == 6 || auth()->user()->id == 61))
            <ul class="list-group push">
                @php
                    $cc = \App\Models\Customer::with('media')
                        ->where('id', $customerCase->customer_id)
                        ->first()
                        ->toArray();
                @endphp
                @foreach ($cc['media'] as $cm)
                    @if (!str_starts_with($cm['collection_name'], 'Paiement'))
                        @php
                            $cd = \App\Models\DocumentType::where('collection_name', $cm['collection_name'])->first();
                        @endphp
                        @if ($cd)
                            @php $cd = $cd->toArray(); @endphp
                        @else
                            @continue
                        @endif
                        <li class="list-group-item">
                            {{ $cd['name_fr'] }}
                            <a class="btn btn-sm btn-outline-secondary text-success sheet-media-download"
                                wire:click="downloadMedia({{ $cm['id'] }})" href="javascript:void(0)">
                                <i class="fas fa-fw fa-download"></i>
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        @endif
    </div>
</div>
