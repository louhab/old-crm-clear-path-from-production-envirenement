<?php
//sub_step_state_id
$case_state_1 = App\Models\CaseState::where('sub_step_state_id', 3)
    ->where('customer_case_id', $customer_case->id)
    ->where('payment_method', 1)
    ->first();
?>

<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title">
            {{ __('lang.portal_personal_infos_title') }}
        </h3>
        <div class="block-options">
            @if ($customer_errors)
                <button type="button" id="toggle-lead-form"
                    class="btn btn-sm btn-outline-secondary">{{ __('lang.portal_personal_infos_cancel_button') }}</button>
            @else
                @administration
                    <button type="button" id="toggle-lead-form"
                        class="btn btn-sm btn-alt-primary">{{ __('lang.portal_personal_infos_edit_button') }}</button>
                @endadministration
                @conseiller
                    @if ($customer_case->program_step_id >= 2)
                        <button type="button" id="toggle-lead-form"
                            class="btn btn-sm btn-alt-primary">{{ __('lang.portal_personal_infos_edit_button') }}</button>
                    @endif
                @endconseiller
            @endif
        </div>
    </div>
    <div class="block-content" style="padding: 20px;">
        @administration
            <div id="lead-form" @if (!$customer_errors) style="display:none;" @endif class="pr-5">
                @include('cases.steps.partials.form-fields', ['form' => $substep])
            </div>
        @endadministration
        @conseiller
            <div id="lead-form" @if (!$customer_errors) style="display:none;" @endif class="pr-5">
                @include('cases.steps.partials.form-fields', ['form' => $substep])
            </div>
        @endconseiller
        <div id="lead-info" @if ($customer_errors) style="display:none;" @endif>
            <table class="table table-sm table-vcenter table-responsive">
                <tbody>
                    <tr>
                        <td style="width: 50%">{{ __('lang.portal_personal_infos_surname') }} : <strong>
                                {{ $customer_case->customer->lastname }}</strong></td>
                        @administration
                            <td>{{ __('lang.portal_personal_infos_phone') }} :
                                <strong>{{ $customer_case->customer->phone }}</strong>
                            </td>
                        @endadministration
                    </tr>
                    <tr>
                        <td>{{ __('lang.portal_personal_infos_name') }} :
                            <strong>{{ $customer_case->customer->firstname }}</strong>
                        </td>
                        @administration
                            <td>{{ __('lang.portal_personal_infos_advisor') }} :
                                <strong>{{ $conseiller ? $conseiller->name : 'Compte Supprimer' }}</strong>
                            </td>
                        @endadministration
                    </tr>
                    @administration
                        @if (
                            $customer_case->customer->program_id == 4 ||
                                $customer_case->customer->program_id == 31 ||
                                $customer_case->customer->program_id == 32)
                            <tr>
                                <td>Téléphone père :
                                    <strong>{{ is_null($customer_case->customer->father_phone) || empty($customer_case->customer->father_phone) ? '-' : $customer_case->customer->father_phone }}</strong>
                                </td>
                                <td>Téléphone mère :
                                    <strong>{{ is_null($customer_case->customer->mother_phone) || empty($customer_case->customer->mother_phone) ? '-' : $customer_case->customer->mother_phone }}</strong>
                                </td>
                            </tr>
                        @endif
                    @endadministration
                    <tr>
                        <td>{{ __('lang.portal_personal_infos_email') }} : <strong
                                style="word-break: break-all;">{{ $customer_case->customer->email }}</strong></td>
                        @administration
                            <td>
                                @if (is_null($support))
                                    Support : <strong>-</strong>
                                @else
                                    Support : <strong>{{ $support->name }}</strong>
                                @endif
                            </td>
                        @endadministration
                    </tr>
                    <tr>
                        <td>{{ __('lang.portal_personal_infos_dob') }} :
                            <strong>{{ $customer_case->customer->birthday }}</strong>
                        </td>

                        <?php
                        $programm_name = \App\Models\Program::where('id', $customer_case->customer->lead->program_id)->first();
                        ?>
                        <td>{{ __('lang.portal_personal_infos_programm') }} :
                            <strong>{{ $lang === 'fr' ? $programm_name->name : $programm_name->labelEng }}</strong>
                        </td>
                    </tr>





                    <tr>
                        @if (!empty($case_state_1))
                            <?php
                            $customer_country = \App\Models\Country::find($customer_case->customer->country_id);
                            ?>
                            <td>
                                @if ($customer_case->customer->country_id)
                                    {{ __('lang.portal_personal_infos_country') }} :
                                    <strong>{{ $lang === 'en' ? $customer_country->name_fr_fr : $customer_country->name_en_gb }}</strong>
                                @else
                                    {{ __('lang.portal_personal_infos_country') }} : <strong> - </strong>
                                @endif
                            </td>
                        @else
                            @administration
                                <?php
                                $customer_country = \App\Models\Country::find($customer_case->customer->country_id);
                                ?>
                                <td>
                                    @if ($customer_case->customer->country_id)
                                        {{ __('lang.portal_personal_infos_country') }} :
                                        <strong>{{ $lang === 'en' ? $customer_country->name_fr_fr : $customer_country->name_en_gb }}</strong>
                                    @else
                                        {{ __('lang.portal_personal_infos_country') }} : <strong> - </strong>
                                    @endif
                                </td>
                            @endadministration


                        @endif
                        <td>
                            Type de rencontre : <strong>{{ $customer_case->customer->lead->to_office }}</strong>
                            @if ($customer_case->customer->lead->to_office == 'office')
                                <a href="javascript:void(0)" class="toggle-meet-type"><i
                                        class="si si-refresh mr-1"></i>Changer en conference</a>
                            @else
                                <a href="javascript:void(0)" class="toggle-meet-type"><i
                                        class="si si-refresh mr-1"></i>Changer en office</a>
                            @endif
                        </td>
                    </tr>



                    <tr>

                        @if (!empty($case_state_1))
                            @php $city2 = \App\Models\Cities::find($customer_case->customer->lead->city2); @endphp
                            <td>
                                {{ __('lang.portal_personal_infos_city') }} :
                                @if ($customer_case->customer->lead->country_id == 144 && $customer_case->customer->lead->city2)
                                    <strong>{{ $city2->city }}</strong>
                                @else
                                    <strong>{{ $customer_case->customer->city ?: '-' }}</strong>
                                @endif
                            </td>
                        @else
                            @administration
                                @php $city2 = \App\Models\Cities::find($customer_case->customer->lead->city2); @endphp
                                <td>
                                    {{ __('lang.portal_personal_infos_city') }} :
                                    @if ($customer_case->customer->lead->country_id == 144 && $customer_case->customer->lead->city2)
                                        <strong>{{ $city2->city }} </strong>
                                    @else
                                        <strong>{{ $customer_case->customer->city ?: '-' }}</strong>
                                    @endif
                                </td>
                            @endadministration


                        @endif


                        <td>
                            @if ($customer_case->customer->lead->program_field)
                                @php $program_field = \App\Models\ProgramField::find($customer_case->customer->lead->program_field); @endphp
                                @if (
                                    $customer_case->customer->program_id == 4 ||
                                        $customer_case->customer->program_id == 31 ||
                                        $customer_case->customer->program_id == 32)
                                    {{ __('lang.portal_personal_infos_study_field') }}
                                    <strong>{{ $lang === 'fr' ? $program_field->label_fr : $program_field->labelEng }}</strong>
                                @else
                                    {{ __('lang.portal_personal_infos_work_field') }}
                                    <strong>{{ $lang === 'fr' ? $program_field->label_fr : $program_field->labelEng }}</strong>
                                @endif
                            @else
                                {{ __('lang.portal_personal_infos_field') }} : <strong>-</strong>
                            @endif
                        </td>
                    </tr>

                    @administration
                        <tr>
                            <td>@livewire('button.media', ['customer_case' => $customer_case, 'collection' => 'Cin recto'])</td>
                            <td>@livewire('button.media', ['customer_case' => $customer_case, 'collection' => 'CV'])</td>
                        </tr>
                        <tr>
                            <td> @livewire('button.media', ['customer_case' => $customer_case, 'collection' => 'Cin verso'])</td>
                        </tr>
                    @endadministration
                </tbody>
            </table>
        </div>
    </div>
    @push('custom_scripts')
        <script>
            //Toggle Meet Type
            $("body").on("click", ".toggle-meet-type", function() {
                Swal.fire({
                    title: "êtes-vous sûr de vouloir changer de type de rencontre ?",
                    text: "L'opération est irréversible!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Oui",
                    cancelButtonText: "Non"
                }).then(result => {
                    if (result.isConfirmed) {
                        Livewire.emit('toggleMeetType');
                    }
                });
            });
        </script>
    @endpush
</div>
