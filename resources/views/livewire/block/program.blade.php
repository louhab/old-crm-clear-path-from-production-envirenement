<div class="block block-rounded">
    <div class="block-header block-header-default">
        @if ($customer_case->program_id == 4)
            <h3 class="block-title"><i class="fa fa-satellite"></i> Soumission de la Demande d’Admission</h3>
        @else
            <h3 class="block-title"><i class="fa fa-satellite"></i> Soumission du profil
                {{ \App\Models\Program::find($customer_case->program_id)->name }}</h3>
        @endif
    </div>
    @if ($customer_case->program_id == 4)
        @if (count(
                $customer_case->customer->schools()->withPivot('admis')->get()))
            @foreach ($customer_case->customer->schools()->withPivot('admis')->get() as $program)
                @php
                    $docs = ['Lettre modèle', 'Accusé de réception', 'Reçu', 'Lettre de suivi', "Lettre d'acceptation", 'Lettre de refus'];
                    $show_admission = false;
                    foreach ($docs as $doc) {
                        $collectionName = $doc . '-admission-' . $program->id;
                        $media = $customer_case->customer->getFirstMedia($collectionName);
                        if (!empty($media)) {
                            $show_admission = true;
                            break;
                        }
                    }
                @endphp
                @if ($show_admission)
                    <div class="block-content row">
                        <div class="col-md-6">
                            <div class="block block-rounded">
                                <div class="block-header">
                                    <h3 class="block-title">
                                        <div class="alert alert-warning py-2 mb-0">
                                            <i class="fa fa-exclamation-triangle mr-1"></i> Deprecated!
                                        </div>
                                        Admission {{ $loop->index + 1 }}: {{ $program->programme }}
                                        <small>{{ $program->nom_etablissement }}</small>
                                    </h3>
                                    <div class="block-options">
                                        <a type="button" class="btn btn-sm btn-alt-danger" href="{{ $program->lien }}"
                                            target="_blank">Lien programme</a>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <table class="table table-vcenter">
                                        @php
                                            $docs = ['Lettre modèle', 'Accusé de réception', 'Reçu', 'Lettre de suivi', "Lettre d'acceptation", 'Lettre de refus'];
                                            // dd(Illuminate\Support\Facades\Route::currentRouteName());
                                        @endphp
                                        @foreach ($docs as $doc)
                                            <tr>
                                                <td>
                                                    {{ $doc }}
                                                </td>
                                                <td>
                                                    @php
                                                        $collectionName = $doc . '-admission-' . $program->id;
                                                        $media = $customer_case->customer->getFirstMedia($collectionName);
                                                        // dd($media);
                                                    @endphp
                                                    @if (!empty($media))
                                                        <a class="btn btn-sm btn-light document-in"
                                                            href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                                                            <i class="fa fa-fw fa-download text-success"></i>
                                                        </a>
                                                        @if (Illuminate\Support\Facades\Auth::user()->isAdmin() ||
                                                                Illuminate\Support\Facades\Auth::user()->isManager() ||
                                                                Illuminate\Support\Facades\Auth::user()->isVerificateur())
                                                            <a class="btn btn-sm btn-light document-in delete-document"
                                                                href="javascript:void(0)"
                                                                wire:click="deleteMedia('{{ addslashes($collectionName) }}')">
                                                                <i class="fa fa-fw fa-times text-danger"></i>
                                                            </a>
                                                        @endif
                                                        @if (
                                                            !$media->getCustomProperty('customer_owner') &&
                                                                (Illuminate\Support\Facades\Auth::user()->isAdmin() ||
                                                                    Illuminate\Support\Facades\Auth::user()->isManager() ||
                                                                    Illuminate\Support\Facades\Auth::user()->isVerificateur()))
                                                            @if ($media->getCustomProperty('visible_for_customer'))
                                                                <a wire:click="toggleProperty({{ $media->id }})"
                                                                    class="btn btn-sm btn-light document-in toggle-property"
                                                                    href="javascript:void(0)">
                                                                    <i class="fa fa-fw fa-eye text-success"></i>
                                                                </a>
                                                            @else
                                                                <a wire:click="toggleProperty({{ $media->id }})"
                                                                    class="btn btn-sm btn-light document-in toggle-property"
                                                                    href="javascript:void(0)">
                                                                    <i class="fa fa-fw fa-eye-slash text-danger"></i>
                                                                </a>
                                                            @endif
                                                        @endif
                                                    @else
                                                        @if ($currentRouteName == 'soumission-view')
                                                            @livewire('customer.partials.upload', ['customer_case' => $customer_case, 'collectionName' => $collectionName])
                                                        @else
                                                            <span
                                                                class="badge badge-primary m-2">{{ __('lang.portal_in_progress_badge') }}</span>
                                                        @endif
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            @livewire('block.admission.partials.program', ['customer_case' => $customer_case, 'programId' => $program->id])
                        </div>
                        <div class="col-md-6">
                            {{-- comment block --}}
                            @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'admission-' . $program->id])
                            @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'admission-' . $program->id])
                            @if ($currentRouteName == 'soumission-view')
                                @livewire('block.task', ['customer_case' => $customer_case, 'canal' => 'admission-' . $program->id])
                            @endif
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
        @if (count(
                $customer_case->customer->courses()->withPivot('admis')->get()))
            @foreach ($customer_case->customer->courses()->withPivot('admis')->get() as $program)
                @livewire('block.admission.partials.confirm', ['customer_case' => $customer_case, 'programId' => $program->id, 'loopIndex' => $loop->index], key($program->id))
            @endforeach
        @else
            <div class="block-content">
                <p>Merci de choisir depuis le clear 1002 les programmes pour les voir afficher ici</p>
            </div>
        @endif
    @endif
</div>
