<div class="block-header">
    <h3 class="block-title">{{ __("lang.portal_meeting_title")}} {{ $meet }}</h3>
    <div class="block-options">
        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
    </div>
</div>
