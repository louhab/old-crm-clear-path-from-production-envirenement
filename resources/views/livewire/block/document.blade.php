@if(is_null($location))
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title"></h3>
            <div class="block-options">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb push">
                        @if($pane == -1)
                            <li class="breadcrumb-item active" aria-current="page">Documents</li>
                        @else
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)" wire:click="togglePane(-1)">Documents</a>
                            </li>
                            @php $mediaSheet = \App\Models\MediaSheet::find($pane); @endphp
                            <li class="breadcrumb-item active" aria-current="page" style="word-break: break-all;max-width: 185px;">{{ $pane == 0 ? "Rapports": $mediaSheet->name_fr }}</li>
                        @endif
                    </ol>
                </nav>
            </div>
        </div>
        <div class="block-content overflow-auto mb-4" style="max-height: 360px;">
            @if($pane == -1)
                <div class="list-group push">
                    <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" wire:click="togglePane(0)" href="javascript:void(0)">
                        Rapports
<!--                        <span class="badge badge-pill badge-secondary">1</span>-->
                    </a>
                    @php $mediasheets = \App\Models\MediaSheet::where('program_id', $customer_case->program_id)->orderBy('order')->get();@endphp
                    @foreach($mediasheets as $mediaSheet)
                        @if(isset($groupedArray[$mediaSheet->id]) && count($groupedArray[$mediaSheet->id]))
                            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" wire:click="togglePane({{ $mediaSheet->id }})" href="javascript:void(0)">
                                {{ $mediaSheet->name_fr }}
                                <span class="badge badge-pill badge-danger">{{ isset($groupedArray[$mediaSheet->id]) ? count($groupedArray[$mediaSheet->id]): 0 }}</span>
                            </a>
                        @endif
                    @endforeach
                </div>
            @elseif($pane == 0)
                <ul class="nav-main-item open pl-0">
                    @php
                        $doc1002    = \App\Models\DocumentType::where("name_fr", "Clear1002 - Orientation")->first();
                        $media1002 = $customer_case->customer->getFirstMedia($doc1002->collection_name);
                        if($customer_case->program_id == 4) {
                            $doc1001 = \App\Models\DocumentType::where("name_fr", "Clear1001 – Conseil")->first();
                            $media1001 = $customer_case->customer->getFirstMedia($doc1001->collection_name);
                        } else {
                            $doc1004 = \App\Models\DocumentType::where("name_fr", "Clear1004 - Rapport Scoring")->first();
                            $media1004 = $customer_case->customer->getFirstMedia($doc1004->collection_name);
                        }
                    @endphp
                    @if($customer_case->program_id == 4)
                        @if(!empty($media1001))
                            <li class="nav-main-item">
                                <a href="{{ route('get-media', $media1001) }}" target="_blank" class="nav-main-link">
                                    <span class="nav-main-link-name">
                                        <i class="fa fa-fw fa-download text-success" style="font-size: 18px;"></i>
                                        {{ $doc1001->name_fr }}
                                    </span>
                                </a>
                            </li>
                        @endif
                        @if(!empty($media1002))
                            <li class="nav-main-item">
                                <a href="{{ route('get-media', $media1002) }}" target="_blank" class="nav-main-link">
                                    <span class="nav-main-link-name">
                                        <i class="fa fa-fw fa-download text-success" style="font-size: 18px;"></i>
                                        {{ $doc1002->name_fr }}
                                    </span>
                                </a>
                            </li>
                        @endif
                    @else
                        @if(!empty($media1004))
                            <li class="nav-main-item">
                                <a href="{{ route('get-media', $media1004) }}" target="_blank" class="nav-main-link">
                                    <span class="nav-main-link-name">
                                        <i class="fa fa-fw fa-download text-success" style="font-size: 18px;"></i>
                                        {{ $doc1004->name_fr }}
                                    </span>
                                </a>
                            </li>
                        @endif
                    @endif
                </ul>
            @else
                @php
                    // $mediaSheet = \App\Models\MediaSheet::find($pane);
                    $docs = $mediaSheet->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get();
                @endphp
                <ul class="nav-main-item open pl-0">
                    @foreach($docs as $doc)
                        @if($customer_case->customer->getMedia($doc->collection_name)->count())
                            @foreach($customer_case->customer->getMedia($doc->collection_name) as $media)
                                <li class="nav-main-item">
                                    <a href="{{ route('get-media', $media) }}" target="_blank" class="nav-main-link">
                                        <span class="nav-main-link-name">
                                            <i class="fa fa-fw fa-download text-success" style="font-size: 18px;"></i>
                                            {{ $doc->name_fr }}@if($customer_case->customer->getMedia($doc->collection_name)->count() > 1) - {{ $loop->index + 1 }} @endif
                                        </span>
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
@else
    <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu bg-danger" style="color: white;" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
            <i class="nav-main-link-icon si si-puzzle" style="color: white;"></i>
            <span class="nav-main-link-name">Documents</span>
        </a>
        <ul class="nav-main-submenu">
            <li class="nav-main-item">
                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">
                    <span class="nav-main-link-name">Rapports</span>
                </a>
                <ul class="nav-main-submenu">
                    @php
                        $doc1002    = \App\Models\DocumentType::where("name_fr", "Clear1002 - Orientation")->first();
                        $media1002 = $customer_case->customer->getFirstMedia($doc1002->collection_name);
                        if($customer_case->program_id == 4) {
                            $doc1001 = \App\Models\DocumentType::where("name_fr", "Clear1001 – Conseil")->first();
                            $media1001 = $customer_case->customer->getFirstMedia($doc1001->collection_name);
                        } else {
                            $doc1004 = \App\Models\DocumentType::where("name_fr", "Clear1004 - Rapport Scoring")->first();
                            $media1004 = $customer_case->customer->getFirstMedia($doc1004->collection_name);
                        }
                    @endphp
                    @if($customer_case->program_id == 4)
                        <li class="nav-main-item">
                            <a href="@if(!empty($media1001)) {{ route('get-media', $media1001) }} @else javascript:void(0) @endif" class="nav-main-link">
                                <span class="nav-main-link-name">
                                    @if(empty($media1001))
                                        <i class="fa fa-fw fa-info-circle text-warning" style="font-size: 18px;"></i>
                                    @else
                                        <i class="fa fa-fw fa-download text-success" style="font-size: 18px;"></i>
                                    @endif
                                    {{ $doc1001->name_fr }}
                                </span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a href="@if(!empty($media1002)) {{ route('get-media', $media1002) }} @else javascript:void(0) @endif" class="nav-main-link">
                                <span class="nav-main-link-name">
                                    @if(empty($media1002))
                                        <i class="fa fa-fw fa-info-circle text-warning" style="font-size: 18px;"></i>
                                    @else
                                        <i class="fa fa-fw fa-download text-success" style="font-size: 18px;"></i>
                                    @endif
                                    {{ $doc1002->name_fr }}
                                </span>
                            </a>
                        </li>
                    @else
                        <li class="nav-main-item">
                            <a href="@if(!empty($media1004)) {{ route('get-media', $media1004) }} @else javascript:void(0) @endif" class="nav-main-link">
                                <span class="nav-main-link-name">
                                    @if(empty($media1004))
                                        <i class="fa fa-fw fa-info-circle text-warning" style="font-size: 18px;"></i>
                                    @else
                                        <i class="fa fa-fw fa-download text-success" style="font-size: 18px;"></i>
                                    @endif
                                    {{ $doc1004->name_fr }}
                                </span>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
            @php $mediasheets = \App\Models\MediaSheet::where('program_id', $customer_case->program_id)->orderBy('order')->get(); @endphp
            @foreach($mediasheets as $mediaSheet)
                <li class="nav-main-item">
                    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">
                        <span class="nav-main-link-name">{{ $mediaSheet->name_fr }}</span>
                    </a>
                    <ul class="nav-main-submenu">
                        @php
                            // $mediaSheet = \App\Models\MediaSheet::where('name_fr', 'Clear1 - Documents de l\'étudiant')->first();
                            $docs = $mediaSheet->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get();
                        @endphp

                        @foreach($docs as $doc)
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                                    <span class="nav-main-link-name">{{ $doc->name_fr }}</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    @if($customer_case->customer->getMedia($doc->collection_name)->count())
                                    @foreach($customer_case->customer->getMedia($doc->collection_name) as $media)
                                        <li class="nav-main-item">
                                            <a href="@if(!empty($media)) {{ route('get-media', $media) }} @else javascript:void(0) @endif" class="nav-main-link">
                                                <span class="nav-main-link-name">
                                                    @if(empty($media))
                                                        <i class="fa fa-fw fa-info-circle text-warning" style="font-size: 18px;"></i>
                                                    @else
                                                        <i class="fa fa-fw fa-download text-success" style="font-size: 18px;"></i>
                                                    @endif
                                                    Document {{ $loop->index + 1 }}
                                                </span>
                                            </a>
                                        </li>
                                    @endforeach
                                    @else
                                        <li class="nav-main-item">
                                            <a href="javascript:void(0)" class="nav-main-link">
                                                <span class="nav-main-link-name">
                                                    <i class="fa fa-fw fa-info-circle text-warning" style="font-size: 18px;"></i>
                                                    Aucun documents..
                                                </span>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>
    </li>
@endif
