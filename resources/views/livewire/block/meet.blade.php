<?php
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
?>
<div class="block block-rounded" style="border: 1px solid #f00;">
    <div class="block-header block-header-default">
        <h3 class="block-title" data-prefix="{{ $prefix }}" data-customer="{{ $customer_case->customer->id }}"><i
                class="fa fa-2x fa-calendar-check me-1"></i>
            {{ __('lang.portal_meet_appointment_title_short') }} {{ $lang === 'fr' ? $prefix : $en_prefix }}</h3>
        <div class="block-options">
            <button type="button"
                class="btn btn-sm btn-alt-primary calendar-table-add">{{ __('lang.portal_make_appointment') }}</button>
        </div>
    </div>
    @if (count($meets))
        <div class="block-content" style="padding: 20px;">
            <table class="table table-sm table-vcenter">
                <thead>
                    <tr class="meets">
                        <th class="d-none d-sm-table-cell" style="width: 40%;">
                            {{ __('lang.portal_meet_appointment_title') }}</th>
                        <th class="d-none d-sm-table-cell" style="width: 15%;">
                            {{ __('lang.portal_meet_appointment_date') }}</th>
                        <th class="d-none d-sm-table-cell" style="width: 30%;">
                            {{ __('lang.portal_meet_appointment_time') }}</th>
                        <th style="width: 20%;">{{ __('lang.portal_personal_infos_advisor') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($meets as $meet)
                        <tr class="meets">
                            <td class="font-size-sm">
                                <p class="text-muted mb-0">{{ $meet->title }}</p>
                            </td>
                            <td class="font-size-sm">
                                <p class="text-muted mb-0">{{ $meet->date }}</p>
                                <!-- November 28, 2018 08:59 -->
                            </td>
                            <td class="font-size-sm">
                                <p class="text-muted mb-0">{{ __('lang.portal_meet_appointment_start') }} :
                                    {{ \Carbon\Carbon::parse($meet->time)->format('H:i') }},
                                    {{ __('lang.portal_meet_appointment_end') }} :
                                    {{ \Carbon\Carbon::parse($meet->time_end)->format('H:i') }}</p>
                            </td>
                            <td class="font-size-sm">
                                <p class="text-muted mb-0"> <strong>{{ $conseillerName }}</strong></p>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="block-content" style="padding-left: 20px;margin-bottom: 0;">
           <p>Pas de rendez vous pour le moment !</p>
        </div>
    @endif
</div>
