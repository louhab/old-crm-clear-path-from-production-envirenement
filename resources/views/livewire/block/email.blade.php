<div class="block block-rounded js-ecom-div-cart d-none d-xl-block">
    <div class="block-header block-header-default">
        <h3 class="block-title">
            Emails
        </h3>
    </div>
    <div class="block-content overflow-auto" style="height: 100%;max-height: 40em;padding: 20px;">
        <div class="row items-push">
            <div class="col-md-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
                <div>
                    <select wire:model="email" wire:change="change" class="form-control form-control-alt">
                        <option>Email ..</option>
                        <option value="1">Email de Bienvenue</option>
                        @if($customer_case->program_id == 1)
                            <option value="2">Guide test de langue</option>
                        @endif
                    </select>
                   <br>
                    <button wire:click="send" class="btn btn-outline-danger btn-block">{{ __("lang.portal_about_the_lead_resend_emails_button") }}</button>
                </div>
            </div>
        </div>
    </div>

</div>
