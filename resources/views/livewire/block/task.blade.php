<div class="block block-rounded">
    <div class="block-header">
        <h3 class="block-title">
            <textarea type="text" class="form-control-plaintext" wire:model="task.title"></textarea>
        </h3>
        <div class="block-options">
            <div class="block-options-item text-warning">
                <div class="custom-control custom-switch mb-1">
                    <input type="checkbox" class="custom-control-input" wire:model="task.state" id="{{ $canal }}" name="{{ $canal }}">
                    <label class="custom-control-label" for="{{ $canal }}"></label>
                </div>
            </div>
        </div>
    </div>
</div>
