<div class="block block-rounded">
    <div class="block-content">
        @php
            $step = \App\Models\ProgramStep::where("id", $substep->step_id)->first();
            $docs = $substep->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot('order')->orderBy('order', 'asc')->get();
            $tmp = array();
            $categories = array(
                'CAQ' => [
                    "5LF3vOoXW7JW2vuT77ixYOppurEhwB",
                    "zD19jYEoTNRQ0VAsoRID9OyFSA9iwe",
                    "P5Xd2WLq7sLp3tTDotKrkMwmZVuSCO",
                    "8RSZRZq2gRGzjgmmedaQZfdOKrjY7z"
                ],
                'CPG' => [
                    "N2AfihMf2gASgmtH21s1haGzWY2745",
                    "QLmvx7p0l06bB6pc42SAdruIv9aduK",
                    "5DmlEd0g9aQ73ZOanbAz6DfBIHrIEX",
                    "jj4nkceLFpQqI5Z0yVRKhqncry1Q07"
                ],
                'Factue' => [
                    "fw1Zn4oEsOhqpW5X8XLy2j2ZkXnaQ7",
                    "JILAtbfP8lGpwYwiLCw1ClD43UNb77",
                    "h7exyvLX1mYAtKC7iQ0PKc36LgBmlQ",
                    "Y8NcdxzARy9PRtXaOJSmix7zEbp77f",
                    "lxfpZ5NvxBuW0hcfGn8YZGCXtiAUfZ"
                ],
                'Prise en charge' => [
                    "cH6Spd09qKybfoUXHFo8cCFvqzoCEa",
                    "TNxBeZbmhLKaH7A8Y6jYoFi5lYvcPW"
                ],
                'Imm5645' => [
                    "BaXV8NxK6F2QUNH7rnrY9Gn2mJS57j",
                    "p9bzJg4NUB5JdmBgTVWkdBtf3m4Jlu"
                ],
                'Imm5476' => [
                    "bk66erNlmYizLbeSH37C5pQQqwGMkH",
                    "nSWkxVMy82s6aeVz9pVOtgGtnIxC4I"
                ]

        );
            $CAQ_collection = [];
            $CPG_collection = [];
            $Factue_collection = [];
            $PEC_collection = [];
            $Imm5645_collection = [];
            $Imm5476_collection = [];

            $other_docs = [];
            foreach ($docs as $doc) {
                if (in_array($doc->collection_name, $categories["CAQ"])) {
                    array_push($CAQ_collection, $doc);
                } elseif (in_array($doc->collection_name, $categories["CPG"])) {
                    array_push($CPG_collection, $doc);
                } elseif (in_array($doc->collection_name, $categories["Factue"])) {
                    array_push($Factue_collection, $doc);
                } elseif (in_array($doc->collection_name, $categories["Prise en charge"])) {
                    array_push($PEC_collection, $doc);
                } elseif (in_array($doc->collection_name, $categories["Imm5645"])) {
                    array_push($Imm5645_collection, $doc);
                }elseif (in_array($doc->collection_name, $categories["Imm5476"])) {
                    array_push($Imm5476_collection, $doc);
                } else {
                    array_push($other_docs, $doc);
                }
            }

            $CAQ_collection = collect($CAQ_collection);
            $CPG_collection = collect($CPG_collection);
            $Factue_collection = collect($Factue_collection);
            $PEC_collection = collect($PEC_collection);
            $Imm5645_collection = collect($Imm5645_collection);
            $Imm5476_collection = collect($Imm5476_collection);

            $other_docs = collect($other_docs);

            // dd($CAQ_collection);

            $total_docs = array(
                "CAQ" => $CAQ_collection,
                "CPG" => $CPG_collection,
                "Facture scolaire" => $Factue_collection,
                "Prise en charge" => $PEC_collection,
                "Imm5645" =>  $Imm5645_collection,
                "Imm5476" =>  $Imm5476_collection,
                "Other" => $other_docs
            );
        @endphp
        @foreach ($total_docs as $title => $col_docs)
            @if(count($col_docs))
            <h3 class="block-title">{{ $title }}</h3>
                <table class="table table-sm table-vcenter">
                    <tbody>
                    @foreach($col_docs as $doc)
                        <tr class="custom_tr ht">
                            <td class="font-size-sm custom_td">
                                {{ $doc->name_fr }}
                            </td>
                            @php
                                $collectionName = $doc->collection_name;
                                $media = $customer_case->customer->getFirstMedia($collectionName);
                            @endphp
                            @if(in_array($doc->id, [148 ,150]))
                                <td class="d-none d-sm-table-cell custom_td">
                                    @if(!empty($media))
                                        <a class="btn btn-sm btn-light document-in" href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                                            <i class="fa fa-fw fa-download text-success"></i>
                                        </a>
                                        @if(Illuminate\Support\Facades\Auth::user()->isAdmin() || Illuminate\Support\Facades\Auth::user()->isManager() || Illuminate\Support\Facades\Auth::user()->isVerificateur())
                                            <a class="btn btn-sm btn-light document-in delete-document" href="javascript:void(0)" wire:click="deleteMedia('{{ addslashes($collectionName) }}')">
                                                <i class="fa fa-fw fa-times text-danger"></i>
                                            </a>
                                        @endif
                                        @if(!$media->getCustomProperty('customer_owner') && (Illuminate\Support\Facades\Auth::user()->isAdmin() || Illuminate\Support\Facades\Auth::user()->isVerificateur() || Illuminate\Support\Facades\Auth::user()->isManager()))
                                            @if($media->getCustomProperty('visible_for_customer'))
                                                <a wire:click="toggleProperty({{ $media->id }})" class="btn btn-sm btn-light document-in toggle-property" href="javascript:void(0)">
                                                    <i class="fa fa-fw fa-eye text-success"></i>
                                                </a>
                                            @else
                                                <a wire:click="toggleProperty({{ $media->id }})" class="btn btn-sm btn-light document-in toggle-property" href="javascript:void(0)">
                                                    <i class="fa fa-fw fa-eye-slash text-danger"></i>
                                                </a>
                                            @endif
                                        @endif
                                    @else
                                        @if($currentRouteName == "soumission-view")
                                            @livewire('customer.partials.upload', ['customer_case' => $customer_case, 'collectionName' => $collectionName])
                                        @else
                                            <span class="badge badge-primary m-2">{{ __("lang.portal_in_progress_badge") }}</span>
                                        @endif
                                    @endif
                                </td>
                            @elseif(in_array($doc->id, [147 ,149, 168]))
                                <td class="d-none d-sm-table-cell custom_td">
                                    @if(!empty($media))
                                        <a class="btn btn-sm btn-light document-in" href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                                            <i class="fa fa-fw fa-download text-success"></i>
                                        </a>
                                        @if(Illuminate\Support\Facades\Auth::user()->isAdmin() || Illuminate\Support\Facades\Auth::user()->isManager())
                                            <a class="btn btn-sm btn-light document-in delete-document" href="javascript:void(0)" wire:click="deleteMedia('{{ addslashes($collectionName) }}')">
                                                <i class="fa fa-fw fa-times text-danger"></i>
                                            </a>
                                        @endif
                                        @if(!$media->getCustomProperty('customer_owner') && (Illuminate\Support\Facades\Auth::user()->isAdmin() || Illuminate\Support\Facades\Auth::user()->isVerificateur() || Illuminate\Support\Facades\Auth::user()->isManager()))
                                            @if($media->getCustomProperty('visible_for_customer'))
                                                <a wire:click="toggleProperty({{ $media->id }})" class="btn btn-sm btn-light document-in toggle-property" href="javascript:void(0)">
                                                    <i class="fa fa-fw fa-eye text-success"></i>
                                                </a>
                                            @else
                                                <a wire:click="toggleProperty({{ $media->id }})" class="btn btn-sm btn-light document-in toggle-property" href="javascript:void(0)">
                                                    <i class="fa fa-fw fa-eye-slash text-danger"></i>
                                                </a>
                                            @endif
                                        @endif
                                    @else
                                        @if($currentRouteName != "soumission-view")
                                            @livewire('customer.partials.upload', ['customer_case' => $customer_case, 'collectionName' => $collectionName])
                                        @else
                                            <span class="badge badge-primary m-2">{{ __("lang.portal_in_progress_badge") }}</span>
                                        @endif
                                    @endif
                                </td>
                            @elseif(in_array($doc->id, [105, 106, 107]))
                                <td class="d-none d-sm-table-cell custom_td">
                                    @if(!empty($media))
                                        <a class="btn btn-sm btn-light document-in" href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                                            <i class="fa fa-fw fa-download text-success"></i>
                                        </a>
                                        @if(Illuminate\Support\Facades\Auth::user()->isAdmin() || Illuminate\Support\Facades\Auth::user()->isManager())
                                            <a class="btn btn-sm btn-light document-in delete-document" href="javascript:void(0)" wire:click="deleteMedia('{{ addslashes($collectionName) }}')">
                                                <i class="fa fa-fw fa-times text-danger"></i>
                                            </a>
                                        @endif
                                        @if(!$media->getCustomProperty('customer_owner') && (Illuminate\Support\Facades\Auth::user()->isAdmin() || Illuminate\Support\Facades\Auth::user()->isVerificateur() || Illuminate\Support\Facades\Auth::user()->isManager()))
                                            @if($media->getCustomProperty('visible_for_customer'))
                                                <a wire:click="toggleProperty({{ $media->id }})" class="btn btn-sm btn-light document-in toggle-property" href="javascript:void(0)">
                                                    <i class="fa fa-fw fa-eye text-success"></i>
                                                </a>
                                            @else
                                                <a wire:click="toggleProperty({{ $media->id }})" class="btn btn-sm btn-light document-in toggle-property" href="javascript:void(0)">
                                                    <i class="fa fa-fw fa-eye-slash text-danger"></i>
                                                </a>
                                            @endif
                                        @endif
                                    @else
                                        @if(auth()->user()->isAdmin() || auth()->user()->isVerificateur() || auth()->user()->isManager() || auth()->user()->isConseiller() || ($currentRouteName != "soumission-view"))
                                            @livewire('customer.partials.upload', ['customer_case' => $customer_case, 'collectionName' => $collectionName])
                                        @else
                                            @include('cases.steps.partials.document', ['document' => $doc->collection_name_fr, 'collectionName' => $collectionName, 'media' => $media])
                                        @endif
                                    @endif
                                </td>
                            @else
                                <td class="d-none d-sm-table-cell custom_td custom_d-flex">
                                    @include('cases.steps.partials.document', ['document' => $doc->collection_name_fr, 'collectionName' => $collectionName, 'media' => $media])
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        @endforeach
    </div>
</div>
