<div class="block block-rounded js-ecom-div-cart d-none d-xl-block">
    <div class="block-header block-header-default">
        <h3 class="block-title">
            Admission Status
        </h3>
        <div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-refresh"></i>
            </button>
        </div>
    </div>

    <div class="block-content scroll" style="height: 100%;max-height: 40em;">

        <div class="row items-push">
            <div class="col-md-12">
                <form action="{{ route('soumission-update-state', ['customer_case' => $customer_case]) }}"
                    method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="view" value="{{ $currentRouteName }}">
                    <div class="form-group">
                        <label for="english">Status Admission</label>
                        @php
                            if (is_null($customer_case->customer->admission_state)) {
                                $status = null;
                            } else {
                                $status = \App\Models\AdmissionState::find($customer_case->customer->admission_state);
                                $status = $status ? $status->label : null;
                            }
                        @endphp
                        <select name="admission_status" data-program="{{ $customer_case->program_id }}"
                            class="form-control form-control-alt">
                            @if (!is_null($status))
                                <option value="{{ $customer_case->customer->admission_state }}" selected>
                                    {{ $status }}</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-outline-danger btn-block">Enregistrer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @push('custom_scripts')
        <script>
            // admission_status
            $('select[name="admission_status"]').select2();
            $('select[name="admission_status"]').select2({
                placeholder: 'Status...',
                allowClear: true,
                ajax: {
                    url: `/api/v1/admission-states`,
                    dataType: 'json',
                    delay: 250,
                    method: 'POST',
                    data: function(term, page) {
                        let program = $(this).data('program');
                        // console.log(pid);
                        return {
                            q: term,
                            pid: program,
                        }
                    },
                    processResults: function(data) {
                        // console.log(data);
                        var res = [];
                        // var minus = -101;
                        $.each(data, function(label, group) {
                            var groupObj = {
                                "text": label
                            };
                            var children = []; // console.log(role, group);
                            $.each(group, function(idx, status) {
                                // console.log(status.name);
                                children.push({
                                    text: status.name,
                                    id: status.id,
                                    group: label
                                });
                            });
                            groupObj["children"] = children;
                            res.push(groupObj);
                        });

                        return {
                            results: res
                        };
                    },
                    initSelection: function(element, callback) {},
                    cache: true
                }
            });
        </script>
    @endpush
</div>
