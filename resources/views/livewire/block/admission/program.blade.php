<div class="block block-mode-hidden">
    <div class="block-header">
        <h3 class="block-title"> Admissions <strong>(deprecated)</strong> </h3>
        <div class="block-options">
            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
        </div>
    </div>
    <div class="block-content">
        @foreach(range(1, 5) as $idx)
            <div class="block block-rounded" wire:key="admission-index-{{ $idx }}">
                <div class="block-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title"><i class="fa fa-bookmark" style="color:red;font-size:1em;margin-right:10px;"></i> Admission {{ $idx }} </h3>
                                </div>
                                <div class="block-content">
                                    @php
                                        $docs = ["Lettre modèle", 'Accusé de réception', 'Reçu', 'Lettre de suivi', "Lettre d'acceptation", "Lettre de refus"];
                                        // dd(Illuminate\Support\Facades\Route::currentRouteName());
                                    @endphp
                                    <table class="table table-hover">
                                        @if($currentRouteName == "case-edit" || auth()->user()->isAdmin() || auth()->user()->isVerificateur() || auth()->user()->isSuperBackoffice())
                                            <tr>
                                                <form>
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 col-form-label" for="name">Nom du programme</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" wire:model.defer="programs.{{ $idx }}" class="form-control form-control-alt" id="name" placeholder="Programme..">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 col-form-label" for="lien">Lien du programme</label>
                                                        <div class="col-sm-8">
                                                            <textarea class="form-control form-control-alt" wire:model.defer="urls.{{ $idx }}" id="lien" name="lien" rows="3" placeholder="Lien.."></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                            <button type="button" wire:click="updateURL({{ $idx }})" class="btn btn-dark float-right">Sanvegarder lien du programme</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </tr>
                                        @elseif($currentRouteName == "soumission-view")
                                            <tr>
                                                <div class="row">
                                                    <div class="col-4"></div>
                                                    <div class="col-8">
                                                        <div class="form-group">
                                                            <label for="program">Programe:</label>
                                                            <p class="font-size-sm text-muted">
                                                                @if (array_key_exists($idx, $urls))
                                                                    {{ $programs[$idx] }}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="lien">Lien:</label>
                                                            <textarea class="form-control form-control-alt" id="lien" name="lien" rows="3" placeholder="Lien En Cours.." disabled>@if (array_key_exists($idx, $urls)){{ $urls[$idx] }}@endif</textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn-dark copy-lien" data-lien="@if (array_key_exists($idx, $urls)){{ $urls[$idx] }}@endif">Copier le lien du programme</button>
                                                            @if (array_key_exists($idx, $urls))
                                                                <div class="btn-group" style="margin:5px">
                                                                    <a class="btn btn-sm btn-light" href="{{ $urls[$idx] }}" target="_blank">
                                                                        <i class="fa fa-fw fa-link text-info"></i>
                                                                    </a>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </tr>
                                        @endif
                                        @foreach($docs as $doc)
                                            @php
                                                $collectionName = $doc . "-admission-" . $idx;
                                                $media = $customer_case->customer->getFirstMedia($collectionName);
                                                // dd($media);
                                            @endphp
                                            <tr wire:key="{{ $collectionName }}" class="test_class">
                                                <td>
                                                    {{ $doc }}
                                                </td>
                                                <td>
                                                    @if(!empty($media))
                                                        <a class="btn btn-sm btn-light document-in" href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                                                            <i class="fa fa-fw fa-download text-success"></i>
                                                        </a>
                                                        @if(auth()->user()->isAdmin() || auth()->user()->isVerificateur() || auth()->user()->isManager() || auth()->user()->isBackoffice())
                                                            <a class="btn btn-sm btn-light document-in delete-document" href="javascript:void(0)" wire:click="deleteMedia('{{ addslashes($collectionName) }}')">
                                                                <i class="fa fa-fw fa-times text-danger"></i>
                                                            </a>
                                                        @endif
                                                        @if(!$media->getCustomProperty('customer_owner') && (Illuminate\Support\Facades\Auth::user()->isAdmin() || Illuminate\Support\Facades\Auth::user()->isVerificateur() || Illuminate\Support\Facades\Auth::user()->isManager()))
                                                            @if($media->getCustomProperty('visible_for_customer'))
                                                                <a wire:click="toggleProperty({{ $media->id }})" class="btn btn-sm btn-light document-in toggle-property" href="javascript:void(0)">
                                                                    <i class="fa fa-fw fa-eye text-success"></i>
                                                                </a>
                                                            @else
                                                                <a wire:click="toggleProperty({{ $media->id }})" class="btn btn-sm btn-light document-in toggle-property" href="javascript:void(0)">
                                                                    <i class="fa fa-fw fa-eye-slash text-danger"></i>
                                                                </a>
                                                            @endif
                                                        @endif
                                                    @else
                                                        @if($currentRouteName == "soumission-view")
                                                            @livewire('customer.partials.upload', ['customer_case' => $customer_case, 'collectionName' => $collectionName])
                                                        @else
                                                            <span class="badge badge-primary m-2">{{ __("lang.portal_in_progress_badge") }}</span>
                                                        @endif
                                                    @endif

                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-admission-' . $idx], key('admission-comment-index-'.$idx))
                            @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-admission-' . $idx], key('admission-comment-index-'.$idx))
                            @if($currentRouteName == "soumission-view")
                                @livewire('block.task', ['customer_case' => $customer_case, 'canal' => 'soumission-admission-' . $idx])
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    @push('custom_scripts')
        <script>
            $('body').on('click', '.copy-lien', function () {

                let str = $(this).data('lien');

                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val(str).select();
                document.execCommand("copy");
                $temp.remove();
            });
        </script>
    @endpush
</div>
