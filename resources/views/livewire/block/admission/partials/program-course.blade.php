<div class="block block-rounded">
    <div class="block-header">
        @php
            $program = $this->customer_case->customer->courses()->wherePivot("program_course_id", $programId)->withPivot('admis')->first();
        @endphp
        <h3 class="block-title">{{ $program->name }}</h3>
        <div class="block-options">
            @if(is_null($program->pivot->admis))
                @if($currentRouteName == 'soumission-view')
                    <button type="button" class="btn btn-sm btn-alt-success" wire:click="updateProgram({{ $program }}, 'Admis')">Admis</button>
                    <button type="button" class="btn btn-sm btn-alt-danger" wire:click="updateProgram({{ $program }}, 'Refus')">Refus</button>
                @endif
            @else
                @if($program->pivot->admis)
                    <div class="block-options-item text-success">Admis!</div>
                @else
                    <div class="block-options-item text-danger">Refus!</div>
                @endif
                @if($currentRouteName == 'soumission-view')
                    <button type="button" class="btn-block-option" wire:click="updateProgram({{ $program }}, 'Reset')">
                        <i class="si si-close"></i>
                    </button>
                @endif
            @endif
        </div>
    </div>
</div>
