<div class="block-content row">
    @if ($isConfirmed)
        <div class="col-md-6">
            <div class="block block-rounded">
                <div class="block-header">
                    <h3 class="block-title">
                        Admission {{ $loopIndex + 1 }} : {{ $this->safeCheck('diplome', $program['properties']) }} -
                        {{ $program->name }}
                        <small>{{ $this->safeCheck('type_etablissement', $program['properties']) }} -
                            {{ $this->safeCheck('nom_etablissement', $program['properties']) }}</small>
                    </h3>
                    <div class="block-options">
                        <div class="dropdown">
                            <button type="button" class="btn-block-option dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">Settings</button>
                            <div class="dropdown-menu dropdown-menu-right font-size-sm" style="">
                                <a class="dropdown-item" href="{{ $this->safeCheck('lien', $program['properties']) }}"
                                    target="_blank">
                                    <i class="fa fa-external-link-alt mr-1" style="font-size: 16px;"></i> Lien programme
                                </a>
                                @if ($currentRouteName != 'soumission-view')
                                    <a class="dropdown-item" href="javascript:void(0)"
                                        wire:click="confirmProgram({{ $program }})">
                                        <i class="fas fa-times mr-1"></i> Annuller programme
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-content">
                    <table class="table table-vcenter">
                        @php
                            $docs = ['Lettre modèle', 'Accusé de réception', 'Reçu', 'Lettre de suivi', "Lettre d'acceptation", 'Lettre de refus'];
                            // dd(Illuminate\Support\Facades\Route::currentRouteName());
                        @endphp
                        @foreach ($docs as $doc)
                            <tr class="custom_tr hb">
                                <td class="custom_td">
                                    {{ $doc }}
                                </td>
                                <td class="custom_td">
                                    @php
                                        $collectionName = $doc . '-course-' . $programId;
                                        $media = $customer_case->customer->getFirstMedia($collectionName);
                                    @endphp
                                    @if (!empty($media))
                                        <a class="btn btn-sm btn-light document-in"
                                            href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                                            <i class="fa fa-fw fa-download text-success"></i>
                                        </a>
                                        @if (Illuminate\Support\Facades\Auth::user()->isAdmin() ||
                                                Illuminate\Support\Facades\Auth::user()->isVerificateur() ||
                                                Illuminate\Support\Facades\Auth::user()->isManager() ||
                                                Illuminate\Support\Facades\Auth::user()->isBackoffice())
                                            <a class="btn btn-sm btn-light document-in delete-document"
                                                href="javascript:void(0)"
                                                wire:click="deleteMedia('{{ addslashes($collectionName) }}')">
                                                <i class="fa fa-fw fa-times text-danger"></i>
                                            </a>
                                        @endif
                                        @if (
                                            (!$media->getCustomProperty('customer_owner') &&
                                                (Illuminate\Support\Facades\Auth::user()->isAdmin() ||
                                                    Illuminate\Support\Facades\Auth::user()->isVerificateur() ||
                                                    Illuminate\Support\Facades\Auth::user()->isManager())) ||
                                                Illuminate\Support\Facades\Auth::user()->isBackoffice())
                                            @if ($media->getCustomProperty('visible_for_customer'))
                                                <a wire:click="toggleProperty({{ $media->id }})"
                                                    class="btn btn-sm btn-light document-in toggle-property"
                                                    href="javascript:void(0)">
                                                    <i class="fa fa-fw fa-eye text-success"></i>
                                                </a>
                                            @else
                                                <a wire:click="toggleProperty({{ $media->id }})"
                                                    class="btn btn-sm btn-light document-in toggle-property"
                                                    href="javascript:void(0)">
                                                    <i class="fa fa-fw fa-eye-slash text-danger"></i>
                                                </a>
                                            @endif
                                        @endif
                                        {{-- @include('cases.steps.partials.document', ['document' => $program->id.".Lettre Acceptation", 'collectionName' => $program->id.".Lettre Acceptation", 'media' => $media]) --}}
                                    @else
                                        @if ($currentRouteName == 'soumission-view')
                                            @livewire('customer.partials.upload', ['customer_case' => $customer_case, 'collectionName' => $collectionName])
                                        @else
                                            <span
                                                class="badge badge-primary m-2">{{ __('lang.portal_in_progress_badge') }}</span>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            @livewire('block.admission.partials.program-course', ['customer_case' => $customer_case, 'programId' => $programId])
        </div>
        <div class="col-md-6">
            @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'course-' . $programId])
            @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'course-' . $programId])
            @if ($currentRouteName == 'soumission-view')
                @livewire('block.task', ['customer_case' => $customer_case, 'canal' => 'course-' . $programId])
            @endif
        </div>
    @elseif($currentRouteName != 'soumission-view')
        <?php
        // dd($program);
        // array_key_exists("message", $arr)
        // it needs double check
        // if key exisits => {} (100%)
        // if value exisits => {} (50%)
        ?>
        <div class="col-md-12">
            <div class="block block-rounded">
                <div class="block-header">
                    <h3 class="block-title">
                        Admission {{ $loopIndex + 1 }} : {{ $this->safeCheck('diplome', $program['properties']) }} -
                        {{ $program->name }}
                        <small>{{ $this->safeCheck('type_etablissement', $program['properties']) }} -
                            {{ $this->safeCheck('nom_etablissement', $program['properties']) }}</small>
                    </h3>
                    @if ($currentRouteName != 'soumission-view')
                        <div class="block-options">
                            <button title="Confirmer programme" class="btn btn-sm btn-light"
                                wire:click="confirmProgram({{ $program }})">
                                <i class="fas fa-check text-success"></i> Confirmer programme
                            </button>
                        </div>
                    @else
                        <div class="block-options">
                            <div class="block-options-item text-danger">Not confirmed yet!</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endif
</div>
