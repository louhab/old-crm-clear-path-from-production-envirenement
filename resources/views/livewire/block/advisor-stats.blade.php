<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title"><i class="far fa-chart-bar me-1"></i> Listes des Statistiques</h3>
    </div>
    @if(count($stats))
        <div class="block-content custom_table" style="padding: 20px;">
            <table class="table table-sm table-vcenter">
                <thead>
                <tr class="calls">
                    <th class="d-none d-sm-table-cell" style="width: 20%;">
                        Rencontre
                    </th>
                    <th style="width: 45%;">
                        Total
                    </th>
                    <th>
                        Total des rencontres
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($stats as $stat)
                    <tr class="comment">
                        <td class="d-none d-sm-table-cell" style="width: 20%;">
                            <em class="font-size-sm text-muted">{{ str_replace("Paiement ", "R", $stat->payment) }}</em>
                        </td>
                        <td class="font-size-sm" style="width: 45%;">
                            <p class="text-muted mb-0">
                                <em class="font-size-sm text-muted">{{ $stat->total_amouts }}</em>
                            </p>
                        </td>
                        <td class="font-size-sm">
                            <p class="text-muted mb-0">
                                <em class="font-size-sm text-muted">{{ $stat->total_meets }}</em>
                            </p>
                        </td>
                    </tr>
                @endforeach
                <tr class="comment">
                    <th class="d-none d-sm-table-cell" style="width: 20%;">
                        Total des rencontres
                    </th>
                    <th class="font-size-sm" style="width: 45%;">
                        <p class="text-muted mb-0">
                            {{-- <em class="font-size-sm text-muted">{{ $stat->total_amouts }}</em> --}}
                            {{ "" }}
                        </p>
                    </th>
                    <th class="font-size-sm">
                        <p class="text-muted mb-0">
                            @php
                                $total_meets = 0;
                                foreach ($stats as $stat) {
                                    $total_meets += $stat->total_meets;
                                }
                            @endphp
                            <em class="font-size-sm text-muted">{{ $total_meets }}</em>
                        </p>
                    </th>
                </tr>
                </tbody>
            </table>
        </div>
    @endif
</div>
