<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>

@section('css_after')
    <style>
        .table td, .table th {
            padding: 0.35rem;
            max-width: 240px;
            word-break: break-word;
        }
    </style>
    @livewireStyles
@endsection

<div class="block block-rounded">
        <div class="block-content" style="padding: 20px;">
            <div>
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-todo-today-tab" data-toggle="pill" href="#pills-todo-today" role="tab" aria-controls="pills-todo-today" aria-selected="true">Tâches à faire Aujourd'hui<span class="badge badge-pill badge-dark ml-2">{{ count($todo_today) }}</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-follow-up-today-tab" data-toggle="pill" href="#pills-follow-up-today" role="tab" aria-controls="pills-follow-up-today" aria-selected="false">Suivi à faire Aujourd'hui<span class="badge badge-pill badge-dark ml-2">{{ count($follow_up_today) }}</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-overdue-task-tab" data-toggle="pill" href="#pills-overdue-task" role="tab" aria-controls="pills-overdue-task" aria-selected="false">Dossier en retard<span class="badge badge-pill badge-dark ml-2">{{ count($overdue_task) }}</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-overdue-follow-up-tab" data-toggle="pill" href="#pills-overdue-follow-up" role="tab" aria-controls="pills-overdue-follow-up" aria-selected="false">Suivi en retard<span class="badge badge-pill badge-dark ml-2">{{ count($overdue_follow_up) }}</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled">Total<span class="badge badge-pill badge-dark ml-2">{{ $total }}</span></a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">

                    <div class="tab-pane fade show active" id="pills-todo-today" role="tabpanel" aria-labelledby="pills-todo-today-tab">
                        <div class="block-content">
                            <table class="table table-vcenter">
                                <thead>
                                    <tr>
                                        <th>Tache</th>
                                        <th class="d-none d-sm-table-cell" style="width: 13%;">Client</th>
                                        <th class="d-none d-sm-table-cell" style="width: 13%;">Backofficer</th>
                                        <th class="d-none d-sm-table-cell" style="width: 15%;">Status</th>
                                        <th class="d-none d-sm-table-cell" style="width: 20%;">Mes Documents</th>
                                        <th class="text-center" style="width: 150px;">Temps restant</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($todo_today as $task_one)
                                        <tr>
                                            <?php
                                                $task_name = ucfirst(str_replace("soumission-", "", $task_one['canal']));
                                            ?>
                                            <td class="fw-semibold fs-sm">
                                                <span><a href="{{ 'soumission/' . $task_one['customer_case_id'] }}">{{ $task_name }}</a></span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fw-semibold fs-sm">{{ $task_one["firstname"] . " " . $task_one["lastname"] }}</span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fw-semibold fs-sm">{{ $task_one["name"] ?: "n/d" }}</span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill {{ $task_one['status_class'] }}" > {{ $task_one["msg"] }} </span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-info-light text-info">{{ $task_one['canal'] }}</span>
                                            </td>
                                            <td class="text-center" scope="row">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-sm btn-alt-info d-inline">
                                                        <div class="d-flex flex-row justify-content-between align-items-center" style="width: 80px;">
                                                            <i class="fa fa-fw fa-clock"></i>
                                                            <p class="pl-1 m-0">n/d</p>
                                                            <input type="hidden" id="{{ 'task-' . $task_one['id'] }}" value="{{ $task_one['created_at'] }}" />
                                                        </div>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="pills-follow-up-today" role="tabpanel" aria-labelledby="pills-follow-up-today-tab">
                        <div class="block-content">
                            <table class="table table-vcenter">
                                <thead>
                                    <tr>
                                        <th>Tache</th>
                                        <th class="d-none d-sm-table-cell" style="width: 13%;">Client</th>
                                        <th class="d-none d-sm-table-cell" style="width: 13%;">Backofficer</th>
                                        <th class="d-none d-sm-table-cell" style="width: 15%;">Status</th>
                                        <th class="d-none d-sm-table-cell" style="width: 20%;">Mes Documents</th>
                                        <th class="text-center" style="width: 150px;">Temps restant</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($follow_up_today as $task_two)
                                        @php
                                            // dd($task_two);
                                        @endphp
                                        <tr>
                                            <?php
                                                $task_name = ucfirst(str_replace("soumission-", "", $task_two['canal']));
                                            ?>
                                            <td class="d-none d-sm-table-cell">
                                                <span><a href="{{ 'soumission/' . $task_two['customer_case_id'] }}">{{ $task_name }}</a></span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fw-semibold fs-sm">{{ $task_two["firstname"] . " " . $task_two["lastname"]}}</span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fw-semibold fs-sm">{{ $task_two["name"] ?: "n/d" }}</span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill {{ $task_two['status_class'] }}" > {{ $task_two["msg"] }} </span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-info-light text-info">{{ $task_two['canal'] }}</span>
                                            </td>
                                            <td class="text-center" scope="row">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-sm btn-alt-info d-inline">
                                                        <div class="d-flex flex-row justify-content-between align-items-center" style="width: 80px">
                                                            <i class="fa fa-fw fa-clock"></i>
                                                            <p class="pl-1 m-0">n/d</p>
                                                            <input type="hidden" id="{{ 'task-' . $task_two['id'] }}" value="{{ $task_two['created_at'] }}" />
                                                        </div>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-overdue-task" role="tabpanel" aria-labelledby="pills-overdue-task-tab">
                        <div class="block-content">
                            <table class="table table-vcenter">
                                <thead>
                                    <tr>
                                        <th>Tache</th>
                                        <th class="d-none d-sm-table-cell" style="width: 13%;">Client</th>
                                        <th class="d-none d-sm-table-cell" style="width: 13%;">Backofficer</th>
                                        <th class="d-none d-sm-table-cell" style="width: 15%;">Status</th>
                                        <th class="d-none d-sm-table-cell" style="width: 20%;">Mes Documents</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($overdue_task as $task_three)
                                        <tr>
                                            <?php
                                                $task_name = ucfirst(str_replace("soumission-", "", $task_three['canal']));
                                            ?>
                                            <td class="d-none d-sm-table-cell">
                                                <span><a href="{{ 'soumission/' . $task_three['customer_case_id'] }}">{{ $task_name }}</a></span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fw-semibold fs-sm">{{ $task_three["firstname"] . " " . $task_three["lastname"]}}</span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fw-semibold fs-sm">{{ $task_three["name"] ?: "n/d" }}</span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill {{ $task_three['status_class'] }}" > {{ $task_three["msg"] }} </span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-info-light text-info">{{ $task_three['canal'] }}</span>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="pills-overdue-follow-up" role="tabpanel" aria-labelledby="pills-overdue-follow-up-tab">
                        <div class="block-content">
                            <table class="table table-vcenter">
                                <thead>
                                    <tr>
                                        <th>Tache</th>
                                        <th class="d-none d-sm-table-cell" style="width: 13%;">Client</th>
                                        <th class="d-none d-sm-table-cell" style="width: 13%;">Backofficer</th>
                                        <th class="d-none d-sm-table-cell" style="width: 15%;">Status</th>
                                        <th class="d-none d-sm-table-cell" style="width: 20%;">Mes Documents</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($overdue_follow_up as $task_four)
                                        <tr>
                                            <?php
                                                $task_name = ucfirst(str_replace("soumission-", "", $task_four['canal']));
                                            ?>
                                            <td class="d-none d-sm-table-cell">
                                                <span><a href="{{ 'soumission/' . $task_four['customer_case_id'] }}">{{ $task_name }}</a></span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fw-semibold fs-sm">{{ $task_four["firstname"] . " " . $task_four["lastname"] }}</span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fw-semibold fs-sm">{{ $task_four["name"] ?: "n/d" }}</span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill {{ $task_four['status_class'] }}" > {{ $task_four["msg"] }} </span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-info-light text-info">{{ $task_four['canal'] }}</span>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

        </div>
</div>
