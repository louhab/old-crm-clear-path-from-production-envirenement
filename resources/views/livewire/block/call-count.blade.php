<?php
$lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title"><i class="fa fa-atom me-1"></i>{{ $lang === "fr" ? " Log Nombre d'appels par status" : " Log Call count per stat"}}</h3>
    </div>
    @if(count($calls))
        <div class="block-content custom_table" style="padding: 20px;">
            <table class="table table-sm table-vcenter">
                <thead>
                <tr class="calls">
                    <th class="d-none d-sm-table-cell">
                        {{ $lang === "fr" ? "Status" : "State"}}
                    </th>
                    <th  class="d-sm-table-cell">
                        {{ $lang === "fr" ? " Nombre total" : " Total number "}}
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($calls as $call)
                    <tr class="calls">
                        <td class="font-size-sm">
                            @php
                                $leadStatus = App\Models\LeadStatus::where("id", $call["lead_state_id"])->first();
                                //dd($lead);
                                $badge = "danger";
                                if ($call["lead_state_id"] == 1)
                                    $badge = "success";
                                if ($call["lead_state_id"] == 2)
                                    $badge = "light";
                                if ($call["lead_state_id"] == 3)
                                    $badge = "secondary";
                                if ($call["lead_state_id"] == 4)
                                    $badge = "info";
                                if ($call["lead_state_id"] == 5)
                                    $badge = "warning";
                                if ($call["lead_state_id"] == 7)
                                    $badge = "activation";
                                if (!$leadStatus)
                                    $badge = "info";
                                // $s = \App\Models\LeadStatus::find($state->state_id);
                            @endphp
                            <p class="text-muted mb-0">
                                <span class="badge badge-{{ $badge }}">{{ $leadStatus? $leadStatus->status_label_fr: "null" }}</span>

                            </p>
                        </td>
                        <td class="font-size-sm">
                            {{ $call["total"] }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
