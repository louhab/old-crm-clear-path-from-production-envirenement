<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
<div class="block block-rounded js-ecom-div-cart d-none d-xl-block">
    <div class="block-header block-header-default">
        <h3 class="block-title">
            {{ $lang === "fr" ? " Message WhatsApp" : " MESSAGE WHATSAPP"}}
        </h3>
    </div>
    <div class="block-content overflow-auto" style="height: 100%;max-height: 40em;padding: 20px;">
        <div class="row items-push">
            <div class="col-md-12">
                @if($case_state->status)
                    <div class="alert alert-success">
                        <i class="far fa-check-circle"></i>
                        {{ $lang === "fr" ? " Message envoyé" : " Message sent"}}
                    </div>
                @endif
                <div>
                    <button type="button" class="btn btn-outline-danger btn-block case-state-update" data-case="{{$case_state->id}}">
                        {{ $lang === "fr" ? "Envoyer un WhatsApp" : "Send WhatsApp"}}
                    </button>
                </div>
            </div>
        </div>
    </div>

</div>
