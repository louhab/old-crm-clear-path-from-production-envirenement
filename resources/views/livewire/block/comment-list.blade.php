<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title"><i class="far fa-comments me-1"></i> Listes des commentaires</h3>
    </div>
    @if(count($comments))
        <div class="block-content custom_table" style="padding: 20px;">
            <table class="table table-sm table-vcenter">
                <thead>
                <tr class="calls">
                    <th class="d-none d-sm-table-cell" style="width: 20%;">
                        Date de commentaire
                    </th>
                    <th style="width: 45%;">
                        Commentaire
                    </th>
                    <th>
                        Client
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($comments as $comment)
                    <tr class="comment">
                        <td class="d-none d-sm-table-cell" style="width: 20%;">
                            <em class="font-size-sm text-muted">{{ $comment->created_at }}</em>
                        </td>
                        <td class="font-size-sm" style="width: 45%;">
                            <p class="text-muted mb-0">
                                {{ $comment->comment }}
                            </p>
                        </td>
                        <td class="font-size-sm">
                            <p class="text-muted mb-0">
                                @php $customer = App\Models\Customer::find($comment->customer_case_id) @endphp
                                {{ $customer->firstname . " " . $customer->lastname}}
                            </p>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
