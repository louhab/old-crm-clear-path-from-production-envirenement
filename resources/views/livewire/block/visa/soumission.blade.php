<div class="block block-rounded">
    <div class="block-header">
        @if ($soumission)
            <h3 class="block-title"><i class="fa fa-satellite"></i> Soumission de la demande de Visa</h3>
        @else
            <h3 class="block-title"><i class="fa fa-satellite"></i> Suivi de la demande de Visa</h3>
        @endif
    </div>
    <div class="block-content">
        @if (count($docs))
            <table class="table table-sm table-vcenter">
                @foreach ($docs as $doc)
                    @php $doc = json_decode(json_encode($doc), true); @endphp
                    @php
                        $collectionName = $doc['collection_name'];
                        $media = $customer_case->customer->getFirstMedia($collectionName);
                    @endphp
                    @if (
                        $soumission ||
                            (in_array($doc['collection_name'], [
                                'ol0U5FzljIUCiOkEjflLtoQGyBv6uW',
                                'aKvXGxaNjbhLRGzOONs7gL77Pq5BjU',
                                'M2CYMFb8kg680HeqL0H8L5GjqCJN8V',
                                '2bOlUxdb73gNkQGWuDOnXeSwdHVhZC',
                                '83B80vRhPfqaJ1liglDIRWACYyNWrY',
                            ]) &&
                                $customer_case->program_id === 1) ||
                            (!empty($media) && $customer_case->program_id === 1) ||
                            $customer_case->program_id === 4)
                        {{-- @if (true) --}}
                        <tr class="custom_tr">
                            <td class="font-size-sm custom_td" style="width: 45%;">
                                {{-- {{ $doc['name_fr'] }} - {{ $doc['collection_name'] }} --}}
                                {{ $doc['name_fr'] }}
                            </td>
                            <td class="d-none d-sm-table-cell custom_td">

                                @if (!empty($media))
                                    <a class="btn btn-sm btn-light document-in"
                                        href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                                        <i class="fa fa-fw fa-download text-success"></i>
                                    </a>
                                    @if (auth()->user()->isAdmin() ||
                                            auth()->user()->isVerificateur() ||
                                            auth()->user()->isManager() ||
                                            auth()->user()->isBackoffice())
                                        <a class="btn btn-sm btn-light document-in delete-document"
                                            href="javascript:void(0)" wire:click="deleteMedia('{{ $collectionName }}')">
                                            <i class="fa fa-fw fa-times text-danger"></i>
                                        </a>
                                    @endif
                                    @if (
                                        !$media->getCustomProperty('customer_owner') &&
                                            (Illuminate\Support\Facades\Auth::user()->isAdmin() ||
                                                Illuminate\Support\Facades\Auth::user()->isVerificateur() ||
                                                Illuminate\Support\Facades\Auth::user()->isManager()))
                                        @if ($media->getCustomProperty('visible_for_customer'))
                                            <a class="btn btn-sm btn-light document-in toggle-property"
                                                href="javascript:void(0)">
                                                <i class="fa fa-fw fa-eye text-success"></i>
                                            </a>
                                        @else
                                            <a class="btn btn-sm btn-light document-in toggle-property"
                                                href="javascript:void(0)">
                                                <i class="fa fa-fw fa-eye-slash text-danger"></i>
                                            </a>
                                        @endif
                                    @endif
                                @else
                                    @if (
                                        ($currentRouteName != 'soumission-view' && $doc['customer_owner'] == 1) ||
                                            ($currentRouteName == 'soumission-view' && $doc['customer_owner'] == 0))
                                        @livewire('customer.partials.upload', ['customer_case' => $customer_case, 'collectionName' => $collectionName])
                                    @else
                                        <span
                                            class="badge badge-primary m-2">{{ __('lang.portal_in_progress_badge') }}</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
            </table>
        @endif
    </div>
</div>
