<?php
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
if (!function_exists('safeCheck')) {
    function safeCheck($to_look_for, $to_look_in)
    {
        if (array_key_exists('key', $to_look_in[$to_look_for]) && array_key_exists('value', $to_look_in[$to_look_for])) {
            return $to_look_in[$to_look_for]['value'];
        } else {
            if (count($to_look_in[$to_look_for]['values'])) {
                return $to_look_in[$to_look_for]['values'][0];
            }
            return '';
        }
        return '';
    }
}

?>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-vcenter">
            <thead>
                <tr>
                    @if ($meet != 'R6')
                        <th class="text-center text-light" style="background:#d52b26;" colspan="2">{{ $title }}
                        </th>
                    @else
                        <th class="text-center text-light" style="background:#d52b26;" colspan="2">Rencontre 6 :
                            Préparation et planification de séjour</th>
                    @endif
                </tr>
            </thead>
        </table>
    </div>
    <div class="col-sm-6">

        <div class="block block-rounded">
            <div class="block-header">
                <h3 class="block-title"> {{ __('lang.customer_portal_docs_to_consolt') }} </h3>
            </div>

            @if ($meet != 'R6')

                <div class="block-content">
                    @if ($meet == 'R2')
                        <table class="table table-bordered table-vcenter">
                            <tbody>
                                @if ($customer_case->program_id == 4)
                                    @php
                                        $doc = \App\Models\DocumentType::where('name_fr', 'Clear1002 - Orientation')->first();
                                        $media = $customer_case->customer->getFirstMedia($doc->collection_name);
                                    @endphp
                                    <tr>
                                        <th class="text-center" scope="row" width="85%">Clear1002 - Orientation
                                        </th>
                                        <td class="text-center document-td">
                                            @if (!empty($media))
                                                @include('cases.steps.partials.document', [
                                                    'document' => $doc->collection_name_fr,
                                                    'collectionName' => $doc->collection_name,
                                                    'media' => $media,
                                                ])
                                            @endif
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <th class="text-center" scope="row" width="85%">CLEAR11 - PROCÉDURE POUR
                                            DEMANDER L'ÉQUIVALENCE DES DIPLÔMES</th>
                                        <td class="text-center">
                                            <a class="btn btn-sm btn-light"
                                                href="{{ asset('/pdf/' . "Clear11 - Phase 1-Procédure pour demander l'équivalence des diplomes.pdf") }}"
                                                target="_blank">
                                                <i class="fa fa-fw fa-download text-success"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endif

                                {{-- @php
                                    $sheets_bis = \App\Models\InformationSheet::with('substeps');
                                @endphp
                                @foreach ($sheets_bis->get() as $sheet)
                                    @if (!$sheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                                        @if ($sheet->id == 3)
                                            <tr>
                                                <th class="text-center" scope="row" width="85%">
                                                    @if ($customer_case->program_id === 4)
                                                        Clear 3 -
                                                        {{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }}s
                                                    @else
                                                        Clear 13 -
                                                        {{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }}s
                                                    @endif
                                                </th>
                                                <td class="text-center document-td">
                                                    <a class="btn btn-sm btn-outline-secondary"
                                                        href="{{ route('customer-edit-information-sheet', ['customer_case' => $customer_case, 'information_sheet' => $sheet]) }}"
                                                        target="_blank">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @else
                                            <tr>
                                                <th class="text-center" scope="row" width="85%">
                                                    @if ($sheet->id == 3)
                                                        Clear13 -
                                                        {{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }}s
                                                    @else
                                                        {{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }}
                                                    @endif
                                                </th>
                                                <td class="text-center document-td">
                                                    <a class="btn btn-sm btn-outline-secondary"
                                                        href="{{ route('customer-edit-information-sheet', ['customer_case' => $customer_case, 'information_sheet' => $sheet, 'join' => 'Couple']) }}"
                                                        target="_blank">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach --}}
                            </tbody>
                        </table>
                    @endif
                </div>
                {{-- </div> --}}
            @endif

            {{-- bill --}}
            <div class="block-content">
                <table class="table table-bordered table-vcenter">
                    <tbody>
                        @php
                            $doc_recu = \App\Models\DocumentType::where('name_fr', 'Reçu')->first();
                            $payment_name = $state->name_fr;
                            $recu = $payment_name . $doc_recu->collection_name;
                            $media = $customer_case->customer->getFirstMedia($recu);
                            if (empty($media)) {
                                $recu = $doc_recu->collection_name;
                                $media = $customer_case->customer->getFirstMedia($recu);
                            }
                        @endphp
                        <tr>
                            <th class="text-center" scope="row" width="85%">
                                {{ __('lang.customer_portal_bill_title') }}</th>
                            <td class="text-center document-td">
                                @if (!empty($media))
                                    @include('cases.steps.partials.document', [
                                        'document' => $doc_recu->collection_name_fr,
                                        'collectionName' => $recu,
                                        'media' => $media,
                                    ])
                                @endif
                            </td>
                        </tr>
                        @if ($meet === 'R6')
                            <tr>
                                <th class="text-center" scope="row" width="85%">
                                    Devis </th>
                                <td class="text-center document-td"><a class="btn btn-sm btn-light"
                                        href="{{ asset('/pdf/' . 'CA-Devis-package-service-d-accueil-et-premieres-deemarches-MAD.pdf') }}"
                                        target="_blank">
                                        <i class="fa fa-fw fa-download text-success"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>

            {{-- Meet Docs --}}
            @if ($meet != 'R6')
                <div class="block-content">
                    <table class="table table-bordered table-vcenter">
                        <tbody>
                            @php
                                $step = \App\Models\ProgramStep::where('id', $substeps[0]->step_id)->first();
                                $docs = new \Illuminate\Database\Eloquent\Collection();
                                
                                foreach ($substeps as $substep) {
                                    $docs = $docs->merge(
                                        $substep
                                            ->docs()
                                            ->wherePivot('program_id', '=', $customer_case->program_id)
                                            ->withPivot('order')
                                            ->where('customer_owner', 0)
                                            ->orderBy('order', 'asc')
                                            ->get(),
                                    );
                                }
                                unset($substep);
                            @endphp
                            @foreach ($docs as $doc)
                                @if (!in_array($doc->id, [148, 150]))
                                    <tr>
                                        <th class="text-center" scope="row" width="85%">
                                            {{ $lang === 'fr' ? $doc->name_fr : $doc->name_en }}</th>
                                        <td class="text-center document-td">
                                            @php
                                                $collectionName = $doc->collection_name;
                                                $media = $customer_case->customer->getFirstMedia($collectionName);
                                            @endphp

                                            @if (!empty($media))
                                                @include('cases.steps.partials.show-file', [
                                                    'document' => $doc->collection_name_fr,
                                                    'model' => $customer_case->customer,
                                                    'collectionName' => $collectionName,
                                                    'formId' => Str::random(10),
                                                    'signable' => $media->getCustomProperty('signable'),
                                                    'customer_case' => $customer_case,
                                                    'media_name' =>
                                                        $doc->name_fr == 'Devis' ? $doc->name_fr : null,
                                                ])
                                            @endif

                                            @if ($doc->name_fr == 'Devis')
                                                @php $m = $customer_case->customer->getFirstMedia($doc->collection_name); @endphp
                                                @if ($m && $m->hasCustomProperty('devis'))
                                                    @if ($m->getCustomProperty('devis') == 'accepted')
                                                        <a class="btn btn-sm btn-light text-success"
                                                            href="javascript:void(0)">
                                                            Accepted!
                                                        </a>
                                                    @else
                                                        <a class="btn btn-sm btn-light text-danger"
                                                            href="javascript:void(0)">
                                                            vous avez refusée le devis!
                                                        </a>
                                                    @endif
                                                @else
                                                    <a class="btn btn-sm btn-light" href="javascript:void(0)"
                                                        id="enable-quote" data-url="{{ route('enable-quote') }}">
                                                        <i class="far fa-2x fa-check-circle text-success"></i>
                                                    </a>
                                                    <a class="btn btn-sm btn-light" href="javascript:void(0)">
                                                        <i class="fa fa-fw fa-times text-danger"></i>
                                                    </a>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>

            @endif
        </div>

        @if ($meet == 'R3' && $customer_case->program_id == 4)
            @php
                $docs = ['Lettre modèle', 'Accusé de réception', 'Reçu', 'Lettre de suivi', "Lettre d'acceptation", 'Lettre de refus'];
            @endphp
            <div class="block block-rounded">
                <div class="block-header">
                    <h3 class="block-title"> {{ __('lang.customer_portal_docs_to_consolt') }} </h3>
                </div>
                <div class="block-content">
                    @foreach ($customer_case->customer->schools()->withPivot('admis')->get() as $program)
                        <table class="table table-bordered table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center text-light" style="background:#d52b26;" colspan="2">
                                        Admission {{ $loop->index + 1 }}: {{ $program->programme }}</th>
                                </tr>
                            </thead>
                        </table>
                        <table class="table table-bordered table-vcenter">
                            <tbody>
                                @foreach ($docs as $doc)
                                    @php
                                        $collectionName = $doc . '-course-' . $program->id;
                                        $media = $customer_case->customer->getFirstMedia($collectionName);
                                    @endphp
                                    <tr>
                                        <th class="text-center" scope="row" width="85%">
                                            {{ $doc }}
                                        </th>
                                        <td class="text-center document-td">
                                            @if (!empty($media))
                                                @include('cases.steps.partials.document', [
                                                    'document' => $collectionName,
                                                    'collectionName' => $collectionName,
                                                    'media' => $media,
                                                ])
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endforeach
                    @foreach ($customer_case->customer->courses()->withPivot('admis', 'is_confirmed')->get() as $program)
                        @if ($program->pivot->is_confirmed)
                            <table class="table table-bordered table-vcenter">
                                <thead>
                                    <tr>
                                        <th class="text-center text-light" style="background:#d52b26;" colspan="2">
                                            Admission {{ $loop->index + 1 }}:
                                            {{ $program->properties['diplome']['value'] ?? 'Diplôme' }} -
                                            {{ $program->name }}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($docs as $doc)
                                        @php
                                            $collectionName = $doc . '-course-' . $program->id;
                                            $media = $customer_case->customer->getFirstMedia($collectionName);
                                        @endphp
                                        <tr>
                                            <th class="text-center" scope="row" width="85%">
                                                {{ $doc }}
                                            </th>
                                            <td class="text-center document-td">
                                                @if (!empty($media))
                                                    @include('cases.steps.partials.document', [
                                                        'document' => $collectionName,
                                                        'collectionName' => $collectionName,
                                                        'media' => $media,
                                                    ])
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <table class="table table-bordered table-vcenter">
                                <thead>
                                    <tr>
                                        <th class="text-center text-light" style="background:#ecb62b;" colspan="2">
                                            Admission {{ $loop->index + 1 }}:
                                            {{ $program->properties['diplome']['value'] ?? 'Diplôme' }} -
                                            {{ $program->name }}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="col-md-12">
                                                <div class="block block-rounded">
                                                    <div class="block-header">
                                                        <h3 class="block-title">
                                                            {{ $program->name }}
                                                            <small>
                                                                {{ safeCheck('type_etablissement', $program['properties']) }}
                                                                -
                                                                {{ safeCheck('nom_etablissement', $program['properties']) }}
                                                            </small>
                                                        </h3>
                                                        <div class="block-options">
                                                            <button title="Confirmer programme"
                                                                class="btn btn-sm btn-light"
                                                                wire:click="confirmProgram({{ $program }})">
                                                                <i class="fas fa-check text-success"></i>
                                                                Confirmer
                                                                programme
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        @endif
                    @endforeach
                </div>
            </div>
        @endif

        @if ($meet == 'R1' && $customer_case->program_id == 1)
            @livewire('block.cnp', ['customer_case' => $customer_case])
        @endif

    </div>

    @php
        $sheets = \App\Models\InformationSheet::with('substeps');
    @endphp

    {{-- Left part --}}

    <div class="col-sm-6">
        <div class="row">
            {{-- @if ($sheets->count()) --}}
            {{-- 🔥 Formulaire a remplir --}}
            <div class="col-sm-12">
                <div class="block block-rounded">
                    <div class="block-header">
                        <h3 class="block-title">{{ __('lang.customer_portal_form_to_fill') }}</h3>
                    </div>
                    <div class="block-content">
                        <table class="table table-bordered table-vcenter">
                            <tbody>
                                @if ($customer_case->program_id !== 4 && $meet == 'R2')
                                    {{-- Clear 13 --}}
                                    @php
                                        $sheets_bis = \App\Models\InformationSheet::with('substeps');
                                    @endphp
                                    @foreach ($sheets_bis->get() as $sheet)
                                        @if (!$sheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                                            @if ($sheet->id == 3)
                                                <tr>
                                                    <th class="text-center" scope="row" width="85%">
                                                        Clear 13 -
                                                        {{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }}s
                                                    </th>
                                                    <td class="text-center document-td">
                                                        <a class="btn btn-sm btn-outline-secondary"
                                                            href="{{ route('customer-edit-information-sheet', ['customer_case' => $customer_case, 'information_sheet' => $sheet]) }}"
                                                            target="_blank">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <th class="text-center" scope="row" width="85%">
                                                        {{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }}
                                                    </th>
                                                    <td class="text-center document-td">
                                                        <a class="btn btn-sm btn-outline-secondary"
                                                            href="{{ route('customer-edit-information-sheet', ['customer_case' => $customer_case, 'information_sheet' => $sheet]) }}"
                                                            target="_blank">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endif
                                    @endforeach
                                @else
                                    @foreach ($sheets->get() as $sheet)
                                        @if (!$sheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', $sheet_steps)->get()->isEmpty())
                                            <tr>
                                                @if ($customer_case->program_id == 4)
                                                    <th class="text-center" scope="row" width="85%">
                                                        @if ($sheet->id == 3)
                                                            Clear 3 -
                                                            {{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }}s
                                                        @else
                                                            {{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }}
                                                        @endif
                                                    </th>
                                                @else
                                                    <th class="text-center" scope="row" width="85%">
                                                        @if ($sheet->id == 3)
                                                            Clear 13 -
                                                            {{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }}s
                                                        @else
                                                            {{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }}
                                                        @endif
                                                    </th>
                                                @endif
                                                <td class="text-center document-td">
                                                    <a class="btn btn-sm btn-outline-secondary"
                                                        href="{{ route('customer-edit-information-sheet', ['customer_case' => $customer_case, 'information_sheet' => $sheet]) }}"
                                                        target="_blank">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                                {{--
                                @if ($customer_case->program_id !== 4 && $meet == 'R1')
                                    @php
                                        $sheet = \App\Models\InformationSheet::find(9);
                                    @endphp
                                    <tr>
                                        <th class="d-none d-sm-table-cell">
                                            <span>{{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }} (Single)
                                            </span>
                                        </th>
                                        <td class="text-center document-td">
                                            <a class="btn btn-sm btn-outline-secondary"
                                                href="{{ route('customer-edit-information-sheet', ['customer_case' => $customer_case, 'information_sheet' => $sheet]) }}"
                                                target="_blank">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endif --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{-- @endif --}}
            {{-- @if ($customer_case->program_id !== 4) --}}

            {{-- 🔥 Documents à téléverser --}}
            <div class="col-sm-12">
                <div class="block block-rounded">
                    <div class="block-header">
                        <h3 class="block-title">{{ __('lang.customer_portal_form_to_upload') }}</h3>
                    </div>
                    <div class="block-content">
                        <table class="table table-bordered table-vcenter">
                            <tbody>
                                @php
                                    $media_sheets = \App\Models\MediaSheet::with('substeps');
                                @endphp
                                @foreach ($media_sheets->get() as $mediaSheet)
                                    @if (!$mediaSheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', $steps)->get()->isEmpty())
                                        @php
                                            $sheet = $customer_case->customer->mediaSheets()->wherePivot('media_sheet_id', '=', $mediaSheet->id);
                                            if (count($sheet->get())) {
                                                $customer_case->customer->mediaSheets()->updateExistingPivot($mediaSheet->id, [
                                                    'can_edit' => true,
                                                ]);
                                            } else {
                                                $customer_case->customer->mediaSheets()->attach([
                                                    $mediaSheet->id => [
                                                        'can_edit' => true,
                                                    ],
                                                ]);
                                            }
                                        @endphp
                                        @if ($mediaSheet->id !== 4 && $mediaSheet->id !== 6 && $mediaSheet->id !== 5)
                                            <tr>
                                                <th class="text-center" scope="row" width="85%">
                                                    @if ($mediaSheet->id == 10)
                                                        Clear12 -
                                                        {{ $lang === 'fr' ? $mediaSheet->name_fr : $mediaSheet->labelEng }}
                                                    @else
                                                        {{ $lang === 'fr' ? $mediaSheet->name_fr : $mediaSheet->labelEng }}
                                                    @endif
                                                </th>
                                                <td class="text-center document-td">
                                                    <a class="btn btn-sm btn-outline-secondary"
                                                        href="{{ route('customer-edit-media-sheet', ['customer_case' => $customer_case, 'media_sheet' => $mediaSheet]) }}"
                                                        target="_blank">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                        <table class="table table-bordered table-vcenter">
                            <tbody>
                                @php
                                    $step = \App\Models\ProgramStep::where('id', $substeps[0]->step_id)->first();
                                    $docs = new \Illuminate\Database\Eloquent\Collection();
                                    foreach ($substeps as $substep) {
                                        $docs = $docs->merge(
                                            $substep
                                                ->docs()
                                                ->wherePivot('program_id', '=', $customer_case->program_id)
                                                ->withPivot('order')
                                                ->where('customer_owner', 1)
                                                ->orderBy('order', 'asc')
                                                ->get(),
                                        );
                                    }
                                    unset($substep);
                                @endphp
                                @foreach ($docs as $doc)
                                    @if (!in_array($doc->id, [147, 149]))
                                        <tr>
                                            <th class="text-center" scope="row" width="85%">
                                                {{ $lang === 'fr' ? $doc->name_fr : $doc->name_en }}</th>

                                        </tr>
                                        <tr>
                                            <td class="text-center document-td">
                                                @php
                                                    $collectionName = $doc->collection_name;
                                                    $media = $customer_case->customer->getFirstMedia($collectionName);
                                                @endphp
                                                @include('cases.steps.partials.document', [
                                                    'document' => $doc_recu->collection_name_fr,
                                                    'collectionName' => $collectionName,
                                                    'media' => $media,
                                                ])
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{-- @endif --}}
        </div>
    </div>

    @if ($meet == 'R5')
        <div class="col-sm-6">
            <div class="block block-rounded">
                <div class="block-content">
                    <table class="table table-bordered table-vcenter">
                        <tbody>
                            @php
                                $docs = DB::table('document_types as dt')
                                    ->leftJoin('document_type_program_sub_step as dp', 'dp.document_type_id', '=', 'dt.id')
                                    ->whereIn('dp.program_sub_step_id', [32, 33])
                                    ->where('dp.program_id', $customer_case->program_id)
                                    ->orderBy('order')
                                    ->get();
                            @endphp
                            @foreach ($docs as $doc)
                                @if (
                                    (in_array($doc->collection_name, [
                                        'ol0U5FzljIUCiOkEjflLtoQGyBv6uW',
                                        'aKvXGxaNjbhLRGzOONs7gL77Pq5BjU',
                                        'M2CYMFb8kg680HeqL0H8L5GjqCJN8V',
                                        '2bOlUxdb73gNkQGWuDOnXeSwdHVhZC',
                                        '83B80vRhPfqaJ1liglDIRWACYyNWrY',
                                    ]) &&
                                        $customer_case->program_id === 1) ||
                                        $customer_case->program_id === 4)
                                    <tr>
                                        <th class="text-center" scope="row" width="85%">
                                            {{ $doc->name_fr }}
                                            {{-- {{ $doc->collection_name }} --}}
                                        </th>
                                        <td class="text-center document-td">
                                            @php
                                                $collectionName = $doc->collection_name;
                                                $media = $customer_case->customer->getFirstMedia($collectionName);
                                            @endphp
                                            @if (!empty($media))
                                                <a class="btn btn-sm btn-light document-in"
                                                    href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                                                    <i class="fa fa-fw fa-download text-success"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
</div>
