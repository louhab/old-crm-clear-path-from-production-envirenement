<div class="block block-rounded">
    <div class="block-header  block-header-default">
        <h3 class="block-title"><i class="fa fa-tag me-1"></i> {{ __('lang.portal_noc') }}</h3>
        <div class="block-options">
            <div>
                <a class="btn btn-sm btn-alt-primary" href="{{ __('lang.portal_noc_link') }}" target="_blank">
                    {{ __('lang.portal_find_your_noc') }}
                </a>
            </div>
        </div>
    </div>
    <div class="block-content block-content-full">
        <div class="row">
            <div class="col-lg-4">
                <label class="form-label" for="example-text-input-alt">{{ __('lang.portal_your_noc') }}</label>
                <input type="text" wire:model.defer="cnp" class="form-control form-control-alt" id="cnp">
            </div>
            <div class="col-lg-5">
                <label class="form-label" for="example-password-input-alt">{{ __('lang.portal_skill_level') }}</label>
                <input type="text" wire:model.defer="level" class="form-control form-control-alt" id="level">
            </div>
            <div class="col-lg-2">
                <button type="button" wire:click="save"
                    class="btn btn-outline-danger mt-4">{{ __('lang.portal_noc_save') }}</button>
            </div>
        </div>
    </div>
</div>
