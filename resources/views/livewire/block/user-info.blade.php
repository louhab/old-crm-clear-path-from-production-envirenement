<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title">
            {{ __("lang.portal_personal_infos_title") }}
        </h3>
    </div>
    <div class="block-content" style="padding: 20px;">
        <div id="advisor-info">
            <table class="table table-sm table-vcenter">
                <tbody>
                <tr>
                    <td class="p-2">Nom : <strong>{{$user->name}}</strong></td>
                </tr>
                <tr>
                    <td class="p-2">Téléphone : <strong>{{$user->phone}}</strong></td>
                </tr>
                <tr>
                    <td class="p-2">Date de creation : <strong>{{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y') }}</strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
