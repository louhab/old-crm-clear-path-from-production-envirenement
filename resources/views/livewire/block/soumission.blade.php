<div class="block block-rounded">
    @if ($customer_case->program_id == 4)
        <div class="block-header block-header-default">
            <h3 class="block-title"><i class="fa fa-bookmark" style="color:red;font-size:1em;margin-right:10px;"></i>
                Suivie de la demande d’admission</h3>
            <div class="block-options">
                @if (
                    !(auth()->user()->isAdmin() ||
                        auth()->user()->isVerificateur()
                    ) && $case_state->status)
                    <div class="block-options-item">
                        <i class="fa fa-fw fa-lock text-danger"></i>
                    </div>
                @endif
            </div>
        </div>
    @endif
    <div class="block-content">
        @if ($customer_case->program_id == 4)
            @foreach ($customer_case->customer->schools()->withPivot('admis')->get() as $program)
                <div class="block block-rounded">
                    <div class="block-header">
                        <h3 class="block-title">{{ $program->programme }}</h3>
                        <div class="block-options">
                            @if (is_null($program->pivot->admis))
                                @if ($currentRouteName == 'soumission-view')
                                    <button type="button" class="btn btn-sm btn-alt-success"
                                        wire:click="updateProgram({{ $program }}, 'Admis')">Admis</button>
                                    <button type="button" class="btn btn-sm btn-alt-danger"
                                        wire:click="updateProgram({{ $program }}, 'Refus')">Refus</button>
                                @endif
                            @else
                                @if ($program->pivot->admis)
                                    <div class="block-options-item text-success">Admis!</div>
                                @else
                                    <div class="block-options-item text-danger">Refus!</div>
                                @endif
                                @if ($currentRouteName == 'soumission-view')
                                    <button type="button" class="btn-block-option"
                                        wire:click="updateProgram({{ $program }}, 'Reset')">
                                        <i class="si si-close"></i>
                                    </button>
                                @endif
                            @endif
                        </div>
                    </div>
                    @if ($currentRouteName == 'soumission-view')
                        <div class="block-content">
                            @if (!is_null($program->pivot->admis))
                                @php
                                    if ($program->pivot->admis) {
                                        $docs = ['Lettre de suivi', "Lettre d'acceptation"];
                                    } else {
                                        $docs = ['Lettre de refus'];
                                    }
                                @endphp
                                <table class="table table-vcenter">
                                    @foreach ($docs as $doc)
                                        <tr>
                                            <td>
                                                {{ $doc }}
                                            </td>
                                            <td>
                                                <div class="d-flex justify-content-center action">
                                                    <div>
                                                        @php
                                                            $collectionName = $doc . '-admission-' . $program->id;
                                                            $media = $customer_case->customer->getFirstMedia($collectionName);
                                                            // dd($media);
                                                        @endphp
                                                        @if (!empty($media))
                                                            <a class="btn btn-sm btn-light document-in"
                                                                href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                                                                <i class="fa fa-fw fa-download text-success"></i>
                                                            </a>
                                                            @if (auth()->user()->isAdmin() ||
                                                                    auth()->user()->isVerificateur() ||
                                                                    auth()->user()->isManager() ||
                                                                    auth()->user()->isBackoffice())
                                                                <a class="btn btn-sm btn-light document-in delete-document"
                                                                    href="javascript:void(0)"
                                                                    wire:click="deleteMedia('{{ $collectionName }}')">
                                                                    <i class="fa fa-fw fa-times text-danger"></i>
                                                                </a>
                                                            @endif
                                                            @if (
                                                                !$media->getCustomProperty('customer_owner') &&
                                                                    (Illuminate\Support\Facades\Auth::user()->isAdmin() ||
                                                                        Illuminate\Support\Facades\Auth::user()->isVerificateur() ||
                                                                        Illuminate\Support\Facades\Auth::user()->isManager()))
                                                                @if ($media->getCustomProperty('visible_for_customer'))
                                                                    <a wire:click="toggleProperty({{ $media->id }})"
                                                                        class="btn btn-sm btn-light document-in toggle-property"
                                                                        href="javascript:void(0)">
                                                                        <i class="fa fa-fw fa-eye text-success"></i>
                                                                    </a>
                                                                @else
                                                                    <a wire:click="toggleProperty({{ $media->id }})"
                                                                        class="btn btn-sm btn-light document-in toggle-property"
                                                                        href="javascript:void(0)">
                                                                        <i
                                                                            class="fa fa-fw fa-eye-slash text-danger"></i>
                                                                    </a>
                                                                @endif
                                                            @endif
                                                        @else
                                                            @livewire('customer.partials.upload', ['customer_case' => $customer_case, 'collectionName' => $collectionName])
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            @endif
                        </div>
                    @endif
                </div>
            @endforeach
        @else
            @php
                $substep = \App\Models\ProgramSubStep::find(17);
                // dd($substep);
                $step = \App\Models\ProgramStep::where('id', $substep->step_id)->first(); // id = 6 (EE)
                // dd($step);
                $docs = $substep
                    ->docs()
                    ->wherePivot('program_id', '=', $customer_case->program_id)
                    ->withPivot('order')
                    ->orderBy('order', 'asc')
                    ->get();
                
                // dd($docs);
                
            @endphp
            @if (count($docs))
                <table class="table table-vcenter">
                    @foreach ($docs as $doc)
                        <tr>
                            <td class="font-size-sm" style="width: 65%;">
                                {{ $doc->name_fr }}
                            </td>
                            <td class="d-none d-sm-table-cell">
                                @php
                                    $collectionName = $doc->collection_name;
                                    $media = $customer_case->customer->getFirstMedia($collectionName);
                                @endphp
                                @if (!empty($media))
                                    <a class="btn btn-sm btn-light document-in"
                                        href="{{ route('get-media', $customer_case->customer->getFirstMedia($collectionName)) }}">
                                        <i class="fa fa-fw fa-download text-success"></i>
                                    </a>
                                    @if (auth()->user()->isAdmin() ||
                                            auth()->user()->isManager() ||
                                            auth()->user()->isBackoffice())
                                        <a class="btn btn-sm btn-light document-in delete-document"
                                            href="javascript:void(0)"
                                            wire:click="deleteMedia('{{ $collectionName }}')">
                                            <i class="fa fa-fw fa-times text-danger"></i>
                                        </a>
                                    @endif
                                    @if (
                                        !$media->getCustomProperty('customer_owner') &&
                                            (Illuminate\Support\Facades\Auth::user()->isAdmin() ||
                                                Illuminate\Support\Facades\Auth::user()->isVerificateur() ||
                                                Illuminate\Support\Facades\Auth::user()->isManager()))
                                        @if ($media->getCustomProperty('visible_for_customer'))
                                            <a class="btn btn-sm btn-light document-in toggle-property"
                                                href="javascript:void(0)">
                                                <i class="fa fa-fw fa-eye text-success"></i>
                                            </a>
                                        @else
                                            <a class="btn btn-sm btn-light document-in toggle-property"
                                                href="javascript:void(0)">
                                                <i class="fa fa-fw fa-eye-slash text-danger"></i>
                                            </a>
                                        @endif
                                    @endif
                                @else
                                    @if ($currentRouteName == 'soumission-view')
                                        @livewire('customer.partials.upload', ['customer_case' => $customer_case, 'collectionName' => $collectionName])
                                    @else
                                        <span
                                            class="badge badge-primary m-2">{{ __('lang.portal_in_progress_badge') }}</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td class="font-size-sm" style="width: 45%;">
                            Score
                        </td>
                        <td class="d-none d-sm-table-cell">
                            <input type="number" wire:model="score" class="form-control" id="block-form6-username"
                                name="block-form6-username" placeholder="Score..">
                            @error('score')
                                <span class="text-success">{{ $message }}</span>
                            @enderror
                        </td>
                    </tr>
                </table>
            @endif
        @endif
    </div>
</div>
