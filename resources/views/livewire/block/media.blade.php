<?php
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
?>

<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title">
            <i class="fa fa-2x fa-comments me-1"></i> {{ __('lang.customer_meet_title') }} 1
        </h3>
        @if (auth()->user()->isAdmin() ||
                auth()->user()->isVerificateur() ||
                $checkClear)
            <div class="block-options">

                @if (
                    !empty($customer_case->customer->getFirstMedia($docs[0]->collection_name)) &&
                        !in_array($customer_case->customer->lead->lead_status_id, [7, 8, 5]))
                    <a class="btn btn-sm btn-outline-success" wire:click="toggleStatus('Admissible')" href="#">
                        <i class="far fa-smile-beam me-1"></i> Admissible
                    </a>
                    <a class="btn btn-sm btn-outline-danger" wire:click="toggleStatus('Inadmissible')" href="#">
                        <i class="far fa-frown me-1"></i> Inadmissible
                    </a>
                @endif
                @if (in_array($customer_case->customer->lead->lead_status_id, [7, 5]))
                    <span style="margin: 0 12px 0 12px;">
                        <span class="alert alert-success py-1 mb-0">
                            <i class="fa fa-check-circle mr-1" style="font-size: 15px;"></i> Admissible
                        </span>
                    @elseif($customer_case->customer->lead->lead_status_id == 8)
                        <span class="alert alert-danger py-1 mb-0">
                            <i class="fa fa-check-circle mr-1" style="font-size: 15px;"></i> Inadmissible
                        </span>
                @endif
                </span>

                <button type="button" class="btn btn-sm btn-alt-primary infos-modal-btn" id="clear-id">
                    @if (count($docs) && !empty($customer_case->customer->getFirstMedia($docs[1]->collection_name)))
                        {{ __('lang.customer_view_btn') }}
                    @else
                        {{ __('lang.customer_fill_btn') }}
                        @endif clear @if (
                            $customer_case->customer->program_id == 4 ||
                                $customer_case->customer->program_id == 31 ||
                                $customer_case->customer->program_id == 32)
                            1001
                        @elseif($customer_case->customer->program_id == 9)
                            1010
                        @else
                            1004
                        @endif
                </button>
            </div>
        @else
            <div class="block-options">
                <div class="block-options-item">
                    <i class="fa fa-fw fa-lock text-danger"></i>
                </div>
            </div>
        @endif
    </div>
    @if (count($docs))
        @if (!empty($customer_case->customer->getFirstMedia($docs[1]->collection_name)))
            <div class="block-content" style="padding: 20px;">
                <table class="table table-sm table-vcenter">
                    {{-- <thead>
                        <tr class="calls">
                            <th class="d-none d-sm-table-cell" style="width: 30%;"> {{ __('lang.portal_doc_type') }}
                            </th>
                            <th>{{ __('lang.portal_doc_creation_date') }}</th>
                            <th class="d-none d-sm-table-cell" style="width: 42%;">Actions</th>
                        </tr>
                    </thead> --}}
                    @foreach ($docs as $doc)
                        <?php $media = $customer_case->customer->getFirstMedia($doc->collection_name); ?>
                        <tr>
                            <td class="font-size-sm">
                                <p class="text-muted mb-0">{{ $doc->name_fr }}</p>
                            </td>
                            @if (!empty($media))
                                <td class="font-size-sm">
                                    <p class="text-muted mb-0">{{ $media->created_at }}</p>
                                </td>
                            @else
                                <td class="font-size-sm"></td>
                            @endif
                            <td class="font-size-sm" style="width: 40%">
                                @if (($substep->id == 6 or $substep->id == 7) && empty($media))
                                    <a class="btn btn-light case-state-icon" href="javascript:void(0)">
                                        <i class="fa fa-sync fa-spin text-warning"></i>
                                    </a>
                                @else
                                    @if ($doc->is_link)
                                        @include('cases.steps.partials.link', [
                                            'docURL' => $doc->url,
                                            'formId' => Str::random(10),
                                            'documentLink' => $customer_case->customer->requestedDocuments()->wherePivot('program_step_id', '=', $step->id)->wherePivot('document_type_id', '=', $doc->id)->wherePivot('link', '=', 1)->get(),
                                        ])
                                    @else
                                        @include('cases.steps.partials.document', [
                                            'document' => $doc->collection_name_fr,
                                            'collectionName' => $doc->collection_name,
                                            'media' => $media,
                                            'customer_case' => $customer_case,
                                            'media_name' => $doc->name_fr == 'Devis' ? $doc->name_fr : null,
                                        ])
                                    @endif
                                @endif
                            </td>

                        </tr>
                    @endforeach
                    @if ($customer_case->program_id == 4)
                        <tr>
                            <td class="font-size-sm">
                                <p class="text-muted mb-0">Devis package service d'accueil et premières démarches</p>
                            </td>
                            <td class="font-size-sm"></td>
                            <td class="font-size-sm" style="width: 40%">
                                @if ($customer_case->customer->lead->currency_id === 2)
                                    <a class="btn btn-sm btn-light"
                                        href="{{ asset('/pdf/' . 'CA-Devis-package-service-d-accueil-et-premieres-deemarches-EUR.pdf') }}"
                                        target="_blank">
                                    @else
                                        <a class="btn btn-sm btn-light"
                                            href="{{ asset('/pdf/' . 'CA-Devis-package-service-d-accueil-et-premieres-deemarches-MAD.pdf') }}"
                                            target="_blank">
                                @endif
                                <i class="fa fa-fw fa-download text-success"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="font-size-sm">
                                <p class="text-muted mb-0">Clear - 6 : Charte Accompagnement Package Accueil</p>
                            </td>
                            <td class="font-size-sm"></td>
                            <td class="font-size-sm" style="width: 40%">
                                <a class="btn btn-sm btn-light"
                                    href="https://drive.google.com/drive/u/8/folders/1vsAhTdPqPMTwyS-feuze9oRiIICpZ3_7"
                                    target="_blank">
                                    <i class="fa fa-fw fa-download text-success"></i>
                                </a>
                            </td>
                        </tr>
                    @endif
                </table>
            </div>
        @endif
    @endif
    @if ($customer_case->program_id == 1)
        @php
            $sheet = \App\Models\InformationSheet::find(9);
        @endphp
        <div class="block-content" style="padding: 20px;">
            <table class="table table-sm table-vcenter">
                <thead>
                    <tr class="meets">
                        <th class="d-none d-sm-table-cell" style="width: 40%;">Fiche</th>
                        <th class="px-4" style="width: 20%;">Action</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <th class="d-none d-sm-table-cell">
                            <span>{{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }} (Single) </span>
                        </th>
                        <th class="d-none d-sm-table-cell">
                            @include('cases.steps.partials.sheet', [
                                'informationSheet' => $sheet,
                                'formId' => Str::random(10),
                            ])
                        </th>
                    </tr>
                    {{-- <tr>
                        <th class="d-none d-sm-table-cell">
                            <span>{{ $lang === 'fr' ? $sheet->name_fr : $sheet->name_en }} (Couple) </span>
                        </th>
                        <th class="d-none d-sm-table-cell">
                            @include('cases.steps.partials.sheet', [
                                'informationSheet' => $sheet,
                                'formId' => Str::random(10),
                                'join' => 'Couple',
                            ])
                        </th>
                    </tr> --}}
                </tbody>
            </table>
        </div>
    @endif

    @if (auth()->user()->isAdmin() ||
            auth()->user()->isVerificateur() ||
            $checkClear)
        @if (
            !empty($customer_case->customer->getFirstMedia($docs[0]->collection_name)) &&
                in_array($customer_case->customer->lead->lead_status_id, [11, 7, 5, 13, 15]))
            @if ($customer_case->customer->is_client != 1)
                <div class="block-header block-header-default">
                    <h3 class="block-title">
                        <button type="button" class="btn btn-sm btn-dark" id="faire-r2"
                            data-lead-id="{{ $customer_case->customer->lead->id }}"><i class="fa fa-arrow-right"
                                style="font-size: 13px;"></i> Faire R2</button>
                    </h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-sm btn-alt-primary" id="calendar-table-add"><i
                                class="fa fa-arrow-right" style="font-size: 13px;"></i> Fixer un rendez-vous</button>
                    </div>
                </div>
            @endif
        @endif
    @endif
    {{-- @if ($customer_case->program_id == 4)
    <!-- parents phone Lead Edit Modal -->
    <div class="modal" id="modal-parents-phone" tabindex="-1" role="dialog" aria-labelledby="modal-parents-phone" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-parents-phone">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Téléphone des parents</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row" style="display: none;" id="phone-notification">
                            <div class="col-md-3"></div>
                            <div class="col-md-6" style="text-align: center">
                                <div class="alert alert-success alert-dismissable" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    redirection vers formulaire clear1001...
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form modal-parents-phone lead edit -->
                                <form action="" method="POST" onsubmit="return false;" id="parents-phone-form">
                                    <div class="form-group">
                                        <label for="form-mother-phone">Téléphone Mère</label>
                                        <span class="text-danger">*</span>
                                        <br>
                                        <input type="tel" class="form-control form-control-alt" id="form-mother-phone" name="form-mother-phone" aria-describedby="form-mother-phone-error" aria-invalid="false">
                                        <div id="form-mother-phone-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="form-father-phone">Téléphone Père</label>
                                        <span class="text-danger">*</span>
                                        <br>
                                        <input type="tel" class="form-control form-control-alt" id="form-father-phone" name="form-father-phone" aria-describedby="form-father-phone-error" aria-invalid="false">
                                        <div id="form-father-phone-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="parents-phone-btn"><i class="fa fa-fw fa-plus mr-1"></i>Enregistrer</button>
                                    </div>
                                </form>
                                <!-- END Form modal-parents-phone Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END parents phone Add Modal -->
    @else --}}
</div>
<!-- parents phone Lead Edit Modal -->
<div class="modal" id="modal-infos" tabindex="-1" role="dialog" aria-labelledby="modal-infos" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="block block-rounded block-themed block-transparent mb-0" id="block-infos">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Infos Personnel</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    <div class="row" style="display: none;" id="infos-notification">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                redirection vers formulaire clear @if (
                                    $customer_case->customer->program_id == 4 ||
                                        $customer_case->customer->program_id == 31 ||
                                        $customer_case->customer->program_id == 32)
                                    1001
                                @else
                                    1004
                                @endif...
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="" method="POST" onsubmit="return false;" id="infos-form">
                                @method('PUT')
                                @csrf
                                <input type="hidden" name="popup" value="true">
                                <?php
                                $sheet_id = null;
                                $lead = \App\Models\Lead::where('id', $customer_case->customer->lead_id)->first();
                                $lead_lang = $lead->lang ?: 'fr';
                                ?>
                                @foreach ($groupedResultsArray as $key => $groupedFieldArr)
                                    @foreach ($groupedFieldArr as $field)
                                        @if ($field['customer_field_group_id'] == 1)
                                            @if ($loop->first)
                                                @unless (Route::is('case-edit'))
                                                    <!-- <span class="badge badge-danger">{{ $field['group']['name_fr'] }}</span> -->
                                                @endunless
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label class="d-block">Rencontre :</label>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio"
                                                                    id="example-radios-inline1" name="radios-office"
                                                                    value="office"
                                                                    @if ($customer_case->customer->lead->to_office == 'office') checked @endif>
                                                                <label class="form-check-label"
                                                                    for="example-radios-inline1">Venir Au
                                                                    Bureau</label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio"
                                                                    id="example-radios-inline2" name="radios-office"
                                                                    value="conference"
                                                                    @if ($customer_case->customer->lead->to_office == 'conference') checked @endif>
                                                                <label class="form-check-label"
                                                                    for="example-radios-inline2">Vidéo
                                                                    conférence</label>
                                                            </div>
                                                            <div id="radios-office-error" class="invalid-feedback">
                                                                {{ isset($errors) && $errors->has('radios-office') ? $errors->first('radios-office') : '' }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program_id">Programme</label>
                                                            <span class="text-danger">*</span>
                                                            {{ Form::select('program_id', \App\Models\Program::orderBy('name')->pluck('name', 'id'), old('program_id', $lead->program_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('program_id') ? ' is-invalid' : (isset($errors) && $errors->has('program_id') ? 'true' : 'false')), 'aria-describedby' => 'program_id-error', 'aria-invalid' => '']) }}
                                                            <div id="program_id-error" class="invalid-feedback">
                                                                {{ isset($errors) && $errors->has('program_id') ? $errors->first('program_id') : '' }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @support
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="conseiller">Conseiller</label>
                                                                <span class="text-danger">*</span>
                                                                <?php
                                                                $list_roles = [3];
                                                                if (
                                                                    auth()
                                                                        ->user()
                                                                        ->isAdmin() ||
                                                                    auth()
                                                                        ->user()
                                                                        ->isVerificateur() ||
                                                                    auth()
                                                                        ->user()
                                                                        ->isManager()
                                                                ) {
                                                                    array_push($list_roles, 2);
                                                                }
                                                                $queryCS = \App\Models\User::where('status', 1)->whereIn('role_id', $list_roles);
                                                                
                                                                ?>
                                                                @php $user_id = Illuminate\Support\Facades\Auth::id(); @endphp
                                                                {{ Form::select('conseiller', $queryCS->orderBy('name')->pluck('name', 'id'), old('conseiller', $lead->conseiller_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('conseiller') ? ' is-invalid' : (isset($errors) && $errors->has('conseiller') ? 'true' : 'false')), 'aria-describedby' => 'conseiller-error', 'aria-invalid' => '']) }}
                                                                <div id="conseiller-error" class="invalid-feedback">
                                                                    {{ isset($errors) && $errors->has('conseiller') ? $errors->first('conseiller') : '' }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endsupport
                                                    @admin
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="support">Support</label>
                                                                <span class="text-danger">*</span>
                                                                {{ Form::select('support',\App\Models\User::where('status', 1)->whereIn('role_id', [7])->orderBy('name')->pluck('name', 'id'),old('support', $lead->support_id),['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('support') ? ' is-invalid' : (isset($errors) && $errors->has('support') ? 'true' : 'false')), 'aria-describedby' => 'support-error', 'placeholder' => 'Support..', 'aria-invalid' => '']) }}
                                                                <div id="support-error" class="invalid-feedback">
                                                                    {{ isset($errors) && $errors->has('support') ? $errors->first('support') : '' }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endadmin
                                                </div>
                                                <div class="row">
                                            @endif
                                            @if (in_array(\Auth::user()->role_id, [
                                                    \App\Models\Role::Support,
                                                    \App\Models\Role::Agent,
                                                    \App\Models\Role::Conseiller,
                                                ]) && $field['field_name'] == 'phone')
                                            @elseif(in_array($field['field_name'], ['phone', 'father_phone', 'mother_phone']))

                                            @elseif (!in_array($field['id'], range(188, 193)))
                                                <div class="col-md-6"
                                                    @if (
                                                        $field['field_name'] == 'lead_status' &&
                                                            !in_array(\Illuminate\Support\Facades\Auth::user()->role_id, [
                                                                \App\Models\Role::Admin,
                                                                \App\Models\Role::Manager,
                                                                \App\Models\Role::Verificateur,
                                                            ])) style="display: none;" @endif>
                                                    @include('form.elements.' . $field['template'], [
                                                        'customer_field' => $field,
                                                        'field_name' => $field['field_name'],
                                                    ])
                                                </div>
                                            @endif
                                            @if ($field['field_name'] == 'phone')
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="form-phone">Téléphone</label>
                                                        <span class="text-danger">*</span>
                                                        <br>
                                                        <input type="tel"
                                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('phone') ? ' is-invalid' : '' }}"
                                                            value="{{ old('phone', $customer_case->customer->phone) }}"
                                                            id="form-phone" name="phone"
                                                            aria-describedby="phone-error" aria-invalid="false">
                                                        <div id="phone-error" class="invalid-feedback"></div>
                                                    </div>
                                                </div>
                                                @if (
                                                    $customer_case->customer->program_id == 4 ||
                                                        $customer_case->customer->program_id == 31 ||
                                                        $customer_case->customer->program_id == 32)
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="mother-phone">Téléphone Mère</label>
                                                            <span class="text-danger">*</span>
                                                            <br>
                                                            <input type="tel"
                                                                class="form-control form-control-alt{{ isset($errors) && $errors->has('mother_phone') ? ' is-invalid' : '' }}"
                                                                value="{{ old('mother_phone', $customer_case->customer->mother_phone) }}"
                                                                id="mother-phone" name="mother-phone"
                                                                aria-describedby="mother-phone-error"
                                                                aria-invalid="false">
                                                            <div id="mother-phone-error" class="invalid-feedback">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="father-phone">Téléphone Père</label>
                                                            <span class="text-danger">*</span>
                                                            <br>
                                                            <input type="tel"
                                                                class="form-control form-control-alt{{ isset($errors) && $errors->has('father_phone') ? ' is-invalid' : '' }}"
                                                                value="{{ old('father_phone', $customer_case->customer->father_phone) }}"
                                                                id="father-phone" name="father-phone"
                                                                aria-describedby="father-phone-error"
                                                                aria-invalid="false">
                                                            <div id="father-phone-error" class="invalid-feedback">
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                            @if ($field['field_name'] == 'campaign_id')
                                                @php
                                                    /*$cmps = \App\Models\LeadFormMarketingCampaign::all();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    $newCmps = $cmps->filter(function ($cmp) use ($customer_case) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        return $cmp->id == $customer_case->customer->campaign_id && ( $cmp->id == 16 || $cmp->id == 7 );
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    });*/
                                                    $campaign = \App\Models\LeadFormMarketingCampaign::find($customer_case->customer->campaign_id);
                                                    $forms = \App\Models\LeadForm::where('lead_form_marketing_campaign_id', $customer_case->customer->campaign_id)->pluck('title', 'id');
                                                @endphp
                                                <div class="col-md-6"
                                                    @if (count($forms) == 0) style="display: none" @endif>
                                                    <div class="form-group">
                                                        <label for="form">{{ $campaign->campaign_name }}</label>
                                                        <span class="text-danger">*</span>
                                                        {{ Form::select('form', $forms, old('form', $customer_case->customer->lead->influencer), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('form') ? ' is-invalid' : (isset($errors) && $errors->has('form') ? 'true' : 'false')), 'aria-describedby' => 'form-error', 'aria-invalid' => '', 'placeholder' => 'Référents']) }}
                                                        <div id="form-error" class="invalid-feedback">
                                                            {{ isset($errors) && $errors->has('form') ? $errors->first('form') : '' }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6"
                                                    @if ($customer_case->customer->campaign_id != 23) style="display: none" @endif>
                                                    <div class="form-group">
                                                        <label for="reference">Reference</label>
                                                        <span class="text-danger">*</span>
                                                        <input type="text"
                                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('reference') ? ' is-invalid' : '' }}"
                                                            id="reference" name="reference"
                                                            value="{{ old('reference', $customer_case->customer->lead->reference) }}"
                                                            placeholder="Reference.."
                                                            aria-describedby="reference-error"
                                                            aria-invalid="{{ isset($errors) && $errors->has('reference') ? 'true' : 'false' }}">
                                                        <div id="reference-error" class="invalid-feedback">
                                                            {{ isset($errors) && $errors->has('reference') ? $errors->first('reference') : '' }}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($loop->last)
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="country_id">Pays</label>
                                                        <span class="text-danger">*</span>
                                                        {{ Form::select('country_id', \App\Models\Country::orderBy('name_fr_fr')->pluck('name_fr_fr', 'id'), old('country_id', $customer_case->customer->country_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('country_id') ? ' is-invalid' : (isset($errors) && $errors->has('country_id') ? 'true' : 'false')), 'aria-describedby' => 'country_id-error', 'placeholder' => 'Pays..', 'aria-invalid' => '']) }}
                                                        <div id="country_id-error" class="invalid-feedback">
                                                            {{ isset($errors) && $errors->has('country_id') ? $errors->first('country_id') : '' }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="city">Ville</label>
                                                        <span class="text-danger">*</span>
                                                        <input type="text"
                                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('city') ? ' is-invalid' : '' }}"
                                                            id="city" name="city"
                                                            value="{{ old('city', $customer_case->customer->city) }}"
                                                            placeholder="Ville.." aria-describedby="city-error"
                                                            aria-invalid="{{ isset($errors) && $errors->has('city') ? 'true' : 'false' }}">
                                                        <div id="city-error" class="invalid-feedback">
                                                            {{ isset($errors) && $errors->has('city') ? $errors->first('city') : '' }}
                                                        </div>
                                                    </div>
                                                </div>
                                                @support_deny
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="password">Mot de passe</label>
                                                            <div class="input-group">
                                                                <input type="password"
                                                                    class="form-control form-control-alt" id="password"
                                                                    name="password" placeholder="Mot de passe">
                                                                <div class="input-group-append">
                                                                    <button type="button" class="btn btn-alt-primary"
                                                                        id="toggle-password">
                                                                        <i class="fa fa-eye" style="font-size:1em;"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endsupport_deny
                                                <?php
                                                // $program_field = \App\Models\ProgramField::find($customer_case->customer->lead->program_field);
                                                if ($customer_case->program_id == 4 || $customer_case->program_id == 31 || $customer_case->program_id == 32) {
                                                    $label = "Domaines d'études";
                                                } else {
                                                    $label = "Domaines d'emplois";
                                                }
                                                /*if($customer_case->customer->lead->program_field) {
                                                                    $old_pf = old('program_field', $lead->program_field);
                                                                } else {
                                                                    $old_pf = $label;
                                                                }*/
                                                ?>

                                                @php
                                                    $vals = \App\Models\ProgramField::where('visible', 1)
                                                        ->get()
                                                        ->groupBy('category');
                                                @endphp
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {{-- @program_fields --}}
                                                        <label for="program_field">{{ $label }}</label>
                                                        <span class="text-danger">*</span>
                                                        <select
                                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('program_field') ? ' is-invalid' : '' }}"
                                                            id="program_field" name="program_field"
                                                            aria-describedby="program_field-error"
                                                            aria-invalid="{{ isset($errors) && $errors->has('program_field') ? 'true' : 'false' }}">
                                                            <option selected disabled value="">
                                                                {{ $label }}
                                                            </option>
                                                            @foreach ($vals as $key => $value)
                                                                <optgroup label="{{ $key }}">
                                                                    @foreach ($value as $sub_key => $sub_value)
                                                                        <option
                                                                            {{ old('program_field') == $sub_value->id ? 'selected' : '' }}
                                                                            value={{ $sub_value->id }}>
                                                                            {{ $sub_value->label_fr }}
                                                                        </option>
                                                                    @endforeach
                                                                </optgroup>
                                                            @endforeach
                                                        </select>
                                                        {{-- {{ Form::select('program_field',\App\Models\ProgramField::where('visible', 1)->orderBy('label_fr')->pluck('label_fr', 'id'),old('program_field', $lead->program_field),['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('program_field') ? ' is-invalid' : (isset($errors) && $errors->has('program_field') ? 'true' : 'false')), 'placeholder' => $label, 'aria-describedby' => 'program_field-error', 'aria-invalid' => '']) }} --}}
                                                        <div id="program_field-error" class="invalid-feedback">
                                                            {{ isset($errors) && $errors->has('program_field') ? $errors->first('program_field') : '' }}
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Devise field!! -->
                                                @support_deny
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="currency_id">Devise</label>
                                                            <span class="text-danger">*</span>
                                                            {{ Form::select(
                                                                'currency_id',
                                                                \App\Models\Currency::pluck('iso', 'id'),
                                                                old('currency_id', $customer_case->customer->currency_id),
                                                                [
                                                                    'class' =>
                                                                        'form-control form-control-alt' .
                                                                        (isset($errors) && $errors->has('currency_id')
                                                                            ? ' is-invalid'
                                                                            : (isset($errors) && $errors->has('currency_id')
                                                                                ? 'true'
                                                                                : 'false')),
                                                                    'placeholder' => 'Devise',
                                                                    'aria-describedby' => 'currency_id-error',
                                                                    'aria-invalid' => '',
                                                                ],
                                                            ) }}
                                                            <div id="currency_id-error" class="invalid-feedback">
                                                                {{ isset($errors) && $errors->has('currency_id') ? $errors->first('currency_id') : '' }}
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- Language field -->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="lang">Langue</label>
                                                            <span class="text-danger">*</span>
                                                            {{ Form::select('lang', ['en' => __('lang.english'), 'fr' => __('lang.french')], $lead_lang, ['class' => 'form-control form-control-alt', 'placeholder' => 'Language']) }}
                                                        </div>
                                                    </div>
                                                @endsupport_deny
                        </div>
                        @endif
                        @endif
                        @endforeach
                        @endforeach
                        <div class="form-group col-md-12" style="padding-top: 20px;">
                            <button type="button" class="btn btn-lg btn-alt-primary btn-block" id="infos-btn"><i
                                    class="fa fa-fw fa-check mr-1"></i>Enregistrer</button>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="block-content block-content-full text-right border-top">
                <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END parents phone Add Modal -->
@push('custom_scripts')
    <script>
        let customerId = {!! json_encode($customer_case->customer->id) !!};
        let customerCaseId = {!! json_encode($customer_case->id) !!};
        let programId = {!! json_encode($customer_case->customer->program_id) !!};
        let sheetVisited = {!! json_encode($sheet_visited) !!};
        // father_phone
        if (programId == 4 || programId == 31 || programId == 32) {
            window.setIntlTel("mother-phone");
            window.setIntlTel("father-phone");
            // infos-form
            window.setIntlTel("form-phone");
        }

        $('body').on('click', '.infos-modal-btn', function() {
            if (sheetVisited === true) {
                if (programId == 4 || programId == 31 || programId == 32) {
                    window.location.href = `/sheets/${customerCaseId}/1`;
                } else if (programId == 9) {
                    window.location.href = `/sheets/${customerCaseId}/10`;
                } else {
                    window.location.href = `/sheets/${customerCaseId}/7`;
                }
            } else {
                $('#modal-infos').modal('show');
            }
        });

        $('#infos-btn').on('click', function() {
            let phoneFormSelector = $('#infos-form');
            let phoneBlockSelector = $('#block-infos');

            //hide previous validation errors
            phoneFormSelector.find('.form-control').removeClass('is-invalid');

            phoneBlockSelector.addClass('block-mode-loading');
            $.ajax({
                method: 'post',
                url: `/api/v1/customers/${customerId}`,
                data: phoneFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'Infos Updated succefully!',
                        'success'
                    ).then(() => {
                        phoneBlockSelector.removeClass('block-mode-loading');
                        $('#infos-notification').show();
                        Livewire.emit("modalEdited");
                        window.setTimeout(function() {
                            if (programId == 4 || programId == 31 || programId == 32) {
                                window.location.href = `/sheets/${customerCaseId}/1`;
                            } else if (programId == 9) {
                                window.location.href = `/sheets/${customerCaseId}/10`;
                            } else {
                                window.location.href = `/sheets/${customerCaseId}/7`;
                            }
                        }, 2000);
                    })
                },
                error: (xhr) => {

                    if (xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key, value) {
                            // console.log(key);
                            $(`#block-infos #${key}`).addClass('is-invalid');
                            $(`#block-infos #${key}-error`).html(value.toString().replace(key,
                                $("label[for='" + $(`#block-infos #${key}`).attr('id') +
                                    "']").html())).show();
                        });
                        phoneBlockSelector.removeClass('block-mode-loading');
                    } else
                        Swal.fire(
                            'Ajout!',
                            'erreur infos!',
                            'error'
                        ).then(() => {
                            phoneBlockSelector.removeClass('block-mode-loading');
                        })
                }
            });
        });
    </script>
@endpush
