
<div class="block block-rounded mb-0">
    <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link{{ ($active == 1) ? ' active':'' }}" href="javascript:void(0)" wire:click="toggleTab(1)">
                Programes
                <span class="nav-main-link-badge badge badge-pill badge-primary">{{ $cprograms }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link{{ ($active == 2) ? ' active':'' }}" href="javascript:void(0)" wire:click="toggleTab(2)">
                Etablissement
                <span class="nav-main-link-badge badge badge-pill badge-primary">{{ $cschools }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link{{ ($active == 3) ? ' active':'' }}" href="javascript:void(0)" wire:click="toggleTab(3)">
                Choix
                <span class="nav-main-link-badge badge badge-pill badge-primary">{{ count($choices['programs']) }}</span>
            </a>
        </li>
    </ul>
    <div class="block-content tab-content overflow-auto" style="height: 490px;">
        <div class="tab-pane fade fade-left{{ ($active == 1) ? ' active show':'' }}" id="btabs-programs" role="tabpanel">
            <div class="list-group push">
                @foreach($programs as $id => $program_)
                    @php
                        $program = \App\Models\OrientationSchool::where('programme', $program_)->first()->toArray();
                    @endphp
                    @if (in_array($program['programme'], $choices['programs']))
                        <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center active" href="javascript:void(0)" wire:click="selectProgram('{{ $program['id'] }}')">
                            <address>
                                <strong>{{ $program['programme']}}</strong><br>
                                <strong>Frai Admission : </strong> {{' '. $program['frais_admission'] .' '}}<br>
                                <strong>Ville : </strong> {{' '. $program['ville'] .' '}}<br>
                                <strong> Code prg : </strong> {{$program['code_programme']!='' ? $program['code_programme'] : 'no code' }}
                            </address>
                            {{--{{ $program['programme']}}
                            <p style="text-align: right;font-weight:500;font-size:15px"> <span>   <strong>Frai Admission : </strong> {{' '. $program['frais_admission'] .' '}}</span>   &nbsp; <span>  <strong>Ville : </strong> {{' '. $program['ville'] .' '}}</span>  &nbsp;    <span> <strong> Code prg : </strong> {{$program['code_programme']!='' ? $program['code_programme'] : 'no code' }}  </span></p>--}}
                        </a>
                        <a  class="passingID" style="display: inline;color:rgba(62, 62, 243, 0.842);text-align:right;position: relative;top: -53px; " data-lien="{{$program['lien']}}" data-typeetab="{{$program['type_etablissement']}}" data-diplome="{{$program['diplome']}}" data-ville="{{$program['ville']}}" data-prog ="{{$program['programme']}}" data-codeprog="{{$program['code_programme']}}" data-nometab ="{{$program['nom_etablissement']}}" data-duree ="{{$program['duree']}}" data-langue ="{{$program['langue']}}" data-session ="{{$program['session']}}" data-fraiAdm ="{{$program['frais_admission']}}" data-fraisScolAn ="{{$program['prix_annuel']}}" data-prixtotal ="{{$program['prix_total']}}"> <i class="fas fa-eye"></i></a>

                        @else
                        <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" href="javascript:void(0)" wire:click="selectProgram('{{ $program['id'] }}')">
                            {{ $program['programme']}}
                           <p style="text-align: right;font-weight:500;font-size:15px"> <span>   <strong>Frai Admission : </strong> {{' '. $program['frais_admission'] .' '}}</span>   &nbsp;<span>   <strong>Ville : </strong> {{' '. $program['ville'] .' '}}</span>   &nbsp;   <span> <strong>Code prg : </strong> {{$program['code_programme']!='' ? $program['code_programme'] : 'no code' }}  </span></p>
                        </a>

                        <a  class="passingID" style="display: inline;color:rgba(62, 62, 243, 0.842);text-align:right;position: relative;top: -53px; " data-lien="{{$program['lien']}}" data-typeetab="{{$program['type_etablissement']}}" data-diplome="{{$program['diplome']}}" data-ville="{{$program['ville']}}" data-prog ="{{$program['programme']}}" data-codeprog="{{$program['code_programme']}}" data-nometab ="{{$program['nom_etablissement']}}" data-duree ="{{$program['duree']}}" data-langue ="{{$program['langue']}}" data-session ="{{$program['session']}}" data-fraiAdm ="{{$program['frais_admission']}}" data-fraisScolAn ="{{$program['prix_annuel']}}" data-prixtotal ="{{$program['prix_total']}}"> <i class="fas fa-eye"></i></a>
                        @endif
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade fade-left{{ ($active == 2) ? ' active show':'' }}" id="btabs-schools" role="tabpanel">
            <div class="list-group push">
                @foreach($schools as $id => $school)
                    @if (in_array($school, $choices['schools']))
                        <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center active" href="javascript:void(0)" wire:click="selectSchool('{{ $school }}')">
                            {{ $school }}
                        </a>
                    @else
                    <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" href="javascript:void(0)" wire:click="selectSchool('{{ $school }}')">
                        {{ $school }}
                    </a>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade fade-left{{ ($active == 3) ? ' active show':'' }}" id="btabs-selected" role="tabpanel">
            <div class="list-group push">
                @foreach($programs as $id => $program_)
                    @php
                        $program = \App\Models\OrientationSchool::where('programme', $program_)->first()->toArray();
                    @endphp
                    @if (in_array($program['programme'], $choices['programs']))
                        <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" href="javascript:void(0)">
                            {{ $program['programme']}}
                            <p style="text-align: right;font-weight:500;font-size:15px"> <span>   <strong>Frai Admission : </strong> {{' '. $program['frais_admission'] .' '}}</span>   &nbsp;<span>   <strong>Ville : </strong> {{' '. $program['ville'] .' '}}</span>   &nbsp;   <span> <strong>Code prg : </strong> {{$program['code_programme']!='' ? $program['code_programme'] : 'no code' }}  </span></p>
                        </a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="block-content overflow-auto">
        <div class="row" style="display: block;">
            <button type="button" class="btn btn-danger btn-lg float-right m-4" wire:click="saveChoices">Sauvegarder</button>

        </div>
    </div>
    <!-- Modal -->
  <div class="modal fade bd-example-modal-lg" id="myModal">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Détail programme</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body  ">


                <div class="form-group">
                    <label>Programme : </label>
                    <input  class="form-control" id="prog">
                  </div>
                  <div class="form-group mb-2">
                    <label >Code de programme  : </label>
                    <input class="form-control" id="codeprog">
                  </div>
                  <div class="form-group">
                    <label >Ville  : </label>
                    <input  class="form-control" id="ville">
                  </div>
                <div class="form-group">
                    <label >Nom de l'établissement : </label>
                    <input  class="form-control" id="nometab">
                  </div>
                  <div class="form-group">
                    <label >Lien : </label>
                    <input  class="form-control" id="lien">
                  </div>
                  <div class="form-group">
                    <label >Durée d'études : </label>
                    <input class="form-control" id="duree">
                  </div>
                  <div class="form-group">
                    <label >Langue : </label>
                    <input  class="form-control" id="langue">
                  </div>
                  <div class="form-group">
                    <label >Session  : </label>
                    <input  class="form-control" id="session">
                  </div>
                  <div class="form-group">
                    <label >Type de l'établissement  : </label>
                    <input  class="form-control" id="typeetab">
                  </div>
                  <div class="form-group">
                    <label >Diplome  : </label>
                    <input  class="form-control" id="diplome">
                  </div>

                  <div class="form-group">
                    <label >Frais admission  : </label>
                    <input class="form-control" id="frais_ad">
                  </div>
                  <div class="form-group">
                    <label >Frais de scolarité annuelle  : </label>
                    <input  class="form-control" id="frais_scol_annuel">
                  </div>
                  <div class="form-group" >
                    <label >Frais de scolarité totale  : </label>
                    <input  class="form-control" id="frais_scol_totale">
                  </div>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<style>
    .list-group-item-action {
    width: 95%;

}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script>
    $(".passingID").click(function () {
    let prog = $(this).attr('data-prog');
    let codeprog = $(this).attr('data-codeprog');
    let ville = $(this).attr('data-ville');
    let nometab = $(this).attr('data-nometab');
    let duree = $(this).attr('data-duree');
    let langue = $(this).attr('data-langue');
    let session = $(this).attr('data-session');
    let fraiAdm = $(this).attr('data-fraiAdm');
    let fraisScolAn = $(this).attr('data-fraisScolAn');
    let prixtotal = $(this).attr('data-prixtotal');
    let typeetab = $(this).attr('data-typeetab');
    let diplome = $(this).attr('data-diplome');
    let lien= $(this).attr('data-lien');

    $("#lien").val(lien);
    $("#prog").val( prog );
    $("#codeprog").val(codeprog);
    $("#ville").val( ville );
    $("#nometab").val( nometab );
    $("#duree").val( duree );
    $("#langue").val( langue );
    $("#session").val( session );
    $("#frais_ad").val( fraiAdm );
    $("#frais_scol_annuel").val( fraisScolAn );
    $("#frais_scol_totale").val( prixtotal );
    $("#typeetab").val( typeetab );
    $("#diplome").val( diplome );
    $('#myModal').modal('show');

});
</script>
