<?php
$lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title"><i class="fa fa-atom me-1"></i>{{ $lang === "fr" ? " Log des status" : " All states"}}</h3>
    </div>
    @if(count($states))
        <div class="block-content custom_table" style="padding: 20px;">
            <table class="table table-sm table-vcenter">
                <thead>
                    <tr class="calls">
                        <th class="d-none d-sm-table-cell" style="width: 20%;">
                            {{ $lang === "fr" ? "Date de status" : "State Date"}}
                        </th>
                        <th style="width: 45%;">
                            {{ $lang === "fr" ? "Status" : "State"}}
                        </th>
                        <th>
                            {{ $lang === "fr" ? "Effectué par" : "CALLED BY"}}
                        </th>
                    </tr>
                </thead>
                <tbody>
                @foreach($states as $state)
                    <tr class="calls">
                        <td class="d-none d-sm-table-cell" style="width: 20%;">
                            <em class="font-size-sm text-muted">{{ $state->created_at }}</em>
                        </td>
                        <td class="font-size-sm" style="width: 45%;">
                            @php
                                $leadStatus = App\Models\LeadStatus::where("id", $state->state_id)->first();
                                //dd($lead);
                                $badge = "danger";
                                if ($state->state_id == 1)
                                    $badge = "success";
                                if ($state->state_id == 2)
                                    $badge = "light";
                                if ($state->state_id == 3)
                                    $badge = "secondary";
                                if ($state->state_id == 4)
                                    $badge = "info";
                                if ($state->state_id == 5)
                                    $badge = "warning";
                                if ($state->state_id == 7)
                                    $badge = "activation";
                                if (!$leadStatus)
                                    $badge = "info";
                                $s = \App\Models\LeadStatus::find($state->state_id);
                            @endphp
                            <p class="text-muted mb-0">
                                <span class="badge badge-{{ $badge }}">{{ $s? $s->status_label_fr: "null" }}</span>

                            </p>
                        </td>
                        <td class="font-size-sm">
                            @php $u = \App\Models\User::find($state->user_id); @endphp
                            <p class="text-muted mb-0">
                                {{ $u? $u->name: null}}
                            </p>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
