<?php
$lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title"><i class="fa fa-atom me-1"></i>{{ $lang === "fr" ? " Log des affectations" : " All Assignements"}}</h3>
    </div>
    @if(count($assignements))
        <div class="block-content custom_table" style="padding: 20px;">
            <table class="table table-sm table-vcenter">
                <thead>
                    <tr class="calls">
                        <th class="d-none d-sm-table-cell" style="width: 20%;">
                            {{ $lang === "fr" ? "Date de status" : "State Date"}}
                        </th>
                        <th style="width: 45%;">
                            {{ $lang === "fr" ? "Affecter a" : "Assigned to"}}
                        </th>
                        <th>
                            {{ $lang === "fr" ? "Effectué par" : "CALLED BY"}}
                        </th>
                    </tr>
                </thead>
                <tbody>
                @foreach($assignements as $assignement)
                    <tr class="calls">
                        <td class="d-none d-sm-table-cell" style="width: 20%;">
                            <em class="font-size-sm text-muted">{{ $assignement->created_at }}</em>
                        </td>
                        <td class="font-size-sm" style="width: 45%;">
                            @php
                                $conseiller = \App\Models\User::find($assignement->conseiller_id);
                            @endphp
                            <p class="text-muted mb-0">
                                {{ $conseiller ? $conseiller->name : null}}
                            </p>
                        </td>
                        <td class="font-size-sm">
                            @php $user = \App\Models\User::find($assignement->user_id); @endphp
                            <p class="text-muted mb-0">
                                {{ $user ? $user->name : null }}
                            </p>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
