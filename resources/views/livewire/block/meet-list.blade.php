@foreach($filtred_meets as $key => $meets)
    <div class="block block-rounded">
        <div class="block-header block-header-default">
            <h3 class="block-title"><i class="fa fa-users me-1"></i> Listes des Roncontres {{ "(" .  $key . ")"}} </h3>
        </div>
        @if(count($meets))
            <div class="block-content custom_table" style="padding: 20px;">
                <table class="table table-sm table-vcenter">
                    <thead>
                    <tr class="calls">
                        <th class="d-none d-sm-table-cell" style="width: 20%;">
                            Date de rencontre
                        </th>
                        <th style="width: 45%;">
                            Client
                        </th>
                        <th>
                            Rencontre
                        </th>
                        <th>
                            Programme
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($meets as $meet)
                        <tr class="comment">
                            <td class="d-none d-sm-table-cell" style="width: 20%;">
                                <em class="font-size-sm text-muted">{{ $meet->performed_at }}</em>
                            </td>
                            <td class="font-size-sm" style="width: 45%;">
                                <p class="text-muted mb-0">
                                    {{ $meet->name }}
                                </p>
                            </td>
                            <td class="font-size-sm">
                                <p class="text-muted mb-0">
                                    {{ str_replace("Paiement ", "R", $meet->payment) }}
                                </p>
                            </td>
                            <td class="font-size-sm">
                                <p class="text-muted mb-0">
                                    {{ $meet->program_name }}
                                </p>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endforeach

