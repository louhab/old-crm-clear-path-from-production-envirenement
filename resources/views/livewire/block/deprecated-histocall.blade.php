<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title"><i class="fa fa-phone me-1"></i>{{ $lang === "fr" ? " Log des appels" : " All Calls"}}</h3>
        @if(in_array($lead->lead_status_id, [5, 11]))
        <div class="block-options">
            <button type="button" id="histocall-customer-table-add" class="btn btn-sm btn-alt-primary">
                {{ $lang === "fr" ? "Effectuer un appel" : "Make a call"}}
            </button>
        </div>
        @endif
    </div>
    @if(count($calls))
        <div class="block-content custom_table" style="padding: 20px;">
            <table class="table table-sm table-vcenter">
                <thead>
                <tr class="calls">
                    <th class="d-none d-sm-table-cell" style="width: 20%;">
                        {{ $lang === "fr" ? "Date d'appel" : "CALL DATE"}}
                    </th>
                    <th style="width: 45%;">
                        {{ $lang === "fr" ? "Description" : "Description"}}
                    </th>
                    <th>
                        {{ $lang === "fr" ? "Effectué par" : "CALLED BY"}}
                    </th>
                    <th class="d-none d-sm-table-cell" style="width: 10%;">
                        {{ $lang === "fr" ? "Durée" : "DURATION"}}
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($calls->reverse() as $call)
                    <tr class="calls">
                        <td class="d-none d-sm-table-cell" style="width: 20%;">
                            <em class="font-size-sm text-muted">{{ $call->created_at }}</em>
                        </td>
                        <td class="font-size-sm" style="width: 45%;">
                            <p class="text-muted mb-0">
                                {{ $call->call_descr }}
                            </p>
                        </td>
                        <td class="font-size-sm">
                            @php $u = \App\Models\User::find($call->user_id); @endphp
                            <p class="text-muted mb-0">
                                {{ $u? $u->name: null}}
                            </p>
                        </td>
                        <td class="d-none d-sm-table-cell" style="width: 10%;">
                            <span class="badge badge-success">{{ $call->duration }} seconds</span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
