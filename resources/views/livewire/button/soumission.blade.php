<div class="block-options">
    @if(auth()->user()->isAdmin() || auth()->user()->isVerificateur() || $admis)
    <button type="button" class="btn btn-lg btn-rounded btn-alt-danger" disabled>
        @if($soumission)
            <i class="fa fa-check text-success mr-3"></i>
        @else
            <i class="fa fa-sync fa-spin text-warning mr-3"></i>
        @endif
        Soumission
    </button>
    @else
        <div class="block-options-item">
            <i class="fa fa-fw fa-lock text-danger"></i>
        </div>
    @endif
</div>
