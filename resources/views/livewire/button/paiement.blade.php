<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
<form wire:submit.prevent="save" class="row" style="margin: unset;align-items: center;">

    @if($checkMedia)
        <table class="table table-sm table-vcenter">
            <thead>
            <tr class="calls">
                <th class="d-none d-sm-table-cell" style="width: 30%;">{{ __("lang.portal_doc_type") }}</th>
                <th>{{ __("lang.portal_doc_creation_date") }}</th>
                <th class="d-none d-sm-table-cell" style="width: 30%;">Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="font-size-sm">
                    <p class="text-muted mb-0">{{ $lang === "fr" ? " Preuve de" : " Proof of"}} {{ $lang === "fr" ? $documentType->name_fr : $documentType->name_en }}</p>
                </td>
                <td class="font-size-sm">
                    <p class="text-muted mb-0"></p>
                </td>
                <td>
                    <a class="btn btn-sm btn-light document-in" href="{{ route('get-media', $customer_case->customer->getFirstMedia($collection)) }}">
                        <i class="fa fa-fw fa-download text-success"></i>
                    </a>
                    <a  class="btn btn-sm btn-light document-in" id="delete-cv-href" href="javascript:void(0)" wire:click="delete('{{ $collection }}')"><i class="fa fa-fw fa-times text-danger"></i></a>
                </td>
            </tr>
            </tbody>
        </table>
    @else
            <div class="custom-file" lang={{$lang}}>
                <input type="file" wire:model="document" class="custom-file-input js-custom-file-input-enabled" id="file-input-cin" name="file-input-cin" placeholder="test">
                <label class="custom-file-label" id="label-paiement-input" for="file-input-cin">{{ __("lang.portal_personal_infos_upload") }} {{ $lang = "fr"  }}</label>
            </div>
    @endif
</form>
