<table width="100%">
    <tr>
        @if(!$allDocsExists)
        <td class="text-danger pt-1">
            <div class="alert alert-warning py-2 mb-0">
                <i class="fa fa-exclamation-triangle mr-1"></i> Documents Missing!
            </div>
        </td>
        @endif
        <td>
            <button type="button" class="btn btn-lg btn-rounded btn-alt-danger float-right" @if(!$allDocsExists) disabled @else wire:click="confirmDocs" @endif>
                Confirmer documents
            </button>
        </td>
    </tr>
</table>
