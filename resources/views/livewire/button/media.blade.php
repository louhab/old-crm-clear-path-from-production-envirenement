<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>

<form wire:submit.prevent="save" class="row">
    <div class="col-md-3 mr-3">
        {{ $lang === "fr" ? $documentType->name_fr : $documentType->name_en }} :
    </div>
    @if($checkMedia)
        <a class=" mr-3" href="{{ route('get-media', $customer_case->customer->getFirstMedia($collection)) }}"><i class="fa fa-2x fa-download"></i> </a>
        <a class=" mr-3" id="delete-cv-href" href="javascript:void(0)" wire:click="delete('{{ $collection }}')"><i class="fa fa-2x fa-times"></i></a>
    @else
        <div class="col-md-7">
            <div class="custom-file" lang={{ $lang}}>
                <input type="file" wire:model="document" class="custom-file-input js-custom-file-input-enabled" id="file-input-cin" name="file-input-cin" placeholder="test">
                <label class="custom-file-label" id="label-cv-input" for="file-input-cin">{{ __("lang.portal_personal_infos_upload") }} {{ $lang === "fr" ? $documentType->name_fr : $documentType->name_en }}</label>
            </div>
        </div>
    @endif
</form>
