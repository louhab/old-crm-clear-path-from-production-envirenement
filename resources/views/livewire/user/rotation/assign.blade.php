<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
<div class="block block-rounded">
    <div class="block-header">
        <h3 class="block-title">{{ __("lang.assign_block_title") }}</h3>
    </div>
    <div class="block-content">
        <form id="assign-user">
            <div class="form-group">
                <label for="lang">{{ __("lang.assign_block_language") }}: </label>
                <select name="lang" id="lang" class="form-control form-control-alt">
                    <option value="">{{ __("lang.assign_block_language") }}</option>
                    <option value="fr" @if(auth()->user()->lang == 'fr') selected @endif>{{ __("lang.assign_block_french") }}</option>
                    <option value="en" @if(auth()->user()->lang == 'en') selected @endif>{{ __("lang.assign_block_engilsh") }}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="country">{{ __("lang.assign_block_country") }}</label>
                <select class="form-control form-control-alt" id="country" name="country">
                    <option value="">{{__('lang.country')}}</option>
                    @foreach(\App\Models\Country::all() as $country)
                        <option value="{{ $country->id }}" {{ auth()->user()->country_id == $country->id ? 'selected' : ''  }}>
                            {{ $lang === "fr" ? $country->name_fr_fr : $country->name_en_gb}}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group" style="padding-top: 20px;">
                <button type="button" class="btn btn-outline-danger btn-block">{{ __("lang.assign_block_save_btn") }}</button>
            </div>
        </form>
    </div>
    @push('custom_scripts')
        <script>
            // $('#lang').select2();
            // $('select[name="lang"]').select2();
        </script>
    @endpush
</div>
