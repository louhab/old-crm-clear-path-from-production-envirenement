<?php
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
// dd($docs);
?>
<div class="row">
    <div class="col-xl-4">
        <!-- With Links -->
        <div class="block block-rounded pb-5">
            <div class="block-header">
                <h3 class="block-title">With Links</h3>
            </div>
            <div class="block-content overflow-auto" style="height: 414px;">
                <!--                <span class="badge badge-danger">Emploi</span>-->
                @php
                    $clear_fields = \App\Models\CustomerClear1000Fields::where('customer_id', $customerCase->customer->id)->first();
                    $visa_requests = \App\Models\CustomerVisaRequest::where('customer_id', $customerCase->customer_id)
                        ->orderBy('id', 'ASC')
                        ->get();
                    // dd($visa_requests);
                @endphp
                <div class="list-group push">
                    @foreach ($docs as $doc)
                        @if ($loop->first)
                            @php $first_doc = $doc["collection_name"]; @endphp
                        @endif
                        {{-- @if (in_array($doc['id'], [159]) && $clear_fields->visa_request != 82)
                            @continue
                        @endif --}}
                        @if (!isCustomer() && auth()->user()->id != 7 && isset($doc['isVisible']) && $doc['isVisible'] == false)
                            {{-- @php dd(); @endphp --}}
                            @continue
                        @endif
                        @if ($doc['id'] == 158 && $clear_fields->visa_request != 82)
                            @continue
                        @endif
                        @if ($doc['id'] == 159 && count($visa_requests) == 0)
                            @continue
                        @endif
                        @if ($doc['id'] == 160 && $clear_fields->visa_request_result != 85)
                            @continue
                        @endif
                        @if ($doc['id'] == 161 && $clear_fields->health != 4)
                            @continue
                        @endif
                        @php
                            $collection = $doc['collection_name'];
                            /*if ($this->mediaSheet->id == 1) {
                                if(in_array($loop->index, [3, 4, 5, 6])) {
                                    #107
                                    $collection = "107::".$collection;
                                }
                                elseif(in_array($loop->index, [15, 16, 17, 18])) {
                                    #110
                                    $collection = "110::".$collection;
                                }
                                elseif(in_array($loop->index, [20, 21, 22, 23, 24, 25])) {
                                    #111
                                    $collection = "111::".$collection;
                                }
                                elseif(in_array($loop->index, [27, 28, 29, 30])) {
                                    #112
                                    $collection = "112::".$collection;
                                }
                            }*/
                            $ddt = \App\Models\DocumentType::find($doc['id']);
                        @endphp
                        <a wire:click="toggle({{ $loop->index }})"
                            class="list-group-item list-group-document list-group-item-action d-flex justify-content-between align-items-center{{ $loop->index == $tabActive ? ' active' : '' }}"
                            href="javascript:void(0)">
                            <p>
                                {{ $doc[$lang === 'fr' ? 'name_fr' : 'name_en'] }}
                                @if (
                                    !isCustomer() &&
                                        (auth()->user()->isAdmin() ||
                                            auth()->user()->isVerificateur()))
                                    @foreach ($ddt->tagNames() as $tag)
                                        <span class="badge badge-info">{{ $tag }}</span>
                                    @endforeach
                                @endif
                            </p>
                            <span
                                class="badge badge-pill badge-secondary">{{ $customerCase->customer->getMedia($collection)->count() }}</span>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- END With Links -->
    </div>
    <div class="col-xl-8">
        <!-- With Links -->
        <div class="block block-rounded pb-5" id="block-media">
            <div class="block-header">
                <h3 class="block-title">With Links</h3>
                @if (!isCustomer() && auth()->user()->id == 7)
                    <div class="block-options">
                        <form class="form-inline" method="POST" onsubmit="return false;">
                            <label class="sr-only" for="document-tags">Tags</label>
                            <input type="text" class="form-control form-control-alt mb-2 mr-sm-2 mb-sm-0"
                                wire:model.defer="tags" id="document-tags" name="document-tags"
                                placeholder="Tag1,Tag2..">
                            <button type="submit" wire:click="updateDocumentTags" class="btn btn-dark">Update</button>
                        </form>
                    </div>
                @endif
            </div>
            <div class="block-content" style="height: 414px;">
                <div class="block-content block-content-full">
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ $dropzoneAction }}" method="post" enctype="multipart/form-data"
                                id="my-dropzone" class="dropzone">
                                @csrf
                            </form>
                        </div>
                        @if (count($docs))
                            <div class="col-12">
                                <table
                                    class="js-table-sections table table-hover table-vcenter js-table-sections-enabled"
                                    id="table-medias">
                                    <thead>
                                        <tr>
                                            <th style="width: 30px;"></th>
                                            <th>Name</th>
                                            <th style="width: 15%;">Access</th>
                                            <th class="d-none d-sm-table-cell" style="width: 20%;">Date</th>
                                        </tr>
                                    </thead>
                                    <tbody class="js-table-sections-header show table-active">
                                        <tr>
                                            <td class="text-center">
                                                <i class="fa fa-angle-right text-muted toggle-medias"></i>
                                            </td>
                                            <td class="font-w600 font-size-sm">
                                                <div class="py-1">
                                                    <a href="javascript:void(0)"
                                                        class="toggle-medias">{{ $docs[$tabActive][$lang === 'fr' ? 'name_fr' : 'name_en'] }}</a>
                                                </div>
                                            </td>
                                            <td>
                                                <span class="badge badge-success">Pending</span>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <em class="font-size-sm text-muted">November 10, 2018 08:46</em>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tbody class="font-size-sm">
                                        @foreach ($medias as $media)
                                            <tr id="{{ $media->id }}">
                                                <td class="text-center"></td>
                                                <td class="font-w600 font-size-sm">+ {{ $docs[$tabActive]['name_fr'] }}
                                                    - {{ $loop->index }}</td>
                                                <td colspan="2">
                                                    {{-- {{ route("customer-view-media", ['media' => $media]) }}
                                                @livewire('sheet.media', ['document' => $docs[$tabActive], 'media' => $media, 'index', $loop->index])
                                                --}}
                                                    <a class="btn btn-sm btn-outline-secondary text-success sheet-media-download"
                                                        wire:click="downloadMedia({{ $media->id }})"
                                                        href="javascript:void(0)">
                                                        <i class="fas fa-fw fa-download"></i>
                                                    </a>
                                                    <a class="btn btn-sm btn-outline-secondary text-info"
                                                        href="{{ route((isCustomer() ? 'customer-' : '') . 'view-media', ['media' => $media]) }}"
                                                        target="_blank">
                                                        <i class="fas fa-eye"></i>
                                                    </a>
                                                    @if (
                                                        \Auth::guard('customer')->user() ||
                                                            \Auth::user()->isAdmin() ||
                                                            \Auth::user()->isManager() ||
                                                            \Auth::user()->isBackoffice() ||
                                                            \Auth::user()->isConseiller())
                                                        <a class="btn btn-sm btn-outline-secondary text-danger media-delete"
                                                            wire:click="removeMedia({{ $media->id }})"
                                                            href="javascript:void(0);">
                                                            <i class="fas fa-trash"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- END With Links -->
    </div>
    @push('custom_scripts')
        @if ($mediaSheet->id != 6)
            <script>
                // is_multiple
                let isMultiple = {!! json_encode($docs[$tabActive]['is_multiple']) !!};
                let count = {!! json_encode(count($medias)) !!};
            </script>
        @else
            <script>
                // is_multiple
                let isMultiple = 0;
                let count = 0;
            </script>
        @endif
        <script>
            console.log("hey from is multi", isMultiple);
            if (count > 0 && (isMultiple == 0)) {
                $(".dz-hidden-input").prop("disabled", true);
            } else {
                $(".dz-hidden-input").prop("disabled", false);
            }
            // console.log(count);
            Livewire.on('deletedMedia', obj => {
                // console.log(obj);
                $('#' + obj.mediaId).remove();
            });
            Livewire.on('toggleDropzone', data => {
                // console.log(data.is_multiple == 0);
                count = data.count;
                if (count > 0 && (data.is_multiple == 0)) {
                    $(".dz-hidden-input").prop("disabled", true);
                } else {
                    $(".dz-hidden-input").prop("disabled", false);
                }
            });
            $('#my-dropzone').on('click', function() {
                // console.log();
                if ($(".dz-hidden-input").attr("disabled") == "disabled") {
                    Swal.fire(
                        'Ajout!',
                        'warning documents already uploaded!',
                        'warning'
                    );
                }
            });
            /*$('.media-delete').on('click', function () {
                $(this).closest('tr').remove();
            });*/
            // one media per document
            // $(".dz-hidden-input").prop("disabled",true);
        </script>
    @endpush
</div>
