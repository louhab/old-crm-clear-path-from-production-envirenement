@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/conseillers.js') }}"></script>
    <script>
        window.setIntlTel("phone");
        window.setIntlTel("add-form-phone");
    </script>
@endsection

@section('css_after')
    <!-- Styles -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
    <style>
        /* .select2-selection__rendered {
            line-height: 39px !important;
        }
        .select2-container .select2-selection--single {
            height: 42px !important;
        }
        .select2-selection__arrow {
            height: 43px !important;
        } */

        .select2-selection__choice__remove {
            color: #fff !important;
        }
    </style>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Conseillers/Supports','subTitle' => 'File d\'attente des Conseillers/Supports', 'navItems' => ['Admin', 'Conseillers/Supports']])

    <!-- Page Content -->
    <div class="content">

        <!-- Users Table -->
        <!-- <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">
                            <div class="col-3">
                                <select class="form-control dt_search_field" name="ranking">
                                    <option value="">Sélectionnez un ranking</option>
                                    @foreach(\App\Models\Ranking::all() as $ranking)
                                        <option value="{{ $ranking->id }}">{{ $ranking->ranking_label }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-3">
                                <input type="text" class="form-control dt_search_field" name="name" placeholder="Nom">
                            </div>
                            <div class="col-3">
                                <input type="text" class="form-control dt_search_field" name="email" placeholder="Email">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div> -->

        <div class="block block-rounded" id="users-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">File d'attente des Conseillers/Supports</h3>
                </div>
                <table class="table table-bordered table-sm table-condensed table-vcenter" id="users-dt">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">ID</th>
                        <th>Nom</th>
                        <th>Nombre de leads affecté</th>
                        <th class="d-none d-sm-table-cell" style="width: 25%;">Email</th>
                        <th class="d-none d-sm-table-cell" style="width: 15%;">Téléphone</th>
                        <th style="width: 15%;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Users Table -->

    </div>
    <!-- END Page Content -->

    <!-- User Add Modal -->
    <div class="modal" id="modal-user-add" tabindex="-1" role="dialog" aria-labelledby="modal-user-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-user-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter utilisateur</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form User Add -->
                                <form action="" method="POST" onsubmit="return false;" id="user-add-form">
                                    <div class="form-group">
                                        <label for="add-form-name">Nom</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-name" name="add-form-name" placeholder="Nom.." aria-describedby="add-form-name-error" aria-invalid="false">
                                        <div id="add-form-name-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-email">Email</label>
                                        <input type="email" class="form-control form-control-alt" id="add-form-email" name="add-form-email" placeholder="Email.." aria-describedby="add-form-email-error" aria-invalid="false">
                                        <div id="add-form-email-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-phone">Phone</label>
                                        <span class="text-danger">*</span><br/>
                                        <input type="tel" class="form-control form-control-alt" id="add-form-phone" name="add-form-phone" aria-describedby="add-form-phone-error" aria-invalid="false">
                                        <div id="add-form-phone-error" class="text-danger"></div>
                                        <span id="valid-msg" class="text-success" style="display:none;">✓ Valid</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-password">Password</label>
                                        <input type="password" class="form-control form-control-alt" id="add-form-password" name="add-form-password" placeholder="Password.." aria-describedby="add-form-password-error" aria-invalid="false">
                                        <div id="add-form-password-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-role">Rôle</label>
                                        <select class="form-control form-control-alt" id="add-form-role" name="add-form-role" aria-describedby="add-form-role-error" aria-invalid="false">
                                            <option value="">Sélectionnez un rôle</option>
                                            @foreach(\App\Models\Role::all() as $role)
                                                <option value="{{ $role->id }}">{{ $role->role_name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-role-error" class="invalid-feedback"></div>
                                    </div>
                                    @admin
                                    <div class="form-group">
                                        <label for="add-form-status">Status</label>
                                        <select class="form-control form-control-alt" id="add-form-status" name="add-form-status" aria-describedby="add-form-status-error" aria-invalid="false">
                                        @php $user = Illuminate\Support\Facades\Auth::user(); @endphp
                                            <option value="1" selected>Enabled</option>
                                            <option value="0">Disabled</option>
                                        </select>
                                        <div id="add-form-status-error" class="invalid-feedback"></div>
                                    </div>
                                    @endadmin
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-user-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter utilisateur</button>
                                    </div>
                                </form>
                                <!-- END Form User Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END User Add Modal -->

    <!-- User Edit Modal -->
    <div class="modal" id="modal-user-edit" tabindex="-1" role="dialog" aria-labelledby="modal-user-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-user-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Nombre de leads affecté par rotation</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form User Edit -->
                                <form action="" method="POST" onsubmit="return false;" id="user-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-queue">Nom</label>
                                        <input type="number" class="form-control form-control-alt" id="edit-form-queue" name="edit-form-queue" placeholder="Nom.." aria-describedby="edit-form-queue-error" aria-invalid="false">
                                        <div id="edit-form-queue-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="update-user-btn"><i class="fa fa-fw fa-check mr-1"></i>Modifier utilisateur</button>
                                    </div>
                                </form>
                                <!-- END Form User Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END User Edit Modal -->
@endsection
