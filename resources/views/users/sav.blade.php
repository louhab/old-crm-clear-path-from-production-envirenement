@extends('layouts.backend')
@section('js_after')
    <!--    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>-->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>


    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/users.js') }}"></script>

@endsection



@section('content')
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Supports Technique <button type="button" class="btn btn-alt-primary" id="user-table-add"><i class="fa fa-fw fa-plus mr-1"></i>nouvelle demande</button>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">CRM</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="">Supports</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">

        <!-- Users Table -->


        <div class="block block-rounded" id="users-dt-block">
            <div class="block-content block-content-full">


                    <iframe class="clickup-embed" src="https://sharing.clickup.com/9009098307/l/h/6-900301545695-1/5259ff8c80ae78f" onwheel="" width="100%" height="700px" style="background: transparent; border: 1px solid #ccc;"></iframe>

            </div>
        </div>
        <!-- END Users Table -->

    </div>
    <!-- END Page Content -->

    <!-- User Add Modal -->
    <div class="modal" id="modal-user-add" tabindex="-1" role="dialog" aria-labelledby="modal-user-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-user-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter nouvelle demande</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <iframe class="clickup-embed clickup-dynamic-height" src="https://forms.clickup.com/9009098307/f/8cfqvj3-3422/4NEU3104NH23QB3ONM" onwheel="" width="100%" height="100%" style="background: transparent; border: 1px solid #ccc;"></iframe><script async src="https://app-cdn.clickup.com/assets/js/forms-embed/v1.js"></script>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END User Add Modal -->


@endsection


