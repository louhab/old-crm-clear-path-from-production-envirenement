@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>

    <!-- Page JS Code -->
    <script>
        //ADD FORM
        let userAddBlockSelector = $('#block-user-add');
        let userAddFormSelector = $('#user-add-form');

        $('#toggle-user-modal').on('click', function () {

            //hide previous validation errors
            userAddFormSelector.find('.invalid-feedback').hide();
            userAddFormSelector.find('.form-control').removeClass('is-invalid');

            $('#user-add-form').trigger('reset');
            $('#modal-user-add').modal('show');
        });

        $('#add-user-btn').on('click', function () {

            //hide previous validation errors
            userAddFormSelector.find('.invalid-feedback').hide();
            userAddFormSelector.find('.form-control').removeClass('is-invalid');

            userAddBlockSelector.addClass('block-mode-loading');

            $.ajax({
                method: 'post',
                url: `/rotation/update`,
                data: userAddFormSelector.serialize(),
                success: () => {
                    Swal.fire(
                        'Ajout!',
                        'utilisateur ajouté avec succés!',
                        'success'
                    ).then(() => {
                        userAddBlockSelector.removeClass('block-mode-loading');
                        $('#user-add-form').trigger('reset');
                        // usersTable.api().ajax.reload();
                    })
                },
                error: (xhr) => {

                    if(xhr.statusCode().status === 422) {

                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $(`#${key}`).addClass('is-invalid');
                            $(`#${key}-error`).html(value.toString().replace(key, $("label[for='" + $(`#${key}`).attr('id') + "']").html())).show();
                        });
                        userAddBlockSelector.removeClass('block-mode-loading');
                    } else {
                        Swal.fire(
                            'Ajout!',
                            'erreur ajout utilisateur!',
                            'error'
                        ).then(() => {
                            userAddBlockSelector.removeClass('block-mode-loading');
                        })
                    }
                }
            });
        });
    </script>
@endsection

@section('css_after')
    <!-- Styles -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Rotation','subTitle' => 'File d\'attente des Conseillers/Supports/Backoffice', 'navItems' => ['Admin', 'Conseillers/Supports/Backoffice']])

    <!-- Page Content -->
    <div class="content">

        <div class="block block-rounded" id="users-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">File d'attente des Conseillers/Supports</h3>
                    <div class="block-options">
                        <a href="javascript:void(0)" class="btn btn-alt-primary" id="toggle-user-modal">
                            <i class="fa fa-fw fa-plus mr-1"></i>
                            Ajouter
                        </a>
                    </div>
                </div>
                <table class="table table-bordered table-sm table-condensed table-vcenter" id="users-dt">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">ID</th>
                        <th>Nom</th>
                        <th>Nombre de leads affecté</th>
                        <th class="d-none d-sm-table-cell" style="width: 25%;">Email</th>
                        <th class="d-none d-sm-table-cell" style="width: 15%;">Téléphone</th>
                        <th style="width: 15%;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(\App\Models\UserAssign::all() as $rotation)
                        <tr>
                            <td class="text-center" style="width: 80px;">ID</td>
                            <td>{{ $rotation->users }}</td>
                            <td>Nombre de leads affecté</td>
                            <td class="d-none d-sm-table-cell" style="width: 25%;">Email</td>
                            <td class="d-none d-sm-table-cell" style="width: 15%;">Téléphone</td>
                            <td style="width: 15%;">Actions</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Users Table -->

    </div>
    <!-- END Page Content -->

    <!-- User Add Modal -->
    <div class="modal" id="modal-user-add" tabindex="-1" role="dialog" aria-labelledby="modal-user-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-user-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter utilisateur</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form User Add -->
                                <form action="" method="POST" onsubmit="return false;" id="user-add-form">
                                    <div class="form-group">
                                        <label for="add-form-rotation">Rotation</label>
                                        <select class="form-control form-control-alt" id="add-form-rotation" name="add-form-rotation" aria-describedby="add-form-rotation-error" aria-invalid="false">
                                            <option value="">Sélectionnez une rotation</option>
                                            @foreach(\App\Models\Role::whereIn("id", [3, 7, 6])->get() as $role)
                                                <option value="{{ $role->id }}">{{ $role->role_name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-rotation-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-user">Utilisateur</label>
                                        <select class="form-control form-control-alt" id="add-form-user" name="add-form-user" aria-describedby="add-form-user-error" aria-invalid="false">
                                            <option value="">Sélectionnez un utilisateur</option>
                                            @foreach(\App\Models\User::all() as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-user-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Langue</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="add-form-lang-fr" value="fr" name="add-form-lang" checked="">
                                            <label class="custom-control-label" for="add-form-lang-fr">Français</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="add-form-lang-en" value="en" name="add-form-lang">
                                            <label class="custom-control-label" for="add-form-lang-en">Anglais</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-country">Pays</label>
                                        <select class="form-control form-control-alt" id="add-form-country" name="add-form-country" aria-describedby="add-form-country-error" aria-invalid="false">
                                            @foreach(\App\Models\Country::all() as $country)
                                                <option value="{{ $country->id }}" @if($country->id == 144) selected @endif>{{ $country->name_fr_fr }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-country-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-user-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter utilisateur</button>
                                    </div>
                                </form>
                                <!-- END Form User Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END User Add Modal -->
@endsection
