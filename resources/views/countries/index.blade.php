@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/countries.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Pays','subTitle' => 'liste', 'navItems' => ['Admin', 'Liste des pays']])

    <!-- Page Content -->
    <div class="content">
        <!-- phases Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">

                            <div class="col-3">
                                <input type="text" class="form-control dt_search_field" name="name" placeholder="nom du pays">
                            </div>

                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>


        <!-- countries Table -->
        <div class="block block-rounded" id="countries-dt-block">
            <div class="block-content block-content-full">

                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Pays</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary" id="country-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter pays</button>
                    </div>
                </div>
                <h4></h4>


                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="countries-dt">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">ID</th>
                        <th>Nom FR</th>
                        <th>Nom EN</th>
                        <th>Code</th>
                        <th>Code Alpha 2</th>
                        <th>Code Alpha 3</th>
                        <th style="width: 15%;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END countries Table -->
    </div>
    <!-- END Page Content -->

    <!-- country Add Modal -->
    <div class="modal" id="modal-country-add" tabindex="-1" role="dialog" aria-labelledby="modal-country-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-country-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter pays</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form country Add -->
                                <form action="" method="POST" onsubmit="return false;" id="country-add-form">

                                    <div class="form-group">
                                        <label for="add-form-name_fr_fr">Nom FR</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-name_fr_fr" name="add-form-name_fr_fr" placeholder="Pays en FR.." aria-describedby="add-form-name_fr_fr-error" aria-invalid="false">
                                        <div id="add-form-name_fr_fr-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-name_en_gb">Nom EN</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-name_en_gb" name="add-form-name_en_gb" placeholder="Pays en EN.." aria-describedby="add-form-name_en_gb-error" aria-invalid="false">
                                        <div id="add-form-name_en_gb-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-code">Code</label>
                                        <input type="number" class="form-control form-control-alt" id="add-form-code" name="add-form-code" placeholder="code.." aria-describedby="add-form-code-error" aria-invalid="false">
                                        <div id="add-form-code-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-alpha2">Alpha 2</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-alpha2" name="add-form-alpha2" placeholder="Alpha 2.." aria-describedby="add-form-alpha2-error" aria-invalid="false">
                                        <div id="add-form-alpha2-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="add-form-alpha3">Alpha 3</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-alpha3" name="add-form-alpha3" placeholder="Alpha 3.." aria-describedby="add-form-alpha3-error" aria-invalid="false">
                                        <div id="add-form-alpha3-error" class="invalid-feedback"></div>
                                    </div>


                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-country-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter pays</button>
                                    </div>
                                </form>
                                <!-- END Form country Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END country Add Modal -->

    <!-- country Edit Modal -->
    <div class="modal" id="modal-country-edit" tabindex="-1" role="dialog" aria-labelledby="modal-country-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-country-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Modifier pays</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form country edit -->
                                <form action="" method="POST" onsubmit="return false;" id="country-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-name_fr_fr">Nom FR</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-name_fr_fr" name="edit-form-name_fr_fr" placeholder="Pays en FR.." aria-describedby="edit-form-name_fr_fr-error" aria-invalid="false">
                                        <div id="edit-form-name_fr_fr-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-name_en_gb">Nom EN</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-name_en_gb" name="edit-form-name_en_gb" placeholder="Pays en EN.." aria-describedby="edit-form-name_en_gb-error" aria-invalid="false">
                                        <div id="edit-form-name_en_gb-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-code">Code</label>
                                        <input type="number" class="form-control form-control-alt" id="edit-form-code" name="edit-form-code" placeholder="code.." aria-describedby="edit-form-code-error" aria-invalid="false">
                                        <div id="edit-form-code-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-alpha2">Code aplpha 2</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-alpha2" name="edit-form-alpha2" placeholder="Alpha 2.." aria-describedby="edit-form-alpha2-error" aria-invalid="false">
                                        <div id="edit-form-alpha2-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="edit-form-alpha3">Code aplpha 3</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-alpha3" name="edit-form-alpha3" placeholder="Alpha 3.." aria-describedby="edit-form-alpha3-error" aria-invalid="false">
                                        <div id="edit-form-alpha3-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="update-country-btn"><i class="fa fa-fw fa-check mr-1"></i>Modifier pays</button>
                                    </div>
                                </form>
                                <!-- END Form country Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END country Edit Modal -->
@endsection

