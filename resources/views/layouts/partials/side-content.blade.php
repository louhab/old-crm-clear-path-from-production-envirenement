<!-- Side Content -->
<div class="content-side">
    <!-- Users Management -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">{{ __('lang.user_management') }}</h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" data-toggle="block-option"
                    data-action="content_toggle"></button>
            </div>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('users-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-users"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">{{ __('lang.users') }}</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a class="text-dark media py-2" href="{{ route('conseillers-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-users"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">{{ __('lang.Queue_conseillers_supports') }}</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END Users Management -->

    {{-- <!-- Notifications -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">{{ __('lang.params')}}</h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
            </div>
        </div>
        <div class="block-content">
            <form action="javascript:void(0)" method="POST" onsubmit="return false;">
                <div class="form-group">
                    <div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" id="so-settings-check1" name="so-settings-check1" checked="">
                        <label class="custom-control-label" for="so-settings-check1">Activer les notifications</label>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Notifications --> --}}

    <!-- Facturation -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">{{ __('lang.billings') }}</h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" data-toggle="block-option"
                    data-action="content_toggle"></button>
            </div>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('services-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-book-open"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">{{ __('lang.services') }}</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END Facturation -->

    <!-- Params -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">{{ __('lang.params') }}</h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" data-toggle="block-option"
                    data-action="content_toggle"></button>
            </div>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('import-leads') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">Importer leads</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('programs-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">Programmes configuration</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('payment-discount') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">{{ __('lang.Payments_Discount') }}</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <!--        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('orientation-home') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">Orientation</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>-->
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('forms-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">{{ __('lang.forms') }}</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('documents-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">Documents</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('field-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">Customer Fields</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('clears-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">Clears</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('sheet-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">Sheets</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('steps-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">Steps</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('campaigns-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">{{ __('lang.campaigns') }}</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('phases-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">{{ __('lang.steps') }}</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('schoollevels-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">{{ __('lang.schoollevels') }}</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('tags-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-shield"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">{{ __('lang.keywords') }}</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('emailnotifications-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-flag"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">{{ __('lang.emailnotification') }}</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('substepnotifications-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-flag"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">{{ __('lang.substepnotification') }}</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            <ul class="nav-items mb-0">
                <li>
                    <a class="text-dark media py-2" href="{{ route('countries-list') }}">
                        <div class="mr-3 ml-2">
                            <i class="si si-flag"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">{{ __('lang.country') }}</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        {{-- <div class="block-content">
            <ul class="nav-items mb-0">
                <li id="selected_leads_support">
                    <a class="text-dark media py-2" href="javascript:void(0)">
                        <div class="mr-3 ml-2">
                            <i class="si si-book-open"></i>
                        </div>
                        <div class="media-body">
                            <div class="font-size-sm font-w600">Paramètre d'affection support/leads</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div> --}}
    </div>
    <!-- END Params -->
</div>
<!-- END Side Content -->
