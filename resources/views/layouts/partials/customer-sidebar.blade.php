
<nav id="sidebar" aria-label="Main Navigation">
    <!-- Side Header -->
    <div class="content-header bg-white-5" style="padding-top: 35px;">
        <!-- Logo -->
        <a class="font-w600 text-dual" href="{{ url('dashboard') }}">
            <img src="{{ asset("/img/logo.png") }}" style="width: 200px;">
        </a>
        <!-- END Logo -->

        <!-- Extra -->
        <div>

            <!-- Close Sidebar, Visible only on mobile screens -->
            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
            <a class="d-lg-none btn btn-sm btn-dual ml-1" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
                <i class="fa fa-fw fa-times"></i>
            </a>
            <!-- END Close Sidebar -->
        </div>
        <!-- END Extra -->
    </div>
    <!-- END Side Header -->

    <!-- Sidebar Scrolling -->
    <div class="js-sidebar-scroll">
        <!-- Side Navigation -->
        <div class="content-side content-side-full" style="padding-top: 60px;">
            <ul class="nav-main">
                <!-- <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->routeIs('customer-inbox') ? ' active' : '' }}" href="{{ route('customer-inbox') }}">
                        <i class="nav-main-link-icon si si-docs"></i>
                        <span class="nav-main-link-name">{{ __('lang.messages')}}</span>
                    </a>
                </li>

                <li class="nav-main-heading">{{ __('lang.case')}}</li>-->
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->routeIs('customer-mycase') ? ' active' : '' }}" href="{{ route('customer-mycase', Auth::guard('customer')->user()->case->step->id) }}">
                        <i class="fa fa-fw fa-star"></i>
                        <span class="nav-main-link-name">{{ __('lang.mycase')}}</span>
                    </a>
                </li>

                <!-- <li class="nav-main-heading">{{ __('lang.billings')}}</li>
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->routeIs('customer-myquotes') ? ' active' : '' }}" href="{{ route('customer-myquotes') }}">
                        <span class="nav-main-link-name">{{ __('lang.myquotes')}}</span>
                    </a>
                </li>
                <!-- <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->routeIs('customer-mybills') ? ' active' : '' }}" href="{{ route('customer-mybills') }}">
                        <span class="nav-main-link-name">{{ __('lang.mybills')}}</span>
                    </a>
                </li> -->
            </ul>
        </div>
        <!-- END Side Navigation -->
    </div>
    <!-- END Sidebar Scrolling -->
</nav>
<!-- END Sidebar -->
