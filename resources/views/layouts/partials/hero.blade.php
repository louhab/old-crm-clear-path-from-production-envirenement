<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                {{ $title }} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">{{ $subTitle }}</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    @foreach($navItems as $navItem)
                        @if($loop->last)
                            <li class="breadcrumb-item" aria-current="page">
                                <a class="link-fx" href="">{{ $navItem }}</a>
                            </li>
                        @else
                            <li class="breadcrumb-item">@php echo Auth::guard('web')->check() ? $lang === "fr" ? ucfirst(Auth::user()->role->role_name) : ucfirst(Auth::user()->role->labelEng) : 'Customer'; @endphp</li>
                        @endif
                    @endforeach
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->
