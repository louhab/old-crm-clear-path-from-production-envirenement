<!-- Side Content -->
<div class="content-side">
    <!-- Notifications -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">{{ __('lang.params') }}</h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" data-toggle="block-option"
                    data-action="content_toggle"></button>
            </div>
        </div>
        <div class="block-content">
            <form action="javascript:void(0)" method="POST" onsubmit="return false;">
                <div class="form-group">
                    <div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" id="so-settings-check1"
                            name="so-settings-check1" checked="">
                        <label class="custom-control-label" for="so-settings-check1">Activer les notifications</label>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Notifications -->
</div>
<!-- END Side Content -->
