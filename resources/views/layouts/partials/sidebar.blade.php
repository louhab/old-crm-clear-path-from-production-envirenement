<nav id="sidebar" aria-label="Main Navigation">
    <!-- Side Header -->
    <div class="content-header bg-white-5" style="padding-top: 45px;">
        <!-- Logo -->
        <a class="font-w600 text-dual" href="{{ route('dashboard') }}">
            @php
                if (Config::get('app.locale') == 'fr') {
                    $logo = 'fr';
                } else {
                    $logo = 'en';
                }
            @endphp
            <img src="{{ asset('/img/logo-' . $logo . '.png') }}" style="width: 200px;">
        </a>
        <!-- END Logo -->

        <!-- Extra -->
        <div>

            <!-- Close Sidebar, Visible only on mobile screens -->
            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
            <a class="d-lg-none btn btn-sm btn-dual ml-1" data-toggle="layout" data-action="sidebar_close"
                href="javascript:void(0)">
                <i class="fa fa-fw fa-times"></i>
            </a>
            <!-- END Close Sidebar -->
        </div>
        <!-- END Extra -->
    </div>
    <!-- END Side Header -->

    <!-- Sidebar Scrolling -->
    <div class="js-sidebar-scroll">
        <!-- Side Navigation -->
        <div class="content-side content-side-full" style="padding-top: 80px;">
            <ul class="nav-main">
                @management_agent_support
                    <li class="nav-main-item{{ request()->is('dashboard*') ? ' open active ' : '' }}">
                        <a @administration class="nav-main-link nav-main-link-submenu" data-toggle="submenu"
                                aria-haspopup="true" aria-expanded="{{ request()->is('dashboard*') ? 'true' : 'false' }}false"
                            @else class="nav-main-link {{ request()->is('dashboard') ? ' active' : '' }}"
                            @endadministration href="{{ route('dashboard') }}">
                            <i class="nav-main-link-icon si si-cursor"></i>
                            <span class="nav-main-link-name">{{ __('lang.dashboard') }}</span>
                        </a>
                        @management
                            <ul class="nav-main-submenu">
                                <li class="nav-main-item">
                                    <a class="nav-main-link" href="{{ route('dashboard') }}">
                                        <span class="nav-main-link-name">{{ __('lang.leads') }}</span>
                                    </a>
                                </li>
                                <li class="nav-main-item">
                                    <a class="nav-main-link" href="{{ route('dashboard-clients') }}">
                                        <span class="nav-main-link-name">{{ __('lang.customers') }}</span>
                                    </a>
                                </li>
                            </ul>
                        @endmanagement
                    </li>
                @endmanagement_agent_support

                <li class="nav-main-heading">{{ __('lang.customers_management') }}</li>

                @if (!Auth::user()->isBackoffice() && !Auth::user()->isSuperBackoffice())
                    <li class="nav-main-item">
                        <a class="nav-main-link{{ request()->is('leads') ? ' active' : '' }}"
                            href="{{ route('leads-index') }}">
                            <i class="nav-main-link-icon si si-target"></i>
                            <span class="nav-main-link-name">{{ __('lang.leads') }}</span>
                        </a>
                    </li>
                @endif
                @show_clients
                @if (!Auth::user()->isBackoffice() && !Auth::user()->isSuperBackoffice())
                    <li class="nav-main-item">
                        <a class="nav-main-link{{ request()->is('cases') ? ' active' : '' }}"
                            href="{{ route('cases-list') }}">
                            <i class="nav-main-link-icon si si-briefcase"></i>
                            <span class="nav-main-link-name">{{ __('lang.clients') }}</span>
                        </a>
                    </li>
                    @if (Route::currentRouteName() == 'case-edit' &&
                            (Auth::user()->isBackoffice() || Auth::user()->isAdmin() || Auth::user()->isVerificateur()))
                        @livewire('block.document', ['customer_case' => $customer_case, 'location' => 'sidebar'])
                    @endif
                @endif
                @admission
                    <li class="nav-main-item">
                        <a class="nav-main-link{{ request()->is('soumissions') ? ' active' : '' }}"
                            href="{{ route('soumissions-index') }}">
                            <i class="nav-main-link-icon si si-target"></i>
                            <span class="nav-main-link-name">{{ __('lang.submissions') }}</span>
                        </a>
                    </li>
                    @admin
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('tasks') ? ' active' : '' }}"
                                href="{{ route('tasks-list') }}">
                                <i class="nav-main-link-icon si si-check"></i>
                                <span class="nav-main-link-name">{{ __('lang.tasks') }}</span>
                            </a>
                        </li>
                    @endadmin
                    @if (Route::currentRouteName() == 'soumission-view' &&
                            (Auth::user()->isBackoffice() ||
                                Auth::user()->isAdmin() ||
                                Auth::user()->isVerificateur() ||
                                Auth::user()->isSuperBackoffice()))
                        @livewire('block.document', ['customer_case' => $customer_case, 'location' => 'sidebar'])
                    @endif
                @endadmission
                @endshow_clients

                @management_agent_support
                    @admin
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('orientation-home') ? ' active' : '' }}"
                                href="{{ route('orientation-home') }}">
                                <i class="nav-main-link-icon si si-check"></i>
                                <span class="nav-main-link-name">Orientation</span>
                            </a>
                        </li>
                    @endadmin

                    {{-- new part for docs --}}
                    <li class="nav-main-heading">Documentation</li>
                    {{-- FIXME (link) --}}
                    <li class="nav-main-item">
                        <a class="nav-main-link"
                            href="https://drive.google.com/drive/folders/1fp-NNYCfCy-4RFq84LHpYg2RQHFCQ7Vn?usp=sharing"
                            target="_blank">
                            <i class="nav-main-link-icon si si-notebook"></i>
                            <span class="nav-main-link-name">Règlements et Objectifs</span>
                        </a>
                    </li>
                    @support_deny
                        <li class="nav-main-item">
                            <a class="nav-main-link"
                                href="https://drive.google.com/drive/folders/1-04lsQb-8BkMGfz9uiATeayOHsgdlvKp" target="_blank">
                                <i class="nav-main-link-icon si si-notebook"></i>
                                <span class="nav-main-link-name">Évaluation et Contrôle de qualité</span>
                            </a>
                        </li>
                    @endsupport_deny

                    <li class="nav-main-heading">{{ __('lang.followed') }}</li>
                    <li class="nav-main-item">
                        <a class="nav-main-link{{ request()->is('calendar') ? ' active' : '' }}"
                            href="{{ route('calendar') }}">
                            <i class="nav-main-link-icon si si-notebook"></i>
                            <span class="nav-main-link-name">{{ __('lang.agenda') }}</span>
                        </a>
                    </li>

                    @management
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('histocalls') ? ' active' : '' }}"
                                href="{{ route('histocalls-list') }}">
                                <i class="nav-main-link-icon si si-call-out"></i>
                                <span class="nav-main-link-name">{{ __('lang.call_hostory') }}</span>
                            </a>
                        </li>
                    @endmanagement
                @endmanagement_agent_support
                @financial
                    <li class="nav-main-heading">{{ __('lang.billings') }}</li>
                    <li class="nav-main-item">
                        <a class="nav-main-link{{ request()->is('finances') ? ' active' : '' }}"
                            href="{{ route('finances') }}">
                            <i class="nav-main-link-icon si si-calculator"></i>
                            <span class="nav-main-link-name">Finances</span>
                        </a>
                    </li>
                    {{-- <li class="nav-main-item">
                        <a class="nav-main-link{{ request()->is('quotes') ? ' active' : '' }}"
                            href="{{ route('quotes') }}">
                            <i class="nav-main-link-icon si si-docs"></i>
                            <span class="nav-main-link-name">{{ __('lang.estimate') }}</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link{{ request()->is('bills') ? ' active' : '' }}"
                            href="{{ route('bills') }}">
                            <i class="nav-main-link-icon si si-notebook"></i>
                            <span class="nav-main-link-name">{{ __('lang.invoices') }}</span>
                        </a>
                    </li> --}}
                    <li class="nav-main-heading">{{ __('') }} Activities</li>
                @endfinancial
                @admin
                    <li class="nav-main-item">
                        <a class="nav-main-link{{ request()->is('leadslog') ? ' active' : '' }}"
                            href="{{ route('leads-log') }}">
                            <i class="nav-main-link-icon far fa-file"></i>
                            <span class="nav-main-link-name">Log</span>
                        </a>
                    </li>
                @endadmin
            </ul>
        </div>
        <!-- END Side Navigation -->
    </div>
    <!-- END Sidebar Scrolling -->
</nav>
<!-- END Sidebar -->
