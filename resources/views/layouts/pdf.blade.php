<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Invoice</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,500;0,700;1,200;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" id="css-main" href="{{ asset('/js/plugins/flag-icon-css/css/flag-icon.min.css') }}">
    <style>

        body{
            font-family: "Poppins", sans-serif;
            background: url({{ public_path('/img/background_devis.png') }});
            /* background-size: 100%; */
            background-repeat: no-repeat;
            background-position: right bottom;
        }
        #invoice{
            padding: 0;
        }

        .invoice {
            position: relative;
            background-color: #FFF;
            min-height: 400px;
            padding: 5px
        }

        .invoice header {
            padding: 0;
            margin-bottom: 10px;
            margin-left: 30px;
            border-bottom: transparent;
        }

        .invoice .company-details {
            text-align: right
        }

        .invoice .company-details .name {
            margin-top: 0;
            margin-bottom: 0
        }

        .invoice .contacts {
            margin-bottom: 10px
        }

        .invoice .invoice-to {
            text-align: left
        }

        .invoice .invoice-to .to {
            margin-top: 0;
            margin-bottom: 0
        }

        .invoice .invoice-details {
            text-align: right
        }

        .invoice .invoice-details .invoice-id {
            margin-top: 0;
            color: #c61322
        }

        .invoice main {
            padding-bottom: 5px;
        }

        .invoice main .thanks {
            margin-top: -100px;
            font-size: 2em;
            margin-bottom: 50px;
        }

        .invoice main .notices {
            padding-left: 6px;
            border-left: 2px solid #c6394e
        }

        .invoice main .notices .notice {
            font-size: 1.2em
        }

        .notices {
            padding-top: 50px;
        }

        .invoice table {
            font-family: "Poppins", sans-serif;
            margin-top: 5px;
            margin-right: 100px;
            width: 100%;
            margin: 5px 30px;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px
        }

        .invoice table td {
            font-family: "Poppins", sans-serif;
            padding: 1px 5px;
            background: #fff;
            border: 1px solid #c6394e;
            /* text-align: right; */
        }

        .invoice table th {
            font-family: "Poppins", sans-serif;
            white-space: nowrap;
            background: #c6394e;
            color: #fff;
            /* font-weight: 400; */
            font-size: 1em;
            padding: 1px 5px;
        }

        .invoice table td h3 {
            font-family: "Poppins", sans-serif;
            margin: 0;
            font-weight: 400;
            color: #c6394e;
            font-size: 1em
        }

        .invoice table .qty,.invoice table .total,.invoice table .unit {
            font-family: "Poppins", sans-serif;
            text-align: center;
            vertical-align: middle;
            font-size: 1em
        }

        .invoice table .no {
            font-family: "Poppins", sans-serif;
            color: #000;
            font-size: 1em;
            background: #fff;
        }

        .invoice table tbody tr.srow td {
            background: #c6394e;text-align: right;color: #fff;
        }

        .invoice table .unit {
            background: #fff;
        }
        
        .invoice table .total {
            background: #fff;
            color: #000;
        }

        .invoice table tbody tr:last-child td {
            font-family: "Poppins", sans-serif;
            border: none
        }

        .invoice table tfoot td {
            font-family: "Poppins", sans-serif;
            background: 0 0;
            border-bottom: none;
            white-space: nowrap;
            text-align: right;
            padding: 5px 10px;
            font-size: 1em;
            /* border-top: 1px solid #aaa */
        }

        .invoice table tfoot tr:first-child td {
            border-top: none
        }

        .invoice table tfoot tr:last-child td {
            color: #c6394e;
            font-size: 1.2em;
            /* border-top: 1px solid #c6394e */
        }

        .invoice table tfoot tr td:first-child {
            font-family: "Poppins", sans-serif;
            border: none
        }

        .invoice footer {
            font-family: "Poppins", sans-serif;
            width: 70%;
            text-align: left;
            color: #777;
            /* border-top: 1px solid #c6394e; */
            border-left: 1px solid #c6394e;
            padding:0;
            padding-left: 6px;
            margin-left: 30px;
            position:absolute;
            bottom:50px;
            height:150px;   /* Height of the footer */
        }

        @media print {
            .invoice {
                font-family: "Poppins", sans-serif;
                font-size: 11px!important;
                overflow: hidden!important
            }

            .invoice footer {
                font-family: "Poppins", sans-serif;
                position: absolute;
                bottom: 30px;
                page-break-after: always
            }

            .invoice>div:last-child {
                font-family: "Poppins", sans-serif;
                page-break-before: always
            }
        }
    </style>
    @yield('css_after')
    <!-- Stylesheets -->
</head>
<body>
@yield('content')
</body>
</html>
