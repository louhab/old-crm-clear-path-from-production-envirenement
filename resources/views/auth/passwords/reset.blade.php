<?php
    $lang = session()->get('locale');
?>

@extends('layouts.customer')

@section('content')
<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="bg-body-light mb-4">
                <div class="content content-full">
                    <div class="text-center">
                        <h1 class="flex-sm-fill h3 my-2">
                            {{ __("lang.customer_welcome_message") }}
                        </h1>
                        <h1 class="flex-sm-fill h3 my-2">
                            {{ __("lang.customer_services_message") }}
                        </h1>
                    </div>
                </div>
            </div>
            <!-- <div class="card mb-4">
                <div class="card-body">
                    <a class="img-link img-link-zoom-in img-thumb img-lightbox" href="#">
                        <img class="img-fluid" src="#" alt="Conseiller Images..">
                    </a>
                </div>
            </div> -->
            <div class="block mt-3">
                <div class="block-content">
                    @php
                        $customer = \App\Models\Customer::where("id", Illuminate\Support\Facades\Auth::id())->first();
                        $lead = \App\Models\Lead::where("id", $customer->lead_id)->first();
                        $conseiller = \App\Models\User::where("id", $lead->conseiller_id)->first();
                    @endphp
                    @if ($lang === "fr")
                    <p>
                        Bonjour <b>{{ $customer->firstname }},</b><br />
                        Je suis {{ $conseiller->name }} votre {{ $conseiller->gender == 'female' ? 'Conseillère':'Conseiller' }}
                        qui vous accompagnera lors de votre visite pour répondre à vos requêtes.<br />
                        Veuillez réinitialiser le mot de passe en insérant le MP Actuel
                        dans la barre <b>(Mot de passe courante)</b>, ensuite vous changerez
                        le MP dans la barre <b>(Mot de passe)</b> et vous confirmerez le nouveau MP dans la
                        barre <b>(Confirmer mot de passe)</b>
                    </p>
                    @else
                    <p>
                        Hello, <b>{{ $customer->firstname }},</b><br />
                        I am {{ $conseiller->name }} your Adviser who will accompany you during the whole process.<br />
                        Please, reset the password by inserting the <b>Current password</b> in the bar, then you will load the <b>new password </b> in the bar and, finally, <b>Confirm the new password</b>.
                    </p>
                    @endif
                    <br />
                    <form method="POST" action="{{ route('changePassword') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="current-password" class="col-md-4 col-form-label text-md-right">{{ __("lang.customer_current_password") }}</label>

                            <div class="col-md-6">
                                <input id="current-password" type="password" class="form-control @error('current-password') is-invalid @enderror" name="current-password" required>

                                @error('current-password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('lang.customer_password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('lang.customer_confirm_password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary float-right">
                                    {{ __('lang.customer_change_password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
