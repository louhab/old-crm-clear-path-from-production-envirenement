<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>

@extends('layouts.simple')

@section('js_after')
    <script src="{{ asset('js/pages/login.js') }}"></script>
@endsection



@section('content')
    <div id="page-container">
        <main id="main-container">
            <div class="bg-image" style="background-image: url({{ asset('img/bgoffice.jpg') }});">
                <div class="row no-gutters bg-primary-dark-op" style=" background-color: rgb(156 5 5 / 57%)!important;">
                    <div class="hero-static col-lg-6 d-none d-lg-flex flex-column justify-content-center">
                        <div class="p-4 p-xl-5 flex-grow-1 d-flex align-items-center">
                            <div class="w-100">
                                <a class="link-fx font-w600 font-size-h2 text-white" href="#">
                                    Clear Path <span class="font-w400">Consulting</span>
                                </a>
                                <p class="text-white-75 mr-xl-8 mt-2">
                                </p>
                            </div>
                        </div>
                        <div class="p-4 p-xl-5 d-xl-flex justify-content-between align-items-center font-size-sm">
                            <p class="font-w500 text-white-50 mb-0">
                                <strong>Clear Path Consulting</strong> &copy; <span data-toggle="year-copy"></span>
                            </p>
                        </div>
                    </div>
                    <div class="hero-static col-lg-6 d-flex flex-column align-items-center bg-white">
                        <div class="p-3 w-100 d-lg-none text-center">
                            <a class="link-fx font-w600 font-size-h3 text-dark" href="/">
                                Clear Path <span class="font-w400">Consulting</span>
                            </a>
                        </div>
                        <div class="p-4 w-100 flex-grow-1 d-flex align-items-center">
                            <div class="w-100">
                                <div class="text-center mb-5">
                                    <h1 class="font-w700 mb-2">
                                        <img src="{{ asset("/img/logo-".(Config::get('app.locale') == "fr" ? "fr":"en").".png") }}" style="width: 320px;">
                                        <br>
                                        <br>
                                    </h1>
                                </div>
                                <div class="row no-gutters justify-content-center">
                                    <div class="col-sm-8 col-xl-4">
                                        <form class="js-validation-signin" method="POST" action="{{ isset($isCustomer) && $isCustomer == true ? route('customerLogin') : route('login') }}">
                                            @csrf
                                            @if (session('error'))
                                                <div class="alert alert-danger">
                                                    {{ session('error') }}
                                                </div>
                                            @endif
                                            @if(isset($isCustomer) && $isCustomer == true)
                                                <div class="form-group">
                                                    <input id="phone" type="phone" class="form-control form-control-lg form-control-alt py-4 @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone"  placeholder="Phone" autofocus>
                                                    @error('phone')
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                    @enderror
                                                </div>
                                            @else
                                                <div class="form-group">
                                                    <input id="email" type="email" class="form-control form-control-lg form-control-alt py-4 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email"  placeholder="Email" autofocus>
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            @endif
                                            <div class="form-group">
                                                <input id="password" type="password" class="form-control form-control-lg form-control-alt py-4 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"  placeholder="Mot de passe">
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group d-flex justify-content-between align-items-center">
                                                <div>
                                                    @if (Route::has('password.request'))
                                                        <a class="text-muted font-size-sm font-w500 d-block d-lg-inline-block mb-1" href="{{ route('password.request') }}">
                                                            {{ __('Forgot Your Password?') }}
                                                        </a>
                                                    @endif

                                                </div>
                                                <div>
                                                    <button type="submit" class="btn btn-lg btn-alt-primary">
                                                        <i class="fa fa-fw fa-sign-in-alt mr-1 opacity-50"></i> {{ __('Login') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        @if(isset($isCustomer) && $isCustomer)
                                        <div class="form-group pt-5">
                                            {{-- <label for="lang">Langue</label> --}}
                                            {{Form::select('lang',["en" => __('lang.english'), "fr" => __('lang.french')], $lang ,['class' => 'form-control form-control-lg form-control-alt', 'id' => 'customer-lang'])}}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="px-4 py-3 w-100 d-lg-none d-flex flex-column flex-sm-row justify-content-between font-size-sm text-center text-sm-left">
                            <p class="font-w500 text-black-50 py-2 mb-0">
                                <strong>Clear Path Consulting</strong> &copy; <span data-toggle="year-copy"></span>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
