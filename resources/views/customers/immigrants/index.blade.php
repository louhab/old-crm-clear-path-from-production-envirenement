@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/customers/immigrants.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Clients','subTitle' => 'immigrants', 'navItems' => ['Admin', 'Clients']])

    <!-- Page Content -->
    <div class="content">


        <!-- customers Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">
                            <div class="col-3">
                                <select class="form-control dt_search_field" name="ranking">
                                    <option value="">Sélectionnez un ranking</option>
                                    @foreach(\App\Models\Ranking::all() as $ranking)
                                        <option value="{{ $ranking->id }}">{{ $ranking->ranking_label }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-3">
                                <input type="text" class="form-control dt_search_field" name="name" placeholder="Nom">
                            </div>
                            <div class="col-3">
                                <input type="text" class="form-control dt_search_field" name="email" placeholder="Email">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="immigrants-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Liste des clients immigrants</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary"  id="immigrant-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter client immigrant</button>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="immigrants-dt">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">ID</th>
                        <th>Prénom</th>
                        <th>Nom</th>
                        <th>Email</th>
                        <th>Pays</th>
                        <th>Ranking</th>
                        <th>Date de naissance</th>
                        <th style="width: 15%;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END customers Table -->




    </div>
    <!-- END Page Content -->

    <!-- immigrant Add Modal -->
    <div class="modal" id="modal-immigrant-add" tabindex="-1" role="dialog" aria-labelledby="modal-immigrant-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-immigrant-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter immigrant</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form immigrant Add -->
                                <form action="" method="POST" onsubmit="return false;" id="immigrant-add-form">
                                    <div class="form-group">
                                        <label for="add-form-firstname">Prénom</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-firstname" name="add-form-firstname" placeholder="Prénom.." aria-describedby="add-form-firstname-error" aria-invalid="false">
                                        <div id="add-form-firstname-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-lastname">Nom</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-lastname" name="add-form-lastname" placeholder="Nom.." aria-describedby="add-form-lastname-error" aria-invalid="false">
                                        <div id="add-form-lastname-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-email">Email</label>
                                        <input type="email" class="form-control form-control-alt" id="add-form-email" name="add-form-email" placeholder="Email.." aria-describedby="add-form-email-error" aria-invalid="false">
                                        <div id="add-form-email-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-languagelevel">Niveau langue</label>
                                        <select class="form-control form-control-alt" id="add-form-languagelevel" name="add-form-languagelevel" aria-describedby="add-form-languagelevel-error" aria-invalid="false">
                                            <option value="">Sélectionnez un niveau</option>
                                            @foreach(\App\Models\EnglishLevel::all() as $level)
                                                <option value="{{ $level->id }}">{{ $level->english_level_name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-languagelevel-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-country">Pays</label>
                                        <select class="form-control form-control-alt" id="add-form-country" name="add-form-country" aria-describedby="add-form-country-error" aria-invalid="false">
                                            <option value="">Sélectionnez un pays</option>
                                            @foreach(\App\Models\Country::all() as $country)
                                                <option value="{{ $country->id }}">{{ $country->name_fr_fr }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-country-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-adresse1">Adresse Ligne 1</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-adresse1" name="add-form-adresse1" placeholder="Adresse (ligne 1).." aria-describedby="add-form-adresse1-error" aria-invalid="false">
                                        <div id="add-form-adresse1-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-adresse2">Adresse Ligne 2</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-adresse2" name="add-form-adresse2" placeholder="Adresse (ligne 2).." aria-describedby="add-form-adresse2-error" aria-invalid="false">
                                        <div id="add-form-adresse2-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-postalcode">Code Postal</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-postalcode" name="add-form-postalcode" placeholder="Code postal.." aria-describedby="add-form-postalcode-error" aria-invalid="false">
                                        <div id="add-form-postalcode-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-experience">Experience professionelle (ans)</label>
                                        <input type="number" class="form-control form-control-alt" id="add-form-experience" name="add-form-experience" placeholder="Experience professionelle.." aria-describedby="add-form-experience-error" aria-invalid="false">
                                        <div id="add-form-experience-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-immigrant-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter immigrant</button>
                                    </div>
                                </form>
                                <!-- END Form immigrant Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END immigrant Add Modal -->

    <!-- immigrant Edit Modal -->
    <div class="modal" id="modal-immigrant-edit" tabindex="-1" role="dialog" aria-labelledby="modal-immigrant-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-immigrant-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Modifier immigrant</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form immigrant Edit -->
                                <form action="" method="POST" onsubmit="return false;" id="immigrant-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-firstname">Prénom</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-firstname" name="edit-form-firstname" placeholder="Prénom.." aria-describedby="edit-form-firstname-error" aria-invalid="false">
                                        <div id="edit-form-firstname-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-lastname">Nom</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-lastname" name="edit-form-lastname" placeholder="Nom.." aria-describedby="edit-form-lastname-error" aria-invalid="false">
                                        <div id="edit-form-lastname-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-email">Email</label>
                                        <input type="email" class="form-control form-control-alt" id="edit-form-email" name="edit-form-email" placeholder="Email.." aria-describedby="edit-form-email-error" aria-invalid="false">
                                        <div id="edit-form-email-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-languagelevel">Niveau langue</label>
                                        <select class="form-control form-control-alt" id="edit-form-languagelevel" name="edit-form-languagelevel" aria-describedby="edit-form-languagelevel-error" aria-invalid="false">
                                            <option value="">Sélectionnez un niveau</option>
                                            @foreach(\App\Models\EnglishLevel::all() as $level)
                                                <option value="{{ $level->id }}">{{ $level->english_level_name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-languagelevel-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-country">Pays</label>
                                        <select class="form-control form-control-alt" id="add-form-country" name="add-form-country" aria-describedby="add-form-country-error" aria-invalid="false">
                                            <option value="">Sélectionnez un pays</option>
                                            @foreach(\App\Models\Country::all() as $country)
                                                <option value="{{ $country->id }}">{{ $country->name_fr_fr }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-country-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-adresse1">Adresse Ligne 1</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-adresse1" name="edit-form-adresse1" placeholder="Adresse (ligne 1).." aria-describedby="edit-form-adresse1-error" aria-invalid="false">
                                        <div id="edit-form-adresse1-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-adresse2">Adresse Ligne 2</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-adresse2" name="edit-form-adresse2" placeholder="Adresse (ligne 2).." aria-describedby="edit-form-adresse2-error" aria-invalid="false">
                                        <div id="edit-form-adresse2-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-postalcode">Code Postal</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-postalcode" name="edit-form-postalcode" placeholder="Code postal.." aria-describedby="edit-form-postalcode-error" aria-invalid="false">
                                        <div id="edit-form-postalcode-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-experience">Experience professionelle (ans)</label>
                                        <input type="number" class="form-control form-control-alt" id="edit-form-experience" name="edit-form-experience" placeholder="Experience professionelle.." aria-describedby="edit-form-experience-error" aria-invalid="false">
                                        <div id="edit-form-experience-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="update-immigrant-btn"><i class="fa fa-fw fa-check mr-1"></i>Modifier immigrant</button>
                                    </div>
                                </form>
                                <!-- END Form immigrant Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END immigrant Edit Modal -->
@endsection
