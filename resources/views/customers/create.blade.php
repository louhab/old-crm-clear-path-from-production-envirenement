@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/customers/create.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Clients','subTitle' => 'créer', 'navItems' => ['Clients', 'créer']])

    <!-- Page Content -->
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les informations !</a>
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        <!-- Customer Create Form -->
        <form action="{{ route('customer-store') }}" method="POST" id="customer-form" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="block block-rounded block-themed">
                        <div class="block-header bg-city">
                            <h3 class="block-title">Infos Clients</h3>
                        </div>
                        <div class="block-content">
                            <div class="form-group">
                                <label for="program">Programme</label>
                                <span class="text-danger">*</span>
                                {{Form::select('program', \App\Models\Program::pluck('name', 'id'), old('program'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('program') ? ' is-invalid' : (isset($errors) && $errors->has('program') ? 'true' : 'false')), 'aria-describedby' => 'program-error', 'aria-invalid' => ''])}}
                                <div id="program-error" class="invalid-feedback">{{ isset($errors) && $errors->has('program') ? $errors->first('program') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="budget">Budget</label>
                                <input type="number" step=".01" class="form-control form-control-alt{{ isset($errors) && $errors->has('budget') ? ' is-invalid' : '' }}" id="budget" name="budget" value="{{ old('budget') }}" placeholder="Budget.." aria-describedby="budget-error" aria-invalid="{{ isset($errors) && $errors->has('budget') ? 'true' : 'false' }}">
                                <div id="budget-error" class="invalid-feedback">{{ isset($errors) && $errors->has('budget') ? $errors->first('budget') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="firstname">Prénom</label>
                                <span class="text-danger">*</span>
                                <input type="text" class="form-control form-control-alt{{ isset($errors) && $errors->has('firstname') ? ' is-invalid' : '' }}" id="firstname" name="firstname" value="{{ old('firstname') }}" placeholder="Prénom.." aria-describedby="firstname-error" aria-invalid="{{ isset($errors) && $errors->has('firstname') ? 'true' : 'false' }}">
                                <div id="firstname-error" class="invalid-feedback">{{ isset($errors) && $errors->has('firstname') ? $errors->first('firstname') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="lastname">Nom</label>
                                <span class="text-danger">*</span>
                                <input type="text" class="form-control form-control-alt{{ isset($errors) && $errors->has('lastname') ? ' is-invalid' : '' }}" id="lastname" name="lastname" value="{{ old('lastname') }}" placeholder="Nom.." aria-describedby="lastname-error" aria-invalid="{{ isset($errors) && $errors->has('lastname') ? 'true' : 'false' }}">
                                <div id="lastname-error" class="invalid-feedback">{{ isset($errors) && $errors->has('lastname') ? $errors->first('lastname') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <span class="text-danger">*</span>
                                <input type="email" class="form-control form-control-alt{{ isset($errors) && $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" placeholder="Email.." aria-describedby="email-error" aria-invalid="{{ isset($errors) && $errors->has('email') ? 'true' : 'false' }}">
                                <div id="email-error" class="invalid-feedback">{{ isset($errors) && $errors->has('email') ? $errors->first('email') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="birthday">Date de naissance</label>
                                <span class="text-danger">*</span>
                                <input type="date" class="form-control form-control-alt{{ isset($errors) && $errors->has('birthday') ? ' is-invalid' : '' }}" id="birthday" name="birthday" value="{{ old('birthday') }}" placeholder="Date de naissance.." aria-describedby="birthday-error" aria-invalid="{{ isset($errors) && $errors->has('birthday') ? 'true' : 'false' }}">
                                <div id="birthday-error" class="invalid-feedback">{{ isset($errors) && $errors->has('birthday') ? $errors->first('birthday') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="english">Niveau anglais</label>
                                {{Form::select('english', \App\Models\EnglishLevel::pluck('english_level_name', 'id'), old('english'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('english') ? ' is-invalid' : (isset($errors) && $errors->has('english') ? 'true' : 'false')), 'aria-describedby' => 'english-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner niveau...'])}}
                                <div id="english-error" class="invalid-feedback">{{ isset($errors) && $errors->has('english') ? $errors->first('english') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="school">Niveau étude</label>
                                {{Form::select('school', \App\Models\SchoolLevel::pluck('school_level_name', 'id'), old('school'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('school') ? ' is-invalid' : (isset($errors) && $errors->has('school') ? 'true' : 'false')), 'aria-describedby' => 'school-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner niveau...'])}}
                                <div id="school-error" class="invalid-feedback">{{ isset($errors) && $errors->has('school') ? $errors->first('school') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="country">Pays</label>
                                {{Form::select('country', \App\Models\Country::orderBy('name_fr_fr', 'asc')->pluck('name_fr_fr', 'id'), old('country'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('country') ? ' is-invalid' : (isset($errors) && $errors->has('country') ? 'true' : 'false')), 'aria-describedby' => 'country-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner pays...'])}}
                                <div id="country-error" class="invalid-feedback">{{ isset($errors) && $errors->has('country') ? $errors->first('country') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="phone">Téléphone</label>
                                <input type="text" class="form-control form-control-alt{{ isset($errors) && $errors->has('phone') ? ' is-invalid' : '' }}" id="phone" name="phone" value="{{ old('phone') }}" placeholder="Tél.." aria-describedby="phone-error" aria-invalid="{{ isset($errors) && $errors->has('phone') ? 'true' : 'false' }}">
                                <div id="phone-error" class="invalid-feedback">{{ isset($errors) && $errors->has('phone') ? $errors->first('phone') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="adresse1">Adresse ligne 1</label>
                                <textarea class="form-control form-control-alt{{ isset($errors) && $errors->has('adresse1') ? ' is-invalid' : '' }}" id="adresse1" name="adresse1" placeholder="Adresse ligne 1.." aria-describedby="adresse1-error" aria-invalid="{{ isset($errors) && $errors->has('adresse1') ? 'true' : 'false' }}">{{ old('adresse1') }}</textarea>
                                <div id="adresse1-error" class="invalid-feedback">{{ isset($errors) && $errors->has('adresse1') ? $errors->first('adresse1') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="adresse2">Adresse ligne 2</label>
                                <textarea class="form-control form-control-alt{{ isset($errors) && $errors->has('adresse2') ? ' is-invalid' : '' }}" id="adresse2" name="adresse2" placeholder="Adresse ligne 2.." aria-describedby="adresse2-error" aria-invalid="{{ isset($errors) && $errors->has('adresse2') ? 'true' : 'false' }}">{{ old('adresse2') }}</textarea>
                                <div id="adresse2-error" class="invalid-feedback">{{ isset($errors) && $errors->has('adresse2') ? $errors->first('adresse2') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="postal">Code postal</label>
                                <input type="text" class="form-control form-control-alt{{ isset($errors) && $errors->has('postal') ? ' is-invalid' : '' }}" id="postal" name="postal" value="{{ old('postal') }}" placeholder="Code postal.." aria-describedby="postal-error" aria-invalid="{{ isset($errors) && $errors->has('postal') ? 'true' : 'false' }}">
                                <div id="postal-error" class="invalid-feedback">{{ isset($errors) && $errors->has('postal') ? $errors->first('postal') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="ranking">Ranking</label>
                                {{Form::select('ranking', \App\Models\Ranking::pluck('ranking_label', 'id'), old('ranking'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('ranking') ? ' is-invalid' : (isset($errors) && $errors->has('ranking') ? 'true' : 'false')), 'aria-describedby' => 'ranking-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner ranking...'])}}
                                <div id="ranking-error" class="invalid-feedback">{{ isset($errors) && $errors->has('ranking') ? $errors->first('ranking') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="projected">Diplôme projeté</label>
                                {{Form::select('projected', \App\Models\ProjectedDiplomaType::pluck('projected_diploma_type_name', 'id'), old('projected'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('projected') ? ' is-invalid' : (isset($errors) && $errors->has('projected') ? 'true' : 'false')), 'aria-describedby' => 'projected-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner diplôme...'])}}
                                <div id="projected-error" class="invalid-feedback">{{ isset($errors) && $errors->has('projected') ? $errors->first('projected') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="pdomain">Domaine professionel</label>
                                {{Form::select('pdomain', \App\Models\ProfessionalDomain::pluck('professional_domain_name', 'id'), old('pdomain'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('pdomain') ? ' is-invalid' : (isset($errors) && $errors->has('pdomain') ? 'true' : 'false')), 'aria-describedby' => 'pdomain-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner domaine...'])}}
                                <div id="pdomain-error" class="invalid-feedback">{{ isset($errors) && $errors->has('pdomain') ? $errors->first('pdomain') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="experience">Expérience professionelle</label>
                                <span class="text-danger">*</span>
                                <input type="number" class="form-control form-control-alt{{ isset($errors) && $errors->has('experience') ? ' is-invalid' : '' }}" id="experience" name="experience" value="{{ old('experience') }}" aria-describedby="experience-error" aria-invalid="{{ isset($errors) && $errors->has('experience') ? 'true' : 'false' }}">
                                <div id="experience-error" class="invalid-feedback">{{ isset($errors) && $errors->has('experience') ? $errors->first('experience') : '' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block block-rounded block-themed">
                        <div class="block-header bg-city">
                            <h3 class="block-title">Documents</h3>
                        </div>
                        <div class="block-content">
                            <div class="form-group">
                                <div class="custom-file" lang="fr">
                                    <!-- Populating custom file input label with the selected filename (data-toggle="custom-file-input" is initialized in Helpers.coreBootstrapCustomFileInput()) -->
                                    <!-- When multiple files are selected, we use the word 'Files'. You can easily change it to your own language by adding the following to the input, eg for DE: data-lang-files="Dateien" -->
                                    <input type="file" class="custom-file-input js-custom-file-input-enabled{{ isset($errors) && $errors->has('file-input-cv') ? ' is-invalid' : '' }}" data-toggle="file-input-cv" id="file-input-cv" name="file-input-cv" aria-describedby="file-input-cv-error" aria-invalid="{{ isset($errors) && $errors->has('file-input-cv') ? 'true' : 'false' }}">
                                    <div id="file-input-cv-error" class="invalid-feedback">{{ isset($errors) && $errors->has('file-input-cv') ? $errors->first('file-input-cv') : '' }}</div>
                                    <label class="custom-file-label" id="label-cv-input" for="file-input-cv">Sélectionnez CV</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-lg btn-alt-primary btn-block" id="update-customer-btn"><i class="fa fa-fw fa-check mr-1"></i>Enregistrer</button>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </form>
        <!-- END Customer Create Form -->
    </div>
    <!-- END Page Content -->
@endsection
