@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/customers/list.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Clients','subTitle' => 'customers', 'navItems' => ['Admin', 'Clients']])

    <!-- Page Content -->
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les informations !</a>
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        <!-- customers Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">

                            <div class="col-3">
                                <input type="text" class="form-control dt_search_field" name="name" placeholder="Nom">
                            </div>
                            <div class="col-2">
                                <input type="text" class="form-control dt_search_field" name="email" placeholder="Email">
                            </div>
                            <div class="col-2">
                                <select class="form-control dt_search_field" name="country">
                                    <option value="">Sélectionnez un pays</option>
                                    @foreach(\App\Models\Country::all() as $country)
                                        <option value="{{ $country->id }}">{{ $country->name_fr_fr }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-2">
                                <select class="form-control dt_search_field" name="ranking">
                                    <option value="">Sélectionnez un ranking</option>
                                    @foreach(\App\Models\Ranking::all() as $ranking)
                                        <option value="{{ $ranking->id }}">{{ $ranking->ranking_label }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="customers-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Liste des customers</h3>
                    <div class="block-options">
                        <a href="{{ route('customer-create') }}" class="btn btn-alt-primary"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter client</a>
                    </div>
                </div>
                <h4></h4>

                <div class="table-responsive">
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                    <table class="table table-bordered table-striped table-vcenter" id="customers-dt">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 80px;">ID</th>
                            <th>Prénom</th>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Programme</th>
                            <th>Etape dossier</th>
                            <th>Pays</th>
                            <th>CV</th>
                            <th>Ranking</th>
                            <th>Date de naissance</th>
                            <th style="width: 15%;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END customers Table -->
    </div>
    <!-- END Page Content -->
@endsection
