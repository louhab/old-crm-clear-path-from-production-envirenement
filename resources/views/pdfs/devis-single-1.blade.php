<?php
    $quote = \App\Models\Quote::where('customer_id', $customer->id)->first();
    $currency = $customer->currency_id ? \App\Models\Currency::where("id", $customer->currency_id)->first()->iso : "MAD";
    $lead = \App\Models\Lead::where("id", $customer->lead_id)->first();
?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Invoice</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">-->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,500;0,700;1,200;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <style>
        @page { margin: 0in;size: 700pt 1050pt; }
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100&display=swap');
        * {
            margin: 0;
            padding: 0;
        }
        body {
            font-family: 'Poppins', sans-serif;
            font-size: 16px;
            background: url("{{ public_path('/img/background_devis.png') }}");
            background-size: 350px;
            background-repeat: no-repeat;
            background-position: 100% 100%;
            margin-top:1cm;
        }
        .head-info li{
            background: #c0c9d2;
            color: black;
            margin: 5px 0;
        }
        .page-break {
            page-break-before: always;
        }
        .sectionContainer{
            /*border: 1px solid #ff2929;*/
            border-radius: 7px;
            width: 97%;
            margin: 0;
            padding: 5px 12px 5px 12px;
        }
        .sectionContainer  p{
            font-family: 'Poppins', serif;
            font-weight: 300;
            line-height: 0.9em;

        }
        table {
            border-collapse: collapse;
        }
        table, th, td {
            border: 2px solid #F4CCCC;
            text-align: center;
        }
        .total-step {
            background-color: #F4CCCC;
            color: white;
        }
    </style>
</head>
<body>
<div style="margin: 1.6em;">
    <div class="sectionContainer" style="text-align: center;">
        <img class="image" style="width: 140px;" src="{{ public_path('/img/logo.png') }}" alt="">
    </div>
    <div class="sectionContainer">
        <table>
            <thead>
            <tr>
                <th colspan="3" style="color: #FF0100;">
                    @if($lead->lang === "en") Express entry program @else Programme Entree Express @endif
                </th>
            </tr>
            <tr class="total-step">
                <th width="20%" style=""></th>
                <th style="text-align:center;border-left: none;">Description</th>
                <th width="10%">@if($lead->lang === "en") Amount @else Montants @endif</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td rowspan='6'>
                    @if($lead->lang === "en") Phase @else Etape @endif 1:<br>
                    <span style="color: #FF0100;">@if($lead->lang === "en") Data Analysis and Eligibility @else Analyse des données et admissibilité @endif</span>
                </td>
                <td>
                    <p style="color: #FF0100; font-weight: 500;">
                        @if($lead->lang === "en") Meeting @else Rencontre @endif 1 : @if($lead->lang === "en") Data Analysis and Eligibility @else Analyse des données et admissibilité @endif
                    </p>
                </td>
                <td class="unit" rowspan='6'>
                    @if($currency === "MAD")
                        200
                    @elseif ($currency === "EUR")
                        50
                    @else
                        50
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Information collection and student questionnaire completion @else Collecte des informations et remplir le questionnaire étudiant @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Analyze information and recommendations Borrar 1004 @else Analyse des informations et recommandations Clear 1004 @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                    @if($lead->lang === "en") Explanation of documents: Clear 10, CNP, Quote, contract @else Explication des documents : Clear 10, CNP, Devis, contrat @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Access to the portal and customer area @else Accès au portail et espace client @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Become a member of the Clear Path Canada network to receive all the news @else Devenir membre du réseau Clear Path Canada pour recevoir toutes les nouvelles @endif
                    </div>
                </td>
            </tr>
            <tr class="total-step">
                <td></td>
                <td class="align-middle">
                    <div class="text-muted">
                        Total @if($lead->lang === "en") Phase @else étape @endif 1
                    </div>
                </td>
                <td class="unit">
                    @if($currency === "MAD")
                        200
                    @elseif ($currency === "EUR")
                        50
                    @else
                        50
                    @endif
                </td>
            </tr>
            <!-- Etape 2-->
            <!--subtep1-->
            <tr>
                <td rowspan='18'>
                    @if($lead->lang === "en") Phase @else Etape @endif 2:<br>
                    <span style="color: #FF0100;">@if($lead->lang === "en") Development of the immigration project @else Développement du projet d'immigration @endif</span>
                </td>
                <td>
                    <p style="color: #FF0100; font-weight: 500;">
                        @if($lead->lang === "en") Meeting @else Rencontre @endif 2 : @if($lead->lang === "en") Preparation of the equivalence request and language tests @else Préparation de la demande d'equivalence et les tests de langues @endif
                    </p>
                </td>
                <td class="unit" rowspan='8'>
                    @if($currency === "MAD")
                        700
                    @elseif ($currency === "EUR")
                        130
                    @else
                        100
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") File opening  @else Ouverture du dossier @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Explanation of the procedure: Clear 11, Clear 12 @else Explication de la procédure : Clear 11, Clear 12 @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Development of the immigration project, support and supervision @else Développement du projet d'immigration @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Preparation of the equivalence of diplomas request @else Préparation de la demande d'equivalence des diplomes @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Preparation for language tests: TCF Canada, TEF Canada and IELTS @else Preparation des tests de langues : TCF Canada, TEF Canada et IELTS @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Choice of the CNP @else Le choix du CNP @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Equivalence of diplomas request presentation @else Soumission de la demande d'equivalence des diplomes @endif
                    </div>
                </td>
            </tr>
            <!--subtep2-->
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") WES fees @else Frais WES @endif
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        2450
                    @elseif ($currency === "EUR")
                        260
                    @else
                        250
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        TCF Canada
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        2750
                    @elseif ($currency === "EUR")
                        270
                    @else
                        280
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        TEF Canada
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        2750
                    @elseif ($currency === "EUR")
                        270
                    @else
                        280
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        IELTS
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        2750
                    @elseif ($currency === "EUR")
                        270
                    @else
                        280
                    @endif
                </td>
            </tr>
            <!--substep3-->
            <tr>
                <td>
                    <p style="color: #FF0100; font-weight: 500;">
                        @if($lead->lang === "en") Meeting @else Rencontre @endif 3 : @if($lead->lang === "en") Preparation of the Express Entry profile application @else Préparation de la demande du profil Entree Express @endif
                    </p>
                </td>
                <td class="unit" rowspan='6'>
                    @if($currency === "MAD")
                        5000
                    @elseif ($currency === "EUR")
                        600
                    @else
                        600
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Validation of the equivalence request results @else Valider les resultas de la demande dequivalence @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Validation of the language test results @else Valider les resultats des test de francais @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") CNP choice validation @else Valider le choix du CNP @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Creation of the express entry profile @else Creation du profil Entree Express @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Express Entry profile application and presentation @else Soumission de la demande du profil Entree Express @endif
                    </div>
                </td>
            </tr>
            <tr class="total-step">
                <td></td>
                <td class="align-middle">
                    <div class="text-muted">
                        Total @if($lead->lang === "en") Phase @else étape @endif 2
                    </div>
                </td>
                <td class="unit">
                    @if($currency === "MAD")
                        10900
                    @elseif ($currency === "EUR")
                        1260
                    @else
                        1230
                    @endif
                </td>
            </tr>
            <!--Etape3-->
            <!--substep1-->
            <tr>
                <td rowspan='11'>
                    @if($lead->lang === "en") Phase @else Etape @endif 3:<br>
                    <span style="color: #FF0100;">@if($lead->lang === "en") Visa application and permanent residence @else Demande du visa et residence permanente @endif</span>
                </td>
                <td>
                    <p style="color: #FF0100; font-weight: 500;">
                        @if($lead->lang === "en") Meeting @else Rencontre @endif 4 : @if($lead->lang === "en") Preparation of the Visa & PR application @else Préparation de la demande du Visa & RP @endif
                    </p>
                </td>
                <td class="unit" rowspan='4'>
                    @if($currency === "MAD")
                        5000
                    @elseif ($currency === "EUR")
                        700
                    @else
                        600
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Explanation of the procedure: Clear 13 @else Explication de la procédure : Clear 13 @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Accompaniment and supervision @else Accompagnement et encadrement @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Preparation of the visa application and permanent residence @else Préparation de la demande du visa et residence permanente @endif
                    </div>
                </td>
            </tr>
            <!--substep2-->
            <tr>
                <td>
                    <p style="color: #FF0100; font-weight: 500;">
                        @if($lead->lang === "en") Meeting @else Rencontre @endif 5 : @if($lead->lang === "en") Visa & PR request presentation @else Soumission de la demande du Visa & RP @endif
                    </p>
                </td>
                <td class="unit" rowspan='4'>
                    @if($currency === "MAD")
                        5000
                    @elseif ($currency === "EUR")
                        700
                    @else
                        600
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Document Verification: Clear 13 @else Vérification des documents : Clear 13 @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Visa and permanent residence forms signature @else Signature des formulaires de visa et residence permanente @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Visa and permanent residence application and presentation @else Soumission de la demande de visa et residence permanente @endif
                    </div>
                </td>
            </tr>
            <!--substep3-->
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Immigration application fee @else Frais de la demande d'immigration @endif
                    </div>
                </td>
                <td class="unit">
                    @if($currency === "MAD")
                        3800
                    @elseif ($currency === "EUR")
                        400
                    @else
                        400
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Medical visit fee @else Frais de la visite medicale @endif
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        1550
                    @elseif ($currency === "EUR")
                        200
                    @else
                        160
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Biometrics, visa fees and permanent residency @else Biométrie, Frais de visa et résidence permanente @endif
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        6300
                    @elseif ($currency === "EUR")
                        750
                    @else
                        640
                    @endif
                </td>
            </tr>
            <tr class="total-step">
                <td></td>
                <td class="align-middle">
                    <div class="text-muted">
                        Total @if($lead->lang === "en") Phase @else étape @endif 3
                    </div>
                </td>
                <td class="unit">
                    @if($currency === "MAD")
                        21650
                    @elseif ($currency === "EUR")
                        2750
                    @else
                        2400
                    @endif
                </td>
            </tr>
            <tr style="color: #FF0100;">
                <td></td>
                <td>
                    <p style="font-weight: bold;">
                        Total*
                    </p>
                </td>
                <td class="unit">
                    @if($currency === "MAD")
                        32750
                    @elseif ($currency === "EUR")
                        4060
                    @else
                        3680
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle no" rowspan='3'>
                    <span style="color: #FF0100;">Options</span>
                </td>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Welcome package: Welcome, integration, advice and support in Canada @else Package : Accueil, intégration, conseils et encadrement au Canada @endif
                    </div>
                </td>
                <td class="unit">
                    @if($currency === "MAD")
                    16500
                    @elseif ($currency === "EUR")
                        1450
                    @else
                        1450
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Coaching session in Canada (per hour) @else Séance de coaching par heure au Canada @endif
                    </div>
                </td>
                <td class="unit">
                    @if($currency === "MAD")
                        700
                    @elseif ($currency === "EUR")
                        70
                    @else
                        70
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Mailing fees @else Frais d'un envoi postal @endif
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        850
                    @elseif ($currency === "EUR")
                        85
                    @else
                        90
                    @endif
                </td>
            </tr>
            <tr>
                <td class="float-right" style="color: #FF0100;" colspan='3'>
                    @if($lead->lang === "en") * All amounts are exclusive of tax @else *Tous les montants mentionnés sur les devis sont Hors taxes HT @endif
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<footer style="margin: 1.6em 2.4em 0 2.4em;border-left: #F4CCCC solid 2px;padding-left: 5px;padding-top: 30px;">
    <p style="font-size: 15px;display: inline-block;">
        <strong>Agence Maroc</strong> : 42 Rue Bachir Laalej,<br />
        Casablanca, Maroc <br /><i class="fab fa-1x fa-whatsapp"></i> +212 7 02 05 09 09<br />
        <span style="font-size: 14px;">info@clearpathcanada.ca <br> www.clearpathcanada.ca</span> <br> IF : 50152557 | ICE : 002741390000066 | RC : 496031
    </p>
    <p style="font-size: 15px;display: inline-block;">
        <strong>Agence Canada</strong> : 2000 avenue McGill College
        6ème étage,<br/>Montréal Québec H3A 3H3, Canada. <br /><i class="fab fa-1x fa-whatsapp"></i> +1 438 404-0960
    </p>
</footer>

<img style="width: 180px;position:absolute;bottom:5%;right:20%;" src="{{ public_path('/img/cachett.png') }}" alt="">
</body>
</html>
