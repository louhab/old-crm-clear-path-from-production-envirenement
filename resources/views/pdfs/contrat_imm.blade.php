<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Invoice</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,500;0,700;1,200;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <style>
    @page {
        size: a4 360pt;
        /* size: a4 landscape; */
        margin:0;
        padding:0;
    }
    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100&display=swap');
    /* @font-face {
        font-family: 'Poppins-Light';
        src: url('{{ public_path('Poppins-Light.ttf') }}') format('truetype');
    }
    @font-face {
        font-family: 'Poppins-Bold';
        src: url('{{ public_path('Poppins-Bold.ttf') }}') format('truetype');
    }
    @font-face {
        font-family: 'Poppins-Thin';
        src: url('{{ public_path('Poppins-Thin.ttf') }}') format('truetype');
    } */
    body {
        /* font-family: 'Poppins-Light', Fallback, sans-serif; */
        font-family: 'Poppins', sans-serif;
        font-size: 17px;
        /* text-align: center; */
        /* border: thin solid black;   */
        /* font-family: "Poppins", sans-serif; */
        background: url("{{ public_path('/img/background_devis.png') }}");
        background-size: 350px;
        background-repeat: no-repeat;
        background-position: 100% 100%;
        /* margin-top:35px; */
        margin-top: 2cm;
        /* margin-left: 2cm; */
        margin-right: 0;
        margin-bottom: 0;
    }
    br {
        /* line-height:22px; */
    }
    /* .test {
        background-image: linear-gradient(black 33%, rgba(255,255,255,0) 0%);
        background-position: right;
        background-size: 1px 3px;
        background-repeat: repeat-y;
    } */
    /* strong{
        font-family: 'Poppins-Bold', sans-serif;
    } */
    .page-break {
        page-break-before: always;
    }
    table.devis, th, td {
       border: 1px solid red;
    }
   /** Define the header rules **/
   header {
        position: fixed;
        top: 0cm;
        left: 0cm;
        right: 0cm;
        height: 3cm;

        /** Extra personal styles **/
        /* background-color: #c6394e; */
        color: black;
        font-size: .8em;
        /* font-weight: lighter; */
        /* text-align: center; */
        line-height: 0.8cm;
        margin: 20px;
        margin-top: 30px;
        padding-bottom: 100px;
        line-height: 0.3cm;
    }

    /** Define the footer rules **/
    footer {
        position: fixed; 
        bottom: 0; 
        left: 35px; 
        right: 35px;
        height: 1cm;

        /** Extra personal styles **/
        /* background-color: #c6394e; */
        color: black;
        font-size: .7em;
        text-align: center;
        line-height: 0.4cm;
        padding:0;
        margin:0;
        border-top: 2px solid red;

    }
    .cric {
        display:inline-block;
        height:70px;
        float:right;
        margin-right: 60px;
        margin-top: 15px;
    }
    </style>
    </head>
<body>
    <header>
        <span style="display:inline;margin-bottom:50px;">
            <img class="image" style="width: 140px;" src="{{ public_path('/img/logo.png') }}" alt="">
        </span>
        <span class="cric text-muted">
            No de membre du CRIC : <strong>R705679</strong><br />
            No de dossier du client :
        </span>
    </header>

    <footer class="text-muted">
        Contrat établie selon le règlement régissant le Contrat de Service Professionnel fourni par le Conseil (V : 2016-004)
    </footer>
    <!-- <header class="m-5" style="text-align:center;">
        <img class="image" style="width: 175px;" src="{{ public_path('/img/logo.png') }}" alt="">
        <div>
            <h3 style="color:red;border: 1px dashed black;padding-bottom:10px;margin: 10px 120px;font-weight: bold;font-size:1.1em;">CONTRAT DE SERVICE PROFESSIONNEL</h3>
        </div>
        <table width="100%">
            <tr class="text-danger" style="font-weight: bold;">
                <td class="test">N° de membre du CRIC :</td>
                <td><span style="position:absolute;right:100px">N° de dossier client :</span></td>
            </tr>
        </table>
    </header> -->
    <main class="m-4">
        <div class="text-center">
            <h3 style="color:red;border: 1px dashed black;padding-bottom:10px;margin: 10px 120px;font-weight: bold;font-size:1.1em;">CONTRAT DE SERVICE PROFESSIONNEL</h3>
        </div>
        <p style="font-family: 'Poppins', sans-serif;">

            <span style="line-height:0.4cm;">
                Ce contrat de service professionnel est établi ce {{ \Carbon\Carbon::now()->format('d-m-Y') }}, entre : Monsieur <strong>Ahmed BAHAJOUB</strong>
                (le « CRIC »)<strong>, au, 1 285 Allan, Laval, Qc, H7w1h1 (Clear Path Canada Immigration Services)</strong>, et le client Monsieur 
                <strong>{{ $quote->customer->firstname }} {{ $quote->customer->lastname }}</strong>
            </span>
            <br />
            <span>
                ATTENDU QUE le CRIC et le client souhaitent conclure une entente par écrit qui contient les
                conditions convenues selon lesquelles le CRIC fournira ses services au client ;<br />
                ET ATTENDU QUE le CRIC est un membre du Conseil de réglementation des consultants en
                immigration du Canada (le « Conseil »), l’organisme de réglementation canadien des
                consultants en immigration ;<br />
                EN CONTREPARTIE des engagements réciproques de ce contrat,
                es parties conviennent de ce qui suit :
            </span>
            <br />
            <strong>1. Responsabilités et obligations du CRIC</strong><br/>
            <span style="line-height:0.4cm;">
                Le client demande au CRIC, et celui-ci accepte, de représenter le client au sujet de<br/>
                <b>Demande de résidence temporaire au Canada sous le programme d'étudiant étranger.</b><br/>
                En contrepartie des frais payés et en raison de l’affaire mentionnée<br/>
                ci-dessus, le CRIC s’engage à :<br/>
                <span>
                    <strong>A)</strong> Demande de CAQ (Le cas échéant).<br/>
                    <strong>B)</strong> Envois de la demande officielle de résidence temporaire du client.<br/>
                    <strong>C)</strong> Assurer le suivi intégral de la demande jusqu'à ce qu’une décision du gouvernement sont
                    rendue.<br/>
                    <strong>D)</strong> Préparer le client a son entrevue officielle d’immigration, le cas échéant.<br/>
                    <strong>E)</strong> Répondre à toute demande d’information du client au sujet de sa demande.<br/>
                    * L’emploi du masculin vise à alléger le texte; le genre masculin inclut le féminin.<br/>
                </span>
            </span>
            <strong>2. Responsabilités et obligations du client</strong><br/>
            <span>
                2.1 Le client doit fournir à la demande du CRIC :<br/>
                <ul>
                    <li>Toute la documentation nécessaire</li>
                    <li>Toute la documentation en anglais ou en français ou traduite en anglais ou en français</li>
                </ul>
                2.2 Le client convient qu’il doit fournir de l’information exacte et honnête et que toute
                information inexacte peut annuler ce contrat ou avoir une incidence défavorable sur la
                réponse à la demande ou le maintien de tout statut obtenu. Les obligations du CRIC en vertu
                du contrat de service professionnel sont nulles si le client fournit en toute connaissance de
                cause des renseignements importants inexacts, trompeurs ou faux. Les obligations
                financières du client demeurent.<br/>
                2.3 Si Immigration, Réfugiés et Citoyenneté Canada (IRCC) ou Emploi et Développement social
                Canada (EDSC) communique directement avec le client,
                le client doit immédiatement en aviser le CRIC.<br/>
                <div class="page-break"></div>
                <br/>
                2.4 Le client doit immédiatement informer le CRIC de tout changement de situation
                matrimoniale, de famille ou d’état civil ou de coordonnées de toute personne
                incluse dans la demande.<br/>
                2.5 Dans le cas d’un contrat de service professionnel conjoint, les clients conviennent
                que le CRIC peut échanger de l’information entre tous les clients au besoin.
                En outre, si un conflit surgit qui ne peut être résolu, le CRIC ne pourra pas continuer
                à représenter les deux clients ou tous les clients
                et devra peut-être se retirer complètement de l’affaire.
                <br/>
            </span>
            <strong>3. Méthode de facturation</strong><br/>
            Le client sera facturé une fois le service rendu.<br/>
            Voici en détail la méthode de facturation utilisée :<br/>
            <table class="devis ml-4">
                <thead>
                <tr>
                    <th>Etape</th>
                    <th class="text-center">Description</th>
                    @if($quote->customer->program_id == 4)
                        <th>Montant</th>
                    @else
                        <th>Célibataire</th>
                        <th>Couple</th>
                    @endif
                    <!-- <th>Quantité</th> -->
                    <!-- <th>Total</th> -->
                </tr>
                </thead>
                <tbody>
                @php 
                    $groupedResultsArray = []; 
                    $ttc = 0;
                    $ttc_couple = 0;
                @endphp
                @foreach($quote->services->sortBy('phase_id') as $service)
                    @php
                        $groupedResultsArray[$service->phase->name][] = $service;
                    @endphp
                @endforeach
                @foreach($groupedResultsArray as $phase)
                    @php 
                        $qte_sum = 0;
                        $qte_sum_couple = 0;
                    @endphp
                    @if($loop->last)
                        <tr class="text-center">
                            @if($quote->customer->program_id == 4)
                                <td colspan="2" style='border:none;'>Total TTC</td>
                                <td style='border:none;'>{{ $ttc }}</td>
                            @else
                                <td colspan="2">Total TTC</td>
                                <td>{{ $ttc }}</td>
                                <td>{{ $ttc_couple }}</td>
                            @endif
                        </tr>
                    @endif
                    @foreach($phase as $service)
                    <tr>
                        @if($loop->first) <td class="text-justify" rowspan='{{ count($phase) }}'>{{ $phase[0]->phase->name }} </td>@endif
                        <td>
                            <div class="text-muted text-center">
                                @if(count($service->details))
                                    @foreach($service->details as $detail)
                                        {{ $detail->name }} <br>
                                    @endforeach
                                @else
                                    {{ $service->name }}
                                @endif
                            </div>
                        </td>
                        @if($quote->customer->program_id == 4)
                            <td class="unit">{{ $service->pivot->billed_price }}</td>
                        @else
                            <td class="unit">{{ $service->single_price }}</td>
                            <td class="unit">{{ $service->couple_price }}</td>
                        @endif
                        <!-- <td class="qty">{{ $service->pivot->qte }}</td> -->
                        @php 
                            $qte_sum += $service->single_price; 
                            $qte_sum_couple += $service->couple_price; 
                        @endphp
                        <!-- <td class="total">{{ $service->pivot->billed_price * $service->pivot->qte }}</td> -->
                    </tr>
                    @endforeach
                    @php
                        $ttc += $qte_sum;
                        $ttc_couple += $qte_sum_couple;
                    @endphp
                    @if($loop->last)
                        @break
                    @endif
                    <tr class="text-center" style="background:red;color:white;">
                    @if($quote->customer->program_id == 4)
                        <td style="text-align:left;" colspan="2">Total {{ $phase[0]->phase->name }}</td>
                        <td>{{ $qte_sum }}</td>
                    @else
                        <td style="text-align:left;" colspan="2">Total {{ $phase[0]->phase->name }}</td>
                        <td>{{ $qte_sum }}</td>
                        <td>{{ $qte_sum_couple }}</td>
                    @endif
                    </tr>
                @endforeach
                </tbody>
            
            
            </table>
            <strong>4. Échéancier des paiements</strong><br/>
            Le paiement sera exigé à la fin de chaque service rendu.<br/>
            <strong>5. Politique de remboursement</strong><br/>
                Le client reconnaît que l’obtention d’un visa ou d’un statut ainsi que le temps de traitement de
                cette demande est à l’entière discrétion<br/>
                du gouvernement et non du CRIC. En outre, le client reconnaît que les honoraires ne sont pas
                remboursables s’il
                advenait que la demande soit refusée.<br/>
                Cependant, si le CRIC ou son personnel ne remplissent pas les tâches mentionnées
                à la section 2 du présent contrat, le CRIC remboursera les
                honoraires perçus, en partie ou en totalité. 
                <div class="page-break"></div>
                <br/>
                Le client consent à ce que les honoraires soient
                payés pour les services mentionnés ci-dessus et que
                tout remboursement s’applique uniquement au montant des honoraires payés. Les frais non
                utilisés seront remboursés comme suit : 
                <ul><li>Méthode de paiement initial dans un délai maximum de 45 jours.</li></ul>
                Si le client a lieu de croire que le CRIC est décédé, frappé d’incapacité, etc., le client devra
                communiquer avec le CRCIC.
            <br/>
            <strong>6. Résolution de conflits relatifs au Code d’éthique professionnelle</strong><br/>
            Dans le cas d’un conflit relatif au Code d’éthique professionnelle, le client et le CRIC doivent
            s’efforcer autant que
            possible de résoudre le problème entre les deux parties. Si le conflit ne peut pas être réglé, le
            client doit présenter une plainte par écrit au CRIC et donner au CRIC 45 jours pour répondre au
            client. Si le conflit n’est toujours pas réglé, le client peut suivre le processus de plainte et de
            discipline décrit sur le site Web du Conseil dans la section « Déposer une plainte ». REMARQUE :
            tous les formulaires de plainte doivent être signés.<br/>
            <strong>7. Confidentialité</strong><br/>
            Tous les renseignements et les documents examinés par le CRIC, requis par IRCC et tous les
            autres corps dirigeants, et
            utilisés pour préparer la demande ne seront pas divulgués à un tiers, autre qu’à des agents et
            des employés, sans consentement préalable, sauf si requis par la loi. Le CRIC, et tous ses
            agents et employés, sont aussi liés par les exigences de
            confidentialité de l’article 8 du Code d’éthique professionnelle. Le client consent à ce que les
            communications aient lieu par voie électronique et au stockage de l’information confidentielle.
            Le CRIC s’efforcera
            autant que possible d’assurer un niveau de sécurité élevé en matière de communication
            électronique et de stockage de l’information.<br/>
            <strong>8. Force majeure</strong><br/>
            En cas d’incapacité du CRIC à respecter toute clause de ce contrat de service professionnel,
            en raison de conditions hors de son
            contrôle telles que, sans en exclure d’autres, des restrictions gouvernementales ou des lois
            postérieures, une guerre, des grèves ou des catastrophes
            naturelles, on ne considérera pas qu’il y a rupture de ce contrat.<br/>
            <div class="text-muted" style="font-size:0.7em;display:block;position:absolute;top:80%;left:25px;width:350px;">
                    <div style="border:3px solid grey;"></div>
                    <span>
                        1. Conseil de réglementation des consultants en immigration du Canada (CRCIC)<br/>
                        5500 North Service Rd., bureau 1002<br/>
                        Burlington (Ontario) L7L 6W6<br/>
                        Sans frais : 1-877-836-754<br/>
                    </span>
                    <span>2. Tous les formulaires de plainte doivent être signés.</span><br/>
                    <span>3. http://iccrc-crcic.info/fr/</span><br/>
                    <span>4. http://fr.iccrc-crcic.ca/admin/contentEngine/contentImages/<br/>
                    file/Complaint_Form_against_an_RCIC__modified26March12FR.pdf</span>
            </div>
            <div class="page-break"></div>
            <br />
            <strong>9. Politique de changement</strong><br/>
            Le client convient qu’en raison d’une demande faite au CRIC de le représenter dans une affaire
            autre que celle mentionnée ci- dessus dans ce contrat, d’un changement de sa situation
            matérielle, de faits matériels non divulgués au début de la demande ou d’un changement
            dans la loi<br/>
            du gouvernement concernant le traitement des demandes en matière d’immigration
            ou de citoyenneté, le contrat peut être modifié en conséquence.<br/>
            <strong>10. Fin du contrat</strong>
            <ul style="list-style:none;padding: 0;list-style-type: none;">
                <li>
                    <b>10.1</b> Ce contrat prend fin lorsque les tâches précisées à la section 2 de ce contrat
                ont été exécutées.
                </li>
                <li>
                    <b>10.2</b> Ce contrat prend fin si des changements d’ordre matériel se produisent dans la demande
                    ou l’admissibilité du client, et qui empêchent
                    d’exécuter les tâches décrites à la section 2 de ce contrat.
                </li>
            </ul>
            <strong>11. Libération des obligations ou retrait d’une affaire</strong>
            <ul style="list-style:none;padding: 0;list-style-type: none;">
                <li>
                    <b>11.1</b> Le client peut libérer le CRIC de ses obligations et mettre fin au contrat par écrit.<br/>
                    Les honoraires ou débours
                    impayés seront payés par le CRIC au client / les honoraires ou débours impayés seront payés
                    par le client au CRIC.
                </li>
                <li>
                    <b>11.2</b> En vertu de l’article 11 du Code d’éthique professionnelle, le CRIC peut se retirer d’une affaire
                    et mettre fin au contrat par écrit pourvu que cela ne cause pas de préjudice au client
                    Les honoraires ou débours impayés seront
                    payés par le CRIC au client / les honoraires ou débours impayés seront payés par le client
                    au CRIC.
                </li>
            </ul>
            <strong>12. Loi du contrat</strong><br/>
            Ce contrat est régi par les lois en vigueur de la Province/du Territoire Québec , et les lois
            fédérales canadiennes qui s’y appliquent et, sauf pour des conflits prévus à la section 8 aux
            présentes, tout conflit concernant les clauses de<br/>
            ce contrat sera réglé par un tribunal compétent de la Province/du Territoire Québec.
            <strong>13. Divers</strong>
            <ul style="list-style:none;padding: 0;list-style-type: none;">
                <li>
                    <b>13.1</b> Le client autorise explicitement le CRIC à agir en son nom dans les limites des fonctions
                    précises pour lesquelles
                    les services du CRIC ont été retenus, conformément au paragraphe 2 des présentes.
                </li>
                <div class="page-break"></div>
                <br/>
                <li>
                    <b>13.2</b> Ce contrat constitue la totalité du contrat entre les parties pour ce qui est de l’objet
                    ci-contre et remplace tous les contrats, tous les
                    accords, toutes les garanties, représentations, négociations et discussions précédents,
                    verbaux ou écrits, des parties sauf pour ce qui est
                    indiqué précisément aux présentes.
                </li>
                <li>
                    <b>13.3</b> Ce contrat lie les parties aux présentes et leurs héritiers, administrateurs, successeurs
                    et ayants droit autorisés respectifs.
                </li>
                <li>
                    <b>13.4</b> Ce contrat peut uniquement être modifié si les changements sont effectués par écrit
                    et signés par les parties aux présentes.
                </li>
                <li>
                    <b>13.5</b> Les dispositions de ce contrat sont jugées dissociables. Si une disposition de ce contrat
                    est considérée comme inexécutable par un
                    tribunal compétent, la disposition sera supprimée de ce contrat, et les autres dispositions
                    demeureront en vigueur.
                </li>
                <li>
                    <b>13.6</b> Les titres utilisés dans ce contrat visent seulement à faciliter la lecture et ne doivent en
                    aucun cas être interprétés comme des ajouts ou des limitations des engagements et ententes
                    contenus dans ce contrat.
                </li>
                <li>
                    <b>13.7</b> Chaque partie aux présentes fait, établit, donne, signe ou fait faire, établir, donner
                    ou signer toutes les autres choses, les assurances et
                    tous les actes, actes de transfert et documents jugés nécessaires ou raisonnablement requis
                    pour que l’intention et l’objet de ce contrat
                    soient pleinement et effectivement appliqués.
                </li>
                <li>
                    <b>13.8</b> Le client convient qu’il a eu suffisamment de temps pour examiner ce contrat et qu’on lui a
                    donné la possibilité d’obtenir un avis
                    juridique indépendant et une traduction avant la signature et la remise de ce contrat. Si le
                    client n’a pas demandé un avis juridique
                    indépendant avant la signature et la remise de ce contrat, il l’a fait de son propre chef sans
                    subir de pression indue et convient que le fait de ne pas avoir obtenu d’avis juridique
                    indépendant ne pourra pas être utilisé comme moyen de
                    défense pour faire respecter les obligations créées dans ce contrat.
                </li>
                <li>
                    <b>13.9</b> En outre, le client reconnaît avoir reçu une copie de ce contrat et convient être lié par
                    ses conditions.
                </li>
                <li>
                    <b>13.10</b> Le client qui souhaite remplir ce contrat en anglais doit en faire la demande.
                </li>
            </ul>
            <div class="page-break"></div>
                <br />
            <strong>14. Coordonnées</strong><br/>
            <ul style="list-style:none;padding: 0;list-style-type: none;">
                <li>
                    <b>Client 1:</b><br>
                    <strong>{{ $quote->customer->firstname }}</strong> <strong>{{ $quote->customer->lastname }}</strong><br />
                    Adresse <strong>{{ \App\Models\Lead::where("id", $quote->customer->lead_id)->first()->adresse_line_1 }}</strong><br/>
                    Numéro de téléphone : <strong>+{{ $quote->customer->phone }}</strong> Adresse courriel : <strong>{{ $quote->customer->email }}</strong>
                </li>
                <li>
                    <b>Client 2:</b><br>
                    <strong></strong> <strong></strong><br />
                    Adresse <strong></strong><br/>
                    Numéro de téléphone : <strong></strong> Adresse courriel : <strong></strong>
                </li>
                <li>
                    <b>CRIC :</b><br>
                    <strong>Ahmed</strong> <strong>Bahajoub</strong>
                    Adresse <strong>1285 Allan Laval, Quebec, Canada </strong>
                    Numéro de téléphone : <strong>+15148153614</strong> Adresse courriel : <strong>clearpathcanada@gmail.com</strong><br/>
                    <span>
                        EN FOI DE QUOI ce contrat a été dûment signé par les parties aux présentes
                        à la date indiquée au début du présent contrat
                    </span>
                </li>
            </ul>
            <!-- Prénom <strong>{{ $quote->customer->firstname }}</strong> Nom <strong>{{ $quote->customer->lastname }}</strong> Adresse <strong>{{ \App\Models\Lead::where("id", $quote->customer->lead_id)->first()->adresse_line_1 }}</strong><br/>
            <strong>Numéro de téléphone :</strong> +{{ $quote->customer->phone }} <strong>Adresse courriel :</strong> {{ $quote->customer->email }}
            EN FOI DE QUOI ce contrat a été dûment signé par les parties aux présentes
            à la date indiquée au début du présent contrat. -->
        </p>
    </main>
    <img style="width: 170px;position:absolute;top:65%;right:10%;" src="{{ public_path('/img/signature.png') }}" alt="">
    <img style="width: 200px;position:absolute;top:50%;right:25%;" src="{{ public_path('/img/cachett.png') }}" alt="">
    <span style="margin: 10px 50px;">
        <div style="display:inline;position:absolute;left:50px;"><b>Signature du client</b></div>
        <div style="display:inline;position:absolute;right:50px;"><b>Signature du CRIC</b></div>
    </span>
    <!-- <div class="page-break"></div> -->
</body>
</html>