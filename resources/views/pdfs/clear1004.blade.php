<?php
    $quote = \App\Models\Quote::where('customer_id', $customer->id)->first();
    $currency = $customer->currency_id ? \App\Models\Currency::where("id", $customer->currency_id)->first()->iso : "MAD";
    $lead = \App\Models\Lead::where("id", $customer->lead_id)->first();
    App::setLocale($lead->lang);
?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Invoice</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,500;0,700;1,200;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <style>
    @page {
        size: a4 360pt;
        /* size: a4 landscape; */
        margin:0;
        padding:0;
    }
    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100&display=swap');
    body {
        /* font-family: 'Poppins-Light', Fallback, sans-serif; */
        font-family: 'Poppins', sans-serif;
        font-size: 17px;
        /* text-align: center; */
        /* border: thin solid black;   */
        /* font-family: "Poppins", sans-serif; */
        background: url("{{ public_path('/img/background_devis.png') }}");
        background-size: 350px;
        background-repeat: no-repeat;
        background-position: 100% 100%;
        margin-top:1.3cm;
    }
    .head-info {
        list-style:none;
        padding:0;
        margin:0;
    }
    .head-info li{
        background: #c0c9d2;
        color: black;
        margin: 5px 0;
    }
    .head-r{
        display:inline;
        font-size:0.6em;
        width:30%;
        position:absolute;
        right:20px;
        top:10px;
    }
    .head-l {
        position:absolute;
        top:40px;
        left:20px;
    }
    .page-break {
        page-break-before: always;
    }
    .table-fields {
        border-collapse: separate;
        border-spacing:10px;
        font-size: 0.75em;
    }
    .table-fields td {
        border: 1px dashed black;
        padding: 5px 8px;
    }
    .table-fields tr td:first-child{
        width: 50%;
        border-color: red;
        line-height: 0.7em;
    }
    .table-fields tr td:first-child p{
        margin:0;
    }
    .factor-header {
        display: block;
        margin: 20px;
        /* border:2px solid black; */
    }
    .factor-header .b{
        text-align:center;
        border:2px dashed red;
        /* width: 100%; */
        padding-bottom: 5px;
        font-weight: bold;
        /* font-size: 1.2em; */
    }
    .factor-header p{
        margin-top: 10px;
        font-size: .95em;
        line-height: .8em;
    }
    </style>
    </head>
<body>
    <header style="position:relative;">
        <span class="head-l">
            <img class="image" style="width: 140px;" src="{{ public_path('/img/logo.png') }}" alt="">
        </span>
        <div class="text-muted head-r">
            <b>@if($lead->lang === "en") Customer Interview Report @else Rapport d’Entretien Client @endif</b>
            <p>
                @if($lead->lang === "en") Creation date @else Date de création @endif : 07/09/2021<br>
                {{ $customer->firstname }} {{ $customer->lastname }}<br>
                @php $conseiller = \App\Models\User::where('id', $customer->lead->conseiller_id)->first(); @endphp
                {{ $conseiller->name }}
            </p>
            <ul class="head-info">
                <li>
                    @if($lead->lang === "en") Customer dossier number @else Numero de dossier client @endif : {{ $customer->phone }}
                </li>
                <li>
                    @if($lead->lang === "en") Email @else Adresse mail @endif : {{ $customer->email }}
                </li>
                <li>
                    @if($lead->lang === "en") Telephone number @else Téléphone mobile @endif : {{ $customer->phone }}
                </li>
            </ul>
        </div>
    </header>
    <!-- <div style="margin:0 40px;">
        <div style="display:inline;">
            <img class="image" style="width: 185px;" src="{{ public_path('/img/logo.png') }}" alt="">
        </div>
        <div style="display:inline;float:right;margin-top:30px">
            <b style="font-size:1.1em;line-height:0.5em;">Clear 1001 - Conseil</b>
            <p class="text-muted" style="font-size:0.92em;line-height:0.7em;">
                Date de création: 07/09/2021
            </p>
        </div>
    </div> -->
    <div style="position:relative;height:4cm;"></div>

    <ul style='list-style:none;padding:0;margin:0 20px;'><li>
            <table style="color:red;width: 100%;border-collapse: separate;border-spacing:10px;">
                <tr>
                    <th style="width: 50%;padding: 0 8px;">@if($lead->lang === "en") Questions @else QUESTIONS @endif</th>
                    <th style='padding: 0 8px;'>@if($lead->lang === "en") Remarks @else REMARQUES @endif</th>
                </tr>
            </table>
        </li>
    </ul>
    @php
        $customer_clear = \App\Models\CustomerClear1004Fields::where('customer_id', $customer->id)->first();
        $skip = array('id', 'customer_id', 'currency_id', 'updated_at', 'created_at');
        $columns = array_diff(Illuminate\Support\Facades\Schema::getColumnListing('customer_clear1004_fields'), $skip);
        // dd($columns);
    @endphp
    <ul style='list-style:none;padding:0;margin:0 20px;'><li>
            <table class="table-fields" style="width: 100%;">
                @foreach($columns as $col)
                <tr>
                    <td>
                            @php
                                $customer_field = \App\Models\CustomerField::where('field_name', $col)->first();
                                $eng_fields = [
                                    "marital_status" => "status_label_en",
                                    "spouse_cit" => "value_en",
                                    "spouse_joining" => "value_en",
                                    "age" => "labelEng",
                                    "education_level" => "labelEng",
                                    "diplomas_canadien" => "value_en",
                                    "education_type" => "value_en",
                                    "lang_test_expire" => "value_en",
                                    "lang_test" => "value_en",
                                    "other_lang_test" => "value_en",
                                    "work_experience_canada" => "labelEng",
                                    // "work_experience_abroad" => "labelEng",
                                    "certificate_of_competence" => "value_en",
                                    "valid_job" => "value_en",
                                    "job_kind" => "value_en",
                                    "certificate_of_designation" => "value_en",
                                    "sibling_canada" => "value_en",
                                    "spouse_education_level" => "labelEng",
                                    "spouse_work_experience_canada" => "labelEng",
                                    "spouse_lang_test" => "value_en",
                                ];

                                if ($lead->lang === "en" && in_array($col, array_keys($eng_fields))) {
                                    $label_ = $eng_fields[$col];
                                } else {
                                    $label_ = $customer_field->select_model_label;
                                }
                                // dd($col, $lead->lang, $label_);
                                $id_ = $customer_field->select_model_id;
                                $model = $customer_field->select_model::where($id_, $customer_clear->$col)->pluck($label_, $id_)->toArray();
                            @endphp
                        <p>
                            {{ __('lang.' . $col) }}<br/>
                            @if($customer_field->select_model == "\App\Models\LanguageTestScore")
                                <b>@if($customer_clear->$col) {{ $customer_clear->$col }} @else -- @endif</b>
                            @else
                                <b>@if($customer_clear->$col) {{ $model[$customer_clear->$col] }} @else -- @endif</b>
                            @endif
                        </p>
                    </td>
                    <td>
                        @php
                            $field = \App\Models\CustomerField::where('field_name', $col)->first();
                            $conseill = \App\Models\CustomerFieldRemark::where('customer_id', $customer->id)->where('field_id', $field->id)->first();
                        @endphp
                        {{ $conseill->value }}
                    </td>
                </tr>
                @endforeach
                {{-- <? dd($total_array);?> --}}
            </table>
        </li>
    </ul>
    @if($scoring)
    <div class="page-break"></div>
    <br/>
    <div style="margin: 10px;">
        <?php
            $factors = json_decode($scoring, true);
            $total = 0;
        ?>
        @foreach($factors as $factor)
            <div class="factor-header">
                <div class="b">{{ $factor["title"] }}</div>
                <p>
                    @if(array_key_exists('groups', $factor["factors"]))
                        @foreach($factor["factors"]["groups"] as $ff)
                            <b>{{ $ff["title"] }}</b><br />
                            @php
                                $cc = array_combine($ff["factors"]["keys"], $ff["factors"]["values"]);
                            @endphp
                            @foreach($cc as $key => $value)
                                {{ $key }} = <b>{{ $value }}</b><br />
                            @endforeach
                            <b style="color:red;">Sous-total = {{ $ff["total"] }}</b><br />
                        @endforeach
                    @endif
                    @php
                        $cc = array_combine($factor["factors"]["keys"], $factor["factors"]["values"]);
                        $total += intval($factor["total"]);
                    @endphp
                    @foreach($cc as $key => $value)
                        {{ $key }} = <b>{{ $value }}</b><br />
                    @endforeach
                        <b style="color:red;">@if($lead->lang === "en") Subtotal @else Sous-total @endif - {{ $factor["title"] }} = {{ $factor["total"] }}</b>
                    @if($loop->last)
                        <br /><b>@if($lead->lang === "en") Ranking system formula final sum @else Formule du système de classement somme finale @endif = {{ $total }}</b>
                    @endif
                <p>
            </div>
        @endforeach
    </div>
    @endif
    <!-- <ul style='list-style:none;padding:0;margin:0 20px;'><li>
            <table style="width: 100%;">
                <tr style="border: 1px dashed red;">
                    <th style='padding: 0 8px;'></th>
                </tr>
            </table>
        </li>
    </ul> -->
    <img style="width: 180px;position:absolute;top:75%;right:20%;" src="{{ public_path('/img/cachett.png') }}" alt="">
    <!-- <div class="page-break"></div> -->
</body>
</html>
