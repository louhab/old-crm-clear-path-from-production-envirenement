<?php
$lead = \App\Models\Lead::where('id', $customer->lead_id)->first();
// dd($lead);
?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Invoice</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,500;0,700;1,200;1,300;1,400;1,700;1,900&display=swap"
        rel="stylesheet">
    <style>
        @page {
            size: a4 landscape;
            margin: 0;
            padding: 0;
            /* margin-top: 20px; */
        }

        body {
            font-family: "Times New Roman";
            font-size: 30px;
            /* text-align: center; */
            /* border: thin solid black;   */
            font-family: "Poppins", sans-serif;
            background: url("{{ public_path('/img/background_devis.png') }}");
            background-size: 450px;
            background-repeat: no-repeat;
            background-position: 100% 70%;
        }

        table tr:last-child td {
            margin-left: 20px;
        }

        .footer {
            position: absolute;
            bottom: 5%;
            width: 100%;
            padding: 5px 50px;
        }

        .footer p {
            color: #000;
            font-size: .30em;
            display: inline-block;
        }

        .container {
            max-width: 1674px;
            /* adjust to allow for padding as needed */
            padding: 0 50px;
        }

        #ftr-wrap {
            display: table;
            table-layout: fixed;
            width: 100%;
            margin: 0 auto;
        }

        #ftr-wrap>div {
            display: table-cell;
            vertical-align: middle;
            /*outline:1px dashed red;   TEST Outline.  TO BE DELETED. */
            padding: auto 10px;
        }

        span.divider {
            /* display: block; */
            width: 0;
            height: 70px;
            padding-top: 15px;
            border-left: 1px solid blue;
            border-right: 1px solid red;
        }

        .amazon {
            box-sizing: border-box;
            height: 50px;
            width: fit-content;
            border-right: 3px solid red;
        }


        /* #ftr-wrap > div:nth-child(1) {text-align:left;}
        #ftr-wrap > div:nth-child(2) {text-align:center;}
        #ftr-wrap > div:nth-child(3) {text-align:center;}
        #ftr-wrap > div:nth-child(4) {text-align:right;} */

        .ftr-links ul {
            padding: 0;
        }

        .ftr-links ul li {
            /* display: inline-block; */
            padding-right: 15px;
            font-size: .30em;
        }

        .ftr-links ul li a {
            color: #000;
            margin: 0;
        }
    </style>
</head>

<body>
    <header class="m-5" style="margin-bottom: 10px !important;">
        <div class="row d-flex justify-content-center">
            <img class="image" style="width: 175px;" src="{{ public_path('/img/Logo-CPSA.png') }}" alt="">
        </div>
    </header>
    <div class="h3 text-white" style="background: red;">
        <p class="w-100 text-center ">
            @if ($lead->lang === 'en')
                Customer Receipt
            @else
                Reçu Client
            @endif
        </p>
        <span>
            <p style="position: absolute;right: 25px;top:152px;">Date :
                {{ \Carbon\Carbon::parse($customer->updated_at)->format('d/m/Y') }}</p>
        </span>
    </div>
    <div>
        <table class="m-5">
            <tr>
                <td class="font-weight-light">
                    @if ($lead->lang === 'en')
                        Surname
                    @else
                        Nom
                        @endif / @if ($lead->lang === 'en')
                            Name
                        @else
                            Prénom
                        @endif :
                </td>
                <td class="h3 mt-2">{{ $customer->lastname . '  ' . $customer->firstname }}</td>
            </tr>
            <tr>
                <td class="font-weight-light">
                    @if ($lead->lang === 'en')
                        Payement ref.
                    @else
                        Ref du Paiement
                    @endif :
                </td>
                <td class="h3 mt-2">
                    <?php
                    $states = [3, 5, 10, 20, 29, 30];

                    $steps = [2, 3, 4, 5, 6, 7];
                    $stepIndex = array_search($step_state->id, $states);
                    if ($stepIndex !== false) {
                        $step = \App\Models\ProgramStep::find($steps[$stepIndex]);
                        // if ($step_state->id === 30) {
                        //     dd('asxasx', $step);
                        // }
                        if ($customer->program_id != 4) {
                            $pt = 'Imm';
                            $payment = $step
                                ->payments()
                                ->withPivot('program_id', 'amount', 'couple_amount')
                                ->where('program_id', 1)
                                ->where('currency_id', $customer->currency_id)
                                ->first();
                        } else {
                            $pt = 'Etud';
                            $payment = $step
                                ->payments()
                                ->withPivot('program_id', 'amount')
                                ->where('program_id', 4)
                                ->where('currency_id', $customer->currency_id)
                                ->first();
                        }
                        $amount_ = $payment->pivot->amount;

                        if ($customer->program_id != 4) {
                            $clear1004 = \App\Models\CustomerClear1004Fields::where('customer_id', $customer->id)->first();
                            if ($clear1004 && $clear1004->spouse_joining == 'spouse_joining-B') {
                                $amount_ = $payment->pivot->couple_amount;
                            }
                        }
                        $currency = \App\Models\Currency::find($customer->currency_id)->iso;
                        $som = 0;
                        foreach (
                            $customer
                                ->options()
                                ->where('program_payment_id', $payment->id)
                                ->get()
                            as $option
                        ) {
                            $som += intval($option->getTranslation('single_price', $currency));
                        }

                        $__pyament = DB::table('program_payment_customer as pc')
                            ->select('pc.discount_percent')
                            ->where([
                                'pc.program_payment_id' => $payment->id,
                                'pc.customer_id' => $customer->id,
                            ])
                            ->get();

                        // dd('this is payment .. ', $__pyament);

                        $discount_ = 0;

                        if (count($__pyament)) {
                            // dd($__pyament);
                            $discount_ = $__pyament[0]->discount_percent;
                        }
                        // payments()
                        $amount_ = $amount_ + $som;

                        // New Price = Original Price - (Original Price x Discount Percentage)
                        $amount_ = $amount_ - ($amount_ * $discount_) / 100;
                        // dd($amount_, $som, $customer->options()->get());
                    }
                    ?>
                    {{ $payment->name_fr }}-{{ $pt }}
                </td>
            </tr>
            <tr>
                <td class="font-weight-light">
                    @if ($lead->lang === 'en')
                        Amount
                    @else
                        Montant
                    @endif :
                </td>
                @php
                    $currency = \App\Models\Currency::find($customer->currency_id)->iso;
                    $query = DB::table('program_payment_customer')
                        ->where('program_payment_id', '=', $payment->id)
                        ->where('payment_method', '=', 2)
                        ->count();

                    $ttc_include = $customer
                        ->payments()
                        ->wherePivot('program_payment_id', $payment->id)
                        ->wherePivotIn('payment_method', [2])
                        ->count();
                @endphp
                <td class="h3">{{ $amount_ . ' ' . $currency }}</td>
                {{-- <td class="h3">{{ $discount_ . ' %' }}</td> --}}
                {{-- <td class="h3">{{ $payment->id . '  paiement' }}</td> --}}
            </tr>
            @if (true)
                <tr>
                    <td class="font-weight-light">
                        @if ($lead->lang === 'en')
                            TVA Amount
                        @else
                            Montant TVA
                        @endif :
                    </td>
                    @php
                        $amount_tva = intval($amount_) * 0.2;
                    @endphp
                    <td class="h3">{{ $amount_tva . ' ' . $currency }}</td>
                </tr>
                <tr>
                    <td class="font-weight-light">
                        @if ($lead->lang === 'en')
                            TTC Amount
                        @else
                            Montant TTC
                        @endif :
                    </td>
                    @php
                        $amount_ttc = $amount_tva + $amount_;
                    @endphp
                    <td class="h3">{{ $amount_ttc . ' ' . $currency }}</td>
                </tr>
            @endif
        </table>
        {{-- FiXME --}}
        <img style="width: 400px;position:absolute;top:25%;right:10px;" src="{{ public_path('/img/cachett.png') }}"
            alt="">
    </div>
    <!-- <div>
      <div class="row d-flex justify-content-center">
        <div class="col-3">Test1</div>
        <div class="col-3">Test2</div>
        <div class="col-3">Test3</div>
        <div class="col-3">Test4</div>
      </div>
    </div> -->
    <footer class="footer">
        <div class="container">
            <div id="ftr-wrap">
                {{-- <div>
                    <p class="amazon">
                        <strong>Agence Canada:</strong> 2000 avenue McGill College 6éme étage.
                        Montréal, Québec,H3A 3H3,Canada.
                        <strong>+1 438 404-0960</strong>
                    </p>
                </div> --}}
                <div>
                    <p class="amazon">
                        <strong>Agence Maroc:</strong> 42 rue Bachir Laalej,
                        Casablanca, Maroc.<br />
                        <strong>+212 7 02 05 09 09</strong>
                    </p>
                    {{-- <span class="divider"></span> --}}
                </div>
                <div>
                    <p class="amazon">
                        <i class="fab fa-2x fa-facebook-f"></i>/Clearpathcanada
                        <strong>info@clearpathcanada.ca<br />
                            www.clearpathcanada.ca</strong>
                    </p>
                    {{-- <span class="divider"></span> --}}
                </div>
                <div class="ftr-links">
                    <ul>
                        <li><a href="#">IF : 50152557</a></li>
                        <li><a href="#">ICE : 002741390000066</a></li>
                        <li><a href="#">RC : 496031</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</body>

</html>
