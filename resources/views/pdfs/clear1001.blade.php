<?php
    $currency = $customer->currency_id ? \App\Models\Currency::where("id", $customer->currency_id)->first()->iso : "MAD";
    $lead = \App\Models\Lead::where("id", $customer->lead_id)->first();
    App::setLocale($lead->lang);
    $lang = $lead->lang;
?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Invoice</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="{{ public_path('/css/bootstrap.min.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,500;0,700;1,200;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <style>

        @page { margin: 0in;size: 700pt 1020pt; }
        body {
            font-family: 'Poppins', sans-serif;
            font-size: 14px;
            background-size: 350px;
            background-repeat: no-repeat;
            background-position: 100% 100%;
            margin:20px;
            /* margin-bottom:50px; */
        }
        .page-break {
            page-break-before: always;
        }
        .head-info {
            list-style:none;
            padding:0;
            margin:0;
        }
        p{
            margin: 0;
        }
        .head-info li{
            background: #c0c9d2;
            color: black;
            margin: 5px 0;
        }
        .head-r{
            display:inline;
            font-size:0.6em;
            width:30%;
            position:absolute;
            right:20px;
            top:10px;
        }
        .head-l {
            display: flex;
            justify-content: center;
        }
        .table-fields {
            border-collapse: separate;
            border-spacing:10px;
            font-size: 0.75em;
        }
        .table-fields td {
            border: 1px dashed black;
            padding: 5px 8px;
        }
        .table-fields tr td:first-child{
            width: 40%;
            border-color: red;
            line-height: 0.7em;
        }
        .table-fields tr td:first-child p{
            margin:0;
        }
        .sectionContainer{
            border: 1px solid #ff2929;
            border-radius: 7px;
            width: 97%;
            margin: 0;
            padding: 5px 12px 5px 12px;
        }
        .sectionContainer  p{
            font-family: 'Poppins', serif;
            font-weight: 300;
            line-height: 0.9em;

        }

        .bold{
            font-family: 'Poppins', serif;
            font-weight: 900;
        }
        .title{
            font-family: 'Poppins', serif;
            color: #ff2929;
            font-size: 17px;
            font-weight: bold;
            /*text-transform: capitalize;*/
            /*border: black dashed 2px;*/
            margin: 0;
            text-align: center;
        }

        .section_Container{
            border: 1px solid #ff2929;
            border-radius: 7px;
            width: 97%;
            margin: 0;
            padding: 5px 12px 5px 12px;
        }
        .text-normal {
            font-size: 12px;
            font-weight: 300;
            line-height: 0.7em;
        }
        .section{
            width: 95%;
            margin: auto;
            padding: 12px;
        }
        .big-title {
            font-family: 'Poppins', serif !important;
            font-size: 28px;
            font-weight: bold;
            text-align: center;
            color: #0b0b0b;
        }
    </style>
</head>
<body>
<div class="sectionContainer" style="background-color: #fff8f8;margin-top: 5px;">
<div style="width: 100%;text-align: center;">
    <img class="center" style="width: 190px;" src="{{ public_path('/img/logo-fr.png') }}" alt="">
</div>
</div>
<div class="sectionContainer" style="background-color: #ff2929;margin-top: 5px;">
    <h1 class="big-title" style="margin-top: 40px;margin-bottom: 20px;line-height: 0;color: white">Clear 1001 : {{ __('lang.clear1001_title')}} </h1>
</div>
<br>
@php
    $conseiller = \App\Models\User::where('id', $customer->lead->conseiller_id)->first();
    $groupedResultsArray = [];
    $form = \App\Models\InformationSheet::where('name_fr', 'Clear1000 - Questionnaire')->first();
    $customer_case = \App\Models\CustomerCase::where('customer_id', $customer->id)->first();
    $program = \App\Models\Program::where('id' , $customer_case->program_id)->first();

    $customer_ = App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first();

    $level = App\Models\LevelOfStudy::where('id', $customer_->level_study)->first();
    $branch = App\Models\Branch::where('id', $customer_->branch)->first();
    $graduation_year = \App\Models\GraduationYear::find($customer_->graduation_year);
    $diploma_fr = App\Models\FrenchDiploma::where('id', $customer_->diploma_fr)->first();
    $diploma_En = App\Models\EnglishDiploma::where('id', $customer_->diploma_en)->first();
    $longuge_  = App\Models\CustomerRadioField::where('id', $customer_->language)->first();
    $program1  = App\Models\ProgramChoice::where('id', $customer_->program_first)->first();
    $program2  = App\Models\ProgramChoice::where('id', $customer_->program_second)->first();
    $city = App\Models\CanadaCity::where('id', $customer_->city)->first();
    $health  = App\Models\CustomerRadioField::where('id', $customer_->health)->first();
    $diplomas = App\Models\CustomerRadioField::where('id', $customer_->diplomas)->first();
    $criminel_record  = App\Models\CustomerRadioField::where('id', $customer_->criminal_record)->first();
    $budgetScolaire  = App\Models\CustomerRadioField::where('id', $customer_->annual_budget)->first();

    // dd($customer_);
@endphp

<div class="sectionContainer" style="margin-top: 0;">
    <p>
        {{ __('lang.clear1001_date_of_meeting')}} : <strong> {{ $customer_->created_at }} </strong>
    </p>
    <p>
        {{ __('lang.clear1001_advisor_name')}} : <strong> {{ $conseiller->name }}</strong>
    </p>
    <p>
        {{ __('lang.clear1001_advisor_phone')}} : <strong> {{$conseiller->phone}}</strong>
    </p>
    <p>
        {{ __('lang.clear1001_customer_fullname')}} : <strong> {{ $customer->firstname }} {{ $customer->lastname }}</strong>
    </p>
    <p>
        {{ __('lang.clear1001_program')}} : <strong> {{ $lang === "fr" ? $program->name : $program->labelEng }}</strong>
    </p>
</div>
<h2 class="title">{{ __('lang.clear1001_customer_information_title')}}</h2>
<div class="sectionContainer">
    <p>
        {{ __('lang.clear1001_customer_dob')}} :<strong> {{ $customer->birthday }}</strong>
    </p>
    <p>
        {{ __('lang.clear1001_customer_phone')}} :<strong> {{ $customer->phone }}</strong>
    </p>
    <p>
        {{ __('lang.clear1001_customer_email')}} :<strong> {{ $customer->email }}</strong>
    </p>
</div>
<h2 class="title">{{ __('lang.clear1001_school_record_title')}}</h2>
<div class="sectionContainer">
    <table>
        <tr>
            <td style="width: 400px;">
                <p>{{ __('lang.clear1001_customer_study_level')}} :<strong> {{ $lang === "fr" ? $level->level_label_fr : $level->level_label_en }}</strong></p>
                <p>{{ __('lang.clear1001_customer_graduation_year')}} :<strong> {{ $graduation_year ? $graduation_year->label: "-" }}</strong></p>
                <p>{{ __('lang.clear1001_custome_study_field')}} :<strong> {{ $lang === "fr" ? $branch->branch_label_fr : $branch->branch_label_en }}</strong></p>
                <!--Todo : Ajouter la valeur si branch : Autre - Done-->
                @if($branch->id == 11)
                    <p>{{ __('lang.clear1001_custome_study_field')}} si autre :<strong> {{ $customer_->other_branch }}</strong></p>
                @endif
                <p>{{ __('lang.clear1001_customer_last_mark_diploma')}} : <strong> {{$customer_->grade }}</strong></p>
                <!--Todo : Ajouter la valeur Note des mathematiques sur le clear 1001  - Done-->
                <p>{{ __('lang.clear1001_customer_math_score')}} : <strong> {{$customer_->math_grade }}</strong></p>
                <p style="font-size: 10px;">{{ __('lang.clear1001_customer_bac_note')}}</p>

            </td>
            <td>
                <p>{{ __('lang.clear1001_customer_french_score')}} : <strong>{{ $customer_->lang_level }} </strong></p>
                <p style="font-size: 10px;">{{ __('lang.clear1001_customer_bac_note')}}</p>

                <!--Todo : Ajouter la valeur Test de français sur le clear 1001-->
                {{-- @php $test_fr = \App\Models\FrenchTest::find($customer_->test_fr); @endphp --}}
                {{-- <p>{{ __('lang.clear1001_customer_french_test')}} :<strong> {{ $test_fr ? $test_fr->test_label: '-' }}</strong></p> --}}

                <!--Todo : Ajouter la valeur Diplôme de français en multiple-->
                @php
                    $dpfs = \App\Models\CustomerFieldValue::where('customer_id', $customer->id)
                        ->where('field_name', 'language_tests')->pluck('model_id')
                        ->toArray();
                    $dpfs_ = \App\Models\LanguageTest::whereIn('id', $dpfs)->pluck('label')->toArray();
                @endphp
                <p>{{ __('lang.clear1001_customer_langue_diploma')}} :<strong> {{ implode(",", $dpfs_) }}</strong></p>

                <!--Todo : Ajouter la valeur Test de français sur le clear 1001-->
                {{-- @php $test_en = \App\Models\EnglishTest::find($customer_->test_en); @endphp --}}
                {{-- <p>{{ __('lang.clear1001_customer_english_test')}} : <strong> {{ \App\Models\EnglishTest::find($customer_->test_en)->test_label }}</strong></p> --}}

                {{-- <p>{{ __('lang.clear1001_customer_english_diploma')}} :<strong> {{ implode(",", $dpes_) }}</strong></p> --}}
            </td>
        </tr>
    </table>
</div>
<h2 class="title">{{ __('lang.clear1001_planned_studies_info_title')}}</h2>
<div class="sectionContainer">
    <table>
        <tr>
            <td style="width: 400px;">
                <!--Todo : Ajouter la valeur multiple-->
                @php
                    $study_session = \App\Models\StudySession::where("id", $customer_->study_session)->first();
                    $langs = \App\Models\CustomerFieldValue::where('customer_id', $customer->id)
                        ->where('field_name', 'language')->pluck('model_id')
                        ->toArray();
                    $langs_ = \App\Models\CustomerRadioField::whereIn('id', $langs)->pluck($lang === "fr" ? 'value_fr' : 'value_en')->toArray();
                    $cities = \App\Models\CustomerFieldValue::where('customer_id', $customer->id)
                        ->where('field_name', 'city')->pluck('model_id')
                        ->toArray();
                    $cities_ = \App\Models\CanadaCity::whereIn('id', $cities)->pluck($lang === "fr" ? 'label' : 'labelEng')->toArray();
                @endphp
                <p>{{ __('lang.clear1001_customer_study_session')}} : <strong>{{ $study_session->label_fr }}</strong></p>
                <p>{{ __('lang.clear1001_customer_language')}} : <strong>{{ implode(",", $langs_) }}</strong></p>
                <!--Todo : Ajouter la valeur multiple-->
                <p>{{ __('lang.clear1001_customer_city')}} : <strong>{{ implode(",", $cities_) }}</strong></p>
                <!--Todo : Ajouter la valeur sur le clear 1001 Done-->
                <p>{{ __('lang.clear1001_customer_educational_establishment')}} :<strong> {{ $customer_->est_name }}</strong></p>
                <p>{{ __('lang.clear1001_custome_study_field')}} : <strong>{{ $lang === "fr" ? $branch->branch_label_fr : $branch->branch_label_en }}</strong></p>

            </td>
            <td>
                <p>{{ __('lang.clear1001_customer_first_program_choice')}} : <strong>{{ $lang === "fr" ? $program1->choice_label : $program1->labelEng }}</strong></p>
                <p>{{ __('lang.clear1001_customer_second_program_choice')}} : <strong>{{ $lang === "fr" ? $program2->choice_label : $program2->labelEng }}</strong></p>
                <p>{{ __('lang.clear1001_customer_diploma')}} : <strong>{{ $lang === "fr" ? $diplomas->value_fr : $diplomas->value_en }}</strong></p>
                <p>{{ __('lang.clear1001_customer_tuition_fee')}} : <strong>{{ $lang === "fr" ? $budgetScolaire->value_fr : $budgetScolaire->value_en}} CAD</strong></p>
            </td>
        </tr>
    </table>
</div>

<h2 class="title">{{ __('lang.clear1001_financial_file_info_title')}}</h2>
<div class="sectionContainer">
    <table>
        <tr>
            <td style="width: 400px;">
                @php
                    $grmi = \App\Models\MonthlyIncome::find($customer_->income);
                    $grt = \App\Models\GuarantorType::find($customer_->garant_type);
                    $gra = \App\Models\AmountAvailable::find($customer_->amount);
                    $grg = \App\Models\CustomerRadioField::find($customer_->goods);
                    $gri = \App\Models\CustomerRadioField::find($customer_->rental_income);
                    $grbi = \App\Models\CustomerRadioField::find($customer_->business_income);

                    $guarantor_income_type = \App\Models\CustomerFieldValue::where('customer_id', $customer->id)
                        ->where('field_name', 'guarantor_income_type')->pluck('model_id')
                        ->toArray();
                    $g_income_type = \App\Models\MonthlyIncomeType::whereIn('id', $guarantor_income_type)->pluck('label_fr')->toArray();

                @endphp

                <p>{{ __('lang.clear1001_customer_guarantor_type')}} :<strong> {{ $grt ? $lang === "fr" ? $grt->garant_label_fr : $grt->garant_label_en : '-' }}</strong></p>

                <p>{{ __('lang.clear1001_customer_garant_first_name')}} :<strong> {{ $customer_->garant_first_name  ??  '-' }}</strong></p>

                <p>{{ __('lang.clear1001_customer_garant_last_name')}} :<strong> {{ $customer_->garant_last_name  ??  '-' }}</strong></p>

                <p>{{ __('lang.clear1001_customer_current_bank_balance')}} :<strong> {{ $customer_->bank_amount }}</strong></p>

                <p>{{ __('lang.clear1001_customer_study_project_budget')}} :<strong> {{ $gra ? $currency === "MAD" ? $gra->amount : ($currency === "EUR" ? $gra->amount_eur : $gra->amount_usd) : '-' }}</strong></p>
            </td>
            <td>
                <p>{{ __('lang.clear1001_customer_guarantor_income_type')}} :<strong> {{ implode(",", $g_income_type) }}</strong></p>

                <p>{{ __('lang.clear1001_customer_goods')}} :<strong> {{ $grg ? $lang === "fr" ? $grg->value_fr : $grg->value_en : '-' }}</strong></p>

                {{-- <p>{{ __('lang.clear1001_customer_rental_income')}} :<strong> {{ $gri ? $lang === "fr" ? $gri->value_fr : $gri->value_en : '-' }}</strong></p> --}}

                {{-- <p>{{ __('lang.clear1001_customer_business_income')}} :<strong> {{ $grbi ? $lang === "fr" ? $grbi->value_fr : $grbi->value_en : '-' }}</strong></p> --}}
            </td>
        </tr>
    </table>
</div>
<!--Todo : Put multiple garant : please keep style as inline like exemple style="margin-top: 10px;" -->
@php
    $garants = \App\Models\CustomerGarant::where('customer_id', $customer->id)->orderBy('id', 'ASC')->get();
@endphp
@if(count($garants))
    @foreach($garants as $garant)
        <div class="sectionContainer" style="margin-top: 10px;">
            <table>
                <tr>
                    @php
                        $grmi = \App\Models\MonthlyIncome::find($garant->income);
                        $grt = \App\Models\GuarantorType::find($garant->garant_type);
                        $gra = \App\Models\AmountAvailable::find($garant->amount);
                        $grg = \App\Models\CustomerRadioField::find($garant->goods);
                        $gri = \App\Models\CustomerRadioField::find($garant->rental_income);
                        $grbi = \App\Models\CustomerRadioField::find($garant->business_income);
                    @endphp
                    <td style="width: 400px;">
                        <p>Type de garant :<strong> {{ $grt ? $lang === "fr" ? $grt->garant_label_fr : $grt->garant_label_en : '-' }}</strong></p>
                        <p>Revenu mensuel :<strong> {{ $grmi ? $currency === "MAD" ? $grmi->income : ($currency === "EUR" ? $grmi->income_eur : $grmi->income_usd) : '-' }}</strong></p>
                        <p>Solde bancaire actuel :<strong> {{ $garant->bank_amount }}</strong></p>
                        <p>Budget projet d’étude :<strong> {{ $gra ? $currency === "MAD" ? $gra->amount : ($currency === "EUR" ? $gra->amount_eur : $gra->amount_usd) : '-' }}</strong></p>
                    </td>
                    <td>
                        <p>Biens :<strong> {{ $grg ? $lang === "fr" ? $grg->value_fr : $grg->value_en : '-' }}</strong></p>
                        <p>Revenu locatif :<strong> {{ $gri ? $lang === "fr" ? $gri->value_fr : $gri->value_en : '-' }}</strong></p>
                        <p>Revenu d’entreprise :<strong> {{ $grbi ? $lang === "fr" ? $grbi->value_fr : $grbi->value_en : '-' }}</strong></p>
                    </td>
                </tr>
            </table>
        </div>
    @endforeach
@endif
<h2 class="title">{{ __('lang.clear1001_medical_criminal_infos_title')}}</h2>
<div class="sectionContainer">
    <table>
        <tr>
            <td style="width: 400px;">
                <p>{{ __('lang.clear1001_customer_medical_info')}} <strong>{{ $lang === "fr" ? $health->value_fr : $health->value_en }}</strong></p>
                <!--Todo : Ajouter la valeur sur le clear 1001 et affichage ici 4-->
                @if($health->id == 4)
                    <p>Si oui, lesquels ? :<strong> {{ $customer_->health_rep }}</strong></p>
                @endif
            </td>
            <td>
                <p>{{ __('lang.clear1001_customer_criminal_info')}} : <strong>{{ $lang === "fr" ? $criminel_record->value_fr : $criminel_record->value_en }}</strong></p>
                <!--Todo : Ajouter la valeur sur le clear 1001 et affichage ici 6-->
                @if($criminel_record->id == 6)
                    <p>Si oui, lesquels ?  :<strong> {{ $customer_->criminal_record_rep }}</strong></p>
                @endif
            </td>
        </tr>
    </table>
</div>
<h2 class="title">{{ __('lang.clear1001_previous_visa_app_title')}}</h2>
<div class="sectionContainer">
    <table>
        <tr>
            <td style="width: 400px;">
                <!--Todo : Ajouter la valeur-->
                @php $vr = \App\Models\CustomerRadioField::find($customer_->visa_request);  @endphp
                <p>{{ __('lang.clear1001_customer_previous_visa_app_info')}} : <strong> {{ $vr ? $lang === "fr" ? $vr->value_fr : $vr->value_en : '-' }} </strong></p>
            </td>
        </tr>
        @if($customer_->visa_request == 82)
        <tr>
            <td>
                <!--Todo : Ajouter la valeur en multiple-->
                <p>Si oui, indiquez la date de toutes les demandes, le type de la demande et le résultat de chaque demande ? :</p>
                <table>
                    <tr>
                        <td style="width: 33%">
                            <p>Date de la demande : <strong> {{ $customer_->visa_request_date }} </strong></p>
                        </td>
                        <td style="width: 33%">
                            @php $vrd = \App\Models\Program::find($customer_->visa_request_type); @endphp
                            <p>Type de la demande : <strong> {{ $vrd ? $vrd->name:'-' }} </strong></p>
                        </td>
                        <td style="width: 33%">
                            @php $vvr = \App\Models\CustomerRadioField::find($customer_->visa_request_result);  @endphp
                            <p>Résultat de la demande : <strong> {{ $vvr ? $vvr->value_fr: '-' }} </strong></p>
                        </td>
                    </tr>
                    @php
                        $visa_requests = \App\Models\CustomerVisaRequest::where('customer_id', $customer->id)->orderBy('id', 'ASC')->get();
                    @endphp
                    @if(count($visa_requests))
                        @foreach($visa_requests as $visa_request)
                            <tr>
                                <td style="width: 33%">
                                    <p>Date de la demande : <strong> {{ $visa_request->visa_request_date }} </strong></p>
                                </td>
                                <td style="width: 33%">
                                    @php $vrd = \App\Models\Program::find($visa_request->visa_request_type); @endphp
                                    <p>Type de la demande : <strong> {{ $vrd ? $vrd->name:'-' }} </strong></p>
                                </td>
                                <td style="width: 33%">
                                    @php $vrr = \App\Models\CustomerRadioField::find($visa_request->visa_request_result);  @endphp
                                    <p>Résultat de la demande : <strong> {{ $vrr ? $vrr->value_fr: '-' }} </strong></p>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                </table>
            </td>
        </tr>
        @endif
    </table>

</div>
<div style="page-break-before: always;"></div>
<div class="sectionContainer" style="background-color: #fff8f8;margin-top: 5px;">
    <div style="width: 100%;text-align: center;">
        <img class="center" style="width: 190px;" src="{{ public_path('/img/logo-fr.png') }}" alt="">
    </div>
</div>
<div class="sectionContainer" style="background-color: #ff2929;margin-top: 5px;">
    <h1 class="big-title" style="margin-top: 40px;margin-bottom: 20px;line-height: 0;color: white">Clear 1001 : {{ __('lang.clear1001_title_bis')}}</h1>
</div>

<h2 class="title">{{ __('lang.clear1001_processing_refusals_title')}}</h2>
<div class="section_Container">
    <p  class="text-normal"> {{ __('lang.clear1001_processing_refusals_text')}}</p>
</div>
<h2 class="title">{{ __('lang.clear1001_canada_study_programs_title')}}</h2>
<div class="section_Container">
    <p  class="text-normal">{{ __('lang.clear1001_canada_study_programs_text')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_canada_study_programs_subtitle_1')}} </strong>{{ __('lang.clear1001_canada_study_programs_text_1')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_canada_study_programs_subtitle_2')}} </strong>{{ __('lang.clear1001_canada_study_programs_text_2')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_canada_study_programs_subtitle_3')}} </strong>{{ __('lang.clear1001_canada_study_programs_text_3')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_canada_study_programs_subtitle_4')}} </strong>{{ __('lang.clear1001_canada_study_programs_text_4')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_canada_study_programs_subtitle_5')}} </strong>{{ __('lang.clear1001_canada_study_programs_text_5')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_canada_study_programs_subtitle_6')}} </strong>{{ __('lang.clear1001_canada_study_programs_text_6')}}</p>
</div>
<h2 class="title">{{ __('lang.clear1001_guarantor_financial_prerequisites_title')}}</h2>
<div class="section_Container">
    <p class="text-normal"><strong>{{ __('lang.clear1001_guarantor_financial_prerequisites_subtitle_1')}} </strong> {{ __('lang.clear1001_guarantor_financial_prerequisites_text_1')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_guarantor_financial_prerequisites_subtitle_2')}} </strong> {{ __('lang.clear1001_guarantor_financial_prerequisites_text_2')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_guarantor_financial_prerequisites_subtitle_3')}} </strong> {{ __('lang.clear1001_guarantor_financial_prerequisites_text_3')}}</p>
</div>
<h2 class="title">{{ __('lang.clear1001_financial_solutions_title')}}</h2>
<div class="section_Container">
    <p class="text-normal"><strong>{{ __('lang.clear1001_financial_solutions_subtitle_1')}} </strong>{{ __('lang.clear1001_financial_solutions_text_1')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_financial_solutions_subtitle_2')}} </strong>{{ __('lang.clear1001_financial_solutions_text_2')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_financial_solutions_subtitle_3')}} </strong>{{ __('lang.clear1001_financial_solutions_text_3')}}</p>
</div>
<h2 class="title">{{ __('lang.clear1001_direct_component_program_title')}}</h2>
<div class="section_Container">
    <p class="text-normal">
        {{ __('lang.clear1001_direct_component_program_text')}}
    </p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_direct_component_program_subtitle_1')}} </strong>{{ __('lang.clear1001_direct_component_program_text_1')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_direct_component_program_subtitle_2')}} </strong>{{ __('lang.clear1001_direct_component_program_text_2')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_direct_component_program_subtitle_3')}} </strong>{{ __('lang.clear1001_direct_component_program_text_3')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_direct_component_program_subtitle_4')}} </strong>{{ __('lang.clear1001_direct_component_program_text_4')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_direct_component_program_subtitle_5')}} </strong>{{ __('lang.clear1001_direct_component_program_text_5')}}</p>
    <p class="text-normal"><strong>{{ __('lang.clear1001_direct_component_program_subtitle_6')}} </strong>{{ __('lang.clear1001_direct_component_program_text_6')}}</p>
</div>
<img style="width: 180px;position:absolute;top:85%;right:5%;" src="{{ public_path('/img/cachett.png') }}" alt="">

</body>
</html>
