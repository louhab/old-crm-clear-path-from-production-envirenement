<?php
    $quote = \App\Models\Quote::where('customer_id', $customer->id)->first();
    $currency = $customer->currency_id ? \App\Models\Currency::where("id", $customer->currency_id)->first()->iso : "MAD";
    $lead = \App\Models\Lead::where("id", $customer->lead_id)->first();
?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Invoice</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,500;0,700;1,200;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <style>
        @page { margin: 0in;size: 700pt 1020pt; }
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100&display=swap');
        /* @font-face {
            font-family: 'Poppins-Light';
            src: url('{{ public_path('Poppins-Light.ttf') }}') format('truetype');
    }
    @font-face {
        font-family: 'Poppins-Bold';
        src: url('{{ public_path('Poppins-Bold.ttf') }}') format('truetype');
    }
    @font-face {
        font-family: 'Poppins-Thin';
        src: url('{{ public_path('Poppins-Thin.ttf') }}') format('truetype');
    } */
        body {
            /* font-family: 'Poppins-Light', Fallback, sans-serif; */
            font-family: 'Poppins', sans-serif;
            font-size: 17px;
            /* text-align: center; */
            /* border: thin solid black;   */
            /* font-family: "Poppins", sans-serif; */
            background: url("{{ public_path('/img/background_devis.png') }}");
            background-size: 350px;
            background-repeat: no-repeat;
            background-position: 100% 100%;
            /* margin-top:35px; */
            margin-top: 2cm;
            /* margin-left: 2cm; */
            margin-right: 0;
            margin-bottom: 0;
        }
        br {
            /* line-height:22px; */
        }
        /* .test {
            background-image: linear-gradient(black 33%, rgba(255,255,255,0) 0%);
            background-position: right;
            background-size: 1px 3px;
            background-repeat: repeat-y;
        } */
        /* strong{
            font-family: 'Poppins-Bold', sans-serif;
        } */
        .page-break {
            page-break-before: always;
        }
        table.devis, th, td {
            border: 1px solid red;
        }
        /** Define the header rules **/
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 3cm;

            /** Extra personal styles **/
            /* background-color: #c6394e; */
            color: black;
            font-size: .8em;
            /* font-weight: lighter; */
            /* text-align: center; */
            line-height: 0.8cm;
            margin: 20px;
            margin-top: 30px;
            padding-bottom: 100px;
            line-height: 0.3cm;
        }

        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0;
            left: 35px;
            right: 35px;
            height: 1cm;
            color: black;
            font-size: .7em;
            text-align: center;
            line-height: 0.4cm;
            padding:0;
            margin:0;
            border-top: 2px solid red;

        }
        .cric {
            display:inline-block;
            height:70px;
            float:right;
            margin-right: 60px;
            margin-top: 15px;
        }
        /* devis */
        .invoice table {
            font-family: "Poppins", sans-serif;
            font-size: .85em;
            /*margin-top: 5px;*/
            /*margin-right: 100px;*/
            /*width: 100%;*/
            /*margin: 5px 30px;*/
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
            /*page-break-inside: auto;*/
        }

        /*.invoice table tr {
            page-break-inside: always;
        }*/

        .invoice table td {
            font-family: "Poppins", sans-serif;
            padding: 1px 5px;
            background: #fff;
            border: 1px solid #c6394e;
            /* text-align: right; */
            /*page-break-inside: always;*/
        }

        .invoice table th {
            font-family: "Poppins", sans-serif;
            white-space: nowrap;
            background: #c6394e;
            color: #fff;
            /* font-weight: 400; */
            font-size: 1em;
            padding: 1px 5px;
        }

        .invoice table td h3 {
            font-family: "Poppins", sans-serif;
            margin: 0;
            font-weight: 400;
            color: #c6394e;
            font-size: 1em
        }

        .invoice table .qty,.invoice table .total,.invoice table .unit {
            font-family: "Poppins", sans-serif;
            text-align: center;
            vertical-align: middle;
            font-size: 1em
        }

        .invoice table .no {
            font-family: "Poppins", sans-serif;
            color: #000;
            font-size: 1em;
            background: #fff;
        }

        .invoice table tbody tr.srow td {
            background: #c6394e;text-align: right;color: #fff;
        }

        .invoice table .unit {
            background: #fff;
            color: #000;
        }

        .invoice table .total {
            background: #fff;
            color: #000;
        }

        .invoice table tbody tr:last-child td {
            font-family: "Poppins", sans-serif;
            border: none
        }

        .invoice table tfoot td {
            font-family: "Poppins", sans-serif;
            background: 0 0;
            border-bottom: none;
            white-space: nowrap;
            text-align: right;
            padding: 5px 10px;
            font-size: 1em;
            /* border-top: 1px solid #aaa */
        }

        .invoice table tfoot tr:first-child td {
            border-top: none
        }

        .invoice table tfoot tr:last-child td {
            color: #c6394e;
            font-size: 1.2em;
            /* border-top: 1px solid #c6394e */
        }

        .invoice table tfoot tr td:first-child {
            font-family: "Poppins", sans-serif;
            border: none
        }
        th, td {
            text-align:center;
        }
        .total-step {
            background-color: #F4CCCC;
            color: white;
        }
    </style>
</head>
<body>
<header>
        <span style="display:inline;margin-bottom:50px;">
            <img class="image" style="width: 140px;" src="{{ public_path('/img/logo.png') }}" alt="">
        </span>
    <span class="cric text-muted">
        @if($lead->lang === "en") RCIC member No @else No de membre du CRIC @endif : <strong>R705679</strong><br />
        @if($lead->lang === "en") No de membre du CRIC  @else No de dossier du client @endif  : {{ $quote->customer->phone }}
    </span>
</header>

<footer>
    <table width="100%">
        <tr>
            <td width="100%" style="text-align: center;">
                @if($lead->lang === "en") Contract established in accordance with the regulations governing the Professional Service Contract granted by the City Council. (V : 2016-004)  @else Contrat établie selon le règlement régissant le Contrat de Service Professionnel fourni par le Conseil (V : 2016-004) @endif
            </td>
        </tr>
    </table>
</footer>
<!-- <header class="m-5" style="text-align:center;">
        <img class="image" style="width: 175px;" src="{{ public_path('/img/logo.png') }}" alt="">
        <div>
            <h3 style="color:red;border: 1px dashed black;padding-bottom:10px;margin: 10px 120px;font-weight: bold;font-size:1.1em;">CONTRAT DE SERVICE PROFESSIONNEL</h3>
        </div>
        <table width="100%">
            <tr class="text-danger" style="font-weight: bold;">
                <td class="test">N° de membre du CRIC :</td>
                <td><span style="position:absolute;right:100px">N° de dossier client :</span></td>
            </tr>
        </table>
    </header> -->
<br>
<br>
<table width="100%">
    <tr>
        <td width="20%" style="border: none;"></td>
        <td style="text-align: center;font-size: 20px;font-weight: bold;color: red;border: black dashed 1.2px;padding-bottom: 7px;">
            @if($lead->lang === "en") PROFESSIONAL SERVICE CONTRACT  @else CONTRAT DE SERVICE PROFESSIONNEL @endif
        </td>
        <td width="20%" style="border: none;"></td>
    </tr>
</table>
<main class="invoice m-4">
<!--    <div class="text-center">
        <h3 style="text-align: center;color:red;border: 1px dashed black;padding-bottom:10px;margin: 10px 120px;font-weight: bold;font-size:1.1em;">CONTRAT DE SERVICE PROFESSIONNEL</h3>
    </div>-->
    <p style="font-family: 'Poppins', sans-serif;">
            <span style="line-height:0.4cm;">
                @if($lead->lang === "en")
                    This professional service contract is established, on the {{ \Carbon\Carbon::now()->format('d-m-Y') }}, between : Mr <strong>Ahmed BAHAJOUB</strong>
                    (the «RCIC»), with address<strong>, au, 1 285 Allan, Laval, Qc, H7w1h1 (Clear Path Canada Immigration Services)</strong>, and Mr./Mrs
                    <strong>{{ $quote->customer->firstname }} {{ $quote->customer->lastname }}</strong> (the «Client»).
                @else
                    Ce contrat de service professionnel est établi le {{ \Carbon\Carbon::now()->format('d-m-Y') }}, entre : Monsieur <strong>Ahmed BAHAJOUB</strong>
                    (le « CRIC »)<strong>, au, 1 285 Allan, Laval, Qc, H7w1h1 (Clear Path Canada Immigration Services)</strong>, et le client
                    <strong>{{ $quote->customer->firstname }} {{ $quote->customer->lastname }}</strong>
                @endif
            </span>
        <br />
        <span>
                @if($lead->lang === "en")
                    AND WHEREAS the RCIC is a member of the Regulatory Council of Immigration Consultants of Canada (the “Council”), the Canadian Regulatory Organization for Immigration Consultants;;<br />
                    CONSIDERING the reciprocal commitments of this contract, the parties agree to the following:
                @else
                    ATTENDU QUE le CRIC et le client souhaitent conclure une entente par écrit qui contient les
                    conditions convenues selon lesquelles le CRIC fournira ses services au client ;<br />
                    ET ATTENDU QUE le CRIC est un membre du Conseil de réglementation des consultants en
                    immigration du Canada (le « Conseil »), l’organisme de réglementation canadien des
                    consultants en immigration ;<br />
                    EN CONTREPARTIE des engagements réciproques de ce contrat,
                    les parties conviennent de ce qui suit :
                @endif
            </span>
        <br />
        @if($lead->lang === "en")
            <strong>1. Responsibilities and obligations of the RCIC:</strong><br/>
        @else
            <strong>1. Responsabilités et obligations du CRIC</strong><br/>
        @endif
        <span>
            @if($lead->lang === "en")
                The Client requests, and the RCIC agrees, to represent the Client in:<br/>
            @else
                Le client demande au CRIC, et celui-ci accepte, de représenter le client au sujet de<br/>
            @endif
        </span>
        @if($quote->customer->program_id == 4)
            @if($lead->lang === "en")
                <b style="line-height:0.4cm;">Application for temporary residence in Canada under the foreign student program.</b><br/>
                In exchange for the fees paid, and due to the aforementioned case, the RCIC undertakes to:<br/>
                <span style="line-height:0.4cm;">
                    <strong>A)</strong> Apply for the Québec Acceptance Certificate (CAQ) (if applicable);<br/>
                    <strong>B)</strong> Send the official application for temporary residence;<br/>
                    <strong>C)</strong> Ensure full follow-up of the application until a government decision is issued;<br/>
                    <strong>D)</strong> Prepare the Client for their official immigration interview (if applicable);<br/>
                    <strong>E)</strong> Respond to any request for information about the application<br/>
                    {{-- * L’emploi du masculin vise à alléger le texte; le genre masculin inclut le féminin.<br/> --}}
                </span>
            @else
                <b style="line-height:0.4cm;">Demande de résidence temporaire au Canada sous le programme d'étudiant étranger.</b><br/>
                En contrepartie des frais payés et en raison de l’affaire mentionnée<br/>
                ci-dessus, le CRIC s’engage à :<br/>
                <span style="line-height:0.4cm;">
                    <strong>A)</strong> Demande de CAQ (Le cas échéant).<br/>
                    <strong>B)</strong> Envois de la demande officielle de résidence temporaire du client.<br/>
                    <strong>C)</strong> Assurer le suivi intégral de la demande jusqu'à ce qu’une décision du gouvernement soit rendue.<br/>
                    <strong>D)</strong> Préparer le client a son entrevue officielle d’immigration, le cas échéant.<br/>
                    <strong>E)</strong> Répondre à toute demande d’information du client au sujet de sa demande.<br/>
                    * L’emploi du masculin vise à alléger le texte; le genre masculin inclut le féminin.<br/>
                </span>
            @endif
        @else
        {{-- Should translated --}}
        @if($lead->lang === "en")
            <b style="line-height:0.4cm;">Application for temporary residence in Canada under the Express Entry.</b><br/>
            In exchange for the fees paid, and due to the aforementioned case, the RCIC undertakes to:<br/>
            <span style="line-height:0.4cm;">
                <strong>A)</strong> Préparer et envoyer la demande officielle d’immigration du client.<br/>
                <strong>B)</strong> Préparation et soumission de la demande d'équivalence des diplômes le cas échéant.<br/>
                <strong>C)</strong> Assurer le suivi intégral de la demande jusqu'à ce qu’une décision du gouvernement sont
                rendue.<br/>
                <strong>D)</strong> Préparer le client a son entrevue officielle d’immigration, le cas échéant.<br/>
                <strong>E)</strong> Répondre à toute demande d’information du client au sujet de sa demande.<br/>
            </span>
        @else
            <b style="line-height:0.4cm;"> Demande d’immigration au Canada sous le programme d’Entrée Express.</b><br/>
            En contrepartie des frais payés et en raison de l’affaire mentionnée<br/>
            ci-dessus, le CRIC s’engage à :<br/>
            <span style="line-height:0.4cm;">
                <strong>A)</strong> Préparer et envoyer la demande officielle d’immigration du client.<br/>
                <strong>B)</strong> Préparation et soumission de la demande d'équivalence des diplômes le cas échéant.<br/>
                <strong>C)</strong> Assurer le suivi intégral de la demande jusqu'à ce qu’une décision du gouvernement sont
                rendue.<br/>
                <strong>D)</strong> Préparer le client a son entrevue officielle d’immigration, le cas échéant.<br/>
                <strong>E)</strong> Répondre à toute demande d’information du client au sujet de sa demande.<br/>
            </span>
        @endif

        @endif
        @if($lead->lang === "en")
            <strong>2. Responsibilities and obligations of the Client:</strong><br/>
            <span>
                    2.1 The Client must provide at the request of the RCIC:<br/>
            </span>
            <ul>
                <li>All the necessary documentation;</li>
                <li>All documentation in English or French or translated into English or French.</li>
            </ul>
            2.2 The Client agrees that he must provide accurate and honest information and that any inaccurate information may void this contract or adversely affect the response to the request or the maintenance of any status obtained. RCIC's obligations under the Professional Service Agreement are void if the Client knowingly provides material information that is inaccurate, misleading or false. The financial obligations of the Client remain<br/>
            2.3 If Immigration, Refugees and Citizenship Canada (IRCC) or Employment and Social Development Canada (EDSC) communicate directly with the Client, the Client must immediately notify the RCIC.<br/>
            2.4 The Client must immediately inform the RCIC of any change in the marital, family or marital status or contact details of any person included in the request.<br/>
            2.5 In the case of a joint professional service contract, Clients agree that RCIC may exchange information between all Clients as necessary. Additionally, if a conflict that cannot be resolved arises, the RCIC will no longer be able to represent both or all Clients and may have to withdraw from the case entirely.<br/>
            <strong>3. Billing method:</strong><br/>
            The Client will be billed once the service has been provided.<br/>
            Below is the billing method used in detail:<br/>
        @else
            <strong>2. Responsabilités et obligations du client</strong><br/>
            <span>
                    2.1 Le client doit fournir à la demande du CRIC :<br/>
            </span>
            <ul>
                <li>Toute la documentation nécessaire</li>
                <li>Toute la documentation en anglais ou en français ou traduite en anglais ou en français</li>
            </ul>
            2.2 Le client convient qu’il doit fournir de l’information exacte et honnête et que toute
            information inexacte peut annuler ce contrat ou avoir une incidence défavorable sur la
            réponse à la demande ou le maintien de tout statut obtenu. Les obligations du CRIC en vertu
            du contrat de service professionnel sont nulles si le client fournit en toute connaissance de
            cause des renseignements importants inexacts, trompeurs ou faux. Les obligations
            financières du client demeurent.<br/>
            2.3 Si Immigration, Réfugiés et Citoyenneté Canada (IRCC) ou Emploi et Développement social
            Canada (EDSC) communique directement avec le client,
            le client doit immédiatement en aviser le CRIC.<br/>
            2.4 Le client doit immédiatement informer le CRIC de tout changement de situation
            matrimoniale, de famille ou d’état civil ou de coordonnées de toute personne
            incluse dans la demande.<br/>
            2.5 Dans le cas d’un contrat de service professionnel conjoint, les clients conviennent
            que le CRIC peut échanger de l’information entre tous les clients au besoin.
            En outre, si un conflit surgit qui ne peut être résolu, le CRIC ne pourra pas continuer
            à représenter les deux clients ou tous les clients
            et devra peut-être se retirer complètement de l’affaire.
            <br/>
        @endif

    </span>

    @if($lead->lang === "en")
    @else
        <strong>3. Méthode de facturation</strong><br/>
        Le client sera facturé une fois le service rendu.<br/>
        Voici en détail la méthode de facturation utilisée :<br/>
    @endif

    <div class="page-break"></div>
    <div style="line-height: 25px;margin-top: 1.5em;">
        <table width="100%" style="margin-bottom: 0;">
            <thead>
                <tr class="total-step">
                    <th width="20%" style=""></th>
                    <th style="text-align:center;border-left: none;">Description</th>
                    <th width="10%">@if($lead->lang === "en") Amount @else Montants @endif</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td rowspan='6'>
                        @if($lead->lang === "en") Phase @else Etape @endif 1:<br>
                        <span style="color: #FF0100;">@if($lead->lang === "en") Data Analysis and Eligibility @else Analyse des données et admissibilité @endif</span>
                    </td>
                    <td>
                        <span style="color: #FF0100;">
                            @if($lead->lang === "en") Meeting @else Rencontre @endif 1 : @if($lead->lang === "en") Data Analysis and Eligibility @else Analyse des données et admissibilité @endif
                        </span>
                    </td>
                    <td class="unit" rowspan='6'>
                        @if($currency === "MAD")
                            200
                        @elseif ($currency === "EUR")
                            50
                        @else
                            50
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <div class="text-muted">
                            @if($lead->lang === "en") Information collection and student questionnaire completion @else Collecte des informations et remplir le questionnaire étudiant @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <div class="text-muted">
                            @if($lead->lang === "en") Analyze information and recommendations Borrar 1004 @else Analyse des informations et recommandations Clear 1004 @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <div class="text-muted">
                            @if($lead->lang === "en") Explanation of documents: Clear 10, CNP, Quote, contract @else Explication des documents : Clear 10, CNP, Devis, contrat @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <div class="text-muted">
                            @if($lead->lang === "en") Access to the portal and customer area @else Accès au portail et espace client @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <div class="text-muted">
                            @if($lead->lang === "en") Become a member of the Clear Path Canada network to receive all the news @else Devenir membre du réseau Clear Path Canada pour recevoir toutes les nouvelles @endif
                        </div>
                    </td>
                </tr>
                <tr class="total-step">
                    <td></td>
                    <td class="align-middle">
                        <div class="text-muted">
                            Total @if($lead->lang === "en") Phase @else étape @endif 1
                        </div>
                    </td>
                    <td class="unit">
                        @if($currency === "MAD")
                            200
                        @elseif ($currency === "EUR")
                            50
                        @else
                            50
                        @endif
                    </td>
                </tr>

                <!-- Etape 2-->
                <!--subtep1-->
                <tr>
                    <td rowspan='18'>
                        @if($lead->lang === "en") Phase @else Etape @endif 2:<br>
                        <span style="color: #FF0100;">@if($lead->lang === "en") Development of the immigration project @else Développement du projet d'immigration @endif</span>
                    </td>
                    <td>
                        <span style="color: #FF0100;">
                            @if($lead->lang === "en") Meeting @else Rencontre @endif 2 : @if($lead->lang === "en") Preparation of the equivalence request and language tests @else Préparation de la demande d'equivalence et les tests de langues @endif
                        </span>
                    </td>
                    <td class="unit" rowspan='8'>
                        @if($currency === "MAD")
                            700
                        @elseif ($currency === "EUR")
                            130
                        @else
                            100
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <div class="text-muted">
                            @if($lead->lang === "en") File opening  @else Ouverture du dossier @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <div class="text-muted">
                            @if($lead->lang === "en") Explanation of the procedure: Clear 11, Clear 12 @else Explication de la procédure : Clear 11, Clear 12 @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <div class="text-muted">
                            @if($lead->lang === "en") Development of the immigration project, support and supervision @else Développement du projet d'immigration @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <div class="text-muted">
                            @if($lead->lang === "en") Preparation of the equivalence of diplomas request @else Préparation de la demande d'equivalence des diplomes @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <div class="text-muted">
                            @if($lead->lang === "en") Preparation for language tests: TCF Canada, TEF Canada and IELTS @else Preparation des tests de langues : TCF Canada, TEF Canada et IELTS @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <div class="text-muted">
                            @if($lead->lang === "en") Choice of the CNP @else Le choix du CNP @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="align-middle">
                        <div class="text-muted">
                            @if($lead->lang === "en") Equivalence of diplomas request presentation @else Soumission de la demande d'equivalence des diplomes @endif
                        </div>
                    </td>
                </tr>
            <!--subtep2-->
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") WES fees @else Frais WES @endif
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        2450
                    @elseif ($currency === "EUR")
                        260
                    @else
                        250
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        TCF Canada
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        2750
                    @elseif ($currency === "EUR")
                        270
                    @else
                        280
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        TEF Canada
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        2750
                    @elseif ($currency === "EUR")
                        270
                    @else
                        280
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        IELTS
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        2750
                    @elseif ($currency === "EUR")
                        270
                    @else
                        280
                    @endif
                </td>
            </tr>
            <!--substep3-->
            <tr>
                <td>
                    <span style="color: #FF0100;">
                        @if($lead->lang === "en") Meeting @else Rencontre @endif 3 : @if($lead->lang === "en") Preparation of the Express Entry profile application @else Préparation de la demande du profil Entree Express @endif
                    </span>
                </td>
                <td class="unit" rowspan='6'>
                    @if($currency === "MAD")
                        5000
                    @elseif ($currency === "EUR")
                        600
                    @else
                        600
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Validation of the equivalence request results @else Valider les resultas de la demande dequivalence @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Validation of the language test results @else Valider les resultats des test de francais @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") CNP choice validation @else Valider le choix du CNP @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Creation of the express entry profile @else Creation du profil Entree Express @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Express Entry profile application and presentation @else Soumission de la demande du profil Entree Express @endif
                    </div>
                </td>
            </tr>
            <tr class="total-step">
                <td></td>
                <td class="align-middle">
                    <div class="text-muted">
                        Total @if($lead->lang === "en") Phase @else étape @endif 2
                    </div>
                </td>
                <td class="unit">
                    @if($currency === "MAD")
                        10900
                    @elseif ($currency === "EUR")
                        1260
                    @else
                        1230
                    @endif
                </td>
            </tr>
            <!--Etape3-->
            <!--substep1-->
            <tr>
                <td rowspan='11'>
                    @if($lead->lang === "en") Phase @else Etape @endif 3:<br>
                    <span style="color: #FF0100;">@if($lead->lang === "en") Visa application and permanent residence @else Demande du visa et residence permanente @endif</span>
                </td>
                <td>
                    <span style="color: #FF0100;">
                        @if($lead->lang === "en") Meeting @else Rencontre @endif 4 : @if($lead->lang === "en") Preparation of the Visa & PR application @else Préparation de la demande du Visa & RP @endif
                    </span>
                </td>
                <td class="unit" rowspan='4'>
                    @if($currency === "MAD")
                        5000
                    @elseif ($currency === "EUR")
                        700
                    @else
                        600
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Explanation of the procedure: Clear 13 @else Explication de la procédure : Clear 13 @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Accompaniment and supervision @else Accompagnement et encadrement @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Preparation of the visa application and permanent residence @else Préparation de la demande du visa et residence permanente @endif
                    </div>
                </td>
            </tr>
            <!--substep2-->
            <tr>
                <td>
                    <span style="color: #FF0100;">
                        @if($lead->lang === "en") Meeting @else Rencontre @endif 5 : @if($lead->lang === "en") Visa & PR request presentation @else Soumission de la demande du Visa & RP @endif
                    </span>
                </td>
                <td class="unit" rowspan='4'>
                    @if($currency === "MAD")
                        5000
                    @elseif ($currency === "EUR")
                        700
                    @else
                        600
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Document Verification: Clear 13 @else Vérification des documents : Clear 13 @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Visa and permanent residence forms signature @else Signature des formulaires de visa et residence permanente @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Visa and permanent residence application and presentation @else Soumission de la demande de visa et residence permanente @endif
                    </div>
                </td>
            </tr>
            <!--substep3-->
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Immigration application fee @else Frais de la demande d'immigration @endif
                    </div>
                </td>
                <td class="unit">
                    @if($currency === "MAD")
                        3800
                    @elseif ($currency === "EUR")
                        400
                    @else
                        400
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Medical visit fee @else Frais de la visite medicale @endif
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        1550
                    @elseif ($currency === "EUR")
                        200
                    @else
                        160
                    @endif
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <div class="text-muted">
                        @if($lead->lang === "en") Biometrics, visa fees and permanent residency @else Biométrie, Frais de visa et résidence permanente @endif
                    </div>
                </td>
                <td>
                    @if($currency === "MAD")
                        6300
                    @elseif ($currency === "EUR")
                        750
                    @else
                        640
                    @endif
                </td>
            </tr>
            <tr class="total-step">
                <td></td>
                <td class="align-middle">
                    <div class="text-muted">
                        Total @if($lead->lang === "en") Phase @else étape @endif 3
                    </div>
                </td>
                <td class="unit">
                    @if($currency === "MAD")
                        21650
                    @elseif ($currency === "EUR")
                        2750
                    @else
                        2400
                    @endif
                </td>
            </tr>
            <tr style="color: #FF0100;">
                <td></td>
                <td>
                    <span style="font-weight: bold;line-height: 12px">
                        Total*
                    </span>
                </td>
                <td class="unit">
                    @if($currency === "MAD")
                        32750
                    @elseif ($currency === "EUR")
                        4060
                    @else
                        3680
                    @endif
                </td>
            </tr>
        </tbody>
    </table>

            <div class="page-break"></div>
            <br/>
            <br/>
            <table style="width: 100%;">
                <thead>
                    <tr class="total-step">
                        <th width="20%" style=""></th>
                        <th style="text-align:center;border-left: none;">Description</th>
                        <th width="10%">@if($lead->lang === "en") Amount @else Montants @endif</th>
                    </tr>
                    </thead>
                <tbody>
                    <tr>
                        <td class="align-middle no" rowspan='3'>
                            <span style="color: #FF0100;">Options</span>
                        </td>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if($lead->lang === "en") Welcome package: Welcome, integration, advice and support in Canada @else Package : Accueil, intégration, conseils et encadrement au Canada @endif
                            </div>
                        </td>
                        <td class="unit">
                            @if($currency === "MAD")
                            16500
                            @elseif ($currency === "EUR")
                                160
                            @else
                                145
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if($lead->lang === "en") Coaching session in Canada (per hour) @else Séance de coaching par heure au Canada @endif
                            </div>
                        </td>
                        <td class="unit">
                            @if($currency === "MAD")
                                700
                            @elseif ($currency === "EUR")
                                70
                            @else
                                70
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if($lead->lang === "en") Mailing fees @else Frais d'un envoi postal @endif
                            </div>
                        </td>
                        <td>
                            @if($currency === "MAD")
                                850
                            @elseif ($currency === "EUR")
                                85
                            @else
                                90
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="float-right" style="color: #FF0100;" colspan='3'>
                            @if($lead->lang === "en") * All amounts are exclusive of tax @else *Tous les montants mentionnés sur les devis sont Hors taxes HT @endif
                        </td>
                    </tr>
                </tbody>
            </table>
    </div>
    <!--
        <div class="page-break"></div>
        <br/>
    -->
<!--    <div class="page-break"></div>-->
    <br/>
        @if($lead->lang === "en")
            <strong>4. Payment schedule:</strong><br/>
            Payment will be required at the end of each service provided.<br/>
            <strong>5. Refund Policy:</strong><br/>
            The Client acknowledges that obtaining a visa or status, as well as the processing time for such application, is at the sole discretion of the government and not the RCIC. Furthermore, the Client acknowledges that the fees are non-refundable if the request is rejected.<br/>
            However, if the RCIC or its staff do not perform the tasks mentioned in section 2 of this contract, the RCIC will reimburse the fees charged, in part or in full. The Client agrees that the fees will be paid for the services mentioned above and that any refund will only apply to the amount of fees paid. Unused fees will be refunded as follows:
            <ul><li>Initial payment method within a maximum period of 45 days.</li></ul>
            If the Client has reason to believe that the RCIC is deceased, incapacitated, etc., the Client should contact the ICCRC.<sup>1</sup>
            <br/>
            <strong>6. Resolution of controversies related to the Code of Professional Ethics:</strong><br/>
            In the event of a conflict related to the <u>Code of Professional Ethics</u>, The Client and the RCIC must do everything possible
            to resolve the problem between the two parties. If the conflict cannot be resolved, the Client must submit a written complaint to the RCIC and give the RCIC
            <u>45 days</u> to respond to the Client. If the conflict is still not resolved, the Client can follow the complaints and discipline process described on the Council’s  <u>website</u><sup>2</sup> at the available section «<u>Contact us</u>»<sup>3</sup>.
            {{-- @if($quote->customer->program_id == 4) REMARQUE : tous les formulaires de plainte doivent être signés.<br/> @endif --}}
            <br/>
            <strong>7. Confidentiality:</strong><br/>
            All information and documents reviewed by RCIC, required by IRCC and all other governing bodies, and used in preparing the application, will not be disclosed to any third party, except agents and employees, without prior consent. except if required by law. RCIC, and all its agents and employees, are also subject to the confidentiality requirements of Article 8 of the Code of Professional Ethics.<br>
            The Client consents that the communications are carried out electronically and the storage of confidential information. The RCIC will make every effort to ensure a high level of security in electronic communications and information storage.<br>
            {{-- needs page break --}}
        @else
            <strong>4. Échéancier des paiements</strong><br/>
            Le paiement sera exigé à la fin de chaque service rendu.<br/>
            <strong>5. Politique de remboursement</strong><br/>
            Le client reconnaît que l’obtention d’un visa ou d’un statut ainsi que le temps de traitement de
            cette demande est à l’entière discrétion<br/>
            du gouvernement et non du CRIC. En outre, le client reconnaît que les honoraires ne sont pas
            remboursables s’il
            advenait que la demande soit refusée.<br/>
            Cependant, si le CRIC ou son personnel ne remplissent pas les tâches mentionnées
            à la section 2 du présent contrat, le CRIC remboursera les
            honoraires perçus, en partie ou en totalité.
            Le client consent à ce que les honoraires soient
            payés pour les services mentionnés ci-dessus et que
            tout remboursement s’applique uniquement au montant des honoraires payés. Les frais non
            utilisés seront remboursés comme suit :
            <ul><li>Méthode de paiement initial dans un délai maximum de 45 jours.</li></ul>
            Si le client a lieu de croire que le CRIC est décédé, frappé d’incapacité, etc., le client devra
            communiquer avec le CRIC.
            <br/>
            <strong>6. Résolution de conflits relatifs au Code d’éthique professionnelle</strong><br/>
            Dans le cas d’un conflit relatif au <u>Code d’éthique professionnelle</u>, le client et le CRIC doivent
            s’efforcer autant que
            possible de résoudre le problème entre les deux parties. Si le conflit ne peut pas être réglé, le
            client doit présenter une plainte par écrit au CRIC et donner au CRIC <u>45</u> jours pour répondre au
            client. Si le conflit n’est toujours pas réglé, le client peut suivre le processus de plainte et de
            discipline décrit sur le <u>site Web</u> du Conseil dans la section <br/><u>«Déposer une plainte.»</u>
            <br/>
            <strong>7. Confidentialité</strong><br/>
            Tous les renseignements et les documents examinés par le CRIC, requis par IRCC et tous les
            autres corps dirigeants, et
            utilisés pour préparer la demande ne seront pas divulgués à un tiers, autre qu’à des agents et
            des employés, sans consentement préalable, sauf si requis par la loi. Le CRIC, et tous ses
            agents et employés, sont aussi liés par les exigences de
            confidentialité de l’article 8 du <u>Code d’éthique professionnelle</u>. Le client consent à ce que les
            communications aient lieu par voie électronique et au stockage de l’information confidentielle.
            @if($quote->customer->program_id == 4) REMARQUE : tous les formulaires de plainte doivent être signés.<br/> @endif
        @endif

    <div class="text-muted" style="font-size:0.7em;display:block;position:absolute;top:80%;left:25px;width:350px;">
        <div style="border:3px solid grey;"></div>
        @if($lead->lang === "en")
            <span>
                1. Immigration Consultants of Canada Regulatory Council (ICCRC)<br/>
                5500 North Service Rd., bureau 1002<br/>
                Burlington (Ontario) L7L 6W6<br/>
                Sans frais : 1-877-836-754<br/>
            </span>
            <span>2. https://iccrc-crcic.ca </span><br/>
            <span>3. https://iccrc-crcic.ca/contact-us</span><br/>
            {{-- <span>4. http://fr.iccrc-crcic.ca/admin/contentEngine/contentImages/<br/> file/Complaint_Form_against_an_RCIC__modified26March12FR.pdf</span> --}}
        @else
            <span>
                1. Conseil de réglementation des consultants en immigration du Canada (CRCIC)<br/>
                5500 North Service Rd., bureau 1002<br/>
                Burlington (Ontario) L7L 6W6<br/>
                Sans frais : 1-877-836-754<br/>
            </span>
            <span>2. Tous les formulaires de plainte doivent être signés.</span><br/>
            <span>3. http://iccrc-crcic.info/fr/</span><br/>
            <span>4. http://fr.iccrc-crcic.ca/admin/contentEngine/contentImages/<br/> file/Complaint_Form_against_an_RCIC__modified26March12FR.pdf</span>
        @endif
    </div>
    <div class="page-break"></div>
    <br>
    @if($lead->lang === "en")
        <strong>8. Force majeure:</strong><br/>
        In the event that RCIC is unable to comply with any clause of this professional service contract, due to conditions beyond its control such as, without excluding others, government restrictions or subsequent laws, wars, strikes or natural disasters, a breach of this contract will not be deemed to exist.<br/>
        <strong>9. Exchange policy:</strong><br/>
        The Client agrees that, due to a request made to RCIC to represent it in a matter other than the one mentioned above in this contract, a change in its material situation, material facts not disclosed at the beginning of the request or a change in government law regarding, upon processing of immigration or citizenship applications, the contract may be modified accordingly.<br/>
        <strong>10. Fin del contrato:</strong>
        <ul style="list-style:none;padding: 0;list-style-type: none;">
            <li>
                <b>10.1</b> This contract ends when the tasks specified in section 2 of this contract have been carried out.
            </li>
            <li>
                <b>10.2</b> This contract ends if material changes occur in the Client's application or eligibility, and that prevent the performance of the tasks described in section 2 of this contract.
            </li>
        </ul>
        <strong>11. Release of obligations or withdrawal from a business:</strong>
        <ul style="list-style:none;padding: 0;list-style-type: none;margin-bottom:5px;">
            <li>
                <b>11.1</b> The Client may release the RCIC from its obligations and terminate the contract in writing. RCIC will pay unpaid fees or disbursements to the Client / the Client will pay unpaid fees or disbursements to RCIC.<br/>
            </li>
            <li>
                <b>11.2</b> Pursuant to article 11 of the Code of Professional Ethics, the RCIC may withdraw from a case and terminate the contract in writing as long as this does not cause harm to the Client. RCIC will pay unpaid fees or disbursements to the Client / the Client will pay unpaid fees or disbursements to RCIC.
            </li>
        </ul>
        <strong>12. Contract law:</strong><br/>
        This contract is governed by the laws in force of the province / territory of Quebec, and the Canadian federal laws that apply to it and,
        except for the conflicts provided in section 6 of this document,
        any conflict related to the clauses of this contract will be resolved by a competent court of the province / territory of Quebec.<br>
        <strong>13. Clauses:</strong>
        <ul style="list-style:none;padding: 0;list-style-type: none;">
            <li>
                <b>13.1</b> The Client expressly authorizes the RCIC to act on his behalf within the limits of the precise functions for which the services of the RCIC have been contracted, in accordance with paragraph 2 hereof.
            </li>
            <li>
                <b>13.2</b> This contract constitutes the entire contract between the parties with respect to the opposite object and supersedes all contracts, all agreements, all guarantees, representations, negotiations and discussions, verbal or written, of the parties, except as specifically established in this document.
            </li>
            <li>
                <b>13.3</b> This contract is binding on the parties hereto and their respective heirs, administrators, successors and authorized assignees.
            </li>
            <li>
                <b>13.4</b> This contract can only be modified if the changes are made in writing and signed by both parties.
            </li>
            <li>
                <b>13.5</b> The provisions of this contract are considered divisible; If any provision of this contract is found unenforceable by a court of competent jurisdiction, the provision will be removed from this contract and the other provisions will remain in effect.
            </li>
            <br/>
            <br/>
            <br/>
            <li>
                <b>13.6</b> The titles used in this contract are intended solely for ease of reading and should in no way be construed as additions or limitations to the covenants and agreements contained in this contract.
            </li>
            <li>
                <b>13.7</b> Each present party makes, establishes, gives, signs or causes to be carried out, established, given or signed all other things, guarantees and all acts, transfer deeds and documents that are considered necessary or reasonably required for the intention and the purpose of this contract is fully and effectively implemented.
            </li>
            <li>
                <b>13.8</b> The Client agrees that he has been given sufficient time to review this contract and that he has been given the opportunity to obtain independent legal advice and translation prior to the signing and delivery of this contract. If the Client has not sought independent legal advice before signing and delivering this contract, he has done so on his own without undue pressure and agrees that the lack of independent legal advice cannot be used as a defense to enforce the obligations created in this contract.
            </li>
            <li>
                <b>13.9</b> Furthermore, the Client acknowledges having received a copy of this contract and agrees to be bound by its conditions.
            </li>
            <li>
                <b>13.10</b> The Client who wishes to fulfill this contract in English must request it.
            </li>
        </ul>
        <strong>14. Contact information:</strong><br/>
    @else
        Le CRIC s’efforcera
        autant que possible d’assurer un niveau de sécurité élevé en matière de communication
        électronique et de stockage de l’information.<br>
        <strong>8. Force majeure</strong><br/>
        En cas d’incapacité du CRIC à respecter toute clause de ce contrat de service professionnel,
        en raison de conditions hors de son
        contrôle telles que, sans en exclure d’autres, des restrictions gouvernementales ou des lois
        postérieures, une guerre, des grèves ou des catastrophes
        naturelles, on ne considérera pas qu’il y a rupture de ce contrat.<br/>
        <strong>9. Politique de changement</strong><br/>
        Le client convient qu’en raison d’une demande faite au CRIC de le représenter dans une affaire
        autre que celle mentionnée ci- dessus dans ce contrat, d’un changement de sa situation
        matérielle, de faits matériels non divulgués au début de la demande ou d’un changement
        dans la loi du gouvernement concernant le traitement des demandes en matière d’immigration
        ou de citoyenneté, le contrat peut être modifié en conséquence.<br/>
        <strong>10. Fin du contrat</strong>
        <ul style="list-style:none;padding: 0;list-style-type: none;">
            <li>
                <b>10.1</b> Ce contrat prend fin lorsque les tâches précisées à la section 2 de ce contrat
                ont été exécutées.
            </li>
            <li>
                <b>10.2</b> Ce contrat prend fin si des changements d’ordre matériel se produisent dans la demande
                ou l’admissibilité du client, et qui empêchent
                d’exécuter les tâches décrites à la section 2 de ce contrat.
            </li>
        </ul>
        <strong>11. Libération des obligations ou retrait d’une affaire</strong>
        <ul style="list-style:none;padding: 0;list-style-type: none;margin-bottom:5px;">
            <li>
                <b>11.1</b> Le client peut libérer le CRIC de ses obligations et mettre fin au contrat par écrit.<br/>
                Les honoraires ou débours
                impayés seront payés par le CRIC au client / les honoraires ou débours impayés seront payés
                par le client au CRIC.
            </li>
            <li>
                <b>11.2</b> En vertu de l’article 11 du Code d’éthique professionnelle, le CRIC peut se retirer d’une affaire
                et mettre fin au contrat par écrit pourvu que cela ne cause pas de préjudice au client
                Les honoraires ou débours impayés seront
                payés par le CRIC au client / les honoraires ou débours impayés seront payés par le client
                au CRIC.
            </li>
        </ul>
        <strong>12. Loi du contrat</strong><br/>
        Ce contrat est régi par les lois en vigueur de la Province / du Territoire <u>Québec</u> , et les lois
        fédérales canadiennes qui s’y appliquent et, sauf pour des conflits prévus à la section @if($quote->customer->program_id == 4) 8 @else 6 @endif aux
        présentes, tout conflit concernant les clauses de ce contrat sera réglé par un tribunal compétent de la Province / du Territoire <u>Québec</u>.<br>
        <strong>13. Divers</strong>
        <ul style="list-style:none;padding: 0;list-style-type: none;">
            <li>
                <b>13.1</b> Le client autorise explicitement le CRIC à agir en son nom dans les limites des fonctions
                précises pour lesquelles les services du CRIC ont été retenus, conformément au paragraphe 2 des présentes.
            </li>
            <li>
                <b>13.2</b> Ce contrat constitue la totalité du contrat entre les parties pour ce qui est de l’objet
                ci-contre et remplace tous les contrats, tous les
                accords, toutes les garanties, représentations, négociations et discussions précédents,
                verbaux ou écrits, des parties sauf pour ce qui est
                indiqué précisément aux présentes.
            </li>
            <li>
                <b>13.3</b> Ce contrat lie les parties aux présentes et leurs héritiers, administrateurs, successeurs
                et ayants droit autorisés respectifs.
            </li>
            <li>
                <b>13.4</b> Ce contrat peut uniquement être modifié si les changements sont effectués par écrit
                et signés par les parties aux présentes.
            </li>
            <br/>
            <br/>
            <li>
                <b>13.5</b> Les dispositions de ce contrat sont jugées dissociables. Si une disposition de ce contrat
                est considérée comme inexécutable par un
                tribunal compétent, la disposition sera supprimée de ce contrat, et les autres dispositions
                demeureront en vigueur.
            </li>
            <li>
                <b>13.6</b> Les titres utilisés dans ce contrat visent seulement à faciliter la lecture et ne doivent en
                aucun cas être interprétés comme des ajouts ou des limitations des engagements et ententes
                contenus dans ce contrat.
            </li>
            <li>
                <b>13.7</b> Chaque partie aux présentes fait, établit, donne, signe ou fait faire, établir, donner
                ou signer toutes les autres choses, les assurances et
                tous les actes, actes de transfert et documents jugés nécessaires ou raisonnablement requis
                pour que l’intention et l’objet de ce contrat
                soient pleinement et effectivement appliqués.
            </li>
            <li>
                <b>13.8</b> Le client convient qu’il a eu suffisamment de temps pour examiner ce contrat et qu’on lui a
                donné la possibilité d’obtenir un avis
                juridique indépendant et une traduction avant la signature et la remise de ce contrat. Si le
                client n’a pas demandé un avis juridique
                indépendant avant la signature et la remise de ce contrat, il l’a fait de son propre chef sans
                subir de pression indue et convient que le fait de ne pas avoir obtenu d’avis juridique
                indépendant ne pourra pas être utilisé comme moyen de
                défense pour faire respecter les obligations créées dans ce contrat.
            </li>
            <li>
                <b>13.9</b> En outre, le client reconnaît avoir reçu une copie de ce contrat et convient être lié par
                ses conditions.
            </li>
            <li>
                <b>13.10</b> Le client qui souhaite remplir ce contrat en anglais doit en faire la demande.
            </li>
        </ul>
        <strong>14. Coordonnées</strong><br/>
    @endif

    @if($lead->lang === "en")
        <ul style="list-style:none;padding: 0;list-style-type: none;">
            <li style="line-height:0.95em;">
                <b>CLIENT:</b><br>
                Name: <strong>{{ $quote->customer->firstname }}</strong><br>
                Surename: <strong>{{ $quote->customer->lastname }}</strong><br>
                @php $leadd = \App\Models\Lead::where("id", $quote->customer->lead_id)->first(); @endphp
                @if(!empty($leadd->adresse_line_1))
                    Address:  <strong>{{ $leadd->adresse_line_1 }}</strong><br/>
                @endif
                Telephone number: <strong>{{ $quote->customer->phone }}</strong> <br />
                E-mail: : <strong>{{ $quote->customer->email }}</strong>
            </li>
            <li style="line-height:0.95em;margin-top:15px;">
                <b>RCIC :</b><br>
                Name: <strong>Ahmed</strong><br>
                Surename: <strong>Bahajoub</strong><br>
                Address <strong>1285 Allan Laval, Quebec, Canada </strong><br>
                Telephone number : <strong>+15148153614</strong> <br/>
                E-mail : <strong>clearpathcanada@gmail.com</strong><br/><br/><br/>
                <span>
                    IN ACCORDANCE with what is stated in this contract, it has been duly signed by the parties on the date indicated at the beginning of this contract.
                </span>
            </li>
        </ul>
        <br/>
        <br/>
    @else
        <ul style="list-style:none;padding: 0;list-style-type: none;">
            <li style="line-height:0.95em;">
                <b>Client :</b><br>
                <strong>{{ $quote->customer->firstname }} {{ $quote->customer->lastname }}</strong><br />
                @php $leadd = \App\Models\Lead::where("id", $quote->customer->lead_id)->first(); @endphp
                @if(!empty($leadd->adresse_line_1))
                    Adresse <strong>{{ $leadd->adresse_line_1 }}</strong><br/>
                @endif
                Numéro de téléphone : <strong>{{ $quote->customer->phone }}</strong> <br />
                Adresse courriel : <strong>{{ $quote->customer->email }}</strong>
            </li>
            <li style="line-height:0.95em;margin-top:15px;">
                <b>CRIC :</b><br>
                <strong>Ahmed Bahajoub</strong><br/>
                Adresse <strong>1285 Allan Laval, Quebec, Canada </strong><br>
                Numéro de téléphone : <strong>+15148153614</strong> <br/>
                Adresse courriel : <strong>clearpathcanada@gmail.com</strong><br/><br/>
                <span>
                    EN FOI DE QUOI ce contrat a été dûment signé par les parties aux présentes
                    à la date indiquée au début du présent contrat.
                </span>
            </li>
        </ul>
    @endif
<!-- Prénom <strong>{{ $quote->customer->firstname }}</strong> Nom <strong>{{ $quote->customer->lastname }}</strong> Adresse <strong>{{ \App\Models\Lead::where("id", $quote->customer->lead_id)->first()->adresse_line_1 }}</strong><br/>
            <strong>Numéro de téléphone :</strong> +{{ $quote->customer->phone }} <strong>Adresse courriel :</strong> {{ $quote->customer->email }}
    EN FOI DE QUOI ce contrat a été dûment signé par les parties aux présentes
    à la date indiquée au début du présent contrat. -->
    </p>
</main>
<img style="width: 170px;position:absolute;top:85%;right:8%;" src="{{ public_path('/img/signature.png') }}" alt="">
<img style="width: 200px;position:absolute;top:70%;right:25%;" src="{{ public_path('/img/cachett.png') }}" alt="">
<span style="margin: 10px 50px;">
    @if($lead->lang === "en")
        <div style="display:inline;position:absolute;left:50px;"><b>Client’s signature</b><br><br><br><p>{signature:signer____}</p></div>
        <div style="display:inline;position:absolute;right:50px;"><b>RCIC’s signature</b></div>
    @else
        <div style="display:inline;position:absolute;left:50px;"><b>Signature du client</b><br><br><br><p>{signature:signer____}</p></div>
        <div style="display:inline;position:absolute;right:50px;"><b>Signature du CRIC</b></div>
    @endif
    </span>
<!-- <div class="page-break"></div> -->
</body>
</html>
