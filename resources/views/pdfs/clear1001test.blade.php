<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Invoice</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,500;0,700;1,200;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <style>
    @page {
        size: a4 360pt;
        /* size: a4 landscape; */
        margin:0;
        padding:0;
    }
    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100&display=swap');
    body {
        /* font-family: 'Poppins-Light', Fallback, sans-serif; */
        font-family: 'Poppins', sans-serif;
        font-size: 17px;
        /* text-align: center; */
        /* border: thin solid black;   */
        /* font-family: "Poppins", sans-serif; */
        background: url("{{ public_path('/img/background_devis.png') }}");
        background-size: 350px;
        background-repeat: no-repeat;
        background-position: 100% 100%;
        margin:0;
    }
    .page-break {
        page-break-before: always;
    }
    .head-info {
        list-style:none;
        padding:0;
        margin:0;
    }
    .head-info li{
        background: #c0c9d2;
        color: black;
        margin: 5px 0;
    }
    .head-r{
        display:inline;
        font-size:0.6em;
        width:30%;
        position:absolute;
        right:20px;
        top:10px;
    }
    .head-l {
        position:absolute;
        top:40px;
        left:20px;
    }
    .qs{
        width: 100%;
        border-collapse: separate;
        border-spacing:10px;
        /* margin: auto 15px; */
    }
    .qs tr td:first-child {
        line-height: 0.6em;
        width: 35%;
    }
    .qs td,.qss td{
        border: 2px dashed grey;
        padding: 15px 25px;
    }
    .ul-qs li {
        margin: 15px 20px;
    }
    .qss {
        width: 35%;
    }
    </style>
    </head>
<body>
    <header style="position:relative;">
        <span class="head-l">
            <img class="image" style="width: 140px;" src="{{ public_path('/img/logo.png') }}" alt="">
        </span>
        <div class="text-muted head-r">
            <b>Rapport d’Entretien Client</b>
            <p>
                Date de création: 07/09/2021<br>
                {{ $customer->firstname }} {{ $customer->lastname }}<br>
                @php $conseiller = \App\Models\User::where('id', $customer->lead->conseiller_id)->first(); @endphp
                {{ $conseiller->name }}
            </p>
            <ul class="head-info">
                <li>
                    Numero de dossier client :  {{ $customer->phone }}
                </li>
                <li>
                    Adresse mail : {{ $customer->email }}
                </li>
                <li>
                    Téléphone mobile : {{ $customer->phone }}
                </li>
            </ul>
        </div>
    </header>
    <br />
    <!-- <div style="display;block;width:100%;margin: 0 50px;margin-top:4cm;margin-bottom:0;">
        <div style="display:inline-block;">QUESTIONS</div>
        <div style="display:inline-block;float:right;">CONSEILS</div>
    </div> -->
    @php
        $groupedResultsArray = [];
        $form = \App\Models\InformationSheet::where('name_fr', 'Clear1000 - Questionnaire')->first();
        $customer_case = \App\Models\CustomerCase::where('customer_id', $customer->id)->first();
    @endphp
    @foreach($form->fields()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get() as $field)
        @php
            $groupedResultsArray[$field->customer_field_group_id][] = $field;
        @endphp
    @endforeach
    @php
        $groupsSorted = \App\Models\CustomerFieldGroup::orderBy('order')->pluck('id');
        $groupedRemarksArray = [];
    @endphp

    @foreach(App\Models\Remarks::whereNotNull("remark_group_id")->get() as $remark)
        @php $groupedRemarksArray[$remark->remark_group_id][] = $remark; @endphp
    @endforeach
    <div style="display;block;width:100%;font-size:0.8em;margin-top:4cm;">
        <ul style='list-style:none;padding:0;margin:10px 20px;'><li>
            <table style="color:red;width: 100%;">
                <tr>
                    <th style="width: 35%;">QUESTIONS</th>
                    <th>CONSEILS</th>
                </tr>
            </table>
        </li></ul>
        @foreach($groupsSorted as $key)
            @if(array_key_exists($key, $groupedResultsArray))
                @if(!array_key_exists($key, $groupedRemarksArray))
                    <ul class="ul-qs" style="list-style:none;padding:0;">
                    @foreach($groupedResultsArray[$key] as $field)
                        @php 
                            $field_name = $field->field_name;
                            $customer_ = App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first(); 
                            
                            $remark_id = \App\Models\CustomerFieldRemark::where('customer_id', $customer_case->customer->id)->where('field_id',$field->id)->first()->remark_id;
                            $remark = \App\Models\Remarks::where('id', $remark_id)->first();
                        @endphp
                        <li>
                            @if($loop->first)
                                <b>{{ $field->group->name_fr }}</b>
                            @endif
                            <table class="qs">
                                <tr>
                                    <td style="width: 35%;margin-right:20px;">
                                        {{ $field_name }}<br/>
                                        @if($customer_ && $field->select_model) 
                                            @php
                                                $model = $field->select_model::where($field->select_model_id, $customer_->$field_name)->pluck($field->select_model_label, $field->select_model_id)->toArray();
                                            @endphp
                                            <b>{{ $model[$customer_->$field_name] }}</b>
                                        @elseif($customer_)
                                            <b>{{ $customer_->$field_name }}</b>
                                        @endif
                                    </td>
                                    <td style="background:#dad1d1;">{{ $remark->conseill }}</td>
                                </tr>
                            </table>
                        </li>
                    @endforeach
                    </ul>
                @else
                    <ul class="ul-qs" style="list-style:none;padding:0;">
                        <li>
                            <table style="width:100%;">
                                <tr style="border: 1px solid red;">
                                    <td style="width:35%%;margin-right:20px;display: table-cell;">
                                        <table class="qs" style="margin:0;padding:0;">
                                        @foreach($groupedResultsArray[$key] as $field)
                                            @php 
                                                $field_name = $field->field_name;
                                                $customer_ = App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first(); 
                                            @endphp
                                            <tr>
                                                <td>
                                                    {{ $field_name }}<br/>
                                                    @if($customer_ && $field->select_model) 
                                                        @php
                                                            $model = $field->select_model::where($field->select_model_id, $customer_->$field_name)->pluck($field->select_model_label, $field->select_model_id)->toArray();
                                                        @endphp
                                                        <b>{{ isset($model[$customer_->$field_name]) ? $customer_->$field_name: '---' }}</b>
                                                    @elseif($customer_)
                                                        <b>{{ $customer_->$field_name }}</b>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </table>
                                    </td>
                                    <td style="background:#dad1d1;border: 1px dashed gray;padding:20px;width:66%;display: table-cell;">
                                        <p>
                                            @foreach($groupedRemarksArray[$key] as $remark)
                                                @if (isset($remark->code) && !is_null($remark->code))
                                                    <!-- @php \App\Models\Remarks::where('field_name', $field->field_name)->where('code', $remark->code); @endphp -->
                                                    @php $tt = \App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first()->training_type; @endphp
                                                    @if($remark->code == $tt)
                                                        {{$remark->conseill}} <br />
                                                    @endif
                                                @else
                                                    {{$remark->conseill}} <br />
                                                @endif
                                            @endforeach
                                        <p>
                                    <td>
                                </tr>
                            </table>
                            <!-- <div style="border: 1px dashed grey;display:inline-block;float:right;with:60%;">
                                
                            </div> -->
                        </li>
                    </ul>
                @endif
            @endif
        @endforeach
    </div>
    
    <img style="width: 200px;position:absolute;top:65%;right:25%;" src="{{ public_path('/img/cachett.png') }}" alt="">
    <!-- <div class="page-break"></div> -->
</body>
</html>