<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Invoice</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="{{ public_path('/css/bootstrap.min.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,500;0,700;1,200;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <style>

        @page { margin: 0in;size: 700pt 1020pt; }
        body {
            font-family: 'Poppins', sans-serif;
            font-size: 14px;
            background-size: 350px;
            background-repeat: no-repeat;
            background-position: 100% 100%;
            margin:20px;
            /* margin-bottom:50px; */
        }
        .page-break {
            page-break-before: always;
        }
        .head-info {
            list-style:none;
            padding:0;
            margin:0;
        }
        p{
            margin: 0;
        }
        .head-info li{
            background: #c0c9d2;
            color: black;
            margin: 5px 0;
        }
        .head-r{
            display:inline;
            font-size:0.6em;
            width:30%;
            position:absolute;
            right:20px;
            top:10px;
        }
        .head-l {
            display: flex;
            justify-content: center;
        }
        .table-fields {
            border-collapse: separate;
            border-spacing:10px;
        }
        .table-fields td {
            padding: 2px 2px;
            line-height: 0.3;
        }
        .table-fields tr td:first-child{
            width: 50%;
        }

        .sectionContainer{
            border: 1px solid #ff2929;
            border-radius: 7px;
            width: 97%;
            margin: 0;
            padding: 5px 12px 5px 12px;
        }


        .bold{
            font-family: 'Poppins', serif;
            font-weight: 900;
        }
        .title{
            font-family: 'Poppins', serif;
            color: #ff2929;
            font-size: 18px;
            font-weight: bold;
            /*text-transform: capitalize;*/
            text-align: center;
        }

        .section_Container{
            border: 1px solid #ff2929;
            border-radius: 7px;
            width: 97%;
            margin: 0;
            padding: 5px 12px 5px 12px;
        }
        .sectionContainer p{
            line-height: 0.9em;
        }
        .text-normal {
            font-size: 12px;
            font-weight: 300;
            line-height: 0.7em;
        }
        .section{
            width: 95%;
            margin: auto;
            padding: 12px;
        }
        .big-title {
            font-family: 'Poppins', serif !important;
            font-size: 28px;
            font-weight: bold;
            text-align: center;
            color: #0b0b0b;
        }
    </style>
    </head>
<body>
@php
    $conseiller = \App\Models\User::where('id', $customer->lead->conseiller_id)->first();

    function safeCheck($to_look_for, $to_look_in)
    {
        if (array_key_exists('key', $to_look_in[$to_look_for]) && array_key_exists('value', $to_look_in[$to_look_for])) {
            return $to_look_in[$to_look_for]["value"];
        } else {
            if (count($to_look_in[$to_look_for]["values"])) return $to_look_in[$to_look_for]["values"][0];
            return "";
        }
        return "";
    }

@endphp

<div class="sectionContainer" style="background-color: #fff8f8;margin-top: 5px;">
    <div style="width: 100%;text-align: center;">
        <img class="center" style="width: 190px;" src="{{ public_path('/img/logo-fr.png') }}" alt="">
    </div>
</div>
<div class="sectionContainer" style="background-color: #ff2929;margin-top: 5px;">
    <h1 class="big-title" style="margin-top: 40px;margin-bottom: 20px;line-height: 0;color: white">Clear 1002 : Rapport d’orientation client</h1>
</div>
<br>
<div class="sectionContainer" style="margin-top: 0;">
    <p>
        @php $meet = \App\Models\Calendrier::where('customer_id', $customer->id)->where('title', 'LIKE', 'Rencontre 2%')->orWhere('title', 'LIKE', 'R2 %')->get()->last(); @endphp
        Date de rencontre : <strong> {{ $meet ? $meet->created_at: '-' }} </strong></p>
    <p>Nom du conseiller Clear Path Canada : <strong> {{ $conseiller->name }}</strong></p>
    <p>Numéro de téléphone du conseiller : <strong> {{$conseiller->phone}}</strong></p>
    <p>Nom & prénom du client : <strong> {{ $customer->firstname }} {{ $customer->lastname }}</strong></p>
    <p>Numéro de téléphone du client :<strong> {{ $customer->phone }} </strong></p>
</div>
        @foreach($customer->courses()->get() as $program)
            @php
                $program = $program->toArray();
            @endphp
            <h2 class="title">
                Choix {{ $loop->index + 1 }} : <strong>
                    {{ safeCheck("diplome", $program["properties"]) }}
                    {{ safeCheck("domaine_etude", $program["properties"]) }}
                </strong>
            </h2>
            <div class="sectionContainer">
                <table class="table-fields" style="width: 100%; padding-bottom: 10px;">
                    <tr>
                        <td><strong>Type d'établissement : </strong>
                            {{ safeCheck("type_etablissement", $program["properties"]) }}
                        </td>
                        <td><strong>Session : </strong>
                            {{ safeCheck("session", $program["properties"]) }}
                        </td>
                    </tr>
                    <tr>
                        <td><p><strong>Nom de l'établissement : </strong>
                            {{ safeCheck("nom_etablissement", $program["properties"]) }}
                        </p></td>
                        @php
                            $months = array("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");
                        @endphp
                        <td><strong>Date limite d'admission : </strong>
                            {{ array_key_exists('key', $program["properties"]["date_limit_admission"]) && array_key_exists('value', $program["properties"]["date_limit_admission"]) && !empty($program["properties"]["date_limit_admission"]["value"])
                            ? "Le " . $program["properties"]["date_limit_admission"]["value"]["day"] . ucfirst($months[intval($program["properties"]["date_limit_admission"]["value"]["month"]) - 1])
                            : ""
                            }}
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Ville : </strong>
                            {{ safeCheck("ville", $program["properties"]) }}
                        </td>
                    </tr>
                </table>
                <table class="table-fields" style="width: 100%; padding-bottom: 10px;">
                    <tr>
                        <td><strong>Diplôme : </strong>
                            {{ safeCheck("diplome", $program["properties"]) }}
                        </td>
                        <td><strong>Langue : </strong>
                            {{ safeCheck("langue", $program["properties"]) }}
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Domaine d'études : </strong>
                            {{ safeCheck("domaine_etude", $program["properties"]) }}
                        </td>
                        <td><strong>Durée : </strong>
                            {{ safeCheck("duree", $program["properties"]) }}
                        </td>
                    </tr>
                    <tr>
                        <td><p><strong>Programme : </strong>
                            {{ $program["name"] }}
                        </p></td>
                        <td><strong>Frais d’admission : </strong>
                            {{ safeCheck("frais_admission", $program["properties"]) }}
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Concentration : </strong>
                            {{ safeCheck("concentration", $program["properties"]) }}
                        </td>
                        <td><strong>Frais de scolarité par année : </strong>
                            {{ safeCheck("prix_annuel", $program["properties"]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Lien : </strong>
                            <p style="width: 210px;overflow-wrap: break-word;line-height: 15px;">
                                {{ safeCheck("lien", $program["properties"]) }}
                            </p>
                        </td>
                        <td><strong>Total des frais de scolarité : </strong>
                            {{ safeCheck("prix_total", $program["properties"]) }}
                        </td>
                    </tr>
                </table>
                <table class="table-fields" style="width: 100%; padding-bottom: 10px;">
                    <tr>
                        <td style="line-height: normal;"><strong>Exigences : </strong>
                            {{ safeCheck("exigence", $program["properties"]) }}
                        </td>
                        <td><strong>Objectifs de formation : </strong></td>
                    </tr>
                </table>
                <table class="table-fields" style="width: 100%; padding-bottom: 10px;">
                    <tr>
                        <td><strong>Test de langue : </strong>
                            {{ safeCheck("test_langue", $program["properties"]) }}
                        </td>
                        <td><strong>Débouchés du diplôme : </strong></td>
                    </tr>
                </table>
                <table class="table-fields" style="width: 100%; padding-bottom: 10px;">
                    <tr>
                        <td><strong>Score :</strong>
                            {{ safeCheck("score", $program["properties"]) }}
                        </td>
                    </tr>
                </table>
            </div>
            @if($loop->last)
                @break
            @endif
            @if(($loop->index + 1) % 2 == 0)
                <div class="page-break"></div>
            @endif
        @endforeach
    <img style="width: 180px;position:absolute;top:85%;right:5%;" src="{{ public_path('/img/cachett.png') }}" alt="">
    <!---Todo : page break after 2 choice programme-->
    <!-- <div class="page-break"></div> -->
</body>
</html>
