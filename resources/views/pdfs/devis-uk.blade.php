<?php
$currency = $customer->currency_id ? \App\Models\Currency::where('id', $customer->currency_id)->first()->iso : 'MAD';
$lead = \App\Models\Lead::where('id', $customer->lead_id)->first();
?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Invoice</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">-->
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,500;0,700;1,200;1,300;1,400;1,700;1,900&display=swap"
        rel="stylesheet">
    <style>
        @page {
            margin: 0in;
            size: 700pt 1020pt;
        }

        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100&display=swap');

        * {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: 'Poppins', sans-serif;
            font-size: 16px;
            background: url("{{ public_path('/img/background_devis.png') }}");
            background-size: 350px;
            background-repeat: no-repeat;
            background-position: 100% 100%;
        }

        .head-info li {
            background: #c0c9d2;
            color: black;
            margin: 5px 0;
        }

        .page-break {
            page-break-before: always;
        }

        .sectionContainer {
            /*border: 1px solid #ff2929;*/
            border-radius: 7px;
            width: 97%;
            margin: 0;
            padding: 5px 12px 5px 12px;
        }

        .sectionContainer p {
            font-family: 'Poppins', serif;
            font-weight: 300;
            line-height: 0.9em;

        }

        table {
            border-collapse: collapse;
        }

        table,
        th,
        td {
            border: 2px solid #F4CCCC;
            text-align: center;
        }

        .total-step {
            background-color: #F4CCCC;
            color: white;
        }
    </style>
</head>

<body>
    <div style="margin: 1.6em;">
        <div class="sectionContainer" style="text-align: center;">
            <img class="image" style="width: 140px;" src="{{ public_path('/img/Logo-CPSA.png') }}" alt="">
        </div>
        <div class="sectionContainer">
            <table>
                <thead>
                    <tr>
                        <th colspan="3" style="color: #FF0100;">
                            @if ($lead->lang === 'en')
                                Study in UK
                            @else
                                Étude en Royaume-Uni
                            @endif
                        </th>
                    </tr>
                    <tr class="total-step">
                        <th width="20%" style=""></th>
                        <th style="text-align:center;border-left: none;">Description</th>
                        <th width="10%">
                            @if ($lead->lang === 'en')
                                Amount
                            @else
                                Montants
                            @endif
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td rowspan='6'>
                            @if ($lead->lang === 'en')
                                Phase
                            @else
                                Etape
                            @endif 1:<br>
                            <span style="color: #FF0100;">
                                @if ($lead->lang === 'en')
                                    Data Analysis and Eligibility
                                @else
                                    Analyse des données et admissibilité
                                @endif
                            </span>
                        </td>
                        <td>
                            <p style="color: #FF0100; font-weight: 500;">
                                @if ($lead->lang === 'en')
                                    Meeting
                                @else
                                    Rencontre
                                    @endif 1 : @if ($lead->lang === 'en')
                                        Data Analysis and Eligibility
                                    @else
                                        Analyse des données et admissibilité
                                    @endif
                            </p>
                        </td>
                        <td class="unit" rowspan='6'>
                            @if ($currency === 'MAD')
                                200
                            @else
                                50
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Information collection and student questionnaire completion
                                @else
                                    Collecte des informations et remplir le questionnaire étudiant
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Clear 1001 information analysis and recommendations
                                @else
                                    Analyse des informations et recommandations Clear 1001
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Explanation of documents: Quote, contract
                                @else
                                    Explication des documents : Devis, contrat
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Access to the portal and customer area
                                @else
                                    Accès au portail et espace client
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Become a member of the Clear Path Study Abroad network to receive all the news
                                @else
                                    Devenir membre du réseau Clear Path Study Abroad pour recevoir toutes les nouvelles
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr class="total-step">
                        <td class="align-middle" colspan='2'>
                            <div class="text-muted">
                                Total @if ($lead->lang === 'en')
                                    Phase
                                @else
                                    étape
                                @endif 1
                            </div>
                        </td>
                        <td class="unit">
                            @if ($currency === 'MAD')
                                200
                            @else
                                50
                            @endif
                        </td>
                    </tr>
                    <!-- Etape 2-->
                    <tr>
                        <td rowspan='14'>
                            @if ($lead->lang === 'en')
                                Phase
                            @else
                                Etape
                            @endif 2:<br>
                            <span style="color: #FF0100;">
                                @if ($lead->lang === 'en')
                                    Development of the study project
                                @else
                                    Développement du projet d'études
                                @endif
                            </span>
                        </td>
                        <td>
                            <p style="color: #FF0100; font-weight: 500;">
                                @if ($lead->lang === 'en')
                                    Meeting
                                @else
                                    Rencontre
                                    @endif 2 : @if ($lead->lang === 'en')
                                        Preparation of the admission request
                                    @else
                                        Préparation de la demande d'admission
                                    @endif
                            </p>
                        </td>
                        <td class="unit" rowspan='7'>
                            @if ($currency === 'MAD')
                                2800
                            @else
                                280
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    File opening
                                @else
                                    Ouverture du dossier
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Explanation of the procedure: Clear 1, Clear 2 and Clear 3
                                @else
                                    Explication de la procédure : Clear 1, Clear 2 et Clear 3
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Accompaniment and supervision
                                @else
                                    Accompagnement et encadrement
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Development of the study project
                                @else
                                    Développement du projet d'études
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Preparation of the admission application
                                @else
                                    Préparation de la demande d'admission
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Orientation and proposals of Clear 1002 programs
                                @else
                                    Orientation et propositions des programmes Clear 1002
                                @endif
                            </div>
                        </td>
                    </tr>
                    <!--substep2-->
                    <tr>
                        <td>
                            <p style="color: #FF0100; font-weight: 500;">
                                @if ($lead->lang === 'en')
                                    Meeting
                                @else
                                    Rencontre
                                    @endif 3 : @if ($lead->lang === 'en')
                                        Admission application presentation
                                    @else
                                        Soumission de la demande d'admission
                                    @endif
                            </p>
                        </td>
                        <td class="unit" rowspan='7'>
                            @if ($currency === 'MAD')
                                7500
                            @else
                                750
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Verification of Clear 1, Clear 2 and Clear 3 documents
                                @else
                                    Vérification des documents Clear 1, Clear 2 et Clear 3
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Financial analysis of guarantors
                                @else
                                    Analyse Financière des garants
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Recommendation and financial solutions
                                @else
                                    Recommandation et solutions financières
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Confirmation and program choice
                                @else
                                    Confirmation et choix des programmes
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Admission application presentation
                                @else
                                    Soumission de la demande d'admission
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    College and university admissions fees
                                @else
                                    Frais des admissions collégiales et universitaires
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr class="total-step">
                        <td class="align-middle" colspan='2'>
                            <div class="text-muted">
                                Total @if ($lead->lang === 'en')
                                    Phase
                                @else
                                    étape
                                @endif 2
                            </div>
                        </td>
                        <td class="unit">
                            @if ($currency === 'MAD')
                                10300
                            @else
                                1030
                            @endif
                        </td>
                    </tr>
                    <!--Etape3-->
                    <tr>
                        <td rowspan='9'>
                            @if ($lead->lang === 'en')
                                Phase
                            @else
                                Etape
                            @endif 3:<br>
                            <span style="color: #FF0100;">
                                @if ($lead->lang === 'en')
                                    Application for Student Visa
                                @else
                                    Demande du Visa Étude
                                @endif
                            </span>
                        </td>
                        <td>
                            <p style="color: #FF0100; font-weight: 500;">
                                @if ($lead->lang === 'en')
                                    Meeting
                                @else
                                    Rencontre
                                    @endif 4 : @if ($lead->lang === 'en')
                                        Preparation of the Study Visa application
                                    @else
                                        Préparation de la demande du Visa Étude
                                    @endif
                            </p>
                        </td>
                        <td class="unit" rowspan='4'>
                            @if ($currency === 'MAD')
                                6000
                            @else
                                600
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Explanation of the procedure: Clear 5
                                @else
                                    Explication de la procédure : Clear 5
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Accompaniment and supervision
                                @else
                                    Accompagnement et encadrement
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Preparation of the Study Visa application
                                @else
                                    Préparation de la demande du Visa Étude
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="color: #FF0100; font-weight: 500;">
                                @if ($lead->lang === 'en')
                                    Meeting
                                @else
                                    Rencontre
                                    @endif 5 : @if ($lead->lang === 'en')
                                        Study Visa application presentation
                                    @else
                                        Soumission de la demande du Visa Étude
                                    @endif
                            </p>
                        </td>
                        <td class="unit" rowspan='4'>
                            @if ($currency === 'MAD')
                                6000
                            @else
                                600
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Document verification: Clear 5
                                @else
                                    Vérification des documents : Clear 5
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Signature of Study Visa forms
                                @else
                                    Signature des formulaires de Visa Étude
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Visa application and study permit presentation
                                @else
                                    Soumission de la demande de Visa Étude
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">
                            <div class="text-muted">
                                @if ($lead->lang === 'en')
                                    Study Visa fees
                                @else
                                    Frais de Visa Étude
                                @endif
                            </div>
                        </td>
                        <td class="unit">
                            @if ($currency === 'MAD')
                                6000
                            @else
                                600
                            @endif
                        </td>
                    </tr>
                    <tr class="total-step">
                        <td class="align-middle" colspan='2'>
                            <div class="text-muted">
                                Total @if ($lead->lang === 'en')
                                    Phase
                                @else
                                    étape
                                @endif 3
                            </div>
                        </td>
                        <td class="unit">
                            @if ($currency === 'MAD')
                                18000
                            @else
                                1800
                            @endif
                        </td>
                    </tr>
                    <tr style="color: #FF0100;">
                        <td colspan='2'>
                            <p style="font-weight: bold;">
                                Total*
                            </p>
                        </td>
                        <td class="unit">
                            @if ($currency === 'MAD')
                                28500
                            @else
                                2880
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <td class="float-right" style="color: #FF0100;" colspan='3'>
                            @if ($lead->lang === 'en')
                                * All amounts are exclusive of tax
                            @else
                                *Tous les montants mentionnés sur les devis sont Hors taxes HT
                            @endif
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
    <footer style="margin: 0 2.4em;border-left: #F4CCCC solid 2px;padding-left: 5px;">
        <strong>Agence Maroc</strong> : 42 Rue Bachir Laalej,<br />
        Casablanca, Maroc <br /><i class="fab fa-1x fa-whatsapp"></i> +212 7 02 05 09 09<br />
        <span style="font-size: 14px;">info@clearpathcanada.ca <br> www.clearpathcanada.ca</span> <br> IF :
        50152557 | ICE : 002741390000066 | RC : 496031
        </p>
    </footer>
</body>

</html>
