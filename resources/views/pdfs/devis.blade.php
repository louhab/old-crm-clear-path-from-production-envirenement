@extends('layouts.pdf')

@section('css_after')
    <style>
    @page {
        size: a4 360pt;
        /* size: a4 landscape; */
        margin:0;
        padding:0;
    }
    body {
        /* font-family: 'Poppins-Light', Fallback, sans-serif; */
        font-family: 'Poppins', sans-serif;
        font-size: 17px;
        /* text-align: center; */
        /* border: thin solid black;   */
        /* font-family: "Poppins", sans-serif; */
        background: url("{{ public_path('/img/background_devis.png') }}");
        background-size: 350px;
        background-repeat: no-repeat;
        background-position: 100% 100%;
        margin:0;
        margin-top:40px;
    }
    th, td {
        text-align:center;
    }
    .page-break {
        page-break-before: always;
    }
    </style>
@endsection

@section('content')
    <div id="invoice">
        <div class="invoice overflow-auto">
            <div>
                <header>
                    <div class="row">
                        <div class="col">
                            <img class="image" style="width: 150px;" src="{{ public_path('/img/logo.png') }}" alt="">
                        </div>
                    </div>
                </header>
                <main>
                    <div class="text-center">
                        @if($quote->customer->program_id == 4)
                            <h3 style="color:#c6394e;">Devis Étude au Canada</h3>
                        @else
                            <h3 style="color:#c6394e;">Devis pour Immigration au Canada</h3>
                        @endif
                    </div>
                    @if($quote->customer->program_id == 4)
                        <table class="table table-bordered border-danger" style="font-size: 0.8em;margin-right: 15px;">
                            <thead>
                            <tr>
                                <th></th>
                                <th style="text-align:center;">Description</th>
                                <th>Montant</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="align-middle no" rowspan='6'>
                                        Etape 1:<br>
                                        Analyse des données et admissibilité
                                    </td>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Rencontre 1 : Analyse des données et admissibilité
                                        </div>
                                    </td>
                                    <td class="unit" rowspan='6'>200</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Collecte des informations et remplir le questionnaire étudiant
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Analyse des informations et recommandations Clear 1001
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Explication des documents : Devis, contrat
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Accès au portail et espace client
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Devenir membre du réseau Clear Path Canada pour recevoir toutes les nouvelles
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle"  colspan='2'>
                                        <div class="text-muted">
                                            Total étape 1
                                        </div>
                                    </td>
                                    <td class="unit">200</td>
                                </tr>
                            <!-- Etape 2-->
                                <tr>
                                    <td class="align-middle no" rowspan='14'>
                                        Etape 2:<br>
                                        Développement du projet d'études
                                    </td>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Rencontre 2 : Préparation de la demande d'admission
                                        </div>
                                    </td>
                                    <td class="unit" rowspan='7'>2800</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Ouverture du dossier
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Explication de la procédure : Clear 1, Clear 2 et Clear 3
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Accompagnement et encadrement
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Développement du projet d'étude
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Préparation de la demande d'admission
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Orientation et propositions des programmes Clear 1002
                                        </div>
                                    </td>
                                </tr>
                                <!--substep2-->
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Rencontre 3 : Soumission de la demande d'admission
                                        </div>
                                    </td>
                                    <td class="unit" rowspan='6'>5000</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Vérification des documents Clear 1, Clear 2 et Clear 3
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Analyse Financière des garants
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Recommandation et solutions financières
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Confirmation et choix des programmes
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Soumission de la demande d'admission
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Frais des admissions collégiales et universitaires
                                        </div>
                                    </td>
                                    <td class="unit">2500</td>
                                </tr>
                                <tr>
                                    <td class="align-middle"  colspan='2'>
                                        <div class="text-muted">
                                            Total étape 2
                                        </div>
                                    </td>
                                    <td class="unit">10300</td>
                                </tr>
                            <!--Etape3-->
                                <tr>
                                    <td class="align-middle no" rowspan='9'>
                                        Etape 3:<br>
                                        Demande du visa et permis d'études
                                    </td>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Rencontre 4 : Préparation de la demande du Visa & PE
                                        </div>
                                    </td>
                                    <td class="unit" rowspan='4'>6000</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Explication de la procédure : Clear 4, Clear 5 et Clear 6
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Accompagnement et encadrement
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Préparation de la demande du visa et permis d'études
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Rencontre 5 : Soumission de la demande du Visa & PE
                                        </div>
                                    </td>
                                    <td class="unit" rowspan='4'>6000</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Vérification des documents : Clear 4, Clear 5 et Clear 6
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Signature des formulaires de visa et permis d'études
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Soumission de la demande de visa et permis d'études
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            Frais de visa et de permis d'études
                                        </div>
                                    </td>
                                    <td class="unit">1800</td>
                                </tr>
                                <tr>
                                    <td class="align-middle"  colspan='2'>
                                        <div class="text-muted">
                                            Total étape 3
                                        </div>
                                    </td>
                                    <td class="unit">13800</td>
                                </tr>
                                <tr>
                                    <td class="align-middle"  colspan='2'>
                                        <div class="text-muted">
                                            Total TTC
                                        </div>
                                    </td>
                                    <td class="unit">24300</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="page-break"></div>
                        <table class="table table-bordered border-danger" style="font-size: 0.8em;margin-right: 15px;">
                            <tbody>
                            <!--options-->
                            <tr>
                                <td class="align-middle no" rowspan='5'>
                                    Options
                                </td>
                                <td class="align-middle">
                                    <div class="text-muted">
                                        Frais de la demande de CAQ
                                    </div>
                                </td>
                                <td class="unit">900</td>
                            </tr>
                            <tr>
                                <td class="align-middle">
                                    <div class="text-muted">
                                        Resoumission de la demande de visa et permis d'études
                                    </div>
                                </td>
                                <td class="unit">3000</td>
                            </tr>
                            <tr>
                                <td class="align-middle">
                                    <div class="text-muted">
                                        Test de langue
                                    </div>
                                </td>
                                <td class="unit">2800</td>
                            </tr>
                            <tr>
                                <td class="align-middle">
                                    <div class="text-muted">
                                        Package : Accueil, intégration, conseils et encadrement au Canada
                                    </div>
                                </td>
                                <td class="unit">14500</td>
                            </tr>
                            <tr>
                                <td class="align-middle">
                                    <div class="text-muted">
                                        Séance de coaching par heure au Canada
                                    </div>
                                </td>
                                <td class="unit">700</td>
                            </tr>
                            </tbody>
                        </table>
                    @else
                    <table class="table table-bordered border-danger" style="font-size: 0.8em;">
                        <thead>
                        <tr>
                            <th></th>
                            <th style="text-align:center;">Description</th>
                            @if($quote->customer->program_id == 4)
                                <th>Montant</th>
                            @else
                                <th>Montant TTC</th>
                                <!-- <th>Couple</th> -->
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $groupedResultsArray = [];
                            $ttc = 0;
                            $ttc_couple = 0;
                        @endphp
                        @foreach($quote->services->sortBy('phase_id') as $service)
                            @php
                                $groupedResultsArray[$service->phase->name][] = $service;
                            @endphp
                        @endforeach
                        @foreach($groupedResultsArray as $phase)
                            @php
                                $qte_sum = 0;
                                $qte_sum_couple = 0;
                            @endphp
                            @if($loop->last)
                                <tr>
                                    @if($quote->customer->program_id == 4)
                                        <td></td>
                                        <td style="text-align:center;color:#c6394e;">Total TTC</td>
                                        <td style="text-align:center;color:#c6394e;">{{ $ttc }}</td>
                                    @else
                                        <td></td>
                                        <td style="text-align:center;color:#c6394e;">Total TTC</td>
                                        <td style="text-align:center;color:#c6394e;">{{ $ttc }}</td>
                                        <!-- <td style="text-align:center;color:#c6394e;">{{ $ttc_couple }}</td> -->
                                    @endif
                                </tr>
                            @endif
                            @if($quote->customer->program_id != 4 && $phase[0]->phase->name == "Options")
                                @php
                                    $services = \App\Models\Service::where("phase_id", $phase[0]->phase->id)->orderBy('order')->get();
                                @endphp
                                @foreach($services as $service)
                                <tr>
                                    @if($loop->first) <td class="align-middle no" rowspan='{{ count($services) }}'>{{ $phase[0]->phase->name }} </td>@endif
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            @if(count($service->details))
                                                @foreach($service->details as $detail)
                                                    {{ $detail->name }} <br>
                                                @endforeach
                                            @else
                                                {{ $service->name }}
                                            @endif
                                        </div>
                                    </td>
                                    <td class="unit">{{ $service->single_price }}</td>
                                    @php
                                        $qte_sum += $service->single_price;
                                        $qte_sum_couple += $service->couple_price;
                                    @endphp
                                </tr>
                                @endforeach
                            @else
                                @foreach($phase as $service)
                                <tr>
                                    @if($loop->first) <td class="align-middle no" rowspan='{{ count($phase) }}'>{{ $phase[0]->phase->name }} </td>@endif
                                    <td class="align-middle">
                                        <div class="text-muted">
                                            @if(count($service->details))
                                                @foreach($service->details as $detail)
                                                    {{ $detail->name }} <br>
                                                @endforeach
                                            @else
                                                {{ $service->name }}
                                            @endif
                                        </div>
                                    </td>
                                    @if($quote->customer->program_id == 4 || $phase[0]->phase->name == "Options")
                                        <td class="unit">{{ $service->pivot->billed_price }}</td>
                                    @else
                                        <td class="unit">{{ $service->single_price }}</td>
                                        <!-- <td class="unit">{{ $service->couple_price }}</td> -->
                                    @endif
                                    <!-- <td class="qty">{{ $service->pivot->qte }}</td> -->
                                    @php
                                        $qte_sum += $service->single_price;
                                        $qte_sum_couple += $service->couple_price;
                                    @endphp
                                    <!-- <td class="total">{{ $service->pivot->billed_price * $service->pivot->qte }}</td> -->
                                </tr>
                                @endforeach
                            @endif
                            @php
                                $ttc += $qte_sum;
                                $ttc_couple += $qte_sum_couple;
                            @endphp
                            @if($loop->last)
                                @break
                            @endif
                            <tr class="srow">
                            @if($quote->customer->program_id == 4)
                                <td></td>
                                <td style="text-align:center;">Total {{ $phase[0]->phase->name }}</td>
                                <td style="text-align: center;">{{ $qte_sum }}</td>
                            @else
                                <td></td>
                                <td style="text-align:center;">Total {{ $phase[0]->phase->name }}</td>
                                <td style="text-align: center;">{{ $qte_sum }}</td>
                                <!-- <td style="text-align: center;">{{ $qte_sum_couple }}</td> -->
                            @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                    <img style="width: 200px;position:absolute;top:860px;right:40px;" src="{{ public_path('/img/cachett.png') }}" alt="">
                </main>
                <footer>
                    <p  style="font-size: 10px;"> <strong>Agence Canada</strong> : 2000 avenue McGill College
                        6ème étage,<br/>Montréal Québec H3A 3H3, Canada. <br /><i class="fab fa-1x fa-whatsapp"></i> +1 438 404-0960
                        <strong>Agence Maroc</strong> : 42 Rue Bachir Laalej,<br />
                        Casablanca, Maroc <br /><i class="fab fa-1x fa-whatsapp"></i> +212 7 02 05 09 09<br />
                        <span style="font-size: 10px;">info@clearpathcanada.ca <br> www.clearpathcanada.ca</span> <br> IF : 50152557 | ICE : 002741390000066 | RC : 496031
                    </p>
                </footer>
            </div>
            <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
            <div></div>
        </div>
    </div>
@endsection
