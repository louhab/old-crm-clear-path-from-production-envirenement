<?php
    $lang = session()->get('locale');
    $currency = $customer_case->customer->currency_id ? \App\Models\Currency::where("id", $customer_case->customer->currency_id)->first()->iso : "MAD";
?>
@extends('layouts.backend')
@section('js_after')
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/pages/admissions/view.js') }}"></script>
    <script src="{{ asset('js/plugins/slick-carousel/slick.min.js') }}"></script>
    @livewireScripts
    <script>
    $(function() {

        $('.list-group-item').on('click', function() {
        $('.fa', this)
            .toggleClass('fa-chevron-right')
            .toggleClass('fa-chevron-down');
        });

    });
    $('body').on('click', '#toggle-lead-form', (event) => {
        const clickedBtn = $(event.target);
        $("#lead-form").toggle();
        $("#lead-info").toggle();
        if (clickedBtn.text() == 'Editer') {
            clickedBtn.replaceWith('<button type="button" id="toggle-lead-form" class="btn btn-sm btn-outline-secondary">Annuler</button>');
        } else {
            clickedBtn.replaceWith('<button type="button" id="toggle-lead-form" class="btn btn-sm btn-alt-primary">Editer</button>');
            // clickedBtn.text('Editer');
        }
        //console.log(clickedBtn.html());
        //$(this).closest('.block').removeClass('block-mode-loading');
    });
    </script>
    <script>
        $('.sign-document, .requested-doc').on('click', function(event) {
            event.preventDefault()
            $(`#${$(this).attr('data-formId')}`).submit()
        })
        /*$('.toggle-property').on('click', function(event) {
            event.preventDefault()
            let $form = $(`#${$(this).attr('data-formId')}`)
            let input = jQuery(`<input name="property" value="${$(this).attr('data-property')}">`)
            $form.append(input)
            $form.submit()
        })*/
        $(document).ready(function(){
            $('.js-slider').slick({
                dots: true,
                infinite: false,
                slidesToShow: 2,
                slidesToScroll: 2,
                arrows: true
                //prevArrow: '<button type="button" class="btn btn-sm btn-alt-primary">Previous</button>'
            });
        });
    </script>
    <script>
        jQuery(function(){
            One.helpers('table-tools-sections');
        });
    </script>
@endsection
@section('css_after')
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick-theme.css') }}">
    @livewireStyles
    <style>
      .just-padding {
        padding: 15px;
      }

      .list-group.list-group-root {
        padding: 0;
        overflow: hidden;
      }

      .list-group.list-group-root .list-group {
        margin-bottom: 0;
      }

      .list-group.list-group-root .list-group-item {
        border-radius: 0;
        border-width: 1px 0 0 0;
      }

      .list-group.list-group-root > .list-group-item:first-child {
        border-top-width: 0;
      }

      .list-group.list-group-root > .list-group > .list-group-item {
        padding-left: 30px;
      }

      .list-group.list-group-root > .list-group > .list-group > .list-group-item {
        padding-left: 45px;
      }

      .list-group-item .glyphicon {
        margin-right: 5px;
      }
    </style>
    <style>
        /*.select2-results { background-color: #f5b0b0; }select2-selection__rendered*/
        /*.select2-results__options--nested .select2-results__option:nth-child(1) { background-color: #f5b0b0; }
        .select2-results__options--nested .select2-results__option:nth-child(2) { background-color: #b0f5c5; }
        .select2-results__options--nested .select2-results__option:nth-child(3) { background-color: #b0e2f5; }
        .select2-results__options--nested .select2-results__option:nth-child(4) { background-color: #cbb0f5; }
        .select2-results__options--nested .select2-results__option:nth-child(5) { background-color: #f5c9b0; }*/


        .select2-selection__choice__remove {
            color: #fff !important;
        }
        .select2-search.select2-search--inline, .select2-search__field {
            width: 200px !important;
        }
    </style>
@endsection
@section('content')
    <!--  -->
    <div class="content">
        <div class="row">
            <div class="col-xl-8">
                <div class="block block-rounded">
                    <div class="block-header block-header-default">
                        <?php
                        $substep = \App\Models\ProgramSubStep::where("id", 1)->first();
                        $customer_errors = false;
                        $fields = $substep->fields()->wherePivot('program_id', '=', $customer_case->program_id)->get();
                        foreach($fields as $field) {
                            if (isset($errors) && $errors->has($field->field_name)) {
                                $customer_errors = true;
                                break;
                            }
                        }
                        ?>
                        <h3 class="block-title">{{ __("lang.portal_personal_infos_title") }}</h3>
                    </div>
                    <div class="block-content" style="padding: 20px;">
                        <div id="lead-form" @if(!$customer_errors) style="display:none;" @endif class="pr-5">
                            @include('cases.steps.partials.form-fields', ['form' => $substep])
                        </div>
                        <div id="lead-info" @if($customer_errors) style="display:none;" @endif>
                            <?php $conseiller = \App\Models\User::where('id', $customer_case->customer->lead->conseiller_id)->first(); ?>
                            <?php $support = \App\Models\User::where('id', $customer_case->customer->lead->support_id)->first();?>
                            <table class="table table-sm table-vcenter">
                                <tbody>
                                <tr>
                                    <td style="width: 50%">{{ __("lang.portal_personal_infos_surname") }} : <strong> {{ $customer_case->customer->lastname }}</strong></td>
                                    @administration
                                    <td>{{ __("lang.portal_personal_infos_phone") }} : <strong>{{ $customer_case->customer->phone }}</strong></td>
                                    @endadministration
                                </tr>
                                <tr>
                                    <td>{{ __("lang.portal_personal_infos_name") }} : <strong>{{ $customer_case->customer->firstname }}</strong></td>
                                    @administration
                                    <td>{{ __("lang.portal_personal_infos_advisor") }} : <strong>{{ $conseiller->name }}</strong></td>
                                    @endadministration
                                </tr>
                                @administration
                                @if($customer_case->customer->program_id==4)
                                    <tr>
                                        <td>Téléphone père : <strong>{{ is_null($customer_case->customer->father_phone) ? '-': $customer_case->customer->father_phone }}</strong></td>
                                        <td>Téléphone mère : <strong>{{ is_null($customer_case->customer->mother_phone) ? '-': $customer_case->customer->mother_phone }}</strong></td>
                                    </tr>
                                @endif
                                @endadministration
                                <tr>
                                    <td>{{ __("lang.portal_personal_infos_email") }} : <strong>{{ $customer_case->customer->email }}</strong></td>
                                    @administration
                                    <td>
                                        @if(is_null($support))
                                            Support : <strong>-</strong>
                                        @else
                                            Support : <strong>{{ $support->name }}</strong>
                                        @endif
                                    </td>
                                    @endadministration
                                </tr>
                                <tr>
                                    <td>{{ __("lang.portal_personal_infos_dob") }} : <strong>{{ $customer_case->customer->birthday }}</strong></td>
                                    <td>
                                        @if($customer_case->customer->lead->program_field)
                                            @php $program_field = \App\Models\ProgramField::find($customer_case->customer->lead->program_field); @endphp
                                            @if($customer_case->customer->program_id == 4)
                                                {{ __("lang.portal_personal_infos_study_field") }} : <strong>{{ $program_field->label_fr }}</strong>
                                            @else
                                                {{ __("lang.portal_personal_infos_work_field") }} : <strong>{{ $program_field->label_fr }}</strong>
                                            @endif
                                        @else
                                            {{ __("lang.portal_personal_infos_field") }} : <strong>-</strong>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @if($customer_case->customer->country_id)
                                            {{ __("lang.portal_personal_infos_country") }} : <strong>{{ \App\Models\Country::find($customer_case->customer->country_id)->name_fr_fr }}</strong>
                                        @else
                                            {{ __("lang.portal_personal_infos_country") }} : <strong> - </strong>
                                        @endif
                                    </td>
                                    <td>Type de rencontre : <strong>{{ $customer_case->customer->to_office }}</strong></td>
                                </tr>
                                <tr>
                                    <td>
                                        @if($customer_case->customer->city)
                                            {{ __("lang.portal_personal_infos_city") }} : <strong>{{ $customer_case->customer->city }}</strong>
                                        @else
                                            {{ __("lang.portal_personal_infos_city") }} : <strong> - </strong>
                                        @endif

                                    </td>
                                    <td>{{ __("lang.portal_personal_infos_programm") }} : <strong>{{ \App\Models\Program::where('id', $customer_case->customer->program_id)->first()->name }}</strong></td>
                                </tr>
                                <tr>
                                    <td> @livewire('button.media', ['customer_case' => $customer_case, 'collection' => 'Cin recto'])</td>
                                    <td>@livewire('button.media', ['customer_case' => $customer_case, 'collection' => 'CV'])</td>
                                </tr>
                                <tr>
                                    <td> @livewire('button.media', ['customer_case' => $customer_case, 'collection' => 'Cin verso'])</td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @php
                    $clear_fields = \App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first();
                    $visa_requests = \App\Models\CustomerVisaRequest::where('customer_id', $customer_case->customer_id)->orderBy('id', 'ASC')->get();
                @endphp
                @if($customer_case->program_id == 4 && $clear_fields && $clear_fields->visa_request_result == 85)
                <div class="block block-rounded">
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-6">
                                @livewire('block.result', ['customer_case' => $customer_case, 'meet' => 'R1'])
                            </div>
                            <div class="col-md-6">
                                @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-Demande de note'])
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if($customer_case->program_id == 1)
                    @livewire('block.cnp', ['customer_case' => $customer_case])
                    @php
                        $sheet = \App\Models\InformationSheet::find(9);
                    @endphp
                    <div class="block block-rounded">
                        <div class="block-header">
                            <h3 class="block-title">{{ $sheet->name_fr }}</h3>
                            <div class="block-options">
                                @include('cases.steps.partials.sheet', ['informationSheet' => $sheet, 'formId' => Str::random(10)])
                            </div>
                        </div>
                    </div>
                    <div class="block block-rounded">
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-6">
                                    @livewire('block.result', ['customer_case' => $customer_case, 'meet' => 'R4'])
                                </div>
                                <div class="col-md-6">
                                    @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-WES'])
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @management
                @if ($customer_case->program_id === 4)
                    {{-- submissions fees start @only for student --}}
                    @php
                    $canal_list = [
                        "soumission-admission-1",
                        "soumission-admission-2",
                        "soumission-admission-3",
                        "soumission-admission-4",
                        "soumission-admission-5",
                        "soumission-CAQ",
                        "soumission-VISA PE",
                    ];
                    // get tasks
                    $tasks_to_list = [];
                    $first = "";
                    $second = "";
                    $tmp = "";
                    $arr = [];
                    $total = 0;

                    $tasks = $customer_case->comments()
                            ->join('users', 'users.id', '=', 'case_comments.user_id')
                            ->join('roles', 'roles.id', '=', 'users.role_id')
                            ->join('customer_case_case_comment as cc', 'cc.case_comment_id', '=', 'case_comments.id')
                            ->whereIn('cc.canal', $canal_list)
                            ->where(function ($query) {
                                $query->where('role_name', '=', "Admin")
                                        ->orWhere('role_name', '=', "Conseiller")
                                        ->orWhere('role_name', '=', "Manager");
                        })->get(['case_comments.fees', 'cc.canal as canal','case_comments.id', 'is_done as status', 'case_comments.comment', 'case_comments.created_at'])->toArray();

                    foreach ($tasks as $key => $item) {
                        $arr[$item["canal"]][$key] = $item;
                    }
                    foreach ($arr as $yakey => $yaval) {
                        $tmp = array_reverse($yaval);
                        $first = array_pop($tmp);

                        if (isset($first)) {
                            // we gonna show all tasks even its done
                            $currentTime = \Carbon\Carbon::now();
                            $issueTime = Carbon\Carbon::parse($first["created_at"])->addHours(24);
                            if($currentTime->lte($issueTime)) {
                                $first["url"] = "/tasks#pills-todo-today";
                            } else {
                                $first["url"] = "/tasks#pills-overdue-task";
                            }
                            $first['canal'] = $first['canal'] . " (1)";
                            $tasks_to_list[] = $first;

                            // second task
                            $second = array_pop($tmp);
                            if (isset($second)) {
                                $currentTime = \Carbon\Carbon::now();
                                $issueTime = \Carbon\Carbon::parse($second["created_at"])->addHours(12);

                                if($currentTime->lte($issueTime)) {
                                    $second["url"] = "/tasks#pills-follow-up-today";
                                } else {
                                    $second["url"] = "/tasks#pills-overdue-follow-up";
                                }
                                $second['canal'] = $second['canal'] . " (2)";
                                $tasks_to_list[] = $second;
                            }
                        }
                    }

                    // total fees
                    foreach($tasks_to_list as $task_in_lst_cont) {
                            $total += $task_in_lst_cont['fees'];
                    }
                @endphp
                    <div class="block block-rounded">
                        <div class="block-header block-header-default">
                            <h3 class="block-title"><i class="fa fa-money-check"></i> les frais des taches soumission (en {{ $currency }})</h3>
                        </div>
                        <div class="block-content">
                            <table class="table table-sm table-vcenter">
                                <thead>
                                <tr class="meets">
                                    <th class="d-none d-sm-table-cell" style="width: 40%;">Type de document</th>
                                    <th class="px-4" style="width: 20%;">Frais</th>
                                    <th class="px-3" style="width: 20%;">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($tasks_to_list as $task_in_lst)
                                    <tr>
                                        <?php
                                            $task_name = ucfirst(str_replace("soumission-", "", $task_in_lst['canal']));
                                        ?>
                                        <td class="d-none d-sm-table-cell">
                                            <span>{{ $task_name }}</span>
                                        </td>
                                        <td class="d-none d-sm-table-cell">
                                            <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-light text-dark">{{ $task_in_lst['fees'] != 0 ? $task_in_lst['fees'] . " " . $currency : "-"}}</span>
                                        </td>
                                        <td class="d-none d-sm-table-cell">
                                            <button class="btn btn-sm btn-alt-primary" data-currency="{{ $currency }}" data-init-value="{{ $task_in_lst['fees'] }}" data-comment-id="{{ $task_in_lst['id'] }}" id="{{"admission-add-" . $task_in_lst['id']}}">
                                                {{ $task_in_lst['fees'] != 0 ? "Modifer " : "Ajouter "}} frais
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th class="d-none d-sm-table-cell">
                                        <span>Total frais: </span>
                                    </th>
                                    <th class="d-none d-sm-table-cell">
                                        <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-dark text-light">{{ $total != 0 ? $total . " " . $currency : "-"}}</span>
                                    </th>
                                    <th class="d-none d-sm-table-cell">
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
                @endmanagement
                {{-- submissions fees end --}}

                <div class="block block-rounded">
                    <div class="block-header block-header-default">
                        @if($customer_case->program_id == 4)
                            <h3 class="block-title"><i class="fa fa-satellite"></i> Préparation de la demande d’admission</h3>
                        @else
                            <h3 class="block-title"><i class="fa fa-satellite"></i> Préparation du profil {{ \App\Models\Program::find($customer_case->program_id)->name }} </h3>
                        @endif
                    </div>
                    <div class="block-content">
                        <table class="table table-sm table-vcenter">
                            @if($customer_case->program_id == 4)
                                <thead>
                                <tr class="meets">
                                    <th class="d-none d-sm-table-cell" style="width: 40%;">Type de document</th>
                                    <th style="width: 20%;">Actions</th>
                                </tr>
                                </thead>

                                <tr>
                                    <td>
                                        Clear1002 - Orientation
                                    </td>
                                    <td>
                                        @php
                                            $doc    = \App\Models\DocumentType::where("name_fr", "Clear1002 - Orientation")->first();
                                            $media = $customer_case->customer->getFirstMedia($doc->collection_name);
                                            $informationSheet = \App\Models\InformationSheet::where('name_fr', 'Clear1002 - Orientation')->first();
                                        @endphp
                                        @if(!empty($media))
                                            @include('cases.steps.partials.document', ['document' => $doc->collection_name_fr, 'collectionName' => $doc->collection_name, 'media' => $media])
                                        @endif
                                    </td>
                                </tr>
                            @endif
                            @php
                                $media_sheets = \App\Models\MediaSheet::with('substeps');
                            @endphp
                            @foreach($media_sheets->get() as $mediaSheet)
                                @if(!$mediaSheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                                    <tr>
                                        <td>
                                            {{ $mediaSheet->name_fr }}
                                        </td>
                                        <td>
                                            @include('cases.steps.partials.sheet-media', ['mediaSheet' => $mediaSheet, 'formId' => Str::random(10)])
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            @php
                                $sheets = \App\Models\InformationSheet::with('substeps');
                            @endphp
                            @foreach($sheets->get() as $sheet)
                                @if(!$sheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                                    <tr>
                                        <td>
                                            @if($customer_case->program_id!=4 && $sheet->id==3)
                                                Clear 13 - Fiche de renseignement
                                            @else
                                                {{ $sheet->name_fr }}
                                            @endif
                                        </td>
                                        <td>
                                            @include('cases.steps.partials.sheet', ['informationSheet' => $sheet, 'formId' => Str::random(10)])
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            @if($customer_case->program_id != 4)
                                <tr>
                                    @php
                                        $stat = \App\Models\SubStepState::where('name_fr', 'Guide de préparation de test de langue')->first();
                                        $cs = App\Models\CaseState::where("sub_step_state_id", $stat->id)
                                            ->where("customer_case_id", $customer_case->id)
                                            ->first();
                                        if(!$cs) {
                                            $cs = \App\Models\CaseState::create([
                                                'user_id' => Auth::id(),
                                                'customer_case_id' => $customer_case->id,
                                                'sub_step_state_id' => $stat->id,
                                                'status' => false,
                                            ]);
                                        }
                                    @endphp
                                    <td>
                                        @if($cs->status)
                                            <i class="fa fa-check text-success mr-4"></i>
                                        @else
                                            <i class="fa fa-sync fa-spin text-warning mr-4"></i>
                                        @endif
                                        <?= $lang === "fr" ? $stat->name_fr : $stat->name_en?>
                                    </td>
                                    <td>
                                        <a class="btn btn-lg btn-light case-state-update" data-case="{{$cs->id}}">
                                            @if($cs->status)
                                                <i class="fas fa-toggle-on"></i>
                                            @else
                                                <i class="fas fa-toggle-off"></i>
                                            @endif
                                        </a>
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
                <!-- Customer Medias -->
                @if($customer_case->program_id == 4)
                    @php
                        $admissions = App\Models\AdmissionUrl::where('customer_id', $customer_case->customer_id)->count();
                        $cmnts = $customer_case->comments()->wherePivot('canal', 'Like', "soumission-admission-%")->count();
                        $docs = ["Lettre modèle", 'Accusé de réception', 'Reçu', 'Lettre de suivi', "Lettre d'acceptation", "Lettre de refus"];
                        $have_docs = false;
                    @endphp
                    @foreach($docs as $doc)
                        @foreach(range(1, 5) as $idx)
                            @php
                                $collectionName = $doc . "-admission-" . $idx;
                                $media = $customer_case->customer->getFirstMedia($collectionName);
                                if ($media) {
                                    $have_docs = true;
                                    break;
                                }
                            @endphp
                        @endforeach
                    @endforeach
                    @if($admissions > 0 || $cmnts > 0 || $have_docs)
                        @livewire('block.admission.program', ['customer_case' => $customer_case])
                    @endif
                @endif
                @livewire('block.program', ['customer_case' => $customer_case])
                @if($customer_case->program_id == 1)
                    <div class="block block-rounded">
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-6">
                                    @livewire('block.soumission', ['customer_case' => $customer_case])
                                </div>
                                <div class="col-md-6">
                                    @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-PROFIL'])
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if($customer_case->program_id == 4)
                    <div class="block block-rounded">
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-6">
                                    @livewire('block.result', ['customer_case' => $customer_case, 'meet' => 'R4'])
                                </div>
                                <div class="col-md-6">
                                    <div class="table-responsive push">
                                        <table class="js-table-sections table table-bordered table-vcenter mb-0">
                                            <tbody class="js-table-sections-header show table-active">
                                            <tr>
                                                <td>
                                                    <div class="block m-0">
                                                        <div class="block-header">
                                                            <h3 class="block-title">Tache CAQ</h3>
                                                            <div class="block-options">
                                                                <div class="block-options-item">
                                                                    <span class="badge badge-primary">{{ $customer_case->comments()->wherePivot('canal', '=', "soumission-CAQ")->count() }}</span>
                                                                </div>
                                                                <span class="btn-block-option text-warning">
                                                                    <i class="far fa-fw fa-bell"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-CAQ'])
                                                    {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-CAQ']) --}}
                                                </td>
                                            </tr>
                                            </tbody>
                                            <tbody class="js-table-sections-header">
                                            <tr>
                                                <td>
                                                    <div class="block m-0">
                                                        <div class="block-header">
                                                            <h3 class="block-title">Tache CPG</h3>
                                                            <div class="block-options">
                                                                <div class="block-options-item">
                                                                    <span class="badge badge-primary">{{ $customer_case->comments()->wherePivot('canal', '=', "soumission-CPG")->count() }}</span>
                                                                </div>
                                                                <span class="btn-block-option text-warning">
                                                                    <i class="far fa-fw fa-bell"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-CPG'])
                                                    {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-CPG']) --}}
                                                </td>
                                            </tr>
                                            </tbody>
                                            <tbody class="js-table-sections-header">
                                            <tr>
                                                <td>
                                                    <div class="block m-0">
                                                        <div class="block-header">
                                                            <h3 class="block-title">Tache Facture scolaire</h3>
                                                            <div class="block-options">
                                                                <div class="block-options-item">
                                                                    <span class="badge badge-primary">{{ $customer_case->comments()->wherePivot('canal', '=', "soumission-Facture scolaire")->count() }}</span>
                                                                </div>
                                                                <span class="btn-block-option text-warning">
                                                                    <i class="far fa-fw fa-bell"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-Facture scolaire'])
                                                    {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-Facture scolaire']) --}}
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block block-rounded">
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-6">
                                    @livewire('block.result', ['customer_case' => $customer_case, 'meet' => 'R5'])
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="table-responsive push">
                                            <table class="js-table-sections table table-bordered table-vcenter mb-0">
                                                <tbody class="js-table-sections-header show table-active">
                                                <tr>
                                                    <td>
                                                        <div class="block m-0">
                                                            <div class="block-header">
                                                                <h3 class="block-title">Tache Prise en charge</h3>
                                                                <div class="block-options">
                                                                    <div class="block-options-item">
                                                                        <span class="badge badge-primary">{{ $customer_case->comments()->wherePivot('canal', '=', "soumission-Prise en charge")->count() }}</span>
                                                                    </div>
                                                                    <span class="btn-block-option text-warning">
                                                                        <i class="far fa-fw fa-bell"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-Prise en charge'])
                                                        {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-Prise en charge']) --}}
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tbody class="js-table-sections-header">
                                                <tr>
                                                    <td>
                                                        <div class="block m-0">
                                                            <div class="block-header">
                                                                <h3 class="block-title">Tache Imm5645</h3>
                                                                <div class="block-options">
                                                                    <div class="block-options-item">
                                                                        <span class="badge badge-primary">{{ $customer_case->comments()->wherePivot('canal', '=', "soumission-Imm5645")->count() }}</span>
                                                                    </div>
                                                                    <span class="btn-block-option text-warning">
                                                                        <i class="far fa-fw fa-bell"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-Imm5645'])
                                                        {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-Imm5645']) --}}
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tbody class="js-table-sections-header">
                                                <tr>
                                                    <td>
                                                        <div class="block m-0">
                                                            <div class="block-header">
                                                                <h3 class="block-title">Tache Imm5476</h3>
                                                                <div class="block-options">
                                                                    <div class="block-options-item">
                                                                        <span class="badge badge-primary">{{ $customer_case->comments()->wherePivot('canal', '=', "soumission-Imm5476")->count() }}</span>
                                                                    </div>
                                                                    <span class="btn-block-option text-warning">
                                                                        <i class="far fa-fw fa-bell"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-Imm5476'])
                                                        {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-Imm5476']) --}}
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    @livewire('block.result', ['customer_case' => $customer_case, 'meet' => 'R5'])
                @endif
                <div class="block block-rounded">
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-6">
                                @livewire('block.visa.soumission', ['customer_case' => $customer_case, 'soumission' => true])
                                @livewire('block.visa.soumission', ['customer_case' => $customer_case, 'soumission' => false])
                            </div>
                            <div class="col-md-6">
                                @if($customer_case->program_id == 4)
                                    @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-VISA PE'])
                                    {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-VISA PE']) --}}
                                @endif
                                @if($customer_case->program_id == 1)
                                    @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-VISA'])
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4">
                <div class="block block-rounded">
                    <div class="block-header block-header-default text-center">
                        <h3 class="block-title">A propos du Client</h3>
                    </div>
                    <div class="block-content">
                        <table class="table table-striped table-borderless font-size-sm">
                            <tbody>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-calendar mr-1"></i> <strong>Inscrit le</strong> {{ ($customer_case->created_at)->format('d M Y') }}
                                    <!-- 13 Juin 2021 -->
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-user mr-1"></i> <strong>Affecter à</strong>  <em>{{ \App\Models\User::where('id', $customer_case->customer->lead->conseiller_id)->first()->name }}</em>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-socks mr-1"></i> <strong>Canal</strong>  <em>{{ ($customer_case->customer->lead->campaign_id) ? App\Models\LeadFormMarketingCampaign::where('id', $customer_case->customer->lead->campaign_id)->first()->campaign_name : "N/A" }}</em>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                        $admission_state = \App\Models\AdmissionState::where("id", $customer_case->customer->admission_state)->first();
                                        $state_id = $admission_state ? $admission_state->id: null;
                                        switch ($state_id) {
                                            case 1:
                                                $badge = "info";
                                                break;
                                            case 2:
                                                $badge = "warning";
                                                break;
                                            case 3:
                                                $badge = "success";
                                                break;
                                            case 4:
                                                $badge = "danger";
                                                break;
                                            default:
                                                $badge = "info";
                                        }
                                    ?>
                                    <i class="fa fa-fw fa-user mr-1"></i> <strong>Status</strong>  <em><span class="badge badge-{{ $badge }}">{{ ($admission_state) ? $admission_state->label : 'N/A' }}</span></em>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Start status -->
                @livewire('block.admission.state', ['customer_case' => $customer_case])
                <!-- End status -->
                <!-- Start comments -->

                @livewire('block.comment', ['customer_case' => $customer_case, 'canal' => 'soumission'])
                <!-- End comments -->
                @livewire('block.document', ['customer_case' => $customer_case])

                <!--@haveDocuments($customer_case)@endhaveDocuments-->
            </div>
        </div>
    </div>
    <!-- histocall Customer Add Modal -->
    <div class="modal" id="modal-message-add" tabindex="-1" role="dialog" aria-labelledby="modal-message-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-message-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter message pour conseiller</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <form action="" method="POST" onsubmit="return false;" id="add-message-form">
                                    <div class="form-group">
                                        <ul style="list-style:none;">
                                            <li style="display:inline;margin-right:40px;"><strong>Client:</strong> {{ $customer_case->customer->firstname}}</li>
                                            <li style="display:inline;"><strong>Conseiller: </strong>{{ \App\Models\User::find($customer_case->customer->lead->conseiller_id)->name }}</li>
                                        </ul>
                                    </div>
                                    <input type="hidden" name="add-form-customer_id" value="{{ $customer_case->customer->id }}" />
                                    <input type="hidden" name="add-form-object" value="SOUMISSION" />
                                    <div class="form-group">
                                        <textarea type="text" class="form-control form-control-alt" id="add-form-message" name="add-form-message" placeholder="Message.." aria-describedby="add-form-message-error" aria-invalid="false"></textarea>
                                        <div id="add-form-message-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" id="add-message-btn" class="btn btn-alt-primary mr-1" data-dismiss="modal">Ajouter message</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END histocall Add Modal -->

    <!-- add fees Modal -->
    <div class="modal" id="modal-set-admission-fees" tabindex="-1" role="dialog" aria-labelledby="modal-set-admission-fees" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-histocall-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter frais admission</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                {{-- form start --}}
                                <form action="" method="POST" onsubmit="return false;" id="form-add-admision-fees">
                                    <p style="line-height: 24px;font-size: 26px;width: 100%;">
                                        <strong id="task-title">Frais de la tache </strong>
                                    </p>
                                    <div class="form-group mt-4">
                                        <input type="number" class="form-control form-control-alt" id="form-add-admision-fees-rate" name="form-add-admision-fees-rate" aria-describedby="form-add-admision-fees-rate-error" aria-invalid="false" />
                                        <div id="form-add-admision-fees-rate-error" class="invalid-feedback"></div>
                                    </div>
                                        <div class="form-group">
                                            <button type="button" id="form-add-admision-fees-btn" class="btn btn-alt-primary btn-block">Sauvegarder</button>
                                        </div>
                                </form>
                                {{-- form end --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END add fees Modal -->

    <!-- END Page Content -->
    @include('billing.modals.services')
@endsection
