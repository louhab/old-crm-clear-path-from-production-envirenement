@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <!-- Page JS Code -->
    <script>
        window.userRole = {!! auth()->user()->toJson() !!};
        $('input[name="daterange"]').daterangepicker({
            opens: 'left',
            locale: {
                cancelLabel: 'Clear'
            }
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format(
                'YYYY-MM-DD'));
            $('input[name="date_start"]').val(start.format('YYYY-MM-DD'));
            $('input[name="date_end"]').val(end.format('YYYY-MM-DD'));
            // dashboardPage.initDashboardAnalytics();
            $('#admissions-dt').DataTable().ajax.reload(null, false);
        });
        $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            $('input[name="date_start"]').val('');
            $('input[name="date_end"]').val('');
            $('#admissions-dt').DataTable().ajax.reload(null, false);
        });
        $("[name='tags[]']").select2({
            placeholder: "Selectionnez mots clés",
        });
        $("[name='status']").select2({
            placeholder: "Status ...",
            allowClear: true,
            width: "100%"
        });
        $("[name='program']").select2({
            placeholder: "Programmes ...",
            allowClear: true,
            width: "100%"
        });
        $("[name='conseiller']").select2({
            placeholder: "Conseiller ...",
            allowClear: true,
            width: "100%"
        });
        // backoffice
        $("[name='backoffice']").select2({
            placeholder: "Backoffice ...",
            allowClear: true,
            width: "100%"
        });
        $("[name='task_soumission']").select2({
            placeholder: "Tache Soumission...",
            allowClear: true,
            width: "100%"
        });
        $("[name='task_client']").select2({
            placeholder: "Tache Client...",
            allowClear: true,
            width: "100%"
        });
        $(".dt_search_admission").select2({
            placeholder: function() {
                $(this).data('placeholder');
            },
            allowClear: true,
            width: "100%"
        });
        // admission_document
        $("[name^='admission_document_']").select2();
        $("[name^='soumission_document_']").select2();
    </script>
    <script>
        // admission_status
        $('select[name="status"]').select2();
        $('select[name="status"]').select2({
            placeholder: 'Status...',
            allowClear: true,
            ajax: {
                url: `/api/v1/admission-states`,
                dataType: 'json',
                delay: 250,
                method: 'POST',
                data: function(term, page) {
                    // console.log(pid);
                    return {
                        q: term,
                    }
                },
                processResults: function(data) {
                    // console.log(data);
                    var res = [];
                    // var minus = -101;
                    $.each(data, function(label, group) {
                        var groupObj = {
                            "text": label
                        };
                        var children = []; // console.log(role, group);
                        $.each(group, function(idx, status) {
                            // console.log(status.name);
                            children.push({
                                text: status.name,
                                id: status.id,
                                group: label
                            });
                        });
                        groupObj["children"] = children;
                        res.push(groupObj);
                    });

                    return {
                        results: res
                    };
                },
                initSelection: function(element, callback) {},
                cache: true
            }
        });
    </script>
    <script src="{{ asset('js/pages/admissions/list.js') }}"></script>
    <script>
        jQuery(function() {
            One.helpers('table-tools-sections');
        });
    </script>
    @livewireScripts
@endsection

@section('css_after')
    <!-- Styles -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        .select2-selection__choice__remove {
            color: #fff !important;
        }

        .select2-search.select2-search--inline,
        .select2-search__field {
            width: 200px !important;
        }

        .select2-selection__choice {
            height: 100% !important;
        }

        .select2-selection__rendered {
            word-wrap: break-word !important;
            text-overflow: inherit !important;
            white-space: normal !important;
        }

        .select2-container {
            width: 100% !important;
        }

        /* .recent-comment {
                                            color: white;
                                            background-color: #FD971F;
                                        }
                                        .recent-comment-client {
                                            color: white;
                                            background-color: #ff6363;
                                            height: 100%;
                                        } */
    </style>
    @livewireStyles
@endsection

@section('content')
    @include('layouts.partials.hero', [
        'title' => 'Soumissions',
        'subTitle' => 'Listes',
        'navItems' => ['Admin', 'Soumissions'],
    ])

    <!-- Page Content -->
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if ($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les
                                informations !</a>
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        <!-- admissions Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>{{ __('lang.make_search') }}</h4>
                <div class="row push search-filter">
                    <!--<div class="col-lg-12">
                                                        <form id="search-form" name="search-form" action="" method="post" onsubmit="return false">
                                                            @csrf
                                                            <div class="form-group form-row">
                                                                <div class="col-md-8">
                                                                    <div class="row">
                                                                        <div class="col-3 p-1 pl-4">
                                                                            <label for="client" class="sr-only">Client</label>
                                                                            <select class="form-control dt_search_field dt_search_client" id="client" name="client">
                                                                            </select>
                                                                        </div>
                                                                        <div class="col p-1">
                                                                            <label for="status" class="sr-only">Status</label>
                                                                            {{ Form::select('status', \App\Models\AdmissionState::pluck('label', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', 'placeholder' => 'Status...', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                                                        </div>
                                                                        <div class="col p-1">
                                                                            <label for="phone" class="sr-only">Phone</label>
                                                                            <input class="form-control dt_search_field dt_search_phone" id="phone" name="phone" style="height:44px" placeholder="Phone">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="row">
                                                                        <div class="col-5 p-1 pl-4">
                                                                            <button type="button" class="btn btn-alt-primary btn-block submit-search" id="search-btn"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                                                                        </div>
                                                                        <div class="col-3 p-1">
                                                                            <button type="button" class="btn btn-alt-danger btn-block clear-search" id="clear-search-btn"><i class="fa fa-fw fa-redo mr-1"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>-->
                    <div class="col-lg-12">
                        <form id="search-form" name="search-form" action="" method="post" onsubmit="return false">
                            @csrf
                            <div class="form-group form-row">
                                <div class="col-md-12">
                                    <div class="row p-2">
                                        <div class="col-3 p-1">
                                            <input class="form-control dt_search_field dt_search_customer" id="customer"
                                                name="customer" style="height:44px"
                                                placeholder="{{ __('lang.customers') }}..">
                                        </div>

                                        <div class="col-3 p-1">
                                            <input class="form-control dt_search_field dt_search_phone" id="phone"
                                                name="phone" style="height:44px" placeholder="{{ __('lang.phone') }}">
                                        </div>
                                        <div class="col-3 p-1">
                                            {{ Form::select('program', \App\Models\Program::where('active', 1)->pluck('name', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', 'multiple' => 'multiple', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>
                                        <div class="col-3 p-1">
                                            {{ Form::select('status', \App\Models\AdmissionState::pluck('label', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', 'multiple' => 'multiple', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>
                                        <div class="col-3 p-1">
                                            <?php
                                            $list_roles = [3];
                                            if (\Illuminate\Support\Facades\Auth::user()->isAdmin() || \Illuminate\Support\Facades\Auth::user()->isVerificateur()) {
                                                array_push($list_roles, 2);
                                            }
                                            $queryConseiller = \App\Models\User::where('status', 1)->where(function ($query) use ($list_roles) {
                                                $query->where(function ($query) use ($list_roles) {
                                                    $query->whereIn('role_id', $list_roles);
                                                });
                                                if (\Illuminate\Support\Facades\Auth::user()->isManager()) {
                                                    $query->orWhere(function ($query) {
                                                        $query->where('id', \Illuminate\Support\Facades\Auth::id());
                                                    });
                                                }
                                            });
                                            $queryConseillerArray = ['n/d' => 'Non Affecté'] + $queryConseiller->pluck('name', 'id')->toArray();
                                            ?>
                                            {{ Form::select('conseiller', $queryConseillerArray, null, ['class' => 'form-control form-control-alt dt_search_field', 'multiple' => 'multiple', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>
                                        <div class="col-3 p-1">
                                            <?php
                                            $list_roles = [6];
                                            $queryConseiller = \App\Models\User::where('status', 1)->where(function ($query) {
                                                $query->whereIn('role_id', [6]);
                                            });
                                            $queryConseillerArray = ['n/d' => 'Non Affecté'] + $queryConseiller->pluck('name', 'id')->toArray();
                                            ?>
                                            {{ Form::select('backoffice', $queryConseillerArray, null, ['class' => 'form-control form-control-alt dt_search_field', 'multiple' => 'multiple', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>
                                        <div class="p-1 col-3">
                                            @livewire('input.cities', ['value' => null])
                                        </div>
                                        <div class="col-3 p-1">
                                            <?php
                                            $tts = ['Prise en charge', 'Imm5645', 'Imm5476', 'CPG', 'Facture scolaire'];
                                            $states = \App\Models\AdmissionState::where('order', 1)
                                                ->pluck('label')
                                                ->toArray();
                                            $states = array_merge($states, $tts);
                                            // dd($states);
                                            // $queryConseillerArray = array("n/d" => "Non Affecté") + $queryConseiller->pluck('name', 'id')->toArray();
                                            ?>
                                            {{ Form::select('task_soumission', array_combine($states, $states), null, ['class' => 'form-control form-control-alt dt_search_field', 'multiple' => 'multiple', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>

                                        <div class="col-3 p-1">
                                            <?php
                                            // $list_roles = [6];
                                            //$states = \App\Models\AdmissionState::where("order", 1)->pluck("label")->toArray();
                                            // $queryConseillerArray = array("n/d" => "Non Affecté") + $queryConseiller->pluck('name', 'id')->toArray();
                                            ?>
                                            {{ Form::select('task_client', array_combine($states, $states), null, ['class' => 'form-control form-control-alt dt_search_field', 'multiple' => 'multiple', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>
                                        <div class="col-3 p-1">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text input-group-text-alt">
                                                            <i class="far fa-calendar-alt"></i>
                                                        </span>
                                                    </div>
                                                    @php
                                                        $firstDay = \Carbon\Carbon::parse('2021-11-01')->format('m/d/Y');
                                                        $lastDay = \Carbon\Carbon::today()->format('m/d/Y');
                                                        $current_month = $firstDay . ' - ' . $lastDay;
                                                    @endphp
                                                    {{-- <input type="text" class="form-control form-control"
                                                        id="daterange" name="daterange" value="{{ $current_month }}">
                                                    <input type="hidden" name="date_end" class="dt_search_field"
                                                        value="{{ $lastDay }}" />
                                                    <input type="hidden" name="date_start" class="dt_search_field"
                                                        value="{{ $firstDay }}" /> --}}
                                                </div>
                                            </div>
                                        </div>
                                        @administration
                                            <div class="col-md-12">
                                                <div class="table-responsive push">
                                                    <table class="js-table-sections table table-bordered table-vcenter mb-0">
                                                        <tbody class="js-table-sections-header show table-active">
                                                            <tr>
                                                                <td>
                                                                    <div class="block m-0">
                                                                        <div class="block-header">
                                                                            <h3 class="block-title">Orientation Clear 1002</h3>
                                                                            <div class="block-options">
                                                                                <span class="btn-block-option text-warning">
                                                                                    <i class="far fa-fw fa-bell"></i>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <div class="row push p-3">
                                                                        @php
                                                                            $allValues = [];
                                                                            $filter_columns = ['nom_etablissement', 'session', 'date_limit_admission'];
                                                                            foreach ($filter_columns as $column) {
                                                                                $values = \App\Models\ProgramCourse::where('properties->' . $column . '->value', 'NOT LIKE', '')
                                                                                    ->select('properties->' . $column . '->value')
                                                                                    ->get()
                                                                                    ->toArray();
                                                                                $values = array_map(function ($item) {
                                                                                    return array_values($item)[0];
                                                                                }, $values);
                                                                                $values = array_unique($values);
                                                                            
                                                                                /*$values = DB::table('orientation_schools')
                                                                                ->where($column, 'NOT LIKE', "")
                                                                                ->groupBy($column)
                                                                                ->pluck($column);*/
                                                                            
                                                                                $allValues[$column] = array_combine($values, $values);
                                                                                // dd($field->field_name);
                                                                            }
                                                                            $programs = \App\Models\ProgramCourse::pluck('name')->toArray();
                                                                            $programs = array_unique(
                                                                                array_map(function ($item) {
                                                                                    return trim($item);
                                                                                }, $programs),
                                                                            );
                                                                            $allValues['programme'] = array_combine($programs, $programs);
                                                                        @endphp
                                                                        @foreach ($allValues as $column => $values)
                                                                            <div class="col-3 p-1">
                                                                                <div class="form-group">
                                                                                    {{ Form::select('admission_' . $column, $values, null, ['class' => 'form-control form-control-alt dt_search_field dt_search_admission', 'data-placeholder' => __('lang.admission_' . $column), 'multiple' => 'multiple', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tbody class="js-table-sections-header">
                                                            <tr>
                                                                <td>
                                                                    <div class="block m-0">
                                                                        <div class="block-header">
                                                                            <h3 class="block-title">Documents</h3>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <div class="row push p-3">
                                                                        @php
                                                                            $docs = ['Lettre modèle', 'Accusé de réception', 'Reçu', 'Lettre de suivi', "Lettre d'acceptation", 'Lettre de refus'];
                                                                        @endphp
                                                                        @foreach ($docs as $doc)
                                                                            <div class="col-3 p-1">
                                                                                <div class="form-group">
                                                                                    {{ Form::select('admission_document_' . $loop->index, [1 => 'Oui', 2 => 'Non'], null, ['class' => 'form-control form-control-alt dt_search_field', 'placeholder' => $doc, 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tbody class="js-table-sections-header">
                                                            <tr>
                                                                <td>
                                                                    <div class="block m-0">
                                                                        <div class="block-header">
                                                                            <h3 class="block-title">Visa & PE</h3>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <div class="row push p-3">
                                                                        @php
                                                                            // $docs = new Illuminate\Support\Collection();
                                                                            $docs = new \Illuminate\Database\Eloquent\Collection();
                                                                            foreach ([9, 22] as $substepId) {
                                                                                $substep = \App\Models\ProgramSubStep::find($substepId);
                                                                                $docs = $docs->merge(
                                                                                    $substep
                                                                                        ->docs()
                                                                                        ->withPivot('order')
                                                                                        ->where('customer_owner', 0)
                                                                                        ->orderBy('order', 'asc')
                                                                                        ->get(),
                                                                                );
                                                                            }
                                                                        @endphp
                                                                        @foreach ($docs as $doc)
                                                                            <div class="col-3 p-1">
                                                                                <div class="form-group">
                                                                                    {{ Form::select('soumission_document_' . $loop->index, [1 => 'Oui', 2 => 'Non'], null, ['class' => 'form-control form-control-alt dt_search_field', 'placeholder' => $doc->name_fr, 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endadministration
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-5 p-1">
                                                    <button type="button"
                                                        class="btn btn-alt-primary btn-block submit-search"
                                                        id="search-btn"><i class="fa fa-fw fa-search mr-1"
                                                            style="font-size: 15px;"></i>{{ __('lang.search') }}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="admissions-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">{{ __('lang.soumission_list') }}</h3>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-vcenter" id="admissions-dt">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- comment info Modal -->
    <div class="modal" id="modal-comment-info" tabindex="-1" role="dialog" aria-labelledby="modal-comment-info"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">{{ __('lang.modal_comment_info_title') }}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                @livewire('modal.comment-info')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END comment Info Modal -->
@endsection
