@extends('layouts.backend')
@section('css_after')
    <meta http-equiv="Expires" content="Mon, 26 Jul 1997 05:00:00 GMT">
    <meta http-equiv="Pragma" content="no-cache">
    @livewireStyles
    <script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pace-js@latest/pace-theme-default.min.css">
    <style>
        .pace {
            -webkit-pointer-events: none;
            pointer-events: none;

            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;

            z-index: 2000;
            position: fixed;
            height: 90px;
            width: 90px;
            margin: auto;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }

        .pace.pace-inactive .pace-activity {
            display: none;
        }

        .pace .pace-activity {
            position: fixed;
            z-index: 2000;
            display: block;
            position: absolute;
            left: -30px;
            top: -30px;
            height: 90px;
            width: 90px;
            display: block;
            border-width: 30px;
            border-style: double;
            border-color: #d52b12 transparent transparent;
            border-radius: 50%;

            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;

            -webkit-animation: spin 1s linear infinite;
            -moz-animation: spin 1s linear infinite;
            -o-animation: spin 1s linear infinite;
            animation: spin 1s linear infinite;
        }

        .pace .pace-activity:before {
            content: ' ';
            position: absolute;
            top: 10px;
            left: 10px;
            height: 50px;
            width: 50px;
            display: block;
            border-width: 10px;
            border-style: solid;
            border-color: #d52b12 transparent transparent;
            border-radius: 50%;

            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
        }

        @-webkit-keyframes spin {
            100% { -webkit-transform: rotate(359deg); }
        }

        @-moz-keyframes spin {
            100% { -moz-transform: rotate(359deg); }
        }

        @-o-keyframes spin {
            100% { -moz-transform: rotate(359deg); }
        }

        @keyframes spin {
            100% {  transform: rotate(359deg); }
        }
        /*clear docs*/
        .document-in {
            margin: 0 !important;
        }
        .document-in label {
            padding: 0 !important;
            margin: 0 !important;
        }
        .document-in .form-group {line-height: 0.9;}
        .document-in .form-group {
            margin: 0 !important;
        }
    </style>
@endsection
@section('js_after')
    <!-- Page JS Helpers (Table Tools helpers) -->
    <!-- Page JS DataTables Plugins -->
<!--    <script src="{{ asset('js/pages/dropzone.js') }}"></script>-->
    @livewireScripts
    <script>
        $(document).on("click", ".document-clear-table-edit", function() {
            let clearsEditFormSelector = $("#document-clear-edit-form");
            let clearsEditBlockSelector = $("#block-document-clear-edit");

            //hide previous validation errors
            clearsEditFormSelector.trigger("reset");
            clearsEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            clearsEditBlockSelector.addClass("block-mode-loading");

            // get data with ajax
            let rowID = $(this).closest('tr').data('ds');
            $.ajax({
                method: "get",
                url: `/api/v1/document-clear/${rowID}`,
                success: (data) => {
                    // console.log(data);
                    $("#edit-form-clear").val(data.media_sheet_id);
                    $("#edit-form-program").val(data.program_id);
                    $("#edit-form-order").val(data.order);
                }
                /*error: xhr => {
                    if (xhr.statusCode().status === 422) {
                        $.each(xhr.responseJSON.errors, function(key, value) {
                            $(`#${key}`).addClass("is-invalid");
                            $(`#${key}-error`)
                                .html(
                                    value
                                        .toString()
                                        .replace(
                                            key,
                                            $(
                                                "label[for='" +
                                                $(`#${key}`).attr("id") +
                                                "']"
                                            ).html()
                                        )
                                )
                                .show();
                        });
                        clearsEditBlockSelector.removeClass(
                            "block-mode-loading"
                        );
                    } else
                        Swal.fire(
                            "Ajout!",
                            "erreur edition document!",
                            "error"
                        ).then(() => {
                            clearsEditBlockSelector.removeClass(
                                "block-mode-loading"
                            );
                        });
                }*/
            });
            clearsEditBlockSelector.removeClass(
                "block-mode-loading"
            );
            //hide previous validation errors
            clearsEditFormSelector.find(".invalid-feedback").hide();
            clearsEditFormSelector
                .find(".form-control")
                .removeClass("is-invalid");

            // $('#leadsforms-edit-form').trigger('reset');
            $("#modal-document-edit").modal("show");
        });
        //DELETE FORM
        $("body").on("click", ".document-clear-table-delete", function() {
            window.CLEARPATH_DOCUMENT_CLEAR_ID = $(this).closest('tr').data('ds');

            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui, supprimer!",
                cancelButtonText: "Annuler"
            }).then(result => {
                if (result.isConfirmed) {
                    deleteDocumentClear();
                }
            });
        });
        function deleteDocumentClear() {
            $("#document-clear-block").addClass("block-mode-loading");

            $.ajax({
                method: "delete",
                url: `/api/v1/document-clear/${window.CLEARPATH_DOCUMENT_CLEAR_ID}`,
                success: () => {
                    Swal.fire(
                        "Suppression!",
                        "Document supprimé avec succés!",
                        "success"
                    ).then(() => {
                        // dt.api().ajax.reload(null, false);
                        location.reload();
                    });
                },
                error: () => {
                    Swal.fire(
                        "Suppression!",
                        "erreur suppression document!",
                        "error"
                    ).then(() => {
                        $("#document-clear-block").removeClass(
                            "block-mode-loading"
                        );
                    });
                }
            });
        }
    </script>
@endsection
@section('content')
    @include('layouts.partials.hero', ['title' => 'Document','subTitle' => $document->name_fr, 'navItems' => ['Document', 'informations']])

    <!-- Page Content -->
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les informations !</a>
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        <div class="row">
            <div class="col-12">
                <div class="block block-rounded" id="document-clear-block">
                    <div class="block-content">
                        <table class="table table-hover table-vcenter">
                            <thead>
                            <tr>
                                <th>Clear Name</th>
                                <th class="d-none d-sm-table-cell" style="width: 15%;">Programe</th>
                                <th class="d-none d-sm-table-cell" style="width: 15%;">Ordre</th>
                                <th class="text-center" style="width: 100px;">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($document->sheets()->withPivot('id')->get() as $clear)
                                <tr data-ds="{{ $clear->pivot->id }}">
                                    <td class="font-w600 font-size-sm">
                                        <a href="javascript:void(0)">{{ $clear->name_fr }}</a>
                                    </td>
                                    <td class="d-none d-sm-table-cell">
                                        @php
                                            $program = \App\Models\Program::find($clear->pivot->program_id);
                                        @endphp
                                        {{ $program->name }}
                                    </td>
                                    <td class="d-none d-sm-table-cell">
                                        {{ $clear->pivot->order }}
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <button href="javascript:void(0)" title="Editer Document" class="btn btn-sm btn-primary document-clear-table-edit">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <button href="javascript:void(0)" title="Supprimer Document" class="btn btn-sm btn-danger document-clear-table-delete">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{--<div class="col-6">
                <div class="block">
                    <div class="block-header">
                        <h3 class="block-title">Groupes</h3>
                    </div>
                    <div class="block-content">
                        <table class="table table-hover table-vcenter">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th class="d-none d-sm-table-cell" style="width: 15%;">Access</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($document->groups()->get() as $group)
                                <tr>
                                    <td class="font-w600 font-size-sm">
                                        <a href="be_pages_generic_profile.html">{{ $group->name_fr }}</a>
                                    </td>
                                    <td class="d-none d-sm-table-cell">
                                        <span class="badge badge-primary">Personal</span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>--}}
        </div>
    </div>
    <!-- END Page Content -->
    <!-- leadsforms Lead Add Modal -->
    <div class="modal" id="modal-documents-add" tabindex="-1" role="dialog" aria-labelledby="modal-documents-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-documents-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter document</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form documents lead Add -->
                                <form action="" method="POST" onsubmit="return false;" id="documents-add-form">
                                    <div class="form-group">
                                        <label for="add-form-title-en">Titre Anglais</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="add-form-title-en" name="add-form-title-en" aria-describedby="add-form-title-en-error" aria-invalid="false" placeholder="Titre Anglais..">
                                        <div id="add-form-title-en-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-title-fr">Titre Francais</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="add-form-title-fr" name="add-form-title-fr" aria-describedby="add-form-title-fr-error" aria-invalid="false" placeholder="Titre Français..">
                                        <div id="add-form-title-fr-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-group">Groupe</label>
                                        <select class="form-control form-control-alt" id="add-form-group" name="add-form-group" aria-describedby="add-form-group-error" aria-invalid="false">
                                            <option value="">Sélectionnez groupe</option>
                                            @foreach(\App\Models\DocumentTypeGroup::all() as $group)
                                                <option value="{{ $group->id }}">{{ $group->name_fr }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-group-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Client Propriétaire :</label>
                                        <div class="custom-control custom-switch custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" id="add-form-redirect" name="add-form-redirect">
                                            <label class="custom-control-label" for="add-form-redirect">Client peut téléverser?</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-documents-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter document</button>
                                    </div>
                                </form>
                                <!-- END Form documents Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END leadsforms Add Modal -->
    <!-- leadsforms Lead Edit Modal -->
    <div class="modal" id="modal-document-edit" tabindex="-1" role="dialog" aria-labelledby="modal-document-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-document-clear-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Editer document</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form leadsforms lead edit -->
                                <form action="" method="POST" onsubmit="return false;" id="document-clear-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-clear">Clear</label>
                                        <select class="form-control form-control-alt" id="edit-form-clear" name="edit-form-clear" aria-describedby="edit-form-clear-error" aria-invalid="false">
                                            <option value="">Sélectionnez clear</option>
                                            @foreach(\App\Models\MediaSheet::all() as $clear)
                                                <option value="{{ $clear->id }}">{{ $clear->name_fr }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-clear-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-program">Programme</label>
                                        <select class="form-control form-control-alt" id="edit-form-program" name="edit-form-program" aria-describedby="edit-form-program-error" aria-invalid="false">
                                            <option value="">Sélectionnez programme</option>
                                            @foreach(\App\Models\Program::all() as $program)
                                                <option value="{{ $program->id }}">{{ $program->name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-program-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-order">Order :</label>
                                        <input type="number" class="form-control form-control-alt" id="edit-form-order" name="edit-form-order" aria-describedby="edit-form-order-error" aria-invalid="false">
                                        <div id="edit-form-order-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="edit-document-clear-btn"><i class="fa fa-fw fa-plus mr-1"></i>Editer document</button>
                                    </div>
                                </form>
                                <!-- END Form leadsforms Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END leadsforms Add Modal -->
@endsection

