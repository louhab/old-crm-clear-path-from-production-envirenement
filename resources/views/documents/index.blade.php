@extends('layouts.backend')

@section('css_after')
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    @livewireStyles
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    @livewireScripts
    <script src="{{ asset('js/pages/documents/list.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Documents','subTitle' => 'conf', 'navItems' => ['Admin', 'Documents']])

    <!-- Page Content -->
    <div class="content">

        <!-- leadsforms Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                    <form id="search-form" name="search-form" action="" method="post" onsubmit="return false">
                        <div class="form-group form-row">
                            <div class="col-3 p-1">
                                <label for="name_fr" class="sr-only">Nom En Français</label>
                                <input class="form-control dt_search_field" id="name_fr" name="name_fr" style="height:44px" placeholder="Nom en français..">
                            </div>
                            <div class="col-3 p-1">
                                <label for="name_en" class="sr-only">Nom En Anglais</label>
                                <input class="form-control dt_search_field" id="name_en" name="name_en" style="height:44px" placeholder="Nom en anglais..">
                            </div>
                            <div class="col-3 p-1">
                                <label for="group" class="sr-only">Groupe</label>
                                <select class="form-control form-control-alt dt_search_field" id="group" name="group">
                                    <option value="">Sélectionnez groupe</option>
                                    @foreach(\App\Models\DocumentTypeGroup::all() as $group)
                                        <option value="{{ $group->id }}">{{ $group->name_fr }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-3 p-1">
                                <label for="clear" class="sr-only">Clear</label>
                                <select class="form-control form-control-alt dt_search_field" id="clear" name="clear">
                                    <option value="">Sélectionnez clear</option>
                                    @foreach(\App\Models\MediaSheet::all() as $sheet)
                                        <option value="{{ $sheet->id }}">{{ $sheet->name_fr }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search" id="search-btn"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search" id="clear-search-btn"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>

                        </div>

                    </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="documents-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Formulaires des leads</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary"  id="documents-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter document</button>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="documents-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END leadsforms Table -->




    </div>
    <!-- END Page Content -->

    <!-- leadsforms Lead Add Modal -->
    <div class="modal" id="modal-documents-add" tabindex="-1" role="dialog" aria-labelledby="modal-documents-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-documents-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter document</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form documents lead Add -->
                                <form action="" method="POST" onsubmit="return false;" id="documents-add-form">
                                    <div class="form-group">
                                        <label for="add-form-title-en">Titre Anglais</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="add-form-title-en" name="add-form-title-en" aria-describedby="add-form-title-en-error" aria-invalid="false" placeholder="Titre Anglais..">
                                        <div id="add-form-title-en-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-title-fr">Titre Francais</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="add-form-title-fr" name="add-form-title-fr" aria-describedby="add-form-title-fr-error" aria-invalid="false" placeholder="Titre Français..">
                                        <div id="add-form-title-fr-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-group">Groupe</label>
                                        <select class="form-control form-control-alt" id="add-form-group" name="add-form-group" aria-describedby="add-form-group-error" aria-invalid="false">
                                            <option value="">Sélectionnez groupe</option>
                                            @foreach(\App\Models\DocumentTypeGroup::all() as $group)
                                                <option value="{{ $group->id }}">{{ $group->name_fr }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-group-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Client Propriétaire :</label>
                                        <div class="custom-control custom-switch custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" id="add-form-redirect" name="add-form-redirect">
                                            <label class="custom-control-label" for="add-form-redirect">Client peut téléverser?</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-documents-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter document</button>
                                    </div>
                                </form>
                                <!-- END Form documents Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END leadsforms Add Modal -->
    <!-- leadsforms Lead Edit Modal -->
    <div class="modal" id="modal-document-edit" tabindex="-1" role="dialog" aria-labelledby="modal-document-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-document-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Editer document</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form leadsforms lead edit -->
                                <form action="" method="POST" onsubmit="return false;" id="documents-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-title-en">Titre Anglais</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-title-en" name="edit-form-title-en" aria-describedby="edit-form-title-en-error" aria-invalid="false" placeholder="Titre Anglais..">
                                        <div id="edit-form-title-en-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-title-fr">Titre Francais</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-title-fr" name="edit-form-title-fr" aria-describedby="edit-form-title-fr-error" aria-invalid="false" placeholder="Titre Français..">
                                        <div id="edit-form-title-fr-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-group">Groupe</label>
                                        <select class="form-control form-control-alt" id="edit-form-group" name="edit-form-group" aria-describedby="edit-form-group-error" aria-invalid="false">
                                            <option value="">Sélectionnez groupe</option>
                                            @foreach(\App\Models\DocumentTypeGroup::all() as $group)
                                                <option value="{{ $group->id }}">{{ $group->name_fr }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-group-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Client Propriétaire :</label>
                                        <div class="custom-control custom-switch custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" id="edit-form-redirect" name="edit-form-redirect" checked="">
                                            <label class="custom-control-label" for="edit-form-redirect">Client peut téléverser?</label>
                                        </div>
                                    </div>
                                    @livewire('input.document.group')
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="edit-document-btn"><i class="fa fa-fw fa-plus mr-1"></i>Editer document</button>
                                    </div>
                                </form>
                                <!-- END Form leadsforms Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END leadsforms Add Modal -->

@endsection
