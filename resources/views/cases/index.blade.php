<?php
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
?>
@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <!-- Page JS Code -->
    {{-- <script src="{{ asset('js/pages/histocalls.js') }}"></script> --}}
    <script src="{{ asset('js/stopwatch.js') }}"></script>
    {{-- <script src="{{ asset('js/pages/leads/view.js') }}"></script> --}}
    @livewireScripts
    <script>
        window.userRole = {!! auth()->user()->toJson() !!};
        $('input[name="daterange"]').daterangepicker({
            opens: 'left',
            locale: {
                cancelLabel: 'Clear'
            }
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format(
                'YYYY-MM-DD'));
            $('input[name="date_start"]').val(start.format('YYYY-MM-DD'));
            $('input[name="date_end"]').val(end.format('YYYY-MM-DD'));
            // dashboardPage.initDashboardAnalytics();
            $('#cases-dt').DataTable().ajax.reload(null, false);
        });
        $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            $('input[name="date_start"]').val('');
            $('input[name="date_end"]').val('');
            $('#cases-dt').DataTable().ajax.reload(null, false);
        });

        const lang = $('input[name="lang"]').val();

        $("[name='tags[]']").select2({
            placeholder: lang === "fr" ? "Selectionnez mots clés" : "Select tag ...",
            allowClear: true,
            width: "100%"
        });
        $("[name='status']").select2({
            placeholder: "Status ...",
            allowClear: true,
            width: "100%"
        });
        $("[name='meeting']").select2({
            placeholder: "R1 .. R6 ...",
            allowClear: true,
            width: "100%"
        });
        $("[name='program']").select2({
            placeholder: lang === "fr" ? "Programmes ..." : "Programs ...",
            allowClear: true,
            width: "100%"
        });
        $("[name='campaign']").select2({
            placeholder: "Canal ...",
            allowClear: true,
            width: "100%"
        }).on("select2:unselect", function(e) {
            /*var data = e.params.data;
            console.log(data);*/
            Livewire.emit("campaignChanged", $("[name='campaign']").select2("val"))
        }).on("select2:select", function(e) {
            Livewire.emit("campaignChanged", $("[name='campaign']").select2("val"))
        });
        $("[name='influencer']").select2({
            placeholder: lang === "fr" ? "Représentant ..." : "Representing ...",
            allowClear: true,
            width: "100%"
        });
        $("[name='office']").select2({
            placeholder: lang === "fr" ? "Rencontre ..." : "Meeting ...",
            allowClear: true,
            width: "100%"
        });
        $("[name='conseiller']").select2({
            placeholder: lang === "fr" ? "Conseiller ..." : "Adviser ...",
            allowClear: true,
            width: "100%"
        });
        $("[name='support']").select2({
            placeholder: "Support ...",
            allowClear: true,
            width: "100%"
        });

        $("[name='source']").select2({
            placeholder: lang === "fr" ? "Source ..." : "Source ...",
            width: "100%",
            allowClear: true

        });
        $("[name='field-activity']").select2({
            placeholder: "Domaine d’activité ...",
            width: "100%",
            allowClear: true

        });
        Livewire.on("updatedSource", obj => {
            console.log("test");
            $("[name='source']").select2({
                placeholder: lang === "fr" ? "Source ..." : "Source ...",
                width: "100%",
                allowClear: true

            });
        });
    </script>
    <script src="{{ asset('js/pages/cases/list.js') }}"></script>
@endsection

@section('css_after')
    <!-- Styles -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        table.dataTable thead>tr>td.sorting,
        table.dataTable thead>tr>td.sorting_asc,
        table.dataTable thead>tr>td.sorting_desc,
        table.dataTable thead>tr>th.sorting,
        table.dataTable thead>tr>th.sorting_asc,
        table.dataTable thead>tr>th.sorting_desc {
            padding-right: 0;
        }

        .select2-selection__rendered {
            line-height: 39px !important;
        }

        .select2-container .select2-selection--single {
            height: 42px !important;
        }

        .select2-selection__arrow {
            height: 43px !important;
        }

        /* .recent-comment {
                    color: white;
                    background-color: #FD971F;
                }
                .recent-comment-client {
                    color: white;
                    background-color: #ff6363;
                    height: 100%;
                } */
    </style>
    @livewireStyles
@endsection
@section('content')
    @include('layouts.partials.hero', [
        'title' => 'Clients',
        'subTitle' => 'Lists',
        'navItems' => ['Admin', 'Clients'],
    ])

    <!-- Page Content -->
    <div class="content">
        <input type="hidden" name="lang" value="{{ $lang }}">
        <!-- histocalls Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>{{ __('lang.lead_filter_title') }}</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <form id="search-form" name="search-form" action="" method="post" onsubmit="return false">
                            @csrf
                            <div class="form-group form-row">
                                <div class="col-md-12">
                                    <div class="row p-2">
                                        <div class="col-3 p-1">
                                            <label for="customer" class="sr-only">Customer</label>
                                            <input class="form-control dt_search_field dt_search_customer" id="customer"
                                                name="customer" style="height:44px" placeholder="Customer..">
                                        </div>
                                        <div class="col-3 p-1">
                                            <label for="email" class="sr-only">{{ __('lang.lead_filter_email') }}</label>
                                            <input class="form-control dt_search_field dt_search_email" id="email"
                                                name="email" style="height:44px" placeholder="Email">
                                        </div>
                                        <div class="col-3 p-1">
                                            <label for="phone" class="sr-only">{{ __('lang.lead_filter_phone') }}</label>
                                            <input class="form-control dt_search_field dt_search_phone" id="phone"
                                                name="phone" style="height:44px" placeholder="Phone">
                                        </div>
                                        <div class="col-3 p-1">
                                            {{ Form::select('status', ['n/d' => __('lang.lead_filter_status_untreated')] + \App\Models\AdmissionState::pluck($lang === 'fr' ? 'label' : 'labelEng', 'id')->toArray(), null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>

                                        <div class="col-3 p-1">
                                            {{ Form::select('tags[]', \App\Models\Tag::pluck('tag_name', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>
                                        <div class="col-3 p-1">
                                            {{ Form::select('program', \App\Models\Program::pluck($lang === 'fr' ? 'name' : 'labelEng', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>
                                        <div class="col-3 p-1">
                                            {{ Form::select('campaign', \App\Models\LeadFormMarketingCampaign::pluck('campaign_name', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>
                                        <div class="col-3 p-1">
                                            {{ Form::select('influencer', \App\Models\LeadForm::whereIn('lead_form_marketing_campaign_id', [7, 16])->pluck('title', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>
                                        @livewire('input.source')
                                        <div class="col-3 p-1">
                                            <label for="office"
                                                class="sr-only">{{ __('lang.lead_filter_status') }}</label>
                                            <select name="office" id="office"
                                                class="form-control form-control-alt dt_search_field" data-control="select2"
                                                multiple="multiple" aria-describedby="tags-error" aria-invalid=""
                                                style="height: 42px;border: 1px solid #aaa;">
                                                <option value="n/d">{{ __('lang.lead_filter_status_untreated') }}
                                                </option>
                                                <option value="office">{{ __('lang.lead_filter_status_come_office') }}
                                                </option>
                                                <option value="conference">{{ __('lang.lead_filter_status_video_conf') }}
                                                </option>
                                            </select>
                                        </div>

                                        <div class="col-3 p-1">
                                            <label for="meeting"
                                                class="sr-only">{{ __('lang.lead_filter_status') }}</label>
                                            <select name="meeting" id="meeting"
                                                class="form-control form-control-alt dt_search_field" data-control="select2"
                                                multiple="multiple" aria-describedby="tags-error" aria-invalid=""
                                                style="height: 42px;border: 1px solid #aaa;">
                                                <option value="1">R1</option>
                                                <option value="2">R2</option>
                                                <option value="3">R3</option>
                                                <option value="4">R4</option>
                                                <option value="5">R5</option>
                                            </select>
                                        </div>
                                        @unlessconseiller
                                            <div class="col-3 p-1">
                                                <label for="conseiller"
                                                    class="sr-only">{{ __('lang.lead_filter_Adviser') }}</label>
                                                <?php
                                                $list_roles = [3];
                                                if (\Illuminate\Support\Facades\Auth::user()->isAdmin() || \Illuminate\Support\Facades\Auth::user()->isVerificateur()) {
                                                    array_push($list_roles, 2);
                                                }
                                                $queryConseiller = \App\Models\User::where('status', 1)->where(function ($query) use ($list_roles) {
                                                    $query->where(function ($query) use ($list_roles) {
                                                        $query->whereIn('role_id', $list_roles);
                                                    });
                                                    if (\Illuminate\Support\Facades\Auth::user()->isManager()) {
                                                        $query->orWhere(function ($query) {
                                                            $query->where('id', \Illuminate\Support\Facades\Auth::id());
                                                        });
                                                    }
                                                });
                                                $queryConseillerArray = ['n/d' => __('lang.lead_filter_status_unaffected')] + $queryConseiller->pluck('name', 'id')->toArray();
                                                ?>
                                                {{ Form::select('conseiller', $queryConseillerArray, null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                            </div>
                                            @livewire('input.toggle-state', ['name' => 'call_by_conseiller'])
                                        @endconseiller
                                        @support_deny
                                            <div class="col-3 p-1">
                                                <label for="support" class="sr-only">Support</label>
                                                <?php
                                                $list_roles = [7];
                                                $querySupport = \App\Models\User::where('status', 1)
                                                    ->whereIn('role_id', $list_roles)
                                                    ->pluck('name', 'id');
                                                $querySupportArray = ['n/d' => __('lang.lead_filter_status_unaffected')] + $querySupport->toArray();
                                                ?>
                                                {{ Form::select('support', $querySupportArray, null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                            </div>
                                            @livewire('input.toggle-state', ['name' => 'call_by_support'])
                                            <div class="col-3 p-1">
                                                <?php
                                                $program_field = \App\Models\ProgramField::get()
                                                    ->pluck('label_fr', 'id')
                                                    ->toArray();
                                                ?>
                                                {{ Form::select('field-activity', $program_field, null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                            </div>
                                        @endsupport_deny
                                        <div class="col-3 p-1">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text input-group-text-alt">
                                                            <i class="far fa-calendar-alt"></i>
                                                        </span>
                                                    </div>
                                                    @php
                                                        $firstDay = \Carbon\Carbon::parse('2021-10-01')->format('m/d/Y');
                                                        $lastDay = \Carbon\Carbon::today()->format('m/d/Y');
                                                        $current_month = $firstDay . ' - ' . $lastDay;
                                                    @endphp
                                                    <input type="text" class="form-control form-control" id="daterange"
                                                        name="daterange" value="{{ $current_month }}">
                                                    <input type="hidden" name="date_end" class="dt_search_field"
                                                        value="{{ $lastDay }}" />
                                                    <input type="hidden" name="date_start" class="dt_search_field"
                                                        value="{{ $firstDay }}" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-1 col-3">
                                            @livewire('input.cities', ['value' => null])
                                        </div>
                                        @livewire('input.toggle-state', ['name' => 'payment_proof'])
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-5 p-1">
                                                    <button type="button"
                                                        class="btn btn-alt-primary btn-block submit-search"
                                                        id="search-btn"><i
                                                            class="fa fa-fw fa-search mr-1"></i>{{ __('lang.lead_filter_search_btn') }}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="cases-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">{{ __('lang.cases_list_title') }}</h3>
                    {{-- <div class="block-options">
                        <button type="button" class="btn btn-alt-primary" id="histocall-customer-table-add"><i style="font-size:18px" class="fa fa-phone fa-sm mr-2"></i>faire appel</button>
                    </div> --}}
                </div>
                <h4></h4>

                <div class="table-responsive">
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                    <table class="table table-bordered table-striped table-vcenter" id="cases-dt">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END histocalls Table -->



    </div>
    <!-- END Page Content -->

    <!-- case Edit Modal -->
    <div class="modal" id="modal-case-edit" tabindex="-1" role="dialog" aria-labelledby="modal-case-edit"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-case-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">{{ __('lang.cases_list_modal_title') }}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-8">
                                <!-- Form case Add -->
                                <form action="" method="POST" onsubmit="return false;" id="case-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-firstname">{{ __('lang.cases_list_modal_name') }}</label>
                                        <input type="text" class="form-control form-control-alt"
                                            id="edit-form-firstname" name="edit-form-firstname"
                                            placeholder="{{ __('lang.cases_list_modal_name') }} .."
                                            aria-describedby="edit-form-firstname-error" aria-invalid="false">
                                        <div id="edit-form-firstname-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-lastname">{{ __('lang.cases_list_modal_surname') }}</label>
                                        <input type="text" class="form-control form-control-alt"
                                            id="edit-form-lastname" name="edit-form-lastname"
                                            placeholder="{{ __('lang.cases_list_modal_surname') }} .."
                                            aria-describedby="edit-form-lastname-error" aria-invalid="false">
                                        <div id="edit-form-lastname-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-email">{{ __('lang.cases_list_modal_email') }}</label>
                                        <input type="email" class="form-control form-control-alt" id="edit-form-email"
                                            name="edit-form-email"
                                            placeholder="{{ __('lang.cases_list_modal_email') }} .."
                                            aria-describedby="edit-form-email-error" aria-invalid="false">
                                        <div id="edit-form-email-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            for="edit-form-languagelevel">{{ __('lang.cases_list_modal_language_lvl') }}</label>
                                        <select class="form-control form-control-alt" id="edit-form-languagelevel"
                                            name="edit-form-languagelevel"
                                            aria-describedby="edit-form-languagelevel-error" aria-invalid="false">
                                            <option value="">{{ __('lang.cases_list_modal_select_language_lvl') }}
                                            </option>
                                            @foreach (\App\Models\EnglishLevel::all() as $level)
                                                <option value="{{ $level->id }}">{{ $level->english_level_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-languagelevel-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            for="edit-form-adresse1">{{ __('lang.cases_list_modal_address_line_1') }}</label>
                                        <input type="text" class="form-control form-control-alt"
                                            id="edit-form-adresse1" name="edit-form-adresse1"
                                            placeholder="{{ __('lang.cases_list_modal_address_line_1') }} .."
                                            aria-describedby="edit-form-adresse1-error" aria-invalid="false">
                                        <div id="edit-form-adresse1-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            for="edit-form-adresse2">{{ __('lang.cases_list_modal_address_line_2') }}</label>
                                        <input type="text" class="form-control form-control-alt"
                                            id="edit-form-adresse2" name="edit-form-adresse2"
                                            placeholder="{{ __('lang.cases_list_modal_address_line_2') }} .."
                                            aria-describedby="edit-form-adresse2-error" aria-invalid="false">
                                        <div id="edit-form-adresse2-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            for="edit-form-postalcode">{{ __('lang.cases_list_modal_zip_code') }}</label>
                                        <input type="text" class="form-control form-control-alt"
                                            id="edit-form-postalcode" name="edit-form-postalcode"
                                            placeholder="{{ __('lang.cases_list_modal_zip_code') }} .."
                                            aria-describedby="edit-form-postalcode-error" aria-invalid="false">
                                        <div id="edit-form-postalcode-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            for="edit-form-experience">{{ __('lang.cases_list_modal_professional_experience') }}</label>
                                        <input type="number" class="form-control form-control-alt"
                                            id="edit-form-experience" name="edit-form-experience"
                                            placeholder="{{ __('lang.cases_list_modal_professional_experience') }}.."
                                            aria-describedby="edit-form-experience-error" aria-invalid="false">
                                        <div id="edit-form-experience-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-info btn-block" id="update-case-btn"><i
                                                class="fa fa-fw fa-check mr-1"></i>{{ __('lang.cases_list_modal_title') }}</button>
                                    </div>
                                </form>
                                <!-- END Form case Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1"
                            data-dismiss="modal">{{ __('lang.cases_list_modal_close_btn') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END case Edit Modal -->
    <!-- histocall Customer Add Modal -->
    <div class="modal" id="modal-histocall-customer-add" tabindex="-1" role="dialog"
        aria-labelledby="modal-histocall-customer-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-histocall-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter appel client</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form histocall customer Add -->
                                <form action="" method="POST" onsubmit="return false;"
                                    id="histocall-add-customer-form">
                                    {{-- @ahaloua the request should be like this --}}
                                    <div class="form-group">
                                        <p style="line-height: 24px;font-size: 26px;width: 100%;"><strong>Téléphone
                                                :</strong><span id="customer-phone"></span></p>
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <div class="col-md-6 input-wrapper">
                                            <div class="buttons-wrapper">
                                                <button class="btn btn-alt-secondary" id="start-cronometer">Start
                                                    Stopwatch</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <input type="hidden" name="add-form-duration" id="add-form-duration">
                                        <div id="timer" class="col-md-10" style="display: none;">
                                            <div class="text-center clock-wrapper h1">
                                                <span class="hours">00</span>
                                                <span class="dots">:</span>
                                                <span class="minutes">00</span>
                                                <span class="dots">:</span>
                                                <span class="seconds">00</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <div class="buttons-wrapper">
                                            <button class="btn btn-alt-secondary" id="resume-timer"
                                                style="display: none;"><i class="fa fa-fw fa-play"></i> Resume
                                                Timer</button>
                                            <button class="btn btn-alt-secondary" id="stop-timer"
                                                style="display: none;"><i class="fa fa-fw fa-pause"></i> Stop
                                                Timer</button>
                                            <button class="btn btn-alt-secondary" id="reset-timer"
                                                style="display: none;"><i class="fa fa-fw fa-trash-restore"></i> Reset
                                                Timer</button>
                                        </div>
                                    </div>
                                    <div class="form-group mt-4">
                                        <label for="add-form-call_descr" className="col-form-label">Note :</label>
                                        <textarea type="text" class="form-control form-control-alt" id="add-form-call_descr" name="add-form-call_descr"
                                            placeholder="Notes.." aria-describedby="add-form-call_descr-error" aria-invalid="false"></textarea>
                                        <div id="add-form-call_descr-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" id="add-histocall-customer-btn-bis"
                                            class="btn btn-alt-primary btn-block"><i
                                                class="fa fa-fw fa-plus mr-1"></i>Ajouter appel</button>
                                    </div>
                                </form>
                                <!-- END Form histocall Add -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END histocall Add Modal -->
    <!-- comment info Modal -->
    <div class="modal" id="modal-comment-info" tabindex="-1" role="dialog" aria-labelledby="modal-comment-info"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">{{ __('lang.modal_comment_info_title') }}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                @livewire('modal.comment-info')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END comment Info Modal -->
@endsection
