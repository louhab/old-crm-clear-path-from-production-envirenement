<div class="row items-push">
    <div class="col-md-12">
        <div class="block block-rounded block-themed">

            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 220px;">Nom du doc</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="text-center" scope="row">Résultat TCF Canada</th>
                    <td class="text-center">
                        @include('cases.steps.partials.document', ['document' => 'Tcf canada', 'collectionName' => 'TCF_RESULT', 'model' => $customer_case->customer])
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">Résultat TEF Canada </th>
                    <td class="text-center">
                        @include('cases.steps.partials.document', ['document' => 'Tef Canada', 'collectionName' => 'TEF_RESULT', 'model' => $customer_case->customer])
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">Résultat IELTS Anglais </th>
                    <td class="text-center">
                        @include('cases.steps.partials.document', ['document' => 'ielts anglais', 'collectionName' => 'IELTS_RESULT', 'model' => $customer_case->customer])
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>
