<div class="row items-push">
    <div class="col-md-12">
        <div class="block block-rounded block-themed">


            <div class="alert alert-warning py-2 mb-0">
                <i class="fa fa-exclamation-triangle mr-1"></i> Votre dossier est en cours de traitement par les autorités canadiennes
            </div>

        </div>
    </div>
</div>
