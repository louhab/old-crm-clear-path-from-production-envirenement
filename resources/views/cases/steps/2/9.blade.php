<div class="row items-push">
    <div class="col-md-12">
        <div class="block block-rounded block-themed">

            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center"   style="width: 300px;">Nom du doc</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="text-center" scope="row">Attestation de travail (description détaillé des fonctions et des taches )</th>
                    <td class="text-center">
                        @include('cases.steps.partials.document', ['document' => 'Attestation de travail', 'collectionName' => 'JOB_CERTIFICATE', 'model' => $customer_case->customer])
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">Contrat de travail </th>
                    <td class="text-center">
                        @include('cases.steps.partials.document', ['document' => 'Contrat de travail', 'collectionName' => 'JOB_CONTRACT', 'model' => $customer_case->customer])
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">4 derniers relevés bancaire </th>
                    <td class="text-center">
                        @include('cases.steps.partials.document', ['document' => 'Relevé bancaire', 'collectionName' => 'BANK_STATEMENT', 'model' => $customer_case->customer])
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">Fiche entro-prometrique</th>
                    <td class="text-center">
                        @include('cases.steps.partials.document', ['document' => 'Fiche entro-prometrique', 'collectionName' => 'ENTRO_PROOMETRIC_SHEET', 'model' => $customer_case->customer])
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">Casier judiciaire </th>
                    <td class="text-center">
                        @include('cases.steps.partials.document', ['document' => 'Casier judiciaire', 'collectionName' => 'CRIMINAL_RECORD', 'model' => $customer_case->customer])
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>
