<div class="row items-push">
    <div class="col-md-12">
        <div class="block block-rounded block-themed">
            <div class="alert alert-warning py-2 mb-0">
                <i class="fa fa-exclamation-triangle mr-1"></i> Votre dossier est en cours de traitement par l'agent
            </div>
            <br>
            <div class="alert alert-primary py-2 mb-0">
                <i class="fa fa-exclamation-triangle mr-1"></i> Merci de compléter : <br>
                <br>
               <h3>Liste des formations souhaités</h3>
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="example-checkbox-custom1" name="example-checkbox-custom1" checked="">
                                <label class="custom-control-label" for="example-checkbox-custom1">Formation 1</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="example-checkbox-custom2" name="example-checkbox-custom2">
                                <label class="custom-control-label" for="example-checkbox-custom2">Formation 2</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="example-checkbox-custom3" name="example-checkbox-custom3">
                                <label class="custom-control-label" for="example-checkbox-custom3">Formation 3</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="example-checkbox-custom1" name="example-checkbox-custom1" checked="">
                                <label class="custom-control-label" for="example-checkbox-custom1">Formation 1</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="example-checkbox-custom2" name="example-checkbox-custom2">
                                <label class="custom-control-label" for="example-checkbox-custom2">Formation 2</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="example-checkbox-custom3" name="example-checkbox-custom3">
                                <label class="custom-control-label" for="example-checkbox-custom3">Formation 3</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="example-checkbox-custom1" name="example-checkbox-custom1" checked="">
                                <label class="custom-control-label" for="example-checkbox-custom1">Formation 1</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="example-checkbox-custom2" name="example-checkbox-custom2">
                                <label class="custom-control-label" for="example-checkbox-custom2">Formation 2</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="example-checkbox-custom3" name="example-checkbox-custom3">
                                <label class="custom-control-label" for="example-checkbox-custom3">Formation 3</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="example-checkbox-custom1" name="example-checkbox-custom1" checked="">
                                <label class="custom-control-label" for="example-checkbox-custom1">Formation 1</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="example-checkbox-custom2" name="example-checkbox-custom2">
                                <label class="custom-control-label" for="example-checkbox-custom2">Formation 2</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="example-checkbox-custom3" name="example-checkbox-custom3">
                                <label class="custom-control-label" for="example-checkbox-custom3">Formation 3</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-alt-primary">
                        Valider
                    </button>
                </div>

            </div>
        </div>
    </div>
</div>
