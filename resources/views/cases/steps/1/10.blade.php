<div class="row items-push">
    <div class="col-md-12">
        <div class="block block-rounded block-themed">


            <div class="alert alert-warning py-2 mb-0">
                <i class="fa fa-exclamation-triangle mr-1"></i> Votre dossier est en cours de traitement
            </div>

            <br>


            <div class="alert alert-primary py-2 mb-0">
                <i class="fa fa-exclamation-triangle mr-1"></i> Merci de compléter l'informations <br>
                <ul>
                    <li>Joindre le document correspendant : recu pour l'empriente</li>
                </ul>
                @if(!$customer_case->customer->cv_id)
                    <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'empriente'])
                    </form>
                @else
                    @include('cases.steps.partials.show-file', ['document' => 'empriente', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                @endif
            </div>

        </div>
    </div>
</div>
