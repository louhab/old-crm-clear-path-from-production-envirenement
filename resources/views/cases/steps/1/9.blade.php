<div class="row items-push">
    <div class="col-md-12">
        <div class="block block-rounded block-themed">

            <h3>Etudiant</h3>

            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 320px;">Nom du doc</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="text-center" scope="row">PHOTO D’IDENTITÉ RÉCENTE, FORMAT PASSEPORT (35 mm X 45 mm)</th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'photo'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'photo', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">RELEVÉS DE NOTES OFFICIEL </th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Relevé de note'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Relevé de note', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">FICHE ANTHROPOMÉTRIQUE </th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Fiche Enthropometrqiue'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Fiche Enthropometrqiue', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">COPIE DU CASIER JUDICIAIRE (Tribunal) </th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Casier judiciare'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Casier judiciare', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">LETTRE D’ACCEPTATION DE L'UNIVERSITÉ</th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Lettre'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Lettre', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>

                </tbody>
            </table>

            <h3>Parrain (Pere ou Mere)</h3>
            <h5>SALARIÉ OU RETRAITÉ:</h5>

            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 320px;">Nom du doc</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="text-center" scope="row">RELEVÉ DES QUATRES DERNIERS MOIS DE VOTRE BANQUE (Compte Personnel)</th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'photo'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'photo', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">ATTESTATION DE SALAIRE (PÈRE ET/OU MÈRE) </th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Relevé de note'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Relevé de note', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">ATTESTATION DE TRAVAIL DES PARENTS (PÈRE ET/OU MÈRE)</th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Fiche Enthropometrqiue'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Fiche Enthropometrqiue', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">4 DERNIERS BULLETINS DE PAIES </th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Casier judiciare'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Casier judiciare', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">ATTESTATION PENSION</th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Lettre'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Lettre', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">ENGAGEMENT ET PRISE EN CHARGE (VOIR MODÈLE)</th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Lettre'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Lettre', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">UNE ATTESTATION DES BIENS IMMOBILIERS : maison, terrain, magasin ... (type de bien, sa valeur,</th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Lettre'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Lettre', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>

                </tbody>
            </table>

            <h5>PROFESSION LIBÉRALE:</h5>

            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 320px;">Nom du doc</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="text-center" scope="row">RELEVÉ DES QUATRES DERNIERS MOIS DE VOTRE BANQUE (Compte Personnel)</th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'photo'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'photo', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">RELEVÉ DES QUATRES DERNIERS MOIS DE VOTRE BANQUE (Compte Entreprise)</th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Relevé de note'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Relevé de note', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">COPIE DU STATUT DE L’ENTREPRISE</th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Fiche Enthropometrqiue'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Fiche Enthropometrqiue', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>


                <tr>
                    <th class="text-center" scope="row">ENGAGEMENT ET PRISE EN CHARGE (VOIR MODÈLE)</th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Lettre'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Lettre', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">UNE ATTESTATION DES BIENS IMMOBILIERS : maison, terrain, magasin ... (type de bien, sa valeur,</th>
                    <td>
                        @if(!$customer_case->customer->cv_id)
                            <form id="add-media-cv-form" action="{{ route('customer-add-media',['customer' => $customer_case->customer, 'collectionName' => 'CVs']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @include('cases.steps.partials.upload-file', ['name' => 'file-input-media', 'document' => 'Lettre'])
                            </form>
                        @else
                            @include('cases.steps.partials.show-file', ['document' => 'Lettre', 'model' => $customer_case->customer, 'collectionName' => 'CVs', 'deleteId' => 'delete-cv-href'])
                        @endif
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>


</div>
