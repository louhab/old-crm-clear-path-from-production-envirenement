<div class="row items-push">
    <div class="col-md-12">
        <div class="block block-rounded block-themed">

            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 220px;">Nom du doc</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="text-center" scope="row">CV</th>
                    <td>
                        @include('cases.steps.partials.document', ['document' => 'CV', 'collectionName' => 'PREPARED_CV', 'model' => $customer_case->customer])
                    </td>
                </tr>
                <tr>
                    <th class="text-center" scope="row">Lettre de motiviation </th>
                    <td>
                        @include('cases.steps.partials.document', ['document' => 'Lettre de motiviation', 'collectionName' => 'MOTIVATION_LETTER', 'model' => $customer_case->customer])
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>


</div>
