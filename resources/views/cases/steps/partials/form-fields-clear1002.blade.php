@php
    $groupedResultsArray = [];
@endphp
@foreach($form->fields()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get() as $field)
    @php
        $groupedResultsArray[$field->customer_field_group_id][] = $field;
    @endphp
@endforeach
<form action="{{ route('customer-update-clear1002', $customer_case->customer_id) }}" method="POST" id="customer-form" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <?php
        $sheet_id = null;
        if ($form instanceof App\Models\InformationSheet) {
            $sheet_id = $form->id;
            echo '<input type="hidden" name="sheet" value="'.$sheet_id.'" />';
        }
    ?>
    <input type="hidden" name="combs" value="" />
    @foreach($groupedResultsArray as $key => $groupedFieldArr)
        @foreach($groupedFieldArr as $field)
            @if($loop->first)
                <!-- <span class="badge badge-danger">{{ $field->group->name_fr }}</span> -->
                <div class="row">
            @endif
            <div class="col-md-6">
                <?php
                    $field_name = $field->field_name;
                    $old_value = $customer_case->customer->$field_name;

                    if($field_name == "lead_status") {
                        $values_ = $field->select_model::where("visible", 1)->pluck($field->select_model_label, $field->select_model_id);
                    }
                    else {
                        if ($field->select_model == null)
                            continue;
                        $values_ = $field->select_model::pluck($field->select_model_label, $field->select_model_id);
                    }
                    $attributes = [
                        'class' => 'form-control form-control-alt' . (isset($errors) && $errors->has($field_name) ? ' is-invalid' : (isset($errors) && $errors->has($field_name) ? 'true' : 'false')),
                        'aria-describedby' => $field_name . '-error',
                        'aria-invalid' => ''
                    ];

                    $attributes['data-placeholder'] = __('lang.' . $field->field_name) . '..';
                    $attributes['multiple'] = "multiple";
                    $attributes['name'] = $field_name . "[]";
                    // $attributes['style'] = "width: 400px;";
                ?>
                <div class="form-group">
                    <label for="{{ $field_name }}">{{ __('lang.' . $field->field_name)}}</label>
                    @if($field->pivot->required)
                        <span class="text-danger">*</span>
                    @endif
                    {{ Form::select($field_name, $values_, old($field_name, $old_value), $attributes) }}
                    <div id="{{ $field_name }}-error" class="invalid-feedback">{{ isset($errors) && $errors->has($field_name) ? $errors->first($field_name) : '' }}</div>
                </div>

            </div>
        @endforeach
        </div>
    @endforeach
    <div class="form-group" style="padding-top: 20px;">
        <button type="submit" class="btn btn-lg btn-alt-primary btn-block"><i class="fa fa-fw fa-check mr-1"></i>Enregistrer</button>
    </div>

</form>
