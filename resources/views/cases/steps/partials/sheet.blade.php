<div>
    @if(!isCustomer())
        @php
            $params = ['customer_case'=> $customer_case, 'information_sheet' => $informationSheet];
            if (isset($join))
                $params['join'] = $join;
        @endphp
        <a class="btn btn-sm btn-outline-danger " href="{{ route('edit-information-sheet', $params) }}" target="_blank">
            {{ __("lang.customer_fill_sheet_btn") }}
        </a>
    @else
        @php
            $params = ['customer_case'=> $customer_case, 'information_sheet' => $informationSheet];
            if (isset($join))
                $params['join'] = $join;
        @endphp
        <a class="btn btn-sm btn-outline-secondary" href="{{ route('customer-edit-information-sheet', $params) }}" target="_blank">
            <i class="fas fa-edit"></i>
        </a>
    @endif
</div>
