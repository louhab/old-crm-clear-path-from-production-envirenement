<div class="row items-push">
    <div class="col-md-12">
        <div class="block block-rounded block-themed">

            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 250px;">Nom du contrat</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="text-center" scope="row">Contrat Immigrant</th>
                    <td class="text-center">
                            @include('cases.steps.partials.document', ['document' => 'Contrat', 'collectionName' => 'CUSTOMER_CONTRACT_SIGNABLE', 'model' => $customer_case->customer])
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
