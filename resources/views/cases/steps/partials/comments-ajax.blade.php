@foreach($comments as $comment)
    <div class="media push comment-wrapper" data-lid="{{ $comment->id }}" style="border:1px solid #e8e8e8; padding: 5px;margin-bottom: 6px;">
        <div class="media-body">
            <p style="white-space: pre-wrap;margin: 0;"><strong>{{ is_null($comment->author) ? "Utilisateur Supprimé": $comment->author->name }}</strong> - <u>{{ $comment->created_at }}</u></p>
            <p style="white-space: pre-wrap;margin: 0;">{{ $comment->comment }} </p>
        </div>
    </div>
@endforeach
