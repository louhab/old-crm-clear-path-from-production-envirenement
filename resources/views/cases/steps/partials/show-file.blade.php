<div class="document-wrapper">
    @if (!isCustomer())
        <a class="btn btn-sm btn-light document-in"
            href="{{ route('get-media', $model->getFirstMedia($collectionName)) }}">
            <i class="fa fa-fw fa-download text-success"></i>
        </a>
        @if (Illuminate\Support\Facades\Auth::user()->isAdmin() ||
                Illuminate\Support\Facades\Auth::user()->isManager() ||
                Illuminate\Support\Facades\Auth::user()->isVerificateur())
            <a data-formId="form-{{ $formId }}" class="btn btn-sm btn-light document-in delete-document"
                href="javascript:void(0)">
                <i class="fa fa-fw fa-times text-danger"></i>
            </a>
        @endif
        @if (
            !$media->getCustomProperty('customer_owner') &&
                (Illuminate\Support\Facades\Auth::user()->isAdmin() ||
                    Illuminate\Support\Facades\Auth::user()->isManager() ||
                    Illuminate\Support\Facades\Auth::user()->isVerificateur()))
            @if ($media->getCustomProperty('visible_for_customer'))
                <a data-formId="form-{{ $formId . '2' }}" data-property="visible_for_customer"
                    class="btn btn-sm btn-light document-in toggle-property" href="javascript:void(0)">
                    <i class="fa fa-fw fa-eye text-success"></i>
                </a>
            @else
                <a data-formId="form-{{ $formId . '2' }}" data-property="visible_for_customer"
                    class="btn btn-sm btn-light document-in toggle-property" href="javascript:void(0)">
                    <i class="fa fa-fw fa-eye-slash text-danger"></i>
                </a>
            @endif
        @endif
        @if ($signable)
            @if (!$media->getCustomProperty('is_signed') && strlen($media->getCustomProperty('pending_file_id')) > 1)
                <p class="document-in">En attente de la signature client</p>
            @else
                <a data-formId="form-{{ $formId . '1' }}" class="btn btn-light document-in sign-document"
                    href="javascript:void(0)">
                    <i class="fa fa-fw fa-pencil-alt text-primary"></i>
                    {{ getSigningLabel($media) }}
                </a>
            @endif
        @endif
    @else
        @if ($media_name)
            @livewire('customer.partials.show', ['customer_case' => $customer_case, 'collectionName' => $collectionName, 'media' => $media, 'customer_case' => $customer_case ?? null, 'media_name' => $media_name ?? null])
        @else
            @if ($media->getCustomProperty('customer_owner'))
                <a class="btn btn-sm btn-light document-in"
                    href="{{ route('get-media', $model->getFirstMedia($collectionName)) }}">
                    <i class="fa fa-fw fa-download text-success"></i>
                </a>
                @if (!$media->getCustomProperty('validated'))
                    <a data-formId="form-{{ $formId }}" class="btn btn-sm btn-light document-in delete-document"
                        href="javascript:void(0)">
                        <i class="fa fa-fw fa-times text-danger"></i>
                    </a>
                @endif
            @else
                @if ($media->getCustomProperty('visible_for_customer'))
                    <a class="btn btn-sm btn-light document-in"
                        href="{{ route('get-media', $model->getFirstMedia($collectionName)) }}">
                        <i class="fa fa-fw fa-download text-success"></i>
                    </a>
                @endif
                @if ($signable)
                    @if ($media->getCustomProperty('is_signed') || strlen($media->getCustomProperty('pending_file_id')) > 1)
                        <a data-formId="form-{{ $formId . '1' }}" class="btn btn-light document-in sign-document"
                            href="javascript:void(0)" target="_blank">
                            <i class="fa fa-fw fa-pencil-alt text-primary"></i>
                            {{ getSigningLabel($media) }}
                        </a>
                    @endif
                @endif
            @endif
        @endif
    @endif
    <form id="form-{{ $formId }}"
        action="{{ route('customer-delete-media', ['customer' => $model, 'collectionName' => $collectionName]) }}"
        method="POST" style="display: none;">
        @csrf
        @method('DELETE')
    </form>
    <form id="form-{{ $formId . '2' }}" action="{{ route('toggle-media-property', ['media' => $media]) }}"
        method="POST" style="display: none;">
        @csrf
    </form>
    @if ($signable)
        @if (!isCustomer())
            @if (!$media->getCustomProperty('is_signed') && $media->getCustomProperty('pending_file_id') > 0)
                <!--            <p>En attente de la signature client</p>-->
            @else
                <form id="form-{{ $formId . '1' }}"
                    action="{{ route(getSigningRouteName($media), ['customer' => $model, 'collectionName' => $collectionName]) }}"
                    method="POST" style="display: none;">
                    @csrf
                </form>
            @endif
        @else
            @if ($media->getCustomProperty('is_signed') || strlen($media->getCustomProperty('pending_file_id')) > 0)
                <form id="form-{{ $formId . '1' }}"
                    action="{{ route(getSigningRouteName($media), ['customer' => $model, 'collectionName' => $collectionName]) }}"
                    method="POST" style="display: none;">
                    @csrf
                </form>
            @endif
        @endif
    @endif
</div>
