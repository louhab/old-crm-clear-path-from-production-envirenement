<div class="row items-push">
    <div class="col-md-8">
        <div class="block block-rounded block-themed">
            <div>
                <h3>Devis <a href="{{ route('quotes') }}" target="_blank" class="btn btn-outline-danger" id="evaluation-form-update"><i class="fa fa-fw fa-plus mr-1"></i>Nouveau devis</a></h3>
            </div>
            @if($customer_case->customer->quotes && $customer_case->customer->quotes->count())
                <table class="table table-bordered table-vcenter">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 140px;">#</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($customer_case->customer->quotes as $quote)
                        <tr>
                            <th class="text-center" scope="row">#DEV{{ sprintf('%06d', $quote->id) }}</th>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ route('quote-download', $quote) }}" class="btn btn-sm btn-light js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Devis Client">
                                        <i class="fa fa-fw fa-download"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <p class="mb-0">Aucun devis crée !</p>
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="block block-rounded block-themed">
            <div class="block-content">
                <div class="block block-rounded d-flex flex-column" style="border: 1px solid #e2e2e2;">
                    <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                        <dl class="mb-0">
                            <dt class="font-size-h2 font-w700" style="font-size: 1.25rem;">Documents</dt>
                            <dd class="text-muted mb-0" style="margin-top: 10px;">
                                <div class="form-group">
                                    <label for="">Customer rapport</label>

                                    @include('cases.steps.partials.document', ['document' => 'Rapport de points', 'collectionName' => 'POINTS_REPORT', 'model' => $customer_case->customer])
                                </div>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
