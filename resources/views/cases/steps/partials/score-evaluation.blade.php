<div class="row">
    <div class="col-sm-4">
        <div class="block block-rounded d-flex flex-column" style="border: 1px solid #e2e2e2;">
            <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                <dl class="mb-0">
                    <dt class="font-size-h1 font-w700" style="font-size: 3.25rem; {{ $customer_case->evaluation->ranking_id ? "color: " . getRankingTextColor($customer_case->evaluation->ranking_id) : "" }}">{{ $customer_case->evaluation->score ?? "n/d" }}</dt>
                    <dd class="text-muted mb-0">{{ $customer_case->evaluation->ranking_id ? $customer_case->evaluation->ranking->ranking_label : "n/d" }}</dd>
                    <dd class="text-muted mb-0" style="margin-top: 10px;">
                        @if($customer_case->evaluation->ranking_id)
                            <div class="js-rating" style="cursor: pointer;">
                                <i data-alt="1" class="fa fa-fw fa-star {{ $customer_case->evaluation->ranking_id <= 3 ? "text-warning" : "text-muted" }}"></i>
                                <i data-alt="2" class="fa fa-fw fa-star {{ $customer_case->evaluation->ranking_id <= 3 ? "text-warning" : "text-muted" }}"></i>
                                <i data-alt="3" class="fa fa-fw fa-star {{ $customer_case->evaluation->ranking_id <= 2 ? "text-warning" : "text-muted" }}"></i>
                                <i data-alt="4" class="fa fa-fw fa-star {{ $customer_case->evaluation->ranking_id <= 1 ? "text-warning" : "text-muted" }}"></i>
                                <i data-alt="5" class="fa fa-fw fa-star {{ $customer_case->evaluation->ranking_id <= 1 ? "text-warning" : "text-muted" }}"></i>
                            </div>
                        @endif
                    </dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <!-- Evaluation Form -->
        <form action="{{ route('case-update-score', $customer_case->evaluation) }}" method="POST" id="user-evaluation-form">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="ranking">Ranking</label>
                {{Form::select('ranking', \App\Models\Ranking::pluck('ranking_label', 'id'), old('ranking', $customer_case->evaluation->ranking_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('ranking') ? ' is-invalid' : (isset($errors) && $errors->has('ranking') ? 'true' : 'false')), 'aria-describedby' => 'ranking-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner ranking...'])}}
                <div id="ranking-error" class="invalid-feedback"></div>
            </div>
            <div class="form-group">
                <label for="score">Score</label>
                <input type="number" class="form-control" id="score" name="score" value="{{ old('score', $customer_case->evaluation->score) }}" placeholder="Score.." aria-describedby="score-error" aria-invalid="false">
                <div id="score-error" class="invalid-feedback"></div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-outline-danger" id="evaluation-form-update"><i class="fa fa-fw fa-save mr-1"></i>Mettre à jour</button>
            </div>
        </form>
        <!-- END Evaluation Form -->
    </div>
</div>
