<div class="document-wrapper">
    @if(!isCustomer())
        <a class="btn btn-sm btn-light" href="{{ $docURL }}" target="_blank">
            <i class="fa fa-fw fa-link text-info"></i>
        </a>
        @if(count($documentLink))
            <a data-formId="form-{{ $formId . "5" }}" class="btn btn-sm btn-light toggle-link-visibility" href="javascript:void(0)">
                <i class="fa fa-fw fa-eye text-success"></i>
            </a>
        @else
            <a data-formId = "form-{{ $formId . "5" }}" class="btn btn-sm btn-light toggle-link-visibility" href="javascript:void(0)">
                <i class="fa fa-fw fa-eye-slash text-danger"></i>
            </a>
        @endif
    @else
        @if(count($documentLink))
            <a class="btn btn-sm btn-light" href="{{ $docURL }}" target="_blank">
                <i class="fa fa-fw fa-link text-info"></i>
            </a>
        @endif
    @endif
        <form id="form-{{ $formId . "5" }}" action="{{ route('toggle-link-visibility',['customer' => $customer_case->customer, 'document_type' => $doc, 'program_step' => $step]) }}" method="POST" style="display: none;">
            @csrf
        </form>
</div>
