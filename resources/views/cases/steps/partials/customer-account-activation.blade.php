<div class="row items-push">
    <div class="col-md-12">
        <div class="block block-rounded block-themed">
            <div>
                <div class="block block-rounded">
                    <div class="block-content">
                        @if(empty($customer_case->customer->email) || empty($customer_case->customer->password))
                            <form action="{{ route('customer-account-activation', $customer_case->customer->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="row push">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <span class="text-danger">*</span>
                                            <input type="email" class="form-control form-control-alt{{ isset($errors) && $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email', $customer_case->customer->email) }}" placeholder="Email.." aria-describedby="email-error" aria-invalid="{{ isset($errors) && $errors->has('email') ? 'true' : 'false' }}">
                                            <div id="email-error" class="invalid-feedback">{{ isset($errors) && $errors->has('email') ? $errors->first('email') : '' }}</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Mot de passe</label>
                                            <span class="text-danger">*</span>
                                            <input type="text" class="form-control form-control-alt{{ isset($errors) && $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password" value="{{ old('password') }}" placeholder="Mot de passe.." aria-describedby="password-error" aria-invalid="{{ isset($errors) && $errors->has('password') ? 'true' : 'false' }}">
                                            <div id="password-error" class="invalid-feedback">{{ isset($errors) && $errors->has('password') ? $errors->first('password') : '' }}</div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-alt-primary">
                                                Mettre à jour
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @else
                            <p>Le compte client est <span class="text-success">activé!</span></p>
                            <form id="deactivation-form" action="{{ route('customer-account-deactivation', $customer_case->customer->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="row push">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-alt-primary">
                                                Désactiver
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

