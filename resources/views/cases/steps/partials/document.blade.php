@if (empty($media))
    @include('cases.steps.partials.upload-file', [
        'document' => $document,
        'collectionName' => $collectionName,
        'formId' => Str::random(10),
        'requestedDocument' => $customer_case->customer->requestedDocuments()->wherePivot('program_step_id', '=', $step->id)->wherePivot('document_type_id', '=', $doc->id)->wherePivot('link', '=', false)->get(),
    ])
@else
    @livewire('customer.partials.show', ['customer_case' => $customer_case, 'collectionName' => $collectionName, 'media' => $media, 'customer_case' => $customer_case ?? null, 'media_name' => $media_name ?? null])
@endif
