@php
    $groupedResultsArray = [];
@endphp
@foreach($form->fields()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get() as $field)
    @php
        $groupedResultsArray[$field->customer_field_group_id][] = $field;
    @endphp
@endforeach
<form action="{{ route('customer-update-clear3', $customer_case->customer_id) }}" method="POST" id="customer-form" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <?php
    $sheet_id = null;
    if ($form instanceof App\Models\InformationSheet) {
        $sheet_id = $form->id;
        echo '<input type="hidden" name="sheet" value="'.$sheet_id.'" />';
    }
    // $lead = \App\Models\Lead::where('id', $customer_case->customer->lead_id)->first();
    $customer_ = \App\Models\CustomerClear3Fields::where("customer_id", $customer_case->customer->id)->first();
    ?>
    @foreach($groupedResultsArray as $key => $groupedFieldArr)
        @foreach($groupedFieldArr as $field)
            @if($loop->first)
                <span class="badge badge-danger">{{ $field->group->name_fr }}</span>
                <div class="row">
                    @endif
                    <div class="col-md-6">
                        @php
                            $field_name = $field->field_name;
                        @endphp
                        @if($field->template == "text")
                            <div class="form-group">
                                <label for="{{ $field->field_name }}">{{ __('lang.' . $field->field_name)}}</label>
                                @if($field->pivot->required)
                                    <span class="text-danger">*</span>
                                @endif
                                <input type="text" class="form-control form-control-alt{{ isset($errors) && $errors->has($field->field_name) ? ' is-invalid' : '' }}" id="{{ $field->field_name }}" name="{{$field->field_name}}" value="@if($customer_) {{ old($field->field_name, $customer_->$field_name) }} @endif" placeholder="{{ __('lang.' . $field->field_name)}}.." aria-describedby="{{ $field->field_name }}-error" aria-invalid="{{ isset($errors) && $errors->has($field->field_name) ? 'true' : 'false' }}">
                                <div id="{{ $field->field_name }}-error" class="invalid-feedback">{{ isset($errors) && $errors->has($field->field_name) ? $errors->first($field->field_name) : '' }}</div>
                            </div>
                        @else
                            @include('form.elements.' . $field->template, ['customer_field' => $field, 'field_name' => $field_name])
                        @endif
                    </div>
                    @if($loop->last)
                </div>
            @endif
        @endforeach
    @endforeach
    <div class="form-group col-md-12" style="padding-top: 20px;">
        <button type="submit" class="btn btn-lg btn-alt-primary btn-block"><i class="fa fa-fw fa-check mr-1"></i>Enregistrer</button>
    </div>

</form>
