<div class="row items-push">
    <div class="col-md-12">
        <div class="block block-rounded block-themed">
            <div class="block-content">
                <div class="row items-push">
                    <div class="col-md-6">
                        <table class="table table-striped table-borderless">
                            <thead>
                            <tr>
                                <th colspan="2"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="width: 40%;">
                                    <i class="fa fa-fw fa-user text-muted mr-1"></i> Nom
                                </td>
                                <td>{{ $customer_case->customer->firstname }} {{ $customer_case->customer->lastname }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-phone text-muted mr-1"></i> Téléphone
                                </td>
                                <td>{{ $customer_case->customer->phone }} </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-envelope text-muted mr-1"></i> Email
                                </td>
                                <td>
                                    {{ $customer_case->customer->customer_email }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-graduation-cap text-muted mr-1"></i> Niveau scolaire
                                </td>
                                <td>
                                    Baccalauréat
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-striped table-borderless">
                            <thead>
                            <tr>
                                <th colspan="2"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="width: 60%;">
                                    <i class="fa fa-fw fa-wallet text-muted mr-1"></i> Budget
                                </td>
                                <td>7000 $</td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-graduation-cap text-muted mr-1"></i> Type de diplôme projeté
                                </td>
                                <td>MBS </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-box text-muted mr-1"></i> Niveau anglais
                                </td>
                                <td>
                                    Moyen
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

                <table class="table table-striped table-borderless">
                    <thead>
                    <tr>
                        <th colspan="2">Fichier</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="width: 20%;"><i class="fa fa-fw fa-file-pdf text-muted mr-1"></i> CV Client</td>
                        <td>
                            @include('cases.steps.partials.document', ['document' => 'CV Client', 'collectionName' => 'CVs', 'model' => $customer_case->customer])
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
