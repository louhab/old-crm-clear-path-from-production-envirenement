@php
    $groupedResultsArray = [];
    if ($customer_case->program_id == 5) {
        $pid = 1;
    } else {
        $pid = $customer_case->program_id;
    }
@endphp
@foreach ($form->fields()->wherePivot('program_id', '=', $pid)->withPivot(['required', 'order'])->orderBy('order')->get() as $field)
    @php
        $groupedResultsArray[$field->customer_field_group_id][] = $field;
    @endphp
@endforeach
{{-- <div> {{ (isCustomer() ? 'customer-' : '') . 'customer-update' }}</div> --}}
<form action="{{ route((isCustomer() ? 'customer-' : '') . 'customer-update', $customer_case->customer_id) }}"
    method="POST" id="customer-form" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <?php
    $sheet_id = null;
    if ($form instanceof App\Models\InformationSheet) {
        $sheet_id = $form->id;
        echo '<input type="hidden" name="sheet" value="' . $sheet_id . '" />';
    }
    $lead = \App\Models\Lead::where('id', $customer_case->customer->lead_id)->first();
    $lead_lang = $lead->lang ?: 'fr';
    ?>
    @foreach ($groupedResultsArray as $key => $groupedFieldArr)
        @foreach ($groupedFieldArr as $field)
            @if ($field->customer_field_group_id == 1)
                @if ($loop->first)
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="d-block">Rencontre2 :</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="example-radios-inline1"
                                        name="radios-office" value="office"
                                        @if ($customer_case->customer->lead->to_office == 'office') checked @endif>
                                    <label class="form-check-label" for="example-radios-inline1">Venir Au Bureau</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="example-radios-inline2"
                                        name="radios-office" value="conference"
                                        @if ($customer_case->customer->lead->to_office == 'conference') checked @endif>
                                    <label class="form-check-label" for="example-radios-inline2">Vidéo
                                        conférence</label>
                                </div>
                                <div id="radios-office-error" class="invalid-feedback">
                                    {{ isset($errors) && $errors->has('radios-office') ? $errors->first('radios-office') : '' }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="program_id">Programme</label>
                                <span class="text-danger">*</span>
                                {{ Form::select('program_id', \App\Models\Program::orderBy('name')->pluck('name', 'id'), old('program_id', $lead->program_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('program_id') ? ' is-invalid' : (isset($errors) && $errors->has('program_id') ? 'true' : 'false')), 'aria-describedby' => 'program_id-error', 'aria-invalid' => '']) }}
                                <div id="program_id-error" class="invalid-feedback">
                                    {{ isset($errors) && $errors->has('program_id') ? $errors->first('program_id') : '' }}
                                </div>
                            </div>
                        </div>
                        @support
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="conseiller">Conseiller</label>
                                    <span class="text-danger">*</span>
                                    <?php
                                    $list_roles = [3];
                                    if (
                                        auth()
                                            ->user()
                                            ->isAdmin() ||
                                        auth()
                                            ->user()
                                            ->isManager() ||
                                        auth()
                                            ->user()
                                            ->isVerificateur()
                                    ) {
                                        array_push($list_roles, 2);
                                    }
                                    $queryCS = \App\Models\User::where('status', 1)->whereIn('role_id', $list_roles);

                                    ?>
                                    @php $user_id = Illuminate\Support\Facades\Auth::id(); @endphp
                                    {{ Form::select('conseiller', $queryCS->orderBy('name')->pluck('name', 'id'), old('conseiller', $lead->conseiller_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('conseiller') ? ' is-invalid' : (isset($errors) && $errors->has('conseiller') ? 'true' : 'false')), 'aria-describedby' => 'conseiller-error', 'aria-invalid' => '']) }}
                                    <div id="conseiller-error" class="invalid-feedback">
                                        {{ isset($errors) && $errors->has('conseiller') ? $errors->first('conseiller') : '' }}
                                    </div>
                                </div>
                            </div>
                        @endsupport
                        @admin
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="support">Support</label>
                                    <span class="text-danger">*</span>
                                    {{ Form::select('support',\App\Models\User::where('status', 1)->whereIn('role_id', [7])->orderBy('name')->pluck('name', 'id'),old('support', $lead->support_id),['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('support') ? ' is-invalid' : (isset($errors) && $errors->has('support') ? 'true' : 'false')), 'aria-describedby' => 'support-error', 'placeholder' => 'Support..', 'aria-invalid' => '']) }}
                                    <div id="support-error" class="invalid-feedback">
                                        {{ isset($errors) && $errors->has('support') ? $errors->first('support') : '' }}
                                    </div>
                                </div>
                            </div>
                        @endadmin
                    </div>
                    <div class="row">
                @endif

                @if (in_array(\Auth::user()->role_id, [
                        \App\Models\Role::Support,
                        \App\Models\Role::Agent,
                        \App\Models\Role::Conseiller,
                    ]) && $field->field_name == 'phone')
                @elseif (!in_array($field->id, range(188, 193)))
                    <div class="col-md-6" @if (
                        $field->field_name == 'lead_status' &&
                            !in_array(\Illuminate\Support\Facades\Auth::user()->role_id, [
                                \App\Models\Role::Admin,
                                \App\Models\Role::Manager,
                                \App\Models\Role::Verificateur,
                            ])) style="display: none;" @endif>
                        @include('form.elements.' . $field->template, [
                            'customer_field' => $field,
                            'field_name' => $field->field_name,
                        ])
                    </div>
                @endif

                @if ($field->field_name == 'campaign_id')
                    @php
                        $campaign = \App\Models\LeadFormMarketingCampaign::find($customer_case->customer->campaign_id);
                        $forms = \App\Models\LeadForm::where('lead_form_marketing_campaign_id', $customer_case->customer->campaign_id)
                            ->orderBy('title')
                            ->pluck('title', 'id');
                    @endphp
                    <div class="col-md-6" @if (count($forms) == 0) style="display: none" @endif>
                        <div class="form-group">
                            <label for="form">{{ $campaign->campaign_name }}</label>
                            <span class="text-danger">*</span>
                            {{ Form::select('form', $forms, old('form', $customer_case->customer->lead->influencer), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('form') ? ' is-invalid' : (isset($errors) && $errors->has('form') ? 'true' : 'false')), 'aria-describedby' => 'form-error', 'aria-invalid' => '', 'placeholder' => 'Référents']) }}
                            <div id="form-error" class="invalid-feedback">
                                {{ isset($errors) && $errors->has('form') ? $errors->first('form') : '' }}</div>
                        </div>
                    </div>
                    <div class="col-md-6" @if ($customer_case->customer->campaign_id != 23) style="display: none" @endif>
                        <div class="form-group">
                            <label for="reference">Reference</label>
                            <span class="text-danger">*</span>
                            <input type="text"
                                class="form-control form-control-alt{{ isset($errors) && $errors->has('reference') ? ' is-invalid' : '' }}"
                                id="reference" name="reference"
                                value="{{ old('reference', $customer_case->customer->lead->reference) }}"
                                placeholder="Reference.." aria-describedby="reference-error"
                                aria-invalid="{{ isset($errors) && $errors->has('reference') ? 'true' : 'false' }}">
                            <div id="reference-error" class="invalid-feedback">
                                {{ isset($errors) && $errors->has('reference') ? $errors->first('reference') : '' }}
                            </div>
                        </div>
                    </div>
                @endif

                @if ($loop->last)
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="country_id">Pays</label>
                            <span class="text-danger">*</span>
                            {{ Form::select('country_id', \App\Models\Country::orderBy('name_fr_fr')->pluck('name_fr_fr', 'id'), old('country_id', $customer_case->customer->country_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('country_id') ? ' is-invalid' : (isset($errors) && $errors->has('country_id') ? 'true' : 'false')), 'aria-describedby' => 'country_id-error', 'placeholder' => 'Pays..', 'aria-invalid' => '']) }}
                            <div id="country_id-error" class="invalid-feedback">
                                {{ isset($errors) && $errors->has('country_id') ? $errors->first('country_id') : '' }}
                            </div>
                        </div>
                    </div>
                    {{-- Ville Inputs --}}
                    <div class="col-md-6" id="city2"
                        style="
                        {{ $customer_case->customer->lead->country_id != 144 ? 'display: none' : '' }}">
                        <div class="form-group">
                            <label for="city2">Ville</label>
                            <span class="text-danger">*</span>
                            <div class="p-1">
                                @livewire('input.cities', ['value' => $customer_case->customer->lead->city2])
                            </div>
                            <div id="city2-error" class="invalid-feedback">
                                {{ isset($errors) && $errors->has('city2') ? $errors->first('city2') : '' }}</div>
                        </div>
                    </div>

                    <div class="col-md-6" id="city"
                        style="
                    {{ $customer_case->customer->lead->country_id == 144 ? 'display: none' : '' }}">
                        <div class="form-group">
                            <label for="city">Ville</label>
                            <span class="text-danger">*</span>
                            <input type="text"
                                class="form-control form-control-alt{{ isset($errors) && $errors->has('city') ? ' is-invalid' : '' }}"
                                id="city" name="city" value="{{ old('city', $customer_case->customer->city) }}"
                                placeholder="Ville.." aria-describedby="city-error"
                                aria-invalid="{{ isset($errors) && $errors->has('city') ? 'true' : 'false' }}">
                            <div id="city-error" class="invalid-feedback">
                                {{ isset($errors) && $errors->has('city') ? $errors->first('city') : '' }}</div>
                        </div>
                    </div>

                    @support_deny
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password">Mot de passe</label>
                                <div class="input-group">
                                    <input type="password" class="form-control form-control-alt" id="password"
                                        name="password" placeholder="Mot de passe">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-alt-primary" id="toggle-password">
                                            <i class="fa fa-eye" style="font-size:1em;"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endsupport_deny
                    <?php
                    // $program_field = \App\Models\ProgramField::find($customer_case->customer->lead->program_field);
                    if ($customer_case->program_id == 4) {
                        $label = "Domaines d'études";
                    } else {
                        $label = "Domaines d'emplois";
                    }
                    ?>

                    @php
                        $vals = \App\Models\ProgramField::where('visible', 1)
                            ->get()
                            ->groupBy('category');
                    @endphp
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="program_field">{{ $label }}</label>
                            <span class="text-danger">*</span>
                            <select
                                class="form-control form-control-alt{{ isset($errors) && $errors->has('program_field') ? ' is-invalid' : '' }}"
                                id="program_field" name="program_field" aria-describedby="program_field-error"
                                aria-invalid="{{ isset($errors) && $errors->has('program_field') ? 'true' : 'false' }}">
                                <option selected disabled value=>{{ $label }}</option>
                                @foreach ($vals as $key => $value)
                                    <optgroup label="{{ $key }}">
                                        @foreach ($value as $sub_key => $sub_value)
                                            <option
                                                {{ $lead->program_field && $sub_value->id == $lead->program_field ? 'selected' : '' }}
                                                {{ old('program_field') == $sub_value->id ? 'selected' : '' }}
                                                value={{ $sub_value->id }}>{{ $sub_value->label_fr }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                            <div id="program_field-error" class="invalid-feedback">
                                {{ isset($errors) && $errors->has('program_field') ? $errors->first('program_field') : '' }}
                            </div>
                        </div>
                    </div>

                    <!-- Devise field ?? -->
                    @support_deny
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="currency_id">Devise</label>
                                <span class="text-danger">*</span>
                                {{ Form::select(
                                    'currency_id',
                                    \App\Models\Currency::pluck('iso', 'id'),
                                    old('currency_id', $customer_case->customer->currency_id),
                                    [
                                        'class' =>
                                            'form-control form-control-alt' .
                                            (isset($errors) && $errors->has('currency_id')
                                                ? ' is-invalid'
                                                : (isset($errors) && $errors->has('currency_id')
                                                    ? 'true'
                                                    : 'false')),
                                        'placeholder' => 'Devise',
                                        'aria-describedby' => 'currency_id-error',
                                        'aria-invalid' => '',
                                    ],
                                ) }}
                                <div id="currency_id-error" class="invalid-feedback">
                                    {{ isset($errors) && $errors->has('currency_id') ? $errors->first('currency_id') : '' }}
                                </div>
                            </div>
                        </div>

                        <!-- Language field -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lang">Langue</label>
                                <span class="text-danger">*</span>
                                {{ Form::select('lang', ['en' => __('lang.english'), 'fr' => __('lang.french')], $lead_lang, ['class' => 'form-control form-control-alt', 'placeholder' => 'Language']) }}
                            </div>
                        </div>
                    @endsupport_deny
                    </div>
                @endif
            @endif
        @endforeach
    @endforeach
    <div class="form-group col-md-12" style="padding-top: 20px;">
        <button type="submit" class="btn btn-lg btn-alt-primary btn-block"><i
                class="fa fa-fw fa-check mr-1"></i>Enregistrer</button>
    </div>
</form>
