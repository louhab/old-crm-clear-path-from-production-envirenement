
<?php
    $docs = $form->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get();
    $sheet_id = null;
    if ($form instanceof App\Models\InformationSheet) {
        $sheet_id = $form->id;
        echo '<input type="hidden" name="sheet" value="'.$sheet_id.'" />';
    }
    $step = \App\Models\ProgramStep::where("id", 4)->first();
?>

<div class="block block-rounded">
    <div class="block-header">
        <h3 class="block-title">Table Sections</h3>
        <div class="block-options">
            <div class="block-options-item">
                <code>uploads</code>
            </div>
        </div>
    </div>
    <div class="block-content">
        <table class="js-table-sections table table-hover table-vcenter js-table-sections-enabled" id="table-medias">
            <thead>
            <tr>
                <th style="width: 30px;"></th>
                <th>Name</th>
                <th style="width: 15%;">Access</th>
                <th class="d-none d-sm-table-cell" style="width: 20%;">Date</th>
            </tr>
            </thead>
            @foreach($docs as $doc)
                <tbody class="js-table-sections-header @if($loop->first) show table-active @endif">
                <tr>
                    <td class="text-center">
                        <i class="fa fa-angle-right text-muted toggle-medias"></i>
                    </td>
                    <td class="font-w600 font-size-sm">
                        <div class="py-1">
                            <a href="javascript:void(0)" class="toggle-medias">{{ $doc->name_fr }}</a>
                        </div>
                    </td>
                    <td>
                        <span class="badge badge-success">Pending</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="font-size-sm text-muted">November 10, 2018 08:46</em>
                    </td>
                </tr>
                </tbody>
                <tbody class="font-size-sm">
                    @foreach ($customer_case->customer->getMedia($doc->collection_name) as $media)
                        @livewire('sheet.media', ['document' => $doc, 'media' => $media, 'index', $loop->index])
                    @endforeach
                </tbody>
            @endforeach
        </table>
    </div>
</div>
