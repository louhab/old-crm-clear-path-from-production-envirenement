@php
    $groupedResultsArray = [];
@endphp
@foreach($form->fields()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get() as $field)
    @php
        $groupedResultsArray[$field->customer_field_group_id][] = $field;
    @endphp
@endforeach
<form action="{{ route('customer-update-clear1004', $customer_case->customer_id) }}" method="POST" id="customer-form" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <?php
        $sheet_id = $form->id;
        $customer = \App\Models\Customer::where('id', $customer_case->customer_id)->first();
        $lead = \App\Models\Lead::where("id", $customer->lead_id)->first();
    ?>
    <input type="hidden" name="sheet" value="{{ $sheet_id }}">
    @foreach($groupedResultsArray as $key => $groupedFieldArr)
        @foreach($groupedFieldArr as $field)
            <div class="row" @if(!$loop->first) style="display: none" @endif>
                <div class="col-md-8">
                    @include('form.elements.' . $field->template, ['customer_field' => $field, 'field_name' => $field->field_name])
                    @if($field->field_name == 'lang_test_expire')
                        <div id="neg-results" style="display: none;"></div>
                    @endif
                </div>
                <div class="col-md-4">
                    <!-- <textarea name="remark-{{ $field->field_name }}">Remarques..</textarea> -->
                    <div class="form-group">
                        <label for="remark-{{ $field->field_name }}"></label>
                        @php $conseill = \App\Models\CustomerFieldRemark::where('customer_id', $customer_case->customer->id)->where('field_id', $field->id)->first(); @endphp
                        <textarea class="form-control form-control-alt" id="remark-{{ $field->field_name }}" name="remark-{{ $field->field_name }}" placeholder="{{ __('lang.' . $field->field_name)}}..">@if($conseill) {{ $conseill->value }} @endif</textarea>
                    </div>

                </div>
            </div>
        @endforeach
    @endforeach
    <a class="btn btn-sm btn-alt-success" id="calc-score">{{ __("lang.clear1004_calculate_your_results") }}</a>
    <div id="results"></div>
    <div class="form-group" style="padding-top: 20px;">
        <button type="submit" class="btn btn-lg btn-alt-primary btn-block" id="update-customer-btn"><i class="fa fa-fw fa-check mr-1"></i>{{ __("lang.clear1004_save_btn") }}</button>
    </div>
</form>
