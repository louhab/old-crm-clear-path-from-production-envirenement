<div class="document-in">
    <form id="add-media-cv-form" action="{{ route(isCustomer() ? 'customer-add-media' : 'add-media',['customer' => $customer_case->customer, 'collectionName' => $collectionName]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group"  style="margin-bottom: .5rem;">
            <div class="custom-file" lang="fr">
                <input type="file" class="custom-file-input js-custom-file-input-enabled{{ isset($errors) && $errors->has('file-input-media') ? ' is-invalid' : '' }}" data-toggle="file-input-media" id="file-input-media" name="file-input-media" aria-describedby="file-input-media-error" aria-invalid="{{ isset($errors) && $errors->has('file-input-media') ? 'true' : 'false' }}">
                <div id="file-input-media-error" class="invalid-feedback">{{ isset($errors) && $errors->has('file-input-media') ? $errors->first('file-input-media') : '' }}</div>
                <label class="custom-file-label" for="file-input-media">Sélectionnez {{ $document }}</label>

            </div>
        </div>
        <div class="form-group" style="margin-bottom: .5rem;">
            <button type="submit" style="min-width:100%" class="btn btn-alt-primary upload-document">
                Ajouter
            </button>
        </div>
    </form>
</div>
