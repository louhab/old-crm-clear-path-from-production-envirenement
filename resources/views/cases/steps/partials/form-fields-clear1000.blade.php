@php
    $lang = session()->get('locale') ?? 'fr';
    $clear_fields = \App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first();
    $groupedResultsArray = [];
@endphp
@foreach ($form->fields()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get() as $field)
    @php
        $groupedResultsArray[$field->customer_field_group_id][] = $field;
    @endphp
@endforeach
@php
    $groupsSorted = \App\Models\CustomerFieldGroup::orderBy('order')->pluck('id');
    // dd($clear_fields);
@endphp
<form action="{{ route('customer-update-clear1000', $customer_case->customer_id) }}" method="POST" id="customer-form"
    enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <?php
    $sheet_id = null;
    if ($form instanceof App\Models\InformationSheet) {
        $sheet_id = $form->id;
        echo '<input type="hidden" name="sheet" value="' . $sheet_id . '" />';
    }
    $groupedRemarksArray = [];
    foreach (App\Models\Remarks::whereNotNull('remark_group_id')->get() as $remark) {
        $groupedRemarksArray[$remark->remark_group_id][] = $remark;
    }

    $section_title_by_program = [11];

    ?>
    @foreach ($groupsSorted as $key)
        @if (array_key_exists($key, $groupedResultsArray))
            @if (array_key_exists($key, $groupedRemarksArray))
                @php $group_name =  $lang === "fr" ? $groupedResultsArray[$key][0]->group->name_fr : $groupedResultsArray[$key][0]->group->name_en ; @endphp
                <div class="block block-rounded">
                    <span class="block-header block-header-default">
                        {{-- change it to Other places to study ❌ str_replace (in test 🧪) --}}
                        <h3 class="block-title">
                            @if ($customer_case->program_id == 31 && in_array($groupedResultsArray[$key][0]->group->id, $section_title_by_program))
                                @if ($lang === 'fr')
                                    {{ str_replace('au Canada', 'au UK', $group_name) }}
                                @else
                                    {{ str_replace('in Canada', 'in UK', $group_name) }}
                                @endif
                            @elseif ($customer_case->program_id == 32 && in_array($groupedResultsArray[$key][0]->group->id, $section_title_by_program))
                                @if ($lang === 'fr')
                                    {{ str_replace('au Canada', 'en Espagne', $group_name) }}
                                @else
                                    {{ str_replace('in Canada', 'in Spain', $group_name) }}
                                @endif
                                {{-- ['name_fr' => 'Etude Envisagée au Canada', 'name_en' => 'Study Considered in Canada', 'order' => '6'] --}}
                                {{-- ['name_fr' => 'Informations sur les anciennes demandes des visas & permis d’études', 'name_en' => 'Informations sur les anciennes demandes des visas & permis d’études', 'order' => '13'] --}}
                                {{-- this one should be dynamic with the program_id --}}
                                {{-- {{ str_contains($group_name, 'au Canada') ? str_replace('au Canada', 'en ES', $group_name) : $group_name }} --}}
                                {{-- check also for the last one (visa et PE) --}}
                            @else
                                {{ str_contains($group_name, ' & permis d’études') ? str_replace(' & permis d’études', '', $group_name) : $group_name }}
                            @endif
                        </h3>
                    </span>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-6">
                                <ul style='list-style:none;padding:0;'>
                                    {{-- @php dd($groupedResultsArray); @endphp --}}
                                    @foreach ($groupedResultsArray[$key] as $field)
                                        <li>
                                            @if (in_array($field->field_name, [
                                                    'diploma_fr',
                                                    'diploma_en',
                                                    'language',
                                                    'training_type',
                                                    'diplomas',
                                                    'province',
                                                    'city',
                                                    'guarantor_income_type',
                                                    'language_tests',
                                                ]))
                                                {{-- should be fix 🚀 language_tests[✅], language[❌], province[⏳], city[✅], training_type[❌], diplomas[❌] --}}
                                                {{--
                                                    1. language_tests ✅ (we added program_ids migration into the table `language_tests`)
                                                    2. language => customer_radio_fields ❌ (⚙️ should updated manualy after running the migration)
                                                    3. province => needs UK and ES province (after that add migration for porgram_ids) ⏳
                                                    4. city => 🧪 we added program_ids migration into the table `canada_cites`)
                                                    5. training_type => customer_radio_fields ⏳ (noting to change till now)
                                                    6. diplomas => customer_radio_fields ❌ (⚙️ should updated manualy after running the migration)
                                                    steps we added the migration and we have to update data into tables to fix the issue
                                                    --}}
                                                @livewire('input.form.select2', ['customer_case' => $customer_case, 'customer_field' => $field])
                                            @elseif($field->template === 'date')
                                                @include('form.elements.date-text', [
                                                    'customer_field' => $field,
                                                    'field_name' => $field->field_name,
                                                    'customer_garant' => $garant,
                                                ])
                                            @else
                                                @include('form.elements.' . $field->template, [
                                                    'customer_field' => $field,
                                                    'field_name' => $field->field_name,
                                                ])
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-6">

                                <div class="alert alert-info d-flex align-items-center" role="alert">
                                    <div class="flex-shrink-0" style="padding-right: 10px;">
                                        <i class="fa fa-fw fa-info-circle"></i>
                                    </div>
                                    <div class="flex-grow-1 ms-3">
                                        <p class="mb-0">
                                            @foreach ($groupedRemarksArray[$key] as $remark)
                                                @if (isset($remark->code) && !is_null($remark->code))
                                                    @php $tt = $clear_fields->training_type; @endphp
                                                    @if ($remark->code == $tt)
                                                        {{ $lang === 'fr' ? $remark->conseill : $remark->labelEng }}
                                                        <br />
                                                    @endif
                                                @else
                                                    {{ $lang === 'fr' ? $remark->conseill : $remark->labelEng }} <br />
                                                @endif
                                            @endforeach
                                        </p>
                                    </div>
                                </div>

                            </div>
                            @if ($key == 8)
                                @livewire('input.garant-container', ['customer_case' => $customer_case, 'fields' => $groupedResultsArray[8]])
                            @endif
                        </div>
                    </div>
                </div>
            @else
                {{-- @php
                    // visa_request
                    $result_fields = ["visa_request_date", "visa_request_type", "visa_request_result"];
                    $show_result = true;
                @endphp --}}
                @foreach ($groupedResultsArray[$key] as $field)
                    @if ($loop->first)
                        <div class="block block-rounded">
                            <span class="block-header block-header-default">
                                <h3 class="block-title">
                                    {{ $lang === 'fr' ? $field->group->name_fr : $field->group->name_en }}</h3>
                            </span>
                            <div class="block-content">
                                <div class="row">
                    @endif
                    @if ($key != 13)
                        <div class="col-md-6">
                            @if (in_array($field->field_name, [
                                    'diploma_fr',
                                    'diploma_en',
                                    'language',
                                    'training_type',
                                    'diplomas',
                                    'province',
                                    'city',
                                    'guarantor_income_type',
                                    'language_tests',
                                ]))
                                @livewire('input.form.select2', ['customer_case' => $customer_case, 'customer_field' => $field])
                            @elseif ($field->template === 'date')
                                @include('form.elements.date-text', [
                                    'customer_field' => $field,
                                    'field_name' => $field->field_name,
                                ])
                            @else
                                @include('form.elements.' . $field->template, [
                                    'customer_field' => $field,
                                    'field_name' => $field->field_name,
                                ])
                            @endif
                        </div>
                    @endif
                    @if ($field->field_name == 'branch')
                        @livewire('input.branch', ['customer_case' => $customer_case, 'field' => $field])
                    @endif
                    @if ($loop->last)
                        @if ($key == 8)
                            @livewire('input.garant-container', ['customer_case' => $customer_case, 'fields' => $groupedResultsArray[8]])
                        @endif
                        @if ($key == 13)
                            <div class="col-12">
                                @livewire('input.visa-request-container', ['customer_case' => $customer_case, 'fields' => $groupedResultsArray[13]])
                            </div>
                        @endif
                        </div>
                        </div>
                        </div>
                    @endif
                @endforeach
            @endif
        @endif
    @endforeach
    <div class="form-group" style="padding-top: 20px;">
        <button type="submit" class="btn btn-lg btn-alt-primary"><i class="fa fa-fw fa-check mr-1"></i>
            {{ __('lang.portal_about_the_lead_save_button') }}
        </button>
    </div>

</form>
<!-- do not delete this element, it is responsible for adding new garant -->
<!--<template id="garant-template">
</template>-->
@push('custom_scripts')
    <script>
        $('select[name="branch"]').change(function() {
            window.livewire.emit('branchSelected', $(this).val());
        });

        // hidden inputs
        $('select[name="health"]').change(function() {
            if ($(this).val() == 4) {
                $('input[name="health_rep"]').parent().show();
            } else {
                $('input[name="health_rep"]').parent().hide();
                $('input[name="health_rep"]').val('');
            }
        })
        $('select[name="criminal_record"]').change(function() {
            if ($(this).val() == 6) {
                $('input[name="criminal_record_rep"]').parent().show();
            } else {
                $('input[name="criminal_record_rep"]').parent().hide();
                $('input[name="criminal_record_rep"]').val('');
            }
        })
        $('select[name="goods"]').change(function() {
            if ($(this).val() == 9) {
                $('select[name="goods_res"]').parent().show();
                $('input[name="rental_income"]').parent().show();
            } else {
                $('select[name="goods_res"]').parent().hide();
                $('select[name="goods_res"]').prop('selectedIndex', 0);
                $('input[name="rental_income"]').parent().hide();
                $('input[name="rental_income"]').val('');
            }
        })

        $('select[name="tef_test"]').change(function() {
            if ($(this).val() == 3) {
                $('input[name="tef_test_res"]').parent().show();
            } else {
                $('input[name="tef_test_res"]').parent().hide();
                $('input[name="tef_test_res"]').val('');
            }
        })

        const guarantor_res = {
            1: "guarantor_work_income",
            2: "guarantor_retirement_income",
            3: "guarantor_business_income",
            4: "guarantor_professional_income",
            5: "guarantor_self_employed_income",
            6: "guarantor_rental_income",
            7: "guarantor_investment_income",

        }

        $('select[name="guarantor_income_type[]"]').change(function() {
            let selected = []
            $(this).select2('data')?.forEach(element => {
                selected.push(element.id)
            });

            for (const key in guarantor_res) {
                if (selected.includes(key)) {
                    $(`select[name="${guarantor_res[key]}"]`).parent().show();
                } else {
                    $(`select[name="${guarantor_res[key]}"]`).parent().hide();
                    $(`select[name="${guarantor_res[key]}"]`).prop('selectedIndex', 0);
                }
            }
        })

        $('select[name="is_language_taken"]').change(function() {
            if ($(this).val() == 1 || $(this).val() == 3) {
                $('#language_tests').css('visibility', 'visible')
            } else {
                $('#language_tests').css('visibility', 'hidden')
                $('select[name="language_tests[]"]').val([]).change();
            }
        })

        // $('select[name="visa_request"]').change(function () {
        //         if ($(this).val() == 82) {
        //             $(".visa-result-field").each(function () {
        //                 $(this).show();
        //             });
        //         } else {
        //             $(".visa-result-field").each(function () {
        //                 $(this).hide();
        //             });
        //         }
        //     });
    </script>
@endpush
