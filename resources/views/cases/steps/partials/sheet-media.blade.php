<div>
    @if(!isCustomer())
        <a class="btn btn-sm btn-alt-primary" href="{{ route('edit-media-sheet', ['customer_case'=> $customer_case, 'media_sheet' => $mediaSheet]) }}" target="_blank">
            {{  __("lang.portal_view_documents") }}
        </a>
        {{--@if(count($mediaSheet->customers()->wherePivot('customer_id', '=', $customer_case->customer_id)->wherePivot('can_edit', '=', true)->get()))
             <a data-formId="form-{{ $formId . "7" }}" class="btn btn-sm btn-light toggle-media-sheet-visibility" href="javascript:void(0)">
                 <i class="fa fa-fw fa-eye text-success"></i>
             </a>
         @else
            <a data-formId = "form-{{ $formId . "7" }}" class="btn btn-sm btn-light toggle-media-sheet-visibility" href="javascript:void(0)">
                 <i class="fa fa-fw fa-eye-slash text-danger"></i>
             </a>
         @endif--}}
    @endif
</div>

{{--<form id="form-{{ $formId . "7" }}" action="{{ route('toggle-media-sheet-visibility',['customer' => $customer_case->customer, 'media_sheet' => $mediaSheet]) }}" method="POST" style="display: none;">
    @csrf
</form>--}}
