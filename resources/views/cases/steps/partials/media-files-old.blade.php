@php
    $docs = $form->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get();
@endphp
    <?php
        $sheet_id = null;
        if ($form instanceof App\Models\InformationSheet) {
            $sheet_id = $form->id;
            echo '<input type="hidden" name="sheet" value="'.$sheet_id.'" />';
        }
        $step = \App\Models\ProgramStep::where("id", 4)->first();
        // dd($docs);
    ?>

<div class="row justify-content-center">
    @method('PUT')
    @csrf
    @foreach($docs as $doc)
    <div class="col-md-12">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">{{ $doc->name_fr }}</h3>
                <div class="block-options">
{{--                    @php $media = $customer_case->customer->getFirstMedia($doc->collection_name); @endphp--}}
{{--                    @if(!empty($media))--}}
{{--                        @include('cases.steps.partials.show-file', ['document' => $doc->collection_name_fr, 'model' => $customer_case->customer, 'collectionName' => $doc->collection_name, 'formId' => Str::random(10), 'signable' => $media->getCustomProperty('signable')])--}}
{{--                    @endif--}}
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
