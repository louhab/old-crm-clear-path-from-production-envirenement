<div class="document-wrapper">
    @if(!isCustomer())
        @if($doc->customer_owner)
            @if($requestedDocument->count())
                <a data-formId="form-{{ $formId . "3" }}" class="btn btn-sm btn-light document-in requested-doc" href="javascript:void(0)">
                    <i class="fa fa-fw fa-envelope text-danger"></i>
                    Annuler la demande de ce documment
                </a>
            @else
                <a data-formId="form-{{ $formId . "4" }}" class="btn btn-sm btn-light document-in requested-doc" href="javascript:void(0)">
                    <i class="fa fa-fw fa-envelope-open text-success"></i>
                    Demander le document
                </a>
            @endif
        @else
            @livewire('customer.partials.upload', ['customer_case' => $customer_case, 'collectionName' => $collectionName])
        @endif
    @else
        @livewire('customer.partials.upload', ['customer_case' => $customer_case, 'collectionName' => $collectionName])
        {{--@if($requestedDocument->count())
        @endif--}}
    @endif
        <form id="form-{{ $formId . "3" }}" action="{{ route('remove-requested-document',['customer' => $customer_case->customer, 'document_type' => $doc, 'program_step' => $step]) }}" method="POST" style="display: none;">
            @csrf
        </form>
        <form id="form-{{ $formId . "4" }}" action="{{ route('add-requested-document',['customer' => $customer_case->customer, 'document_type' => $doc, 'program_step' => $step]) }}" method="POST" style="display: none;">
            @csrf
        </form>
</div>
