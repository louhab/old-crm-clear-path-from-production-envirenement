<div>
    <div class="custom-control custom-switch custom-control-success mb-1">
        <input type="checkbox" class="custom-control-input" id="example-sw-custom-success2" name="example-sw-success2" {{ $doc->visible_for_customer ? '  checked=""' : '' }}>
        <label class="custom-control-label" for="example-sw-custom-success2">Visible pour le client</label>
    </div>
    <a class="btn btn-sm btn-light" href="{{ route('get-media', $model->getFirstMedia($collectionName)) }}">
        <i class="fa fa-fw fa-download text-success"></i>
    </a>
    <a data-formId = "form-{{ $formId }}" class="btn btn-sm btn-light delete-document" href="javascript:void(0)">
        <i class="fa fa-fw fa-times text-danger"></i>
    </a>
    @if(isMediaSignable($collectionName))
        <a data-formId = "form-{{ $formId . "1" }}" class="btn btn-light sign-document" href="javascript:void(0)">
            <i class="fa fa-fw fa-pencil-alt text-primary"></i>
            {{ getSigningLabel($model, $collectionName) }}
        </a>
    @endif
</div>
<form id="form-{{ $formId }}" action="{{ route('customer-delete-media',['customer' => $model, 'collectionName' => $collectionName]) }}" method="POST" style="display: none;">
    @csrf
    @method('DELETE')
</form>
@if(isMediaSignable($collectionName))
    <form id="form-{{ $formId . "1" }}" action="{{ route(getSigningRouteName($model, $collectionName),['customer' => $model, 'collectionName' => $collectionName]) }}" method="POST" style="display: none;">
        @csrf
    </form>
@endif

@include('cases.steps.partials.document', ['document' => $doc->collection_name_fr, 'collectionName' => $doc->collection_name, 'model' => $customer_case->customer])
