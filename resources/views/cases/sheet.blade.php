<?php
switch ($customerCase->program_id) {
    case 31:
        $prefix = 'UK-';
        break;

    case 32:
        $prefix = 'ES-';
        break;

    default:
        $prefix = 'CA-';
        break;
}
?>

@extends(
    auth()->guard('customer')->user()
        ? 'layouts.customer'
        : 'layouts.backend'
)
@section('css_after')
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    @livewireStyles
    <meta http-equiv="Expires" content="Mon, 26 Jul 1997 05:00:00 GMT">
    <meta http-equiv="Pragma" content="no-cache">
    <style>
        .center {
            margin: auto;
            width: 50%;
            /* border: 3px solid green; */
            padding: 10px;
        }

        hr {
            display: block;
            height: 1px;
            border: 0;
            border-top: 1px solid red;
            margin: 1em 0;
            padding: 0;
        }

        .select2-selection__choice__remove {
            color: #fff !important;
        }
    </style>
    {{-- @ahaloua --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
    <script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pace-js@latest/pace-theme-default.min.css">
    <style>
        .pace {
            -webkit-pointer-events: none;
            pointer-events: none;

            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;

            -webkit-perspective: 12rem;
            -moz-perspective: 12rem;
            -ms-perspective: 12rem;
            -o-perspective: 12rem;
            perspective: 12rem;

            z-index: 2000;
            position: fixed;
            height: 6rem;
            width: 6rem;
            margin: auto;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }

        .pace.pace-inactive .pace-progress {
            display: none;
        }

        .pace .pace-progress {
            position: fixed;
            z-index: 2000;
            display: block;
            position: absolute;
            left: 0;
            top: 0;
            height: 6rem;
            width: 6rem !important;
            line-height: 6rem;
            font-size: 2rem;
            border-radius: 50%;
            background: rgba(213, 43, 38, 0.8);
            color: #fff;
            font-family: "Helvetica Neue", sans-serif;
            font-weight: 100;
            text-align: center;

            -webkit-animation: pace-theme-center-circle-spin linear infinite 2s;
            -moz-animation: pace-theme-center-circle-spin linear infinite 2s;
            -ms-animation: pace-theme-center-circle-spin linear infinite 2s;
            -o-animation: pace-theme-center-circle-spin linear infinite 2s;
            animation: pace-theme-center-circle-spin linear infinite 2s;

            -webkit-transform-style: preserve-3d;
            -moz-transform-style: preserve-3d;
            -ms-transform-style: preserve-3d;
            -o-transform-style: preserve-3d;
            transform-style: preserve-3d;
        }

        .pace .pace-progress:after {
            content: attr(data-progress-text);
            display: block;
        }

        @-webkit-keyframes pace-theme-center-circle-spin {
            from {
                -webkit-transform: rotateY(0deg)
            }

            to {
                -webkit-transform: rotateY(360deg)
            }
        }

        @-moz-keyframes pace-theme-center-circle-spin {
            from {
                -moz-transform: rotateY(0deg)
            }

            to {
                -moz-transform: rotateY(360deg)
            }
        }

        @-ms-keyframes pace-theme-center-circle-spin {
            from {
                -ms-transform: rotateY(0deg)
            }

            to {
                -ms-transform: rotateY(360deg)
            }
        }

        @-o-keyframes pace-theme-center-circle-spin {
            from {
                -o-transform: rotateY(0deg)
            }

            to {
                -o-transform: rotateY(360deg)
            }
        }

        @keyframes pace-theme-center-circle-spin {
            from {
                transform: rotateY(0deg)
            }

            to {
                transform: rotateY(360deg)
            }
        }
    </style>
    <style>
        li.select2-selection__choice {
            max-width: 40%;
            overflow: hidden;
            white-space: normal;
            word-wrap: break-word;
            height: auto !important;
        }

        ul.select2-selection__rendered {
            padding-right: 12px !important;
        }

        /*li.select2-selection__choice {
                                                                                                                                        max-width: 50%;
                                                                                                                                        overflow: hidden;
                                                                                                                                        !*Altered two below to make word wrap work *!
                                                                                                                                        word-wrap: normal !important;
                                                                                                                                        white-space: normal;
                                                                                                                                    }
                                                                                                                                    ul.select2-selection__rendered {
                                                                                                                                        padding-right: 12px !important;
                                                                                                                                    }*/
        .media-body>div {
            max-width: 60%;
            overflow: hidden;
            white-space: normal;
            word-wrap: break-word;
        }
    </style>
@endsection
@section('js_after')
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/additional-methods.js') }}"></script>

    {{-- @ahaloua --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    {{-- <link href="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/css/tempus-dominus.css" rel="stylesheet" crossorigin="anonymous"> --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.2/dist/umd/popper.min.js" crossorigin="anonymous"></script> --}}
    {{-- <script src="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/js/tempus-dominus.js" crossorigin="anonymous"></script> --}}

    <!-- Page JS Code -->
    @livewireScripts
    <script src="{{ asset('js/pages/wizard.js') }}"></script>
    <script src="{{ asset('js/pages/cases/sheet.js') }}"></script>
    <script>
        $('body').on('click', '.remark-toggle', function() {
            let cc = $(this).closest('.row').children().last();
            if (cc.find('textarea[name^="remark-"]').length != 0) {
                cc.toggle();
            }
        });
    </script>
    <script>
        // window.setIntlTel("phone");
        if ($('#phone').length) {
            window.setIntlTel("phone");
        }
    </script>
@endsection
@section('content')
    @if (str_starts_with($informationSheet->name_fr, 'Clear1000'))
        @include('layouts.partials.hero', [
            'title' => $prefix . 'Clear 1001',
            'subTitle' => 'Questionnaire',
            'navItems' => ['Fiches de renseignements', 'questionnaire'],
        ])
    @else
        @if ($informationSheet->id == 3)
            @include('layouts.partials.hero', [
                'title' => 'Clear 3',
                'subTitle' => isset($join) ? $join : 'informations',
                'navItems' => [__('lang.customer_sheet_title'), isset($join) ? $join : 'informations'],
            ])
        @else
            @include('layouts.partials.hero', [
                'title' =>
                    session()->get('locale') ?? 'fr' === 'fr'
                        ? $informationSheet->name_fr
                        : $informationSheet->name_en,
                'subTitle' => isset($join) ? $join : 'informations',
                'navItems' => [__('lang.customer_sheet_title'), isset($join) ? $join : 'informations'],
            ])
        @endif
    @endif
    <!-- Sheet Content -->
    <input type="hidden" name="lang" value="{{ session()->get('locale') ?? 'fr' }}">
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if ($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les
                                informations !</a>
                            {{-- @php dd($errors->all()); @endphp --}}
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        <div>
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            @if (in_array($informationSheet->id, [3, 9]))
                @livewire('input.group.partials.flash')
            @endif
        </div>
        <div class="block block-rounded">
            <div class="block-content">
                <div class="row">
                    <div id="sheet-form" class="col-md-12 pb-3">
                        @switch($informationSheet->id)
                            @case(1)
                                @include('cases.steps.partials.form-fields-clear1000', [
                                    'form' => $informationSheet,
                                    'customer_case' => $customerCase,
                                ])
                            @break

                            {{-- @case(10)
                            @include('cases.steps.partials.form-fields-clear1010', ['form' => $informationSheet, 'customer_case' => $customerCase])
                            @break --}}
                            @case(3)
                                @livewire('sheet.wizard', ['form' => $informationSheet, 'customer_case' => $customerCase, 'join' => isset($join) ? $join : 'Single'])
                            @break

                            @case(7)
                                @include('cases.steps.partials.form-fields-clear1004', [
                                    'form' => $informationSheet,
                                    'customer_case' => $customerCase,
                                ])
                            @break

                            @case(4)
                                @livewire('sheet.orientation', ['form' => $informationSheet, 'customer_case' => $customerCase])
                            @break

                            @case(8)
                                @include('cases.steps.partials.form-fields-clear11', [
                                    'form' => $informationSheet,
                                    'customer_case' => $customerCase,
                                ])
                            @break

                            @case(9)
                                {{-- @include('cases.steps.partials.form-fields-clear11', ['form' => $informationSheet, 'customer_case' => $customerCase]) --}}
                                @livewire('sheet.wizard', ['form' => $informationSheet, 'customer_case' => $customerCase, 'join' => isset($join) ? $join : 'Single'])
                            @break

                            @default
                                @include('cases.steps.partials.form-fields', [
                                    'form' => $informationSheet,
                                    'customer_case' => $customerCase,
                                ])
                        @endswitch
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END Page Content -->
@endsection
