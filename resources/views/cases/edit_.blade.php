<?php
    $conseiller = \App\Models\User::where('id', $customer_case->customer->lead->conseiller_id)->first();
    $support = \App\Models\User::where('id', $customer_case->customer->lead->support_id)->first();
    $lang = session()->get('locale');
    $currency = $customer_case->customer->currency_id ? \App\Models\Currency::where("id", $customer_case->customer->currency_id)->first()->iso : "MAD";
?>
@extends('layouts.backend')

@section('js_after')
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/stopwatch.js') }}"></script>
    <script src="{{ asset('js/pages/cases/edit.js') }}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/slick-carousel/slick.min.js') }}"></script>
    @livewireScripts

    <!-- Page JS Helpers (Slick Slider Plugin) -->
    <script>

        // jQuery(function(){ One.helpers('slick'); });
        $(document).ready(function(){
            $('.js-slider').slick({
                dots: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                arrows: true
                //prevArrow: '<button type="button" class="btn btn-sm btn-alt-primary">Previous</button>'
            });
        });

        // tree view
        // var toggler = document.getElementsByClassName("caret");
        // var i;

        // for (i = 0; i < toggler.length; i++) {
        //     toggler[i].addEventListener("click", function() {
        //         this.parentElement.querySelector(".nested").classList.toggle("active");
        //         this.classList.toggle("caret-down");
        //     });
        // }
        $(function() {

            $('.list-group-item').on('click', function() {
                $('.fa[class*="fa-chevron"]', this)
                    .toggleClass('fa-chevron-right')
                    .toggleClass('fa-chevron-down');
            });

        });

        if ( $('#phone').length ) {
            window.setIntlTel("phone");
        }
        // father_phone
        if ( $('#father_phone').length ) {
            window.setIntlTel("father_phone");
        }
        if ( $('#mother_phone').length ) {
            window.setIntlTel("mother_phone");
        }
    </script>
    <script>jQuery(function(){One.helpers(['table-tools-checkable', 'table-tools-sections']);});</script>
    <script>
        $('.option-check').change(function(){
            // console.log($(this).val());
            Livewire.emit('optionChanged', $(this).val());
            /*if ($(this).is(':checked')) {
                console.log('checked');
            } else {
                console.log('unchecked');
            }*/
        });
        Livewire.on('paymentDeleted', (obj, data) => {
            Swal.fire(
                'Modification!',
                'Status modiffié avec succés!',
                'success'
            ).then(() => {
                // location.reload();
            });
            // $('.method-modal-btn').closest('.block').removeClass('block-mode-loading');
        });

    </script>
@endsection

@section('css_after')
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick-theme.css') }}">
    <!-- <style>
        /* Remove default bullets */
        ul, #myUL {
        list-style-type: none;
        }

        /* Remove margins and padding from the parent ul */
        #myUL {
        margin: 0;
        padding: 0;
        }

        /* Style the caret/arrow */
        .caret {
        cursor: pointer;
        user-select: none; /* Prevent text selection */
        }

        /* Create the caret/arrow with a unicode, and style it */
        .caret::before {
        content: "\25B6";
        color: black;
        display: inline-block;
        margin-right: 6px;
        }

        /* Rotate the caret/arrow icon when clicked on (using JavaScript) */
        .caret-down::before {
        transform: rotate(90deg);
        }

        /* Hide the nested list */
        .nested {
        display: none;
        }

        /* Show the nested list when the user clicks on the caret/arrow (with JavaScript) */
        .active {
        display: block;
        }
    </style> -->
    <style>
        .clock-wrapper {
            font-weight: 800;
            font-size: 3.25rem;
        }
        .custom_table tbody {
            display: block;
            max-height: 420px;
            overflow: auto;
        }
        .custom_table thead, .custom_table tbody tr {
            display: table;
            width: 100%;
            table-layout: fixed;
        }
    </style>
    <style>

      .just-padding {
        padding: 15px;
      }

      .list-group.list-group-root {
        padding: 0;
        overflow: hidden;
      }

      .list-group.list-group-root .list-group {
        margin-bottom: 0;
      }

      .list-group.list-group-root .list-group-item {
        border-radius: 0;
        border-width: 1px 0 0 0;
      }

      .list-group.list-group-root > .list-group-item:first-child {
        border-top-width: 0;
      }

      .list-group.list-group-root > .list-group > .list-group-item {
        padding-left: 30px;
      }

      .list-group.list-group-root > .list-group > .list-group > .list-group-item {
        padding-left: 45px;
      }

      .list-group-item .glyphicon {
        margin-right: 5px;
      }
    </style>
    @livewireStyles
@endsection
@section('content')
    <?php  $program_name = $lang === "fr" ? $customer_case->program->name : $customer_case->program->labelEng ?>
    @include('layouts.partials.hero', ['title' => 'Client','subTitle' => $customer_case->customer->firstname . ' ' .$customer_case->customer->lastname . ' (' . $program_name . ')', 'navItems' => ['Admin', 'Edition Client']])
    @php
        $conseiller = \App\Models\User::where('id', $customer_case->customer->lead->conseiller_id)->first();
    @endphp
    <div class="content">
        <div class="row">
            <div class="col-xl-9">
                @livewire('block.info', ['customer_case' => $customer_case, 'errors' => $errors])
                @management
                    @if ($customer_case->program_id === 4)
                    {{-- submissions fees start @only for student --}}
                    @php

                    $canal_list = [
                        "soumission-admission-1",
                        "soumission-admission-2",
                        "soumission-admission-3",
                        "soumission-admission-4",
                        "soumission-admission-5",
                        "soumission-CAQ",
                        "soumission-VISA PE",
                    ];
                    // get tasks
                    $tasks_to_list = [];
                    $first = "";
                    $second = "";
                    $tmp = "";
                    $arr = [];
                    $total = 0;

                    $tasks = $customer_case->comments()
                            ->join('users', 'users.id', '=', 'case_comments.user_id')
                            ->join('roles', 'roles.id', '=', 'users.role_id')
                            ->join('customer_case_case_comment as cc', 'cc.case_comment_id', '=', 'case_comments.id')
                            ->whereIn('cc.canal', $canal_list)
                            ->where(function ($query) {
                                $query->where('role_name', '=', "Admin")
                                        ->orWhere('role_name', '=', "Conseiller")
                                        ->orWhere('role_name', '=', "Manager");
                        })->get(['case_comments.fees', 'cc.canal as canal','case_comments.id', 'is_done as status', 'case_comments.comment', 'case_comments.created_at'])->toArray();

                    foreach ($tasks as $key => $item) {
                        $arr[$item["canal"]][$key] = $item;
                    }
                    foreach ($arr as $yakey => $yaval) {
                        $tmp = array_reverse($yaval);
                        $first = array_pop($tmp);

                        if (isset($first)) {
                            // we gonna show all tasks even its done
                            $currentTime = \Carbon\Carbon::now();
                            $issueTime = Carbon\Carbon::parse($first["created_at"])->addHours(24);
                            if($currentTime->lte($issueTime)) {
                                $first["url"] = "/tasks#pills-todo-today";
                            } else {
                                $first["url"] = "/tasks#pills-overdue-task";
                            }
                            $first['canal'] = $first['canal'] . " (1)";
                            $tasks_to_list[] = $first;

                            // second task
                            $second = array_pop($tmp);
                            if (isset($second)) {
                                $currentTime = \Carbon\Carbon::now();
                                $issueTime = \Carbon\Carbon::parse($second["created_at"])->addHours(12);

                                if($currentTime->lte($issueTime)) {
                                    $second["url"] = "/tasks#pills-follow-up-today";
                                } else {
                                    $second["url"] = "/tasks#pills-overdue-follow-up";
                                }
                                $second['canal'] = $second['canal'] . " (2)";
                                $tasks_to_list[] = $second;
                            }
                        }
                    }

                    // total fees
                    foreach($tasks_to_list as $task_in_lst_cont) {
                            $total += $task_in_lst_cont['fees'];
                    }
                @endphp
                    <div class="block block-rounded">
                        <div class="block-header block-header-default">
                            <h3 class="block-title"><i class="fa fa-money-check"></i> les frais des taches soumission</h3>
                        </div>
                        <div class="block-content">
                            <table class="table table-sm table-vcenter">
                                <thead>
                                <tr class="meets">
                                    <th class="d-none d-sm-table-cell" style="width: 40%;">Type de document</th>
                                    <th class="px-4" style="width: 20%;">Frais</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($tasks_to_list as $task_in_lst)
                                    <tr>
                                        <?php
                                            $task_name = ucfirst(str_replace("soumission-", "", $task_in_lst['canal']));
                                        ?>
                                        <td class="d-none d-sm-table-cell">
                                            <span>{{ $task_name }}</span>
                                        </td>
                                        <td class="d-none d-sm-table-cell">
                                            <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-light text-dark">{{ $task_in_lst['fees'] != 0 ? $task_in_lst['fees'] . " " . $currency : "-"}}</span>
                                        </td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <th class="d-none d-sm-table-cell">
                                        <span>Total frais: </span>
                                    </th>
                                    <th class="d-none d-sm-table-cell">
                                        <span class="fs-xs fw-semibold d-inline-block py-1 px-3 rounded-pill bg-dark text-light">{{ $total != 0 ? $total . " " . $currency : "-"}}</span>
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                @endmanagement
                {{-- submissions fees end --}}

                @livewire('block.histocall', ['lead' => $customer_case->customer->lead])
                @livewire('block.message', ['customer_case' => $customer_case])
                @php
                    // get current meet
                    $last_payment = $customer_case->customer->payments()->withPivot('performed_at')->wherePivot('performed_at','!=','null')->get()->last();
                    $current_meet = 0;
                    if ($last_payment) {
                        $current_meet = $last_payment->id + 1;
                    }
                @endphp
                <div class="block block-rounded block-mode-hidden"  style="border: 1px solid #474747;">
                    @livewire('block.partials.header', ['meet' => 1])
                    <div class="block-content block-header-default">
                        @livewire('block.meet', ['customer_case' => $customer_case, 'prefix' => 'Rencontre 1', 'en_prefix' => 'Meeting 1'])
                        @management
                            @if($customer_case->program_id == 1)
                                @livewire('block.cnp', ['customer_case' => $customer_case])
                            @endif
                            @livewire('block.media', ['customer_case' => $customer_case])
                        @endmanagement
                        @php
                            $clear_fields = \App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first();
                            $visa_requests = \App\Models\CustomerVisaRequest::where('customer_id', $customer_case->customer_id)->orderBy('id', 'ASC')->get();
                            // dd($clear_fields->visa_request != 82);
                        @endphp
                        @if($customer_case->program_id == 4 && $clear_fields && $clear_fields->visa_request_result == 85)
                            <div class="block block-rounded">
                                <div class="block-content">
                                    <div class="row">
                                        <div class="col-md-6">
                                            @livewire('block.result', ['customer_case' => $customer_case, 'meet' => 'R1'])
                                        </div>
                                        <div class="col-md-6">
                                            @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-Demande de note'])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @agent
                            @livewire('block.payment', ['customer_case' => $customer_case, 'payment' => 1])
                        @endagent
                    </div>
                </div>
                <div class="block block-rounded{{ $current_meet == 2 ? '':' block-mode-hidden' }}" style="border: 1px solid {{ $current_meet == 2 ? '#ff8787':'#474747' }};">
                    @livewire('block.partials.header', ['meet' => 2])
                    <div class="block-content block-header-default">
                        @livewire('block.meet', ['customer_case' => $customer_case, 'prefix' => 'Rencontre 2', 'en_prefix' => 'Meeting 2'])
                        @php
                            $doc    = \App\Models\DocumentType::where("name_fr", "Clear1002 - Orientation")->first();
                            $media = $customer_case->customer->getFirstMedia($doc->collection_name);
                            $meets2 = \App\Models\Calendrier::all()->filter(function ($meet) use ($customer_case) {
                                    return $meet->customer_id == $customer_case->customer->id && (str_starts_with($meet->title, 'Rencontre 2') || str_starts_with($meet->title, 'R2')) ;
                                });
                        @endphp

                        @management
                        <div class="block block-rounded">
                            <div class="block-header block-header-default">
                                @if($customer_case->program_id == 4)
                                    @php
                                        $doc    = \App\Models\DocumentType::where("name_fr", "Clear1002 - Orientation")->first();
                                        $media = $customer_case->customer->getFirstMedia($doc->collection_name);
                                        $informationSheet = \App\Models\InformationSheet::where('name_fr', 'Clear1002 - Orientation')->first();
                                    @endphp
                                    <h3 class="block-title"><i class="fa fa-2x fa-comments me-1"></i> {{ __("lang.portal_meeting_title") }} 2 - Orientation pour la demande d’admission
                                    </h3>
                                    <div class="block-options">
                                        <a class="btn btn-sm btn-alt-primary" href="{{ route('edit-information-sheet', ['customer_case'=> $customer_case, 'information_sheet' => $informationSheet]) }}" target="_blank">
                                            @if(!empty($customer_case->customer->getFirstMedia($doc->collection_name))) Editer @else Faire  @endif clear 1002
                                        </a>
                                    </div>
                                @else
                                    <h3 class="block-title"><i class="fa fa-2x fa-comments me-1"></i>  {{ __("lang.portal_meeting_title") }} 2 - {{ __("lang.portal_ee_meet_two_content") }}</h3>
                                @endif
                                {{--@if(!auth()->user()->isAdmin())
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-fw fa-lock text-danger"></i>
                                    </div>
                                </div>
                                @endif--}}
                            </div>
                            @php
                                $payment1 = \App\Models\CaseState::where("sub_step_state_id", 3)
                                    ->where("customer_case_id", $customer_case->id)
                                    ->first();
                                // $this->payment_status = $payment->status;
                            @endphp
                            @if(auth()->user()->isAdmin() || $payment1->status == 1)
                                <div class="block-content">
                                    <!-- clear step 9 checker -->
                                    <table class="table table-sm table-vcenter">
                                        <thead>
                                        <tr class="meets">
                                            <th class="d-none d-sm-table-cell" style="width: 40%;"> {{ __("lang.portal_doc_type") }}</th>
                                            <th style="width: 20%;">Actions</th>
                                        </tr>
                                        </thead>
                                        @if($customer_case->program_id == 4)
                                        <tr>
                                            <td class="font-size-sm">Clear1002 - Rapport d'orientation client</td>
                                            <td class="font-size-sm">
                                                @if(!empty($media))
                                                    @include('cases.steps.partials.document', ['document' => $doc->collection_name_fr, 'collectionName' => $doc->collection_name, 'media' => $media])
                                                @else
                                                   En cours
                                                @endif
                                            </td>
                                        </tr>
                                            @php
                                                $media_sheets = \App\Models\MediaSheet::with('substeps');
                                            @endphp
                                            @foreach($media_sheets->get() as $mediaSheet)
                                                @if(!$mediaSheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                                                    <tr>
                                                        <td class="font-size-sm">{{ $mediaSheet->name_fr }}</td>
                                                        <td class="font-size-sm"> @include('cases.steps.partials.sheet-media', ['mediaSheet' => $mediaSheet, 'formId' => Str::random(10)])</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @else
                                            <tr>
                                                <td class="font-size-sm">CLEAR11 - {{ __("lang.portal_clear11") }} </td>
                                                <td class="font-size-sm"><a class="btn btn-sm btn-light" href="{{ route('clear11-download') }}" target="_blank">
                                                        <i class="fa fa-fw fa-download text-success"></i>
                                                    </a></td>
                                            </tr>
                                        @endif
                                        @php
                                            $media_sheets = \App\Models\MediaSheet::with('substeps');
                                        @endphp
                                        @foreach($media_sheets->get() as $mediaSheet)
                                            @if(!$mediaSheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [10])->get()->isEmpty())
                                                <tr>
                                                    <td class="font-size-sm">
                                                        @if ($mediaSheet->id == 10)
                                                            <h3 class="block-title">Clear12 - {{ $lang === "fr" ? $mediaSheet->name_fr : $mediaSheet->labelEng }}</h3>
                                                        @else
                                                            <h3 class="block-title">{{ $lang === "fr" ? $mediaSheet->name_fr : $mediaSheet->labelEng }}</h3>
                                                        @endif
                                                    </td>
                                                    <td class="font-size-sm">
                                                        @include('cases.steps.partials.sheet-media', ['mediaSheet' => $mediaSheet, 'formId' => Str::random(10)])
                                                    </td>

                                                </tr>
                                            @endif
                                        @endforeach
                                        @php
                                            $sheets = \App\Models\InformationSheet::with('substeps');
                                        @endphp
                                        @foreach($sheets->get() as $sheet)
                                            @if(!$sheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                                                @if($sheet->id == 3)
                                                    <tr>
                                                        <td class="font-size-sm">
                                                            Clear13 - {{ $lang === "fr" ? $sheet->name_fr : $sheet->name_en }}s
                                                        </td>
                                                        <td class="font-size-sm">
                                                            @include('cases.steps.partials.sheet', ['informationSheet' => $sheet, 'formId' => Str::random(10)])
                                                        </td>
                                                    </tr>
                                                    @php

                                                        if ($customer_case->program_id != 4) {
                                                            $clear1004 = \App\Models\CustomerClear1004Fields::where("customer_id", $customer_case->customer_id)->first();
                                                        }
                                                    @endphp
                                                    @if($customer_case->program_id != 4 && $clear1004 && $clear1004->spouse_joining == "spouse_joining-B")
                                                        <tr>
                                                            <td class="font-size-sm">
                                                                Clear13 - {{ $lang === "fr" ? $sheet->name_fr : $sheet->name_en }}s (Couple)
                                                            </td>
                                                            <td class="font-size-sm">
                                                                @include('cases.steps.partials.sheet', ['informationSheet' => $sheet, 'formId' => Str::random(10), 'join' => 'Couple'])
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @else
                                                    <tr>
                                                        <td class="font-size-sm">
                                                            @if($sheet->id==3)
                                                                Clear13 - {{ $lang === "fr" ? $sheet->name_fr : $sheet->name_en }}s
                                                            @else
                                                                {{ $lang === "fr" ? $sheet->name_fr : $sheet->name_en }}
                                                            @endif
                                                        </td>
                                                        <td class="font-size-sm">
                                                            @include('cases.steps.partials.sheet', ['informationSheet' => $sheet, 'formId' => Str::random(10)])
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endif
                                        @endforeach
                                        @if($customer_case->program_id != 4)
                                        <tr>
                                            @php
                                                $stat = \App\Models\SubStepState::where('name_fr', 'Guide de préparation de test de langue')->first();
                                                $cs = App\Models\CaseState::where("sub_step_state_id", $stat->id)
                                                    ->where("customer_case_id", $customer_case->id)
                                                    ->first();
                                                if(!$cs) {
                                                    $cs = \App\Models\CaseState::create([
                                                        'user_id' => Auth::id(),
                                                        'customer_case_id' => $customer_case->id,
                                                        'sub_step_state_id' => $stat->id,
                                                        'status' => false,
                                                    ]);
                                                }
                                            @endphp
                                            <td class="font-size-sm">
                                                @if($cs->status)
                                                    <i class="fa fa-check text-success mr-4"></i>
                                                @else
                                                    <i class="fa fa-sync fa-spin text-warning mr-4"></i>
                                                @endif
                                                <?= $lang === "fr" ? $stat->name_fr : $stat->name_en?>
                                            </td>
                                            <td class="font-size-sm">
                                                <a class="btn btn-lg btn-light case-state-update" data-case="{{$cs->id}}">
                                                    @if($cs->status)
                                                        <i class="fas fa-toggle-on"></i>
                                                    @else
                                                        <i class="fas fa-toggle-off"></i>
                                                    @endif
                                                </a>
                                            </td>
                                        </tr>
                                        @endif
                                    </table>
                                </div>
                            @endif
                        </div>
                        @endmanagement
                        @agent
                        @livewire('block.payment', ['customer_case' => $customer_case, 'payment' => 2])
                        @php
                            $pid = 1;
                            if ($customer_case->program_id == 4)
                                $pid = 4;
                            $clear1004 = \App\Models\CustomerClear1004Fields::where("customer_id", $customer_case->customer_id)->first();
                            $has_couple = ($clear1004 && $clear1004->spouse_joining == "spouse_joining-B") ? 1: 0;
                            $options = \App\Models\ProgramOption::where('program_payment_id', 2)
                                ->where('program_id', $pid);
                            if (!$has_couple) {
                                $options->where('has_couple', 0);
                            }
                            // dd($options->get());
                        @endphp
                        @if($options->count())
                            <div class="block block-rounded">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Options</h3>
                                </div>
                                <div class="block-content">
                                    <ul style="list-style-type:none;padding:0;">
                                        @foreach($options->get() as $option)
                                            <li>
                                                <div class="block mb-0">
                                                    <div class="block-header">
                                                        <h3 class="block-title">{{  $lang === "fr" ? $option->name_fr : $option->labelEng }}</h3>
                                                        <div class="block-options">
                                                            <div class="block-options-item text-danger">{{ $option->amount }} DH</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                        @endagent
                        @management
                        @if($customer_case->program_id == 1)
                            <div class="row">
                                <div class="col-md-6">
                                    @livewire('block.result', ['customer_case' => $customer_case, 'meet' => 'R4'])
                                </div>
                                <div class="col-md-6">
                                    @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-WES'])
                                </div>
                            </div>
                        @endif
                        @endmanagement

                    </div>
                </div>
                <div class="block block-rounded{{ $current_meet == 3 ? '':' block-mode-hidden' }}"  style="border: 1px solid {{ $current_meet == 3 ? '#ff8787':( $current_meet > 3 ? '#474747':'#bebfbc') }};">
                    @livewire('block.partials.header', ['meet' => 3])
                    <div class="block-content block-header-default">
                        @livewire('block.meet', ['customer_case' => $customer_case, 'prefix' => 'Rencontre 3', 'en_prefix' => 'Meeting 3'])
                        @php
                            $payment = \App\Models\CaseState::where("sub_step_state_id", 5)
                            ->where("customer_case_id", $customer_case->id)
                            ->first();
                        @endphp
                        @if(auth()->user()->isAdmin() || $payment->status)
                            @management
                            <div class="block block-rounded">
                                <div class="block-header block-header-default">
                                    @if($customer_case->program_id == 4)
                                        <h3 class="block-title"><i class="fa fa-2x fa-comments me-1"></i> {{ __("lang.portal_meeting_title") }} 3: Préparation de la demande d’admission</h3>
                                    @else
                                        @php $user_progam = \App\Models\Program::find($customer_case->program_id) ;@endphp
                                        <h3 class="block-title"><i class="fa fa-2x fa-comments me-1"></i> {{ __("lang.portal_meeting_title") }} 3: {{ __("lang.portal_ee_meet_three_content_one") }} {{ $lang === "fr" ? $user_progam->name : $user_progam->labelEng }} {{ __("lang.portal_ee_meet_three_content_two") }}</h3>
                                    @endif
                                    @php
                                        // dd($customer_case->program_step_id)
                                        $meets = \App\Models\Calendrier::all()->filter(function ($meet) use ($customer_case) {
                                            return $meet->customer_id == $customer_case->customer->id && (str_starts_with($meet->title, 'Rencontre 3') || str_starts_with($meet->title, 'R3')) ;
                                        });
                                        // dd(count($meets));
                                    @endphp

                                </div>
                                <div class="block-content">
                                    <!-- clear step 9 checker -->
                                    @php
                                        $media_sheets = \App\Models\MediaSheet::with('substeps');
                                    @endphp
                                    <table class="table table-sm table-vcenter">
                                        <thead>
                                        <tr class="calls">
                                            <th class="d-none d-sm-table-cell" style="width: 40%;">{{ __("lang.portal_doc_type") }}</th>
                                            <th style="width: 20%;">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($media_sheets->get() as $mediaSheet)
                                            @if(!$mediaSheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [11, 12, 13])->get()->isEmpty())
                                                <tr>
                                                    <td>
                                                        {{ $mediaSheet->name_fr }}
                                                    </td>
                                                    <td>
                                                        @include('cases.steps.partials.sheet-media', ['mediaSheet' => $mediaSheet, 'formId' => Str::random(10)])
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{--@livewire('button.document', ['customerCase' => $customer_case])--}}
                                </div>
                            </div>
                            @endmanagement
                            @agent
                            @livewire('block.payment', ['customer_case' => $customer_case, 'payment' => 3])
                            @endagent
                            @management
                            @livewire('block.program', ['customer_case' => $customer_case])
                            <div class="row">
                                <div class="col-md-6">
                                    @if($customer_case->program_id == 1)
                                        @livewire('block.soumission', ['customer_case' => $customer_case])
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    @if($customer_case->program_id == 1)
                                        @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-PROFIL'])
                                    @endif
                                </div>
                            </div>
                            @if($customer_case->program_id == 4)
                                @php
                                    $admissions = App\Models\AdmissionUrl::where('customer_id', $customer_case->customer_id)->count();
                                    $cmnts = $customer_case->comments()->wherePivot('canal', 'Like', "soumission-admission-%")->count();
                                    $docs = ["Lettre modèle", 'Accusé de réception', 'Reçu', 'Lettre de suivi', "Lettre d'acceptation", "Lettre de refus"];
                                    $have_docs = false;
                                    // dd($cmnts);
                                @endphp
                                @foreach($docs as $doc)
                                    @foreach(range(1, 5) as $idx)
                                        @php
                                            $collectionName = $doc . "-admission-" . $idx;
                                            $media = $customer_case->customer->getFirstMedia($collectionName);
                                            if ($media) {
                                                $have_docs = true;
                                                break;
                                            }
                                            // dd($media);
                                        @endphp
                                    @endforeach
                                @endforeach
                                @if($admissions > 0 || $cmnts > 0 || $have_docs)
                                    @livewire('block.admission.program', ['customer_case' => $customer_case])
                                @endif
                            @endif
                            @endmanagement
                        @endif
                    </div>
                </div>
                <div class="block block-rounded{{ $current_meet == 4 ? '':' block-mode-hidden' }}"  style="border: 1px solid {{ $current_meet == 4 ? '#ff8787':( $current_meet > 4 ? '#474747':'#bebfbc') }};">
                    @livewire('block.partials.header', ['meet' => 4])
                    <div class="block-content block-header-default">
                        @livewire('block.meet', ['customer_case' => $customer_case, 'prefix' => 'Rencontre 4', 'en_prefix' => 'Meeting 4'])
                        @php
                            $payment = \App\Models\CaseState::where("sub_step_state_id", 10)
                            ->where("customer_case_id", $customer_case->id)
                            ->first();
                        @endphp
                        @if(auth()->user()->isAdmin() || $payment->status)
                            @management
                            <div class="block block-rounded">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">
                                        <i class="fa fa-2x fa-comments me-1"></i>
                                        @if($customer_case->program_id == 4)
                                            Rencontre 4: Préparation de la demande de visa et PE
                                        @else
                                            Rencontre 4 : préparation de la demande Visa et immigration
                                        @endif
                                    </h3>
                                    @php
                                        // dd($customer_case->program_step_id)
                                        $meets = \App\Models\Calendrier::all()->filter(function ($meet) use ($customer_case) {
                                            return $meet->customer_id == $customer_case->customer->id && (str_starts_with($meet->title, 'Rencontre 3') || str_starts_with($meet->title, 'R3')) ;
                                        });
                                        // dd(count($meets));
                                    @endphp
                                    @if(!auth()->user()->isAdmin() && count($meets) == 0)
                                    <div class="block-options">
                                        <div class="block-options-item">
                                            <i class="fa fa-fw fa-lock text-danger"></i>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="block-content">
                                    @if($customer_case->program_id == 4)
                                        <table class="table table-sm table-vcenter">
                                            <thead>
                                            <tr class="calls">
                                                <th class="d-none d-sm-table-cell" style="width: 40%;">{{ __("lang.portal_doc_type") }}</th>
                                                <th style="width: 20%;">Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                @php $mediaSheet = \App\Models\MediaSheet::find(4); @endphp
                                                <td>
                                                    {{ $mediaSheet->name_fr }}
                                                </td>
                                                <td>
                                                    @include('cases.steps.partials.sheet-media', ['mediaSheet' => $mediaSheet, 'formId' => Str::random(10)])
                                                </td>
                                            </tr>
                                            <tr>
                                                @php $mediaSheet = \App\Models\MediaSheet::find(5); @endphp
                                                <td>{{ $mediaSheet->name_fr }}</td>
                                                <td>
                                                    @include('cases.steps.partials.sheet-media', ['mediaSheet' => $mediaSheet, 'formId' => Str::random(10)])
                                                </td>
                                            </tr>
                                            <tr>
                                                @php $mediaSheet = \App\Models\MediaSheet::find(6); @endphp
                                                <td>
                                                    {{ $mediaSheet->name_fr }}
                                                </td>
                                                <td>
                                                    @include('cases.steps.partials.sheet-media', ['mediaSheet' => $mediaSheet, 'formId' => Str::random(10)])
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    @else
                                        <table class="table table-sm table-vcenter">
                                            <thead>
                                            <tr class="calls">
                                                <th class="d-none d-sm-table-cell" style="width: 40%;">{{ __("lang.portal_doc_type") }}</th>
                                                <th style="width: 20%;">Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                                $media_sheets = \App\Models\MediaSheet::with('substeps');
                                            @endphp
                                            @foreach($media_sheets->get() as $mediaSheet)
                                                @if(!$mediaSheet->substeps()->wherePivot('program_id', $customer_case->program_id)->wherePivotIn('program_sub_step_id', [18])->get()->isEmpty())

                                                    <tr>
                                                        <td>
                                                            {{ $mediaSheet->name_fr }}
                                                        </td>
                                                        <td>
                                                            @include('cases.steps.partials.sheet-media', ['mediaSheet' => $mediaSheet, 'formId' => Str::random(10)])
                                                        </td>
                                                    </tr>

                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                            @if($customer_case->program_id == 4)
                                <div class="row">
                                    <div class="col-md-6">
                                        @livewire('block.result', ['customer_case' => $customer_case, 'meet' => 'R4'])
                                    </div>
                                    <div class="col-md-6">
                                        <div class="table-responsive push">
                                            <table class="js-table-sections table table-bordered table-vcenter mb-0">
                                                <tbody class="js-table-sections-header show table-active">
                                                <tr>
                                                    <td>
                                                        <div class="block m-0">
                                                            <div class="block-header">
                                                                <h3 class="block-title">Tache CAQ</h3>
                                                                <div class="block-options">
                                                                    <div class="block-options-item">
                                                                        <span class="badge badge-primary">{{ $customer_case->comments()->wherePivot('canal', '=', "soumission-CAQ")->count() }}</span>
                                                                    </div>
                                                                    <span class="btn-block-option text-warning">
                                                                    <i class="far fa-fw fa-bell"></i>
                                                                </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-CAQ'])
                                                        {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-CAQ']) --}}
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tbody class="js-table-sections-header">
                                                <tr>
                                                    <td>
                                                        <div class="block m-0">
                                                            <div class="block-header">
                                                                <h3 class="block-title">Tache CPG</h3>
                                                                <div class="block-options">
                                                                    <div class="block-options-item">
                                                                        <span class="badge badge-primary">{{ $customer_case->comments()->wherePivot('canal', '=', "soumission-CPG")->count() }}</span>
                                                                    </div>
                                                                    <span class="btn-block-option text-warning">
                                                                    <i class="far fa-fw fa-bell"></i>
                                                                </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-CPG'])
                                                        {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-CPG']) --}}
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tbody class="js-table-sections-header">
                                                <tr>
                                                    <td>
                                                        <div class="block m-0">
                                                            <div class="block-header">
                                                                <h3 class="block-title">Tache Facture scolaire</h3>
                                                                <div class="block-options">
                                                                    <div class="block-options-item">
                                                                        <span class="badge badge-primary">{{ $customer_case->comments()->wherePivot('canal', '=', "soumission-Facture scolaire")->count() }}</span>
                                                                    </div>
                                                                    <span class="btn-block-option text-warning">
                                                                    <i class="far fa-fw fa-bell"></i>
                                                                </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-Facture scolaire'])
                                                        {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-Facture scolaire']) --}}
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="block block-rounded">
                                    <div class="block-content">
                                        <div class="row">
                                            <div class="col-md-6">
                                                @livewire('block.result', ['customer_case' => $customer_case, 'meet' => 'R5'])
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="table-responsive push">
                                                        <table class="js-table-sections table table-bordered table-vcenter mb-0">
                                                            <tbody class="js-table-sections-header show table-active">
                                                            <tr>
                                                                <td>
                                                                    <div class="block m-0">
                                                                        <div class="block-header">
                                                                            <h3 class="block-title">Tache Prise en charge</h3>
                                                                            <div class="block-options">
                                                                                <div class="block-options-item">
                                                                                    <span class="badge badge-primary">{{ $customer_case->comments()->wherePivot('canal', '=', "soumission-Prise en charge")->count() }}</span>
                                                                                </div>
                                                                                <span class="btn-block-option text-warning">
                                                                        <i class="far fa-fw fa-bell"></i>
                                                                    </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-Prise en charge'])
                                                                    {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-Prise en charge']) --}}
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                            <tbody class="js-table-sections-header">
                                                            <tr>
                                                                <td>
                                                                    <div class="block m-0">
                                                                        <div class="block-header">
                                                                            <h3 class="block-title">Tache Imm5645</h3>
                                                                            <div class="block-options">
                                                                                <div class="block-options-item">
                                                                                    <span class="badge badge-primary">{{ $customer_case->comments()->wherePivot('canal', '=', "soumission-Imm5645")->count() }}</span>
                                                                                </div>
                                                                                <span class="btn-block-option text-warning">
                                                                        <i class="far fa-fw fa-bell"></i>
                                                                    </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-Imm5645'])
                                                                    {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-Imm5645']) --}}
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                            <tbody class="js-table-sections-header">
                                                            <tr>
                                                                <td>
                                                                    <div class="block m-0">
                                                                        <div class="block-header">
                                                                            <h3 class="block-title">Tache Imm5476</h3>
                                                                            <div class="block-options">
                                                                                <div class="block-options-item">
                                                                                    <span class="badge badge-primary">{{ $customer_case->comments()->wherePivot('canal', '=', "soumission-Imm5476")->count() }}</span>
                                                                                </div>
                                                                                <span class="btn-block-option text-warning">
                                                                        <i class="far fa-fw fa-bell"></i>
                                                                    </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-Imm5476'])
                                                                    {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-Imm5476']) --}}
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @endmanagement
                            @agent
                                @livewire('block.payment', ['customer_case' => $customer_case, 'payment' => 4])
                                @php
                                    //$clear1004 = \App\Models\CustomerClear1004Fields::where("customer_id", $customer_case->customer_id)->first();
                                    //$has_couple = ($clear1004 && $clear1004->spouse_joining == "spouse_joining-B") ? 1: 0;
                                    $options = \App\Models\ProgramOption::where('program_payment_id', 4)
                                        ->where('program_id', $pid);
                                    //dd($pid);
                                    if (!$has_couple) {
                                        $options->where('has_couple', 0);
                                    }
                                    // dd($options);
                                @endphp
                                @if($options->count())
                                    <div class="block">
                                        <div class="block-header">
                                            <h3 class="block-title">Options</h3>
                                        </div>
                                        <div class="block-content">
                                            <ul style="list-style-type:none;padding:0;">
                                                @foreach($options->get() as $option)
                                                    <li>
                                                        <div class="block mb-0">
                                                            <div class="block-header">
                                                                <h3 class="block-title">{{  $lang === "fr" ? $option->name_fr : $option->labelEng }}</h3>
                                                                <div class="block-options">
                                                                    <div class="block-options-item text-danger">{{ $option->amount }} DH</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                            @endagent
                        @endif
                    </div>
                </div>
                <div class="block block-rounded{{ $current_meet == 5 ? '':' block-mode-hidden' }}"  style="border: 1px solid {{ $current_meet == 5 ? '#ff8787':( $current_meet > 5 ? '#474747':'#bebfbc') }};">
                    @livewire('block.partials.header', ['meet' => 5])
                    <div class="block-content block-header-default">
                        @livewire('block.meet', ['customer_case' => $customer_case, 'prefix' => 'Rencontre 5', 'en_prefix' => 'Meeting 5'])
                        @php
                            $payment = \App\Models\CaseState::where("sub_step_state_id", 20)
                            ->where("customer_case_id", $customer_case->id)
                            ->first();
                        @endphp
                        @if(auth()->user()->isAdmin() || $payment->status)
                            @agent
                                @livewire('block.payment', ['customer_case' => $customer_case, 'payment' => 5])
                            @endagent
                            @management
                            @if($customer_case->program_id != 1)
                                @livewire('block.result', ['customer_case' => $customer_case, 'meet' => 'R5'])
                            @endif
                            <div class="row">
                                <div class="col-md-6">
                                    @livewire('block.visa.soumission', ['customer_case' => $customer_case, 'soumission' => true])
                                    @livewire('block.visa.soumission', ['customer_case' => $customer_case, 'soumission' => false])
                                </div>
                                <div class="col-md-6">
                                    @if($customer_case->program_id == 1)
                                        @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-VISA'])
                                    @endif
                                    @if($customer_case->program_id == 4)
                                        @livewire('elements.task', ['customer_case' => $customer_case, 'canal' => 'soumission-VISA PE'])
                                        {{-- @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-VISA PE']) --}}
                                    @endif
                                </div>
                            </div>
                            @endmanagement
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="block block-rounded">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">{{ __("lang.portal_about_client_title") }}</h3>
                    </div>
                    <div class="block-content">
                        <table class="table table-borderless table-vcenter">
                            <tbody>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-calendar mr-1"></i> <strong>{{ __("lang.portal_personal_infos_programm") }}</strong> {{ \App\Models\Program::where('id', $customer_case->program_id)->first()->name }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-business-time mr-1"></i> <strong>{{ __("lang.portal_about_the_lead_reg_date") }}</strong> {{ ($customer_case->created_at)->format('d M Y') }}
                                    <!-- 13 Juin 2021 -->
                                </td>
                            </tr>
                            @admin
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-user mr-1"></i> <strong>{{ __("lang.portal_about_the_lead_assigned_to") }}</strong>  <em>{{ $conseiller->name }}</em>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-socks mr-1"></i> <strong>{{ __("lang.campaign_id") }}</strong>  <em>{{ ($customer_case->customer->lead->campaign_id) ? App\Models\LeadFormMarketingCampaign::where('id', $customer_case->customer->lead->campaign_id)->first()->campaign_name : "N/A" }}</em>
                                </td>
                            </tr>
                            @endadmin
                            <tr>
                                <td>
                                    <?php $admission_state = \App\Models\AdmissionState::where("id", $customer_case->customer->admission_state)->first(); ?>
                                    <i class="fa fa-fw fa-satellite mr-1"></i> <strong>{{ __("lang.portal_about_the_lead_status") }}</strong>  <em><span class="badge badge-info">{{ ($admission_state) ? $admission_state->label : 'N/A' }}</span></em>
                                </td>
                            </tr>
                            @admin
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-user mr-1"></i> <strong>Phase</strong>  <a href="{{ route('lead-view', ['lead' => $customer_case->customer->lead->id]) }}">Prospection</a>
                                </td>
                            </tr>
                            @endadmin
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- Start comments -->
                @livewire('block.email', ['customer_case' => $customer_case])
                @livewire('block.comment', ['customer_case' => $customer_case])
                @management_backoffice
                    @livewire('block.comment', ['customer_case' => $customer_case, 'canal' => 'soumission'])
                @endmanagement_backoffice
                {{-- @livewire('block.email', ['customer_case' => $customer_case]) --}}
                 <!-- End comments -->
            </div>
        </div>
    </div>

    <!-- payment Add Modal -->
    <div class="modal" id="modal-payment-add" tabindex="-1" role="dialog" aria-labelledby="modal-payment-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">{{ __("lang.portal_payment_method") }}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form Payment Add -->
                                <form action="" method="POST" onsubmit="return false;" id="payment-form">
                                    <div class="form-group">
                                        <label for="method-select">{{ __("lang.portal_select_payment_method") }}</label>
                                        <select class="form-control" id="method-select" name="method-select" style="width: 100%">
                                        </select>
                                    </div>
                                    <!-- <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id=""><i class="fa fa-fw fa-plus mr-1"></i>Ajouter appel</button>
                                    </div> -->
                                </form>
                                <!-- END Form histocall Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <?php
                        $case_state_3 = App\Models\CaseState::where("sub_step_state_id", 3)
                            ->where("customer_case_id", $customer_case->id)
                            ->first();
                        ?>
                        <button type="button" class="btn btn-alt-primary mr-1 case-state-update" data-method="1" data-case="{{ $case_state_3->id }}" data-dismiss="modal">{{ __("lang.portal_payment_method_pay") }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Payment Add Modal -->
    <!-- calendar Add Modal -->
    <div class="modal" id="modal-calendar-add" tabindex="-1" role="dialog" aria-labelledby="modal-calendar-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-calendar-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter planification</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form calendar Add -->
                                <form action="{{route("calendar.create")}}" method="POST" id="calendar-add-form">
                                    @csrf
                                    <div class="form-group text-center">
                                        <h3><span></span> : {{$customer_case->customer->lastname}} {{$customer_case->customer->firstname}} - {{ $conseiller->name}}</h3>
                                        <input type="hidden" value=" : {{$customer_case->customer->lastname}} {{$customer_case->customer->firstname}} - {{ $conseiller->name}}" id="add-form-title" name="add-form-title">
                                        <input type="hidden" value="{{ $customer_case->customer->id }}" id="add-form-lead" name="add-form-lead">
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-date">Date Rencontre</label>
                                        <input required type="date" class="form-control form-control-alt" id="add-form-date" name="add-form-date" placeholder="Date Planification.." aria-describedby="add-form-date-error" aria-invalid="false">
                                        <div id="add-form-date-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group form-row">
                                        <div class="col-6">
                                            <label for="add-form-time">Heure Début Rencontre</label>
                                            <input required type="time" class="form-control form-control-alt" id="add-form-time" name="add-form-time" aria-describedby="add-form-time-error" aria-invalid="false">
                                            <div id="add-form-time-error" class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-6">
                                        <label for="add-form-time_end">Heure Fin Rencontre</label>
                                        <input required type="time" class="form-control form-control-alt" id="add-form-time_end" name="add-form-time_end" aria-describedby="add-form-time_end-error" aria-invalid="false" readonly>
                                        <div id="add-form-time_end-error" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!-- <button type="submit" class="btn btn-alt-primary btn-block" id="add-calendar-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter Planification</button> -->
                                    </div>
                                </form>
                                <!-- END Form calendar Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" id="add-calendar-btn">Ajouter Rencontre</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END calendar Add Modal -->
    <!-- histocall Customer Add Modal -->
    <div class="modal" id="modal-histocall-customer-add" tabindex="-1" role="dialog" aria-labelledby="modal-histocall-customer-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-histocall-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter appel client</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form histocall customer Add -->
                                <form action="" method="POST" onsubmit="return false;" id="histocall-add-customer-form">
                                    <input type="hidden" name="add-form-lead_id" value="{{ $customer_case->customer->lead->id }}">
                                    <p style="line-height: 24px;font-size: 26px;width: 100%;"><strong>Téléphone :</strong> {{ $customer_case->customer->phone }}</p>
                                    <div class="row justify-content-md-center">
                                        <div class="col-md-6 input-wrapper">
                                            <div class="buttons-wrapper">
                                                <button class="btn btn-alt-secondary" id="start-cronometer">Start Stopwatch</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <input type="hidden" name="add-form-duration" id="add-form-duration">
                                        <div id="timer" class="col-md-10" style="display: none;">
                                            {{-- <p style="line-height: 24px;font-size: 26px;width: 100%;"><strong>Téléphone :</strong> {{ $customer_case->customer->phone }}</p> --}}
                                            <div class="text-center clock-wrapper h1">
                                                <span class="hours">00</span>
                                                <span class="dots">:</span>
                                                <span class="minutes">00</span>
                                                <span class="dots">:</span>
                                                <span class="seconds">00</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <div class="buttons-wrapper">
                                            <button class="btn btn-alt-secondary" id="resume-timer" style="display: none;"><i class="fa fa-fw fa-play"></i> Resume Timer</button>
                                            <button class="btn btn-alt-secondary" id="stop-timer" style="display: none;"><i class="fa fa-fw fa-pause"></i> Stop Timer</button>
                                            <button class="btn btn-alt-secondary" id="reset-timer" style="display: none;"><i class="fa fa-fw fa-trash-restore"></i> Reset Timer</button>
                                        </div>
                                    </div>
                                    <div class="form-group mt-4">
                                        <label for="add-form-call_descr" class="col-form-label">Note :</label>
                                        <textarea type="text" class="form-control form-control-alt" id="add-form-call_descr" name="add-form-call_descr" placeholder="Notes.." aria-describedby="add-form-call_descr-error" aria-invalid="false"></textarea>
                                        <div id="add-form-call_descr-error" class="invalid-feedback"></div>
                                    </div>
                                        <div class="form-group">
                                            <button type="button" id="add-histocall-customer-btn" class="btn btn-alt-primary btn-block"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter appel</button>
                                        </div>
                                </form>
                                <!-- END Form histocall Add -->
                            </div>
                        </div>
                    </div>
                    {{--<div class="block-content block-content-full text-right border-top">
                        <button type="button" onclick="startStop()" id="add-histocall-customer-btn" class="btn btn-alt-primary mr-1">Ajouter appel</button>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
    <!-- END histocall Add Modal -->
    <!-- END Page Content -->
    @include('billing.modals.services')
@endsection
