@extends(isCustomer() ? 'layouts.customer' : 'layouts.backend')
@section('css_after')
    <meta http-equiv="Expires" content="Mon, 26 Jul 1997 05:00:00 GMT">
    <meta http-equiv="Pragma" content="no-cache">
    @livewireStyles
    <script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pace-js@latest/pace-theme-default.min.css">
    <style>
        .pace {
            -webkit-pointer-events: none;
            pointer-events: none;

            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;

            z-index: 2000;
            position: fixed;
            height: 90px;
            width: 90px;
            margin: auto;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }

        .pace.pace-inactive .pace-activity {
            display: none;
        }

        .pace .pace-activity {
            position: fixed;
            z-index: 2000;
            display: block;
            position: absolute;
            left: -30px;
            top: -30px;
            height: 90px;
            width: 90px;
            display: block;
            border-width: 30px;
            border-style: double;
            border-color: #d52b12 transparent transparent;
            border-radius: 50%;

            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;

            -webkit-animation: spin 1s linear infinite;
            -moz-animation: spin 1s linear infinite;
            -o-animation: spin 1s linear infinite;
            animation: spin 1s linear infinite;
        }

        .pace .pace-activity:before {
            content: ' ';
            position: absolute;
            top: 10px;
            left: 10px;
            height: 50px;
            width: 50px;
            display: block;
            border-width: 10px;
            border-style: solid;
            border-color: #d52b12 transparent transparent;
            border-radius: 50%;

            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
        }

        @-webkit-keyframes spin {
            100% {
                -webkit-transform: rotate(359deg);
            }
        }

        @-moz-keyframes spin {
            100% {
                -moz-transform: rotate(359deg);
            }
        }

        @-o-keyframes spin {
            100% {
                -moz-transform: rotate(359deg);
            }
        }

        @keyframes spin {
            100% {
                transform: rotate(359deg);
            }
        }

        /*clear docs*/
        .document-in {
            margin: 0 !important;
        }

        .document-in label {
            padding: 0 !important;
            margin: 0 !important;
        }

        .document-in .form-group {
            line-height: 0.9;
        }

        .document-in .form-group {
            margin: 0 !important;
        }
    </style>
@endsection
@section('js_after')
    <!-- Page JS Helpers (Table Tools helpers) -->
    <!-- Page JS DataTables Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('js/pages/dropzone.js') }}"></script>
    @livewireScripts
    <script>
        // show table-active
        $(document).on("click", ".toggle-medias", function() {
            //
            $("#table-medias").find("tbody.show").removeClass("show table-active");
            $current = $(this).closest("tbody");
            if (!$current.hasClass("show")) {
                $current.addClass("show table-active")
                //console.log("test");
            }
        });
        $(document).on('click', '.sheet-media-delete', function() {
            let url = $(this).data('url');
            $.ajax({
                method: 'delete',
                url: url,
                /*data:{
                    item_id: "id",
                },*/
                success: (data) => {
                    //
                    jQuery("#sheet-form").load(window.location.href + ' #sheet-form');
                },
                error: () => {
                    console.log("error!");
                }
            });
        });
    </script>
    <script>
        jQuery(function() {
            One.helpers('table-tools-sections');
        });
        /*$(document).on("click",".toggle-medias",function() {
            console.log('test');
        });*/
    </script>
@endsection
@section('content')
    @include('layouts.partials.hero', [
        'title' => 'Clear',
        'subTitle' => 'informations',
        'navItems' => ['Clear', 'informations'],
    ])
    <!-- Page Content -->
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if ($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les
                                informations !</a>
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        @livewire('block.clear-document', ['customerCase' => $customerCase, 'mediaSheet' => $mediaSheet])
    </div>
    <!-- END Page Content -->
    @livewire('modal.clear-document', ['customerCase' => $customerCase, 'mediaSheet' => $mediaSheet])
@endsection
