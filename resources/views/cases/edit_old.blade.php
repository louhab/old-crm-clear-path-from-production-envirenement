@extends('layouts.backend')

@section('js_after')
    <script src="{{ asset('js/plugins/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script>
        $('.collapse-substeps').click(function(){
            $(this).closest('tr').next('.tr-detail').toggle("slow");
        });
        //$('.tr-detail').hide();
        $("#example-masked-time").mask("99:99:99");
    </script>
    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/cases/edit.js') }}"></script>
    <script>
            var x;
            var startstop = 0;

            function startStop() { /* Toggle StartStop */

            startstop = startstop + 1;

            if (startstop === 1) {
                start();
                document.getElementById("start").innerHTML = '<i class="fa fa-fw fa-pause mr-1"></i> stop';
            } else if (startstop === 2) {
                document.getElementById("start").innerHTML = '<i class="fa fa-fw fa-play mr-1"></i> start';
                startstop = 0;
                stop();
            }

            }


            function start() {
            x = setInterval(timer, 10);
            } /* Start */

            function stop() {
            clearInterval(x);
            } /* Stop */

            var milisec = 0;
            var sec = 0; /* holds incrementing value */
            var min = 0;
            var hour = 0;

            /* Contains and outputs returned value of  function checkTime */

            var miliSecOut = 0;
            var secOut = 0;
            var minOut = 0;
            var hourOut = 0;

            /* Output variable End */


            function timer() {
            /* Main Timer */


            miliSecOut = checkTime(milisec);
            secOut = checkTime(sec);
            minOut = checkTime(min);
            hourOut = checkTime(hour);

            milisec = ++milisec;

            if (milisec === 100) {
                milisec = 0;
                sec = ++sec;
            }

            if (sec == 60) {
                min = ++min;
                sec = 0;
            }

            if (min == 60) {
                min = 0;
                hour = ++hour;

            }


            document.getElementById("milisec").innerHTML = miliSecOut;
            document.getElementById("sec").innerHTML = secOut;
            document.getElementById("min").innerHTML = minOut;
            document.getElementById("hour").innerHTML = hourOut;

            }


            /* Adds 0 when value is <10 */


            function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
            }

            function resetChrono() {


                /*Reset*/

                milisec = 0;
                sec = 0;
                min = 0
                hour = 0;

                document.getElementById("milisec").innerHTML = "00";
                document.getElementById("sec").innerHTML = "00";
                document.getElementById("min").innerHTML = "00";
                document.getElementById("hour").innerHTML = "00";

            }
    </script>

@endsection

@section('css_after')
    <link rel="stylesheet" href="<?php echo asset('css/app.css')?>" type="text/css">
@endsection
@section('content')
    @include('layouts.partials.hero', ['title' => 'Client','subTitle' => $customer_case->customer->firstname . ' ' .$customer_case->customer->lastname . ' (' . $customer_case->program->name . ')', 'navItems' => ['Admin', 'Edition Client']])

    <div class="content">
        <!-- <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Collapsible Group Item #1
                    </button>
                </h5>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Collapsible Group Item #2
                    </button>
                </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Collapsible Group Item #3
                    </button>
                </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
                </div>
            </div>
        </div> -->
        <!-- <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block block-rounded">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Title <small>Subtitle</small></h3>
                            </div>
                            <div class="block-content">
                                <p>With header background..</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="block block-rounded">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Title <small>Subtitle</small></h3>
                            </div>
                            <div class="block-content">
                                <p>With header background..</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-xl-8">

                <div class="block block-rounded">
                    <div class="block-content font-size-sm">
                        @php
                            $steps = $customer_case->program->steps()->withPivot('order')->orderBy('order', 'asc')->get();
                            $groupedResultsArray = [];
                        @endphp
                        @foreach($steps as $step)
                            @php
                                $groupedResultsArray[$step->step_phase_id][] = $step;
                            @endphp
                        @endforeach
                        @php
                            $substeps = $customer_case->program->substeps()->withPivot('order')->orderBy('order', 'asc')->get();
                            $groupedResultsArray2 = [];
                        @endphp
                        @foreach($substeps as $substep)
                            @php
                                $groupedResultsArray2[$substep->step_id][] = $substep;
                            @endphp
                        @endforeach
                        @foreach($groupedResultsArray as $phase)
                            <table class="table table-borderless table-vcenter">
                                <tbody>
                                <tr class="table-active">
                                    <th style="width: 50px;"></th>
                                    <th>{{ $phase[0]->phase->phase_label_fr }}</th>
                                    <th class="text-right">
                                        <span class="text-muted">Temps depuis la création : 0.30 heures</span>
                                    </th>
                                </tr>

                                @foreach($phase as $step)
                                    <tr>

                                        @if($customer_case->program_step_id == $step->id)
                                            <td class="table-primary text-center">
                                                <i class="fa fa-fw fa-unlock text-primary"></i>
                                            </td>
                                        @else
                                            @if($customer_case->program_step_id > $step->id)
                                                <td class="table-success text-center">
                                                    <i class="fa fa-fw fa-lock text-success"></i>
                                                </td>
                                            @else
                                                <td class="table-danger text-center">
                                                    <i class="fa fa-fw fa-lock text-danger"></i>
                                                </td>
                                            @endif
                                        @endif
                                        <td>
                                            <a class="collapse-substeps" href="javascript:void(0)">{{ $step->step_label }}</a>
                                        </td>
                                        <td class="text-right">
                                            <em class="text-muted">17 min</em>
                                        </td>
                                    </tr>
                                    @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Admin || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Manager || ((\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Conseiller && $step->visible_for_conseiller) || (\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Agent && $step->visible_for_agent) || (\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Backoffice && $step->visible_for_backoffice)) && $step->id <= $customer_case->step->id)
                                    <!-- Collapse For Substeps -->
                                    <tr id="tr-detail" class="tr-detail" style="{{ ($customer_case->program_step_id == $step->id) ? '': 'display:none'}}">
                                        <!-- <td></td> -->
                                        <td colspan="3">
                                            <div class="detail-content ml-3">
                                                <ul>
                                                    @if(array_key_exists($step->id, $groupedResultsArray2))
                                                    @foreach($groupedResultsArray2[$step->id] as $substep)
                                                        @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Admin || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Manager || (\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Conseiller && $substep->visible_for_conseiller) || (\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Agent && $substep->visible_for_agent) || (\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Backoffice && $substep->visible_for_backoffice))
                                                        <li>
                                                            <div class="detail"></div>
                                                            <div class="detail detail-main">
                                                                <fieldset>
                                                                    <legend><span class="badge badge-primary">{{ $substep->step_label }}</span></legend>
                                                                    <div>
                                                                        <table class="table table-condensed">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th></th>
                                                                                    <th></th>
                                                                                    <th></th>
                                                                                    <td class="text-right"><em class="text-muted">7 min</em></td>
                                                                                </tr>
                                                                            </thead>

                                                                            <tr id="substep-{{$substep->id}}">
                                                                                <td colspan="4">
                                                                                    @if(count($substep->fields()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get()))
                                                                                        <tr>
                                                                                            @include('cases.steps.partials.form-fields', ['form' => $substep])
                                                                                        </tr>
                                                                                    @endif
                                                                                    @php
                                                                                        $sheets = $substep->sheets()->wherePivot('program_id', '=', $customer_case->program_id)->get();
                                                                                    @endphp
                                                                                    @if(count($sheets))
                                                                                        <table  width="100%">
                                                                                        @foreach($sheets as $sheet)
                                                                                            @if($sheet->id == 2)
                                                                                                @continue
                                                                                            @endif
                                                                                            <tr>
                                                                                                <th scope="row">{{ $sheet->name_fr }}</th>
                                                                                                <td>
                                                                                                    <div class="d-flex justify-content-center action">
                                                                                                        <div class="btn-group" style="margin:5px">
                                                                                                            @include('cases.steps.partials.sheet', ['informationSheet' => $sheet, 'formId' => Str::random(10)])
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        @endforeach
                                                                                        </table>
                                                                                    @endif

                                                                                    @php
                                                                                        $substates = $substep->substates()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot('order')->orderBy('order', 'asc')->get();
                                                                                    @endphp
                                                                                    @if(count($substates))
                                                                                        <table width="100%">
                                                                                        @foreach($substates as $stat)
                                                                                            <?php
                                                                                                $case_state = App\Models\CaseState::where("sub_step_state_id", $stat->id)
                                                                                                                                ->where("customer_case_id", $customer_case->id)
                                                                                                                                ->first();
                                                                                                //print_r($case_state);
                                                                                            ?>
                                                                                            <tr>
                                                                                                <th scope="row">
                                                                                                    <div class="pl-3">
                                                                                                        <a class="btn btn-sm btn-light case-state-icon" href="javascript:void(0)">
                                                                                                            @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Admin || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Manager || (\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Conseiller && $stat->visible_for_conseiller) || (\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Agent && $stat->visible_for_agent) || (\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Backoffice && $stat->visible_for_backoffice))
                                                                                                                @if($case_state->status)
                                                                                                                    <i class="fa fa-check text-success mr-4"></i>
                                                                                                                @else
                                                                                                                    <i class="fa fa-sync fa-spin text-warning mr-4"></i>
                                                                                                                @endif
                                                                                                            @else
                                                                                                                <i class="fa fa-fw fa-lock text-success mr-4"></i>
                                                                                                            @endif
                                                                                                            <?php echo $stat->name_fr; ?>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </th>
                                                                                                <td class="">
                                                                                                    @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Admin || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Manager || (\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Conseiller && $stat->visible_for_conseiller) || (\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Agent && $stat->visible_for_agent) || (\Illuminate\Support\Facades\Auth::user()->role_id == \App\Models\Role::Backoffice && $stat->visible_for_backoffice))
                                                                                                        @if(App\Models\SubStepState::where("id", $case_state->sub_step_state_id)->first()->name_fr == "Appel : relance modèle 1")
                                                                                                        <div class="d-flex justify-content-center action">
                                                                                                            <div class="btn-group" style="margin:5px">
                                                                                                                    <button type="button" id="histocall-customer-table-add" class="btn btn-alt-primary">Make Call</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        @else
                                                                                                        <div class="d-flex justify-content-center action">
                                                                                                            <div class="btn-group" style="margin:5px">
                                                                                                                @if($stat->id == 2)
                                                                                                                    @php $customer = $customer_case->customer()->first(); @endphp
                                                                                                                    <a class="btn btn-lg btn-light mr-2" target="_blank" href="https://wa.me/{{ $customer->phone }}?text=Bonjour {{ $customer->firstname }} {{ $customer->lastname }} cv bien ?">
                                                                                                                        <i class="fa fa-bell"></i>
                                                                                                                    </a>
                                                                                                                @endif
                                                                                                                <a class="btn btn-lg btn-light case-state-update" data-case="{{$case_state->id}}">
                                                                                                                    @if($case_state->status)
                                                                                                                        <i class="fas fa-toggle-on"></i>
                                                                                                                    @else
                                                                                                                        <i class="fas fa-toggle-off"></i>
                                                                                                                    @endif
                                                                                                                </a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        @endif
                                                                                                    @endif
                                                                                                </td>
                                                                                            </tr>
                                                                                            @if(App\Models\SubStepState::where("id", $case_state->sub_step_state_id)->first()->name_fr == "Appel : relance modèle 1")
                                                                                                <!-- <tr><td colspan="2"> -->
                                                                                                <?php $calls = App\Models\HistoCall::where("customer_id", $customer_case->customer->id)->get(); ?>
                                                                                                <tr class="calls">
                                                                                                    <th>#</th>
                                                                                                    <th>Description</th>
                                                                                                    <th>Duration</th>
                                                                                                </tr>
                                                                                                @if(count($calls))
                                                                                                    @foreach($calls as $ind => $call)
                                                                                                    <tr class="calls">
                                                                                                        <td>Call {{$ind + 1}}:</td>
                                                                                                        <td>
                                                                                                            <div class="text-wrap text-break" style="width:85%;">
                                                                                                            {{ $call->call_descr }}
                                                                                                            </div>
                                                                                                        </td>
                                                                                                        <td>{{ $call->duration }}</td>
                                                                                                    </tr>
                                                                                                    @endforeach
                                                                                                @endif
                                                                                                <!-- </td></tr> -->
                                                                                            @endif
                                                                                            <!-- @if(App\Models\SubStepState::where("id", $case_state->sub_step_state_id)->first()->name_fr == "Appel : relance modèle 1")
                                                                                                <?php $calls = App\Models\HistoCall::where("customer_id", $customer_case->customer->id)->get(); ?>
                                                                                                @if(count($calls))
                                                                                                <tr>
                                                                                                    <ul>
                                                                                                        @foreach($calls as $ind => $call)
                                                                                                        <li>
                                                                                                            <em>Call {{$ind + 1}}: </em>
                                                                                                            <p>{{ $call->call_descr }}</p>
                                                                                                        </li>
                                                                                                        @endforeach
                                                                                                    </ul>
                                                                                                </tr>
                                                                                                @endif
                                                                                            @endif -->
                                                                                        @endforeach
                                                                                        </table>
                                                                                    @endif
                                                                                    @php
                                                                                        $docs = $substep->docs()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot('order')->orderBy('order', 'asc')->get();
                                                                                    @endphp
                                                                                    @if(count($docs))
                                                                                        <table width="100%">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th></th>
                                                                                                    <th></th>
                                                                                                    <th></th>
                                                                                                    <th></th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                        @foreach($docs as $doc)
                                                                                            <tr>
                                                                                                <th class="row">
                                                                                                    <div class="col-xl-8">
                                                                                                        {{ $doc->name_fr }}
                                                                                                    </div>
                                                                                                    <div class="col-xl-4 text-center">
                                                                                                    <?php $media = $customer_case->customer->getFirstMedia($doc->collection_name); ?>
                                                                                                    @if( ($substep->id == 6 OR $substep->id == 7) && empty($media) )
                                                                                                        <a class="btn btn-light case-state-icon" href="javascript:void(0)">
                                                                                                            <i class="fa fa-sync fa-spin text-warning"></i>
                                                                                                        </a>
                                                                                                    @else
                                                                                                        @if($doc->is_link)
                                                                                                            @include('cases.steps.partials.link', ['docURL' => $doc->url, 'formId' => Str::random(10), 'documentLink' => $customer_case->customer->requestedDocuments()->wherePivot('program_step_id', '=', $step->id)->wherePivot('document_type_id', '=', $doc->id)->wherePivot('link', '=', 1)->get()])
                                                                                                        @else
                                                                                                            @include('cases.steps.partials.document', ['document' => $doc->collection_name_fr, 'collectionName' => $doc->collection_name, 'media' => $media])
                                                                                                        @endif
                                                                                                    @endif
                                                                                                    </div>
                                                                                                </th>
                                                                                            </tr>
                                                                                        @endforeach
                                                                                        </table>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </li>
                                                        @endif
                                                    @endforeach
                                                    @endif
                                                </ul>
                                            </div>
                                            <div class="float-right">
                                                <form action="{{ route('case-update', $customer_case->id) }}" method="POST">
                                                    @method('PUT')
                                                    @csrf
                                                    <input type="hidden" name="step" value="{{$customer_case->program_step_id + 1}}">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary btn-block">Next Step</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                    <!-- End Collapse -->
                                @endforeach
                                </tbody>
                            </table>
                        @endforeach


                    </div>
                </div>

            </div>

            <div class="col-xl-4">
                @administration
                <div class="block block-rounded js-ecom-div-nav d-none d-xl-block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            Etat Client : {{ $customer_case->step->step_label }}
                        </h3>
                    </div>
                    <div class="block-content">
                        <form action="{{ route('case-update', $customer_case->id) }}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label class="sr-only" for="step">Etape</label>
                                {{Form::select('step', $customer_case->program->steps()->pluck('step_label', 'program_steps.id'), old('step', $customer_case->program_step_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('step') ? ' is-invalid' : (isset($errors) && $errors->has('step') ? 'true' : 'false')), 'aria-describedby' => 'step-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner étape...'])}}
                                <div id="step-error" class="invalid-feedback">{{ isset($errors) && $errors->has('step') ? $errors->first('step') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-danger btn-block">Enregistrer</button>
                            </div>
                        </form>

                    </div>
                </div>
                @endadministration

                <div class="block block-rounded">
                    <div class="block-header block-header-default text-center">
                        <h3 class="block-title">A propos du client</h3>
                    </div>
                    <div class="block-content">
                        <table class="table table-striped table-borderless font-size-sm">
                            <tbody>

                            <?php
                                {
                                    $customer = $customer_case->customer()->first();
                                    $agent = $customer_case->user()->first();
                                    $program = $customer_case->program()->first();
                                    $step = $customer_case->step()->first();
                                    $caseDate = $customer_case->created_at;


                                    // Note, this gives you a timestamp, i.e. seconds since the Epoch.
                                    $caseTime = strtotime($caseDate);

                                    // This difference is in seconds.
                                    $difference = time() - $caseTime;
                            ?>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-clock mr-1"></i> {{ round($difference / 60) }} Minutes
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-calendar mr-1"></i> <strong>le</strong> {{ $caseDate->format('d M Y') }}
                                    <!-- 13 Juin 2021 -->
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-user mr-1"></i> <strong>Conseiller</strong>  <em>{{ $agent->name }}</em>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-user mr-1"></i> <strong>Client</strong>  <em>{{ $customer->firstname }} {{ $customer->lastname }}</em>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-socks mr-1"></i> <strong>Canal</strong>  <em>{{ ($customer->campaign) ? $customer->campaign : "N/A" }}</em>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-fw fa-tags mr-1"></i>
                                    <a class="badge badge-primary" href="javascript:void(0)">{{ $program->name }}</a>
                                    <a class="badge badge-primary text-wrap text-break" style="width: 30%;" href="javascript:void(0)">{{ $step->phase()->first()->phase_label_fr }}</a>
                                    <a class="badge badge-primary text-wrap text-break" style="width: 50%;" href="javascript:void(0)">{{ $step->step_label }}</a>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>



                <!-- <div class="block block-rounded block-link-shadow bg-white">
                    <div class="block-header block-header-default text-center">
                        <h3 class="block-title">Discussion</h3>
                    </div>
                    <div class="block-content block-content-full text-center">
                        <div class="form-group">
                            <textarea name="comment" class="form-control form-control-alt" placeholder="Ecrire un commentaire.."></textarea>
                        </div>
                    </div>
                </div> -->
                <!-- Start comments -->
                <div class="block block-rounded js-ecom-div-cart d-none d-xl-block" id="comments-block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            Commentaires
                        </h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" id="refresh-comment-btn">
                                <i class="si si-refresh"></i>
                            </button>
                        </div>
                    </div>

                    <div class="block-content scroll" style="height: 100%;max-height: 40em;overflow-y: scroll;">

                        <div class="row items-push">
                            <div class="col-md-12">
                                <form action="{{ route('store-comment', $customer_case) }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <textarea id="comment" name="comment" class="form-control form-control-alt" placeholder="Ecrire un commentaire.."></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-outline-danger btn-block">Enregistrer</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        @include('cases.steps.partials.comments')

                        <div id="load-old-comments-wrapper" class="media-body" style="display: none;">
                            <button id="load-old-comments" data-lid="0" type="button" class="btn btn-outline-danger btn-block">Charger anciens messages</button>
                        </div>

                    </div>

                </div>
                <!-- End comments -->
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div id="main-comment-container" class="content" data-comment-id="{{ $customer_case->id }}">
        <div class=" status" style="padding-top: 0;padding-bottom: 0;">
            @if($errors->any())
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger alert-dismissable" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h3 class="alert-heading h4 my-2">Oups</h3>
                            <p class="mb-0"> {{ $errors->has('easysign') ? $errors->first('easysign') : "Veuillez renseigner correctement les informations !" }}</p>
                        </div>
                    </div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h3 class="alert-heading h4 my-2">Bravo</h3>
                                <p class="mb-0"> {{ session('status') }}</p>
                            </div>

                        </div>
                    </div>
                @endif
            @endif
        </div>

    <!--        <div class="row">
            <div class="col-md-12">
                <form name="setasign-fom" action="{{ route('setasign-test', $customer_case->customer) }}" method="POST">
                    @csrf
        <button type="submit" class="btn btn-primary btn-block">Test this one !</button>
    </form>
</div>
</div>-->

        <div class="row">
            <!-- <div class="col-xl-4 order-xl-1">
                <div class="block block-rounded js-ecom-div-nav d-none d-xl-block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            Etat Dossier : {{ $customer_case->step->step_label }}
                        </h3>
                    </div>
                    <div class="block-content">

                        <form action="{{ route('case-update', $customer_case->id) }}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label class="sr-only" for="step">Etape</label>
                                {{Form::select('step', $customer_case->program->steps()->pluck('step_label', 'program_steps.id'), old('step', $customer_case->program_step_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('step') ? ' is-invalid' : (isset($errors) && $errors->has('step') ? 'true' : 'false')), 'aria-describedby' => 'step-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner étape...'])}}
                                <div id="step-error" class="invalid-feedback">{{ isset($errors) && $errors->has('step') ? $errors->first('step') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-danger btn-block">Enregistrer</button>
                            </div>
                        </form>

                    </div>
                </div>
                <div class="block block-rounded js-ecom-div-cart d-none d-xl-block" id="comments-block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            Commentaires
                        </h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" id="refresh-comment-btn">
                                <i class="si si-refresh"></i>
                            </button>
                        </div>
                    </div>

                    <div class="block-content scroll" style="height: 100%;max-height: 40em;overflow-y: scroll;">

                        <div class="row items-push">
                            <div class="col-md-12">
                                <form action="{{ route('store-comment', $customer_case) }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <textarea id="comment" name="comment" class="form-control form-control-alt" placeholder="Ecrire un commentaire.."></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-outline-danger btn-block">Enregistrer</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        @include('cases.steps.partials.comments')

                        <div id="load-old-comments-wrapper" class="media-body" style="display: none;">
                            <button id="load-old-comments" data-lid="0" type="button" class="btn btn-outline-danger btn-block">Charger anciens messages</button>
                        </div>

                    </div>

                </div>
            </div> -->
            <!-- <div class="col-xl-8 order-xl-0">

                @foreach($groupedResultsArray as $phase)
                    <div class="block block-rounded block-themed">
                        <div class="block-header bg-city">
                            <h3 class="block-title">{{ $phase[0]->phase->phase_label_fr }}</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-arrow-down"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            @foreach($phase as $step)
                                <div class="block block-rounded {{ $customer_case->program_step_id != $step->id ? "block-mode-hidden" : "" }}">
                                    <div class="block-header {{ $customer_case->program_step_id == $step->id ? "block-header-default" : "bg-flat-dark-op" }}">
                                        <h3 class="block-title">{{ $step->step_label }}</h3>
                                        <div class="block-options">

                                            {{--@if( $loop->first )
                                                <a href="{{ route('customer-edit', $customer_case->customer) }}" target="_self" class="btn btn-sm btn-outline-danger mr-1 mb-3">
                                                    <i class="fa fa-fw fa-user-edit mr-1"></i> Modifier informations client
                                                </a>
                                            @endif--}}

                                            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle">
                                                <i class="si si-arrow-down"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="block-content">
                                        @php
                                            $substates = $step->substates()->wherePivot('program_id', '=', $customer_case->program_id)->get();
                                        @endphp
                                        @if(count($substates))
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-bordered table-vcenter">
                                                        <tbody>
                                                            @foreach($substates as $stat)
                                                            <?php
                                                                $case_state = App\Models\CaseState::where("sub_step_state_id", $stat->id)
                                                                                                ->where("customer_case_id", $customer_case->id)
                                                                                                ->first();
                                                                //print_r($case_state);
                                                            ?>
                                                            <tr>
                                                                <th scope="row">
                                                                    <div class="pl-3">
                                                                        <a class="btn btn-sm btn-light case-state-icon" href="javascript:void(0)">
                                                                            @if($case_state->status)
                                                                                <i class="fa fa-check text-success mr-4"></i>
                                                                            @else
                                                                                <i class="fa fa-sync fa-spin text-warning mr-4"></i>
                                                                            @endif
                                                                            <?php echo $stat->name_fr; ?>
                                                                        </a>
                                                                    </div>
                                                                </th>
                                                                <td class="text-center">
                                                                    <div class="d-flex justify-content-center action">
                                                                        <div class="btn-group" style="margin:5px">
                                                                            <a class="btn btn-lg btn-light case-state-update" data-case="{{$case_state->id}}">
                                                                                @if($case_state->status)
                                                                                    <i class="fas fa-toggle-on"></i>
                                                                                @else
                                                                                    <i class="fas fa-toggle-off"></i>
                                                                                @endif
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endif
                                        @if(count($step->states))
                                            <div class="row items-push">
                                                <div class="col-md-6">
                                                    <form action="{{ route('step-state-update', ['customer_case' => $customer_case, 'program_step' => $step]) }}" method="POST">
                                                        @method('PUT')
                                                        @csrf
                                                        <div class="form-group">
                                                            {{Form::select('state_' . $step->id, $step->states->pluck('select_box_label_fr', 'id'), old('state_' . $step->id, (count($customer_case->states()->wherePivot('program_step_id', '=', $step->id)->get()) ? $customer_case->states()->wherePivot('program_step_id', '=', $step->id)->get()[0]->id : null) ), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('state_' . $step->id) ? ' is-invalid' : (isset($errors) && $errors->has('state_' . $step->id) ? 'true' : 'false')), 'aria-describedby' => 'ranking-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner état...'])}}
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-outline-danger btn-block update-state">Enregistrer</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        @endif
                                        @if(count($step->fields()->wherePivot('program_id', '=', $customer_case->program_id)->withPivot(['required', 'order'])->orderBy('order')->get()))
                                            <div class="row">
                                                <div class="col-md-12">
                                                    @include('cases.steps.partials.form-fields', ['form' => $step])
                                                </div>
                                            </div>
                                        @endif
                                        @php
                                            $sheets = $step->sheets()->wherePivot('program_id', '=', $customer_case->program_id)->get();
                                        @endphp
                                        @if(count($sheets))
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-bordered table-vcenter">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center" colspan="2" style="width: 250px;">Fiches de renseignements</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($sheets as $sheet)
                                                            <tr>
                                                                <th class="text-center" scope="row">{{ $sheet->name_fr }}</th>
                                                                <td class="text-center">
                                                                    @include('cases.steps.partials.sheet', ['informationSheet' => $sheet, 'formId' => Str::random(10)])
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endif

                                        @if($step->id == 2)
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-bordered table-vcenter">
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">
                                                                    <div class="pl-3">
                                                                        <a class="btn btn-sm btn-light" href="javascript:void(0)">
                                                                            <i class="fa fa-sync fa-spin text-warning mr-4"></i>
                                                                            Devis
                                                                        </a>
                                                                    </div>
                                                                </th>
                                                                <td class="text-center">
                                                                    <div class="d-flex justify-content-center action">
                                                                        <div class="btn-group" style="margin:5px">
                                                                            <a class="btn btn-lg btn-light" href="javascript:void(0)" id="show-services-student">
                                                                                <i class="fas fa-fw fa-plus"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endif
                                        @php
                                            $docs = $step->docs()->wherePivot('program_id', '=', $customer_case->program_id)->get();
                                        @endphp
                                        @if(count($docs))
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-bordered table-vcenter">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center" colspan="2" style="width: 250px;">Document(s)</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($docs as $doc)
                                                            <tr>
                                                                <th class="text-center" scope="row">{{ $doc->name_fr }}</th>
                                                                <td class="text-center">
                                                                    @if($doc->is_link)
                                                                        @include('cases.steps.partials.link', ['docURL' => $doc->url, 'formId' => Str::random(10), 'documentLink' => $customer_case->customer->requestedDocuments()->wherePivot('program_step_id', '=', $step->id)->wherePivot('document_type_id', '=', $doc->id)->wherePivot('link', '=', 1)->get()])
                                                                    @else
                                                                        @include('cases.steps.partials.document', ['document' => $doc->collection_name_fr, 'collectionName' => $doc->collection_name, 'media' => $customer_case->customer->getFirstMedia($doc->collection_name)])
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach

            </div> -->
        </div>


    </div>

    <!-- histocall Customer Add Modal -->
    <div class="modal" id="modal-histocall-customer-add" tabindex="-1" role="dialog" aria-labelledby="modal-histocall-customer-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-histocall-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter appel client</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form histocall customer Add -->
                                <form action="" method="POST" onsubmit="return false;" id="histocall-add-customer-form">
                                    <!-- <div class="form-group">
                                        <label for="add-form-customer_id">Client</label>
                                        <select class="form-control form-control-alt" id="add-form-customer_id" name="add-form-customer_id" aria-describedby="add-form-customer_id-error" aria-invalid="false">
                                            <option value="">Sélectionnez un client</option>
                                            @foreach(\App\Models\Customer::all() as $customer)
                                                <option value="{{ $customer->id }}">{{ $customer->firstname }} {{ $customer->lastname }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-customer_id-error" class="invalid-feedback"></div>
                                    </div> -->

                                    <!-- <div class="form-group">
                                        <label for="add-form-call_dt">Date d'appel</label>
                                        <input type="date" class="form-control form-control-alt" id="add-form-call_dt" name="add-form-call_dt" placeholder="Date.." aria-describedby="add-form-call_dt-error" aria-invalid="false">
                                        <div id="add-form-call_dt-error" class="invalid-feedback"></div>
                                    </div> -->

                                    <!-- <div class="form-group">
                                        <div class="stopwatch"></div>
                                    </div> -->
                                    <input type="hidden" name="add-form-customer_id" value="{{ $customer_case->customer->id }}">
                                    <!-- <div class="form-group stopwatch">
                                    </div> -->
                                    <!-- <div class="form-group">
                                        <label for="add-form-duration">Durée d'appel</label>
                                        <input type="number" class="form-control form-control-alt" id="add-form-duration" name="add-form-duration" placeholder="Durée.." aria-describedby="add-form-duration-error" aria-invalid="false">
                                        <div id="add-form-duration-error" class="invalid-feedback"></div>
                                    </div> -->
                                    <div class="form-group text-center">
                                    <input type="hidden" name="add-form-duration" id="add-form-duration">
                                        <h1>
                                            <span id="hour">00</span> :
                                            <span id="min">00</span> :
                                            <span id="sec">00</span> :
                                            <span id="milisec">00</span>
                                        </h1>
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="button" onclick="startStop()" id="start" class="btn btn-alt-secondary mr-1 mb-3"><i class="fa fa-fw fa-play mr-1"></i> start</button>
                                        <button type="button" onclick="resetChrono()" class="btn btn-alt-secondary mr-1 mb-3 reset-chrono"><i class="fa fa-fw fa-trash-restore mr-1"></i> reset</button>
                                    </div>

                                    <div class="form-group">
                                        <!-- <label for="add-form-call_descr">Description</label> -->
                                        <textarea type="text" class="form-control form-control-alt" id="add-form-call_descr" name="add-form-call_descr" placeholder="Description.." aria-describedby="add-form-call_descr-error" aria-invalid="false"></textarea>
                                        <div id="add-form-call_descr-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-histocall-customer-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter appel</button>
                                    </div>
                                </form>
                                <!-- END Form histocall Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END histocall Add Modal -->
    <!-- END Page Content -->
    @include('billing.modals.services')
@endsection

