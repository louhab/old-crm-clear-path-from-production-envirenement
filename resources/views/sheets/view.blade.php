@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script>
        window.CLEARPATH_CLEAR_ID = {!! json_encode($sheet->id) !!};
        // console.log("window.CLEARPATH_DOCUMENT_ID", window.CLEARPATH_DOCUMENT_ID);
    </script>
    <script src="{{ asset('js/pages/sheets/view.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => $sheet->name_fr,'subTitle' => '', 'navItems' => ['Admin', 'Documents']])

    <!-- Page Content -->
    <div class="content">

        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <form id="search-form" name="search-form" action="" method="post" onsubmit="return false">
                            <div class="form-group form-row">
                                <div class="col-3 p-1">
                                    <label for="field" class="sr-only">Field</label>
                                    <input class="form-control dt_search_field" id="field" name="field" style="height:44px" placeholder="Field..">
                                </div>
                                <div class="col-3 p-1">
                                    <label for="group" class="sr-only">Groupe</label>
                                    <select class="form-control form-control-alt dt_search_field" id="group" name="group">
                                        <option value="">Sélectionnez groupe</option>
                                        <option value="n/d">n/d</option>
                                        @foreach(\App\Models\CustomerFieldGroup::all() as $group)
                                            <option value="{{ $group->id }}">{{ $group->name_fr }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-3 p-1">
                                    <label for="program" class="sr-only">Programme</label>
                                    <select class="form-control form-control-alt dt_search_field" id="program" name="program">
                                        <option value="">Sélectionnez programme</option>
                                        @foreach(\App\Models\Program::all() as $program)
                                            <option value="{{ $program->id }}">{{ $program->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-2">
                                    <button type="button" class="btn btn-alt-primary btn-block submit-search" id="search-btn"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                                </div>
                                <div class="col-1">
                                    <button type="button" class="btn btn-alt-danger btn-block clear-search" id="clear-search-btn"><i class="fa fa-fw fa-redo mr-1"></i></button>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="documents-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Fields</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary"  id="documents-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter clear pivot</button>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="documents-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END leadsforms Table -->




    </div>
    <!-- END Page Content -->

    <!-- leadsforms Lead Add Modal -->
    <div class="modal" id="modal-documents-add" tabindex="-1" role="dialog" aria-labelledby="modal-documents-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-documents-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter clear pivot</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form documents lead Add -->
                                <form action="" method="POST" onsubmit="return false;" id="documents-add-form">
                                    <div class="form-group">
                                        <label for="add-form-field">Field</label>
                                        <select class="form-control form-control-alt" id="add-form-field" name="add-form-field" aria-describedby="add-form-field-error" aria-invalid="false">
                                            <option value="">Sélectionnez field</option>
                                            @foreach(\App\Models\CustomerField::all() as $field)
                                                <option value="{{ $field->id }}">{{ $field->field_name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-field-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-program">Programme</label>
                                        <select class="form-control form-control-alt" id="add-form-program" name="add-form-program" aria-describedby="add-form-program-error" aria-invalid="false">
                                            <option value="">Sélectionnez programme</option>
                                            @foreach(\App\Models\Program::all() as $program)
                                                <option value="{{ $program->id }}">{{ $program->name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-program-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-order">Order :</label>
                                        <input type="number" class="form-control form-control-alt" id="add-form-order" name="add-form-order" aria-describedby="add-form-order-error" aria-invalid="false">
                                        <div id="add-form-order-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-documents-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter document</button>
                                    </div>
                                </form>
                                <!-- END Form documents Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Clear Add Modal -->
    <!-- Clear Lead Edit Modal -->
    <div class="modal" id="modal-document-edit" tabindex="-1" role="dialog" aria-labelledby="modal-document-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-document-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Editer field</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form leadsforms lead edit -->
                                <form action="" method="POST" onsubmit="return false;" id="documents-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-field">Field</label>
                                        <select class="form-control form-control-alt" id="edit-form-field" name="edit-form-field" aria-describedby="edit-form-field-error" aria-invalid="false">
                                            <option value="">Sélectionnez field</option>
                                            @foreach(\App\Models\CustomerField::all() as $field)
                                                <option value="{{ $field->id }}">{{ $field->field_name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-field-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-program">Programme</label>
                                        <select class="form-control form-control-alt" id="edit-form-program" name="edit-form-program" aria-describedby="edit-form-program-error" aria-invalid="false">
                                            <option value="">Sélectionnez programme</option>
                                            @foreach(\App\Models\Program::all() as $program)
                                                <option value="{{ $program->id }}">{{ $program->name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-program-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-order">Order :</label>
                                        <input type="number" class="form-control form-control-alt" id="edit-form-order" name="edit-form-order" aria-describedby="edit-form-order-error" aria-invalid="false">
                                        <div id="edit-form-order-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="edit-document-btn"><i class="fa fa-fw fa-plus mr-1"></i>Editer document</button>
                                    </div>
                                </form>
                                <!-- END Form leadsforms Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Clear Edit Modal -->

@endsection
