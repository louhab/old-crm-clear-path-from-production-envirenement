<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
    $role = \App\Models\Role::select('role_name')->where('id', $user->role_id)->first();
    // dd($role->role_name);
?>
@extends('layouts.backend')
@section('css_after')
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    @livewireStyles
    <style>
        .clock-wrapper {
            font-weight: 800;
            font-size: 3.25rem;
        }
        .custom_table tbody {
            display: block;
            max-height: 420px;
            overflow: auto;
        }
        .custom_table thead, .custom_table tbody tr {
            display: table;
            width: 100%;
            table-layout: fixed;
        }
    </style>
@endsection
@section('js_after')
    <!-- <script src="{{ asset('js/plugins/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script> -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    @livewireScripts
    <!-- Page JS Code -->
    <script>
        if ( $('#phone').length ) {
            window.setIntlTel("phone");
        }
        // father_phone
        if ( $('#father_phone').length ) {
            window.setIntlTel("father_phone");
        }
        if ( $('#mother_phone').length ) {
            window.setIntlTel("mother_phone");
        }
        $("#toggle-password").on('click', function(){
            //
            let pass = $('input[name="password"]');
            if(pass.attr('type') == 'password') {
                pass.attr('type', 'text')
            } else if(pass.attr('type') == 'text') {
                pass.attr('type', 'password')
            }
            //console.log(pass.attr('type'));
        });
        $("[name='tags[]']").select2();
    </script>
    <script src="{{ asset('js/stopwatch.js') }}"></script>
    <script src="{{ asset('js/pages/leads/view.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => $role->role_name,'subTitle' => $user->name , 'navItems' => ['Admin', 'Production View']])

    <div class="content">
        <div class="row">
            <div class="col-xl">
                @livewire('block.user-info', ['user' => $user])
                {{-- @livewire('block.comment-list', ['user' => $user]) --}}
                @livewire('block.meet-list', ['user' => $user])
                @livewire('block.advisor-stats', ['user' => $user])
            </div>
        </div>
    </div>
    @include('billing.modals.services')
@endsection

