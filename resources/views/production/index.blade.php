<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
@extends('layouts.backend')
@section('css_after')
    <!-- Styles -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        .select2-selection__choice__remove {
            color: #fff !important;
        }
        .select2-search.select2-search--inline, .select2-search__field {
            width: 200px !important;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pace-js@latest/pace-theme-default.min.css">
    <style>
        .pace {
            -webkit-pointer-events: none;
            pointer-events: none;

            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;

            z-index: 2000;
            position: fixed;
            height: 90px;
            width: 90px;
            margin: auto;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }

        .pace.pace-inactive .pace-activity {
            display: none;
        }

        .pace .pace-activity {
            position: fixed;
            z-index: 2000;
            display: block;
            position: absolute;
            left: -30px;
            top: -30px;
            height: 90px;
            width: 90px;
            display: block;
            border-width: 30px;
            border-style: double;
            border-color: #d52b12 transparent transparent;
            border-radius: 50%;

            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;

            -webkit-animation: spin 1s linear infinite;
            -moz-animation: spin 1s linear infinite;
            -o-animation: spin 1s linear infinite;
            animation: spin 1s linear infinite;
        }

        .pace .pace-activity:before {
            content: ' ';
            position: absolute;
            top: 10px;
            left: 10px;
            height: 50px;
            width: 50px;
            display: block;
            border-width: 10px;
            border-style: solid;
            border-color: #d52b12 transparent transparent;
            border-radius: 50%;

            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
        }

        @-webkit-keyframes spin {
            100% { -webkit-transform: rotate(359deg); }
        }

        @-moz-keyframes spin {
            100% { -moz-transform: rotate(359deg); }
        }

        @-o-keyframes spin {
            100% { -moz-transform: rotate(359deg); }
        }

        @keyframes spin {
            100% {  transform: rotate(359deg); }
        }
    </style>
    @livewireStyles
@endsection
@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/stopwatch.js') }}"></script>
    <script>
        window.userRole = {!! auth()->user()->toJson() !!};
        if (window.userRole.role_id == 1 || window.userRole.role_id == 2 || window.userRole.role_id == 10) {
            $('input[name="daterange"]').daterangepicker({
                opens: 'left',
                locale: {
                    cancelLabel: 'Clear'
                }
            }, function(start, end, label) {
                $('input[name="date_start"]').val(start.format('YYYY-MM-DD'));
                $('input[name="date_end"]').val(end.format('YYYY-MM-DD'));
                $('#leads-dt').DataTable().ajax.reload(null, false);
            });
        } else {
            $('input[name="daterange"]').daterangepicker({
                autoUpdateInput: false,
                opens: 'left',
                locale: {
                    cancelLabel: 'Clear'
                }
            }, function(start, end, label) {
                $('input[name="date_start"]').val(start.format('YYYY-MM-DD'));
                $('input[name="date_end"]').val(end.format('YYYY-MM-DD'));
                $('#leads-dt').DataTable().ajax.reload(null, false);
            });
        }

        const lang = $('input[name="lang"]').val();

        $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            $('input[name="date_start"]').val('');
            $('input[name="date_end"]').val('');
        });

        $("[name='program']").select2({
            placeholder: lang === "fr" ? "Programmes ..." : "Programs ...",
            width:"100%",
            allowClear: true
        });

        $("[name='conseiller']").select2({
            placeholder: lang === "fr" ? "Conseiller ..." : "Adviser ...",
            width:"100%",
            allowClear: true
        });
        $("[name='campaign']").select2({
            placeholder: "Canal ...",
            width:"100%",
            allowClear: true
        }).on("select2:unselect", function (e) {
            Livewire.emit("campaignChanged", $("[name='campaign']").select2("val"))
        }).on("select2:select", function (e) {
            Livewire.emit("campaignChanged", $("[name='campaign']").select2("val"))
        });

    </script>
    <script src="{{ asset('js/pages/production/list.js') }}"></script>
    @livewireScripts
    <script>
    </script>
@endsection
@section('content')
    @include('layouts.partials.hero', ['title' => 'Production','subTitle' => 'conseiller', 'navItems' => ['Admin', 'Production']])
    <!-- Page Content -->
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les informations !</a>
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        <!-- leads Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>{{ __("lang.lead_filter_title") }}</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <form id="search-form" name="search-form" action="" method="post" onsubmit="return false">
                            {{-- language input --}}
                            <input type="hidden" name="lang" value="{{ $lang }}">
                            @csrf
                            <div class="form-group form-row">
                                <div class="col-md-12">
                                    <div class="row p-2">
                                        <div class="col-3 p-1">
                                            <label for="program" class="sr-only">{{ __("lang.lead_filter_program") }}</label>
                                            {{Form::select('program', \App\Models\Program::pluck($lang === "fr" ? "name" : "labelEng", 'id'), null, ['class' => 'form-control form-control-alt dt_search_field',  "data-control" => "select2", 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '','style' => 'height: 42px;border: 1px solid #aaa;'])}}
                                        </div>
                                        <div class="col-3 p-1">
                                            <label for="conseiller" class="sr-only">{{ __("lang.lead_filter_Adviser") }}</label>
                                            <?php
                                            $list_roles = [3];
                                            if (\Illuminate\Support\Facades\Auth::user()->isAdmin() || \Illuminate\Support\Facades\Auth::user()->isVerificateur())
                                                array_push($list_roles, 2);
                                            $queryConseiller = \App\Models\User::where("status", 1)
                                                ->where(function ($query) use ($list_roles) {
                                                    $query->where(function($query) use ($list_roles) {
                                                        $query->whereIn("role_id", $list_roles);
                                                    });
                                                    if (\Illuminate\Support\Facades\Auth::user()->isManager()){
                                                        $query->orWhere(function($query) {
                                                            $query->where("id", \Illuminate\Support\Facades\Auth::id());
                                                        });
                                                    }
                                                });
                                            $queryConseillerArray = array("n/d" => __("lang.lead_filter_status_unaffected")) + $queryConseiller->pluck('name', 'id')->toArray();
                                            ?>
                                            {{Form::select('conseiller', $queryConseillerArray, null,  ['class' => 'form-control form-control-alt dt_search_field',  "data-control" => "select2", 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '' ,'style' => 'height: 42px;border: 1px solid #aaa;'])}}
                                        </div>
                                        <div class="col-3 p-1">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text input-group-text-alt">
                                                            <i class="far fa-calendar-alt"></i>
                                                        </span>
                                                    </div>
                                                    @php
                                                        $dateMonth = date('Y-m-01');
                                                        $firstDay = \Carbon\Carbon::parse($dateMonth)->format('m/d/Y');
                                                        $lastDay = \Carbon\Carbon::today()->format('m/d/Y');
                                                        $current_month = $firstDay . " - " . $lastDay;
                                                    @endphp
                                                    @if(auth()->user()->isAdmin() || auth()->user()->isVerificateur() || auth()->user()->isManager())
                                                        <input type="text" class="form-control form-control" id="daterange" name="daterange" value="{{ $current_month }}">
                                                        <input type="hidden" name="date_end" class="dt_search_field" value="{{ $lastDay }}" />
                                                        <input type="hidden" name="date_start" class="dt_search_field" value="{{ $firstDay }}" />
                                                    @else
                                                        <input type="text" class="form-control form-control" id="daterange" name="daterange" value="">
                                                        <input type="hidden" name="date_end" class="dt_search_field" value="" />
                                                        <input type="hidden" name="date_start" class="dt_search_field" value="" />
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-5 p-1 pl-4">
                                                    <button type="button" class="btn btn-alt-primary btn-block submit-search" id="search-btn"><i class="fa fa-fw fa-search mr-1"></i>{{ __("lang.lead_filter_search_btn") }}</button>
                                                </div>
                                                <div class="col-3 p-1">
                                                    <button type="button" class="btn btn-alt-danger btn-block clear-search" id="clear-search-btn"><i class="fa fa-fw fa-redo mr-1"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @livewire('block.production-stats')
        <div class="block block-rounded" id="conseillers-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Liste des Conseillers</h3>
                </div>
                {{-- <h4>h4 title</h4> --}}
                <div class="table-responsive">
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                    <table class="table table-bordered table-striped table-vcenter" id="conseillers-dt">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
