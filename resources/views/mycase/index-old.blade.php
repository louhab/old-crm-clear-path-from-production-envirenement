@extends('layouts.customer')

@section('js_after')
    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/mycase.js') }}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/slick-carousel/slick.min.js') }}"></script>

    <!-- Page JS Helpers (Slick Slider Plugin) -->
    <script>
        // jQuery(function(){ One.helpers('slick'); });
        $(document).ready(function() {
            $('.js-slider').slick({
                dots: false,
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: $('.prev'),
                nextArrow: $('.next'),
                //arrows: true
                //prevArrow: '<button type="button" class="btn btn-sm btn-alt-primary">Previous</button>'
            });
        });

        // $("#nav-block-content").children().each(function(){
        //     if (!$(this).hasClass('active')) {
        //         console.log($(this).attr('class'));
        //         $(this).hide();
        //     }
        // });
    </script>
@endsection
@section('css_after')
    <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick-theme.css') }}">
@endsection

@section('content')
    @include('layouts.partials.hero', [
        'title' => 'Dossier',
        'subTitle' => 'mon dossier',
        'navItems' => ['Dossier', 'mon dossier'],
    ])

    <!-- Page Content -->
    <div class="content">
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        @if (session('success'))
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    <div class="block block-rounded">
                        <div class="block-content">
                            <a class="img-link img-link-zoom-in img-thumb img-lightbox" href="#">
                                <img class="img-fluid" src="#" alt="Conseiller Images..">
                            </a>
                            <p>
                                Vous voilà dans votre espace personnel <br />
                                Cet espace vous est réservé pour vous apporter un service plus
                                rapide, plus fluide et plus efficace. <br />
                                Vous pouvez consulter et suivre à tout moment votre dossier
                                de candidature en ligne. <br />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <!-- New Portal -->
        <div class="row justify-content-center" id="nav-block">
            <div class="col-md-10">
                <div class="block">
                    <div class="block-content">
                        <ul class="nav nav-pills nav-justified push">
                            <li class="nav-item mr-1">
                                <a class="nav-link active" href="javascript:void(0)"
                                    data-toggle="block-documents">Documents</a>
                            </li>
                            <li class="nav-item mr-1">
                                <a class="nav-link" href="javascript:void(0)" data-toggle="block-forms">Formulaires</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="javascript:void(0)">Autres</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center" id="nav-block-content">
            <!-- <div class="col-md-4">
                        <div class="block">
                            <div class="block-content">
                                <ul class="nav nav-pills flex-column push">
                                    <li class="nav-item mb-1">
                                        <a class="nav-link active" href="javascript:void(0)">
                                            Documents <span class="badge badge-pill badge-secondary ml-1">3</span>
                                        </a>
                                    </li>
                                    <li class="nav-item mb-1">
                                        <a class="nav-link" href="javascript:void(0)">
                                            Formulaires <span class="badge badge-pill badge-secondary ml-1">1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="javascript:void(0)">Tools</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> -->
            <div class="col-md-10" id="block-documents">
                <div class="block">
                    <div class="block-header">
                        <!-- <div class="container">
                                </div> -->
                        <button class="btn btn-danger prev slick-arrow"> Précédent </button>
                        <button class="btn btn-danger next slick-arrow"> Suivant </button>
                    </div>
                </div>
                <div class="js-slider">
                    <div>
                        <div class="block block-rounded">
                            <div class="block-content">
                                <table class="table table-bordered table-vcenter">
                                    @php
                                        $program_step = \App\Models\ProgramStep::where('id', 2)->first();
                                    @endphp
                                    <thead>
                                        <tr>
                                            <th class="text-center text-light" style="background:#d52b26;" colspan="2">
                                                {{ $program_step->step_label }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $docs = new Illuminate\Support\Collection();
                                            foreach ($program_step->substeps()->get() as $substep) {
                                                $docs_ = $substep
                                                    ->docs()
                                                    ->wherePivot('program_id', '=', $customer_case->program_id)
                                                    ->get();
                                                if (count($docs_)) {
                                                    foreach ($docs_ as $doc) {
                                                        $docs->push($doc);
                                                    }
                                                }
                                            }
                                            //dd($docs_, $customer_case);
                                            //$docs = $program_step->docs()->wherePivot('program_id', '=', $customer_case->program_id)->get();
                                        @endphp
                                        @foreach ($docs as $doc)
                                            @if ($doc->name_fr == 'Facture' || $doc->name_fr == 'CV')
                                                @continue
                                            @endif
                                            <tr>
                                                <th class="text-center" scope="row">{{ $doc->name_fr }}</th>
                                                <td class="text-center document-td">
                                                    <?php
                                                    if ($doc->name_fr == 'Reçu') {
                                                        // $doc = \App\Models\DocumentType::where('name_fr', 'Reçu')->first();
                                                    
                                                        $payment_name = \App\Models\SubStepState::find(3)->name_fr;
                                                        $collectionName = $payment_name . $doc->collection_name;
                                                        $media = $customer_case->customer->getFirstMedia($collectionName);
                                                        if (empty($media)) {
                                                            $collectionName = $doc->collection_name;
                                                            $media = $customer_case->customer->getFirstMedia($collectionName);
                                                        }
                                                        //dd($case_state_3);
                                                    } else {
                                                        $media = $customer_case->customer->getFirstMedia($doc->collection_name);
                                                        $collectionName = $doc->collection_name;
                                                    }
                                                    ?>
                                                    @include('cases.steps.partials.document', [
                                                        'document' => $doc->collection_name_fr,
                                                        'collectionName' => $collectionName,
                                                        'media' => $media,
                                                        'step' => $program_step,
                                                    ])
                                                    @if ($doc->name_fr == 'Devis' && !$customer_case->customer->is_client)
                                                        <a class="btn btn-sm btn-light" href="javascript:void(0)"
                                                            id="enable-quote" data-url="{{ route('enable-quote') }}">
                                                            <i class="far fa-2x fa-check-circle text-success"></i>
                                                        </a>
                                                        <a class="btn btn-sm btn-light" href="javascript:void(0)">
                                                            <i class="fa fa-fw fa-times text-danger"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="block block-rounded">
                            <div class="block-content">
                                <table class="table table-bordered table-vcenter">
                                    @php
                                        $program_step = \App\Models\ProgramStep::where('id', 4)->first();
                                    @endphp
                                    <thead>
                                        <tr>
                                            <th class="text-center text-light" style="background:#d52b26;" colspan="2">
                                                {{ $program_step->step_label }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $docs = new Illuminate\Support\Collection();
                                        $substeps = $program_step->substeps()->get();
                                        $i = 0;
                                        $len = count($substeps);
                                        foreach ($substeps as $substep) {
                                            if ($i == $len - 1) {
                                                break;
                                            }
                                            $docs_ = $substep
                                                ->docs()
                                                ->wherePivot('program_id', '=', $customer_case->program_id)
                                                ->get();
                                            if (count($docs_)) {
                                                foreach ($docs_ as $doc) {
                                                    $requested[$doc->id] = ['program_step_id' => $program_step->id];
                                                    $docs->push($doc);
                                                }
                                            }
                                            $i++;
                                        }
                                        $customer_case->customer->requestedDocuments()->sync($requested);
                                        // $med1 = $customer_case->customer->getFirstMedia($docs[0]->collection_name);
                                        // $med2 = $customer_case->customer->getFirstMedia($docs->last()->collection_name);
                                        // dd($med1->custom_properties, $med2->custom_properties);
                                        // $docs = $program_step->docs()->wherePivot('program_id', '=', $customer_case->program_id)->get();
                                        // dd($program_step->substeps()->get());
                                        ?>
                                        @foreach ($docs as $doc)
                                            @if ($doc->name_fr == 'Facture')
                                                @continue
                                            @endif
                                            <tr>
                                                <th class="text-center" scope="row">{{ $doc->name_fr }}</th>
                                                <td class="text-center document-td">
                                                    @php
                                                        $mediaItem = $customer_case->customer->getFirstMedia($doc->collection_name);
                                                        // $mediaItem->setCustomProperty('visible_for_customer', 1);
                                                        // $mediaItem->save();
                                                    @endphp
                                                    @if (!empty($mediaItem))
                                                        @include('cases.steps.partials.show-file', [
                                                            'document' => $doc->collection_name_fr,
                                                            'model' => $customer_case->customer,
                                                            'collectionName' => $doc->collection_name,
                                                            'formId' => Str::random(10),
                                                            'signable' => $mediaItem->getCustomProperty(
                                                                'signable'),
                                                        ])
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10" id="block-forms" style="display:none;">
                <div class="block block-rounded">
                    <div class="block-content">
                        <table class="table table-bordered table-vcenter">
                            @php
                                $program_step = \App\Models\ProgramStep::where('id', 4)->first();
                                $filtered = $program_step
                                    ->substeps()
                                    ->get()
                                    ->filter(function ($substep) {
                                        return str_starts_with($substep->step_label, 'Clear');
                                    });
                                //dd($filtered);
                            @endphp
                            <thead>
                                <tr>
                                    @if ($customer_case->program_id == 4)
                                        <th class="text-center text-light" style="background:#d52b26;" colspan="2">
                                            Préparation de la demande d’admission (Rencontre 3)</th>
                                    @else
                                        <th class="text-center text-light" style="background:#d52b26;" colspan="2">
                                            Préparation du profil
                                            {{ \App\Models\Program::find($customer_case->program_id)->name }} (Rencontre 3)
                                        </th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @php $mediaSheet = \App\Models\MediaSheet::where('name_fr', 'Clear 1')->first(); @endphp
                                    <th class="text-center" scope="row">{{ $mediaSheet->name_fr }}</th>
                                    <td class="text-center document-td">
                                        <a class="btn btn-sm btn-outline-secondary"
                                            href="{{ route('customer-edit-media-sheet', ['customer_case' => $customer_case, 'media_sheet' => $mediaSheet]) }}"
                                            target="_blank">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    @php $mediaSheet = \App\Models\MediaSheet::where('name_fr', 'Clear 2')->first(); @endphp
                                    <th class="text-center" scope="row">{{ $mediaSheet->name_fr }}</th>
                                    <td class="text-center document-td">
                                        <a class="btn btn-sm btn-outline-secondary"
                                            href="{{ route('customer-edit-media-sheet', ['customer_case' => $customer_case, 'media_sheet' => $mediaSheet]) }}"
                                            target="_blank">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
