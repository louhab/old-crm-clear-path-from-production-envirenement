<?php
$lang = session()->get('locale');
?>

@extends('layouts.customer')

@section('js_after')
    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/mycase.js') }}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/slick-carousel/slick.min.js') }}"></script>
    @livewireScripts

    <!-- Page JS Helpers (Slick Slider Plugin) -->
    <script></script>
@endsection
@section('css_after')
    <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick-theme.css') }}">
    @livewireStyles
@endsection
<?php
$programm = \App\Models\Program::find($customer_case->program_id);
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
$subtitle = $lang === 'fr' ? $programm->name : $programm->labelEng;
?>

@section('content')
    @include('layouts.partials.hero', [
        'title' => 'Dossier',
        'subTitle' => $subtitle,
        'navItems' => ['Dossier', __('lang.customer_portal_my_dossier')],
    ])

    <!-- Page Content -->
    <div class="content">
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        @if (session('success'))
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    <div class="block block-rounded">
                        <div class="block-content">
                            @if ($lang === 'fr')
                                <p>
                                    Bonjour <b>{{ $customer_case->customer->firstname }},</b><br />
                                    Vous voilà dans votre espace personnel <br />
                                    Cet espace vous est réservé pour vous apporter un service plus
                                    rapide, plus fluide et plus efficace. <br />
                                    Vous pouvez consulter et suivre à tout moment votre dossier
                                    de candidature en ligne. <br />
                                </p>
                            @else
                                <p>
                                    Hello <b>{{ $customer_case->customer->firstname }},</b><br />
                                    This is your personal space.<br />
                                    This space is reserved to provide you with faster, smoother and more efficient
                                    service.<br />
                                    From here, you can consult and check the status of your application file online at any
                                    time. <br />
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <input type="hidden" name="lang" value="{{ $lang }}">
        <div class="row justify-content-center" id="nav-block-content">
            <div class="col-md-10" id="block-documents">

                @livewire('block.customer.meet', ['customer_case' => $customer_case, 'meet' => 'R1', 'lang' => $lang])
                <!-- count($customer_case->customer->mediaSheets()->wherePivot('media_sheet_id', '=', $mediaSheet->id)->wherePivot('can_edit', '=', true)->get())-->
                @php
                    $payment2 = App\Models\CaseState::where('sub_step_state_id', 5)
                        ->where('customer_case_id', $customer_case->id)
                        ->first();
                @endphp

                @if ($payment2->status && $payment2->payment_method)
                    @livewire('block.customer.meet', ['customer_case' => $customer_case, 'meet' => 'R2', 'lang' => $lang])
                @endif

                @php
                    $payment3 = App\Models\CaseState::where('sub_step_state_id', 10)
                        ->where('customer_case_id', $customer_case->id)
                        ->first();
                @endphp
                @if ($payment3->status && $payment3->payment_method)
                    @livewire('block.customer.meet', ['customer_case' => $customer_case, 'meet' => 'R3', 'lang' => $lang])
                @endif
                @php
                    $payment4 = App\Models\CaseState::where('sub_step_state_id', 20)
                        ->where('customer_case_id', $customer_case->id)
                        ->first();
                @endphp
                @if ($payment4->status && $payment4->payment_method)
                    @livewire('block.customer.meet', ['customer_case' => $customer_case, 'meet' => 'R4', 'lang' => $lang])
                @endif

                @if ($payment4->status && $payment4->payment_method)
                    @livewire('block.customer.meet', ['customer_case' => $customer_case, 'meet' => 'R5', 'lang' => $lang])
                @endif

                @if ($payment4->status && $payment4->payment_method)
                    @livewire('block.customer.meet', ['customer_case' => $customer_case, 'meet' => 'R6', 'lang' => $lang])
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->

@endsection
