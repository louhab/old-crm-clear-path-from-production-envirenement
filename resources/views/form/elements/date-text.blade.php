<?php
$old_value = $customer_case->customer->$field_name;
// customer_visa_request
if (!is_null($sheet_id) && $sheet_id == 1) {
    $old_value = \App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first()->$field_name;
}
if (isset($customer_visa_request)) {
    $old_value = $customer_visa_request->$field_name;
    $field_name = 'dyn-' . $customer_field->field_name . '_' . $customer_visa_request->id;
    // dd($field_name, $old_value);
}
// dd($field_name, $old_value);
?>
<div class="form-group">
    <label for="{{ $field_name }}">{{ __('lang.' . $customer_field->field_name) }}</label>
    @if ($field->pivot->required)
    @endif
    <input type="text"
        class="form-control form-control-alt{{ isset($errors) && $errors->has($field_name) ? ' is-invalid' : '' }}"
        id="{{ $field_name }}" name="{{ $field_name }}" value="{{ old($customer_field->field_name, $old_value) }}"
        placeholder="{{ __('lang.' . $customer_field->field_name . '_placeholder') }}"
        aria-describedby="{{ $customer_field->field_name }}-error"
        aria-invalid="{{ isset($errors) && $errors->has($field_name) ? 'true' : 'false' }}">
    <div id="{{ $field_name }}-error" class="invalid-feedback">
        {{ isset($errors) && $errors->has($field_name) ? $errors->first($field_name) : '' }}</div>
</div>
