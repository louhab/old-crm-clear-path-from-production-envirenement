<div class="form-group">
    <label for="{{ $customer_field->field_name }}">{{ __('lang.' . $customer_field->field_name)}}</label>
    @if($field->pivot->required)
        <span class="text-danger">*</span>
    @endif
    @if(!is_null($sheet_id) && $sheet_id == 1)
        @php $old_v = \App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first()->$field_name; @endphp
    @else
        @php $old_v = $customer_case->customer->$field_name; @endphp
    @endif
    <input type="number" step="0.01" class="form-control form-control-alt{{ isset($errors) && $errors->has($customer_field->field_name) ? ' is-invalid' : '' }}" {{ $field->step ? 'step=' . $field->step  : '' }} id="{{ $customer_field->field_name }}" name="{{$customer_field->field_name}}" value="{{ old($customer_field->field_name, $old_v) }}" placeholder="0.00" aria-describedby="{{ $customer_field->field_name }}-error" aria-invalid="{{ isset($errors) && $errors->has($customer_field->field_name) ? 'true' : 'false' }}">
    <div id="{{ $customer_field->field_name }}-error" class="invalid-feedback">{{ isset($errors) && $errors->has($customer_field->field_name) ? $errors->first($customer_field->field_name) : '' }}</div>
</div>
