<?php
$old_value = $customer_case->customer->$field_name;

$multi_choice_study = ['tef_test_res'];
// customer_visa_request
if (!is_null($sheet_id) && $sheet_id == 1) {
    $old_value = \App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first()->$field_name;
}
if (isset($customer_visa_request)) {
    $old_value = $customer_visa_request->$field_name;
    $field_name = 'dyn-' . $customer_field['field_name'] . '_' . $customer_visa_request->id;
    // dd($field_name, $old_value);
}
$hidden_by_def = ['criminal_record_rep', 'health_rep', 'tef_test_res', 'rental_income'];

if (in_array($customer_field['field_name'], $multi_choice_study)) {
    switch ($customer_case->program_id) {
        case 31:
            $label = $customer_field['field_name'] . '_uk';
            break;
        case 32:
            $label = $customer_field['field_name'] . '_es';
            break;
        default:
            $label = $customer_field['field_name'] . '_ca';
    }
} else {
    $label = $customer_field['field_name'];
}

?>
<div class="form-group" @if (in_array($customer_field->field_name, $hidden_by_def) &&
        (old($customer_field->field_name, $old_value) === null && !$errors->has($customer_field->field_name))) style="display: none" @endif>
    <label for="{{ $field_name }}">{{ __('lang.' . $label) }}</label>
    @if ($field['pivot']['required'])
        <span class="text-danger">*</span>
    @endif
    <input type="text"
        class="form-control form-control-alt{{ isset($errors) && $errors->has($field_name) ? ' is-invalid' : '' }}"
        id="{{ $field_name }}" name="{{ $field_name }}" value="{{ old($customer_field['field_name'], $old_value) }}"
        placeholder="{{ __('lang.' . $label) }}.." aria-describedby="{{ $customer_field['field_name'] }}-error"
        aria-invalid="{{ isset($errors) && $errors->has($field_name) ? 'true' : 'false' }}">
    <div id="{{ $field_name }}-error" class="invalid-feedback">
        {{ isset($errors) && $errors->has($field_name) ? $errors->first($customer_field['field_name']) : '' }}</div>
</div>
