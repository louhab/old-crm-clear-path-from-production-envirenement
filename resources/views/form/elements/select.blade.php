<?php
$lang = session()->get('locale') ?? 'fr';

// New 🚀
$multi_choice_study = ['lang_level', 'annual_budget', 'tuition_fees', 'cpg_account', 'tef_test'];
$only_for_ca = ['tef_test'];
$old_value = $customer_case->customer->$field_name;
if (!is_null($sheet_id) && $sheet_id == 1) {
    $old_value = \App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first()->$field_name;
}
//dd($old_value);
if (str_ends_with($customer_field['select_model'], 'CustomerRadioField')) {
    // if ($field_name === "education_type") dd($customer_field, $lang);
    // Must applied to all elements
    $select_model_label = $lang === 'fr' ? 'value_fr' : 'value_en';
    $array = $customer_field['select_model']::where('name', '=', $customer_field['field_name'])->pluck($select_model_label, $customer_field['select_model_id']);
    $values_ = [];
    foreach ($array as $key => $value) {
        if (str_starts_with($key, $customer_field['field_name'] . '-')) {
            $key_ = explode('-', $key)[1];
            $values_[$key_] = $value;
        } else {
            $values_[$key] = $value;
        }
    }
    // if ($field_name === "education_type") dd($values_);

    if (str_starts_with($customer_case->customer->$field_name, $customer_field['field_name'] . '-')) {
        $old_value = explode('-', $customer_case->customer->$field_name)[1];
    }
} elseif ($field_name == 'lead_status') {
    $select_model_label = $lang === 'fr' ? 'status_label_fr' : 'status_label_en';
    $values_ = $customer_field['select_model']::where('visible', 1)->pluck($select_model_label, $customer_field['select_model_id']);
    if (\Illuminate\Support\Facades\Auth::user()->isAdmin() || \Illuminate\Support\Facades\Auth::user()->isVerificateur()) {
        $values_ = $customer_field['select_model']::pluck($select_model_label, $customer_field['select_model_id']);
    } elseif ($customer_case->customer->lead_status >= 4) {
        $values_->put(7, 'Admissible');
        $values_->put(8, 'Inadmissible');
    }
    //print_r($values_);
} else {
    // use switch case or the other solution after
    // for clear1004 fields
    $clear_1004_eng_fields = [
        'marital_status' => 'status_label_en',
        'spouse_cit' => 'value_en',
        'spouse_joining' => 'value_en',
        'age' => 'labelEng',
        'education_level' => 'labelEng',
        'diplomas_canadien' => 'value_en',
        'education_type' => 'value_en',
        'lang_test_expire' => 'value_en',
        'lang_test' => 'value_en',
        'other_lang_test' => 'value_en',
        'work_experience_canada' => 'labelEng',
        // "work_experience_abroad" => "labelEng",
        'certificate_of_competence' => 'value_en',
        'valid_job' => 'value_en',
        'job_kind' => 'value_en',
        'certificate_of_designation' => 'value_en',
        'sibling_canada' => 'value_en',
        'spouse_education_level' => 'labelEng',
        'spouse_work_experience_canada' => 'labelEng',
        'spouse_lang_test' => 'value_en',
    ];

    if ($lang === 'en' && str_ends_with($customer_field['select_model'], 'LevelOfStudy')) {
        $values_ = $customer_field['select_model']::pluck('level_label_en', $customer_field['select_model_id']);
    } elseif ($lang === 'en' && str_ends_with($customer_field['select_model'], 'Branch')) {
        // $select_model_label = $lang === "fr" ? "branch_label_fr" : "branch_label_en";
        $values_ = $customer_field['select_model']::pluck('branch_label_en', $customer_field['select_model_id']);
    } elseif ($lang === 'en' && str_ends_with($customer_field['select_model'], 'LanguageLevel')) {
        // no need to translate
        $values_ = $customer_field['select_model']::pluck($customer_field['select_model_label'], $customer_field['select_model_id']);
    } elseif ($lang === 'en' && str_ends_with($customer_field['select_model'], 'FrenchTest')) {
        $values_ = $customer_field['select_model']::pluck($customer_field['select_model_label'], $customer_field['select_model_id']);
        // todo
    } elseif ($lang === 'en' && str_ends_with($customer_field['select_model'], 'EnglishTest')) {
        $values_ = $customer_field['select_model']::pluck($customer_field['select_model_label'], $customer_field['select_model_id']);
        // todo
    } elseif ($lang === 'en' && str_ends_with($customer_field['select_model'], 'ProgramChoice')) {
        $values_ = $customer_field['select_model']::pluck('labelEng', $customer_field['select_model_id']);
    } elseif ($lang === 'en' && str_ends_with($customer_field['select_model'], 'GuarantorType')) {
        $values_ = $customer_field['select_model']::pluck('garant_label_en', $customer_field['select_model_id']);
    } elseif ($lang === 'en' && str_ends_with($customer_field['select_model'], 'IsLanguageTestTaken')) {
        // for the new select model (is lang taken)
        $values_ = $customer_field['select_model']::pluck('label_en', $customer_field['select_model_id']);
    } elseif (str_ends_with($customer_field['select_model'], 'MonthlyIncome')) {
        $values_ = $customer_field['select_model']::pluck($customer_field['select_model_label'], $customer_field['select_model_id']);
        // no need to translate
        // but title should be translated
    } elseif (str_ends_with($customer_field['select_model'], 'AmountAvailable')) {
        $values_ = $customer_field['select_model']::pluck($customer_field['select_model_label'], $customer_field['select_model_id']);
        // no need to translate
        // but title should be translated
    } elseif ($lang === 'en' && str_ends_with($customer_field['select_model'], 'Program')) {
        $values_ = $customer_field['select_model']::pluck('labelEng', $customer_field['select_model_id']);
        // for clear 1004 fields
    } elseif ($lang === 'en' && in_array($field_name, array_keys($clear_1004_eng_fields))) {
        $values_ = $customer_field['select_model']::pluck($clear_1004_eng_fields[$field_name], $customer_field['select_model_id']);
    } else {
        $values_ = $customer_field['select_model']::pluck($customer_field['select_model_label'], $customer_field['select_model_id']);
    }
}

if ($field_name == 'campaign_id') {
    $values_ = $customer_field['select_model']::where('visible', 1)->pluck($customer_field['select_model_label'], $customer_field['select_model_id']);
}
if (isset($customer_garant)) {
    //dd();
    $old_value = $garant->$field_name;
    $field_name = 'dyn-' . $customer_field['field_name'] . '_' . $garant->id;
}
// customer_visa_request
if (isset($customer_visa_request)) {
    $old_value = $customer_visa_request->$field_name;
    $field_name = 'dyn-' . $customer_field['field_name'] . '_' . $customer_visa_request->id;
}
$class_lang = '';
if (str_ends_with($customer_field['select_model'], 'LanguageTestScore')) {
    $class_lang = 'language_test_score';
}
$hidden_by_def = ['guarantor_work_income', 'guarantor_retirement_income', 'guarantor_business_income', 'guarantor_professional_income', 'guarantor_self_employed_income', 'guarantor_rental_income', 'guarantor_investment_income', 'goods_res'];

// check here
if (in_array($customer_field['field_name'], $multi_choice_study)) {
    switch ($customer_case->program_id) {
        case 31:
            $label = $customer_field['field_name'] . '_uk';
            break;
        case 32:
            $label = $customer_field['field_name'] . '_es';
            break;
        default:
            $label = $customer_field['field_name'] . '_ca';
    }
} else {
    $label = $customer_field['field_name'];
}
$is_only_for_ca = in_array($customer_field->field_name, $only_for_ca);
?>
@if (!$is_only_for_ca || ($is_only_for_ca && $customer_case->program_id == 4))
    <div class="form-group" @if (in_array($customer_field->field_name, $hidden_by_def) &&
            old($customer_field->field_name, $old_value) === null &&
            !$errors->has($customer_field->field_name)) style="display: none" @endif>
        <label for="{{ $field_name }}">{{ __('lang.' . $label) }}</label>
        @if ($field['pivot']['required'])
            <span class="text-danger">*</span>
        @endif
        @if (!is_null($sheet_id) && $sheet_id == 7)
            <?php
            $old_value = App\Models\CustomerClear1004Fields::where('customer_id', $customer_case->customer->id)->first()->$field_name;
            if (str_starts_with($customer_case->customer->$field_name, $customer_field['field_name'] . '-')) {
                $old_value = explode('-', $customer_case->customer->$field_name)[1];
            }
            //dd($field_name, old($field_name, $old_value));
            ?>
            <div class="input-group">
        @endif
        <?php
        // 'multiple' => 'multiple', 'name' => $field_name . '[]',
        $attributes = [
            'class' => 'form-control ' . $class_lang . ' form-control-alt' . (isset($errors) && $errors->has($field_name) ? ' is-invalid' : (isset($errors) && $errors->has($field_name) ? 'true' : 'false')),
            'aria-describedby' => $field_name . '-error',
            'aria-invalid' => '',
        ];
        if (!is_null($sheet_id) && $sheet_id == 4) {
            $attributes['data-placeholder'] = __('lang.' . $customer_field['field_name']) . '..';
            $attributes['multiple'] = 'multiple';
            $attributes['name'] = $field_name . '[]';
        } elseif ($sheet_id == 7) {
            $attributes['data-old'] = $old_value;
            $attributes['placeholder'] = __('lang.' . $customer_field['field_name']) . '..';
        } else {
            // dd('ajiya', $label);
            $attributes['placeholder'] = __('lang.' . $label) . '..';
        }
        if (!is_array($values_)) {
            $values_ = $values_->toArray();
        }
        asort($values_);
        ?>
        {{ Form::select($field_name, $values_, old($field_name, $old_value), $attributes) }}
        @if (!is_null($sheet_id) && $sheet_id == 7)
            <div class="input-group-append float-right">
                <button type="button" class="btn btn-danger btn-sm remark-toggle">
                    <i class="far fa-comment-dots"></i>
                </button>
            </div>
    </div>
@endif
<div id="{{ $field_name }}-error" class="invalid-feedback">
    {{ isset($errors) && $errors->has($field_name) ? $errors->first($field_name) : '' }}</div>
</div>
@endif
