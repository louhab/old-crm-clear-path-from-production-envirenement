<div class="form-group">
    <label for="{{ $customer_field->field_name }}">{{ __('lang.' . $customer_field->field_name)}}</label>
    @if($field->pivot->required)
        <span class="text-danger">*</span>
    @endif
    <br />
    <input type="tel" class="form-control form-control-alt{{ isset($errors) && $errors->has($customer_field->field_name) ? ' is-invalid' : '' }}" id="{{ $customer_field->field_name }}" name="{{$customer_field->field_name}}" value="{{ old($customer_field->field_name, $customer_case->customer->$field_name) }}" aria-describedby="{{ $customer_field->field_name }}-error" aria-invalid="{{ isset($errors) && $errors->has($customer_field->field_name) ? 'true' : 'false' }}">
    <div id="{{ $customer_field->field_name }}-error" class="text-danger">{{ isset($errors) && $errors->has($customer_field->field_name) ? $errors->first($customer_field->field_name) : '' }}</div>
    <span id="valid-msg" class="text-success" style="display:none;">✓ Valid</span>
</div>

