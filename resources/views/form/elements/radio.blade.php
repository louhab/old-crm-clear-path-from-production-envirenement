<div class="form-group">
    <label for="{{ $customer_field->field_name }}">{{ __('lang.' . $customer_field->field_name)}}</label>
    @if($field->pivot->required)
        <span class="text-danger">*</span>
    @endif
        <?php 
            $radio_list = $customer_field->select_model::where("name", "=", $customer_field->field_name)->pluck($customer_field->select_model_label, $customer_field->select_model_id);
            //print_r($radio_list);
            //Form::radio('' . $customer_field->field_name, );
            //print_r($radio_list);
            foreach ($radio_list as $key => $input) {
                # code...
                //echo Form::radio('' . $customer_field->field_name, $input[1]);
                
        ?>
            <div class="form-check">
                <input class="form-check-input" type="radio" id="{{ $customer_field->field_name . '-' . $key}}" name="{{ $customer_field->field_name }}" value="{{ $key }}" @if(!is_null($customer_case->customer->$field_name) and $customer_case->customer->$field_name == $key) checked @endif>
                <label class="form-check-label" for="{{ $customer_field->field_name . '-' . $key}}">{{ $input }}</label>
            </div>
        <?php } ?>
    <div id="{{ $customer_field->field_name }}-error" style="display: block;" class="invalid-feedback">{{ isset($errors) && $errors->has($customer_field->field_name) ? $errors->first($customer_field->field_name) : '' }}</div>
</div>
