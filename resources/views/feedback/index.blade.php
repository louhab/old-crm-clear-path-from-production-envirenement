@extends('layouts.getintouch')
@section('js_after')
    <script>
        window.setIntlTel("phone");
        var input = document.getElementById('phone');
        input.oninvalid = function(event) {
            event.target.setCustomValidity('Format : +212650123456');
        }
    </script>
    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/getintouch.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}">
    </script>
    <script type="text/javascript"
        src="{{ asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min.js') }}"></script>
    <script type="text/javascript">
        (function(l) {
            if (!l) {
                window.lintrk = function(a, b) {
                    window.lintrk.q.push([a, b])
                };
                window.lintrk.q = []
            }
            var s = document.getElementsByTagName("script")[0];
            var b = document.createElement("script");
            b.type = "text/javascript";
            b.async = true;
            b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
            s.parentNode.insertBefore(b, s);
        })(window.lintrk);
    </script>

    <script>
        formElem.onsubmit = async (e) => {
            e.preventDefault();
            Swal.fire({
                title: "Etes vous sur?",
                text: "L'opération est irréversible!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Oui!",
                cancelButtonText: "Annuler",
            }).then(async (result) => {
                if (result.isConfirmed) {
                    const res = await fetch('/feedback', {
                        method: 'POST',
                        body: new FormData(formElem)
                    });

                    if (res.status == 200) {
                        window.setTimeout(function() {
                            window.location.href = "https://clearpathcanada.ca/";
                        }, 3000);
                    }
                }
            });
        };
    </script>
    <script>
        // detectlangBrowser
        let userLang = navigator.language || navigator.userLanguage;
        document.getElementById("detectlangbrowser").value = userLang.substring(0, 2);
    </script>
@endsection

@section('css_after')
    <link rel="stylesheet" type="text/css"
        href="{{ asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <style>
        .switch {
            display: inline-block;
            margin: 0 5px;
            text-align: right;
            position: absolute;
            top: 4px;
            right: 9px;
        }

        .switch>span {
            position: absolute;
            top: 10px;
            pointer-events: none;
            font-family: 'Helvetica', Arial, sans-serif;
            font-weight: bold;
            font-size: 12px;
            text-transform: uppercase;
            text-shadow: 0 1px 0 rgba(0, 0, 0, .06);
            width: 50%;
            text-align: center;
        }

        input.check-toggle-round-flat:checked~.off {
            color: #F36F25;
        }

        input.check-toggle-round-flat:checked~.on {
            color: #fff;
        }

        .switch>span.on {
            left: 0;
            padding-left: 2px;
            color: #ff1d00;
            font-weight: 700;
        }

        .switch>span.off {
            right: 0;
            padding-right: 4px;
            color: #fff;
        }

        .check-toggle {
            position: absolute;
            margin-left: -9999px;
            visibility: hidden;
        }

        .check-toggle+label {
            display: block;
            position: relative;
            cursor: pointer;
            outline: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        input.check-toggle-round-flat+label {
            padding: 2px;
            width: 97px;
            height: 35px;
            background-color: #ff2e2e;
            -webkit-border-radius: 60px;
            -moz-border-radius: 60px;
            -ms-border-radius: 60px;
            -o-border-radius: 60px;
            border-radius: 60px;
        }

        input.check-toggle-round-flat+label:before,
        input.check-toggle-round-flat+label:after {
            display: block;
            position: absolute;
            content: "";
        }

        input.check-toggle-round-flat+label:before {
            top: 2px;
            left: 2px;
            bottom: 2px;
            right: 2px;
            background-color: #f32525c2;
            -moz-border-radius: 60px;
            -ms-border-radius: 60px;
            -o-border-radius: 60px;
            border-radius: 60px;
        }

        input.check-toggle-round-flat+label:after {
            top: 4px;
            left: 4px;
            bottom: 4px;
            width: 85px;
            background-color: #fff;
            -webkit-border-radius: 52px;
            -moz-border-radius: 52px;
            -ms-border-radius: 52px;
            -o-border-radius: 52px;
            border-radius: 52px;
            -webkit-transition: margin 0.2s;
            -moz-transition: margin 0.2s;
            -o-transition: margin 0.2s;
            transition: margin 0.2s;
        }

        input.check-toggle-round-flat:checked+label {}

        input.check-toggle-round-flat:checked+label:after {
            margin-left: 44px;
        }

        input.check-toggle-round-flat:checked+label:after {
            margin-left: 73px;
        }

        input.check-toggle-round-flat+label {
            padding: 2px;
            width: 166px;
            height: 35px;
            background-color: #ff2e2e;
            border-radius: 60px;
        }

        .wrapper {
            display: inline-flex;
            height: 80px;
            width: 100%;
            align-items: center;
            justify-content: space-evenly;
            border-radius: 5px;
            padding: 20px 0;
        }

        .wrapper .option {
            background: #fff;
            height: 100%;
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: space-evenly;
            margin-right: 10px;
            border-radius: 5px;
            cursor: pointer;
            padding: 0 10px;
            border: 2px solid lightgrey;
            transition: all 0.3s ease;
        }

        .wrapper .option .dot {
            height: 20px;
            width: 20px;
            background: #d9d9d9;
            border-radius: 50%;
            position: relative;
        }

        .wrapper .option .dot::before {
            position: absolute;
            content: "";
            top: 4px;
            left: 4px;
            width: 12px;
            height: 12px;
            background: #d90028;
            border-radius: 50%;
            opacity: 0;
            transform: scale(1.5);
            transition: all 0.3s ease;
        }

        input[type="radio"] {
            display: none;
        }


        #option-1:checked:checked~.option-1,
        #option-2:checked:checked~.option-2 {
            border-color: #d90028;
            background: #d90019;
        }

        #option-1:checked:checked~.option-1 .dot,
        #option-2:checked:checked~.option-2 .dot {
            background: #fff;
        }

        #option-1:checked:checked~.option-1 .dot::before,
        #option-2:checked:checked~.option-2 .dot::before {
            opacity: 1;
            transform: scale(1);
        }

        .wrapper .option span {
            font-size: 17px;
            color: #808080;
        }

        #option-1:checked:checked~.option-1 span,
        #option-2:checked:checked~.option-2 span {
            color: #fff;
        }


        /*
                                                                                                                                                                                                                                            Program option                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 */
        #program-option-1:checked:checked~.program-option-1,
        #program-option-2:checked:checked~.program-option-2 {
            border-color: #d90028;
            background: #d90019;
        }

        #program-option-1:checked:checked~.program-option-1 .dot,
        #program-option-2:checked:checked~.program-option-2 .dot {
            background: #fff;
        }

        #program-option-1:checked:checked~.program-option-1 .dot::before,
        #program-option-2:checked:checked~.program-option-2 .dot::before {
            opacity: 1;
            transform: scale(1);
        }

        .wrapper .option span {
            font-size: 17px;
            color: #808080;
        }

        #program-option-1:checked:checked~.program-option-1 span,
        #program-option-2:checked:checked~.program-option-2 span {
            color: #fff;
        }

        .chart-scale {
            display: flex;
            align-items: center;
            gap: 5px;
            flex-wrap: wrap;
        }

        .btn-scale {
            min-width: 44px;
            width: 6%;
            text-align: center;
            font-weight: bold;
            color: black;
            opacity: 0.4;
        }

        .btn-scale-asc-1,
        .btn-scale-desc-10 {
            background-color: #33FF00;
        }

        .btn-scale-asc-1:hover,
        .btn-scale-desc-10:hover {
            background-color: #2CDE00;
        }

        .btn-scale-asc-2,
        .btn-scale-desc-9 {
            background-color: #66FF00;
        }

        .btn-scale-asc-2:hover,
        .btn-scale-desc-9:hover {
            background-color: #59DE00;
        }

        .btn-scale-asc-3,
        .btn-scale-desc-8 {
            background-color: #99FF00;
        }

        .btn-scale-asc-3:hover,
        .btn-scale-desc-8:hover {
            background-color: #85DE00;
        }

        .btn-scale-asc-4,
        .btn-scale-desc-7 {
            background-color: #CCFF00;
        }

        .btn-scale-asc-4:hover,
        .btn-scale-desc-7:hover {
            background-color: #B1DE00;
        }

        .btn-scale-asc-5,
        .btn-scale-desc-6 {
            background-color: #FFFF00;
        }

        .btn-scale-asc-5:hover,
        .btn-scale-desc-6:hover {
            background-color: #DEDE00;
        }

        .btn-scale-asc-6,
        .btn-scale-desc-5 {
            background-color: #FFCC00;
        }

        .btn-scale-asc-6:hover,
        .btn-scale-desc-5:hover {
            background-color: #DEB100;
        }

        .btn-scale-asc-7,
        .btn-scale-desc-4 {
            background-color: #FF9900;
        }

        .btn-scale-asc-7:hover,
        .btn-scale-desc-4:hover {
            background-color: #DE8500;
        }

        .btn-scale-asc-8,
        .btn-scale-desc-3 {
            background-color: #FF6600;
        }

        .btn-scale-asc-8:hover,
        .btn-scale-desc-3:hover {
            background-color: #DE5900;
        }

        .btn-scale-asc-9,
        .btn-scale-desc-2 {
            background-color: #FF3300;
        }

        .btn-scale-asc-9:hover,
        .btn-scale-desc-2:hover {
            background-color: #DE2C00;
        }

        .btn-scale-asc-10,
        .btn-scale-desc-1 {
            background-color: #FF0000;
        }

        .btn-scale-asc-10:hover,
        .btn-scale-desc-1:hover {
            background-color: #DE0000;
        }

        input[type="radio"] {
            -webkit-appearance: none;
            display: none;
        }

        input[type="radio"]:checked+label {
            border: solid 1px black;
            opacity: 1
        }


        /* program-option-2 */
        /* Smartphones (portrait) ----------- */
        @media (max-width : 400px) {

            /* Styles */
            .wrapper {
                flex-direction: column;
            }
        }

        @media (max-width : 1050px) {

            /* Styles */
            .wrapper {
                flex-direction: column;
            }
        }
    </style>
@endsection
@section('content')
    <div class="content content-boxed">
        <div class="logo" style="margin-bottom: 30px;">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <img src="{{ asset('/img/logo-' . (Config::get('app.locale') == 'fr' ? 'fr' : 'en') . '.png') }}"
                    style="width: 220px;">
            </div>
        </div>
        <div class="block block-rounded">
            <div class="block-content">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="padding-top: 30px;padding-bottom: 30px;">
                        <br>
                        <form action="{{ route('feedback-store') }}" method="POST" id="formElem">
                            @csrf
                            @if (!$feedback)
                                <div class="form-group">
                                    <label for="first_name">Nom</label>
                                    <span class="text-danger">*</span>
                                    <input type="text"
                                        class="form-control form-control-alt{{ isset($errors) && $errors->has('first_name') ? ' is-invalid' : '' }}"
                                        id="first_name" name="first_name" value="{{ old('first_name') }}"
                                        placeholder="Nom .." aria-describedby="first_name-error"
                                        aria-invalid="{{ isset($errors) && $errors->has('first_name') ? 'true' : 'false' }}">
                                    <div id="first_name-error" class="invalid-feedback">
                                        {{ isset($errors) && $errors->has('first_name') ? $errors->first('first_name') : '' }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Prénom</label>
                                    <span class="text-danger">*</span>
                                    <input type="text"
                                        class="form-control form-control-alt{{ isset($errors) && $errors->has('last_name') ? ' is-invalid' : '' }}"
                                        id="last_name" name="last_name" value="{{ old('last_name') }}"
                                        placeholder="Prénom .." aria-describedby="last_name-error"
                                        aria-invalid="{{ isset($errors) && $errors->has('last_name') ? 'true' : 'false' }}">
                                    <div id="last_name-error" class="invalid-feedback">
                                        {{ isset($errors) && $errors->has('last_name') ? $errors->first('last_name') : '' }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Telephone</label>
                                    <span class="text-danger">*</span><br />
                                    <input type="tel" class="form-control form-control-alt" id="phone"
                                        name="phone" value="{{ old('phone') }}" aria-describedby="phone-error"
                                        aria-invalid="false">
                                    <div id="phone-error" class="text-danger">
                                        {{ isset($errors) && $errors->has('phone') ? $errors->first('phone') : '' }}</div>
                                    <span id="valid-msg" class="text-success" style="display:none;">✓ Valid</span>
                                </div>
                                <div class="form-group">
                                    <label for="conseiller_id">Conseiller</label>
                                    <span class="text-danger">*</span>
                                    <select
                                        class="form-control form-control-alt{{ isset($errors) && $errors->has('conseiller_id') ? ' is-invalid' : '' }}"
                                        id="conseiller_id" name="conseiller_id" aria-describedby="conseiller_id-error"
                                        aria-invalid="{{ isset($errors) && $errors->has('conseiller_id') ? 'true' : 'false' }}">
                                        <option selected disabled value="">
                                            Conseiller ...
                                        </option>
                                        <?php
                                        $CS = \App\Models\User::where('status', 1)
                                            ->whereIn('role_id', [2, 3])
                                            ->whereNotIn('id', [51, 71, 101, 36, 38, 55, 62, 99, 109])
                                            ->orderBy('name', 'ASC')
                                            ->get();
                                        ?>
                                        @foreach ($CS as $key => $value)
                                            <option {{ old('conseiller_id') == $value->id ? 'selected' : '' }}
                                                value={{ $value->id }}>{{ $value->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div id="conseiller_id-error" class="invalid-feedback">
                                        {{ isset($errors) && $errors->has('conseiller_id') ? $errors->first('conseiller_id') : '' }}
                                    </div>
                                </div>
                            @endif
                            <input type="hidden" name="city" value={{ $city ?: '' }}>
                            <input type="hidden" name="feedback" value="{{ $feedback ? $feedback->id : 0 }}" />
                            <label for="conseiller_id">Quel est votre niveau de satisfaction sur les points
                                ci-dessous : </label>
                            <br />
                            <br />

                            <div class="form-group">
                                <label for="degree_of_professionalism">Amabilité et degré du professionnalisme de notre
                                    personnel ?</label>
                                <div class="chart-scale">

                                    <input type="radio" name="degree_of_professionalism" id="degree_of_professionalism_1"
                                        value="1">
                                    <label for="degree_of_professionalism_1"
                                        class="btn btn-scale btn-scale-desc-1">1</label>

                                    <input type="radio" name="degree_of_professionalism" id="degree_of_professionalism_2"
                                        value="2">
                                    <label for="degree_of_professionalism_2"
                                        class="btn btn-scale btn-scale-desc-2">2</label>

                                    <input type="radio" name="degree_of_professionalism" id="degree_of_professionalism_3"
                                        value="3">
                                    <label for="degree_of_professionalism_3"
                                        class="btn btn-scale btn-scale-desc-3">3</label>

                                    <input type="radio" name="degree_of_professionalism"
                                        id="degree_of_professionalism_4" value="4">
                                    <label for="degree_of_professionalism_4"
                                        class="btn btn-scale btn-scale-desc-4">4</label>

                                    <input type="radio" name="degree_of_professionalism"
                                        id="degree_of_professionalism_5" value="5">
                                    <label for="degree_of_professionalism_5"
                                        class="btn btn-scale btn-scale-desc-5">5</label>

                                    <input type="radio" name="degree_of_professionalism"
                                        id="degree_of_professionalism_6" value="6">
                                    <label for="degree_of_professionalism_6"
                                        class="btn btn-scale btn-scale-desc-6">6</label>

                                    <input type="radio" name="degree_of_professionalism"
                                        id="degree_of_professionalism_7" value="7">
                                    <label for="degree_of_professionalism_7"
                                        class="btn btn-scale btn-scale-desc-7">7</label>

                                    <input type="radio" name="degree_of_professionalism"
                                        id="degree_of_professionalism_8" value="8">
                                    <label for="degree_of_professionalism_8"
                                        class="btn btn-scale btn-scale-desc-8">8</label>

                                    <input type="radio" name="degree_of_professionalism"
                                        id="degree_of_professionalism_9" value="9">
                                    <label for="degree_of_professionalism_9"
                                        class="btn btn-scale btn-scale-desc-9">9</label>

                                    <input type="radio" name="degree_of_professionalism"
                                        id="degree_of_professionalism_10" value="10">
                                    <label for="degree_of_professionalism_10"
                                        class="btn btn-scale btn-scale-desc-10">10</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="quality_of_presentation">Qualité des présentations, explications et votre
                                    degré
                                    de
                                    compréhension ?</label>
                                <div class="chart-scale">

                                    <input type="radio" name="quality_of_presentation" id="quality_of_presentation_1"
                                        value="1">
                                    <label for="quality_of_presentation_1"
                                        class="btn btn-scale btn-scale-desc-1">1</label>

                                    <input type="radio" name="quality_of_presentation" id="quality_of_presentation_2"
                                        value="2">
                                    <label for="quality_of_presentation_2"
                                        class="btn btn-scale btn-scale-desc-2">2</label>

                                    <input type="radio" name="quality_of_presentation" id="quality_of_presentation_3"
                                        value="3">
                                    <label for="quality_of_presentation_3"
                                        class="btn btn-scale btn-scale-desc-3">3</label>

                                    <input type="radio" name="quality_of_presentation" id="quality_of_presentation_4"
                                        value="4">
                                    <label for="quality_of_presentation_4"
                                        class="btn btn-scale btn-scale-desc-4">4</label>

                                    <input type="radio" name="quality_of_presentation" id="quality_of_presentation_5"
                                        value="5">
                                    <label for="quality_of_presentation_5"
                                        class="btn btn-scale btn-scale-desc-5">5</label>

                                    <input type="radio" name="quality_of_presentation" id="quality_of_presentation_6"
                                        value="6">
                                    <label for="quality_of_presentation_6"
                                        class="btn btn-scale btn-scale-desc-6">6</label>

                                    <input type="radio" name="quality_of_presentation" id="quality_of_presentation_7"
                                        value="7">
                                    <label for="quality_of_presentation_7"
                                        class="btn btn-scale btn-scale-desc-7">7</label>

                                    <input type="radio" name="quality_of_presentation" id="quality_of_presentation_8"
                                        value="8">
                                    <label for="quality_of_presentation_8"
                                        class="btn btn-scale btn-scale-desc-8">8</label>

                                    <input type="radio" name="quality_of_presentation" id="quality_of_presentation_9"
                                        value="9">
                                    <label for="quality_of_presentation_9"
                                        class="btn btn-scale btn-scale-desc-9">9</label>

                                    <input type="radio" name="quality_of_presentation" id="quality_of_presentation_10"
                                        value="10">
                                    <label for="quality_of_presentation_10"
                                        class="btn btn-scale btn-scale-desc-10">10</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="degree_of_mastery">Compétence et degré de maitrise de votre conseiller
                                    ?</label>
                                <div class="chart-scale">

                                    <input type="radio" name="degree_of_mastery" id="degree_of_mastery_1"
                                        value="1">
                                    <label for="degree_of_mastery_1" class="btn btn-scale btn-scale-desc-1">1</label>

                                    <input type="radio" name="degree_of_mastery" id="degree_of_mastery_2"
                                        value="2">
                                    <label for="degree_of_mastery_2" class="btn btn-scale btn-scale-desc-2">2</label>

                                    <input type="radio" name="degree_of_mastery" id="degree_of_mastery_3"
                                        value="3">
                                    <label for="degree_of_mastery_3" class="btn btn-scale btn-scale-desc-3">3</label>

                                    <input type="radio" name="degree_of_mastery" id="degree_of_mastery_4"
                                        value="4">
                                    <label for="degree_of_mastery_4" class="btn btn-scale btn-scale-desc-4">4</label>

                                    <input type="radio" name="degree_of_mastery" id="degree_of_mastery_5"
                                        value="5">
                                    <label for="degree_of_mastery_5" class="btn btn-scale btn-scale-desc-5">5</label>

                                    <input type="radio" name="degree_of_mastery" id="degree_of_mastery_6"
                                        value="6">
                                    <label for="degree_of_mastery_6" class="btn btn-scale btn-scale-desc-6">6</label>

                                    <input type="radio" name="degree_of_mastery" id="degree_of_mastery_7"
                                        value="7">
                                    <label for="degree_of_mastery_7" class="btn btn-scale btn-scale-desc-7">7</label>

                                    <input type="radio" name="degree_of_mastery" id="degree_of_mastery_8"
                                        value="8">
                                    <label for="degree_of_mastery_8" class="btn btn-scale btn-scale-desc-8">8</label>

                                    <input type="radio" name="degree_of_mastery" id="degree_of_mastery_9"
                                        value="9">
                                    <label for="degree_of_mastery_9" class="btn btn-scale btn-scale-desc-9">9</label>

                                    <input type="radio" name="degree_of_mastery" id="degree_of_mastery_10"
                                        value="10">
                                    <label for="degree_of_mastery_10" class="btn btn-scale btn-scale-desc-10">10</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="speed">Rapidité, efficacité et délai de traitement de votre dossier
                                    ?</label>
                                <div class="chart-scale">

                                    <input type="radio" name="speed" id="speed_1" value="1">
                                    <label for="speed_1" class="btn btn-scale btn-scale-desc-1">1</label>

                                    <input type="radio" name="speed" id="speed_2" value="2">
                                    <label for="speed_2" class="btn btn-scale btn-scale-desc-2">2</label>

                                    <input type="radio" name="speed" id="speed_3" value="3">
                                    <label for="speed_3" class="btn btn-scale btn-scale-desc-3">3</label>

                                    <input type="radio" name="speed" id="speed_4" value="4">
                                    <label for="speed_4" class="btn btn-scale btn-scale-desc-4">4</label>

                                    <input type="radio" name="speed" id="speed_5" value="5">
                                    <label for="speed_5" class="btn btn-scale btn-scale-desc-5">5</label>

                                    <input type="radio" name="speed" id="speed_6" value="6">
                                    <label for="speed_6" class="btn btn-scale btn-scale-desc-6">6</label>

                                    <input type="radio" name="speed" id="speed_7" value="7">
                                    <label for="speed_7" class="btn btn-scale btn-scale-desc-7">7</label>

                                    <input type="radio" name="speed" id="speed_8" value="8">
                                    <label for="speed_8" class="btn btn-scale btn-scale-desc-8">8</label>

                                    <input type="radio" name="speed" id="speed_9" value="9">
                                    <label for="speed_9" class="btn btn-scale btn-scale-desc-9">9</label>

                                    <input type="radio" name="speed" id="speed_10" value="10">
                                    <label for="speed_10" class="btn btn-scale btn-scale-desc-10">10</label>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="overall">Globalement comment évaluez - vous votre expérience au sein de
                                    notre agence ?</label>
                                <div class="chart-scale">

                                    <input type="radio" name="overall" id="overall_1" value="1">
                                    <label for="overall_1" class="btn btn-scale btn-scale-desc-1">1</label>

                                    <input type="radio" name="overall" id="overall_2" value="2">
                                    <label for="overall_2" class="btn btn-scale btn-scale-desc-2">2</label>

                                    <input type="radio" name="overall" id="overall_3" value="3">
                                    <label for="overall_3" class="btn btn-scale btn-scale-desc-3">3</label>

                                    <input type="radio" name="overall" id="overall_4" value="4">
                                    <label for="overall_4" class="btn btn-scale btn-scale-desc-4">4</label>

                                    <input type="radio" name="overall" id="overall_5" value="5">
                                    <label for="overall_5" class="btn btn-scale btn-scale-desc-5">5</label>

                                    <input type="radio" name="overall" id="overall_6" value="6">
                                    <label for="overall_6" class="btn btn-scale btn-scale-desc-6">6</label>

                                    <input type="radio" name="overall" id="overall_7" value="7">
                                    <label for="overall_7" class="btn btn-scale btn-scale-desc-7">7</label>

                                    <input type="radio" name="overall" id="overall_8" value="8">
                                    <label for="overall_8" class="btn btn-scale btn-scale-desc-8">8</label>

                                    <input type="radio" name="overall" id="overall_9" value="9">
                                    <label for="overall_9" class="btn btn-scale btn-scale-desc-9">9</label>

                                    <input type="radio" name="overall" id="overall_10" value="10">
                                    <label for="overall_10" class="btn btn-scale btn-scale-desc-10">10</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="note">Y a-t-il d'autres observations et/ou suggestions que vous
                                    aimeriez
                                    nous
                                    transmettre ?</label>
                                <textarea type="text" class="form-control form-control-alt" id="note" name="note"
                                    placeholder="Message.." aria-describedby="add-form-message-error" aria-invalid="false"></textarea>

                                <div id="note-error" class="invalid-feedback"></div>
                            </div>
                            <div class="form-group" style="padding-top: 20px;">
                                <button type="submit" class="btn btn-lg btn-alt-primary btn-block"><i
                                        class="fa fa-fw fa-plus mr-1"></i>Envoyer</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
