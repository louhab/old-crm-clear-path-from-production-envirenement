@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <!-- Page JS Code -->
    <script>
        window.userRole = {!! auth()->user()->toJson() !!};
        //console.log(window.userRole);
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            $('input[name="date_start"]').val(start.format('YYYY-MM-DD'));
            $('input[name="date_end"]').val(end.format('YYYY-MM-DD'));
            // dashboardPage.initDashboardAnalytics();
        });
        $("[name='tags[]']").select2({
            placeholder: "Selectionnez mots clés",
        });
        var tag = @json($tag_id);
    </script>
    <script src="{{ asset('js/pages/tags/leads.js') }}"></script>
@endsection

@section('css_after')
    <!-- Styles -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        /* .select2-selection__rendered {
            line-height: 39px !important;
        }
        .select2-container .select2-selection--single {
            height: 42px !important;
        }
        .select2-selection__arrow {
            height: 43px !important;
        } */

        .select2-selection__choice__remove {
            color: #fff !important;
        }
    </style>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Leads','subTitle' => 'leads', 'navItems' => ['Admin', 'Leads']])

    <!-- Page Content -->
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les informations !</a>
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        <!-- leads Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche leads</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <form id="search-form" name="search-form" action="" method="post" onsubmit="return false">
                            @csrf
                            <div class="form-group form-row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-3 p-1 pl-4">
                                            <label for="lead" class="sr-only">Lead</label>
                                            <input class="form-control dt_search_field dt_search_lead" id="lead" name="lead" style="height:44px" placeholder="Lead..">
                                            <!-- <select class="form-control dt_search_field dt_search_lead" id="lead" name="lead">
                                            </select> -->
                                        </div>
                                        <div class="col-3 p-1">
                                            <label for="program" class="sr-only">Programme</label>
                                            {{Form::select('program', \App\Models\Program::pluck('name', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', 'placeholder' => 'Programme...','style' => 'height: 42px;border: 1px solid #aaa;'])}}
                                        </div>
                                        <div class="col-3">
                                            <label for="status" class="sr-only">Status</label>
                                            <select name="status" id="status" class="form-control form-control-alt dt_search_field" style="height: 42px;border: 1px solid #aaa;">
                                                <option value>Status...</option>
                                                <option value="n/d">Non Traité</option>
                                                @foreach(\App\Models\LeadStatus::pluck('status_label_fr', 'id') as $key => $status)
                                                    <option value="{{ $key }}">{{ $status }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-3">
                                            <label for="campaign" class="sr-only">Canal</label>
                                            {{Form::select('campaign', \App\Models\LeadFormMarketingCampaign::pluck('campaign_name', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', 'placeholder' => 'Canal...','style' => 'height: 42px;border: 1px solid #aaa;'])}}
                                        </div>
                                        @unlessconseiller
                                        <div class="col-3 pl-4">
                                            <label for="conseiller" class="sr-only">Conseiller</label>
                                            {{Form::select('conseiller', \App\Models\User::where("status", 1)->where("role_id", 3)->pluck('name', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', 'placeholder' => 'Conseiller...','style' => 'height: 42px;border: 1px solid #aaa;'])}}
                                        </div>
                                        @endconseiller
                                        <div class="col-3 p-1">
                                            <label for="phone" class="sr-only">Phone</label>
                                            <input class="form-control dt_search_field dt_search_phone" id="phone" name="phone" style="height:44px" placeholder="Phone">
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text input-group-text-alt">
                                                            <i class="far fa-calendar-alt"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control form-control" id="daterange" name="daterange">
                                                    <input type="hidden" name="date_end" class="dt_search_field" />
                                                    <input type="hidden" name="date_start" class="dt_search_field" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-5 p-1 pl-4">
                                                    <button type="button" class="btn btn-alt-primary btn-block submit-search" id="search-btn"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                                                </div>
                                                <div class="col-3 p-1">
                                                    <button type="button" class="btn btn-alt-danger btn-block clear-search" id="clear-search-btn"><i class="fa fa-fw fa-redo mr-1"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="leads-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Liste des leads</h3>
                </div>
                <h4></h4>

                <div class="table-responsive">
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                    <table class="table table-bordered table-striped table-vcenter" id="leads-dt">
                        <thead>
                        <tr>
                            <th></th>
                            <th class="text-center" style="width: 80px;">ID</th>
                            <th>Prénom</th>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Programme</th>
                            <th>Phone</th>
                            <th>Canal</th>
                            <th>Référent</th>
                            <th>Conseiller</th>
                            <th>Date de Création</th>
                            <th>Status</th>
                            <!-- <th>Client</th> -->
                            <!-- <th style="width: 15%;">Actions</th> -->
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END leads Table -->
    </div>
    <!-- END Page Content -->
@endsection
