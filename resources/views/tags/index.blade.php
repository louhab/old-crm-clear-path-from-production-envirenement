@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tags/tags.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Mots Clés','subTitle' => 'Etapes', 'navItems' => ['Admin', 'Mots Clés']])

    <!-- Page Content -->
    <div class="content">
        <!-- Tag Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">
                            <div class="col-3">
                                <input type="text" class="form-control dt_search_field" name="tag_name" placeholder="Mot Clé">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="tags-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Mots Clés</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary" id="tag-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter mot clé</button>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="tags-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END tags Table -->




    </div>
    <!-- END Page Content -->

    <!-- tag Add Modal -->
    <div class="modal" id="modal-tag-add" tabindex="-1" role="dialog" aria-labelledby="modal-tag-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-tag-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter Mot Clé</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form tag Add -->
                                <form action="" method="POST" onsubmit="return false;" id="tag-add-form">

                                    <div class="form-group">
                                        <label for="add-form-tag_name">Mot Clé</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-tag_name" name="add-form-tag_name" placeholder="Mot Clé.." aria-describedby="add-form-tag_name-error" aria-invalid="false">
                                        <div id="add-form-tag_name-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-tag-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter mot clé</button>
                                    </div>
                                </form>
                                <!-- END Form tag Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END tag Add Modal -->

    <!-- tag Edit Modal -->
    <div class="modal" id="modal-tag-edit" tabindex="-1" role="dialog" aria-labelledby="modal-tag-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-tag-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Modifier mot Clé</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form tag Add -->
                                <form action="" method="POST" onsubmit="return false;" id="tag-edit-form">

                                    <div class="form-group">
                                        <label for="edit-form-tag_name">Nom</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-tag_name" name="edit-form-tag_name" placeholder="Niveau.." aria-describedby="edit-form-tag_name-error" aria-invalid="false">
                                        <div id="edit-form-tag_name-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="update-tag-btn"><i class="fa fa-fw fa-check mr-1"></i>Modifier mot clé</button>
                                    </div>
                                </form>
                                <!-- END Form tag Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END tag Edit Modal -->
@endsection
