@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <!--    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>-->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/programs/list.js') }}"></script>
@endsection
@section('css_after')
    <!-- Styles -->
    <!--    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">-->
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Programmes','subTitle' => 'Liste des programmes', 'navItems' => ['Admin', 'Programmes']])

    <!-- Page Content -->
    <div class="content">
        <!-- Users Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">
                            <div class="col-3">
                                <input type="text" class="form-control dt_search_field" name="name" placeholder="Nom">
                            </div>

                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="programs-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Liste des Programmes</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary" id="program-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter programme</button>
                    </div>
                </div>
                <table class="table table-bordered table-sm table-condensed table-vcenter" id="programs-dt">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">ID</th>
                        <th>Nom</th>
                        <th class="d-none d-sm-table-cell" style="width: 25%;">Status</th>
                        <th style="width: 15%;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Users Table -->

    </div>
    <!-- END Page Content -->

    <!-- User Add Modal -->
    <div class="modal" id="modal-user-add" tabindex="-1" role="dialog" aria-labelledby="modal-user-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-user-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter utilisateur</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form User Add -->
                                <form action="" method="POST" onsubmit="return false;" id="user-add-form">
                                    <div class="form-group">
                                        <label for="add-form-name">Nom</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-name" name="add-form-name" placeholder="Nom.." aria-describedby="add-form-name-error" aria-invalid="false">
                                        <div id="add-form-name-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-email">Email</label>
                                        <input type="email" class="form-control form-control-alt" id="add-form-email" name="add-form-email" placeholder="Email.." aria-describedby="add-form-email-error" aria-invalid="false">
                                        <div id="add-form-email-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-phone">Phone</label>
                                        <span class="text-danger">*</span><br/>
                                        <input type="tel" class="form-control form-control-alt" id="add-form-phone" name="add-form-phone" aria-describedby="add-form-phone-error" aria-invalid="false">
                                        <div id="add-form-phone-error" class="text-danger"></div>
                                        <span id="valid-msg" class="text-success" style="display:none;">✓ Valid</span>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-user-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter utilisateur</button>
                                    </div>
                                </form>
                                <!-- END Form User Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END User Add Modal -->

    <!-- User Edit Modal -->
    <div class="modal" id="modal-user-edit" tabindex="-1" role="dialog" aria-labelledby="modal-user-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-user-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Modifier utilisateur</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form User Edit -->
                                <form action="" method="POST" onsubmit="return false;" id="user-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-name">Nom</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-name" name="edit-form-name" placeholder="Nom.." aria-describedby="edit-form-name-error" aria-invalid="false">
                                        <div id="edit-form-name-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-email">Email</label>
                                        <input type="email" class="form-control form-control-alt" id="edit-form-email" name="edit-form-email" placeholder="Email.." aria-describedby="edit-form-email-error" aria-invalid="false">
                                        <div id="edit-form-email-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <span class="text-danger">*</span><br/>
                                        <input type="tel" class="form-control form-control-alt" id="phone" name="phone" aria-describedby="phone-error" aria-invalid="false">
                                        <div id="phone-error" class="text-danger"></div>
                                        <span id="valid-msg" class="text-success" style="display:none;">✓ Valid</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Gender :</label>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input edit-form-gender" type="radio" id="example-radios1" name="edit-form-gender" value="male">
                                            <label class="form-check-label" for="example-radios1">Homme</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input edit-form-gender" type="radio" id="edit-form-gender" name="edit-form-gender" value="female">
                                            <label class="form-check-label" for="edit-form-gender">Femme</label>
                                        </div>
                                        <div id="gender-error" class="invalid-feedback">{{ isset($errors) && $errors->has('gender') ? $errors->first('gender') : '' }}</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-password">Password</label>
                                        <input type="password" class="form-control form-control-alt" id="edit-form-password" name="edit-form-password" placeholder="Password.." aria-describedby="edit-form-password-error" aria-invalid="false">
                                        <div id="edit-form-password-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-role">Rôle</label>
                                        <select class="form-control form-control-alt" id="edit-form-role" name="edit-form-role" aria-describedby="edit-form-role-error" aria-invalid="false">
                                            <option value="">Sélectionnez un rôle</option>
                                            @foreach(\App\Models\Role::all() as $role)
                                                <option value="{{ $role->id }}">{{ $role->role_name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-role-error" class="invalid-feedback"></div>
                                    </div>
                                    @admin
                                    <div class="form-group">
                                        <label for="edit-form-status">Status</label>
                                        <select class="form-control form-control-alt" id="edit-form-status" name="edit-form-status" aria-describedby="edit-form-status-error" aria-invalid="false">
                                            @php $user = Illuminate\Support\Facades\Auth::user(); @endphp
                                            <option value="1">Enabled</option>
                                            <option value="0">Disabled</option>
                                        </select>
                                        <div id="edit-form-status-error" class="invalid-feedback"></div>
                                    </div>
                                    @endadmin
                                    <hr>
                                    <div class="form-group">
                                        <label for="edit-form-lang">Langue: </label>
                                        <select name="edit-form-lang" id="edit-form-lang" class="form-control form-control-alt">
                                            <option value="">Langue</option>
                                            <option value="fr">Français</option>
                                            <option value="en">Anglais</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-country">Country</label>
                                        <select class="form-control form-control-alt" id="edit-form-country" name="edit-form-country">
                                            <option value="">{{__('lang.country')}}</option>
                                            @foreach(\App\Models\Country::all() as $country)
                                                <option value="{{ $country->id }}">{{ $country->name_fr_fr }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="update-user-btn"><i class="fa fa-fw fa-check mr-1"></i>Modifier utilisateur</button>
                                    </div>
                                </form>
                                <!-- END Form User Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END User Edit Modal -->
@endsection
