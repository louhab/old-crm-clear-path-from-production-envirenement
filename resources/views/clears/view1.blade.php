@extends('layouts.backend')
@section('css_after')
    <meta http-equiv="Expires" content="Mon, 26 Jul 1997 05:00:00 GMT">
    <meta http-equiv="Pragma" content="no-cache">
    @livewireStyles
    <script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pace-js@latest/pace-theme-default.min.css">
    <style>
        .pace {
            -webkit-pointer-events: none;
            pointer-events: none;

            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;

            z-index: 2000;
            position: fixed;
            height: 90px;
            width: 90px;
            margin: auto;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }

        .pace.pace-inactive .pace-activity {
            display: none;
        }

        .pace .pace-activity {
            position: fixed;
            z-index: 2000;
            display: block;
            position: absolute;
            left: -30px;
            top: -30px;
            height: 90px;
            width: 90px;
            display: block;
            border-width: 30px;
            border-style: double;
            border-color: #d52b12 transparent transparent;
            border-radius: 50%;

            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;

            -webkit-animation: spin 1s linear infinite;
            -moz-animation: spin 1s linear infinite;
            -o-animation: spin 1s linear infinite;
            animation: spin 1s linear infinite;
        }

        .pace .pace-activity:before {
            content: ' ';
            position: absolute;
            top: 10px;
            left: 10px;
            height: 50px;
            width: 50px;
            display: block;
            border-width: 10px;
            border-style: solid;
            border-color: #d52b12 transparent transparent;
            border-radius: 50%;

            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
        }

        @-webkit-keyframes spin {
            100% { -webkit-transform: rotate(359deg); }
        }

        @-moz-keyframes spin {
            100% { -moz-transform: rotate(359deg); }
        }

        @-o-keyframes spin {
            100% { -moz-transform: rotate(359deg); }
        }

        @keyframes spin {
            100% {  transform: rotate(359deg); }
        }
        /*clear docs*/
        .document-in {
            margin: 0 !important;
        }
        .document-in label {
            padding: 0 !important;
            margin: 0 !important;
        }
        .document-in .form-group {line-height: 0.9;}
        .document-in .form-group {
            margin: 0 !important;
        }
    </style>
@endsection
@section('js_after')
    <!-- Page JS Helpers (Table Tools helpers) -->
    <!-- Page JS DataTables Plugins -->
    <script src="{{ asset('js/pages/dropzone.js') }}"></script>
    @livewireScripts
@endsection
@section('content')
    @include('layouts.partials.hero', ['title' => $sheet->name_fr,'subTitle' => '', 'navItems' => ['Clear', 'informations']])

    <!-- Page Content -->
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les informations !</a>
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        <div class="row">
            <div class="col-6">
                <div class="block block-rounded">
                    <div class="block-header">
                        <h3 class="block-title">documents</h3>
<!--                        <div class="block-options">
                            <div class="block-options-item">
                                <code>.table-hover</code>
                            </div>
                        </div>-->
                    </div>
                    <div class="block-content">
                        <table class="table table-hover table-vcenter">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th class="d-none d-sm-table-cell" style="width: 15%;">Groupe</th>
                                <th class="text-center" style="width: 100px;">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $docs = $sheet->docs()->get();
                                /*$groupedDocuments = [];
                                foreach ($docs as $doc) {
                                    $doc->tagNames();
                                }*/
                            @endphp
                            @foreach($docs as $doc)
                                <tr>
                                    <td class="font-w600 font-size-sm">
                                        <a href="javascript:void(0)">{{ $doc->name_fr }}</a>
                                    </td>
                                    <td class="d-none d-sm-table-cell">
                                        @php $group = \App\Models\DocumentTypeGroup::find($doc->document_type_group_id); @endphp
                                        <span class="badge badge-info">{{ $group ? $group->name_fr:'null' }}</span>
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-sm btn-light js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Edit Client">
                                                <i class="fa fa-fw fa-pencil-alt"></i>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-light js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Remove Client">
                                                <i class="fa fa-fw fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="block">
                    <div class="block-header">
                        <h3 class="block-title">Clears</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

