@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <!-- Page JS Code -->
    <script>
        window.userRole = {!! auth()->user()->toJson() !!};
        window.dashboard_filter = @json($dashboard_filter);
    </script>
    <script src="{{ asset('js/pages/finances.js') }}"></script>
    <script>
        // filter dashboard
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        }, function(start, end, label) {
            // console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            $('input[name="date_start"]').val(start.format('YYYY-MM-DD'));
            $('input[name="date_end"]').val(end.format('YYYY-MM-DD'));
            $('#finances-dt').DataTable().ajax.reload(null, false);
        });
        //$(document).ready(function() {
        // console.log(dashboard_filter);
        // daterange
        if ("daterange" in window.dashboard_filter) {
            // console.log(Object.keys(dashboard_filter));
            // $('input[name="daterange"]').val(dashboard_filter["daterange"]);
            let start = new Date(window.dashboard_filter["daterange"].split(" - ")[0]);
            let end = new Date(window.dashboard_filter["daterange"].split(" - ")[1]);
            // console.log(start.format('YYYY-MM-DD'), end);
            $('#daterange').data('daterangepicker').setStartDate(start);
            $('#daterange').data('daterangepicker').setEndDate(end);
            $('input[name="date_start"]').val(start.getFullYear() + "-" + (start.getMonth() + 1) + "-" + start.getDate());
            $('input[name="date_end"]').val(end.getFullYear() + "-" + (end.getMonth() + 1) + "-" + end.getDate());
        }
        // status
        if ("state" in window.dashboard_filter) {
            // console.log("Test!");
            $("#status").val(dashboard_filter["state"]);
        }

        $("[name='meeting']").select2({
            placeholder: "R1 .. R6 ...",
            allowClear: true,
            // width:"100%"
        });

        if ("agent-group" in dashboard_filter) {
            // console.log(dashboard_filter["agent-group"], "sdjbsdajkfbksdj")
            if (dashboard_filter["agent-group"] == "Support") {
                $('#search-form select[name="support"]').val(dashboard_filter["agent"]);
            } else {
                $('#search-form select[name="conseiller"]').val(dashboard_filter["agent"]);
            }
        }
    </script>
@endsection

@section('css_after')
    <!-- Styles -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        .select2-selection__rendered {
            line-height: 39px !important;
        }

        .select2-container .select2-selection--single {
            height: 40px !important;
        }

        .select2-selection__arrow {
            height: 43px !important;
        }

        .select2-selection__choice__remove {
            color: #fff !important;
        }
    </style>
@endsection

@section('content')
    @include('layouts.partials.hero', [
        'title' => 'Finance',
        'subTitle' => 'Finances',
        'navItems' => ['Admin', 'Finances'],
    ])

    <!-- Page Content -->
    <div class="content">
        <!-- finances Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">

                            <div class="col-3" style="margin-bottom: 20px;">
                                <input type="text" class="form-control dt_search_field" name="name" placeholder="Nom">
                            </div>
                            @administration
                                <div class="col-3">
                                    <label for="agent" class="sr-only">Utilisateurs...</label>
                                    <select class="form-control form-control-alt dt_search_field dt_search_agent" id="agent"
                                        name="agent" multiple="multiple">
                                        @if (array_key_exists('agent', $dashboard_filter))
                                            <option value="{{ $dashboard_filter['agent'] }}" selected="selected">
                                                {{ \App\Models\User::find($dashboard_filter['agent'])->name }}</option>
                                        @endif
                                    </select>
                                </div>
                                <input type="hidden" class="dt_search_field" name="agent-group" value="">
                            @endadministration

                            {{-- add here --}}
                            <div class="col-3">
                                <label for="method-select" class="sr-only"></label>
                                <select name="method-select" id="method-select"
                                    class="form-control form-control-alt dt_search_field"
                                    style="height: 42px;border: 1px solid #aaa;">
                                    <option value>Méthode de paiement ...</option>
                                    @foreach (\App\Models\PaymentMethod::pluck('method_label_fr', 'id') as $key => $status)
                                        <option value="{{ $key }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-3">
                                <label for="check-select" class="sr-only"></label>
                                <select name="check-select" id="check-select"
                                    class="form-control form-control-alt dt_search_field"
                                    style="height: 42px;border: 1px solid #aaa;">
                                    <option value>Vérification Status ...</option>
                                    <option value="1">Verifié</option>
                                    <option value="n/d">Non Traité</option>
                                </select>
                            </div>


                            {{-- <div class="col-3">
                                <label for="method-select" class="sr-only"></label>
                                <select name="method-select" id="method-select" class="form-control form-control-alt dt_search_field" style="height: 42px;border: 1px solid #aaa;">
                                    <option value>Méthode de paiement ...</option>
                                    @foreach (\App\Models\PaymentMethod::pluck('method_label_fr', 'id') as $key => $status)
                                        <option value="{{ $key }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            <div class="col-3 p-1">
                                <label for="meeting" class="sr-only"></label>
                                <select name="meeting" id="meeting" class="form-control form-control-alt dt_search_field"
                                    style="height: 42px;border: 1px solid #aaa;">
                                    <option value>R1 .. R6</option>
                                    <option value="1">R1</option>
                                    <option value="2">R2</option>
                                    <option value="3">R3</option>
                                    <option value="4">R4</option>
                                    <option value="5">R5</option>
                                </select>
                            </div>
                            {{-- end here --}}

                            <div class="col-3">
                                <label for="status" class="sr-only"></label>
                                <select name="status" id="status" class="form-control form-control-alt dt_search_field"
                                    style="height: 42px;border: 1px solid #aaa;">
                                    <option value>Status...</option>
                                    <option value="processed">Traité</option>
                                    <option value="n/d">Non Traité</option>
                                    @foreach (\App\Models\LeadStatus::pluck('status_label_fr', 'id') as $key => $status)
                                        <option value="{{ $key }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text input-group-text-alt">
                                                <i class="far fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control" id="daterange"
                                            name="daterange" value="">
                                        <input type="hidden" name="date_end" class="dt_search_field" />
                                        <input type="hidden" name="date_start" class="dt_search_field" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i
                                        class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i
                                        class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="finances-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">
                        Finances
                    </h3>
                    <div class="block-options">
                    </div>
                </div>
                <h4></h4>

                {{-- totat element start --}}

                <h4>Total</h4>
                <div class="row row-deck">
                    <div class="px-2">
                        <div class="block block-rounded d-flex flex-column bg-light">
                            <div
                                class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                <dl class="mb-0">
                                    <dt class="font-size-h2 font-w700 finances-stats-total-MAD">0</dt>
                                    <dd class="text-muted mb-0 finances-stats-total-MAD">MAD</dd>
                                </dl>
                            </div>
                        </div>
                    </div>

                    <div class="px-2">
                        <div class="block block-rounded d-flex flex-column bg-light">
                            <div
                                class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                <dl class="mb-0">
                                    <dt class="font-size-h2 font-w700 finances-stats-total-EUR">0</dt>
                                    <dd class="text-muted mb-0 finances-stats-total-EUR">Euro</dd>
                                </dl>
                            </div>
                        </div>
                    </div>

                    <div class="px-2">
                        <div class="block block-rounded d-flex flex-column bg-light">
                            <div
                                class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                <dl class="mb-0">
                                    <dt class="font-size-h2 font-w700 finances-stats-total-USD">0</dt>
                                    <dd class="text-muted mb-0 finances-stats-total-USD">usd</dd>
                                </dl>
                            </div>
                        </div>
                    </div>

                </div>
                {{-- total element end --}}

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="finances-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END finances Table -->
    </div>
    <!-- END Page Content -->
@endsection
