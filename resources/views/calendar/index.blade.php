<?php
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
?>
@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script type="text/javascript" src="{{ asset('/js/plugins/fullcalendar/main.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/fullcalendar/locales-all.min.js') }}"></script>
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <!--    <script src="{{ asset('js/plugins/fullcalendar-scheduler/lib/main.js') }}"></script>-->

    <!-- Page JS Code -->
    <script>
        // var calendar__ = null;
        // var events = [];

        $("[name='advisors']").select2({
            placeholder: "Conseiller ...",
            width: "100%",
            allowClear: true
        });


        // $(document).ready(function() {
        //     console.log("holla !!")

        //     console.log(calendar__, "after render")
        //     if (calendar__) {
        //         calendar = calendar__;
        //         console.log("calendar", calendar)
        //         for (let i = 0; i < Object.keys(calendar).length; i++) {
        //             let title = calendar[i].title;
        //             title = title.replace('Rencontre 2', 'R2');
        //             title = title.replace('Rencontre 1', 'R1');
        //             title = title.replace('Rencontre 3', 'R3');
        //             events.push({
        //                 start: new Date(calendar[i].year, calendar[i].month - 1, calendar[i].day, calendar[
        //                     i].hour, calendar[i].minute),
        //                 end: new Date(calendar[i].year, calendar[i].month - 1, calendar[i].day, calendar[i]
        //                     .hourEnd, calendar[i].minuteEnd),
        //                 title: title,
        //                 RTitle: calendar[i].title,
        //                 description: calendar[i].description,
        //                 customer_id: calendar[i].customer_id,
        //                 step: calendar[i].step,
        //                 isClient: calendar[i].is_client,
        //                 lead_id: calendar[i].lead_id,
        //                 customer_name: calendar[i].customer_name,
        //                 time_start: calendar[i].time_start,
        //                 time_end: calendar[i].time_end,
        //                 id: calendar[i].id,
        //             });
        //         }
        //         console.log(events, "this is events")
        //     }
        // })
    </script>
    <script>
        APP_URL = '{{ URL::to('/') }}';
        calendar = @json($calendar);
        // console.log(calendar)
        var events = [];
        for (let i = 0; i < Object.keys(calendar).length; i++) {
            // console.log("tt: "+calendar[i].customer_id)
            // console.log(calendar[i].year, calendar[i].month - 1, calendar[i].day, calendar[i].hour, calendar[i].minute);
            let title = calendar[i].title;
            //if (title 00)
            title = title.replace('Rencontre 2', 'R2');
            title = title.replace('Rencontre 1', 'R1');
            title = title.replace('Rencontre 3', 'R3');
            events.push({
                start: new Date(calendar[i].year, calendar[i].month - 1, calendar[i].day, calendar[i].hour,
                    calendar[i].minute),
                end: new Date(calendar[i].year, calendar[i].month - 1, calendar[i].day, calendar[i].hourEnd,
                    calendar[i].minuteEnd),
                title: title,
                RTitle: calendar[i].title,
                description: calendar[i].description,
                customer_id: calendar[i].customer_id,
                step: calendar[i].step,
                isClient: calendar[i].is_client,
                lead_id: calendar[i].lead_id,
                customer_name: calendar[i].customer_name,
                time_start: calendar[i].time_start,
                time_end: calendar[i].time_end,
                id: calendar[i].id,
                eventType: calendar[i].event_type,
                // allDay: !0,
            });
        }
    </script>
    <script src="{{ asset('/js/pages/calendar.js') }}"></script>
@endsection

@section('css_after')
    <link rel="stylesheet" href="{{ asset('/js/plugins/fullcalendar/main.css') }}" />
    <!--    <link rel="stylesheet" href="{{ asset('/js/plugins/fullcalendar-scheduler/lib/main.css') }}" />-->
@endsection

@section('content')
    @include('layouts.partials.hero', [
        'title' => 'Calendar',
        'subTitle' => 'Agenda',
        'navItems' => ['Admin', 'Agenda'],
    ])
    <!-- Page Content -->
    <div class="content">
        <input type="hidden" name="lang" value="{{ $lang }}">
        <!-- Calendar -->
        @if (Session::get('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ Session::get('success') }}</p>
            </div>
        @endif

        @if (Session::get('error'))
            <div class="alert alert-info alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ Session::get('error') }}</p>
            </div>
        @endif

        <form id="search-form" name="search-form" action="" method="post" onsubmit="return false">
            @csrf
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <h4>Faire une recherche</h4>
                    <div class="row push search-filter">
                        <div class="col-lg-12">
                            <div class="form-group form-row">
                                <div class="col-3 p-1">
                                    @php
                                    $advisors = \App\Models\User::whereIn('role_id', [3, 2])->where('status', 1)->get()
                                            ->pluck('name', 'id')
                                            ->toArray();
                                    @endphp
                                    <label for="advisors" class="sr-only"></label>
                                    <select name="advisors" id="advisors"
                                        class="form-control form-control-alt dt_search_field"
                                        style="height: 42px;border: 1px solid #aaa;" data-control="select2"
                                        multiple="multiple" aria-describedby="tags-error" aria-invalid="">
                                        @foreach ($advisors as $key => $status)
                                            <option value="{{ $key }}">{{ $status }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{-- <div class="col-3">
                                    @php
                                        $advisors = \App\Models\User::where([
                                            'role_id' => 3,
                                            'status' => 1,
                                        ])
                                            ->get()
                                            ->pluck('name', 'id')
                                            ->toArray();
                                    @endphp
                                    {{ Form::select('advisors', $advisors, null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                </div> --}}
                                <div class="col-2">
                                    <button type="button" class="btn btn-alt-primary btn-block submit-search"><i
                                            class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="block">
            <div class="block-content">
                <div class="" id="users-dt-block">
                    <div class="">
                        <div class="block-header" style="padding: 0;">
                            <h3 style="margin-top: 15px;">{{ __('lang.agenda') }}</h3>
                            <div class="block-options">
                                <button type="button" class="btn btn-alt-primary" id="calendar-table-add"><i
                                        class="fa fa-fw fa-plus mr-1"></i>{{ __('lang.add_agenda') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row items-push">
                    <div class="col-md-12">
                        <!-- Calendar Container -->
                        <div class="js-calendar" id="calendar"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Calendar -->
    </div>
    <!-- END Page Content -->

    <!-- calendar Add Modal -->
    <div class="modal" id="modal-calendar-add" tabindex="-1" role="dialog" aria-labelledby="modal-calendar-add"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-calendar-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">{{ __('lang.add_agenda') }}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form calendar Add -->
                                <form action="{{ route('calendar.create') }}" method="POST" id="calendar-add-form"
                                    novalidate>
                                    @csrf
                                    <input type="hidden" id="add-form-title-hidden" name="add-form-title-hidden"
                                        value="">
                                    <div class="form-group">
                                        <label for="add-form-type">Type de rencontre:</label>
                                        <select class="form-control form-control-alt" id="add-form-type"
                                            name="add-form-type" aria-describedby="add-form-type-error" aria-invalid="false"
                                            style="height: 42px;border: 1px solid #aaa;">
                                            <option value="">Type de Rencontre ...</option>
                                            <option value="rencontre">Rencontre</option>
                                            <option value="suivi">Suivi</option>
                                            <option value="journe_offre">Journe Offre</option>
                                            <option value="rappel">Rappel</option>
                                        </select>
                                        <div id="add-form-type-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-lead">{{ __('lang.lead') }}</label>
                                        <select class="form-control" id="add-form-lead" name="add-form-lead"
                                            style="width: 100%">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-title">{{ __('lang.planification') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-alt" id="meet-step-label">
                                                    {{ __('lang.rencontre') }}
                                                </span>
                                            </div>
                                            <input type="text" class="form-control form-control-alt"
                                                id="add-form-title" name="add-form-title"
                                                placeholder="{{ __('lang.planification') }}.."
                                                aria-describedby="add-form-title-error" aria-invalid="false">
                                            <div id="add-form-title-error" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    {{-- @start --}}
                                    <div class="form-group">
                                        <label for="add-form-title-bis">{{ __('lang.planification') }}</label>
                                        <input type="text" class="form-control form-control-alt"
                                            id="add-form-title-bis" name="add-form-title-bis"
                                            placeholder="{{ __('lang.planification') }}.."
                                            aria-describedby="add-form-title-bis-error" aria-invalid="false">
                                        <div id="add-form-title-bis-error" class="invalid-feedback"></div>
                                    </div>
                                    {{-- @end --}}

                                    <div class="form-group">
                                        <label for="add-form-date">{{ __('lang.plan_date') }}</label>
                                        <input type="date" class="form-control form-control-alt" id="add-form-date"
                                            name="add-form-date" placeholder="{{ __('lang.plan_date') }}.."
                                            aria-describedby="add-form-date-error" aria-invalid="false">
                                        <div id="add-form-date-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group form-row">
                                        <div class="col-6">
                                            <label for="add-form-time">{{ __('lang.plan_hour_begin') }}</label>
                                            <input type="time" class="form-control form-control-alt"
                                                id="add-form-time" name="add-form-time"
                                                aria-describedby="add-form-time-error" aria-invalid="false">
                                            <div id="add-form-time-error" class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-6">
                                            <label for="add-form-time_end">{{ __('lang.plan_hour_end') }}</label>
                                            <input readonly type="time" class="form-control form-control-alt"
                                                id="add-form-time_end" name="add-form-time_end"
                                                aria-describedby="add-form-time_end-error" aria-invalid="false">
                                            <div id="add-form-time_end-error" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-description">{{ __('lang.description') }}</label>
                                        <textarea type="text" class="form-control form-control-alt" id="add-form-description" name="add-form-description"
                                            placeholder="Description.." aria-describedby="add-form-description-error" aria-invalid="false"></textarea>
                                        <div id="add-form-description-error" class="invalid-feedback"></div>
                                    </div>
                                    {{-- @new field is here :: --}}
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-alt-primary btn-block"
                                            id="add-calendar-btn"><i
                                                class="fa fa-fw fa-plus mr-1"></i>{{ __('lang.add_agenda') }}</button>
                                    </div>
                                </form>
                                <!-- END Form calendar Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1"
                            data-dismiss="modal">{{ __('lang.close') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END calendar Add Modal -->

    <!-- Calendar Edit Modal -->
    <div class="modal" id="modal-calendar-edit" tabindex="-1" role="dialog" aria-labelledby="modal-calendar-edit"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-calendar-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">{{ __('lang.edit_agenda') }}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form calendar client Edit -->
                                <form action="{{ route('calendar.update') }}" method="POST" id="calendar-edit-form">
                                    @csrf
                                    <input type="hidden" id="edit-form-id" name="edit-form-id">
                                    <div class="form-group">
                                        <label for="edit-form-type">Type de rencontre:</label>
                                        <select class="form-control form-control-alt" id="edit-form-type"
                                            name="edit-form-type" aria-describedby="edit-form-type-error"
                                            aria-invalid="false" style="height: 42px;border: 1px solid #aaa;">
                                            <option value="rencontre">Rencontre</option>
                                            <option value="suivi">Suivi</option>
                                            <option value="journe_offre">Journe Offre</option>
                                            <option value="rappel">Rappel</option>
                                        </select>
                                        <div id="edit-form-type-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-lead">{{ __('lang.lead') }}</label>
                                        <select class="form-control" id="edit-form-lead" name="edit-form-lead"
                                            style="width: 100%">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-title">{{ __('lang.plan_date') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-alt"
                                                    id="meet-step-label-edit">
                                                    {{ __('lang.rencontre') }}
                                                </span>
                                            </div>
                                            <input type="text" class="form-control form-control-alt"
                                                id="edit-form-title" name="edit-form-title"
                                                placeholder="{{ __('lang.planification') }}.."
                                                aria-describedby="edit-form-title-error" aria-invalid="false">
                                            <div id="edit-form-title-error" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    {{-- @start edit --}}
                                    <div class="form-group">
                                        <label for="edit-form-title-bis">{{ __('lang.planification') }}</label>
                                        <input type="text" class="form-control form-control-alt"
                                            id="edit-form-title-bis" name="edit-form-title-bis"
                                            placeholder="{{ __('lang.planification') }}.."
                                            aria-describedby="edit-form-title-bis-error" aria-invalid="false">
                                        <div id="edit-form-title-bis-error" class="invalid-feedback"></div>
                                    </div>
                                    {{-- @end edit --}}

                                    <div class="form-group">
                                        <label for="edit-form-date">{{ __('lang.plan_date') }}</label>
                                        <input type="date" class="form-control form-control-alt" id="edit-form-date"
                                            name="edit-form-date" placeholder="{{ __('lang.plan_date') }}.."
                                            aria-describedby="edit-form-date-error" aria-invalid="false">
                                        <div id="edit-form-date-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group form-row">
                                        <div class="col-6">
                                            <label for="edit-form-time">{{ __('lang.plan_hour_begin') }}</label>
                                            <input type="time" class="form-control form-control-alt"
                                                id="edit-form-time" name="edit-form-time"
                                                aria-describedby="edit-form-time-error" aria-invalid="false">
                                            <div id="edit-form-time-error" class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-6">
                                            <label for="edit-form-time_end">{{ __('lang.plan_hour_end') }}</label>
                                            <input type="time" class="form-control form-control-alt"
                                                id="edit-form-time_end" name="edit-form-time_end"
                                                aria-describedby="edit-form-time_end-error" aria-invalid="false">
                                            <div id="edit-form-time_end-error" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-description">{{ __('lang.description') }}</label>
                                        <textarea type="text" class="form-control form-control-alt" id="edit-form-description"
                                            name="edit-form-description" placeholder="Description.." aria-describedby="edit-form-description-error"
                                            aria-invalid="false"></textarea>
                                        <div id="edit-form-description-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-danger" id="delete-calendar-btn"><i
                                                class="fa fa-fw fa-trash mr-1"></i>{{ __('lang.delete_agenda') }}</button>
                                        <button type="submit" class="btn btn-alt-primary float-right"
                                            id="update-calendar-btn"><i
                                                class="fa fa-fw fa-plus mr-1"></i>{{ __('lang.edit_agenda') }}</button>
                                    </div>
                                </form>
                                <!-- END Form calendar Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary float-left"><a id="voir-calendar-btn"
                                href="" class="text-white">{{ __('lang.view_lead') }}</a></button>
                        <button type="button" class="btn btn-alt-primary mr-1"
                            data-dismiss="modal">{{ __('lang.close') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Calendar Edit Modal -->
@endsection
