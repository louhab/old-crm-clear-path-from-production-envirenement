@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/emailnotifications.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Historique','subTitle' => 'Email Notifications', 'navItems' => ['Admin', 'Email Notifications']])

    <!-- Page Content -->
    <div class="content">

        <!-- emailnotifications Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">
                            {{--<div class="col-3">
                                <select class="form-control dt_search_field" name="ranking">
                                    <option value="">Sélectionnez un ranking</option>
                                    @foreach(\App\Models\Ranking::all() as $ranking)
                                        <option value="{{ $ranking->id }}">{{ $ranking->ranking_label }}</option>
                                    @endforeach
                                </select>
                            </div>--}}
                            <div class="col-4">
                                <input type="text" class="form-control dt_search_field" name="object" placeholder="Object">
                            </div>
                            <div class="col-4">
                                <input type="text" class="form-control dt_search_field" name="message" placeholder="Message">
                            </div>
                            <div class="col-3">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="emailnotifications-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Email Notifications</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary"  id="emailnotification-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter Email Notification</button>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="emailnotifications-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END emailnotifications Table -->




    </div>
    <!-- END Page Content -->

    <!-- emailnotification Add Modal -->
    <div class="modal" id="modal-emailnotification-add" tabindex="-1" role="dialog" aria-labelledby="modal-emailnotification-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-emailnotification-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter Email Notification</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form emailnotification Add -->
                                <form action="" method="POST" onsubmit="return false;" id="emailnotification-add-form">
                                    <div class="form-group">
                                        <label for="add-form-object">Objet</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-object" name="add-form-object" placeholder="Objet.." aria-describedby="add-form-object-error" aria-invalid="false">
                                        <div id="add-form-object-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-message">Message</label>
                                        <textarea type="text" class="form-control form-control-alt" id="add-form-message" name="add-form-message" placeholder="Message.." aria-describedby="add-form-message-error" aria-invalid="false"></textarea>
                                        <div id="add-form-message-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-info btn-block" id="add-emailnotification-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter Email Notification</button>
                                    </div>
                                </form>
                                <!-- END Form emailnotification Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END emailnotification Add Modal -->

    <!-- emailnotification Edit Modal -->
    <div class="modal" id="modal-emailnotification-edit" tabindex="-1" role="dialog" aria-labelledby="modal-emailnotification-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-emailnotification-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Modifier Email Notification</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form emailnotification Add -->
                                <form action="" method="POST" onsubmit="return false;" id="emailnotification-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-object">Objet</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-object" name="edit-form-object" placeholder="Objet.." aria-describedby="edit-form-object-error" aria-invalid="false">
                                        <div id="edit-form-object-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-message">Message</label>
                                        <textarea type="text" class="form-control form-control-alt" id="edit-form-message" name="edit-form-message" placeholder="Message.." aria-describedby="edit-form-message-error" aria-invalid="false"></textarea>
                                        <div id="edit-form-message-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-info btn-block" id="update-emailnotification-btn"><i class="fa fa-fw fa-check mr-1"></i>Modifier Email Notification</button>
                                    </div>
                                </form>
                                <!-- END Form emailnotification Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END emailnotification Edit Modal -->
@endsection
