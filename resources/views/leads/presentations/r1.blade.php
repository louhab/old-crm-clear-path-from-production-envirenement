<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>CLEARPATH - CRM Application</title>
    <meta name="description" content="Clearpath - CRM Application">
    <meta name="author" content="devwmm">
    <meta name="robots" content="noindex, nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    @yield('css_before')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700,900&display=swap">
    <link rel="stylesheet" id="css-main" href="{{ asset('/css/oneui.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('/js/plugins/flag-icon-css/css/flag-icon.min.css') }}">
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('css/component.css') }}" rel="stylesheet">
    @yield('css_after')
</head>
<body>
    <div id="meeting-show"></div>
<!-- END Page Container -->
<!-- JS Plugins -->
<script src="{{ asset('js/plugins/es6-promise/es6-promise.auto.min.js') }}"></script>
<script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<!-- OneUI Core JS -->
<script src="{{ asset('js/oneui.core.min.js') }}"></script>

<!-- Laravel Scaffolding JS -->
<script src="{{ asset('js/oneui.app.min.js') }}"></script>


<script src="{{mix('js/pages/intl-tel.js')}}"></script>
@yield('js_after')
@stack('custom_scripts')
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
