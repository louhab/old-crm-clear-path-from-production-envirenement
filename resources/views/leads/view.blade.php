<?php
$conseiller = \App\Models\User::where('id', $customer_case->customer->lead->conseiller_id)->first();
$support = \App\Models\User::where('id', $customer_case->customer->lead->support_id)->first();
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
?>
@extends('layouts.backend')
@section('css_after')
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    @livewireStyles
    <style>
        .clock-wrapper {
            font-weight: 800;
            font-size: 3.25rem;
        }

        .custom_table tbody {
            display: block;
            max-height: 420px;
            overflow: auto;
        }

        .custom_table thead,
        .custom_table tbody tr {
            display: table;
            width: 100%;
            table-layout: fixed;
        }

        .select2-container {
            width: -webkit-fill-available !important;
        }
    </style>
@endsection
@section('js_after')
    <!-- <script src="{{ asset('js/plugins/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script> -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    @livewireScripts
    <!-- Page JS Code -->
    <script>
        if ($('#phone').length) {
            window.setIntlTel("phone");
        }
        // father_phone
        if ($('#father_phone').length) {
            window.setIntlTel("father_phone");
        }
        if ($('#mother_phone').length) {
            window.setIntlTel("mother_phone");
        }
        $("#toggle-password").on('click', function() {
            //
            let pass = $('input[name="password"]');
            if (pass.attr('type') == 'password') {
                pass.attr('type', 'text')
            } else if (pass.attr('type') == 'text') {
                pass.attr('type', 'password')
            }
            //console.log(pass.attr('type'));
        });
        $("[name='tags[]']").select2();
    </script>
    <script src="{{ asset('js/stopwatch.js') }}"></script>
    <script src="{{ asset('js/pages/leads/view.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', [
        'title' => 'LEAD',
        'subTitle' => $lead->firstname . ' ' . $lead->lastname . ' (' . $lead->program->name . ')',
        'navItems' => ['Admin', 'Edition Client'],
    ])

    <div class="content">
        <div class="row">
            <div class="col-xl-9">


                @if (Session::get('error'))
                    <div class="alert alert-info alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <p class="mb-0">{{ Session::get('error') }}</p>
                    </div>
                @endif

                @livewire('block.info', ['customer_case' => $customer_case, 'errors' => $errors])
                @admin
                    @livewire('block.log.lead-state', ['lead' => $lead])
                    @livewire('block.log.assignment-log', ['lead' => $lead])
                    @livewire('block.call-count', ['lead' => $lead])
                @endadmin
                @livewire('block.deprecated-histocall', ['lead' => $lead])
                @administration_agent_support
                    {{-- @if ($customer_case->program_id == 4)
                            <div class="block block-rounded">
                                <div class="block-content">
                                    <div class="row">
                                        <div class="col-md-6">
                                            @livewire('block.result', ['customer_case' => $customer_case, 'meet' => 'R1'])
                                        </div>
                                        <div class="col-md-6">
                                            @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-Demande de note'])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif --}}
                    @if ($lead->to_office == 'conference')
                        @livewire('block.payment', ['customer_case' => $customer_case, 'payment' => 1])
                    @endif
                @endadministration_agent_support
                <?php
                $user_id = Illuminate\Support\Facades\Auth::id();
                /*$meets = \App\Models\Calendrier::all()->filter(function ($meet) use ($customer_case) {
                    return $meet->customer_id == $customer_case->customer->id && (str_starts_with($meet->title, "Rencontre 1") || str_starts_with($meet->title, "Recontre 1"));
                });*/
                $meets = \App\Models\Calendrier::all()->filter(function ($meet) use ($customer_case) {
                    return $meet->customer_id == $customer_case->customer->id && (str_starts_with($meet->title, 'Recontre 1') || str_starts_with($meet->title, 'Rencontre 1') || str_starts_with($meet->title, str_replace('Rencontre ', 'R', 'Rencontre 1')));
                });
                // dd($meets);
                $conseiller = App\Models\User::where('id', $lead->conseiller_id)->first();
                ?>
                @if ($lead->to_office != 'office')
                    <div class="block block-rounded">
                        <div class="block-header block-header-default">
                            <h3 class="block-title"><i class="fa fa-2x fa-calendar-check me-1"></i> RDV </h3>
                            @statusMeet($lead, $customer_case)
                                <div class="block-options">
                                    <button type="button" class="btn btn-sm btn-alt-primary" id="calendar-table-add">Fixer un
                                        rendez-vous</button>
                                </div>
                            @else
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-fw fa-lock text-danger"></i>
                                    </div>
                                </div>
                            @endstatusMeet
                        </div>
                        @if (count($meets))
                            <div class="block-content" style="padding: 20px;">
                                <table class="table table-sm table-vcenter">
                                    <thead>
                                        <tr class="meets">
                                            <th class="d-none d-sm-table-cell" style="width: 40%;">Rendez-vous</th>
                                            <th class="d-none d-sm-table-cell" style="width: 15%;">Date du rdv</th>
                                            <th class="d-none d-sm-table-cell" style="width: 30%;">Heure du RDV</th>
                                            <th style="width: 20%;">Conseiller</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($meets as $meet)
                                            <tr class="meets">
                                                <td class="font-size-sm">
                                                    <p class="text-muted mb-0">{{ $meet->title }}</p>
                                                </td>
                                                <td class="font-size-sm">
                                                    <p class="text-muted mb-0">{{ $meet->date }}</p>
                                                    <!-- November 28, 2018 08:59 -->
                                                </td>
                                                <td class="font-size-sm">
                                                    <p class="text-muted mb-0">Début:
                                                        {{ \Carbon\Carbon::parse($meet->time)->format('H:i') }}, Fin:
                                                        {{ \Carbon\Carbon::parse($meet->time_end)->format('H:i') }}</p>
                                                </td>
                                                <td class="font-size-sm">
                                                    <p class="text-muted mb-0">
                                                        <strong>{{ $conseiller ? $conseiller->name : 'Compte Supprimer' }}</strong>
                                                    </p>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                @endif
                @php
                    $substep = \App\Models\ProgramSubStep::where('id', 6)->first();
                    if ($customer_case->program_id == 5) {
                        $pid = 1;
                    } else {
                        $pid = $customer_case->program_id;
                    }
                    $docs = $substep
                        ->docs()
                        ->wherePivot('program_id', '=', $pid)
                        ->withPivot('order')
                        ->whereNotIn('name_fr', ['CV', 'Cin verso', 'Cin recto'])
                        ->orderBy('order', 'asc')
                        ->get();
                @endphp
                @management
                    @if ($customer_case->program_id == 1 || $customer_case->program_id == 9)
                        @livewire('block.cnp', ['customer_case' => $customer_case])
                    @endif
                    @livewire('block.media', ['customer_case' => $customer_case])

                    @if ($customer_case->program_id == 4)
                        <div class="block-header block-header-default" style="padding-top: 1.5rem">
                            <h3 class="block-title">Documents utiles</h3>
                        </div>
                        <div class="block-content block" style="border: 1px solid #ccc;">
                            <table class="table table-sm table-vcenter">
                                <tr>
                                    <td class="font-size-sm">Présentation CPC</td>
                                    <td class="font-size-sm"></td>
                                    <td class="font-size-sm" style="width: 40%"><a class="btn btn-sm btn-light"
                                            href="https://drive.google.com/drive/folders/1Thv64rXvjQT4qVuEzH47-2829XaYYspx"
                                            target="_blank">
                                            <i class="fa fa-fw fa-download text-success"></i>
                                        </a></td>
                                </tr>
                                <tr>
                                    <td class="font-size-sm">Des arguments pour convaincre les étudiants</td>
                                    <td class="font-size-sm"></td>
                                    <td class="font-size-sm" style="width: 40%"><a class="btn btn-sm btn-light"
                                            href="https://drive.google.com/file/d/1j9yofMYlSewrAEgLpJHf4tGdBxNt3wV3/view?usp=drivesdk"
                                            target="_blank">
                                            <i class="fa fa-fw fa-download text-success"></i>
                                        </a></td>
                                </tr>
                                <tr style="width: 30%">
                                    <td class="font-size-sm">Processus Programme Etudiant</td>
                                    <td class="font-size-sm"></td>
                                    <td class="font-size-sm" style="width: 40%"><a class="btn btn-sm btn-light"
                                            href="https://docs.google.com/file/d/1v3NRI3N3eEyUNV1UHsfvso6YvoK3QMF5/edit?usp=docslist_api&filetype=mspresentation"
                                            target="_blank">
                                            <i class="fa fa-fw fa-download text-success"></i>
                                        </a></td>
                                </tr>
                                <tr>
                                    <td class="font-size-sm">Script Entretien Conseiller - Client</td>
                                    <td class="font-size-sm"></td>
                                    <td class="font-size-sm" style="width: 40%"><a class="btn btn-sm btn-light"
                                            href="https://drive.google.com/file/d/1QQgQr-YCDSHDf-VAt6sQBdHxXeVnjOZ3/view?usp=drivesdk"
                                            target="_blank">
                                            <i class="fa fa-fw fa-download text-success"></i>
                                        </a></td>
                                </tr>
                            </table>
                        </div>
                    @endif
                @endmanagement
                <?php
                $case_state_3 = App\Models\CaseState::where('sub_step_state_id', 3)
                    ->where('customer_case_id', $customer_case->id)
                    ->first();
                ?>

                @administration_agent_support
                    @if ($lead->to_office != 'conference')
                        @livewire('block.payment', ['customer_case' => $customer_case, 'payment' => 1])
                    @endif
                @endadministration_agent_support
                @php
                    $clear_fields = \App\Models\CustomerClear1000Fields::where('customer_id', $customer_case->customer->id)->first();
                    $visa_requests = \App\Models\CustomerVisaRequest::where('customer_id', $customer_case->customer_id)
                        ->orderBy('id', 'ASC')
                        ->get();
                @endphp
                @if ($customer_case->program_id == 4 && $clear_fields && $clear_fields->visa_request_result == 85)
                    <div class="block block-rounded">
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-6">
                                    @livewire('block.result', ['customer_case' => $customer_case, 'meet' => 'R1'])
                                </div>
                                <div class="col-md-6">
                                    @livewire('elements.comment', ['customer_case' => $customer_case, 'canal' => 'soumission-Demande de note'])
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            <div class="col-xl-3">
                <div class="block block-rounded">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">{{ __('lang.portal_about_the_lead_title') }}</h3>
                    </div>
                    <div class="block-content">
                        <table class="table table-borderless table-vcenter">
                            <tbody>
                                <?php
                                {
                                    $customer = $customer_case->customer()->first();
                                    $program = $customer_case->program()->first();
                                    $step = $customer_case->step()->first();
                                    $caseDate = $customer_case->created_at;
                                    // Note, this gives you a timestamp, i.e. seconds since the Epoch.
                                    $caseTime = strtotime($caseDate);

                                    // This difference is in seconds.
                                    $difference = time() - $caseTime;
                            ?>
                                <tr>
                                    <td>
                                        <i class="fa fa-fw fa-calendar mr-1"></i>
                                        <strong>{{ __('lang.portal_personal_infos_programm') }}</strong>
                                        {{ \App\Models\Program::where('id', $lead->program_id)->first()->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-fw fa-business-time mr-1"></i>
                                        <strong>{{ __('lang.portal_about_the_lead_reg_date') }}</strong>
                                        {{ $caseDate->format('d M Y') }}
                                        <!-- 13 Juin 2021 -->
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <i class="fa fa-fw fa-user mr-1"></i>
                                        <strong>{{ __('lang.portal_about_the_lead_assigned_to') }}</strong>
                                        <em>{{ $conseiller ? $conseiller->name : 'Compte Supprimer' }}</em>
                                    </td>
                                </tr>
                                @administration
                                    <tr>
                                        <td>
                                            @if (is_null($support))
                                                <i class="fa fa-fw fa-user mr-1"></i> <strong>Support </strong> -
                                            @else
                                                <i class="fa fa-fw fa-user mr-1"></i> <strong>Support
                                                </strong>{{ $support->name }}
                                            @endif
                                        </td>
                                    </tr>
                                @endadministration
                                <tr>
                                    <?php
                                    $leadStatus = App\Models\LeadStatus::where('id', $lead->lead_status_id)->first();
                                    //dd($lead);
                                    $badge = 'danger';
                                    if ($lead->lead_status_id == 1) {
                                        $badge = 'success';
                                    }
                                    if ($lead->lead_status_id == 3) {
                                        $badge = 'secondary';
                                    }
                                    if ($lead->lead_status_id == 4) {
                                        $badge = 'info';
                                    }
                                    if ($lead->lead_status_id == 5) {
                                        $badge = 'warning';
                                    }
                                    if (!$leadStatus) {
                                        $badge = 'info';
                                    }
                                    ?>
                                    <td>
                                        <i class="fa fa-fw fa-satellite mr-1"></i>
                                        <strong>{{ __('lang.portal_about_the_lead_status') }}</strong> <span
                                            class="badge badge-{{ $badge }}">{{ $leadStatus ? $leadStatus->status_label_fr : 'N/A' }}
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Start Tags -->
                <div class="block block-rounded js-ecom-div-cart d-none d-xl-block" id="comments-block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Tags</h3>
                    </div>
                    <div class="block-content" style="padding: 10px;">
                        <div id="main-comment-container" data-comment-id="{{ $customer_case->id }}"
                            class="block-content scroll" style="height: 100%;max-height: 40em;">
                            <div class="row items-push">
                                <div class="col-md-12">
                                    <form action="{{ route('store-tags', $customer_case->customer->lead->id) }}"
                                        method="POST">
                                        @csrf
                                        <div class="form-group">
                                            {{ Form::select('tags[]', \App\Models\Tag::pluck('tag_name', 'id'), \App\Models\Lead::find($customer_case->customer->lead->id)->tags->pluck('id'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('tags') ? ' is-invalid' : (isset($errors) && $errors->has('tags') ? 'true' : 'false')), 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '']) }}
                                        </div>
                                        <div class="form-group">
                                            <button type="submit"
                                                class="btn btn-outline-danger btn-block">{{ __('lang.portal_about_the_lead_save_button') }}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Tags -->
                @livewire('block.comment', ['customer_case' => $customer_case])
                @livewire('block.email', ['customer_case' => $customer_case])
                @livewire('block.message', ['customer_case' => $customer_case])

                <!-- Start regenrate pdfs-->
                @if (count($docs))
                    @if (!empty($customer_case->customer->getFirstMedia($docs[1]->collection_name)))
                        <div class="block block-rounded js-ecom-div-cart d-none d-xl-block" id="regen-block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">{{ __('lang.portal_doc_regeneration') }}</h3>
                            </div>
                            <div class="block-content" style="padding: 10px;">
                                <div id="main-comment-container" data-comment-id="{{ $customer_case->id }}"
                                    class="block-content scroll" style="height: 100%;max-height: 40em;">
                                    <div class="row items-push">
                                        <div class="col-md-12">
                                            <form action="{{ route('regen-docs', $customer_case->customer->lead->id) }}"
                                                method="POST">
                                                @csrf
                                                <div class="form-group">
                                                    <?php
                                                    $currency = \App\Models\Currency::where('id', $customer_case->customer->currency_id)->first();
                                                    $currency_name = $currency ? $currency->name : null;
                                                    $lang = ['en' => __('lang.english'), 'fr' => __('lang.french')];
                                                    $lead_lang = array_values($lang)[array_search($lead->lang, array_keys($lang))];
                                                    ?>
                                                    <i class="si si-flag mr-1"></i>
                                                    <strong>{{ __('lang.portal_doc_regeneration_language') }} : </strong>
                                                    {{ $lead_lang }}
                                                </div>
                                                <div class="form-group">
                                                    <i class="fa fa-dollar-sign mr-1"></i>
                                                    <strong>{{ __('lang.portal_doc_regeneration_currency') }} : </strong>
                                                    {{ $currency_name }}
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit"
                                                        class="btn btn-outline-danger btn-block">{{ __('lang.portal_doc_regeneration_button') }}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
                <!-- End regenrate pdfs-->
            </div>
        </div>
    </div>

    <!-- histocall Customer Add Modal -->
    <div class="modal" id="modal-histocall-customer-add" tabindex="-1" role="dialog"
        aria-labelledby="modal-histocall-customer-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-histocall-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">{{ __('lang.portal_histocall_modal_title') }}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form histocall customer Add -->
                                <form action="" method="POST" onsubmit="return false;"
                                    id="histocall-add-customer-form">
                                    <input type="hidden" name="add-form-lead_id" value="{{ $lead->id }}">
                                    <div class="row">
                                        <div class="col-md-12 input-wrapper">
                                            <div class="buttons-wrapper">
                                                <button class="btn btn-alt-primary" id="start-cronometer">Démarrer le
                                                    chrono</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row  justify-content-md-center">
                                        <input type="hidden" name="add-form-duration" id="add-form-duration">
                                        <div id="timer" class="col-md-10" style="display: none;">
                                            <p style="line-height: 24px;font-size: 26px;width: 100%;">
                                                <strong>{{ __('lang.portal_histocall_modal_phone') }} :</strong>
                                                {{ $customer->phone }}
                                            </p>
                                            <div class="text-center clock-wrapper h1">
                                                <span class="hours">00</span>
                                                <span class="dots">:</span>
                                                <span class="minutes">00</span>
                                                <span class="dots">:</span>
                                                <span class="seconds">00</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <div class="buttons-wrapper">
                                            <button class="btn btn-alt-primary" id="resume-timer"
                                                style="display: none;"><i class="fa fa-fw fa-play"></i>Démarrer</button>
                                            <button class="btn btn-alt-secondary" id="stop-timer"
                                                style="display: none;"><i class="fa fa-fw fa-pause"></i>Pause</button>
                                            <button class="btn btn-alt-secondary" id="reset-timer"
                                                style="display: none;"><i
                                                    class="fa fa-fw fa-trash-restore"></i>Reset</button>
                                        </div>
                                    </div>
                                    <div class="form-group mt-4">
                                        <!-- <label for="add-form-call_descr">Description</label> -->
                                        <textarea type="text" class="form-control form-control-alt" id="add-form-call_descr" name="add-form-call_descr"
                                            placeholder="Notes.." aria-describedby="add-form-call_descr-error" aria-invalid="false"></textarea>
                                        <div id="add-form-call_descr-error" class="invalid-feedback"></div>
                                    </div>
                                    @admin
                                        <div class="form-group">
                                            @php
                                                $stats_label = $lang === 'fr' ? 'status_label_fr' : 'status_label_en';
                                                $statusAll = \App\Models\LeadStatus::all()
                                                    ->pluck($stats_label, 'id')
                                                    ->toArray();
                                                $statusAll['No_Chnage'] = 'No Change';
                                                $statusAll['Rappel'] = 'Rappel';
                                                // dd($statusAll);
                                            @endphp
                                            {{ Form::select('lead_status', $statusAll, null, ['id' => '_lead_status', 'class' => 'form-control form-control-alt', 'aria-describedby' => 'lead_status-error', 'aria-invalid' => '', 'placeholder' => __('lang.portal_histocall_modal_choose_status')]) }}
                                        </div>
                                    @else
                                        <div class="form-group">
                                            @php
                                                $stats_label = $lang === 'fr' ? 'status_label_fr' : 'status_label_en';
                                                $statusAll = \App\Models\LeadStatus::where('visible', 1)
                                                    ->get()
                                                    ->pluck($stats_label, 'id')
                                                    ->toArray();
                                                if (in_array($lead->lead_status_id, [5, 11, 13, 14])) {
                                                    $statusAll = \App\Models\LeadStatus::whereIn('id', [13, 14])
                                                        ->get()
                                                        ->pluck($stats_label, 'id')
                                                        ->toArray();
                                                }
                                                if ($lead->lead_status_id == 1 || is_null($lead->lead_status_id)) {
                                                    $statusAll = \App\Models\LeadStatus::whereIn('id', [1, 2, 3, 6])
                                                        ->get()
                                                        ->pluck($stats_label, 'id')
                                                        ->toArray();
                                                }
                                                $statusAll['No_Chnage'] = 'No Change';
                                                $statusAll['Rappel'] = 'Rappel';
                                                if (Auth::user()->isManager() || Auth::user()->isConseiller() || Auth::user()->isAgent()) {
                                                    unset($statusAll[1]);
                                                }
                                                // dd($statusAll);
                                            @endphp
                                            {{ Form::select('lead_status', $statusAll, null, ['id' => '_lead_status', 'class' => 'form-control form-control-alt', 'aria-describedby' => 'lead_status-error', 'aria-invalid' => '', 'placeholder' => __('lang.portal_histocall_modal_choose_status')]) }}
                                        </div>
                                    @endadmin


                                </form>
                                <!-- END Form histocall Add -->
                            </div>
                        </div>
                    </div>
                    {{-- <div class="block-content block-content-full text-right border-top">
                        <button type="button" onclick="startStop()" id="add-histocall-customer-btn" class="btn btn-alt-primary mr-1">Ajouter appel</button>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
    <!-- END histocall Add Modal -->

    <!-- calendar Add Modal -->
    <div class="modal" id="modal-calendar-add" tabindex="-1" role="dialog" aria-labelledby="modal-calendar-add"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-calendar-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter un rdv</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form calendar Add -->
                                <form action="{{ route('calendar.create') }}" method="POST" id="calendar-add-form">
                                    @csrf
                                    <div class="form-group">
                                        @if (
                                            !empty($customer_case->customer->getFirstMedia($docs[0]->collection_name)) &&
                                                in_array($customer_case->customer->lead->lead_status_id, [11, 7, 5]))
                                            <label for="add-form-date">Votre RDV</label>
                                            <select id="add-form-title" name="add-form-title"
                                                class="form-control form-control-alt">
                                                <option
                                                    value="R2 : {{ $lead->lastname }} {{ $lead->lastname }} - {{ $conseiller ? $conseiller->name : 'Compte Supprimer' }}">
                                                    RDV2</option>
                                                <option
                                                    value="R3 : {{ $lead->lastname }} {{ $lead->lastname }} - {{ $conseiller ? $conseiller->name : 'Compte Supprimer' }}">
                                                    RDV3</option>
                                                <option
                                                    value="R4 : {{ $lead->lastname }} {{ $lead->lastname }} - {{ $conseiller ? $conseiller->name : 'Compte Supprimer' }}">
                                                    RDV4</option>
                                                <option
                                                    value="R5 : {{ $lead->lastname }} {{ $lead->lastname }} - {{ $conseiller ? $conseiller->name : 'Compte Supprimer' }}">
                                                    RDV5</option>
                                                <option
                                                    value="R6 : {{ $lead->lastname }} {{ $lead->lastname }} - {{ $conseiller ? $conseiller->name : 'Compte Supprimer' }}">
                                                    RDV6</option>
                                            </select>
                                            <div id="add-form-date-error" class="invalid-feedback"></div>
                                        @else
                                            <h3>R1 : {{ $lead->lastname }} {{ $lead->lastname }} -
                                                {{ $conseiller ? $conseiller->name : 'Compte Supprimer' }}
                                            </h3>
                                            <input type="hidden"
                                                value="R1 : {{ $lead->lastname }} {{ $lead->lastname }} - {{ $conseiller ? $conseiller->name : 'Compte Supprimer' }}"
                                                id="add-form-title" name="add-form-title">
                                        @endif
                                        <input type="hidden" value="{{ $customer->id }}" id="add-form-lead"
                                            name="add-form-lead">
                                        <input type="hidden" value="rencontre" id="add-form-type" name="add-form-type">
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-date">Date Rencontre</label>
                                        <input required type="date" class="form-control form-control-alt"
                                            id="add-form-date" name="add-form-date" placeholder="Date Planification.."
                                            aria-describedby="add-form-date-error" aria-invalid="false">
                                        <div id="add-form-date-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group form-row">
                                        <div class="col-6">
                                            <label for="add-form-time">Heure Début Rencontre</label>
                                            <input required type="time" class="form-control form-control-alt"
                                                id="add-form-time" name="add-form-time"
                                                aria-describedby="add-form-time-error" aria-invalid="false">
                                            <div id="add-form-time-error" class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-6">
                                            <label for="add-form-time_end">Heure Fin Rencontre</label>
                                            <input required type="time" class="form-control form-control-alt"
                                                id="add-form-time_end" name="add-form-time_end"
                                                aria-describedby="add-form-time_end-error" aria-invalid="false" readonly>
                                            <div id="add-form-time_end-error" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!-- <button type="submit" class="btn btn-alt-primary btn-block" id="add-calendar-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter Planification</button> -->
                                    </div>
                                </form>
                                <!-- END Form calendar Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="submit" form="calendar-add-form" class="btn btn-alt-primary mr-1"
                            id="add-calendar-btn">Ajouter Rencontre</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END calendar Add Modal -->
    <!-- payment Add Modal -->
    <div class="modal" id="modal-payment-add" tabindex="-1" role="dialog" aria-labelledby="modal-payment-add"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">{{ __('lang.portal_payment_method') }}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form Payment Add -->
                                <form action="" method="POST" onsubmit="return false;" id="payment-form">
                                    <div class="form-group">
                                        <div style="height: 20px;"></div>
                                        <label for="method-select">{{ __('lang.portal_select_payment_method') }}</label>
                                        <select class="form-control" id="method-select" name="method-select"
                                            style="width: 100%">
                                        </select>
                                    </div>
                                </form>
                                <div style="padding: 0 20px;">
                                    @livewire('button.paiement', ['customer_case' => $customer_case, 'collection' => 'Paiement 1'])
                                </div>
                                <!-- END Form histocall Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1 case-state-update" data-method="1"
                            data-case="{{ $case_state_3->id }}"
                            data-dismiss="modal">{{ __('lang.portal_payment_method_pay') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Payment Add Modal -->
    <form id="delete-form" action="{{ route('lead-delete-media', ['lead' => $lead, 'collectionName' => 'CVs']) }}"
        method="POST" style="display: none;">
        @csrf
    </form>
    <form id="delete-cin-form" action="{{ route('lead-delete-media', ['lead' => $lead, 'collectionName' => 'CINs']) }}"
        method="POST" style="display: none;">
        @csrf
    </form>
    <!-- END Page Content -->
    @include('billing.modals.services')
@endsection
