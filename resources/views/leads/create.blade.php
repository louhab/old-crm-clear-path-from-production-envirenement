@extends('layouts.backend')

@section('js_after')
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/leads/create.js') }}"></script>
    <script>
        window.setIntlTel("phone");
        var input = document.getElementById('phone');
        input.oninvalid = function(event) {
            event.target.setCustomValidity('Format : +212650123456');
        }

        $("[name='tags[]']").select2();

        // program_field
        // Domaines d'emplois,
        // Domaines d'études,
        $('select[name="program"]').on("change", function() {
            let selected = this.value;
            if (selected == 4) {
                // console.log(selected)
                $('label[for="program_field"]').text("Domaines d'études");
                $('select[name="program_field"] option:first-child').text("Domaines d'études");
            } else {
                $('label[for="program_field"]').text("Domaines d'emplois");
                $('select[name="program_field"] option:first-child').text("Domaines d'emplois");
            }
        });
    </script>
@endsection

@section('css_after')
    <!-- Styles -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Lato:400,500,600,700&display=swap');

        /**{
                                                                                                margin: 0;
                                                                                                padding: 0;
                                                                                                box-sizing: border-box;
                                                                                                font-family: 'Lato', sans-serif;
                                                                                            }
                                                                                            html,body{
                                                                                                display: grid;
                                                                                                height: 100%;
                                                                                                place-items: center;
                                                                                                background: #0069d9;
                                                                                                font-family: 'Lato', sans-serif;
                                                                                            }*/
        .wrapper {
            display: inline-flex;
            height: 80px;
            width: 100%;
            align-items: center;
            justify-content: space-evenly;
            border-radius: 5px;
            padding: 20px 0;
        }

        .wrapper .option {
            background: #fff;
            height: 100%;
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: space-evenly;
            margin-right: 10px;
            border-radius: 5px;
            cursor: pointer;
            padding: 0 10px;
            border: 2px solid lightgrey;
            transition: all 0.3s ease;
        }

        .wrapper .option .dot {
            height: 20px;
            width: 20px;
            background: #d9d9d9;
            border-radius: 50%;
            position: relative;
        }

        .wrapper .option .dot::before {
            position: absolute;
            content: "";
            top: 4px;
            left: 4px;
            width: 12px;
            height: 12px;
            background: #d90028;
            border-radius: 50%;
            opacity: 0;
            transform: scale(1.5);
            transition: all 0.3s ease;
        }

        input[type="radio"] {
            display: none;
        }


        #option-1:checked:checked~.option-1,
        #option-2:checked:checked~.option-2 {
            border-color: #d90028;
            background: #d90019;
        }

        #option-1:checked:checked~.option-1 .dot,
        #option-2:checked:checked~.option-2 .dot {
            background: #fff;
        }

        #option-1:checked:checked~.option-1 .dot::before,
        #option-2:checked:checked~.option-2 .dot::before {
            opacity: 1;
            transform: scale(1);
        }

        .wrapper .option span {
            font-size: 17px;
            color: #808080;
        }

        #option-1:checked:checked~.option-1 span,
        #option-2:checked:checked~.option-2 span {
            color: #fff;
        }

        /* Smartphones (portrait) ----------- */
        @media (max-width : 400px) {

            /* Styles */
            .wrapper {
                flex-direction: column;
            }
        }

        @media (max-width : 1050px) {

            /* Styles */
            .wrapper {
                flex-direction: column;
            }
        }
    </style>
@endsection

@section('content')
    @include('layouts.partials.hero', [
        'title' => 'Leads',
        'subTitle' => 'créer',
        'navItems' => ['Leads', 'créer'],
    ])

    <!-- Page Content -->
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if ($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les
                                informations !</a>
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        <!-- Lead Create Form -->
        <form action="{{ route('lead-store') }}" method="POST" id="lead-form" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="block block-rounded">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Informations personnelles</h3>
                        </div>
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="firstname">Prénom</label>
                                        <span class="text-danger">*</span>
                                        <input type="text"
                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('firstname') ? ' is-invalid' : '' }}"
                                            id="firstname" name="firstname" value="{{ old('firstname') }}"
                                            placeholder="Prénom.." aria-describedby="firstname-error"
                                            aria-invalid="{{ isset($errors) && $errors->has('firstname') ? 'true' : 'false' }}">
                                        <div id="firstname-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('firstname') ? $errors->first('firstname') : '' }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="lastname">Nom</label>
                                        <span class="text-danger">*</span>
                                        <input type="text"
                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('lastname') ? ' is-invalid' : '' }}"
                                            id="lastname" name="lastname" value="{{ old('lastname') }}"
                                            placeholder="Nom.." aria-describedby="lastname-error"
                                            aria-invalid="{{ isset($errors) && $errors->has('lastname') ? 'true' : 'false' }}">
                                        <div id="lastname-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('lastname') ? $errors->first('lastname') : '' }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <span class="text-danger">*</span>
                                        <input type="email"
                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('email') ? ' is-invalid' : '' }}"
                                            id="email" name="email" value="{{ old('email') }}" placeholder="Email.."
                                            aria-describedby="email-error"
                                            aria-invalid="{{ isset($errors) && $errors->has('email') ? 'true' : 'false' }}">
                                        <div id="email-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('email') ? $errors->first('email') : '' }}
                                        </div>
                                    </div>
                                    {{-- pattern="(\+212)[9-1]([ \-_/]*)(\d[ \-_/]*){8}" title="format: +212 650-123456" --}}
                                    <div class="form-group">
                                        <label for="phone">Téléphone</label><br />
                                        <input type="text"
                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('phone') ? ' is-invalid' : '' }}"
                                            id="phone" name="phone" value="{{ old('phone') }}"
                                            aria-describedby="phone-error"
                                            aria-invalid="{{ isset($errors) && $errors->has('phone') ? 'true' : 'false' }}">
                                        <div id="phone-error" class="text-danger">
                                            {{ isset($errors) && $errors->has('phone') ? $errors->first('phone') : '' }}
                                        </div>
                                        <span id="valid-msg" class="text-success" style="display:none;">✓ Valid</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="country">Pays</label>
                                        <span class="text-danger">*</span>
                                        {{ Form::select('country', \App\Models\Country::pluck('name_fr_fr', 'id'), old('country'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('country') ? ' is-invalid' : (isset($errors) && $errors->has('country') ? 'true' : 'false')), 'aria-describedby' => 'country-error', 'placeholder' => 'Pays..', 'aria-invalid' => '']) }}
                                        <div id="country-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('country') ? $errors->first('country') : '' }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="city">Ville</label>
                                        <span class="text-danger">*</span>
                                        <input type="text"
                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('city') ? ' is-invalid' : '' }}"
                                            id="city" name="city" value="{{ old('city') }}"
                                            placeholder="Ville.." aria-describedby="city-error"
                                            aria-invalid="{{ isset($errors) && $errors->has('city') ? 'true' : 'false' }}">
                                        <div id="city-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('city') ? $errors->first('city') : '' }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('lang.Rencontres_1') }} :</label>

                                        <div class="wrapper">
                                            <input type="radio" name="select" id="option-1" value="office" checked>
                                            <input type="radio" name="select" id="option-2" value="conference">
                                            <label for="option-1" class="option option-1">
                                                <div class="dot"></div>
                                                <span>{{ __('lang.Pass_au_bureau') }}</span>
                                            </label>
                                            <label for="option-2" class="option option-2">
                                                <div class="dot"></div>
                                                <span>{{ __('lang.Conference_video') }}</span>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="program">Programme</label>
                                        <span class="text-danger">*</span>
                                        {{ Form::select('program', \App\Models\Program::pluck('name', 'id'), old('program'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('program') ? ' is-invalid' : (isset($errors) && $errors->has('program') ? 'true' : 'false')), 'aria-describedby' => 'program-error', 'aria-invalid' => '']) }}
                                        <div id="program-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('program') ? $errors->first('program') : '' }}
                                        </div>
                                    </div>
                                    @php
                                        $vals = \App\Models\ProgramField::where('visible', 1)
                                            ->get()
                                            ->groupBy('category');
                                    @endphp
                                    <div class="form-group">
                                        <label for="program_field">{{ __('lang.job_fields') }} :</label>
                                        <span class="text-danger">*</span>
                                        <select
                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('program_field') ? ' is-invalid' : '' }}"
                                            id="program_field" name="program_field"
                                            aria-describedby="program_field-error"
                                            aria-invalid="{{ isset($errors) && $errors->has('program_field') ? 'true' : 'false' }}">
                                            <option selected disabled value=>{{ __('lang.job_fields') }}</option>
                                            @foreach ($vals as $key => $value)
                                                <optgroup label="{{ $key }}">
                                                    @foreach ($value as $sub_key => $sub_value)
                                                        <option
                                                            {{ old('program_field') == $sub_value->id ? 'selected' : '' }}
                                                            value={{ $sub_value->id }}>{{ $sub_value->label_fr }}
                                                        </option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                        <div id="program_field-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('program_field') ? $errors->first('program_field') : '' }}
                                        </div>
                                    </div>

                                    @administration
                                        <div class="form-group">
                                            <label for="program">Conseiller</label>
                                            <span class="text-danger">*</span>
                                            @php $user_id = Illuminate\Support\Facades\Auth::id(); @endphp
                                            {{ Form::select('conseiller',\App\Models\User::whereIn('role_id', [3, 2])->where('status', 1)->pluck('name', 'id'),old('conseiller'),['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('conseiller') ? ' is-invalid' : (isset($errors) && $errors->has('conseiller') ? 'true' : 'false')), 'aria-describedby' => 'conseiller-error', 'aria-invalid' => '', 'placeholder' => 'Conseiller..']) }}
                                            <div id="program-error" class="invalid-feedback">
                                                {{ isset($errors) && $errors->has('conseiller') ? $errors->first('conseiller') : '' }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="english">Status Lead</label>
                                            {{ Form::select('lead_status', \App\Models\LeadStatus::where('visible', 1)->pluck('status_label_fr', 'id'), old('lead_status'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('lead_status') ? ' is-invalid' : (isset($errors) && $errors->has('lead_status') ? 'true' : 'false')), 'aria-describedby' => 'lead_status-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner status...']) }}
                                            <div id="english-error" class="invalid-feedback">
                                                {{ isset($errors) && $errors->has('lead_status') ? $errors->first('lead_status') : '' }}
                                            </div>
                                        </div>
                                    @endadministration

                                    <div class="form-group">
                                        <label for="campaign">Canal</label>
                                        <span class="text-danger">*</span>
                                        {{ Form::select('campaign', \App\Models\LeadFormMarketingCampaign::where('visible', 1)->pluck('campaign_name', 'id'), old('campaign'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('campaign') ? ' is-invalid' : (isset($errors) && $errors->has('campaign') ? 'true' : 'false')), 'aria-describedby' => 'campaign-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner Canal...']) }}
                                        <div id="campaign-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('campaign') ? $errors->first('campaign') : '' }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="influencer">Influenceur</label>
                                        <span class="text-danger">*</span>
                                        <select
                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('influencer') ? ' is-invalid' : '' }}"
                                            name="influencer" data-old="{{ old('influencer') }}"
                                            aria-describedby="influencer-error"
                                            aria-invalid="{{ isset($errors) && $errors->has('influencer') ? 'true' : 'false' }}">
                                        </select>
                                        <div id="influencer-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('influencer') ? $errors->first('influencer') : '' }}
                                        </div>
                                    </div>
                                    <div class="form-group"
                                        @if (!isset($errors) || !$errors->has('custom-reference')) style="display: none;" @endif>
                                        <label for="custom-reference">Reference Influenceur</label>
                                        <span class="text-danger">*</span>
                                        <input type="text"
                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('custom-reference') ? ' is-invalid' : '' }}"
                                            id="custom-reference" name="custom-reference"
                                            value="{{ old('custom-reference') }}"
                                            placeholder="Ajouter Nouveau Reference.."
                                            aria-describedby="custom-reference-error"
                                            aria-invalid="{{ isset($errors) && $errors->has('custom-reference') ? 'true' : 'false' }}">
                                        <div id="custom-reference-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('custom-reference') ? $errors->first('custom-reference') : '' }}
                                        </div>
                                    </div>
                                    {{-- Reference --}}
                                    <div class="form-group"
                                        @if (!isset($errors) || !$errors->has('reference')) style="display: none;" @endif>
                                        <div class="form-group">
                                            <label for="reference">Reference</label>
                                            <span class="text-danger">*</span>
                                            <input type="text"
                                                class="form-control form-control-alt{{ isset($errors) && $errors->has('reference') ? ' is-invalid' : '' }}"
                                                id="reference" name="reference" value="{{ old('reference') }}"
                                                placeholder="Reference.." aria-describedby="reference-error"
                                                aria-invalid="{{ isset($errors) && $errors->has('reference') ? 'true' : 'false' }}">
                                            <div id="reference-error" class="invalid-feedback">
                                                {{ isset($errors) && $errors->has('reference') ? $errors->first('reference') : '' }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="currency_id">Devise</label>
                                        <span class="text-danger">*</span>
                                        {{ Form::select('currency_id', \App\Models\Currency::pluck('iso', 'id'), old('currency_id'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('currency_id') ? ' is-invalid' : (isset($errors) && $errors->has('currency_id') ? 'true' : 'false')), 'placeholder' => 'Devise', 'aria-describedby' => 'currency_id-error', 'aria-invalid' => '']) }}
                                        <div id="currency_id-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('currency_id') ? $errors->first('currency_id') : '' }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="lang">Langue</label>
                                        <span class="text-danger">*</span>
                                        {{ Form::select('lang', ['en' => __('lang.english'), 'fr' => __('lang.french')], old('lang'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('lang') ? ' is-invalid' : (isset($errors) && $errors->has('lang') ? 'true' : 'false')), 'placeholder' => 'Language', 'aria-describedby' => 'lang-error', 'aria-invalid' => '']) }}
                                        <div id="lang-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('lang') ? $errors->first('lang') : '' }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="english">Mots Clés</label>
                                        {{ Form::select('tags[]', \App\Models\Tag::pluck('tag_name', 'id'), old('tags'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('tags') ? ' is-invalid' : (isset($errors) && $errors->has('tags') ? 'true' : 'false')), 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '']) }}
                                    </div>

                                </div>
                            </div>




                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-lg btn-alt-primary" id="update-lead-btn"><i
                                class="fa fa-fw fa-save mr-1"></i>Enregistrer</button>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </form>
        <!-- END Lead Create Form -->
    </div>
    <!-- END Page Content -->
@endsection
