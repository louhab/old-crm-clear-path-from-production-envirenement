@extends('layouts.getintouch')

@section('js_after')
    <script>
        window.setIntlTel("phone");
    </script>
    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/getintouch.js') }}"></script>
    <!--<script>
        $(function(){
            setTimeout(function(){ window.top.location.href = "https://clearpathcanada.ca/?subscription=done" }, 3000);
        });
    </script>-->
@endsection
@section('content')
    <div class="content content-boxed">
        <div class="logo" style="margin-bottom: 30px;">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <img src="{{ asset("/img/logo.png") }}" style="width: 220px;">
            </div>
        </div>
        <div class="block block-rounded">
            <div class="block-content">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8" style="padding-top: 30px;padding-bottom: 30px;">
                        <div class="alert-status" style="padding-top: 0;padding-bottom: 0;">
                            @if (Session::get('status'))
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6" style="text-align: center">
                                        <div class="alert alert-success alert-dismissable" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            {{ Session::get('status') }}
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                @php Session::forget('status'); @endphp
                            @endif
                        </div>
                        <div class="row no-gutters justify-content-center">
                              @if (session()->get('locale')=='en')
                             <p>
                                Thank you for your registration,<br>
Your request has been registered.<br>
One of our agents will contact you as soon as possible, we invite you to visit our website for more information.<br>
                             </p>
                              @else
                                  <p>Merci pour votre inscription,<br>
Votre demande a été prise en compte.<br>
Un de nos agents vous contactera dans le plus bref délai, nous vous invitons à visiter notre site Internet, pour obtenir plus de renseignements<br></p>
                                  
                                  
                              @endif
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <div><a href="https://clearpathcanada.ca/" class="btn btn-lg btn-alt-primary btn-block" target="_self">> Vers page d'accueil</a></div>
            </div>
        </div>
    </div>
@endsection
