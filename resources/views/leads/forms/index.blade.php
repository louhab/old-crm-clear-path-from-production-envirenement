@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/leadsforms.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', [
        'title' => 'Formulaires',
        'subTitle' => 'leads',
        'navItems' => ['Admin', 'Forms'],
    ])

    <!-- Page Content -->
    <div class="content">
        <!-- leadsforms Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <form id="search-form" name="search-form" action="" method="post" onsubmit="return false">
                            <div class="form-group form-row">
                                <div class="col-3">
                                    <select class="form-control dt_search_field" name="campaign-type">
                                        <option value="">Sélectionnez type campagne</option>
                                        @foreach (\App\Models\LeadFormMarketingCampaign::all() as $campaign)
                                            <option value="{{ $campaign->id }}">{{ $campaign->campaign_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-2">
                                    <button type="button" class="btn btn-alt-primary btn-block submit-search"
                                        id="search-btn"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                                </div>
                                <div class="col-1">
                                    <button type="button" class="btn btn-alt-danger btn-block clear-search"
                                        id="clear-search-btn"><i class="fa fa-fw fa-redo mr-1"></i></button>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="leadsforms-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Formulaires des leads</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary" id="formsleads-table-add"><i
                                class="fa fa-fw fa-plus mr-1"></i>Ajouter formulaire</button>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="leadsforms-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END leadsforms Table -->




    </div>
    <!-- END Page Content -->

    <!-- leadsforms Lead Add Modal -->
    <div class="modal" id="modal-leadsforms-add" tabindex="-1" role="dialog" aria-labelledby="modal-leadsforms-add"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-leadsforms-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter formulaire</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form leadsforms lead Add -->
                                <form action="" method="POST" onsubmit="return false;" id="leadsforms-add-form">
                                    <div class="form-group">
                                        <label for="add-form-title">Titre</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="add-form-title"
                                            name="add-form-title" aria-describedby="add-form-title-error"
                                            aria-invalid="false" placeholder="Titre..">
                                        <div id="add-form-title-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-campaign">Campagne</label>
                                        <select class="form-control form-control-alt" id="add-form-campaign"
                                            name="add-form-campaign" aria-describedby="add-form-campaign-error"
                                            aria-invalid="false">
                                            <option value="">Sélectionnez type campagne</option>
                                            @foreach (\App\Models\LeadFormMarketingCampaign::all() as $campaign)
                                                <option value="{{ $campaign->id }}">{{ $campaign->campaign_name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-campaign-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group" id="add-form-is-for-commercial">
                                        <label class="d-block">Pour un Commercial</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="add-form-is-not-for-commercial" value="no"
                                                name="add-form-is-for-commercial" checked="">
                                            <label class="custom-control-label"
                                                for="add-form-is-not-for-commercial">Non</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="add-form-is-for-the-commercial" value="yes"
                                                name="add-form-is-for-commercial">
                                            <label class="custom-control-label"
                                                for="add-form-is-for-the-commercial">Oui</label>
                                        </div>
                                    </div>


                                    <div class="form-group" id="add-form-commercial" style="display: none;">
                                        <label for="add-form-commercial">Commerciaux</label>
                                        <select class="form-control form-control-alt" id="add-form-commercial"
                                            name="add-form-commercial" aria-describedby="add-form-commercial-error"
                                            aria-invalid="false">
                                            <option value="">Sélectionnez un Commercial</option>
                                            @foreach (\App\Models\User::whereIn('role_id', [2, 3])->get() as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-commercial-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="d-block">Langue par défault</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="add-form-lang-browser"
                                                value="" name="add-form-lang" checked="">
                                            <label class="custom-control-label" for="add-form-lang-browser">Langue du
                                                navigateur</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="add-form-lang-fr"
                                                value="fr" name="add-form-lang">
                                            <label class="custom-control-label" for="add-form-lang-fr">Français</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="add-form-lang-en"
                                                value="en" name="add-form-lang">
                                            <label class="custom-control-label" for="add-form-lang-en">Anglais</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Après enregistrement redirection vers :</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="add-form-redirect-form" value="0" name="add-form-redirect"
                                                checked="">
                                            <label class="custom-control-label"
                                                for="add-form-redirect-form">Formulaire</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="add-form-redirect-web"
                                                value="1" name="add-form-redirect">
                                            <label class="custom-control-label"
                                                for="add-form-redirect-web">Siteweb</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="d-block">Formulaire pour les :</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="add-form-is-for-student" value='1' name="add-form-is-for">
                                            <label class="custom-control-label"
                                                for="add-form-is-for-student">Etudiants</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="add-form-is-for-immigrant" value='2' name="add-form-is-for">
                                            <label class="custom-control-label"
                                                for="add-form-is-for-immigrant">Immigrants</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="add-form-is-for-both"
                                                value='0' name="add-form-is-for" checked="">
                                            <label class="custom-control-label" for="add-form-is-for-both">Les
                                                deux</label>
                                        </div>
                                    </div>

                                    <div class="form-group" id="add-form-student-program-id" style="display: none;">
                                        <label class="d-block">Programme d'étude :</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="add-form-canada-student" value='4' checked
                                                name="add-form-student-program-id">
                                            <label class="custom-control-label" for="add-form-canada-student">Etudiants
                                                Canada</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="add-form-uk-student"
                                                value='31' name="add-form-student-program-id">
                                            <label class="custom-control-label" for="add-form-uk-student">Etudiants
                                                UK</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="add-form-es-student"
                                                value='32' name="add-form-student-program-id">
                                            <label class="custom-control-label" for="add-form-es-student">Etudiants
                                                Espagne</label>
                                        </div>
                                    </div>

                                    <div class="form-group" id="add-form-is-multiple-program">
                                        <label class="d-block">Plusieurs programmes d'études :</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="add-form-is-not-multiple" value='0' checked
                                                name="add-form-is-multiple-program">
                                            <label class="custom-control-label" for="add-form-is-not-multiple">Non</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="add-form-is-multiple"
                                                value='1' name="add-form-is-multiple-program">
                                            <label class="custom-control-label" for="add-form-is-multiple">Oui</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block"
                                            id="add-leadsforms-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter
                                            formulaire</button>
                                    </div>
                                </form>
                                <!-- END Form leadsforms Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END leadsforms Add Modal -->

    <!-- leadsforms Lead Edit Modal -->
    <div class="modal" id="modal-leadsforms-edit" tabindex="-1" role="dialog"
        aria-labelledby="modal-leadsforms-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-leadsforms-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Editer formulaire</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form leadsforms lead edit -->
                                <form action="" method="POST" onsubmit="return false;" id="leadsforms-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-title">Titre</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-title"
                                            name="edit-form-title" aria-describedby="edit-form-title-error"
                                            aria-invalid="false" placeholder="Titre..">
                                        <div id="edit-form-title-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-campaign">Campagne</label>
                                        <select class="form-control form-control-alt" id="edit-form-campaign"
                                            name="edit-form-campaign" aria-describedby="edit-form-campaign-error"
                                            aria-invalid="false">
                                            <option value="">Sélectionnez type campagne</option>
                                            @foreach (\App\Models\LeadFormMarketingCampaign::all() as $campaign)
                                                <option value="{{ $campaign->id }}">{{ $campaign->campaign_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-campaign-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group" id="edit-form-is-for-commercial">
                                        <label class="d-block">Pour un Commercial</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="edit-form-is-not-for-commercial" value="no"
                                                name="edit-form-is-for-commercial" checked="">
                                            <label class="custom-control-label"
                                                for="edit-form-is-not-for-commercial">Non</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="edit-form-is-for-the-commercial" value="yes"
                                                name="edit-form-is-for-commercial">
                                            <label class="custom-control-label"
                                                for="edit-form-is-for-the-commercial">Oui</label>
                                        </div>
                                    </div>

                                    {{-- commercial part --}}
                                    <div class="form-group" id="edit-form-commercial" style="display: none;">
                                        <label for="edit-form-commercial">Commerciaux</label>
                                        <select class="form-control form-control-alt" id="edit-form-commercial"
                                            name="edit-form-commercial" aria-describedby="edit-form-commercial-error"
                                            aria-invalid="false">
                                            <option value="">Sélectionnez un Commercial</option>
                                            {{-- get only managers and advisers --}}
                                            @foreach (\App\Models\User::whereIn('role_id', [2, 3])->get() as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-commercial-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="d-block">Langue par défault</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="edit-form-lang-browser" value="browser" name="edit-form-lang"
                                                checked="">
                                            <label class="custom-control-label" for="edit-form-lang-browser">Langue du
                                                navigateur</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="edit-form-lang-fr"
                                                value="fr" name="edit-form-lang">
                                            <label class="custom-control-label" for="edit-form-lang-fr">Français</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="edit-form-lang-en"
                                                value="en" name="edit-form-lang">
                                            <label class="custom-control-label" for="edit-form-lang-en">Anglais</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="d-block">Après enregistrement :</label>
                                        <div class="custom-control custom-switch custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" id="edit-form-redirect"
                                                name="edit-form-redirect" checked="">
                                            <label class="custom-control-label" for="edit-form-redirect">Redirection vers
                                                le site web?</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Formulaire pour les :</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="edit-form-is-for-student" value='1' name="edit-form-is-for">
                                            <label class="custom-control-label"
                                                for="edit-form-is-for-student">Etudiants</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="edit-form-is-for-immigrant" value='2' name="edit-form-is-for">
                                            <label class="custom-control-label"
                                                for="edit-form-is-for-immigrant">Immigrants</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="edit-form-is-for-both"
                                                value='0' name="edit-form-is-for" checked="">
                                            <label class="custom-control-label" for="edit-form-is-for-both">Les
                                                deux</label>
                                        </div>
                                    </div>

                                    <div class="form-group" id="edit-form-student-program-id" style="display: none;">
                                        <label class="d-block">Programme d'étude :</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="edit-form-canada-student" value='4' checked
                                                name="edit-form-student-program-id">
                                            <label class="custom-control-label" for="edit-form-canada-student">Etudiants
                                                Canada</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="edit-form-uk-student"
                                                value='31' name="edit-form-student-program-id">
                                            <label class="custom-control-label" for="edit-form-uk-student">Etudiants
                                                UK</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="edit-form-es-student"
                                                value='32' name="edit-form-student-program-id">
                                            <label class="custom-control-label" for="edit-form-es-student">Etudiants
                                                Espagne</label>
                                        </div>
                                    </div>

                                    <div class="form-group" id="edit-form-is-multiple-program">
                                        <label class="d-block">Plusieurs programmes d'études :</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input"
                                                id="edit-form-is-not-multiple" value='0' checked
                                                name="edit-form-is-multiple-program">
                                            <label class="custom-control-label"
                                                for="edit-form-is-not-multiple">Non</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="edit-form-is-multiple"
                                                value='1' name="edit-form-is-multiple-program">
                                            <label class="custom-control-label" for="edit-form-is-multiple">Oui</label>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block"
                                            id="edit-leadsforms-btn"><i class="fa fa-fw fa-plus mr-1"></i>Editer
                                            formulaire</button>
                                    </div>
                                </form>
                                <!-- END Form leadsforms Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END leadsforms Add Modal -->
@endsection
