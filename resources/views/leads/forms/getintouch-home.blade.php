@extends('layouts.getintouch')

@section('js_after')
    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/getintouch.js') }}"></script>
@endsection

<div class="content status" style="padding-top: 0;padding-bottom: 0;">
    @if($errors->any())
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <ul style="margin-bottom: 0;">
                @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @else
        @if (session('status'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{ session('status') }}
            </div>
        @endif
    @endif
</div>

@section('content')

    <div class="content content-boxed">
        <div class="block block-rounded">
            <div class="block-content">
                <div class="p-sm-4 p-xl-7">
                    <h1>
                        Formulaires des leads

                    </h1>
                    <table class="table table-bordered table-vcenter">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 50px;">Programme</th>
                            <th>Lien</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th class="text-center" scope="row">Programme étudiant</th>

                            <td class="d-sm-table-cell">
                                <a href="{{ route('getintouch', $token . "1") }}" class="btn btn-sm btn-alt-primary mr-1">
                                    <i class="si si-rocket mr-1"></i> Cliquez ici
                                </a>
                            </td>

                        </tr>
                        <tr>
                            <th class="text-center" scope="row">Programme immigrant</th>

                            <td class="d-sm-table-cell">
                                <a href="{{ route('getintouch', $token . "2") }}" class="btn btn-sm btn-alt-primary mr-1">
                                    <i class="si si-rocket mr-1"></i> Cliquez ici
                                </a>
                            </td>

                        </tr>

                        </tbody>
                    </table>



                </div>
            </div>
        </div>

    </div>

@endsection
