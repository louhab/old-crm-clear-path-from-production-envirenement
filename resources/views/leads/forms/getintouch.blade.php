@extends('layouts.getintouch')

@section('js_after')
    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/getintouch.js') }}"></script>
@endsection

<div class="content status" style="padding-top: 0;padding-bottom: 0;">
    @if($errors->any())
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <ul style="margin-bottom: 0;">
                @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @else
        @if (session('status'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{ session('status') }}
            </div>
        @endif
    @endif
</div>

@section('content')
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <label for="select-program">Sélectionnez le programme</label>
            <select class="form-control form-control-alt" id="select-program" name="select-program" aria-describedby="select-program-error" aria-invalid="false">
                <option value="">Sélectionnez un programme</option>
                @foreach(\App\Models\Program::all() as $program)
                    <option value="{{ $program->id }}" {{ session('programId') && $program->id == session('programId') ? 'selected' : ''  }}>{{ $program->name }}</option>
                @endforeach
            </select>

            <div id="immigrant-add-form-wrapper" style="display: none;">
                <!-- Form immigrant Add -->
                <form action="{{ route('getintouch-store-student') }}" method="POST" id="immigrant-add-form">
                    @csrf
                    <div class="form-group">
                        <label for="add-form-firstname">Prénom</label>
                        <input type="text" class="form-control form-control-alt" id="add-form-firstname" name="add-form-firstname" placeholder="Prénom.." aria-describedby="add-form-firstname-error" aria-invalid="false">
                        <div id="add-form-firstname-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-lastname">Nom</label>
                        <input type="text" class="form-control form-control-alt" id="add-form-lastname" name="add-form-lastname" placeholder="Nom.." aria-describedby="add-form-lastname-error" aria-invalid="false">
                        <div id="add-form-lastname-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-email">Email</label>
                        <input type="email" class="form-control form-control-alt" id="add-form-email" name="add-form-email" placeholder="Email.." aria-describedby="add-form-email-error" aria-invalid="false">
                        <div id="add-form-email-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-birthday">Date de naissance</label>
                        <input type="date" class="form-control form-control-alt" id="add-form-birthday" name="add-form-birthday" placeholder="Date de naissance.." aria-describedby="add-form-birthday-error" aria-invalid="false">
                        <div id="add-form-birthday-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-languagelevel">Niveau langue</label>
                        <select class="form-control form-control-alt" id="add-form-languagelevel" name="add-form-languagelevel" aria-describedby="add-form-languagelevel-error" aria-invalid="false">
                            <option value="">Sélectionnez un niveau</option>
                            @foreach(\App\Models\EnglishLevel::all() as $level)
                                <option value="{{ $level->id }}">{{ $level->english_level_name }}</option>
                            @endforeach
                        </select>
                        <div id="add-form-languagelevel-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-schoollevel">Niveau scolaire</label>
                        <select class="form-control form-control-alt" id="add-form-schoollevel" name="add-form-schoollevel" aria-describedby="add-form-schoollevel-error" aria-invalid="false">
                            <option value="">Sélectionnez un niveau</option>
                            @foreach(\App\Models\SchoolLevel::all() as $schoolLevel)
                                <option value="{{ $schoolLevel->id }}">{{ $schoolLevel->school_level_name }}</option>
                            @endforeach
                        </select>
                        <div id="add-form-schoollevel-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-country">Pays</label>
                        <select class="form-control form-control-alt" id="add-form-country" name="add-form-country" aria-describedby="add-form-country-error" aria-invalid="false">
                            <option value="">Sélectionnez un pays</option>
                            @foreach(\App\Models\Country::all() as $country)
                                <option value="{{ $country->id }}" {{ isset($programId) && $programId == $country->id ? 'selected' : '' }}>{{ $country->name_fr_fr }}</option>
                            @endforeach
                        </select>
                        <div id="add-form-country-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-adresse1">Adresse Ligne 1</label>
                        <input type="text" class="form-control form-control-alt" id="add-form-adresse1" name="add-form-adresse1" placeholder="Adresse (ligne 1).." aria-describedby="add-form-adresse1-error" aria-invalid="false">
                        <div id="add-form-adresse1-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-adresse2">Adresse Ligne 2</label>
                        <input type="text" class="form-control form-control-alt" id="add-form-adresse2" name="add-form-adresse2" placeholder="Adresse (ligne 2).." aria-describedby="add-form-adresse2-error" aria-invalid="false">
                        <div id="add-form-adresse2-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-postalcode">Code Postal</label>
                        <input type="text" class="form-control form-control-alt" id="add-form-postalcode" name="add-form-postalcode" placeholder="Code postal.." aria-describedby="add-form-postalcode-error" aria-invalid="false">
                        <div id="add-form-postalcode-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-experience">Experience professionelle (ans)</label>
                        <input type="number" class="form-control form-control-alt" id="add-form-experience" name="add-form-experience" placeholder="Experience professionelle.." aria-describedby="add-form-experience-error" aria-invalid="false">
                        <div id="add-form-experience-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-alt-primary btn-block" id="add-immigrant-btn"><i class="fa fa-fw fa-plus mr-1"></i>Envoyer</button>
                    </div>
                </form>
                <!-- END Form immigrant Add -->
            </div>

            <div id="student-add-form-wrapper" style="display: none;">
                <!-- Form student Add -->
                <form action="{{ route('getintouch-store-student') }}" method="POST" id="student-add-form">
                    @csrf
                    <div class="form-group">
                        <label for="add-form-firstname">Prénom</label>
                        <input type="text" class="form-control form-control-alt" id="add-form-firstname" name="add-form-firstname" placeholder="Prénom.." aria-describedby="add-form-firstname-error" aria-invalid="false">
                        <div id="add-form-firstname-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-lastname">Nom</label>
                        <input type="text" class="form-control form-control-alt" id="add-form-lastname" name="add-form-lastname" placeholder="Nom.." aria-describedby="add-form-lastname-error" aria-invalid="false">
                        <div id="add-form-lastname-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-email">Email</label>
                        <input type="email" class="form-control form-control-alt" id="add-form-email" name="add-form-email" placeholder="Email.." aria-describedby="add-form-email-error" aria-invalid="false">
                        <div id="add-form-email-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-languagelevel">Niveau langue</label>
                        <select class="form-control form-control-alt" id="add-form-languagelevel" name="add-form-languagelevel" aria-describedby="add-form-languagelevel-error" aria-invalid="false">
                            <option value="">Sélectionnez un niveau</option>
                            @foreach(\App\Models\EnglishLevel::all() as $level)
                                <option value="{{ $level->id }}">{{ $level->english_level_name }}</option>
                            @endforeach
                        </select>
                        <div id="add-form-languagelevel-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-country">Pays</label>
                        <select class="form-control form-control-alt" id="add-form-country" name="add-form-country" aria-describedby="add-form-country-error" aria-invalid="false">
                            <option value="">Sélectionnez un pays</option>
                            @foreach(\App\Models\Country::all() as $country)
                                <option value="{{ $country->id }}">{{ $country->name_fr_fr }}</option>
                            @endforeach
                        </select>
                        <div id="add-form-country-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-adresse1">Adresse Ligne 1</label>
                        <input type="text" class="form-control form-control-alt" id="add-form-adresse1" name="add-form-adresse1" placeholder="Adresse (ligne 1).." aria-describedby="add-form-adresse1-error" aria-invalid="false">
                        <div id="add-form-adresse1-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-adresse2">Adresse Ligne 2</label>
                        <input type="text" class="form-control form-control-alt" id="add-form-adresse2" name="add-form-adresse2" placeholder="Adresse (ligne 2).." aria-describedby="add-form-adresse2-error" aria-invalid="false">
                        <div id="add-form-adresse2-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-postalcode">Code Postal</label>
                        <input type="text" class="form-control form-control-alt" id="add-form-postalcode" name="add-form-postalcode" placeholder="Code postal.." aria-describedby="add-form-postalcode-error" aria-invalid="false">
                        <div id="add-form-postalcode-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="add-form-experience">Experience professionelle (ans)</label>
                        <input type="number" class="form-control form-control-alt" id="add-form-experience" name="add-form-experience" placeholder="Experience professionelle.." aria-describedby="add-form-experience-error" aria-invalid="false">
                        <div id="add-form-experience-error" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-alt-primary btn-block" id="add-student-btn"><i class="fa fa-fw fa-plus mr-1"></i>Envoyer</button>
                    </div>
                </form>
                <!-- END Form student Add -->
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
@endsection
