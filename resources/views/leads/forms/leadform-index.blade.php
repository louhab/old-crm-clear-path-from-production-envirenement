{{-- @php
    $cities = \App\Models\Cities::select('id', 'city')
        ->orderBy('city', 'asc')
        ->pluck('city', 'id')
        ->toArray();
    dd($cities);

@endphp --}}

@extends('layouts.getintouch')

@section('js_after')
    <script>
        window.setIntlTel("phone");
        var input = document.getElementById('phone');
        input.oninvalid = function(event) {
            event.target.setCustomValidity('Format : +212650123456');
        }
    </script>
    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/getintouch.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}">
    </script>
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/slick-carousel/slick.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min.js') }}"></script>
    <script type="text/javascript">
        (function(l) {
            if (!l) {
                window.lintrk = function(a, b) {
                    window.lintrk.q.push([a, b])
                };
                window.lintrk.q = []
            }
            var s = document.getElementsByTagName("script")[0];
            var b = document.createElement("script");
            b.type = "text/javascript";
            b.async = true;
            b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
            s.parentNode.insertBefore(b, s);
        })(window.lintrk);
    </script>
    <script>
        (function() {
            // console.log("this is select 2 ", $('input[name="old-select2"]').val())
            // reset select 2 values (Program)

            if ($('input[name="old-select2"]').val() === "student") {
                $('label[for="program_field"]').hide();
                $('#program_field_span').hide();
                $('select[name="program_field"]').hide();
                $('label[for="program"]').hide();
                $('select[name="program"]').hide();
                $('#req_program').hide();
                $('#old-select-2').show();
                $('<input>').attr({
                    type: 'hidden',
                    id: 'hidden_program',
                    name: 'program',
                    value: "4"
                }).appendTo('#lead-form');
                $('input:radio[name="select2"]').filter('[value="student"]').attr('checked', true);
            } else {
                $('label[for="program_field"]').show();
                $('#program_field_span').show();
                $('select[name="program_field"]').show();
                $('#hidden_program').remove();
                $('input:radio[name="select2"]').filter('[value="immigrant"]').attr('checked', true);
                $('#old-select-2').hide();
            }



            // reset select 1 values (Meeting 1)
            if ($('input[name="old-select1"]').val() === "conference") {
                $('input:radio[name="select"]').filter('[value="conference"]').attr('checked', true);
            } else {
                $('input:radio[name="select"]').filter('[value="office"]').attr('checked', true);
            }

            // rest for program
            if ($('input[name="old-select-2"]').val()) {
                console.log("tessssst"
                    `[value="${$('input[name="old-select-2"]').val()}"]`);
                $('input:radio[name="select_bis"]').filter(`[value="${$('input[name="old-select-2"]').val()}"]`).attr(
                    'checked', true);
            } else {
                $('input:radio[name="select_bis"]').filter('[value="4"]').attr('checked', true);
            }
        })();

        $('input[name="select2"]').on("change", function() {
            // console.log("selecteinggg")
            // console.log("this is the vall >>", $('input[name="old-select2"]').val())
            let selected = this.value;
            if (selected === "student") {
                $('#old-select-2').show();
                $('label[for="program_field"]').hide();
                $('select[name="program_field"]').hide();
                $('label[for="program"]').hide();
                $('select[name="program"]').hide();
                $('#program_field_span').hide();
                $('#req_program').hide();
                $('<input>').attr({
                    type: 'hidden',
                    id: 'hidden_program',
                    name: 'program',
                    value: "4"
                }).appendTo('#lead-form');
            } else {
                $('#old-select-2').hide();
                $('label[for="program_field"]').show();
                $('select[name="program_field"]').show();
                $('#program_field_span').show();
                $('#hidden_program').remove();
            }
        });

        // Added for add city to getInTouch
        $("[name='country_id']").on("change", function() {
            if ($("[name='country_id']").val() == 144) {
                $("#city2").show();
                $("#city").hide();
            } else {
                $("#city2").hide();
                $("#city").show();
            }
        });
    </script>
    <script>
        $('#datepicker').datepicker({
            format: 'yyyy-mm-dd',
            changeMonth: true,
            changeYear: true,
            startDate: new Date(),
            endDate: '+1m',
            daysOfWeekDisabled: [0],
            autoclose: true,
        });
        $('#datepicker').change(function() {
            $.ajax({
                url: "{{ route('getAvailableDateTime') }}",
                data: {
                    'date': $(this).val()
                },
                success: function(result) {
                    $('#time').empty();
                    $('#time').append(new Option('-', 0, false, false));
                    result = JSON.parse(result);
                    for (var i = 0; i < result.length; i++) {
                        $('#time').append(new Option(result[i], result[i], false, false));
                    }
                }
            })
            //
        })
        $('.option-1').click(function() {
            $('#agenda-45').hide();
        });
        $('.option-2').click(function() {
            $('#agenda-45').show();
        });
    </script>
    <script>
        // detectlangBrowser
        let userLang = navigator.language || navigator.userLanguage;
        document.getElementById("detectlangbrowser").value = userLang.substring(0, 2);
    </script>
@endsection

@section('css_after')
    <link rel="stylesheet" type="text/css"
        href="{{ asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick-theme.css') }}">
    <style>
        .switch {
            display: inline-block;
            margin: 0 5px;
            text-align: right;
            position: absolute;
            top: 4px;
            right: 9px;
        }

        .switch>span {
            position: absolute;
            top: 10px;
            pointer-events: none;
            font-family: 'Helvetica', Arial, sans-serif;
            font-weight: bold;
            font-size: 12px;
            text-transform: uppercase;
            text-shadow: 0 1px 0 rgba(0, 0, 0, .06);
            width: 50%;
            text-align: center;
        }

        input.check-toggle-round-flat:checked~.off {
            color: #F36F25;
        }

        input.check-toggle-round-flat:checked~.on {
            color: #fff;
        }

        .switch>span.on {
            left: 0;
            padding-left: 2px;
            color: #ff1d00;
            font-weight: 700;
        }

        .switch>span.off {
            right: 0;
            padding-right: 4px;
            color: #fff;
        }

        .check-toggle {
            position: absolute;
            margin-left: -9999px;
            visibility: hidden;
        }

        .check-toggle+label {
            display: block;
            position: relative;
            cursor: pointer;
            outline: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        input.check-toggle-round-flat+label {
            padding: 2px;
            width: 97px;
            height: 35px;
            background-color: #ff2e2e;
            -webkit-border-radius: 60px;
            -moz-border-radius: 60px;
            -ms-border-radius: 60px;
            -o-border-radius: 60px;
            border-radius: 60px;
        }

        input.check-toggle-round-flat+label:before,
        input.check-toggle-round-flat+label:after {
            display: block;
            position: absolute;
            content: "";
        }

        input.check-toggle-round-flat+label:before {
            top: 2px;
            left: 2px;
            bottom: 2px;
            right: 2px;
            background-color: #f32525c2;
            -moz-border-radius: 60px;
            -ms-border-radius: 60px;
            -o-border-radius: 60px;
            border-radius: 60px;
        }

        input.check-toggle-round-flat+label:after {
            top: 4px;
            left: 4px;
            bottom: 4px;
            width: 85px;
            background-color: #fff;
            -webkit-border-radius: 52px;
            -moz-border-radius: 52px;
            -ms-border-radius: 52px;
            -o-border-radius: 52px;
            border-radius: 52px;
            -webkit-transition: margin 0.2s;
            -moz-transition: margin 0.2s;
            -o-transition: margin 0.2s;
            transition: margin 0.2s;
        }

        input.check-toggle-round-flat:checked+label {}

        input.check-toggle-round-flat:checked+label:after {
            margin-left: 44px;
        }

        input.check-toggle-round-flat:checked+label:after {
            margin-left: 73px;
        }

        input.check-toggle-round-flat+label {
            padding: 2px;
            width: 166px;
            height: 35px;
            background-color: #ff2e2e;
            border-radius: 60px;
        }

        .wrapper {
            display: inline-flex;
            height: 80px;
            width: 100%;
            align-items: center;
            justify-content: space-evenly;
            border-radius: 5px;
            padding: 20px 0;
        }

        .wrapper .option {
            background: #fff;
            height: 100%;
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: space-evenly;
            margin-right: 10px;
            border-radius: 5px;
            cursor: pointer;
            padding: 0 10px;
            border: 2px solid lightgrey;
            transition: all 0.3s ease;
        }

        .wrapper .option .dot {
            height: 20px;
            width: 20px;
            background: #d9d9d9;
            border-radius: 50%;
            position: relative;
        }

        .wrapper .option .dot::before {
            position: absolute;
            content: "";
            top: 4px;
            left: 4px;
            width: 12px;
            height: 12px;
            background: #d90028;
            border-radius: 50%;
            opacity: 0;
            transform: scale(1.5);
            transition: all 0.3s ease;
        }

        /* New Css */
        .wrapper .option_bis {
            background: #fff;
            height: 100%;
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: space-evenly;
            margin-right: 10px;
            border-radius: 5px;
            cursor: pointer;
            padding: 0 10px;
            border: 2px solid lightgrey;
            transition: all 0.3s ease;
        }

        .wrapper .option_bis .dot {
            height: 20px;
            width: 20px;
            background: #d9d9d9;
            border-radius: 50%;
            position: relative;
        }

        .wrapper .option_bis .dot::before {
            position: absolute;
            content: "";
            top: 4px;
            left: 4px;
            width: 12px;
            height: 12px;
            background: #d90028;
            border-radius: 50%;
            opacity: 0;
            transform: scale(1.5);
            transition: all 0.3s ease;
        }

        /* end New Css */

        input[type="radio"] {
            display: none;
        }


        #option-1:checked:checked~.option-1,
        #option-2:checked:checked~.option-2 {
            border-color: #d90028;
            background: #d90019;
        }

        #option-1:checked:checked~.option-1 .dot,
        #option-2:checked:checked~.option-2 .dot {
            background: #fff;
        }

        #option-1:checked:checked~.option-1 .dot::before,
        #option-2:checked:checked~.option-2 .dot::before {
            opacity: 1;
            transform: scale(1);
        }

        .wrapper .option span {
            font-size: 17px;
            color: #808080;
        }

        #option-1:checked:checked~.option-1 span,
        #option-2:checked:checked~.option-2 span {
            color: #fff;
        }

        /* New Css */
        #program_ca:checked:checked~.option-1,
        #program_uk:checked:checked~.option-2,
        #program_es:checked:checked~.option-3 {
            border-color: #d90028;
            background: #d90019;
        }

        #program_ca:checked:checked~.option-1 .dot,
        #program_uk:checked:checked~.option-2 .dot,
        #program_es:checked:checked~.option-3 .dot {
            background: #fff;
        }

        #program_ca:checked:checked~.option-1 .dot::before,
        #program_uk:checked:checked~.option-2 .dot::before,
        #program_es:checked:checked~.option-3 .dot::before {
            opacity: 1;
            transform: scale(1);
        }

        #program_ca:checked:checked~.option-1 span,
        #program_uk:checked:checked~.option-2 span,
        #program_es:checked:checked~.option-3 span {
            color: #fff;
        }

        .wrapper .option_bis span {
            font-size: 14px;
            color: #808080;
        }


        /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Program option
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        */
        #program-option-1:checked:checked~.program-option-1,
        #program-option-2:checked:checked~.program-option-2 {
            border-color: #d90028;
            background: #d90019;
        }

        #program-option-1:checked:checked~.program-option-1 .dot,
        #program-option-2:checked:checked~.program-option-2 .dot {
            background: #fff;
        }

        #program-option-1:checked:checked~.program-option-1 .dot::before,
        #program-option-2:checked:checked~.program-option-2 .dot::before {
            opacity: 1;
            transform: scale(1);
        }

        .wrapper .option span {
            font-size: 17px;
            color: #808080;
        }

        #program-option-1:checked:checked~.program-option-1 span,
        #program-option-2:checked:checked~.program-option-2 span {
            color: #fff;
        }


        /* program-option-2 */
        /* Smartphones (portrait) ----------- */
        @media (max-width : 400px) {

            /* Styles */
            .wrapper {
                flex-direction: column;
            }
        }

        @media (max-width : 1050px) {

            /* Styles */
            .wrapper {
                flex-direction: column;
            }
        }
    </style>
@endsection
@section('content')
    <div class="content content-boxed">
        <div class="logo" style="margin-bottom: 30px;">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <img src="{{ asset('/img/logo-cpsa.png') }}" style="width: 220px;">
            </div>
        </div>
        <div class="block block-rounded">
            <div class="block-content">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="padding-top: 30px;padding-bottom: 30px;">
                        <div class="alert-status" style="padding-top: 0;padding-bottom: 0;">
                            @if (session('status'))
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6" style="text-align: center">
                                        <div class="alert alert-success alert-dismissable" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            {{ session('status') }}
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <img height="1" width="1" style="display:none;" alt=""
                                    src="https://px.ads.linkedin.com/collect/?pid=3256202&conversionId=6784106&fmt=gif" />
                            @endif
                        </div>
                        <div class="switch">
                            <input id="language-toggle" class="check-toggle check-toggle-round-flat" type="checkbox"
                                onclick="event.preventDefault();document.getElementById('select-language-form').submit();">
                            <label for="language-toggle"></label>
                            <span
                                class="{{ Config::get('app.locale') == 'fr' ? 'on' : 'off' }}">{{ __('lang.french') }}</span>
                            <span
                                class="{{ Config::get('app.locale') == 'en' ? 'on' : 'off' }}">{{ __('lang.english') }}</span>
                            <form id="select-language-form" action="{{ route('select-language') }}" method="POST"
                                style="display: none;">
                                @csrf
                                <input type="hidden" name="locale"
                                    value="{{ Config::get('app.locale') == 'fr' ? 'en' : 'fr' }}">
                            </form>
                        </div>
                        <!-- END Language Dropdown -->
                        <br>
                        <form action="{{ route('getintouch-store') }}" method="POST" id="lead-form"
                            enctype="multipart/form-data" style="">
                            @csrf
                            {{-- if the lead isn't a student --}}
                            @if ($is_students_form === null)
                                {{-- to choose if th --}}
                                <div class="form-group">
                                    <label>{{ __('lang.lead_form_program') }}:</label>
                                    <div class="wrapper">
                                        <input type="hidden" name="old-select2" value="{{ old('select2') }}">
                                        {{-- <input type="radio" name="select2" id="program-option-1" value="immigrant" @if (old('select2') != 'student') checked @endif>
                                            <input type="radio" name="select2" id="program-option-2" value="student" @if (old('select2') == 'student') checked @endif> --}}
                                        <input type="radio" name="select2" id="program-option-1" value="immigrant">
                                        <input type="radio" name="select2" id="program-option-2" value="student">
                                        <label for="program-option-1" class="option program-option-1">
                                            <div class="dot"></div>
                                            <span>{{ __('lang.lead_form_program_immigrant_option') }}</span>
                                        </label>
                                        <label for="program-option-2" class="option program-option-2">
                                            <div class="dot"></div>
                                            <span>{{ __('lang.lead_form_program_student_option') }}</span>
                                        </label>
                                    </div>
                                    @php
                                        $vals_2 = \App\Models\ProgramField::where('visible', 1)
                                            ->get()
                                            ->groupBy('category');
                                    @endphp
                                    <div class="form-group">
                                        <label for="program_field"><?= __('lang.job_fields') ?> :</label>
                                        <span class="text-danger" id="program_field_span">*</span>
                                        <select
                                            class="form-control form-control-alt{{ isset($errors) && $errors->has('program_field') ? ' is-invalid' : '' }}"
                                            id="program_field" name="program_field" aria-describedby="program_field-error"
                                            aria-invalid="{{ isset($errors) && $errors->has('program_field') ? 'true' : 'false' }}">
                                            <option selected disabled value="">
                                                <?= __('lang.job_fields') ?>
                                            </option>
                                            @foreach ($vals_2 as $key => $value)
                                                <optgroup label="{{ $key }}">
                                                    @foreach ($value as $sub_key => $sub_value)
                                                        <option
                                                            {{ old('program_field') == $sub_value->id ? 'selected' : '' }}
                                                            value={{ $sub_value->id }}>{{ $sub_value->label_fr }}
                                                        </option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                        <div id="program_field-error" class="invalid-feedback">
                                            {{ isset($errors) && $errors->has('program_field') ? $errors->first('program_field') : '' }}
                                        </div>
                                    </div>
                                    {{-- ok --}}
                                    <input type="hidden" name="program" value="1">
                            @endif

                            {{-- To be changed ❌ --}}
                            @if ($is_students_form === 1 && $is_multi_program == null)
                                @if ($student_program_id)
                                    <input type="hidden" name="program" value={{ $student_program_id }}>
                                @else
                                    <input type="hidden" name="program" value="4">
                                @endif
                            @endif

                            @if ($is_students_form === 2)
                                @php
                                    $vals = \App\Models\ProgramField::where('visible', 1)
                                        ->get()
                                        ->groupBy('category');
                                @endphp
                                <div class="form-group">
                                    <label for="program_field"><?= __('lang.job_fields') ?> :</label>
                                    <select
                                        class="form-control form-control-alt{{ isset($errors) && $errors->has('program_field') ? ' is-invalid' : '' }}"
                                        id="program_field" name="program_field" aria-describedby="program_field-error"
                                        aria-invalid="{{ isset($errors) && $errors->has('program_field') ? 'true' : 'false' }}">
                                        <option selected disabled value="">
                                            <?= __('lang.job_fields') ?>
                                        </option>
                                        @foreach ($vals as $key => $value)
                                            <optgroup label="{{ $key }}">
                                                @foreach ($value as $sub_key => $sub_value)
                                                    <option {{ old('program_field') == $sub_value->id ? 'selected' : '' }}
                                                        value={{ $sub_value->id }}>{{ $sub_value->label_fr }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                    <div id="program_field-error" class="invalid-feedback">
                                        {{ isset($errors) && $errors->has('program_field') ? $errors->first('program_field') : '' }}
                                    </div>
                                </div>
                                <input type="hidden" name="program" value="1">
                            @endif

                            <input type="hidden" name="token" value="<?php echo $token; ?>">
                            <div class="form-group">
                                <label for="firstname">{{ __('lang.firstname') }}</label>
                                <span class="text-danger">*</span>
                                <input type="text"
                                    class="form-control form-control-alt{{ isset($errors) && $errors->has('firstname') ? ' is-invalid' : '' }}"
                                    id="firstname" name="firstname" value="{{ old('firstname') }}"
                                    placeholder="{{ __('lang.firstname') }}.." aria-describedby="firstname-error"
                                    aria-invalid="{{ isset($errors) && $errors->has('firstname') ? 'true' : 'false' }}">
                                <div id="firstname-error" class="invalid-feedback">
                                    {{ isset($errors) && $errors->has('firstname') ? $errors->first('firstname') : '' }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lastname">{{ __('lang.lastname') }}</label>
                                <span class="text-danger">*</span>
                                <input type="text"
                                    class="form-control form-control-alt{{ isset($errors) && $errors->has('lastname') ? ' is-invalid' : '' }}"
                                    id="lastname" name="lastname" value="{{ old('lastname') }}"
                                    placeholder="{{ __('lang.lastname') }}.." aria-describedby="lastname-error"
                                    aria-invalid="{{ isset($errors) && $errors->has('lastname') ? 'true' : 'false' }}">
                                <div id="lastname-error" class="invalid-feedback">
                                    {{ isset($errors) && $errors->has('lastname') ? $errors->first('lastname') : '' }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email">{{ __('lang.email') }}</label>
                                <input type="email"
                                    class="form-control form-control-alt{{ isset($errors) && $errors->has('email') ? ' is-invalid' : '' }}"
                                    id="email" name="email" value="{{ old('email') }}"
                                    placeholder="{{ __('lang.email') }}.." aria-describedby="email-error"
                                    aria-invalid="{{ isset($errors) && $errors->has('email') ? 'true' : 'false' }}">
                                <div id="email-error" class="invalid-feedback">
                                    {{ isset($errors) && $errors->has('email') ? $errors->first('email') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="phone">{{ __('lang.phone') }}</label>
                                <span class="text-danger">*</span><br />
                                <input type="tel" class="form-control form-control-alt" id="phone"
                                    name="phone" value="{{ old('phone') }}" aria-describedby="phone-error"
                                    aria-invalid="false">
                                <div id="phone-error" class="text-danger">
                                    {{ isset($errors) && $errors->has('phone') ? $errors->first('phone') : '' }}</div>
                                <span id="valid-msg" class="text-success" style="display:none;">✓ Valid</span>
                            </div>

                            {{-- Add City to getInTouch --}}

                            <div class="form-group">
                                <label for="country_id">
                                    {{ Config::get('app.locale') == 'fr' ? 'Pays' : 'Country' }}</label>
                                {{-- <span class="text-danger">*</span> --}}
                                {{ Form::select('country_id', \App\Models\Country::orderBy('name_fr_fr')->pluck('name_fr_fr', 'id'), old('country_id'), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('country_id') ? ' is-invalid' : (isset($errors) && $errors->has('country_id') ? 'true' : 'false')), 'aria-describedby' => 'country_id-error', 'placeholder' => 'Pays..', 'aria-invalid' => '']) }}
                                <div id="country_id-error" class="invalid-feedback">
                                    {{ isset($errors) && $errors->has('country_id') ? $errors->first('country_id') : '' }}
                                </div>
                            </div>

                            {{-- Ville Inputs --}}

                            <div class="form-group" id="city2" style="display: none">
                                <label for="city2">{{ Config::get('app.locale') == 'fr' ? 'Ville' : 'City' }}</label>
                                {{-- <span class="text-danger">*</span> --}}
                                <div class="p-1">
                                    @livewire('input.cities')
                                </div>
                                <div id="city2-error" class="invalid-feedback">
                                    {{ isset($errors) && $errors->has('city2') ? $errors->first('city2') : '' }}</div>
                            </div>

                            <div class="form-group" id="city">
                                <label for="city">{{ Config::get('app.locale') == 'fr' ? 'Ville' : 'City' }}</label>
                                {{-- <span class="text-danger">*</span> --}}
                                <input type="text"
                                    class="form-control form-control-alt{{ isset($errors) && $errors->has('city') ? ' is-invalid' : '' }}"
                                    id="city" name="city" value="{{ old('city') }}" placeholder="Ville.."
                                    aria-describedby="city-error"
                                    aria-invalid="{{ isset($errors) && $errors->has('city') ? 'true' : 'false' }}">
                                <div id="city-error" class="invalid-feedback">
                                    {{ isset($errors) && $errors->has('city') ? $errors->first('city') : '' }}</div>
                            </div>
                            {{-- End Add City to getInTouch --}}

                            {{-- Only if its multi choice --}}
                            @if (($is_students_form == null || $is_students_form == 2) && $is_multi_program)
                                <div class="form-group" id="old-select-2" style="display: none">
                                    <label>{{ __('lang.select_study_program') }}:</label>
                                    <div class="wrapper">
                                        {{-- <input type="hidden" name="old-select-2" value="{{ old('old-select-2') }}"> --}}
                                        <input type="radio" name="select_bis" id="program_ca" value="4">
                                        <input type="radio" name="select_bis" id="program_uk" value="31">
                                        <input type="radio" name="select_bis" id="program_es" value="32">
                                        <label for="program_ca" class="option_bis option-1">
                                            <div class="dot"></div>
                                            <span>Etudes au Canada</span>
                                        </label>
                                        <label for="program_uk" class="option_bis option-2">
                                            <div class="dot"></div>
                                            <span>Etudes au UK</span>
                                        </label>
                                        <label for="program_es" class="option_bis option-3">
                                            <div class="dot"></div>
                                            <span>Etudes en Espagne</span>
                                        </label>
                                    </div>
                                </div>
                                <input type="hidden" name="program_is_selected" value="1">
                            @endif

                            <input type="hidden" id="detectlangbrowser" name="detectlangbrowser">

                            <div class="form-group">
                                <label>{{ __('lang.Rencontres_1') }}:</label>
                                <div class="wrapper">
                                    <input type="hidden" name="old-select1" value="{{ old('select') }}">
                                    <input type="radio" name="select" id="option-1" value="office">
                                    <input type="radio" name="select" id="option-2" value="conference">
                                    <label for="option-1" class="option option-1">
                                        <div class="dot"></div>
                                        <span>{{ __('lang.Pass_au_bureau') }}</span>
                                    </label>
                                    <label for="option-2" class="option option-2">
                                        <div class="dot"></div>
                                        <span>{{ __('lang.Conference_video') }}</span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group" style="padding-top: 20px;">
                                <button type="submit" class="btn btn-lg btn-alt-primary btn-block"><i
                                        class="fa fa-fw fa-plus mr-1"></i>{{ __('lang.soumettre') }}</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
