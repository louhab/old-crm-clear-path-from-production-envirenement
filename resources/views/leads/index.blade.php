<?php
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
?>
@extends('layouts.backend')
@section('css_after')
    <!-- Styles -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        .select2-selection__choice__remove {
            color: #fff !important;
        }

        .select2-search.select2-search--inline,
        .select2-search__field {
            width: 200px !important;
        }

        .select2-container .select2-selection--single {
            min-height: 40px;
        }
    </style>
    @livewireStyles
@endsection
@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/stopwatch.js') }}"></script>
    <script>
        window.userRole = {!! auth()->user()->toJson() !!};
        console.log();
        if (window.userRole.role_id == 1 || window.userRole.role_id == 2 || window.userRole.role_id == 10) {
            $('input[name="daterange"]').daterangepicker({
                opens: 'left',
                locale: {
                    cancelLabel: 'Clear'
                }
            }, function(start, end, label) {
                // console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                $('input[name="date_start"]').val(start.format('YYYY-MM-DD'));
                $('input[name="date_end"]').val(end.format('YYYY-MM-DD'));
                // dashboardPage.initDashboardAnalytics();
                $('#leads-dt').DataTable().ajax.reload(null, false);
            });
        } else {
            $('input[name="daterange"]').daterangepicker({
                autoUpdateInput: false,
                opens: 'left',
                locale: {
                    cancelLabel: 'Clear'
                }
            }, function(start, end, label) {
                // console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                $('input[name="date_start"]').val(start.format('YYYY-MM-DD'));
                $('input[name="date_end"]').val(end.format('YYYY-MM-DD'));
                $('#leads-dt').DataTable().ajax.reload(null, false);
                // dashboardPage.initDashboardAnalytics();
            });
        }

        const lang = $('input[name="lang"]').val();

        $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            $('input[name="date_start"]').val('');
            $('input[name="date_end"]').val('');
        });
        $("[name='tags[]']").select2({
            placeholder: lang === "fr" ? "Selectionnez mots clés" : "Select tag ...",
            width: "100%",
            allowClear: true
        });
        $("[name='program']").select2({
            placeholder: lang === "fr" ? "Programmes ..." : "Programs ...",
            width: "100%",
            allowClear: true
        });
        $("[name='support']").select2({
            placeholder: "Support ...",
            width: "100%",
            allowClear: true
        });
        $("[name='calls-count']").select2({
            placeholder: lang === "fr" ? "Nombre d'appels ..." : "Calls number ...",
            width: "100%",
            allowClear: true
        })

        // $("[name='status']").select2({
        //     placeholder: "Status ...",
        //     width:"100%",
        //     allowClear: true
        // });

        $("[name='conseiller']").select2({
            placeholder: lang === "fr" ? "Conseiller ..." : "Adviser ...",
            width: "100%",
            allowClear: true
        });
        $("[name='campaign']").select2({
            placeholder: "Canal ...",
            width: "100%",
            allowClear: true
        }).on("select2:unselect", function(e) {
            /*var data = e.params.data;
            console.log(data);*/
            Livewire.emit("campaignChanged", $("[name='campaign']").select2("val"))
        }).on("select2:select", function(e) {
            Livewire.emit("campaignChanged", $("[name='campaign']").select2("val"))
        });
        $("[name='office']").select2({
            placeholder: "Office ...",
            width: "100%",
            allowClear: true
        });

        $("[name='payment-proof']").select2({
            placeholder: "Preuve ...",
            width: "100%",
            allowClear: true
        });

        $("[name='influencer']").select2({
            placeholder: lang === "fr" ? "Représentant ..." : "Representing ...",
            width: "100%",
            allowClear: true

        });
        $("[name='source']").select2({
            placeholder: lang === "fr" ? "Source ..." : "Source ...",
            width: "100%",
            allowClear: true

        });
        $("[name='status']").select2({
            placeholder: "Status ...",
            width: "100%",
            allowClear: true

        });
        $("[name='processed']").select2({
            placeholder: lang === "fr" ? "Modification ..." : "Change ...",
            width: "100%",
            allowClear: true
        })
        $("[name='field-activity']").select2({
            placeholder: "Domaine d’activité ...",
            width: "100%",
            allowClear: true
        });
        // lead_state_call
    </script>
    <script src="{{ asset('js/pages/leads/list.js') }}"></script>
    @livewireScripts
    <script>
        // filter dashboard
        //$(document).ready(function() {
        var dashboard_filter = @json($dashboard_filter);
        // console.log(dashboard_filter);
        // daterange
        if ("daterange" in dashboard_filter) {
            // console.log(Object.keys(dashboard_filter));
            // $('input[name="daterange"]').val(dashboard_filter["daterange"]);
            let start = new Date(dashboard_filter["daterange"].split(" - ")[0]);
            let end = new Date(dashboard_filter["daterange"].split(" - ")[1]);
            // console.log(start.format('YYYY-MM-DD'), end);
            $('#daterange').data('daterangepicker').setStartDate(start);
            $('#daterange').data('daterangepicker').setEndDate(end);
            $('input[name="date_start"]').val(start.getFullYear() + "-" + (start.getMonth() + 1) + "-" + start.getDate());
            $('input[name="date_end"]').val(end.getFullYear() + "-" + (end.getMonth() + 1) + "-" + end.getDate());
        }
        // status
        if ("agent-group" in dashboard_filter) {
            if (dashboard_filter["agent-group"] == "Support") {
                $('#search-form select[name="support"]').val(dashboard_filter["agent"]);
            } else {
                $('#search-form select[name="conseiller"]').val(dashboard_filter["agent"]);
            }
        }

        if ("status" in dashboard_filter) {
            $('#status').val(dashboard_filter["status"]);
        }
        if ("recycled" in dashboard_filter) {
            $("#recycled").val(dashboard_filter["recycled"]);
            // console.log(dashboard_filter["status"]);
        }
        /*if ("state" in dashboard_filter) {
            $("#status").val(dashboard_filter["state"]);
        }*/
        if ("program" in dashboard_filter) {
            $('#search-form select[name="program"]').val(dashboard_filter["program"]);
        }
        if ("campaign" in dashboard_filter) {
            // $('#search-form select[name="campaign"]').val(dashboard_filter["campaign"]);
            // window.livewire.emit('campaignFilter', dashboard_filter["campaign"]);
            // console.log('test');
        }
        $.each(dashboard_filter, function(key, value) {
            /*switch (key) {
                case "daterange":
                    break;
            }*/
            // console.log(key, value);
            //$('#search-form').find('[name="'+key+'"]').attr("value", value);
        });
        //});
        Livewire.on("updatedSource", obj => {
            // console.log("test");
            $("[name='source']").select2({
                placeholder: lang === "fr" ? "Source ..." : "Source ...",
                width: "100%",
                allowClear: true

            });
        });
        /*document.addEventListener("DOMContentLoaded", () => {
            Livewire.hook('element.updated', (el, component) => {
                // console.log(el,component);
                if (component.fingerprint.name == "input.source") {
                    // console.log(el);
                    $("[name='source']").select2({
                        placeholder: lang === "fr" ? "Source ..." : "Source ...",
                        width:"100%",
                        allowClear: true

                    });
                }
            });
        });*/
    </script>
@endsection

@section('content')
    @include('layouts.partials.hero', [
        'title' => 'Leads',
        'subTitle' => 'leads',
        'navItems' => ['Admin', 'Leads'],
    ])

    <!-- Page Content -->
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if ($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les
                                informations !</a>
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        <!-- leads Table -->
        <div class="block block-rounded" id="block-leads-search">
            <div class="block-content block-content-full">
                <h4>{{ __('lang.lead_filter_title') }}</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <form id="search-form" name="search-form" action="" method="post" onsubmit="return false">
                            <input type="hidden" name="lang" value="{{ $lang }}">
                            @csrf
                            <div class="form-group form-row">
                                <div class="col-md-12">
                                    <div class="row p-2">
                                        <div class="col-3 p-1">
                                            <label for="lead" class="sr-only">{{ __('lang.lead_filter_lead') }}</label>
                                            <input class="form-control dt_search_field dt_search_lead" id="lead"
                                                name="lead" style="height:44px" placeholder="Nom du lead..">
                                        </div>
                                        <div class="col-3 p-1">
                                            <label for="email"
                                                class="sr-only">{{ __('lang.lead_filter_email') }}</label>
                                            <input class="form-control dt_search_field dt_search_email" id="email"
                                                name="email" style="height:44px" placeholder="Email">
                                        </div>
                                        <div class="col-3 p-1">
                                            <label for="phone"
                                                class="sr-only">{{ __('lang.lead_filter_phone') }}</label>
                                            <input class="form-control dt_search_field dt_search_phone" id="phone"
                                                name="phone" style="height:44px" placeholder="Téléphone">
                                        </div>
                                        <div class="col-3 p-1">
                                            {{ Form::select('tags[]', \App\Models\Tag::pluck('tag_name', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>
                                        <div class="col-3 p-1">
                                            <label for="program"
                                                class="sr-only">{{ __('lang.lead_filter_program') }}</label>
                                            {{ Form::select('program', \App\Models\Program::pluck($lang === 'fr' ? 'name' : 'labelEng', 'id'), null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                        </div>
                                        <div class="col-3 p-1">
                                            <label for="status"
                                                class="sr-only">{{ __('lang.lead_filter_status') }}</label>
                                            <select name="status" id="status"
                                                class="form-control form-control-alt dt_search_field"
                                                style="height: 42px;border: 1px solid #aaa;" data-control="select2"
                                                multiple="multiple" aria-describedby="tags-error" aria-invalid="">
                                                <option value>{{ __('lang.lead_filter_status_placeholder') }}</option>
                                                <option value="n/d">{{ __('lang.lead_filter_status_untreated') }}
                                                </option>
                                                @foreach (\App\Models\LeadStatus::pluck($lang === 'fr' ? 'status_label_fr' : 'status_label_en', 'id') as $key => $status)
                                                    <option value="{{ $key }}">{{ $status }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @livewire('input.processed', ['selected' => array_key_exists('processed', $dashboard_filter) ? $dashboard_filter['processed'] : null])
                                        @livewire('input.campaign', ['selected' => array_key_exists('campaign', $dashboard_filter) ? $dashboard_filter['campaign'] : null])
                                        @livewire('input.form')
                                        @livewire('input.source')
                                        <div class="col-3 p-1">
                                            <label for="office"
                                                class="sr-only">{{ __('lang.lead_filter_status') }}</label>
                                            <select name="office" id="office"
                                                class="form-control form-control-alt dt_search_field"data-control="select2"
                                                multiple="multiple" aria-describedby="tags-error" aria-invalid=""
                                                style="height: 42px;border: 1px solid #aaa;">
                                                <option value>{{ __('lang.lead_filter_status_meeting') }}...</option>
                                                <option value="n/d">{{ __('lang.lead_filter_status_untreated') }}
                                                </option>
                                                <option value="office">{{ __('lang.lead_filter_status_come_office') }}
                                                </option>
                                                <option value="conference">{{ __('lang.lead_filter_status_video_conf') }}
                                                </option>
                                            </select>
                                        </div>
                                        @livewire('input.calls-count')
                                        @unlessconseiller
                                            <div class="p-1 col-3">
                                                @livewire('input.cities', ['value' => null])
                                            </div>
                                            @livewire('input.toggle-state', ['name' => 'call_by_conseiller'])
                                            <div class="col-3 p-1">
                                                <label for="conseiller"
                                                    class="sr-only">{{ __('lang.lead_filter_Adviser') }}</label>
                                                <?php
                                                $list_roles = [3];
                                                if (\Illuminate\Support\Facades\Auth::user()->isAdmin() || \Illuminate\Support\Facades\Auth::user()->isVerificateur()) {
                                                    array_push($list_roles, 2);
                                                }
                                                $queryConseiller = \App\Models\User::where('status', 1)->where(function ($query) use ($list_roles) {
                                                    $query->where(function ($query) use ($list_roles) {
                                                        $query->whereIn('role_id', $list_roles);
                                                    });
                                                    if (\Illuminate\Support\Facades\Auth::user()->isManager()) {
                                                        $query->orWhere(function ($query) {
                                                            $query->where('id', \Illuminate\Support\Facades\Auth::id());
                                                        });
                                                    }
                                                });
                                                $queryConseillerArray = ['n/d' => __('lang.lead_filter_status_unaffected')] + $queryConseiller->pluck('name', 'id')->toArray();
                                                ?>
                                                {{ Form::select('conseiller', $queryConseillerArray, null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                            </div>
                                        @endconseiller
                                        @support_deny
                                            <div class="col-3 p-1">
                                                <label for="support"
                                                    class="sr-only">{{ __('lang.lead_filter_support') }}</label>
                                                <?php
                                                $list_roles = [7];
                                                $querySupport = \App\Models\User::where('status', 1)
                                                    ->whereIn('role_id', $list_roles)
                                                    ->pluck('name', 'id');
                                                $querySupportArray = ['n/d' => __('lang.lead_filter_status_unaffected')] + $querySupport->toArray();
                                                ?>
                                                {{ Form::select('support', $querySupportArray, null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                            </div>
                                            <div class="col-3 p-1">
                                                <?php
                                                $program_field = \App\Models\ProgramField::get()
                                                    ->pluck('label_fr', 'id')
                                                    ->toArray();
                                                ?>
                                                {{ Form::select('field-activity', $program_field, null, ['class' => 'form-control form-control-alt dt_search_field', 'data-control' => 'select2', 'multiple' => 'multiple', 'aria-describedby' => 'tags-error', 'aria-invalid' => '', 'style' => 'height: 42px;border: 1px solid #aaa;']) }}
                                            </div>
                                            @livewire('input.toggle-state', ['name' => 'call_by_support'])
                                        @endsupport_deny

                                        @livewire('input.toggle-state', ['name' => 'payment_proof'])
                                        @livewire('input.toggle-state', ['name' => 'with_payment'])
                                        @livewire('input.toggle-state', ['name' => 'with_R1'])

                                        <div class="col-3 p-1">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text input-group-text-alt">
                                                            <i class="far fa-calendar-alt"></i>
                                                        </span>
                                                    </div>
                                                    @php
                                                        $dateMonth = date('Y-m-01');
                                                        $firstDay = \Carbon\Carbon::parse($dateMonth)->format('m/d/Y');
                                                        $lastDay = \Carbon\Carbon::today()->format('m/d/Y');
                                                        $current_month = $firstDay . ' - ' . $lastDay;
                                                    @endphp
                                                    @if (auth()->user()->isAdmin() ||
                                                            auth()->user()->isManager() ||
                                                            auth()->user()->isVerificateur())
                                                        <input type="text" class="form-control form-control"
                                                            id="daterange" name="daterange"
                                                            value="{{ $current_month }}">
                                                        <input type="hidden" name="date_end" class="dt_search_field"
                                                            value="{{ $lastDay }}" />
                                                        <input type="hidden" name="date_start" class="dt_search_field"
                                                            value="{{ $firstDay }}" />
                                                    @else
                                                        <input type="text" class="form-control form-control"
                                                            id="daterange" name="daterange" value="">
                                                        <input type="hidden" name="date_end" class="dt_search_field"
                                                            value="" />
                                                        <input type="hidden" name="date_start" class="dt_search_field"
                                                            value="" />
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-3 p-1">
                                            <button type="button" class="btn btn-alt-primary btn-block submit-search"
                                                id="search-btn"><i
                                                    class="fa fa-fw fa-search mr-1"></i>{{ __('lang.lead_filter_search_btn') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="leads-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">{{ __('lang.lead_list_title') }}</h3>
                    <div class="block-options">
                        <a href="{{ route('lead-create') }}" class="btn btn-alt-primary"><i
                                class="fa fa-fw fa-plus mr-1"></i>{{ __('lang.lead_list_add_lead_btn') }}</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-vcenter" id="leads-dt">
                        <thead></thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END leads Table -->
    </div>
    <!-- histocall Customer Add Modal -->
    <div class="modal" id="modal-histocall-customer-add" tabindex="-1" role="dialog"
        aria-labelledby="modal-histocall-customer-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-histocall-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter appel client</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form histocall customer Add -->
                                <form action="" method="POST" onsubmit="return false;"
                                    id="histocall-add-customer-form">
                                    {{-- @ahaloua the request should be like this --}}
                                    <div class="form-group">
                                        <p style="line-height: 24px;font-size: 26px;width: 100%;"><strong>Téléphone
                                                :</strong><span id="customer-phone"></span></p>
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <div class="col-md-6 input-wrapper">
                                            <div class="buttons-wrapper">
                                                <button class="btn btn-alt-secondary" id="start-cronometer">Start
                                                    Stopwatch</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <input type="hidden" name="add-form-duration" id="add-form-duration">
                                        <div id="timer" class="col-md-10" style="display: none;">
                                            <div class="text-center clock-wrapper h1">
                                                <span class="hours">00</span>
                                                <span class="dots">:</span>
                                                <span class="minutes">00</span>
                                                <span class="dots">:</span>
                                                <span class="seconds">00</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <div class="buttons-wrapper">
                                            <button class="btn btn-alt-secondary" id="resume-timer"
                                                style="display: none;"><i class="fa fa-fw fa-play"></i> Resume
                                                Timer</button>
                                            <button class="btn btn-alt-secondary" id="stop-timer"
                                                style="display: none;"><i class="fa fa-fw fa-pause"></i> Stop
                                                Timer</button>
                                            <button class="btn btn-alt-secondary" id="reset-timer"
                                                style="display: none;"><i class="fa fa-fw fa-trash-restore"></i> Reset
                                                Timer</button>
                                        </div>
                                    </div>
                                    <div class="form-group mt-4">
                                        <label for="add-form-call_city" className="col-form-label">Ville :</label>
                                        <input type="text" class="form-control form-control-alt"
                                            id="add-form-call_city" name="add-form-call_city" placeholder="Ville"
                                            aria-describedby="add-form-call_city-error" aria-invalid="false" />
                                        <div id="add-form-call_city-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group mt-4">
                                        <label for="add-form-call_descr" className="col-form-label">Note :</label>
                                        <textarea type="text" class="form-control form-control-alt" id="add-form-call_descr" name="add-form-call_descr"
                                            placeholder="Notes.." aria-describedby="add-form-call_descr-error" aria-invalid="false"></textarea>
                                        <div id="add-form-call_descr-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <select name="lead_state_call" id="lead_state_call"></select>
                                    </div>
                                    <!--                                            <div class="form-group">
                                                                                                                                                                                                        <button type="button" id="add-histocall-customer-btn-bis" class="btn btn-alt-primary btn-block"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter appel</button>
                                                                                                                                                                                                    </div>-->
                                </form>
                                <!-- END Form histocall Add _lead_status-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END histocall Add Modal -->
    <!-- END Page Content -->
@endsection
