@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/leads/edit.js') }}"></script>
    <script>
        window.setIntlTel("phone");
        var input = document.getElementById('phone');
        input.oninvalid = function(event) {
	event.target.setCustomValidity('Format : +212650123456');
}
    </script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Leads','subTitle' => 'éditer', 'navItems' => ['Leads', 'éditer']])

    <!-- Page Content -->
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les informations !</a>
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        <!-- Lead Edit Form -->
        <form action="{{ route('lead-update', $lead->id) }}" method="POST" id="lead-form" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="block block-rounded block-themed">
                        <div class="block-header bg-city">
                            <h3 class="block-title">Infos Lead</h3>
                        </div>
                        <div class="block-content">
                            <div class="form-group">
                                <label for="program">Programme</label>
                                <span class="text-danger">*</span>
                                {{Form::select('program', \App\Models\Program::pluck('name', 'id'), old('program', $lead->program_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('program') ? ' is-invalid' : (isset($errors) && $errors->has('program') ? 'true' : 'false')), 'aria-describedby' => 'program-error', 'aria-invalid' => ''])}}
                                <div id="program-error" class="invalid-feedback">{{ isset($errors) && $errors->has('program') ? $errors->first('program') : '' }}</div>
                            </div>
                            <!-- <div class="form-group">
                                <label for="budget">Budget</label>
                                <input type="number" step=".01" class="form-control form-control-alt{{ isset($errors) && $errors->has('budget') ? ' is-invalid' : '' }}" id="budget" name="budget" value="{{ old('budget', $lead->budget) }}" placeholder="Budget.." aria-describedby="budget-error" aria-invalid="{{ isset($errors) && $errors->has('budget') ? 'true' : 'false' }}">
                                <div id="budget-error" class="invalid-feedback">{{ isset($errors) && $errors->has('budget') ? $errors->first('budget') : '' }}</div>
                            </div> -->
                            @administration
                            <div class="form-group">
                                <label for="program">Conseiller</label>
                                <span class="text-danger">*</span>
                                @php $user_id = Illuminate\Support\Facades\Auth::id(); @endphp
                                {{Form::select('conseiller', \App\Models\User::where("role_id", 3)->orWhere("id", $user_id)->pluck('name', 'id'), old('conseiller', $lead->conseiller_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('conseiller') ? ' is-invalid' : (isset($errors) && $errors->has('conseiller') ? 'true' : 'false')), 'aria-describedby' => 'conseiller-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner conseiller...'])}}
                                <div id="program-error" class="invalid-feedback">{{ isset($errors) && $errors->has('conseiller') ? $errors->first('conseiller') : '' }}</div>
                            </div>
                            @endadministration
                            <div class="form-group">
                                <label for="campaign">Canal</label>
                                <span class="text-danger">*</span>
                                {{Form::select('campaign', \App\Models\LeadFormMarketingCampaign::pluck('campaign_name', 'id'), old('campaign', $lead->campaign_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('campaign') ? ' is-invalid' : (isset($errors) && $errors->has('campaign') ? 'true' : 'false')), 'aria-describedby' => 'campaign-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner canal...'])}}
                                <div id="campaign-error" class="invalid-feedback">{{ isset($errors) && $errors->has('campaign') ? $errors->first('campaign') : '' }}</div>
                            </div>
                            <div class="form-group" @if(is_null($lead->campaign_id)) style="display:none;" @endif>
                                <label for="influencer">Influenceur</label>
                                <select class="form-control form-control-alt{{ isset($errors) && $errors->has('influencer') ? ' is-invalid' : '' }}" name="influencer" value="{{ old('influencer', $lead->influencer) }}" aria-describedby="influencer-error" aria-invalid="{{ isset($errors) && $errors->has('influencer') ? 'true' : 'false' }}">
                                </select>
                                <div id="influencer-error" class="invalid-feedback">{{ isset($errors) && $errors->has('influencer') ? $errors->first('influencer') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="firstname">Prénom</label>
                                <span class="text-danger">*</span>
                                <input type="text" class="form-control form-control-alt{{ isset($errors) && $errors->has('firstname') ? ' is-invalid' : '' }}" id="firstname" name="firstname" value="{{ old('firstname', $lead->firstname) }}" placeholder="Prénom.." aria-describedby="firstname-error" aria-invalid="{{ isset($errors) && $errors->has('firstname') ? 'true' : 'false' }}">
                                <div id="firstname-error" class="invalid-feedback">{{ isset($errors) && $errors->has('firstname') ? $errors->first('firstname') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="lastname">Nom</label>
                                <span class="text-danger">*</span>
                                <input type="text" class="form-control form-control-alt{{ isset($errors) && $errors->has('lastname') ? ' is-invalid' : '' }}" id="lastname" name="lastname" value="{{ old('lastname', $lead->lastname) }}" placeholder="Nom.." aria-describedby="lastname-error" aria-invalid="{{ isset($errors) && $errors->has('lastname') ? 'true' : 'false' }}">
                                <div id="lastname-error" class="invalid-feedback">{{ isset($errors) && $errors->has('lastname') ? $errors->first('lastname') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <span class="text-danger">*</span>
                                <input type="email" class="form-control form-control-alt{{ isset($errors) && $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email', $lead->email) }}" placeholder="Email.." aria-describedby="email-error" aria-invalid="{{ isset($errors) && $errors->has('email') ? 'true' : 'false' }}">
                                <div id="email-error" class="invalid-feedback">{{ isset($errors) && $errors->has('email') ? $errors->first('email') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="birthday">Date de naissance</label>
                                <span class="text-danger">*</span>
                                <input type="date" class="form-control form-control-alt{{ isset($errors) && $errors->has('birthday') ? ' is-invalid' : '' }}" id="birthday" name="birthday" value="{{ old('birthday', $lead->birthday) }}" placeholder="Date de naissance.." aria-describedby="birthday-error" aria-invalid="{{ isset($errors) && $errors->has('birthday') ? 'true' : 'false' }}">
                                <div id="birthday-error" class="invalid-feedback">{{ isset($errors) && $errors->has('birthday') ? $errors->first('birthday') : '' }}</div>
                            </div>
                            {{-- pattern="(\+212)[1-9]([ \-_/]*)(\d[ \-_/]*){8}" --}}
                            <div class="form-group">
                                <label for="phone">Téléphone</label><br />
                                <input type="text"  class="form-control form-control-alt{{ isset($errors) && $errors->has('phone') ? ' is-invalid' : '' }}" id="phone" name="phone" value="{{ old('phone',$lead->phone) }}" aria-describedby="phone-error" aria-invalid="{{ isset($errors) && $errors->has('phone') ? 'true' : 'false' }}"
                                >
                                <div id="phone-error" class="text-danger">{{ isset($errors) && $errors->has('phone') ? $errors->first('phone') : '' }}</div>
                                <span id="valid-msg" class="text-success" style="display:none;">✓ Valid</span>
                            </div>
                            {{-- from creat --}}

                            <div class="form-group">
                                <label for="lead_status">Status Lead</label>
                                {{Form::select('lead_status', \App\Models\LeadStatus::pluck('status_label_fr', 'id'), old('lead_status', $lead->lead_status_id), ['class' => 'form-control form-control-alt' . (isset($errors) && $errors->has('lead_status') ? ' is-invalid' : (isset($errors) && $errors->has('lead_status') ? 'true' : 'false')), 'aria-describedby' => 'lead_status-error', 'aria-invalid' => '', 'placeholder' => 'Sélécionner status...'])}}
                                <div id="lead_status-error" class="invalid-feedback">{{ isset($errors) && $errors->has('lead_status') ? $errors->first('lead_status') : '' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block block-rounded block-themed">
                        <div class="block-header bg-city">
                            <h3 class="block-title">Documents</h3>
                        </div>
                        <div class="block-content">
                            <div class="form-group">
                                <!--                                <label>CV</label>-->
                                @if(!$lead->cv_id)
                                    <div class="custom-file" lang="fr">
                                        <!-- Populating custom file input label with the selected filename (data-toggle="custom-file-input" is initialized in Helpers.coreBootstrapCustomFileInput()) -->
                                        <!-- When multiple files are selected, we use the word 'Files'. You can easily change it to your own language by adding the following to the input, eg for DE: data-lang-files="Dateien" -->
                                        <input type="file" class="custom-file-input js-custom-file-input-enabled{{ isset($errors) && $errors->has('file-input-cv') ? ' is-invalid' : '' }}" data-toggle="file-input-cv" id="file-input-cv" name="file-input-cv" aria-describedby="file-input-cv-error" aria-invalid="{{ isset($errors) && $errors->has('file-input-cv') ? 'true' : 'false' }}">
                                        <div id="file-input-cv-error" class="invalid-feedback">{{ isset($errors) && $errors->has('file-input-cv') ? $errors->first('file-input-cv') : '' }}</div>
                                        <label class="custom-file-label" id="label-cv-input" for="file-input-cv">Sélectionnez CV</label>
                                    </div>
                                @else
                                    <div class="dropdown show">
                                        <button type="button" class="btn btn-danger dropdown-toggle" id="dropdown-default-danger" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            CV
                                        </button>
                                        <div class="dropdown-menu font-size-sm" aria-labelledby="dropdown-default-danger" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                                            <a class="dropdown-item" href="{{ route('get-media', $lead->getMedia('CVs')[0]) }}">Télécharger</a>
                                            <a id="delete-cv-href" class="dropdown-item" href="javascript:void(0)">
                                                Supprimer
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
        </div>
            <div class="row">
                    <div class="col-md-6">
                        <div class="form-group" style="padding-top: 20px;">
                            <button type="submit" class="btn btn-lg btn-alt-primary btn-block" id="update-lead-btn"><i class="fa fa-fw fa-check mr-1"></i>Enregistrer</button>
                        </div>
                    </div>
                    <div class="col-md-6"></div>
            </div>
        </form>
        <!-- END Lead Edit Form -->
        <form id="delete-form" action="{{ route('lead-delete-media',['lead' => $lead, 'collectionName' => 'CVs']) }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
    <!-- END Page Content -->
@endsection
