@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <!--    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>-->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
@endsection
@section('css_after')
    <!-- Styles -->
    <!--    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">-->
@endsection

@section('content')
    @include('layouts.partials.hero', [
        'title' => 'Leads',
        'subTitle' => 'importer des Leads',
        'navItems' => ['Admin', 'Importation'],
    ])

    <!-- Page Content -->
    <div class="content">
        <!-- Users Table -->
        @if (Session::get('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <p class="mb-0">{{ Session::get('success') }}</p>
            </div>
        @endif

        @if (Session::get('error'))
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <p class="mb-0">{{ Session::get('error') }}</p>
            </div>
        @endif

        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Importer des leads via CSV</h3>
                </div>
                <form action="{{ route('integrate-leads') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="custom-file text-left">
                        <div class="row" style="margin: 0">
                            <div class="col-md-6">
                                <input type="file" class="custom-file-input js-custom-file-input-enabled"
                                    data-toggle="custom-file-input" id="file" name="file">
                                <label class="custom-file-label" for="file">Sélectionnez un fichier CSV</label>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary btn-block">Import</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <!-- END Users Table -->

    </div>
    <!-- END Page Content -->
@endsection
