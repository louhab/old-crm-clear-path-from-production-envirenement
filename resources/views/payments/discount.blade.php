@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/payments/discount.js') }}"></script>
@endsection

@section('css_after')
    <style>
        input[type=range] {
            height: 25px;
            -webkit-appearance: none;
            margin: 10px 0;
            width: 100%;
        }
        input[type=range]:focus {
            outline: none;
        }
        input[type=range]::-webkit-slider-runnable-track {
            width: 100%;
            height: 5px;
            cursor: pointer;
            animate: 0.2s;
            box-shadow: 0px 0px 0px #000000;
            background: #D52B26;
            border-radius: 1px;
            border: 0px solid #000000;
        }
        input[type=range]::-webkit-slider-thumb {
            box-shadow: 0px 0px 0px #000000;
            border: 1px solid #D52B26;
            height: 18px;
            width: 18px;
            border-radius: 25px;
            background: #FBEAEA;
            cursor: pointer;
            -webkit-appearance: none;
            margin-top: -7px;
        }
        input[type=range]:focus::-webkit-slider-runnable-track {
            background: #D52B26;
        }
        input[type=range]::-moz-range-track {
            width: 100%;
            height: 5px;
            cursor: pointer;
            animate: 0.2s;
            box-shadow: 0px 0px 0px #000000;
            background: #D52B26;
            border-radius: 1px;
            border: 0px solid #000000;
        }
        input[type=range]::-moz-range-thumb {
            box-shadow: 0px 0px 0px #000000;
            border: 1px solid #D52B26;
            height: 18px;
            width: 18px;
            border-radius: 25px;
            background: #FBEAEA;
            cursor: pointer;
        }
        input[type=range]::-ms-track {
            width: 100%;
            height: 5px;
            cursor: pointer;
            animate: 0.2s;
            background: transparent;
            border-color: transparent;
            color: transparent;
        }
        input[type=range]::-ms-fill-lower {
            background: #D52B26;
            border: 0px solid #000000;
            border-radius: 2px;
            box-shadow: 0px 0px 0px #000000;
        }
        input[type=range]::-ms-fill-upper {
            background: #D52B26;
            border: 0px solid #000000;
            border-radius: 2px;
            box-shadow: 0px 0px 0px #000000;
        }
        input[type=range]::-ms-thumb {
            margin-top: 1px;
            box-shadow: 0px 0px 0px #000000;
            border: 1px solid #D52B26;
            height: 18px;
            width: 18px;
            border-radius: 25px;
            background: #FBEAEA;
            cursor: pointer;
        }
        input[type=range]:focus::-ms-fill-lower {
            background: #D52B26;
        }
        input[type=range]:focus::-ms-fill-upper {
            background: #D52B26;
        }
    </style>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Formulaires','subTitle' => 'leads', 'navItems' => ['Admin', 'Forms']])

    <!-- Page Content -->
    <div class="content">

        <!-- leadsforms Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <form id="search-form" name="search-form" action="" method="post" onsubmit="return false">
                            <div class="form-group form-row">
                                <div class="col-3">
                                    <select class="form-control dt_search_field" name="campaign-type">
                                        <option value="">Sélectionnez type campagne</option>
                                        @foreach(\App\Models\LeadFormMarketingCampaign::all() as $campaign)
                                            <option value="{{ $campaign->id }}">{{ $campaign->campaign_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-2">
                                    <button type="button" class="btn btn-alt-primary btn-block submit-search" id="search-btn"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                                </div>
                                <div class="col-1">
                                    <button type="button" class="btn btn-alt-danger btn-block clear-search" id="clear-search-btn"><i class="fa fa-fw fa-redo mr-1"></i></button>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="leadsforms-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Payments des leads</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary"  id="payments-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter Réduction</button>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="leadsforms-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END payments Table -->
    </div>
    <!-- END Page Content -->

    <!-- Discount Lead Add Modal -->
    <div class="modal" id="modal-payments-add" tabindex="-1" role="dialog" aria-labelledby="modal-payments-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-payments-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter Réduction</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form discount lead Add -->
                                <form action="" method="POST" onsubmit="return false;" id="payments-add-form">
                                    <div class="form-group">
                                        <label for="add-form-lead">Lead</label>
                                        <select class="form-control claculate" id="add-form-lead" name="add-form-lead" style="width: 100%">
                                        </select>
                                        <div id="add-form-lead-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-payment">Paiement</label>
                                        <select class="form-control claculate" id="add-form-payment" name="add-form-payment" style="width: 100%">
                                        </select>
                                        <div id="add-form-payment-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-percent">Percentage : <span id="rangePercent">0 %</span></label>
                                        <input type="range" value="0" min="0" max="100" class="form-control form-control-alt" id="add-form-percent" name="add-form-percent" aria-describedby="add-form-percent-error" aria-invalid="false">
                                        <div id="add-form-percent-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Amout : <span id="amount" value=""></span></label>
                                        <br />
                                        <label>New Amout : <span id="newAmount" value=""></span></label>
                                        <input type="hidden" name="add-form-amount" id="add-form-amount" value="">
                                    </div>
                                    <!--<div class="form-group">
                                        <div class="progress">
                                            <div class="progress-bar bg-danger progress-bar-striped active" role="progressbar"
                                                 aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                                                40%
                                            </div>
                                        </div>
                                    </div>-->

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-discount-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter réduction</button>
                                    </div>
                                </form>
                                <!-- END Form discount Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Discount Add Modal -->
    <!-- leadsforms Lead Edit Modal -->
    <div class="modal" id="modal-leadsforms-edit" tabindex="-1" role="dialog" aria-labelledby="modal-leadsforms-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-leadsforms-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Editer formulaire</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form leadsforms lead edit -->
                                <form action="" method="POST" onsubmit="return false;" id="leadsforms-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-title">Titre</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-title" name="edit-form-title" aria-describedby="edit-form-title-error" aria-invalid="false" placeholder="Titre..">
                                        <div id="edit-form-title-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-campaign">Campagne</label>
                                        <select class="form-control form-control-alt" id="edit-form-campaign" name="edit-form-campaign" aria-describedby="edit-form-campaign-error" aria-invalid="false">
                                            <option value="">Sélectionnez type campagne</option>
                                            @foreach(\App\Models\LeadFormMarketingCampaign::all() as $campaign)
                                                <option value="{{ $campaign->id }}">{{ $campaign->campaign_name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-campaign-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="edit-leadsforms-btn"><i class="fa fa-fw fa-plus mr-1"></i>Editer formulaire</button>
                                    </div>
                                </form>
                                <!-- END Form leadsforms Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END leadsforms Add Modal -->

@endsection
