<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
@extends('layouts.backend')

@section('js_after')

    <!-- Page JS Code -->
    <script>
        //import intlTelInput from 'intl-tel-input';

        // var input = document.querySelector("#phone");
        //const input = document.querySelector("#phone");
        // intlTelInput(input, {
        //     // any initialisation options go here
        // });
        window.setIntlTel("phone");
        $("#toggle-password").on('click', function(){
            //
            let pass = $('input[name="password"]');
            if(pass.attr('type') == 'password') {
                pass.attr('type', 'text')
            } else if(pass.attr('type') == 'text') {
                pass.attr('type', 'password')
            }
            //console.log(pass.attr('type'));
        });
    </script>
@endsection
@section('css_after')
    <!-- Styles -->
    <!-- <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet"> -->
    <!-- <link rel="stylesheet" href="build/css/intlTelInput.css"> -->
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Profile','subTitle' => __("lang.adviser_profile_page_subtitle"), 'navItems' => ['Profile', __("lang.adviser_profile_page_subtitle")]])

    <!-- Page Content -->
    <!-- Profile Edit Form -->
    <div class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="block block-rounded block-themed">
                    <div class="block-header bg-city">
                        <h3 class="block-title">{{ __("lang.adviser_profile_info_title") }}</h3>
                    </div>
                    <div class="block-content">
                        <form action="{{ route('profile-update') }}" method="POST" id="profile-form" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="form-group text-center">
                                <label>{{ __("lang.adviser_profile_info_avatar") }}</label>
                                <div class="push" >
                                    @php
                                        $media = \Illuminate\Support\Facades\Auth::user()->getFirstMediaUrl('avatar');
                                    @endphp
                                    @if(empty($media))
                                        <img class="img-avatar" src="http://localhost:8000/media/avatars/avatar10.jpg" alt="">
                                    @else
                                        <img height="225px;" src="{{$media}}" alt="">
                                    @endif
                                </div>
                                <div class="custom-file w-50 text-left">
                                    <!-- Populating custom file input label with the selected filename (data-toggle="custom-file-input" is initialized in Helpers.coreBootstrapCustomFileInput()) -->
                                    <input type="file" class="custom-file-input js-custom-file-input-enabled" data-toggle="custom-file-input" id="avatar" name="avatar">
                                    <label class="custom-file-label" for="avatar">{{ __("lang.adviser_profile_info_choose_avatar") }}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">{{ __("lang.adviser_profile_info_name") }}</label>
                                <input type="text" class="form-control form-control-alt{{ isset($errors) && $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" value="{{ old('name', Auth::user()->name) }}" placeholder="{{ __("lang.adviser_profile_info_name") }}.." aria-describedby="name-error" aria-invalid="{{ isset($errors) && $errors->has('name') ? 'true' : 'false' }}">
                                <div id="name-error" class="invalid-feedback">{{ isset($errors) && $errors->has('name') ? $errors->first('name') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control form-control-alt{{ isset($errors) && $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email', Auth::user()->email) }}" placeholder="Email.." aria-describedby="email-error" aria-invalid="{{ isset($errors) && $errors->has('email') ? 'true' : 'false' }}">
                                <div id="email-error" class="invalid-feedback">{{ isset($errors) && $errors->has('email') ? $errors->first('email') : '' }}</div>
                            </div>
                            <div class="form-group">
                                <label for="password">{{ __("lang.adviser_profile_info_password") }}</label>
                                <div class="input-group">
                                    <input type="password" class="form-control form-control-alt" id="password" name="password" placeholder="{{ __("lang.adviser_profile_info_password") }}">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-alt-primary" id="toggle-password">
                                            <i class="fa fa-eye" style="font-size:1em;"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone">{{ __("lang.adviser_profile_info_phone") }}</label><br />
                                <input type="text" class="form-control form-control-alt{{ isset($errors) && $errors->has('phone') ? ' is-invalid' : '' }}" id="phone" name="phone" value="{{ old('phone', Auth::user()->phone) }}" aria-describedby="phone-error" aria-invalid="{{ isset($errors) && $errors->has('phone') ? 'true' : 'false' }}">
                                <div id="phone-error" class="text-danger">{{ isset($errors) && $errors->has('phone') ? $errors->first('phone') : '' }}</div>
                                <span id="valid-msg" class="text-success" style="display:none;">✓ Valid</span>
                            </div>
                            <div class="form-group">
                                <label class="d-block">{{ __("lang.adviser_profile_info_gender") }} :</label>
                                @php $gender = \Illuminate\Support\Facades\Auth::user()->gender; @endphp
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input edit-form-gender" type="radio" id="example-radios1" name="gender" value="male" @if($gender == "male") checked @endif>
                                    <label class="form-check-label" for="example-radios1">{{ __("lang.adviser_profile_info_male") }}</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input edit-form-gender" type="radio" id="edit-form-gender" name="gender" value="female" @if($gender == "female") checked @endif>
                                    <label class="form-check-label" for="edit-form-gender">{{ __("lang.adviser_profile_info_female") }}</label>
                                </div>
                                <div id="gender-error" class="invalid-feedback">{{ isset($errors) && $errors->has('gender') ? $errors->first('gender') : '' }}</div>
                            </div>
                            <div class="form-group" style="padding-top: 20px;">
                                <button type="submit" class="btn btn-lg btn-alt-primary btn-block" id="update-lead-btn"><i class="fa fa-fw fa-check mr-1"></i>{{ __("lang.adviser_profile_info_save_btn") }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                @management
                    @livewire('user.rotation.assign')
                @endmanagement
            </div>
        </div>
    </div>

@endsection
