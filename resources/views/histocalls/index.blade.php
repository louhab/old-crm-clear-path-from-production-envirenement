<?php
$lang = session()->get('locale') === 'en' ? 'en' : 'fr';
?>
@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.11.3/api/sum().js"></script>

    <!-- Page JS Code -->
    <script>
        window.userRole = {!! auth()->user()->toJson() !!};
        window.dashboard_filter = @json($dashboard_filter);
    </script>
    <script src="{{ asset('js/pages/histocalls.js') }}"></script>
    <script>
        $('input[name="daterange"]').daterangepicker({
            opens: 'left',
            locale: {
                cancelLabel: 'Clear'
            }
        }, function(start, end, label) {
            // console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            $('input[name="date_start"]').val(start.format('YYYY-MM-DD'));
            $('input[name="date_end"]').val(end.format('YYYY-MM-DD'));
            $('#histocalls-dt').DataTable().ajax.reload(null, false);
            // dashboardPage.initDashboardAnalytics();
        });
        $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            $('input[name="date_start"]').val('');
            $('input[name="date_end"]').val('');
            $('#histocalls-dt').DataTable().ajax.reload(null, false);
        });
        // filter dashboard
        /* $('input[name="daterange"]').daterangepicker({
             opens: 'left'
         }, function(start, end, label) {
             // console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
             $('input[name="date_start"]').val(start.format('YYYY-MM-DD'));
             $('input[name="date_end"]').val(end.format('YYYY-MM-DD'));
             $('#histocalls-dt').DataTable().ajax.reload(null, false);
         });*/
        //$(document).ready(function() {
        // console.log(dashboard_filter);

        // daterange (default values)
        // if ("daterange" in window.dashboard_filter) {
        //     let start = new Date(window.dashboard_filter["daterange"].split(" - ")[0]);
        //     let end = new Date(window.dashboard_filter["daterange"].split(" - ")[1]);
        //     $('#daterange').data('daterangepicker').setStartDate(start);
        //     $('#daterange').data('daterangepicker').setEndDate(end);
        //     $('input[name="date_start"]').val(start.getFullYear() + "-" + (start.getMonth() + 1) + "-" + start.getDate());
        //     $('input[name="date_end"]').val(end.getFullYear() + "-" + (end.getMonth() + 1) + "-" + end.getDate());
        // }
        // status
        if ("state" in window.dashboard_filter) {
            // console.log("Test!");
            $("#status").val(dashboard_filter["state"]);
        }
        /*if ("status" in dashboard_filter) {
            $("#status").val(dashboard_filter["status"]);
        }
        if ("program" in dashboard_filter) {
            $('#search-form select[name="program"]').val(dashboard_filter["program"]);
        }
        if ("campaign" in dashboard_filter) {
            $('#search-form select[name="campaign"]').val(dashboard_filter["campaign"]);
        }*/
        //$.each(window.dashboard_filter, function(key,value) {
        /*switch (key) {
            case "daterange":
                break;
        }*/
        console.log(window.dashboard_filter);
        //$('#search-form').find('[name="'+key+'"]').attr("value", value);
        //});
        //});
        // Simply get the sum of a column
        /*var table = $('#histocalls-dt').DataTable();
        console.log(table.column( 5 ).data().sum());*/
    </script>
@endsection

@section('css_after')
    <!-- Styles -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        .select2-selection__rendered {
            line-height: 39px !important;
        }

        .select2-container .select2-selection--single {
            height: 40px !important;
        }

        .select2-selection__arrow {
            height: 43px !important;
        }

        .select2-selection__choice__remove {
            color: #fff !important;
        }
    </style>
@endsection

@section('content')
    @include('layouts.partials.hero', [
        'title' => $lang === 'fr' ? 'Historique' : 'History',
        'subTitle' => $lang === 'fr' ? 'appels' : 'calls',
        'navItems' => ['Admin', $lang === 'fr' ? 'Historique des appels' : 'Calls history'],
    ])

    <!-- Page Content -->
    <div class="content">

        <!-- histocalls Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4> {{ __('lang.call_history_title') }}</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">
                            <div class="col-3">
                                <input type="text" class="form-control dt_search_field" name="name"
                                    placeholder="{{ __('lang.call_history_surname_placeholder') }}">
                            </div>
                            @administration
                                <div class="col-3">
                                    <label for="agent" class="sr-only">{{ __('lang.call_history_users') }}...</label>
                                    <select class="form-control dt_search_field dt_search_agent" id="agent" name="agent">
                                        @if (array_key_exists('agent', $dashboard_filter))
                                            <option value="{{ $dashboard_filter['agent'] }}" selected="selected">
                                                {{ \App\Models\User::find($dashboard_filter['agent'])->name }}</option>
                                        @endif
                                    </select>
                                    <input type="hidden" class="dt_search_field" name="agent-group" value="">
                                </div>
                            @endadministration

                            <div class="col-3">
                                <label for="status" class="sr-only">Status actuelle</label>
                                <select name="status" id="status" class="form-control form-control-alt dt_search_field"
                                    style="height: 42px;border: 1px solid #aaa;">
                                    <option value>Status actuelle...</option>
                                    <option value="processed">{{ __('lang.call_history_processed') }}</option>
                                    <option value="n/d">{{ __('lang.call_history_unprocessed') }}</option>
                                    @foreach (\App\Models\LeadStatus::pluck($lang === 'fr' ? 'status_label_fr' : 'status_label_en', 'id') as $key => $status)
                                        <option value="{{ $key }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-3">
                                <label for="state_before" class="sr-only">Status avant</label>
                                <select name="state_before" id="state_before"
                                    class="form-control form-control-alt dt_search_field"
                                    style="height: 42px;border: 1px solid #aaa;">
                                    <option value>Status avant...</option>
                                    <option value="processed">{{ __('lang.call_history_processed') }}</option>
                                    <option value="n/d">{{ __('lang.call_history_unprocessed') }}</option>
                                    @foreach (\App\Models\LeadStatus::pluck($lang === 'fr' ? 'status_label_fr' : 'status_label_en', 'id') as $key => $status)
                                        <option value="{{ $key }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text input-group-text-alt">
                                                <i class="far fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        @php
                                            // $dateMonth = date('Y-m-01');
                                            // $firstDay = \Carbon\Carbon::parse($dateMonth)->format('m/d/Y');
                                            $lastDay = \Carbon\Carbon::today()->format('m/d/Y');
                                            $current_month = $lastDay . ' - ' . $lastDay;
                                        @endphp
                                        <input type="text" class="form-control form-control" id="daterange"
                                            name="daterange" value="{{ $current_month }}">
                                        <input type="hidden" name="date_end" class="dt_search_field"
                                            value="{{ $lastDay }}" />
                                        <input type="hidden" name="date_start" class="dt_search_field"
                                            value="{{ $lastDay }}" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i
                                        class="fa fa-fw fa-search mr-1"></i>{{ __('lang.call_history_search_btn') }}</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i
                                        class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="histocalls-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">
                        {{ __('lang.call_history_title') }}
                        <small id="durations-sum"
                            class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-success">{{ __('lang.call_history_call_duration') }}
                            : </small>
                    </h3>
                    <div class="block-options">
                        <div class="dropdown">
                            <button type="button" class="btn btn-alt-primary dropdown-toggle" id="dropdown-default-primary"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-fw fa-plus mr-1"></i> {{ __('lang.call_history_add_call') }}
                            </button>
                            <div class="dropdown-menu font-size-sm" aria-labelledby="dropdown-default-primary">
                                <a class="dropdown-item" id="histocall-lead-table-add" data-type="lead">Lead</a>
                                <a class="dropdown-item" id="histocall-customer-table-add" data-type="client">Client</a>
                            </div>
                        </div>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="histocalls-dt">
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="text-align:right">{{ __('lang.call_history_call_duration') }} : </th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- END histocalls Table -->




    </div>
    <!-- END Page Content -->

    <!-- histocall Lead Add Modal -->
    <div class="modal" id="modal-histocall-lead-add" tabindex="-1" role="dialog"
        aria-labelledby="modal-histocall-lead-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-histocall-lead-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">{{ __('lang.call_history_add_lead_call') }}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form histocall lead Add -->
                                <form action="" method="POST" onsubmit="return false;"
                                    id="histocall-add-lead-form">
                                    <div class="form-group">
                                        <label for="add-form-lead_id">Lead</label>
                                        <select class="form-control form-control-alt" id="add-form-lead_id"
                                            name="add-form-lead_id" aria-describedby="add-form-lead_id-error"
                                            aria-invalid="false">
                                            <option value="">{{ __('lang.call_history_select_lead') }}</option>
                                            @foreach (\App\Models\Lead::all() as $lead)
                                                <option value="{{ $lead->id }}">{{ $lead->firstname }}
                                                    {{ $lead->lastname }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-lead_id-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="add-form-call_dt">{{ __('lang.call_history_call_date') }}</label>
                                        <input type="date" class="form-control form-control-alt" id="add-form-call_dt"
                                            name="add-form-call_dt" aria-describedby="add-form-call_dt-error"
                                            aria-invalid="false">
                                        <div id="add-form-call_dt-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <label
                                            for="add-form-duration">{{ __('lang.call_history_call_duration_bis') }}</label>
                                        <input type="number" class="form-control form-control-alt"
                                            id="add-form-duration" name="add-form-duration"
                                            placeholder="{{ __('lang.call_history_duration_placeholder') }}"
                                            aria-describedby="add-form-duration-error" aria-invalid="false">
                                        <div id="add-form-duration-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-call_descr">Description</label>
                                        <textarea type="text" class="form-control form-control-alt" id="add-form-call_descr" name="add-form-call_descr"
                                            placeholder="Description.." aria-describedby="add-form-call_descr-error" aria-invalid="false"></textarea>
                                        <div id="add-form-call_descr-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block"
                                            id="add-histocall-lead-btn"><i
                                                class="fa fa-fw fa-plus mr-1"></i>{{ __('lang.call_history_add_call') }}</button>
                                    </div>
                                </form>
                                <!-- END Form histocall Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1"
                            data-dismiss="modal">{{ __('lang.call_history_close') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END histocall Add Modal -->

    <!-- histocall Lead Edit Modal -->
    <div class="modal" id="modal-histocall-lead-edit" tabindex="-1" role="dialog"
        aria-labelledby="modal-histocall-lead-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-lead-histocall-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">{{ __('lang.call_history_edit_lead_call') }}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form histocall lead Edit -->
                                <form action="" method="POST" onsubmit="return false;"
                                    id="histocall-edit-lead-form">
                                    <div class="form-group">
                                        <label for="edit-form-lead_id">Lead</label>
                                        <select class="form-control form-control-alt" id="edit-form-lead_id"
                                            name="edit-form-lead_id" aria-describedby="edit-form-lead_id-error"
                                            aria-invalid="false">
                                            <option value="">{{ __('lang.call_history_select_lead') }}</option>
                                            @foreach (\App\Models\Lead::all() as $lead)
                                                <option value="{{ $lead->id }}">{{ $lead->firstname }}
                                                    {{ $lead->lastname }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-lead_id-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-call_dt">{{ __('lang.call_history_call_date') }}</label>
                                        <input type="date" class="form-control form-control-alt"
                                            id="edit-form-lead-call_dt" name="edit-form-call_dt"
                                            aria-describedby="edit-form-call_dt-error" aria-invalid="false">
                                        <div id="edit-form-call_dt-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <label
                                            for="edit-form-duration">{{ __('lang.call_history_call_duration_bis') }}</label>
                                        <input type="number" class="form-control form-control-alt"
                                            id="edit-form-lead-duration" name="edit-form-duration"
                                            placeholder="{{ __('lang.call_history_duration_placeholder') }}"
                                            aria-describedby="edit-form-duration-error" aria-invalid="false">
                                        <div id="edit-form-duration-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-call_descr">Description</label>
                                        <textarea type="text" class="form-control form-control-alt" id="edit-form-lead-call_descr"
                                            name="edit-form-call_descr" placeholder="Description.." aria-describedby="edit-form-call_descr-error"
                                            aria-invalid="false"></textarea>
                                        <div id="edit-form-call_descr-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block"
                                            id="update-lead-histocall-btn"><i
                                                class="fa fa-fw fa-plus mr-1"></i>{{ __('lang.call_history_edit_call') }}</button>
                                    </div>
                                </form>
                                <!-- END Form histocall Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1"
                            data-dismiss="modal">{{ __('lang.call_history_close') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END histocall Edit Modal -->


    <!-- histocall Customer Add Modal -->
    <div class="modal" id="modal-histocall-customer-add" tabindex="-1" role="dialog"
        aria-labelledby="modal-histocall-customer-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-histocall-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">{{ __('lang.call_history_add_client_call') }}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form histocall customer Add -->
                                <form action="" method="POST" onsubmit="return false;"
                                    id="histocall-add-customer-form">
                                    <div class="form-group">
                                        <label for="add-form-customer_id">Client</label>
                                        <select class="form-control form-control-alt" id="add-form-customer_id"
                                            name="add-form-customer_id" aria-describedby="add-form-customer_id-error"
                                            aria-invalid="false">
                                            <option value="">{{ __('lang.call_history_select_client') }}</option>
                                            @foreach (\App\Models\Customer::all() as $customer)
                                                <option value="{{ $customer->id }}">{{ $customer->firstname }}
                                                    {{ $customer->lastname }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-customer_id-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="add-form-call_dt">{{ __('lang.call_history_call_date') }}</label>
                                        <input type="date" class="form-control form-control-alt" id="add-form-call_dt"
                                            name="add-form-call_dt" aria-describedby="add-form-call_dt-error"
                                            aria-invalid="false">
                                        <div id="add-form-call_dt-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <label
                                            for="add-form-duration">{{ __('lang.call_history_call_duration_bis') }}</label>
                                        <input type="number" class="form-control form-control-alt"
                                            id="add-form-duration" name="add-form-duration"
                                            placeholder="{{ __('lang.call_history_duration_placeholder') }}"
                                            aria-describedby="add-form-duration-error" aria-invalid="false">
                                        <div id="add-form-duration-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-call_descr">Description</label>
                                        <textarea type="text" class="form-control form-control-alt" id="add-form-call_descr" name="add-form-call_descr"
                                            placeholder="Description.." aria-describedby="add-form-call_descr-error" aria-invalid="false"></textarea>
                                        <div id="add-form-call_descr-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block"
                                            id="add-histocall-customer-btn"><i
                                                class="fa fa-fw fa-plus mr-1"></i>{{ __('lang.call_history_add_call') }}</button>
                                    </div>
                                </form>
                                <!-- END Form histocall Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1"
                            data-dismiss="modal"{{ __('lang.call_history_close') }}></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END histocall Add Modal -->

    <!-- histocall Customer Edit Modal -->
    <div class="modal" id="modal-histocall-customer-edit" tabindex="-1" role="dialog"
        aria-labelledby="modal-histocall-customer-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-customer-histocall-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">{{ __('lang.call_history_edit_client_call') }}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form histocall client Edit -->
                                <form action="" method="POST" onsubmit="return false;"
                                    id="histocall-edit-customer-form">
                                    <div class="form-group">
                                        <label for="edit-form-customer_id">Client</label>
                                        <select class="form-control form-control-alt" id="edit-form-customer_id"
                                            name="edit-form-customer_id" aria-describedby="add-form-customer_id-error"
                                            aria-invalid="false">
                                            <option value="">{{ __('lang.call_history_select_client') }}</option>
                                            @foreach (\App\Models\Customer::all() as $customer)
                                                <option value="{{ $customer->id }}">{{ $customer->firstname }}
                                                    {{ $customer->lastname }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-customer_id-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-call_dt">{{ __('lang.call_history_call_date') }}</label>
                                        <input type="date" class="form-control form-control-alt"
                                            id="edit-form-customer-call_dt" name="edit-form-call_dt"
                                            aria-describedby="edit-form-call_dt-error" aria-invalid="false">
                                        <div id="edit-form-call_dt-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <label
                                            for="edit-form-duration">{{ __('lang.call_history_call_duration_bis') }}</label>
                                        <input type="number" class="form-control form-control-alt"
                                            id="edit-form-customer-duration" name="edit-form-duration"
                                            placeholder="{{ __('lang.call_history_duration_placeholder') }}"
                                            aria-describedby="edit-form-duration-error" aria-invalid="false">
                                        <div id="edit-form-duration-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-call_descr">Description</label>
                                        <textarea type="text" class="form-control form-control-alt" id="edit-form-customer-call_descr"
                                            name="edit-form-call_descr" placeholder="Description.." aria-describedby="edit-form-call_descr-error"
                                            aria-invalid="false"></textarea>
                                        <div id="edit-form-call_descr-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block"
                                            id="update-customer-histocall-btn"><i
                                                class="fa fa-fw fa-plus mr-1"></i>{{ __('lang.call_history_edit_call') }}</button>
                                    </div>
                                </form>
                                <!-- END Form histocall Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1"
                            data-dismiss="modal">{{ __('lang.call_history_close') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END histocall Edit Modal -->


@endsection
