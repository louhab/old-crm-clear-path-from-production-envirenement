@if ($elements['email_phase'] === 1 && $elements['conseiller'])
    @if ($elements['lead_lang'] === 'en')
        <p>Hello, Mr./Ms.{{ $name }},</p>
        <p>I am {{ $elements['conseiller']->gender === 'female' ? 'Ms.' : 'Mr.' }} {{ $elements['conseiller']->name }},
            your {{ $elements['conseiller']->gender === 'female' ? 'advisor.' : 'advisor.' }}<br>It is our pleasure to
            welcome you to Clear Path Consulting.</p>
        @if ($elements['lead']->to_office == 'office')
            <p>I would like to invite you to send me a message by email or Whatsapp, letting me know<br>the time that
                suits you better to set a meeting at our office.</p>
        @else
            <p>I would like to invite you to send me a message by email or WhatsApp, to talk about<br>when we can set
                our first meeting by video-conference.</p>
        @endif
        <p>
            <strong>WhatsApp : {{ $elements['conseiller']->phone }}</strong><br>
            <strong>E-mail : {{ $elements['conseiller']->email }}</strong>
        </p>
        @if (!$elements['pole'])
            <p>
                Address : 42, rue Bachir Laalej, Casablanca - Morocco<br>
                Map location : https://goo.gl/maps/nB5hMXDQKrorCoVX8<br>
                Monday through Friday, from 9:30 am to 7:30 pm. Saturday, from 10:00 am to 6:30 pm
            </p>
        @else
            <p>
                Address : {{ $elements['pole']->address }}<br>
                Map location : {{ $elements['pole']->location }}<br>
                Monday through Friday, from 9:30 am to 7:30 pm. Saturday, from 10:00 am to 6:30 pm.
            </p>
        @endif

        <p>See you soon!</p>
        <p>
            <strong>{{ $elements['conseiller']->name }}</strong><br>
            Clear Path Consulting<br>
            Tel: {{ $elements['conseiller']->phone }}<br>
            E-mail: {{ $elements['conseiller']->email }}<br>
            Our Agencies: https://clearpath-eu.com/contact
        </p>
    @else
        <p>Bonjour, {{ $name }},</p>
        <p>Je suis {{ $elements['conseiller']->name }} votre
            {{ $elements['conseiller']->gender === 'female' ? 'conseillère' : 'conseiller' }} chez Clear Path
            Consulting.
        </p>
        @if ($elements['lead']->to_office == 'office')
            <p>Je serai ravi de vous accueillir dans l'agence à l'heure qui vous convient.</p>
        @else
            <p>Je vous invite à m'envoyer l'heure qui vous convient par e-mail ou message WhatsApp pour passer la
                rencontre par vidéo-conférence.</p>
        @endif
        <p>
            <strong>WhatsApp : {{ $elements['conseiller']->phone }}</strong><br>
            <strong>E-mail : {{ $elements['conseiller']->email }}</strong>
        </p>
        @if (!$elements['pole'])
            <p>
                Adresse : 42, rue Bachir Laalej, Casablanca - Maroc<br>
                Localisation : https://goo.gl/maps/nB5hMXDQKrorCoVX8<br>
                Heures d'ouverture : Lundi au vendredi de 9h30-19h30. Samedi de 10h-18h30
            </p>
        @else
            <p>
                Adresse : {{ $elements['pole']->address }}<br>
                Localisation : {{ $elements['pole']->location }}<br>
                Heures d'ouverture : Lundi au vendredi de 9h30-19h30. Samedi de 10h-18h30
            </p>
        @endif

        <p>
            <br>
        </p>

        <p>A très bientôt !</p>
        <p>
            <strong>{{ $elements['conseiller']->name }}</strong><br>
            Clear Path Consulting<br>
            Tel: {{ $elements['conseiller']->phone }}<br>
            E-mail: {{ $elements['conseiller']->email }}<br>
            Nos Agences : https://clearpath-eu.com/contact
        </p>
    @endif
@elseif ($elements['email_phase'] === 2 && $elements['conseiller'])
    @if ($elements['lead_lang'] === 'en')
        <p>Hello, {{ $name }},</p>
        <p>We thank you for trusting CLEAR PATH Consulting.</p>
        <p>We invite you to join the customer area through the following
            link:<br>https://crm.clearpathcanada.com/login/customer</p>
        <p><strong>User: {{ $elements['customer']->phone }}<br>
                Password: {{ $elements['customer']->phone }}</strong></p>
        <p>
            Through the virtual portal, you can access to all the information related to the progress of your profile.
        </p>
        <p>We wish you have a gratifying day,</p>
        <p>
            <strong>{{ $elements['conseiller']->name }}</strong><br>
            {{ $elements['conseiller']->phone }}<br>
            {{ $elements['conseiller']->email }}
        </p>
    @else
        <p>Bonjour, {{ $name }},</p>
        <p>Nous vous remercions d'avoir fait confiance à CLEAR PATH Consulting et pour l'intérêt que vous portez à notre
            Agence.</p>
        <p>Je suis {{ $elements['conseiller']->name }} votre
            {{ $elements['conseiller']->gender === 'female' ? 'conseillère' : 'conseiller' }}, <br>je vous invite à
            rejoindre votre espace client en vous connectant sur notre Portail à travers le lien ci-dessous
            :<br>https://crm.clearpathcanada.com/login/customer</p>
        <p><strong>Nom d'utilisateur: {{ $elements['customer']->phone }}<br>
                Mot de passe: {{ $elements['customer']->phone }}</strong></p>
        <p>Restez connecter pour toutes informations sur l'état d'avancement de votre dossier.<br>Vous pourriez ainsi
            communiquer avec moi à travers ce portail.</p>
        <p>Bien à vous.</p>
        <p>
            <strong>{{ $elements['conseiller']->name }}</strong><br>
            {{ $elements['conseiller']->phone }}<br>
            {{ $elements['conseiller']->email }}
        </p>
    @endif
@elseif ($elements['email_phase'] === 3)
    {!! $body !!}
@else
    Bonjour {{ $name }}
@endif
