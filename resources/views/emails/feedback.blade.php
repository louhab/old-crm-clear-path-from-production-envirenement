<p>
<p>Bonjour, {{ $name }}</p>
Dans le cadre de l'amélioration de nos services, nous souhaiterions recueillir votre avis quant à la qualité de nos
prestations fournies.<br>
Accordez nous quelques minutes pour répondre à ce questionnaire qui nous aidera à améliorer la qualité de nos
services.<br>
<strong><a href={{ 'https://crm.clearpathcanada.com/feedback?id=' . $id }} target="_blank">Découvrir</a></strong><br>
En vous remerciant par avance.
</p>
