<p>
<p>Bonjour,</p>
Une réponse au questionnaire de satisfaction a été reçue de la part de <strong>{{ $name }}</strong>, avec le
numéro de téléphone: <strong>{{ $phone }}</strong> <br>
conseiller / conseillère : <strong>{{ $advisor }}</strong> <br>
Q - Amabilité et degré du professionnalisme de notre personnel ? <br>
R - <strong>{{ $q1 ?: 'n/d' }}</strong> sur 10 <br><br>

Q - Qualité des présentations, explications et votre degré de compréhension ? <br>
R - <strong>{{ $q2 ?: 'n/d' }}</strong> sur 10 <br><br>

Q - Compétence et degré de maitrise de votre conseiller ? <br>
R - <strong>{{ $q3 ?: 'n/d' }}</strong> sur 10 <br><br>

Q - Rapidité, efficacité et délai de traitement de votre dossier ? <br>
R - <strong>{{ $q4 ?: 'n/d' }}</strong> sur 10 <br><br>

Q - Globalement comment évaluez - vous votre expérience au sein de notre agence ? <br>
R - <strong>{{ $q5 ?: 'n/d' }}</strong> sur 10 <br><br>


Q - Y a-t-il d'autres observations et/ou suggestions que vous aimeriez nous transmettre? <br>
R - <strong>{{ $note ?: 'n/d' }}</strong> <br><br>


<br>
</p>
