@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/fields/list.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Fields','subTitle' => 'configuration', 'navItems' => ['Admin', 'Fields']])

    <!-- Page Content -->
    <div class="content">

        <!-- leadsforms Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                    <form id="search-form" name="search-form" action="" method="post" onsubmit="return false">
                        <div class="form-group form-row">
                            <div class="col-3 p-1">
                                <label for="name_en" class="sr-only">Field name</label>
                                <input class="form-control dt_search_field" id="name_en" name="name_en" style="height:44px" placeholder="Nom..">
                            </div>
                            <div class="col-3 p-1">
                                <label for="group" class="sr-only">Groupe</label>
                                <select class="form-control form-control-alt dt_search_field" id="group" name="group">
                                    <option value="">Sélectionnez groupe</option>
                                    @foreach(\App\Models\CustomerFieldGroup::all() as $group)
                                        <option value="{{ $group->id }}">{{ $group->name_fr }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-3 p-1">
                                <label for="sheet" class="sr-only">Sheet</label>
                                <select class="form-control form-control-alt dt_search_field" id="sheet" name="sheet">
                                    <option value="">Sélectionnez clear</option>
                                    @foreach(\App\Models\InformationSheet::all() as $sheet)
                                        <option value="{{ $sheet->id }}">{{ $sheet->name_fr }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-3 p-1">
                                <label for="program" class="sr-only">Programme</label>
                                <select class="form-control form-control-alt dt_search_field" id="program" name="program">
                                    <option value="">Sélectionnez groupe</option>
                                    @foreach(\App\Models\Program::all() as $program)
                                        <option value="{{ $program->id }}">{{ $program->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search" id="search-btn"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search" id="clear-search-btn"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>

                        </div>

                    </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="fields-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Customer Fields</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary"  id="fields-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter field</button>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="fields-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END leadsforms Table -->




    </div>
    <!-- END Page Content -->

    <!-- leadsforms Lead Add Modal -->
    <div class="modal" id="modal-fields-add" tabindex="-1" role="dialog" aria-labelledby="modal-fields-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-fields-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter field</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form fields lead Add -->
                                <form action="" method="POST" onsubmit="return false;" id="fields-add-form">
                                    <div class="form-group">
                                        <label for="add-form-title-fr">Field name</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="add-form-title-fr" name="add-form-title-fr" aria-describedby="add-form-title-fr-error" aria-invalid="false" placeholder="name..">
                                        <div id="add-form-title-fr-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-group">Groupe</label>
                                        <select class="form-control form-control-alt" id="add-form-group" name="add-form-group" aria-describedby="add-form-group-error" aria-invalid="false">
                                            <option value="">Sélectionnez groupe</option>
                                            @foreach(\App\Models\CustomerFieldGroup::all() as $group)
                                                <option value="{{ $group->id }}">{{ $group->name_fr }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-group-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-template">Template</label>
                                        <select class="form-control form-control-alt" id="add-form-template" name="add-form-template" aria-describedby="add-form-template-error" aria-invalid="false">
                                            <option value="">Sélectionnez template</option>
                                            <option value="text">text</option>
                                            <option value="date">date</option>
                                            <option value="email">email</option>
                                            <option value="number">number</option>
                                            <option value="select">select</option>
                                            <option value="radio">radio</option>
                                            <option value="tel">tel</option>
                                            <option value="textarea">textarea</option>
                                        </select>
                                        <div id="add-form-template-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-model">Model</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="add-form-model" name="add-form-model" aria-describedby="add-form-model-error" aria-invalid="false" placeholder="model..">
                                        <div id="add-form-model-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-model-label">Model label</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="add-form-model-label" name="add-form-model-label" aria-describedby="add-form-model-label-error" aria-invalid="false" placeholder="label..">
                                        <div id="add-form-model-label-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-model-id">Model ID</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="add-form-model-id" name="add-form-model-id" aria-describedby="add-form-model-id-error" aria-invalid="false" placeholder="id..">
                                        <div id="add-form-model-id-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-fields-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter field</button>
                                    </div>
                                </form>
                                <!-- END Form fields Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END leadsforms Add Modal -->
    <!-- leadsforms Lead Edit Modal -->
    <div class="modal" id="modal-field-edit" tabindex="-1" role="dialog" aria-labelledby="modal-field-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-field-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Editer field</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form leadsforms lead edit -->
                                <form action="" method="POST" onsubmit="return false;" id="fields-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-title-fr">Field name</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-title-fr" name="edit-form-title-fr" aria-describedby="edit-form-title-fr-error" aria-invalid="false" placeholder="name..">
                                        <div id="edit-form-title-fr-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-group">Groupe</label>
                                        <select class="form-control form-control-alt" id="edit-form-group" name="edit-form-group" aria-describedby="edit-form-group-error" aria-invalid="false">
                                            <option value="">Sélectionnez groupe</option>
                                            @foreach(\App\Models\CustomerFieldGroup::all() as $group)
                                                <option value="{{ $group->id }}">{{ $group->name_fr }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-group-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-template">Template</label>
                                        <select class="form-control form-control-alt" id="edit-form-template" name="edit-form-template" aria-describedby="edit-form-template-error" aria-invalid="false">
                                            <option value="">Sélectionnez template</option>
                                            <option value="text">text</option>
                                            <option value="date">date</option>
                                            <option value="email">email</option>
                                            <option value="number">number</option>
                                            <option value="select">select</option>
                                            <option value="radio">radio</option>
                                            <option value="tel">tel</option>
                                            <option value="textarea">textarea</option>
                                        </select>
                                        <div id="edit-form-template-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-model">Model</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-model" name="edit-form-model" aria-describedby="edit-form-model-error" aria-invalid="false" placeholder="model..">
                                        <div id="edit-form-model-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-model-label">Model label</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-model-label" name="edit-form-model-label" aria-describedby="edit-form-model-label-error" aria-invalid="false" placeholder="label..">
                                        <div id="edit-form-model-label-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-model-id">Model ID</label>
                                        <span class="text-danger">*</span>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-model-id" name="edit-form-model-id" aria-describedby="edit-form-model-id-error" aria-invalid="false" placeholder="id..">
                                        <div id="edit-form-model-id-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="edit-field-btn"><i class="fa fa-fw fa-plus mr-1"></i>Editer field</button>
                                    </div>
                                </form>
                                <!-- END Form leadsforms Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END leadsforms Add Modal -->

@endsection
