@extends('layouts.customer')

@section('js_after')
    <!-- Page JS Plugins -->

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/myinbox.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'INBOX','subTitle' => 'mes messages', 'navItems' => ['Inbox', 'Messages']])

    <!-- Page Content -->
    <div class="content">
        <div class="row">
            <div class="col-md-7 col-xl-9">
                <!-- Message List -->
                <div class="block block-rounded">
                    <div class="block-header block-header-default">
                        <button class="btn btn-alt-primary btn-sm" id="back-to-inbox-btn" style="display: none;"><< Inbox</button>
                        <h3 class="block-title" id="message-block-title-0" style="display: none; text-align: center;">Normal</h3>
                        <h3 class="block-title" id="message-block-title">
                            {{ $messages->onFirstPage() ? "1" : 15 * ($messages->currentPage() - 1) + 1 }}-{{ min(15 * $messages->currentPage(), $messages->total()) }} <span class="font-w400 text-lowercase">from</span> {{ $messages->total() }}
                        </h3>
                        <div class="block-options">
                            <div id="vendor-pagination-wrapper" style="display: inline;">{{ $messages->links('vendor.pagination.inbox') }}</div>
                            <button type="button" class="btn-block-option" id="message-refresh" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                <i class="si si-refresh"></i>
                            </button>
                            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="fullscreen_toggle"></button>
                        </div>
                    </div>
                    <div class="block-content">
                        <!-- Messages Options -->
<!--                        <div class="d-flex justify-content-between push">
                            <div class="btn-group">
                            </div>
                            <button class="btn btn-sm btn-light" type="button">
                                <i class="fa fa-times text-danger"></i>
                                <span class="d-none d-sm-inline ml-1">Delete</span>
                            </button>
                        </div>-->
                        <!-- END Messages Options -->

                        <!-- Messages and Checkable Table (.js-table-checkable class is initialized in Helpers.tableToolsCheckable()) -->
                        <div class="pull-x" id="messages-list">
                            <table class="js-table-checkable table table-hover table-vcenter font-size-sm" >
                                <tbody>
                                    @foreach($messages as $message)
                                        <tr>
                                            <td class="d-none d-sm-table-cell font-w600 text-center" style="width: 140px;">{{ $message->user->name }}</td>
                                            <td>
                                                <a class="font-w600 message-subject-link" data-content="{{ $message->message }}" data-object="{{ $message->object }}" data-toggle="modal" data-target="#one-inbox-message" href="#">{{ $message->object }}</a>
                                                <div class="text-muted mt-1">{{ substr($message->message, 0, 50) }}</div>
                                            </td>
                                            <td class="d-none d-xl-table-cell text-muted" style="width: 120px;">
                                                <em>{{ $message->created_at }}</em>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <p id="message-content" style="display: none;"></p>
                        <!-- END Messages and Checkable Table -->
                    </div>
                </div>
                <!-- END Message List -->
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
