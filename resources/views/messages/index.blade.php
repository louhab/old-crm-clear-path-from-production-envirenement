@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/messages.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Historique','subTitle' => 'messages', 'navItems' => ['Admin', 'Messages']])

    <!-- Page Content -->
    <div class="content">

        <!-- messages Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">
                            {{--<div class="col-3">
                                <select class="form-control dt_search_field" name="ranking">
                                    <option value="">Sélectionnez un ranking</option>
                                    @foreach(\App\Models\Ranking::all() as $ranking)
                                        <option value="{{ $ranking->id }}">{{ $ranking->ranking_label }}</option>
                                    @endforeach
                                </select>
                            </div>--}}
                            <div class="col-4">
                                <input type="text" class="form-control dt_search_field" name="name" placeholder="Nom">
                            </div>
                            <div class="col-4">
                                <input type="text" class="form-control dt_search_field" name="email" placeholder="Email">
                            </div>
                            <div class="col-3">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="messages-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Messages</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary"  id="message-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter message</button>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="messages-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END messages Table -->




    </div>
    <!-- END Page Content -->

    <!-- message Add Modal -->
    <div class="modal" id="modal-message-add" tabindex="-1" role="dialog" aria-labelledby="modal-message-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-message-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter message</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form message Add -->
                                <form action="" method="POST" onsubmit="return false;" id="message-add-form">
                                    <div class="form-group">
                                        <label for="add-form-customer_id">Client</label>
                                        <select class="form-control form-control-alt" id="add-form-customer_id" name="add-form-customer_id" aria-describedby="add-form-customer_id-error" aria-invalid="false">
                                            <option value="">Sélectionnez un client</option>
                                            @foreach(\App\Models\Customer::all() as $customer)
                                                <option value="{{ $customer->id }}">{{ $customer->firstname }} {{ $customer->lastname }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-customer_id-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-object">Objet</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-object" name="add-form-object" placeholder="Objet.." aria-describedby="add-form-object-error" aria-invalid="false">
                                        <div id="add-form-object-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-message">Message</label>
                                        <textarea type="text" class="form-control form-control-alt" id="add-form-message" name="add-form-message" placeholder="Message.." aria-describedby="add-form-message-error" aria-invalid="false"></textarea>
                                        <div id="add-form-message-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-info btn-block" id="add-message-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter message</button>
                                    </div>
                                </form>
                                <!-- END Form message Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END message Add Modal -->

    <!-- message Edit Modal -->
    <div class="modal" id="modal-message-edit" tabindex="-1" role="dialog" aria-labelledby="modal-message-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-message-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Modifier message</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form message Add -->
                                <form action="" method="POST" onsubmit="return false;" id="message-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-customer_id">Client</label>
                                        <select class="form-control form-control-alt" id="edit-form-customer_id" name="edit-form-customer_id" aria-describedby="add-form-customer_id-error" aria-invalid="false">
                                            <option value="">Sélectionnez un client</option>
                                            @foreach(\App\Models\Customer::all() as $customer)
                                                <option value="{{ $customer->id }}">{{ $customer->firstname }} {{ $customer->lastname }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-customer_id-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-object">Objet</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-object" name="edit-form-object" placeholder="Objet.." aria-describedby="edit-form-object-error" aria-invalid="false">
                                        <div id="edit-form-object-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-message">Message</label>
                                        <textarea type="text" class="form-control form-control-alt" id="edit-form-message" name="edit-form-message" placeholder="Message.." aria-describedby="edit-form-message-error" aria-invalid="false"></textarea>
                                        <div id="edit-form-message-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-info btn-block" id="update-message-btn"><i class="fa fa-fw fa-check mr-1"></i>Modifier message</button>
                                    </div>
                                </form>
                                <!-- END Form message Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END message Edit Modal -->
@endsection
