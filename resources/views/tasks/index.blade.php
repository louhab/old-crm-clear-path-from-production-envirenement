<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
@extends('layouts.backend')

@section('js_after')
    @livewireScripts
    <script src="{{ asset('js/pages/tasks.js') }}"></script>
@endsection


@section('content')
    @include('layouts.partials.hero', ['title' => __('lang.tasks'),'subTitle' => 'Lists', 'navItems' => ['Admin', __('lang.tasks')]])

    <!-- Page Content -->
    <div class="content">
        <input type="hidden" name="lang" value="{{ $lang }}">

        <div class="block block-rounded" id="cases-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">{{ __("lang.tasks-list") }}</h3>
                </div>
                @livewire('block.todos')
            </div>
        </div>
        <!-- END histocalls Table -->



    </div>
    <!-- END Page Content -->


@endsection
