@if ($paginator->hasPages())
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
        <button class="btn-block-option" data-toggle="tooltip" data-placement="left" title="Previous 15 Messages" disabled="">
            <i class="si si-arrow-left"></i>
        </button>
    @else
        <a href="{{ $paginator->previousPageUrl() }}" class="btn-block-option disabled" rel="prev" aria-label="@lang('pagination.previous')" data-toggle="tooltip" data-placement="left" title="Previous 15 Messages">
            <i class="si si-arrow-left"></i>
        </a>
    @endif

    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
        <a href="{{ $paginator->nextPageUrl() }}" class="btn-block-option" rel="next" aria-label="@lang('pagination.next')" data-toggle="tooltip" data-placement="left" title="Previous 15 Messages">
            <i class="si si-arrow-right"></i>
        </a>
    @else
        <button class="btn-block-option" data-toggle="tooltip" data-placement="left" title="Previous 15 Messages" disabled="">
            <i class="si si-arrow-right"></i>
        </button>
    @endif
@endif
