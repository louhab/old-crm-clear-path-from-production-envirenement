@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/services.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Services','subTitle' => 'Etapes', 'navItems' => ['Admin', 'services']])

    <!-- Page Content -->
    <div class="content">

        <!-- services Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">
                            <div class="col-3">
                                <select class="form-control dt_search_field" name="ranking">
                                    <option value="">Sélectionnez une phase</option>
                                    @foreach(\App\Models\Phase::all() as $phase)
                                        <option value="{{ $phase->id }}">{{ $phase->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="services-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">services</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary" id="service-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter service</button>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="services-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END services Table -->




    </div>
    <!-- END Page Content -->

    <!-- service Add Modal -->
    <div class="modal" id="modal-service-add" tabindex="-1" role="dialog" aria-labelledby="modal-service-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-service-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter service</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form service Add -->
                                <form action="" method="POST" onsubmit="return false;" id="service-add-form">
                                    <div class="form-group">
                                        <label for="add-form-phase_id">Phase</label>
                                        <select class="form-control form-control-alt" id="add-form-phase_id" name="add-form-phase_id" aria-describedby="add-form-phase_id-error" aria-invalid="false">
                                            <option value="">Sélectionnez une phase</option>
                                            @foreach(\App\Models\Phase::all() as $phase)
                                                <option value="{{ $phase->id }}">{{ $phase->name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-phase_id-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="add-form-name">Nom</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-name" name="add-form-name" placeholder="Nom.." aria-describedby="add-form-name-error" aria-invalid="false">
                                        <div id="add-form-name-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-single_price">Single price</label>
                                        <input type="number" class="form-control form-control-alt" id="add-form-single_price" name="add-form-single_price" placeholder="Prix single.." aria-describedby="add-form-single_price-error" aria-invalid="false">
                                        <div id="add-form-single_price-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-couple_price">Couple price</label>
                                        <input type="number" class="form-control form-control-alt" id="add-form-couple_price" name="add-form-couple_price" placeholder="Prix couple.." aria-describedby="add-form-couple_price-error" aria-invalid="false">
                                        <div id="add-form-couple_price-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-service-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter service</button>
                                    </div>
                                </form>
                                <!-- END Form service Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END service Add Modal -->

    <!-- service Edit Modal -->
    <div class="modal" id="modal-service-edit" tabindex="-1" role="dialog" aria-labelledby="modal-service-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-service-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Modifier service</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form service Edit -->
                                <form action="" method="POST" onsubmit="return false;" id="service-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-phase_id">Phase</label>
                                        <select class="form-control form-control-alt" id="edit-form-phase_id" name="edit-form-phase_id" aria-describedby="edit-form-phase_id-error" aria-invalid="false">
                                            <option value="">Sélectionnez une phase</option>
                                            @foreach(\App\Models\Phase::all() as $phase)
                                                <option value="{{ $phase->id }}">{{ $phase->name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-phase_id-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-name">Nom</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-name" name="edit-form-name" placeholder="Nom.." aria-describedby="edit-form-name-error" aria-invalid="false">
                                        <div id="edit-form-name-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group form-row">
                                        <div class="col-12">
                                            <label>Single price</label>
                                        </div>
                                        <div class="col-4">
                                            <div class="input-group">
                                                <input type="number" class="form-control form-control-alt" id="edit-form-single_price-mad" name="edit-form-single_price-mad">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-alt">
                                                        MAD
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="input-group">
                                                <input type="number" class="form-control form-control-alt" id="edit-form-single_price-eur" name="edit-form-single_price-eur">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-alt">
                                                        EUR
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="input-group">
                                                <input type="number" class="form-control form-control-alt" id="edit-form-single_price-usd" name="edit-form-single_price-usd">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-alt">
                                                        USD
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-row">
                                        <div class="col-12">
                                            <label>Couple price</label>
                                        </div>
                                        <div class="col-4">
                                            <div class="input-group">
                                                <input type="number" class="form-control form-control-alt" id="edit-form-couple_price-mad" name="edit-form-couple_price-mad">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-alt">
                                                        MAD
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="input-group">
                                                <input type="number" class="form-control form-control-alt" id="edit-form-couple_price-eur" name="edit-form-couple_price-eur">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-alt">
                                                        EUR
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="input-group">
                                                <input type="number" class="form-control form-control-alt" id="edit-form-couple_price-usd" name="edit-form-couple_price-usd">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-alt">
                                                        USD
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <label for="edit-form-single_price">Single price</label>
                                        <input type="number" class="form-control form-control-alt" id="edit-form-single_price" name="edit-form-single_price" placeholder="Prix single.." aria-describedby="edit-form-single_price-error" aria-invalid="false">
                                        <div id="edit-form-single_price-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-couple_price">Couple price</label>
                                        <input type="number" class="form-control form-control-alt" id="edit-form-couple_price" name="edit-form-couple_price" placeholder="Prix couple.." aria-describedby="edit-form-couple_price-error" aria-invalid="false">
                                        <div id="edit-form-couple_price-error" class="invalid-feedback"></div>
                                    </div>-->

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="update-service-btn"><i class="fa fa-fw fa-check mr-1"></i>Modifier service</button>
                                    </div>
                                </form>
                                <!-- END Form service Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END service Edit Modal -->
@endsection
