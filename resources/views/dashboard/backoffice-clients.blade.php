<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- <script src="{{ asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script> -->
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/dashboard/backoffice-clients.js') }}"></script>
@endsection

@section('css_after')
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- <link href="{{ asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <style>
        .select2-selection__rendered {
            line-height: 39px !important;
        }
        .select2-container .select2-selection--single {
            height: 42px !important;
        }
        .select2-selection__arrow {
            height: 43px !important;
        }
        /*compaigns*/
        .card:hover{
            transform: scale(1.05);
            box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
            cursor:pointer;
        }
    </style>
@endsection

@section('content')
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-md-row justify-content-md-between py-2 text-md-start">
                <div class="flex-grow-1 mb-1 mb-md-0">
                    <h1 class="h3 fw-bold mb-2">
                        {{__('lang.dashboard')}}
                    </h1>
                    <h2 class="h6 fw-medium fw-medium text-muted mb-0">
                        {{ $lang === "fr" ? "Bonjour" : "Hello" }} <strong>@php echo ucfirst(Auth::user()->role->role_name); @endphp</strong>
                    </h2>
                </div>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="">{{ __('lang.dashboard')}}</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="content">
        @management
        <div class="block-content">
            <!-- Overview -->
            @administration
            <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding-left: 0; padding-right: 0; ">
                <h5 class="text-muted">{{ __('lang.filtre_globale')}}</h5>
                <div class="block-options search-filter" style="width:70%;">
                    <form action="" method="post" onsubmit="return false">
                        @csrf
                        <div class="form-group form-row d-flex justify-content-end">
                            <div class="col">
                                <label for="programme" class="sr-only">Programmes...</label>
                                <select class="form-control dt_search_field dt_search_program" id="program" name="program">
                                </select>
                            </div>
                            <div class="col">
                                <label for="agent" class="sr-only">Utilisateurs...</label>
                                <select class="form-control dt_search_field dt_search_agent" id="agent" name="agent">
                                </select>
                                <input type="hidden" class="dt_search_field" name="agent-group" value="">
                            </div>
                            <div class="col">
                                <label for="campaign" class="sr-only">Canal...</label>
                                <select class="form-control dt_search_field dt_search_campaign" id="campaign" name="campaign">
                                </select>
                            </div>
                            <div class="">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text input-group-text-alt">
                                                <i class="far fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        @php
                                            $firstDay = \Carbon\Carbon::now()->firstOfMonth()->format('m/d/Y');
                                            $lastDay = \Carbon\Carbon::now()->lastOfMonth()->format('m/d/Y');
                                            $current_month = $firstDay . " - " . $lastDay;
                                        @endphp
                                        <input type="text" class="form-control form-control" id="daterange" name="daterange" value="{{ $current_month }}">
                                        <input type="hidden" name="date_end" />
                                        <input type="hidden" name="date_start" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endadministration
        </div>
        <div class="block-content">
            <!-- Overview -->
            <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding-left: 0; padding-right: 0; ">
                <h4 class="text-muted">
                    Clients - {{ $lang === "fr" ? "Etapes" : "Steps" }}
                </h4>
            </div>
            <div class="row row-deck">
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-clients-filter">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-customers">*</dt>
                                <dd class="text-muted mb-0">{{__('lang.customers')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-clients-filter" data-filter="calls" data-value="">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-calls">*</dt>
                                <dd class="text-muted mb-0">{{__('lang.Appels')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-clients-filter" data-filter="calls">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-durations">*</dt>
                                <dd class="text-muted mb-0">{{__('lang.Durée_appels')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-messages">*</dt>
                                <dd class="text-muted mb-0">{{__('lang.messages')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 col-xl-4"></div>
                @php
                //ProgramStep Models

                if(session()->get('locale')=='fr'){$steps =\App\Models\ProgramStep::pluck('step_label', 'id');}else{$steps =\App\Models\ProgramStep::pluck('labelEng', 'id');}
               @endphp
                @foreach($steps as $id => $step)
                    @if(!in_array($id, [1, 2]))
                    <div class="col-sm-4 col-xl-2">
                        <div class="block block-rounded d-flex flex-column list-clients-filter" data-filter="calls">
                            <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                <dl class="mb-0">
                                    <dt class="font-size-h2 font-w700 dashboard-analytics-step_{{ $id }}">*</dt>
                                    <dd class="text-muted mb-0">{{ $step}}</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
        @administration
        <div class="block-content">
            <!-- Overview -->
            <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding-left: 0; padding-right: 0; ">
                <h5 class="text-muted">{{__('lang.Statistiques_compagne_de_marketing')}}</h5>
            </div>
            <div class="row row-deck">
                @php
                    $campaigns = \App\Models\LeadFormMarketingCampaign::pluck("campaign_name", "id");
                @endphp
                @foreach($campaigns as $id => $campaign)
                    <div class="col-sm-4 col-xl-2">
                        <div class="block block-rounded d-flex flex-column @if($campaign=='Influenceurs' || $campaign=='Reference') card @endif">
                            <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                <dl class="mb-0">
                                    <dt class="font-size-h2 font-w700 dashboard-analytics-campaign_{{ $id }}">*</dt>
                                    <dd class="text-muted mb-0">{{ $campaign }}</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="block-content">
            <!-- Overview -->
            <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding-left: 0; padding-right: 0; ">
                <h5 class="text-muted">Finance</h5>
            </div>
            <div class="row row-deck">
                <div class="col-sm-6 col-xl-4">
                    <div class="block block-rounded d-flex flex-column">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-quotes">* MAD</dt>
                                <dd class="text-muted mb-0">CA </dd>
                                <dd class="text-muted mb-0 dashboard-analytics-quotes">Total: 0 Payments</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                @foreach(\App\Models\ProgramPayment::all() as $payment)
                    <div class="col-sm-4 col-xl-4">
                        <div class="block block-rounded d-flex flex-column">
                            <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                <dl class="mb-0">
                                    <dt class="font-size-h2 font-w700 dashboard-analytics-payment_{{ $payment->id }}">0 MAD</dt>
                                    <dd class="text-muted mb-0">{{ $payment->name_fr }} </dd>
                                    <dd class="text-muted mb-0 dashboard-analytics-payment_{{ $payment->id }}">Total: 0 Payments</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @endadministration
        @endmanagement
    </div>
    <div class="modal" id="modal-campaigns" tabindex="-1" role="dialog" aria-labelledby="modal-campaigns" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Campaign Title</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Prospects</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
