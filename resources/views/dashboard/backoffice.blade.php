<?php
    $lang = session()->get('locale') === 'en' ? "en" : "fr";
?>
@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- <script src="{{ asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script> -->
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <!-- Page JS Code -->
   <script src="{{ asset('js/pages/dashboard.js') }}"></script>
    <script>
        /*var data = {
            id: 0,
            text: 'Non Traité',
            selected: false
        };
        var newOption = new Option(data.text, data.id, false, false);
        $('#state').prepend(newOption).trigger('change');*/
    </script>
@endsection

@section('css_after')
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- <link href="{{ asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    <style>
        .select2-selection__rendered {
            line-height: 39px !important;
        }
        .select2-container .select2-selection--single {
            height: 42px !important;
        }
        .select2-selection__arrow {
            height: 43px !important;
        }
        /*compaigns*/
        .list-leads-filter:hover{
            transform: scale(1.05);
            box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
            cursor:pointer;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pace-js@latest/pace-theme-default.min.css">
    <style>
        .pace {
            width: 140px;
            height: 300px;
            position: fixed;
            top: -90px;
            right: -20px;
            z-index: 2000;
            -webkit-transform: scale(0);
            -moz-transform: scale(0);
            -ms-transform: scale(0);
            -o-transform: scale(0);
            transform: scale(0);
            opacity: 0;
            -webkit-transition: all 2s linear 0s;
            -moz-transition: all 2s linear 0s;
            transition: all 2s linear 0s;
        }
        .pace.pace-active {
            -webkit-transform: scale(.25);
            -moz-transform: scale(.25);
            -ms-transform: scale(.25);
            -o-transform: scale(.25);
            transform: scale(.25);
            opacity: 1;
        }
        .pace .pace-activity {
            width: 140px;
            height: 140px;
            border-radius: 70px;
            background: #d52b12;
            position: absolute;
            top: 0;
            z-index: 1911;
            -webkit-animation: pace-bounce 1s infinite;
            -moz-animation: pace-bounce 1s infinite;
            -o-animation: pace-bounce 1s infinite;
            -ms-animation: pace-bounce 1s infinite;
            animation: pace-bounce 1s infinite;
        }
        .pace .pace-progress {
            position: absolute;
            display: block;
            left: 50%;
            bottom: 0;
            z-index: 1910;
            margin-left: -30px;
            width: 60px;
            height: 75px;
            background: rgba(20, 20, 20, .1);
            box-shadow: 0 0 20px 35px rgba(20, 20, 20, .1);
            border-radius: 30px / 40px;
            -webkit-transform: scaleY(.3) !important;
            -moz-transform: scaleY(.3) !important;
            -ms-transform: scaleY(.3) !important;
            -o-transform: scaleY(.3) !important;
            transform: scaleY(.3) !important;
            -webkit-animation: pace-compress .5s infinite alternate;
            -moz-animation: pace-compress .5s infinite alternate;
            -o-animation: pace-compress .5s infinite alternate;
            -ms-animation: pace-compress .5s infinite alternate;
            animation: pace-compress .5s infinite alternate;
        }
        @-webkit-keyframes pace-bounce {
            0% {
                top: 0;
                -webkit-animation-timing-function: ease-in;
            }
            40% {}
            50% {
                top: 140px;
                height: 140px;
                -webkit-animation-timing-function: ease-out;
            }
            55% {
                top: 160px;
                height: 120px;
                border-radius: 70px / 60px;
                -webkit-animation-timing-function: ease-in;
            }
            65% {
                top: 120px;
                height: 140px;
                border-radius: 70px;
                -webkit-animation-timing-function: ease-out;
            }
            95% {
                top: 0;
                -webkit-animation-timing-function: ease-in;
            }
            100% {
                top: 0;
                -webkit-animation-timing-function: ease-in;
            }
        }
        @-moz-keyframes pace-bounce {
            0% {
                top: 0;
                -moz-animation-timing-function: ease-in;
            }
            40% {}
            50% {
                top: 140px;
                height: 140px;
                -moz-animation-timing-function: ease-out;
            }
            55% {
                top: 160px;
                height: 120px;
                border-radius: 70px / 60px;
                -moz-animation-timing-function: ease-in;
            }
            65% {
                top: 120px;
                height: 140px;
                border-radius: 70px;
                -moz-animation-timing-function: ease-out;}
            95% {
                top: 0;
                -moz-animation-timing-function: ease-in;
            }
            100% {top: 0;
                -moz-animation-timing-function: ease-in;
            }
        }
        @keyframes pace-bounce {
            0% {
                top: 0;
                animation-timing-function: ease-in;
            }
            50% {
                top: 140px;
                height: 140px;
                animation-timing-function: ease-out;
            }
            55% {
                top: 160px;
                height: 120px;
                border-radius: 70px / 60px;
                animation-timing-function: ease-in;
            }
            65% {
                top: 120px;
                height: 140px;
                border-radius: 70px;
                animation-timing-function: ease-out;
            }
            95% {
                top: 0;
                animation-timing-function: ease-in;
            }
            100% {
                top: 0;
                animation-timing-function: ease-in;
            }
        }
        @-webkit-keyframes pace-compress {
            0% {
                bottom: 0;
                margin-left: -30px;
                width: 60px;
                height: 75px;
                background: rgba(20, 20, 20, .1);
                box-shadow: 0 0 20px 35px rgba(20, 20, 20, .1);
                border-radius: 30px / 40px;
                -webkit-animation-timing-function: ease-in;
            }
            100% {
                bottom: 30px;
                margin-left: -10px;
                width: 20px;
                height: 5px;
                background: rgba(20, 20, 20, .3);
                box-shadow: 0 0 20px 35px rgba(20, 20, 20, .3);
                border-radius: 20px / 20px;
                -webkit-animation-timing-function: ease-out;
            }
        }
        @-moz-keyframes pace-compress {
            0% {
                bottom: 0;
                margin-left: -30px;
                width: 60px;
                height: 75px;
                background: rgba(20, 20, 20, .1);
                box-shadow: 0 0 20px 35px rgba(20, 20, 20, .1);
                border-radius: 30px / 40px;
                -moz-animation-timing-function: ease-in;
            }
            100% {
                bottom: 30px;
                margin-left: -10px;
                width: 20px;
                height: 5px;
                background: rgba(20, 20, 20, .3);
                box-shadow: 0 0 20px 35px rgba(20, 20, 20, .3);
                border-radius: 20px / 20px;
                -moz-animation-timing-function: ease-out;
            }
        }
        @keyframes pace-compress {
            0% {
                bottom: 0;
                margin-left: -30px;
                width: 60px;
                height: 75px;
                background: rgba(20, 20, 20, .1);
                box-shadow: 0 0 20px 35px rgba(20, 20, 20, .1);
                border-radius: 30px / 40px;
                animation-timing-function: ease-in;
            }
            100% {
                bottom: 30px;
                margin-left: -10px;
                width: 20px;
                height: 5px;
                background: rgba(20, 20, 20, .3);
                box-shadow: 0 0 20px 35px rgba(20, 20, 20, .3);
                border-radius: 20px / 20px;
                animation-timing-function: ease-out;
            }
        }
    </style>
@endsection

@section('content')
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-md-row justify-content-md-between py-2 text-md-start">
                <div class="flex-grow-1 mb-1 mb-md-0">
                    <h1 class="h3 fw-bold mb-2">
                      {{__('lang.dashboard')}}
                    </h1>
                    <h2 class="h6 fw-medium fw-medium text-muted mb-0">
                        {{ $lang === "fr" ? "Bonjour" : "Hello" }} <strong>@php echo ucfirst(Auth::user()->name); @endphp</strong>
                    </h2>
                </div>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="">{{ __('lang.dashboard')}}</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="content">
        @if(\Illuminate\Support\Facades\Auth::user()->isConseiller())
            <div class="search-filter" style="display:none;">
                @php
                    $firstDay = \Carbon\Carbon::now()->format('m/d/Y');
                    $lastDay = \Carbon\Carbon::now()->format('m/d/Y');
                    $current_month = $firstDay . " - " . $lastDay;
                @endphp
                <input type="text" class="form-control form-control dt_search_field" id="daterange" name="daterange" value="{{ $current_month }}">
                <input type="hidden" name="date_end" />
                <input type="hidden" name="date_start" />
            </div>

        @endif
        @management_agent_support
        <div class="block-content">
            @administration_agent_support
            <!-- Overview -->
            <div style="border-bottom: 1px solid #f5f5f5; padding-left: 0; padding-right: 0; ">
                <h5 class="text-muted">{{__('lang.filtre_globale')}}</h5>
                <div class="search-filter">
                    <form action="" method="post" onsubmit="return false">
                        @csrf
                        <div class="form-group row">
                            <div class="col-4 col-md-2 mb-1">
                                <label for="state" class="sr-only">Status...</label>
                                <select class="form-control dt_search_field dt_search_state" id="state" name="state">
                                </select>
                            </div>
                            <div class="col-4 col-md-2 mb-1">
                                <label for="programme" class="sr-only">Programmes...</label>
                                <select class="form-control dt_search_field dt_search_program" id="program" name="program">

                                </select>
                            </div>
                            @administration
                            <div class="col-4 col-md-3 mb-1">
                                <label for="agent" class="sr-only">Utilisateur...</label>
                                <select class="form-control dt_search_field dt_search_agent" id="agent" name="agent" >
                                </select>
                                <input type="hidden" class="dt_search_field" name="agent-group" value="">
                            </div>
                            @endadministration
                            <div class="col-4 col-md-2 mb-1">
                                <label for="campaign" class="sr-only">Canal...</label>
                                <select class="form-control dt_search_field dt_search_campaign" id="campaign" name="campaign">
                                </select>
                            </div>
                            <div class="col-4 col-md-3 mb-1">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text input-group-text-alt">
                                                <i class="far fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        @php
                                            $firstDay = \Carbon\Carbon::now()->format('m/d/Y');
                                            $lastDay = \Carbon\Carbon::now()->format('m/d/Y');
                                            $current_month = $firstDay . " - " . $lastDay;
                                        @endphp
                                        <input type="text" class="form-control form-control dt_search_field" id="daterange" name="daterange" value="{{ $current_month }}">
                                        <input type="hidden" name="date_end" />
                                        <input type="hidden" name="date_start" />
                                        <!-- <div class="input-group-append">
                                            <span class="input-group-text input-group-text-alt">
                                                <i class="far fa-envelope"></i>
                                            </span>
                                        </div> -->
                                    </div>
                                </div>
                                <!-- <input class="form-control form-control-alt" type="text" name="daterange" value="01/01/2018 - 01/15/2018" /> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endadministration_agent_support
        </div>
        <div class="block-content">
            <!-- Overview -->
            <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding-left: 0; padding-right: 0; ">
                <h5 class="text-muted">@administration {{__('lang.Statistiques_des_Conseillers')}} @else {{__('lang.Mes_Statistiques')}} @endadministration</h5>
            </div>
            <div class="row row-deck">
                @management_agent_support
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="10">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_10">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.customers')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-leads">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.leads')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-8"></div>
                @endmanagement_agent_support
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column recycled list-leads-filter">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-new-leads">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.Nouveaux_leads')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column recycled list-leads-filter" data-filter="processed" data-value="processed">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-processed-leads">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.Leads_traités')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column recycled list-leads-filter" data-filter="status" data-value="n/d">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_null">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.Leads_non_traités')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="1">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_1">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.Rencontres_1')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="9">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_9">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.No_Show')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="4">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_4">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.RDV_1')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="12">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_12">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.No_RDV_1')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="7">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_7">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.Admissibles')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="8">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_8">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.no_Admissibles')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="5">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_5">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.Contrats')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="11">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_11">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.no_Contrats')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                @management_agent_support
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="calls" data-value="">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-calls">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.Appels')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="calls">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-durations">0:0</dt>
                                <dd class="text-muted mb-0">{{__('lang.Durée_appels')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                @endmanagement_agent_support
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="3">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_3">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.Injoignables')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="2">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_2">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.Pas_Intéressés')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="6">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_6">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.Numéro_erronés')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                @management_agent_support
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-messages">0</dt>
                                <dd class="text-muted mb-0">{{__('lang.messages')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                @endmanagement_agent_support
            </div>
            {{--<div class="row row-deck">
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-leads">*</dt>
                                <dd class="text-muted mb-0">Prospects</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-processed-leads">*</dt>
                                <dd class="text-muted mb-0">Traité</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                @foreach(\App\Models\LeadStatus::pluck("status_label_fr", "id") as $id => $status)
                    <div class="col-sm-4 col-xl-2">
                        <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="{{ $id }}">
                            <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                <dl class="mb-0">
                                    <dt class="font-size-h2 font-w700 dashboard-analytics-status_{{ $id }}">*</dt>
                                    <dd class="text-muted mb-0">{{ $status }}</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="n/d">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-status_null">*</dt>
                                <dd class="text-muted mb-0">Non Traité</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                @agentsupport
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="status" data-value="n/d">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-clients">*</dt>
                                <dd class="text-muted mb-0">Clients</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                @endagentsupport
                @management_agent_support
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="calls" data-value="">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-calls">*</dt>
                                <dd class="text-muted mb-0">Appels</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="calls">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-durations">*</dt>
                                <dd class="text-muted mb-0">Durée des appels</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-messages">*</dt>
                                <dd class="text-muted mb-0">Messages</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                @endmanagement_agent_support
            </div>--}}
        </div>
        @administration
        <div class="block-content">
            <!-- Overview -->
            <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding-left: 0; padding-right: 0; ">
                <h5 class="text-muted">{{__('lang.Statistiques_compagne_de_marketing')}}</h5>
            </div>
            <div class="row row-deck">
            @php
                $campaigns = \App\Models\LeadFormMarketingCampaign::pluck("campaign_name", "id");
            @endphp
            @foreach($campaigns as $id => $campaign)
                <div class="col-sm-4 col-xl-2">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="campaign" data-value="{{ $id }}">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-campaign_{{ $id }}">*</dt>
                                <dd class="text-muted mb-0">{{ $campaign }}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
        <div class="block-content">
            <!-- Overview -->
            <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding-left: 0; padding-right: 0; ">
                <h5 class="text-muted">Finances</h5>
            </div>
            <div class="row row-deck">
                <div class="col-sm-6 col-xl-4">
                    <div class="block block-rounded d-flex flex-column list-leads-filter" data-filter="finances">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-quotes">* MAD</dt>
                                <dd class="text-muted mb-0">CA </dd>
                                <dd class="text-muted mb-0 dashboard-analytics-quotes">Total: 0 Paiments</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                {{--<div class="col-sm-4 col-xl-4">
                    <div class="block block-rounded d-flex flex-column">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700 dashboard-analytics-payment_1">0 MAD</dt>
                                <dd class="text-muted mb-0">Paiement 1 </dd>
                                <dd class="text-muted mb-0 dashboard-analytics-payment_1">Total: 0 Payments</dd>
                            </dl>
                        </div>
                    </div>
                </div>--}}
            </div>
        </div>
        @endadministration
        @endmanagement_agent_support
    </div>
    <div class="modal" id="modal-campaigns" tabindex="-1" role="dialog" aria-labelledby="modal-campaigns" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Campaign Title</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Prospects</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form id="list-leads-filter" action="{{ route("leads-index") }}" method="get" style="display: none;">
        @csrf
    </form>
@endsection
