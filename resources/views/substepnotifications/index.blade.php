@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/substepnotifications.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Historique','subTitle' => 'sub step notifications', 'navItems' => ['Admin', 'Sub step notifications']])

    <!-- Page Content -->
    <div class="content">

        <!-- substepnotifications Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">
                            {{--<div class="col-3">
                                <select class="form-control dt_search_field" name="ranking">
                                    <option value="">Sélectionnez un ranking</option>
                                    @foreach(\App\Models\Ranking::all() as $ranking)
                                        <option value="{{ $ranking->id }}">{{ $ranking->ranking_label }}</option>
                                    @endforeach
                                </select>
                            </div>--}}
                            <div class="col-4">
                                <input type="text" class="form-control dt_search_field" name="name_fr" placeholder="Nom en fr">
                            </div>
                            <div class="col-4">
                                <input type="text" class="form-control dt_search_field" name="name_en" placeholder="Nom en En">
                            </div>
                            <div class="col-3">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="substepnotifications-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Sub Step Notifications</h3>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="substepnotifications-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END substepnotifications Table -->
    </div>
    <!-- END Page Content -->

    <!-- substepnotification Edit Modal -->
    <div class="modal" id="modal-substepnotification-edit" tabindex="-1" role="dialog" aria-labelledby="modal-substepnotification-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-substepnotification-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Modifier substepnotification</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form substepnotification Add -->
                                <form action="" method="POST" onsubmit="return false;" id="substepnotification-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-name_fr">Objet</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-name_fr" name="edit-form-name_fr" placeholder="Name fr.." aria-describedby="edit-form-name_fr-error" aria-invalid="false">
                                        <div id="edit-form-name_fr-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-name_en">Message</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-name_en" name="edit-form-name_en" placeholder="Message.." aria-describedby="edit-form-name_en-error" aria-invalid="false">
                                        <div id="edit-form-name_en-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-email_notification_id">Client</label>
                                        <select class="form-control form-control-alt" id="edit-form-email_notification_id" name="edit-form-email_notification_id" aria-describedby="add-form-email_notification_id-error" aria-invalid="false">
                                            <option value="">Sélectionnez un Object</option>
                                            @foreach(\App\Models\EmailNotification::all() as $en)
                                                <option value="{{ $en->id }}">{{ $en->object }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-email_notification_id-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-info btn-block" id="update-substepnotification-btn"><i class="fa fa-fw fa-check mr-1"></i>Modifier sub step notification</button>
                                    </div>
                                </form>
                                <!-- END Form substepnotification Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END substepnotification Edit Modal -->
@endsection
