@extends('layouts.iframe')

@section('content')
    <!-- Page Content -->
    <div class="content">

        <iframe id="signEasyIframe"
                title="Inline Frame Example"
                width="100%"
                height="1000"
                allowfullscreen
                src="{{ Session::get('signeasy_url') }}">
        </iframe>

    </div>
    <!-- END Page Content -->
@endsection

