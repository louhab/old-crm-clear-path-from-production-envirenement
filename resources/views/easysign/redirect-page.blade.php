@extends('layouts.iframe')

@section('js_after')
    <script>
        $(function(){
            setTimeout(function(){ window.top.location.href = "{!! route('customer-mycase', $program_step) !!}" }, 3000);
        });
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="content">

        <p>Merci d'avoir signé le document, notre service vérifiera le document pour validation, la page sera redirigé dans quelques instants ...</p>

    </div>
    <!-- END Page Content -->
@endsection

