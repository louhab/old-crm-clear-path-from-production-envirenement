@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/schoollevels.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'schoollevels','subTitle' => 'Etapes', 'navItems' => ['Admin', 'schoollevels']])

    <!-- Page Content -->
    <div class="content">

        <!-- schoollevels Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">

                            <div class="col-3">
                                <input type="text" class="form-control dt_search_field" name="school_level_name" placeholder="Niveau">
                            </div>

                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="schoollevels-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Niveaux scolaires</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary" id="schoollevel-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter niveau scolaire</button>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="schoollevels-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END schoollevels Table -->




    </div>
    <!-- END Page Content -->

    <!-- schoollevel Add Modal -->
    <div class="modal" id="modal-schoollevel-add" tabindex="-1" role="dialog" aria-labelledby="modal-schoollevel-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-schoollevel-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter Niveau scolaire</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form schoollevel Add -->
                                <form action="" method="POST" onsubmit="return false;" id="schoollevel-add-form">

                                    <div class="form-group">
                                        <label for="add-form-school_level_name">Niveau scolaire</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-school_level_name" name="add-form-school_level_name" placeholder="Niveau scolaire.." aria-describedby="add-form-school_level_name-error" aria-invalid="false">
                                        <div id="add-form-school_level_name-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-schoollevel-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter niveau scolaire</button>
                                    </div>
                                </form>
                                <!-- END Form schoollevel Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END schoollevel Add Modal -->

    <!-- schoollevel Edit Modal -->
    <div class="modal" id="modal-schoollevel-edit" tabindex="-1" role="dialog" aria-labelledby="modal-schoollevel-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-schoollevel-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Modifier niveau scolaire</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form schoollevel Add -->
                                <form action="" method="POST" onsubmit="return false;" id="schoollevel-edit-form">

                                    <div class="form-group">
                                        <label for="edit-form-school_level_name">Nom</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-school_level_name" name="edit-form-school_level_name" placeholder="Niveau.." aria-describedby="edit-form-school_level_name-error" aria-invalid="false">
                                        <div id="edit-form-school_level_name-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="update-schoollevel-btn"><i class="fa fa-fw fa-check mr-1"></i>Modifier niveau scolaire</button>
                                    </div>
                                </form>
                                <!-- END Form schoollevel Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END schoollevel Edit Modal -->
@endsection
