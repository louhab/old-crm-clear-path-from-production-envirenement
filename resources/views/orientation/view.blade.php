<?php
$lang = session()->get('locale');
?>
@extends( auth()->guard('customer')->user() ? 'layouts.customer':'layouts.backend')
@section('css_after')
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
    @livewireStyles
    <meta http-equiv="Expires" content="Mon, 26 Jul 1997 05:00:00 GMT">
    <meta http-equiv="Pragma" content="no-cache">
    <style>
        .center {
            margin: auto;
            width: 50%;
            /* border: 3px solid green; */
            padding: 10px;
        }
        hr {
            display: block;
            height: 1px;
            border: 0;
            border-top: 1px solid red;
            margin: 1em 0;
            padding: 0;
        }

        .select2-selection__choice__remove {
            color: #fff !important;
        }
    </style>
    {{-- @ahaloua --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
    <script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pace-js@latest/pace-theme-default.min.css">
    <style>
        .pace {
            -webkit-pointer-events: none;
            pointer-events: none;

            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;

            -webkit-perspective: 12rem;
            -moz-perspective: 12rem;
            -ms-perspective: 12rem;
            -o-perspective: 12rem;
            perspective: 12rem;

            z-index: 2000;
            position: fixed;
            height: 6rem;
            width: 6rem;
            margin: auto;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }

        .pace.pace-inactive .pace-progress {
            display: none;
        }

        .pace .pace-progress {
            position: fixed;
            z-index: 2000;
            display: block;
            position: absolute;
            left: 0;
            top: 0;
            height: 6rem;
            width: 6rem !important;
            line-height: 6rem;
            font-size: 2rem;
            border-radius: 50%;
            background: rgba(213, 43, 38, 0.8);
            color: #fff;
            font-family: "Helvetica Neue", sans-serif;
            font-weight: 100;
            text-align: center;

            -webkit-animation: pace-theme-center-circle-spin linear infinite 2s;
            -moz-animation: pace-theme-center-circle-spin linear infinite 2s;
            -ms-animation: pace-theme-center-circle-spin linear infinite 2s;
            -o-animation: pace-theme-center-circle-spin linear infinite 2s;
            animation: pace-theme-center-circle-spin linear infinite 2s;

            -webkit-transform-style: preserve-3d;
            -moz-transform-style: preserve-3d;
            -ms-transform-style: preserve-3d;
            -o-transform-style: preserve-3d;
            transform-style: preserve-3d;
        }

        .pace .pace-progress:after {
            content: attr(data-progress-text);
            display: block;
        }

        @-webkit-keyframes pace-theme-center-circle-spin {
            from { -webkit-transform: rotateY(0deg) }
            to { -webkit-transform: rotateY(360deg) }
        }

        @-moz-keyframes pace-theme-center-circle-spin {
            from { -moz-transform: rotateY(0deg) }
            to { -moz-transform: rotateY(360deg) }
        }

        @-ms-keyframes pace-theme-center-circle-spin {
            from { -ms-transform: rotateY(0deg) }
            to { -ms-transform: rotateY(360deg) }
        }

        @-o-keyframes pace-theme-center-circle-spin {
            from { -o-transform: rotateY(0deg) }
            to { -o-transform: rotateY(360deg) }
        }

        @keyframes pace-theme-center-circle-spin {
            from { transform: rotateY(0deg) }
            to { transform: rotateY(360deg) }
        }
    </style>
@endsection
@section('js_after')
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/additional-methods.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!-- Page JS Code -->
    @livewireScripts
@endsection
@section('content')
    @include('layouts.partials.hero', ['title' => 'Clear 1002','subTitle' => 'Orientation', 'navItems' => ['Clear 1002', 'orientation']])
    <!-- Page Content -->
    <input type="hidden" name="lang" value="{{ $lang }}">
    <div class="content">
        <div class="content status" style="padding-top: 0;padding-bottom: 0;">
            @if($errors->any())
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6" style="text-align: center">
                        <p>
                            <a class="badge badge-danger" href="javascript:void(0)">Veuillez renseigner correctement les informations !</a>
                        </p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            @else
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: center">
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            @endif
        </div>
        <div>
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
        </div>
        <div class="block block-rounded">
            <div class="block-content">
                <div class="row">
                    @php
                        $columns = [
                            "Budget" => "budget",
                            "Niveau Scolaire" => "niveau_scolaire",
                            "Note Scolaire" => "note",
                            "Type d'établissement" => "type_etablissement",
                            "Nom d'etablissement" => "nom_etablissement",
                            "Ville" => "ville",
                            "Session" => "session",
                            "Date limite d'admission" => "date_limit_admission",
                            "Diplômes" => "diplome",
                            "Domaine d'études" => "domaine_etude",
                            "Programmes" => "programme",
                            "Concentration" => "concentration",
                            "Langue" => "langue",
                            "Durée" => "duree",
                            "Frais d'admission" => "frais_admission",
                            "Frais scolaires par année" => "prix_annuel",
                            "Total des frais scolaires" => "prix_total",
                            "Lien" => "lien",
                            "Exigences" => "exigence",
                            "Test de langue" => "test_langue",
                            "Score" => "score",
                            "Objectifs de formation et débouchés du diplôme" => "analyse",
                        ];
                        $program_modal = array_map(function ($item){
                            if (array_key_exists('values', $item) && is_array($item["values"]) && count($item["values"])) {
                                return $item["values"];
                            } elseif (array_key_exists('value', $item))
                                return $item["value"];
                            else return "";
                        }, $program->properties);
                        $program_modal["id"] = $program->id;
                        $program_modal["programme"] = $program->name;
                    @endphp
                    @foreach($columns as $value => $key)
                        <div class="col-md-6">
                            <table class="table table-borderless table-vcenter">
                                <thead>
                                <tr>
                                    <th>{{ $value }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="font-w600 font-size-sm">
                                        @if(array_key_exists($key, $program_modal))
                                            @if(empty($program_modal[$key]))
                                                <a href="javascript:void(0)" class="table-infos" data-key="{{ $key }}">--</a>
                                            @elseif($key == "date_limit_admission")
                                                <a href="javascript:void(0)" class="table-infos" data-key="{{ $key }}">{{ $program_modal[$key]["day"] }} / {{ $program_modal[$key]["month"] }}</a>
                                            @elseif(is_array($program_modal[$key]))
                                                <a href="javascript:void(0)" class="table-infos" data-key="{{ $key }}">{{ implode(" - ", $program_modal[$key]) }}</a>
                                            @else
                                                <a href="javascript:void(0)" class="table-infos" data-key="{{ $key }}">{{ $program_modal[$key] }}</a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
    <!-- END Page Content -->
@endsection

