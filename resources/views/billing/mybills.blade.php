@extends('layouts.customer')

@section('js_after')
    <!-- Page JS Plugins -->

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/customer-bills.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Facturation','subTitle' => 'mes factures', 'navItems' => ['Facturation', 'Mes factures']])

    <!-- Page Content -->
    <div class="content">
        <!-- Full Table -->
        <div class="block block-rounded">
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th>Réf</th>
                            <th style="width: 40%;">Total</th>
                            <th class="text-center" style="width: 100px;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bills as $bill)
                            <tr>
                                <td class="font-w600 font-size-sm">
                                    #FAC{{ sprintf('%06d', $bill->id) }}
                                <td class="font-size-sm">{{ $bill->amount_sum }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <div class="btn-group">
                                            <a href="/pdf/mybill/{{ $bill->id }}" class="btn btn-sm btn-alt-primary">
                                                <i class="fa fa-fw fa-download"></i>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Full Table -->
    </div>
    <!-- END Page Content -->
@endsection
