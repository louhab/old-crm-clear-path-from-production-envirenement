@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/quotes.js') }}"></script>
@endsection
@section('css_after')
    <!-- Styles -->
    <link href="{{ asset('js/plugins/select2/css/select2.css') }}" rel="stylesheet">
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Facturation','subTitle' => 'Devis', 'navItems' => ['Facturation', 'Devis']])

    <!-- Page Content -->
    <div class="content">

        <!-- Quotes Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="cid">Sélectionnez client</label>
                                    <select class="form-control dt_search_field" id="cid" name="cid" style="width: 100%">
                                    </select>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="quotes-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Liste des devis</h3>
                    <div class="block-options">
                        <div class="dropdown">
                            <!-- <button type="button" class="btn btn-alt-primary btn-outline-secondary dropdown-toggle" id="dropdown-dropup-outline-secondary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-fw fa-plus mr-1"></i>
                                Ajouter Devis
                            </button> -->
                            <div class="dropdown-menu font-size-sm" aria-labelledby="dropdown-dropup-outline-secondary">
                                <a class="dropdown-item" href="javascript:void(0)" id="show-services-student">Etudiant</a>
                                <a class="dropdown-item" href="javascript:void(0)" id="show-services-immigrant">Immigrant</a>
                            </div>
                        </div>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="quotes-dt">
                    <thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Quotes Table -->



    </div>
    <!-- END Page Content -->

    @include('billing.modals.services')


@endsection
