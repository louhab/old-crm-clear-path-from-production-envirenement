<!-- add Services Modal -->
<div class="modal" id="modal-services" tabindex="-1" role="dialog" aria-labelledby="modal-services" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="block block-rounded block-themed block-transparent mb-0" id="block-services">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Ajouter Facture</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    <div class="row">
                        <div class="col-md-4" id="pt-select-wrapper">
                            <div class="form-group">
                                <label for="pt-select">Sélectionnez type prix</label>
                                <select class="form-control" id="pt-select" name="pt-select">
                                    <option value="1" selected>Célibataire</option>
                                    <option value="2">Marié</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8" id="customer-select-wrapper">
                            <div class="form-group">
                                <label for="customer-select">Sélectionnez client</label>
                                <select class="form-control" id="customer-select" name="customer-select" style="width: 100%">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-vcenter" id="services-table">
                            <thead>
                            <tr>
                                <th class="text-center">Phase</th>
                                <th style="width: 50%;">Service</th>
                                <th style="width: 15%;">Prix</th>
                                <th class="text-center">Quantité</th>
                                <th class="text-center">Séléct</th>
                            </tr>
                            </thead>
                            <tbody id="services-table-body">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="block-content block-content-full text-right border-top">
                    <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Annuler</button>
                    <button type="button" class="btn btn-alt-primary mr-1" id="save-op">Enregistrer</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END add Services Modal -->
