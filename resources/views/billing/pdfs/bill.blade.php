@extends('layouts.pdf')

@section('content')
    <div id="invoice">
        <div class="invoice overflow-auto">
            <div>
                <header>
                    <div class="row">
                        <div class="col">
                            <img class="image" style="width: 150px;" src="{{ public_path('/img/logo.png') }}" alt="">
                        </div>
                    </div>
                </header>
                <main>
                    <div class="text-center">
                        @if($bill->customer->program_id == 4)
                            <h3 style="color:#c6394e;">Facture Etude au Canada</h3>
                        @else
                            <h3 style="color:#c6394e;">Facture pour Immigration au Canada</h3>
                        @endif
                    </div>
                    <table class="table table-bordered border-danger">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Description</th>
                            @if($bill->customer->program_id == 4)
                                <th>Prix</th>
                            @else
                                <th>Célibataire</th>
                                <th>Couple</th>
                            @endif
                            <!-- <th>Quantité</th> -->
                            <!-- <th>Total</th> -->
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $groupedResultsArray = [];
                            $ttc = 0;
                            $ttc_couple = 0;
                        @endphp
                        @foreach($bill->services->sortBy('phase_id') as $service)
                            @php
                                $groupedResultsArray[$service->phase->name][] = $service;
                            @endphp
                        @endforeach
                        @foreach($groupedResultsArray as $phase)
                            @php
                                $qte_sum = 0;
                                $qte_sum_couple = 0;
                            @endphp
                            @if($loop->last)
                                <tr>
                                    @if($bill->customer->program_id == 4)
                                        <td style="text-align:left;color:#c6394e;" colspan="2">Total TTC</td>
                                        <td style="text-align:right;color:#c6394e;">{{ $ttc }}</td>
                                    @else
                                        <td style="text-align:left;color:#c6394e;" colspan="2">Total TTC</td>
                                        <td style="text-align:right;color:#c6394e;">{{ $ttc }}</td>
                                        <td style="text-align:right;color:#c6394e;">{{ $ttc_couple }}</td>
                                    @endif
                                </tr>
                            @endif
                            @foreach($phase as $service)
                            <tr>
                                @if($loop->first) <td class="no" rowspan='{{ count($phase) }}'>{{ $phase[0]->phase->name }} </td>@endif
                                <td>
                                    <div class="text-muted">
                                        @if(count($service->details))
                                            @foreach($service->details as $detail)
                                                {{ $detail->name }} <br>
                                            @endforeach
                                        @else
                                            {{ $service->name }}
                                        @endif
                                    </div>
                                </td>
                                @if($bill->customer->program_id == 4)
                                    <td class="unit">{{ $service->pivot->billed_price }}</td>
                                @else
                                    <td class="unit">{{ $service->single_price }}</td>
                                    <td class="unit">{{ $service->couple_price }}</td>
                                @endif
                                <!-- <td class="qty">{{ $service->pivot->qte }}</td> -->
                                @php
                                    $qte_sum += $service->single_price;
                                    $qte_sum_couple += $service->couple_price;
                                @endphp
                                <!-- <td class="total">{{ $service->pivot->billed_price * $service->pivot->qte }}</td> -->
                            </tr>
                            @endforeach
                            @php
                                $ttc += $qte_sum;
                                $ttc_couple += $qte_sum_couple;
                            @endphp
                            @if($loop->last)
                                @break
                            @endif
                            <tr class="srow">
                            @if($bill->customer->program_id == 4)
                                <td style="text-align:left;" colspan="2">Total {{ $phase[0]->phase->name }}</td>
                                <td>{{ $qte_sum }}</td>
                            @else
                                <td style="text-align:left;" colspan="2">Total {{ $phase[0]->phase->name }}</td>
                                <td>{{ $qte_sum }}</td>
                                <td>{{ $qte_sum_couple }}</td>
                            @endif
                            </tr>
                        @endforeach
                        </tbody>
                        <!-- <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td>Sous total</td>
                            <td>{{ $bill->amount_sum }}</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">TVA 25%</td>
                            <td>$1,300.00</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td>Total</td>
                            <td>{{ $bill->amount_sum }}</td>
                        </tr>
                        </tfoot> -->
                    </table>
                    <img style="width: 180px;position:absolute;top:820px;right:0;" src="{{ public_path('/img/cachett.png') }}" alt="">
                    <!--<div class="notices">
                        <div>NOTICE:</div>
                        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
                    </div>-->
                </main>
                <footer>
                    <p  style="font-size: 10px;"> <strong>Agence Canada</strong> : 2000 McGill College
                        Avenue, 6e étage, Montréal Québec H3A 3H3 <br /><strong>whatsapp</strong> : +1 438404-0960<br>
                        <strong>Agence Maroc</strong> : 42 Rue Bachir Laalej
                        Casablanca <br /><strong>whatsapp</strong> : +212 7 02 05 09 09<br><br />
                        <span style="font-size: 10px;">info@clearpathcanada.ca <br />www.clearpathcanada.ca</span><br> IF : 50152557 | ICE : 002741390000066 | RC : 496031
                    </p>
                </footer>
            </div>
            <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
            <div></div>
        </div>
    </div>
@endsection
