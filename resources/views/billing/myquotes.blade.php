@extends('layouts.customer')

@section('js_after')
    <!-- Page JS Plugins -->

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/customer-quotes.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Facturation','subTitle' => 'mes devis', 'navItems' => ['Facturation', 'Mes devis']])

    <!-- Page Content -->
    <div class="content">
        <!-- Full Table -->
        <div class="block block-rounded">
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th>Réf</th>
                            <th style="width: 40%;">Total</th>
                            <th class="text-center" style="width: 100px;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($quotes as $quote)
                            <tr>
                                <td class="font-w600 font-size-sm">
                                    #DEV{{ sprintf('%06d', $quote->id) }}
                                <td class="font-size-sm">{{ $quote->amount_sum }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <div class="btn-group">
                                            <a href="/pdf/myquote/{{ $quote->id }}" class="btn btn-sm btn-alt-primary">
                                                <i class="fa fa-fw fa-download"></i>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Full Table -->
    </div>
    <!-- END Page Content -->
@endsection
