@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/phases.js') }}"></script>
@endsection

@section('content')
    @include('layouts.partials.hero', ['title' => 'Phases','subTitle' => 'Etapes', 'navItems' => ['Admin', 'Phases']])

    <!-- Page Content -->
    <div class="content">

        <!-- phases Table -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <h4>Faire une recherche</h4>
                <div class="row push search-filter">
                    <div class="col-lg-12">
                        <div class="form-group form-row">
                            <div class="col-3">
                                <select class="form-control dt_search_field" name="ranking">
                                    <option value="">Sélectionnez un programme</option>
                                    @foreach(\App\Models\Program::all() as $program)
                                        <option value="{{ $program->id }}">{{ $program->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-3">
                                <input type="text" class="form-control dt_search_field" name="name" placeholder="Nom">
                            </div>

                            <div class="col-2">
                                <button type="button" class="btn btn-alt-primary btn-block submit-search"><i class="fa fa-fw fa-search mr-1"></i>Rechercher</button>
                            </div>
                            <div class="col-1">
                                <button type="button" class="btn btn-alt-danger btn-block clear-search"><i class="fa fa-fw fa-redo mr-1"></i></button>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="block block-rounded" id="phases-dt-block">
            <div class="block-content block-content-full">
                <div class="block-header" style="border-bottom: 1px solid #f5f5f5; padding: 0;">
                    <h3 style="margin-top: 15px;">Phases</h3>
                    <div class="block-options">
                        <button type="button" class="btn btn-alt-primary" id="phase-table-add"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter phase</button>
                    </div>
                </div>
                <h4></h4>

                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="phases-dt">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END phases Table -->




    </div>
    <!-- END Page Content -->

    <!-- phase Add Modal -->
    <div class="modal" id="modal-phase-add" tabindex="-1" role="dialog" aria-labelledby="modal-phase-add" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-phase-add">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Ajouter phase</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form phase Add -->
                                <form action="" method="POST" onsubmit="return false;" id="phase-add-form">
                                    <div class="form-group">
                                        <label for="add-form-program_id">Programme</label>
                                        <select class="form-control form-control-alt" id="add-form-program_id" name="add-form-program_id" aria-describedby="add-form-program_id-error" aria-invalid="false">
                                            <option value="">Sélectionnez un programme</option>
                                            @foreach(\App\Models\Program::all() as $program)
                                                <option value="{{ $program->id }}">{{ $program->name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="add-form-program_id-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-name">Nom</label>
                                        <input type="text" class="form-control form-control-alt" id="add-form-name" name="add-form-name" placeholder="Nom.." aria-describedby="add-form-name-error" aria-invalid="false">
                                        <div id="add-form-name-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-order">Order</label>
                                        <input type="number" class="form-control form-control-alt" id="add-form-order" name="add-form-order" placeholder="Ordre.." aria-describedby="add-form-order-error" aria-invalid="false">
                                        <div id="add-form-order-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="add-phase-btn"><i class="fa fa-fw fa-plus mr-1"></i>Ajouter phase</button>
                                    </div>
                                </form>
                                <!-- END Form phase Add -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END phase Add Modal -->

    <!-- phase Edit Modal -->
    <div class="modal" id="modal-phase-edit" tabindex="-1" role="dialog" aria-labelledby="modal-phase-edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0" id="block-phase-edit">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Modifier phase</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Form phase Edit -->
                                <form action="" method="POST" onsubmit="return false;" id="phase-edit-form">
                                    <div class="form-group">
                                        <label for="edit-form-program_id">Programme</label>
                                        <select class="form-control form-control-alt" id="edit-form-program_id" name="edit-form-program_id" aria-describedby="edit-form-program_id-error" aria-invalid="false">
                                            <option value="">Sélectionnez un programme</option>
                                            @foreach(\App\Models\Program::all() as $program)
                                                <option value="{{ $program->id }}">{{ $program->name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="edit-form-program_id-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-form-name">Nom</label>
                                        <input type="text" class="form-control form-control-alt" id="edit-form-name" name="edit-form-name" placeholder="Nom.." aria-describedby="edit-form-name-error" aria-invalid="false">
                                        <div id="edit-form-name-error" class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-form-order">Order</label>
                                        <input type="number" class="form-control form-control-alt" id="edit-form-order" name="edit-form-order" placeholder="Ordre.." aria-describedby="edit-form-order-error" aria-invalid="false">
                                        <div id="edit-form-order-error" class="invalid-feedback"></div>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-alt-primary btn-block" id="update-phase-btn"><i class="fa fa-fw fa-check mr-1"></i>Modifier phase</button>
                                    </div>
                                </form>
                                <!-- END Form phase Edit -->
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END phase Edit Modal -->
@endsection
